### Run server ###
```
cd ./docker
cp .env.exmaple .env
docker-compose up -d
```

dump mysql
```
//положить dump базы в /docker/db/db.sql
cd ./docker
sh setup_db.sh
```

_в другой консоли_
```
cd ./docker
sh setup.sh
```

запуск фронт разработки
```
docker exec -it site_php npm run watch
```

Phpmyadmin
http://localhost:8181

Web
https://localhost:8180

### Setup

php artisan vendor:publish --provider="Huangdijia\Sitemap\SitemapServiceProvider"
