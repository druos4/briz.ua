<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faqs', function (Blueprint $table) {
            $table->id();
            $table->integer('faq_group_id');
            $table->text('title_ru')->nullable();
            $table->text('title_ua')->nullable();
            $table->text('title_en')->nullable();
            $table->text('answer_ru')->nullable();
            $table->text('answer_ua')->nullable();
            $table->text('answer_en')->nullable();
            $table->integer('sort')->nullable();
            $table->boolean('active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs');
    }
}
