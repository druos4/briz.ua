<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('property_id');
            $table->text('value_ru');
            $table->text('value_ua')->nullable();
            $table->text('value_en')->nullable();
            $table->integer('sort')->nullable();
            $table->text('slug')->nullable();
            $table->text('meta_keyword_ru')->nullable();
            $table->text('meta_keyword_ua')->nullable();
            $table->text('meta_keyword_en')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_values');
    }
}
