<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHelpDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('help_directories', function (Blueprint $table) {
            $table->id();
            $table->text('title_ru');
            $table->text('title_ua');
            $table->text('title_en');
            $table->text('slug');
            $table->text('icon')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('sort')->nullable();
            $table->tinyInteger('active')->nullable();
            $table->text('anons_ru')->nullable();
            $table->text('anons_ua')->nullable();
            $table->text('anons_en')->nullable();
            $table->text('detail_ru')->nullable();
            $table->text('detail_ua')->nullable();
            $table->text('detail_en')->nullable();
            $table->text('meta_title_ru')->nullable();
            $table->text('meta_title_ua')->nullable();
            $table->text('meta_title_en')->nullable();
            $table->text('meta_description_ru')->nullable();
            $table->text('meta_description_ua')->nullable();
            $table->text('meta_description_en')->nullable();
            $table->text('meta_keywords_ru')->nullable();
            $table->text('meta_keywords_ua')->nullable();
            $table->text('meta_keywords_en')->nullable();
            $table->string('articles_show_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('help_directories');
    }
}
