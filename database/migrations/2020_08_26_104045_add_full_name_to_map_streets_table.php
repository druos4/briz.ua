<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFullNameToMapStreetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('map_streets', function (Blueprint $table) {
            $table->text('full_name_ru')->nullable();
            $table->text('full_name_ua')->nullable();
            $table->text('full_name_en')->nullable();
            $table->float('lat')->nullable();
            $table->float('lon')->nullable();
            $table->text('coordinates')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('map_streets', function (Blueprint $table) {
            //
        });
    }
}
