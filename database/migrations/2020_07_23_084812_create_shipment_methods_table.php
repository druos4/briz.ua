<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment_methods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title_ru');
            $table->text('title_ua')->nullable();
            $table->text('title_en')->nullable();
            $table->tinyInteger('active')->nullable();
            $table->text('code')->nullable();
            $table->float('price')->nullable();
            $table->text('type')->nullable();
            $table->text('has_api')->nullable();
            $table->integer('sort')->nullable();
            $table->text('picture')->nullable();
            $table->text('description_ru')->nullable();
            $table->text('description_ua')->nullable();
            $table->text('description_en')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment_methods');
    }
}
