<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('menu_id');
            $table->integer('parent_id')->nullable();
            $table->integer('sort')->nullable();
            $table->text('item_type');
            $table->text('title_ru')->nullable();
            $table->text('title_ua')->nullable();
            $table->text('title_en')->nullable();
            $table->text('url')->nullable();
            $table->tinyInteger('blank')->nullable();
            $table->text('color')->nullable();
            $table->tinyInteger('bold')->nullable();
            $table->text('options')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
