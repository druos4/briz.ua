<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->id();
            $table->text('title_ru')->nullable();;
            $table->text('title_ua')->nullable();
            $table->text('title_en')->nullable();
            $table->text('picture')->nullable();
            $table->text('url')->nullable();
            $table->text('anons_ru')->nullable();
            $table->text('anons_ua')->nullable();
            $table->text('anons_en')->nullable();
            $table->integer('sort')->nullable();
            $table->integer('active')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
