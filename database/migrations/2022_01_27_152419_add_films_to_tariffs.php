<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilmsToTariffs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tariffs', function (Blueprint $table) {
            $table->boolean('has_films')->default(false);
            $table->text('films_title_ru')->nullable();
            $table->text('films_title_ua')->nullable();
            $table->text('films_title_en')->nullable();
            $table->float('films_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tariffs', function (Blueprint $table) {
            $table->dropColumn('has_films');
            $table->dropColumn('films_title_ru');
            $table->dropColumn('films_title_ua');
            $table->dropColumn('films_title_en');
            $table->dropColumn('films_price');
        });
    }
}
