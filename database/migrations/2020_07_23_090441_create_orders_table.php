<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->float('total')->nullable();
            $table->text('status_id');
            $table->integer('payment_id')->nullable();
            $table->integer('shipment_id')->nullable();
            $table->text('email')->nullable();
            $table->text('first_name')->nullable();
            $table->text('last_name')->nullable();
            $table->text('middle_name')->nullable();
            $table->text('phone')->nullable();
            $table->tinyInteger('dont_call_me')->nullable();
            $table->text('promocode')->nullable();
            $table->text('comments')->nullable();
            $table->text('delivery_address')->nullable();
            $table->text('delivery_city')->nullable();
            $table->text('session_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
