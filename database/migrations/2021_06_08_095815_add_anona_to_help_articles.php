<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAnonaToHelpArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('help_articles', function (Blueprint $table) {
            $table->text('anons_ru')->nullable();
            $table->text('anons_ua')->nullable();
            $table->text('anons_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('help_articles', function (Blueprint $table) {
            //
        });
    }
}
