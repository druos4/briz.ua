<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUslugiToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->text('uslugy_title_ru')->nullable();
            $table->text('uslugy_title_ua')->nullable();
            $table->text('uslugy_title_en')->nullable();
            $table->text('uslugy_description_ru')->nullable();
            $table->text('uslugy_description_ua')->nullable();
            $table->text('uslugy_description_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            //
        });
    }
}
