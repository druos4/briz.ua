<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title_ru');
            $table->text('title_ua')->nullable();
            $table->text('title_en')->nullable();
            $table->text('slug');
            $table->tinyInteger('active');
            $table->dateTime('show_from')->nullable();
            $table->text('picture')->nullable();
            $table->text('anons_ru')->nullable();
            $table->text('anons_ua')->nullable();
            $table->text('anons_en')->nullable();
            $table->text('detail_ru')->nullable();
            $table->text('detail_ua')->nullable();
            $table->text('detail_en')->nullable();
            $table->tinyInteger('on_top')->nullable();
            $table->integer('user_id')->nullable();
            $table->text('meta_title_ru')->nullable();
            $table->text('meta_title_ua')->nullable();
            $table->text('meta_title_en')->nullable();
            $table->text('meta_description_ru')->nullable();
            $table->text('meta_description_ua')->nullable();
            $table->text('meta_description_en')->nullable();
            $table->text('meta_keywords_ru')->nullable();
            $table->text('meta_keywords_ua')->nullable();
            $table->text('meta_keywords_en')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
