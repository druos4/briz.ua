<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapStreetsNewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_streets_new', function (Blueprint $table) {
            $table->id();
            $table->integer('street_ref');
            $table->integer('city_id');
            $table->integer('city_ref');
            $table->text('name_ru')->nullable();
            $table->text('name_ua')->nullable();
            $table->text('name_en')->nullable();

            $table->text('type_ru')->nullable();
            $table->text('type_ua')->nullable();
            $table->text('type_en')->nullable();
            $table->text('type_short')->nullable();

            $table->float('lat')->nullable();
            $table->float('lon')->nullable();
            $table->text('coordinates')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_streets_new');
    }
}
