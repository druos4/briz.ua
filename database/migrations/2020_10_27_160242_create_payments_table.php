<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->text('title_ru');
            $table->text('title_ua')->nullable();
            $table->text('title_en')->nullable();
            $table->boolean('commission')->nullable();
            $table->text('icon')->nullable();
            $table->boolean('active')->nullable();
            $table->text('description_ru')->nullable();
            $table->text('description_ua')->nullable();
            $table->text('description_en')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
