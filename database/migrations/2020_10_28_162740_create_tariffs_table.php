<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tariffs', function (Blueprint $table) {
            $table->id();
            $table->string('tariff_type');
            $table->boolean('monthly')->nullable();
            $table->boolean('daily')->nullable();
            $table->integer('user_type')->nullable();
            $table->text('title_ru')->nullable();
            $table->text('title_ua')->nullable();
            $table->text('title_en')->nullable();
            $table->float('price')->nullable();
            $table->float('price_union')->nullable();
            $table->boolean('bonus_payment')->nullable();
            $table->boolean('get_bonus')->nullable();
            $table->boolean('can_seven_days')->nullable();
            $table->boolean('prepay_discount')->nullable();
            $table->text('description_ru')->nullable();
            $table->text('description_ua')->nullable();
            $table->text('description_en')->nullable();
            $table->integer('billing_id')->nullable();
            $table->boolean('active')->nullable();
            $table->integer('sort')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariffs');
    }
}
