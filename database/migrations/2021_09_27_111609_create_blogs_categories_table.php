<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs_categories', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('title_ru');
            $table->string('title_ua');
            $table->string('title_en');
            $table->boolean('is_active')->default(false);
            $table->string('meta_title_ru')->nullable();
            $table->string('meta_title_ua')->nullable();
            $table->string('meta_title_en')->nullable();
            $table->string('meta_description_ru')->nullable();
            $table->string('meta_description_ua')->nullable();
            $table->string('meta_description_en')->nullable();
            $table->string('meta_keywords_ru')->nullable();
            $table->string('meta_keywords_ua')->nullable();
            $table->string('meta_keywords_en')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs_categories');
    }
}
