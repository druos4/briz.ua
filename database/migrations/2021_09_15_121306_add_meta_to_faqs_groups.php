<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMetaToFaqsGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('faqs_groups', function (Blueprint $table) {
            $table->text('description_ru')->nullable();
            $table->text('description_ua')->nullable();
            $table->text('description_en')->nullable();
            $table->text('meta_title_ru')->nullable();
            $table->text('meta_title_ua')->nullable();
            $table->text('meta_title_en')->nullable();
            $table->text('meta_description_ru')->nullable();
            $table->text('meta_description_ua')->nullable();
            $table->text('meta_description_en')->nullable();
            $table->text('meta_keywords_ru')->nullable();
            $table->text('meta_keywords_ua')->nullable();
            $table->text('meta_keywords_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faqs_groups', function (Blueprint $table) {
            $table->dropColumn('description_ru');
            $table->dropColumn('description_ua');
            $table->dropColumn('description_en');
            $table->dropColumn('meta_title_ru');
            $table->dropColumn('meta_title_ua');
            $table->dropColumn('meta_title_en');
            $table->dropColumn('meta_description_ru');
            $table->dropColumn('meta_description_ua');
            $table->dropColumn('meta_description_en');
            $table->dropColumn('meta_keywords_ru');
            $table->dropColumn('meta_keywords_ua');
            $table->dropColumn('meta_keywords_en');
        });
    }
}
