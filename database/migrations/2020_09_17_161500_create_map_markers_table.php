<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapMarkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_markers', function (Blueprint $table) {
            $table->id();
            $table->text('address_name');
            $table->text('address_num');
            $table->text('lat');
            $table->text('lon');
            $table->tinyInteger('has_inet')->nullable();
            $table->tinyInteger('has_ctv')->nullable();
            $table->tinyInteger('has_iptv')->nullable();
            $table->tinyInteger('has_giga')->nullable();
            $table->integer('addr_id')->nullable();
            $table->integer('addrtv_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_markers');
    }
}
