<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_addresses', function (Blueprint $table) {
            $table->id();
            $table->integer('map_street_id');
            $table->string('dom');
            $table->integer('map_dom_type_id');
            $table->tinyInteger('inet')->nullable();
            $table->tinyInteger('ctv')->nullable();
            $table->tinyInteger('iptv')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_addresses');
    }
}
