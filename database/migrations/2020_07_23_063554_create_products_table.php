<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title_ru');
            $table->text('title_ua');
            $table->text('title_en');
            $table->text('slug');
            $table->tinyInteger('active');
            $table->integer('category_id');
            $table->text('picture')->nullable();
            $table->integer('sort');
            $table->text('article')->nullable();
            $table->tinyInteger('new')->nullable();
            $table->tinyInteger('hit')->nullable();
            $table->text('anons_ru')->nullable();
            $table->text('anons_ua')->nullable();
            $table->text('anons_en')->nullable();
            $table->text('description_ru')->nullable();
            $table->text('description_ua')->nullable();
            $table->text('description_en')->nullable();
            $table->float('price');
            $table->float('discount')->nullable();
            $table->float('price_old')->nullable();
            $table->integer('quantity');
            $table->float('rating')->nullable();
            $table->text('meta_title_ru')->nullable();
            $table->text('meta_title_ua')->nullable();
            $table->text('meta_title_en')->nullable();
            $table->text('meta_description_ru')->nullable();
            $table->text('meta_description_ua')->nullable();
            $table->text('meta_description_en')->nullable();
            $table->text('meta_keywords_ru')->nullable();
            $table->text('meta_keywords_ua')->nullable();
            $table->text('meta_keywords_en')->nullable();
            $table->integer('views')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
