<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique();
            $table->string('title_ru')->nullable();
            $table->string('title_ua')->nullable();
            $table->string('title_en')->nullable();
            $table->boolean('is_active')->default(false);
            $table->text('picture')->nullable();
            $table->text('anons_ru')->nullable();
            $table->text('anons_ua')->nullable();
            $table->text('anons_en')->nullable();
            $table->integer('likes_count')->default(0);
            $table->integer('views_count')->default(0);
            $table->integer('read_time')->nullable();
            $table->timestamps();
            $table->dateTime('published_at')->nullable();
            $table->integer('updated_by')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->integer('deleted_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
