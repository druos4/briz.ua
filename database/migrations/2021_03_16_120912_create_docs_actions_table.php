<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocsActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docs_actions', function (Blueprint $table) {
            $table->id();
            $table->text('title_ru');
            $table->text('title_ua');
            $table->text('title_en');
            $table->boolean('active')->nullable();
            $table->integer('sort')->nullable();
            $table->text('link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docs_actions');
    }
}
