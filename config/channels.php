<?php

return [
    [
        'url' => 'https://bil-api.briz.ua/sat/ctv',
        'tarif_ids' => [43,48],
        'is_analog' => true,
    ],
    [
        'url' => 'https://bil-api.briz.ua/sat/dtv/13',
        'tarif_ids' => [43,48],
        'is_analog' => false,
    ],
    [
        'url' => 'https://bil-api.briz.ua/sat/dtv/21',
        'tarif_ids' => [48],
        'is_analog' => false,
    ],
    [
        'url' => 'https://bil-api.briz.ua/sat/iptv/54',
        'tarif_ids' => [54],
        'is_analog' => false,
    ],
    [
        'url' => 'https://bil-api.briz.ua/sat/iptv/55',
        'tarif_ids' => [55],
        'is_analog' => false,
    ]
];
