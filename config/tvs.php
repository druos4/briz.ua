<?php
//список основных CTV, IPTV
return [
    'packets' => [
        'ctv' => [
            'basic_packet_ids' => env('BASE_CTV'),
        ],
        'iptv' => [
            'basic_packet_ids' => env('BASE_IPTV'),
        ]
    ],
];
