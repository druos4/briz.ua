<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Street extends Model
{
    protected $table = 'map_streets';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name_ru',
        'name_ua',
        'name_en',
    ];


    public function addresses()
    {
        return $this->hasMany(Address::class,'map_street_id');
    }

}
