<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpArticle extends Model
{
    protected $table = 'help_articles';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'slug',
        'icon',
        'help_directory_id',
        'sort',
        'active',
        'anons_ru',
        'anons_ua',
        'anons_en',
        'detail_ru',
        'detail_ua',
        'detail_en',
        'meta_title_ru',
        'meta_title_ua',
        'meta_title_en',
        'meta_description_ru',
        'meta_description_ua',
        'meta_description_en',
        'meta_keywords_ru',
        'meta_keywords_ua',
        'meta_keywords_en',
    ];

    public function directory()
    {
        return $this->belongsTo(HelpDirectory::class, 'help_directory_id');
    }

    public function scopeActive($query){
        return $query->where('active', true);
    }

}
