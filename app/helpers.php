<?php

use App\Genre;
use App\News;
use App\Slider;
use App\Tariff;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Services\MenuService;
use App\Services\FilterService;
//use Browser;

function getLastModForIndex()
{
    $lastMod = '2021-05-01 12:00:00';
    $res = Slider::active()->index()->orderBy('updated_at', 'desc')->first();
    if($res){
        if($res->updated_at > $lastMod){
            $lastMod = $res->updated_at;
        }
    }
    $res = Tariff::active()->orderBy('updated_at','desc')->first();
    if($res){
        if($res->updated_at > $lastMod){
            $lastMod = $res->updated_at;
        }
    }
    $res = News::active()->published()->orderBy('updated_at', 'desc')->first();
    if($res){
        if($res->updated_at > $lastMod){
            $lastMod = $res->updated_at;
        }
    }
    return $lastMod;
}

function checkLangActive($lang){
    $res = DB::table('site_langs')->select()->where('code','=',$lang)->first();
    if(isset($res->id) && $res->active == 1){
        return true;
    } else {
        return false;
    }
}

function getLangs(){
    $res = DB::table('site_langs')->select()->where('code','!=','ru')->get();
    if(isset($res) && count($res) > 0){
        return $res;
    } else {
        return [];
    }
}




function getPublicLangs(){
    $arLangs = [];
    $res = DB::table('site_langs')->select()->get();
    foreach($res as $r){
        $arLangs[$r->code] = $r->active;
    }
    return view('client.partials.lang-switcher',['langs' => LaravelLocalization::getSupportedLocales(), 'arLangs' => $arLangs])->render();
}


function getLocaleHrefPrefix()
{
    if(app()->getLocale() != 'ua'){
        return '/'. app()->getLocale();
    }
    return '';
}

function getLangDBSuf($request)
{
    return '_'.$request->get('lang');
}

function getLocaleDBSuf()
{
    return '_' . app()->getLocale();
}

function switchToLang($lang){
    /*$url = \Request::getRequestUri();
    $url = str_replace('/ua/','/',$url);
    if($lang == 'ua'){
        $url = '/ua'.$url;
    }

    if(auth()->id() > 0){
        $row = \DB::table('users_langs')->select()->where('user_id','=',auth()->id())->first();
        if(isset($row->id) && $row->id > 0){
            \DB::table('users_langs')->where('user_id','=',auth()->id())->update(['lang' => $lang, 'updated_at' => \Carbon\Carbon::now()]);
        } else {
            \DB::table('users_langs')->insert(['user_id' => auth()->id(), 'lang' => $lang, 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()]);
        }
    }
    session(['lang' => $lang]);
    return $url;*/

}


function getCaptcha(){
    $arrNumbers = ['1' => trans('auth.numbers.one'),
        '2' => trans('auth.numbers.two'),
        '3' => trans('auth.numbers.three'),
        '4' => trans('auth.numbers.four'),
        '5' => trans('auth.numbers.five'),
        '6' => trans('auth.numbers.six'),
        '7' => trans('auth.numbers.seven'),
        '8' => trans('auth.numbers.eight'),
        '9' => trans('auth.numbers.nine')];
    $a = rand(1,9);
    $b = rand(1,9);
    if($a > $b){
        $res['r'] = $a - $b;
        $res['txt'] = trans('auth.captcha').' '.$arrNumbers[$a].' '.trans('auth.numbers.minus').' '.$arrNumbers[$b];
    } else {
        $res['r'] = $a + $b;
        $res['txt'] = trans('auth.captcha').' '.$arrNumbers[$a].' '.trans('auth.numbers.plus').' '.$arrNumbers[$b];
    }
    return $res;
}

function getMenu()
{
    $menu = new MenuService('main_menu');
    return $menu->getMenu();
}

function getCategoryFilter($category_id)
{
    $filter = new FilterService($category_id);
    $result = $filter->getFilters();
    return $result;
}

function clearPhoneInput($phone)
{
    $phone = str_replace(' ','',$phone);
    $phone = str_replace('-','',$phone);
    $phone = str_replace('_','',$phone);
    $phone = str_replace('(','',$phone);
    $phone = str_replace(')','',$phone);
    return $phone;
}


function cmpChannels($a, $b) {
    if ($a['sort'] == $b['sort']) {
        return 0;
    }
    return ($a['sort'] < $b['sort']) ? -1 : 1;
}


function getPageMeta($with_full = false)
{
    $arr_url = parse_url(url()->current());
    $url = (!empty($arr_url['path'])) ? str_replace(['/ru/','/en/'],'/',$arr_url['path']) : '/';
    if(in_array($url,['/ru','/en'])){
        $url = '/';
    }
    $meta = [];
    $page = \App\Page::where('slug',$url)->first();

    if($page){
        $meta = [
            'title' => $page->{'title' . getLocaleDBSuf()},
            'meta-title' => $page->{'meta_title' . getLocaleDBSuf()},
            'meta-description' => $page->{'meta_description' . getLocaleDBSuf()},
            'meta-keywords' => $page->{'meta_keywords' . getLocaleDBSuf()},
            'lastmod' => (!empty($page->updated_at)) ? $page->updated_at : false,
        ];
        if($with_full == true){
            $meta['detail'] = $page->{'detail' . getLocaleDBSuf()};
        }
    }

    return $meta;
}

function isAdmin()
{
    $result = false;
    $user = User::with('roles')->where('id',auth()->id())->first();
    if(!empty($user->roles)){
        foreach($user->roles as $role){
            if($role->id == 1){
                $result = true;
            }
        }
    }
    return $result;
}

function getLocale()
{
    $current_lang = app()->getLocale();
    $current_lang.= '-';
    return $current_lang;
}

function drawLastMod($lastmod = false)
{
    $datetime = ($lastmod !== false) ? $lastmod : '2021-05-01 12:00:00';
    $html = '<meta http-equiv="Last-Modified" content="'.date('r',strtotime($datetime)).'">
';
    return $html;
}

function drawAltLangs()
{
    $arr = parse_url(url()->current());
    if(!empty($arr['path'])){
        $url = str_replace(['/ru/','/en/'],'/',$arr['path']);
        if(in_array($url,['/en','/ru'])){
            $url = '';
        }
        $html = '<link rel="alternate" hreflang="uk-UA" href="https://www.briz.ua'.$url.'" />
    <link rel="alternate" hreflang="ru-UA" href="https://www.briz.ua/ru'.$url.'" />
    <link rel="alternate" hreflang="en-UA" href="https://www.briz.ua/en'.$url.'" />';
    } else {
        $html = '<link rel="alternate" hreflang="uk-UA" href="https://www.briz.ua" />
    <link rel="alternate" hreflang="ru-UA" href="https://www.briz.ua/ru" />
    <link rel="alternate" hreflang="en-UA" href="https://www.briz.ua/en" />';
    }
    return $html;
}

function drawRelCanonicalTag()
{
    $arr = parse_url(url()->current());
    /*
    if(strpos(url()->current(),'equipment') !== false){
        $url = $arr['path'];
        $f = parse_url(\Request::getRequestUri());
        if(!empty($f['query'])){
            $q_arr = explode('&',$f['query']);
            foreach($q_arr as $k => $q){
                if(strpos($q,'sort=') !== false){
                    unset($q_arr[$k]);
                }
            }
            if(!empty($q_arr)){
                $url .= '?'.implode('&',$q_arr);
            }
        }
         $html = '
    <link rel="canonical" href="https://www.briz.ua'.$url.'" />';

    } else {
        if(!empty($arr['path'])){
            $url = $arr['path'];
            $html = '
    <link rel="canonical" href="https://www.briz.ua'.$url.'" />';
        } else {
            $url = url()->current();
            $html = '
    <link rel="canonical" href="'.$url.'" />';
        }
    }
*/
    if(!empty($arr['path'])){
        $url = $arr['path'];
        $html = '
    <link rel="canonical" href="https://www.briz.ua'.$url.'" />';
    } else {
        $url = url()->current();
        $html = '
    <link rel="canonical" href="'.$url.'" />';
    }


    return $html;
}

function getOgUrl()
{
    $arr = parse_url(url()->current());
    return (!empty($arr['path'])) ? env('APP_URL').$arr['path'] : env('APP_URL');
}

function prepareMeta($meta)
{
    if(isset($meta['meta-title'])){
        $meta['meta_title'] = $meta['meta-title'];
        unset($meta['meta-title']);
    }
    if(isset($meta['meta-description'])){
        $meta['meta_description'] = $meta['meta-description'];
        unset($meta['meta-description']);
    }
    if(isset($meta['meta-keywords'])){
        $meta['meta_keywords'] = $meta['meta-keywords'];
        unset($meta['meta-keywords']);
    }
    $meta['lastmod'] = (!empty($meta['lastmod'])) ? $meta['lastmod'] : '2021-05-01 12:00:00';
    return $meta;
}

function drawDateTime($datetime)
{
    $dt = strtotime($datetime);
    $str = date('d.m.Y',$dt).'<br />'.date('H:i:s',$dt);
    return $str;
}

function drawDate($datetime)
{
    $dt = strtotime($datetime);
    $str = date('d.m.Y',$dt);
    return $str;
}

function drawShortDate($datetime)
{
    $arr = explode(' ',$datetime);
    return $arr[0];
}

function addNofollowToLink($txt)
{
    if(strpos($txt,'<a') !== false){
        $dom = new DOMDocument();
        $dom->loadHTML(mb_convert_encoding($txt,'HTML-ENTITIES','UTF-8'));
        $links = $dom->getElementsByTagName('a');
        if(!empty($links)){
            foreach($links as $link){
                $href = $link->getAttribute('href');
                if(strpos($href,'http') !== false){
                    if(strpos($href,'briz.ua') === false){
                        $link->setAttribute('rel', 'nofollow');
                    } elseif(strpos($href,'stat.briz.ua') !== false){
                        $link->setAttribute('rel', 'nofollow');
                    }
                }
            }
            $txt = $dom->saveHTML();
        }
        $txt = mb_convert_encoding($txt,'UTF-8','HTML-ENTITIES');
    }

    return $txt;
}

/**
 * Функция, добавляющая всем изображениям атрибуты alt,title используя заголовок страницы
 *
 * @param string $txt исходный текст, который надо модифицировать
 * @param string $title заголовок страницы
 * @return string
 */
function addAltToPics($txt, $title)
{
    if(strpos($txt,'<img') !== false){
        $txt = str_replace('&','&amp;',$txt);
        $dom = new DOMDocument();
        $dom->loadHTML(mb_convert_encoding($txt,'HTML-ENTITIES','UTF-8'));
        $pics = $dom->getElementsByTagName('img');

        if(!empty($pics)){
            $i = 0;
            foreach($pics as $pic){
                $i++;
                $printTitle = (count($pics) > 1) ? $title.','.$i : $title;
                $printAlt = (count($pics) > 1) ? $title.','.$i.' - '.trans('custom.helpBriz') : $title.' - '.trans('custom.helpBriz');
                $pic->setAttribute('alt', $printAlt);
                $pic->setAttribute('title', $printTitle);
            }
            $txt = mb_convert_encoding($dom->saveHTML(),'UTF-8','HTML-ENTITIES');
            //$txt = $dom->saveHTML();
        }
    }
    return $txt;
}

/**
 * Функция, выдающая список всех изображений статьи
 *
 * @param string $data
 * @return array
 */
function getPicsForMicro($data)
{
    $arr = [];
    if(!empty($data['picture'])){
        $arr[] = '"'.env('APP_URL').$data['picture'].'"';
    }
    if(strpos($data['detail'],'<img') !== false){
        $dom = new DOMDocument();
        $dom->loadHTML(mb_convert_encoding($data['detail'],'HTML-ENTITIES','UTF-8'));
        $pics = $dom->getElementsByTagName('img');

        if(!empty($pics)){
            foreach($pics as $pic){
                $arr[] = '"'.$pic->getAttribute('src').'"';
            }
        }
    }
    return $arr;
}

function processLink($input){


    return $input;
}


function detectIE()
{
    $family = Browser::browserFamily();
    if(in_array($family,['Internet Explorer','Sputnik Browser'])){
        return true;
    }
    return false;
}

/*
 * Получение ID базовых ТВ-пакетов
 * return array
 * */
function getBaseTvsPackets()
{
    $ids = [];
    $arrCtvs = explode(',',config('tvs.packets.ctv.basic_packet_ids'));
    $arrIptvs = explode(',',config('tvs.packets.iptv.basic_packet_ids'));
    if(!empty($arrCtvs)){
        foreach($arrCtvs as $arrCtv){
            $id = trim($arrCtv);
            if(!empty($id)){
                $ids[] = $id;
            }
        }
    }
    if(!empty($arrIptvs)){
        foreach($arrIptvs as $arrIptv){
            $id = trim($arrIptv);
            if(!empty($id)){
                $ids[] = $id;
            }
        }
    }

    return $ids;
}

function getLangsCodes()
{
    $langs = [];
    $res = config('laravellocalization.supportedLocales');
    if(!empty($res)){
        foreach($res as $key => $val){
            $langs[] = $key;
        }
    }
    return $langs;
}

function storeDeletedUrl($url)
{
    if(!empty($url)){
        \App\DeletedUrl::updateOrCreate(['url' => $url], ['url' => $url]);
    }
}
