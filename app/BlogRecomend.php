<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogRecomend extends Model
{
    protected $table = 'blogs_recomends';

    protected $dates = [
        'updated_at',
        'created_at'
    ];

    protected $fillable = [
        'blog_id',
        'target_id'
    ];

    public function blog()
    {
        return $this->belongsTo(Blog::class,'blog_id');
    }

    public function target()
    {
        return $this->belongsTo(Blog::class,'target_id');
    }

}
