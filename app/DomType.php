<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DomType extends Model
{
    protected $table = 'map_dom_types';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name_ru',
        'name_ua',
        'name_en',
    ];


    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function user()
    {
        return $this->hasMany(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function scopeActive($query){
        return $query->whereNull('deleted_at');
    }

    public function scopeNoOrder($query){
        return $query->whereNull('order_id');
    }


    public static function rowExist($product_id)
    {
        $exist = Cart::whereNull('order_id')
                ->whereNull('deleted_at')
                ->where('session_id','=', session()->getId())
                ->where('product_id','=', $product_id)
                ->first();

        if(isset($exist->id)){
            return $exist;
        } else {
            return false;
        }
    }


    public static function getCartCount()
    {
        $count = 0;
        $summ = 0;
        $sess = '';
        $res = DB::table('cart')
                ->select()
                ->where('session_id',session()->getId())
                ->whereNull('deleted_at')
                ->whereNull('order_id')
                ->get();

        if(isset($res) && count($res) > 0){
            foreach ($res as $r){
                $sess = $r->session_id;
                if($r->product_type == 'bundle'){
                    $res2 = DB::table('cart_bundles')->select()->where('cart_id','=',$r->id)->get();
                    foreach($res2 as $r2){
                        $count += $r2->qnt;
                    }
                } else {
                    $count += $r->qnt;
                }
                $summ += $r->total;
            }
        }
        return ['qnt' => $count,'total' => $summ,'sess' => $sess];
    }



    public static function getBigCart($order_id = null)
    {
        $cart = ['count' => 0,
            'total' => 0];

        $query = DB::table('cart')
            ->select()
            ->whereNull('deleted_at');
        if(isset($order_id) && $order_id > 0){
            $query->where('order_id','=',$order_id);
        } else {
            $query->where('session_id',session()->getId())->whereNull('order_id');
        }
        $res = $query->get();
        $resourses = getResourseUnitsArray();
        foreach($res as $c){
            if($c->product_type == 'bundle'){
                $prods = [];
                $count = 0;
                $total = 0;
                $products = DB::table('products as p')
                    ->select('p.id','p.title','p.title_ua','p.title_en','p.slug','cat.slug as category_slug','p.picture',
                        'p.article','cb.qnt','cb.price','cb.total','p.opt_from',
                        'p.price_old', 'p.discount', 'p.price_opt', 'p.quantity','p.resourse_unit_id','p.waiting_days')
                    ->leftJoin('cart_bundles as cb','cb.product_id','=','p.id')
                    ->leftJoin('categories as cat','cat.id','=','p.category_id')
                    ->where('cb.cart_id','=',$c->id)
                    ->whereNull('p.deleted_at')
                    ->where('p.active','=',1)
                   // ->where('p.quantity','>',0)
                    ->where('p.price','>',0)
                    ->get();


                foreach ($products as $prod){
                    if(isset($prod->resourse_unit_id) && $prod->resourse_unit_id > 0){
                        if(isset($resourses[$prod->resourse_unit_id])){
                            $r_unit = $resourses[$prod->resourse_unit_id];
                        }
                    }

                    $count += $prod->qnt;
                    $total += $prod->price;
                    $prods[] = ['id' => $prod->id,
                        'title' => $prod->title,
                        'title_ua' => $prod->title_ua,
                        'title_en' => $prod->title_en,
                        'article' => $prod->article,
                        'slug' => $prod->slug,
                        'category_slug' => $prod->category_slug,
                        'picture' => $prod->picture,
                        'price' => $prod->price,
                        'price_opt' => $prod->price_opt,
                        'opt_from' => $prod->opt_from,
                        'price_old' => $prod->price_old,
                        'discount' => $prod->discount,
                        'qnt' => $prod->qnt,
                        'quantity' => $prod->quantity,
                        'total' => $prod->total,
                        'waiting_days' => $prod->waiting_days,
                        'resourse_unit' => (!empty($r_unit)) ? $r_unit : ''
                        ];
                }

                $cart['products'][] = ['id' => $c->id,
                    'price' => $total,
                    'qnt' => $c->qnt,
                    'total' => round(($total * $c->qnt),2),
                    'product' => $prods
                ];
                $cart['count'] += $count;
                $cart['total'] += round(($total * $c->qnt),2);

            } else {

                $prod = DB::table('products as p')
                    ->select('p.id','p.title','p.title_ua','p.title_en','p.slug','cat.slug as category_slug','p.picture',
                        'p.price','p.article','p.opt_from',
                        'p.price_old', 'p.discount', 'p.price_opt', 'p.quantity','p.waiting_days')
                    ->leftJoin('categories as cat','cat.id','=','p.category_id')
                    ->where('p.id','=',$c->product_id)
                    ->whereNull('p.deleted_at')
                    ->where('p.active','=',1)
                   // ->where('p.quantity','>',0)
                    ->where('p.price','>',0)
                    ->first();
                if($prod){
                    $price = $prod->price;
                    $sum = round(($price * $c->qnt),2);
                    $cart['count'] += $c->qnt;
                    $cart['total'] += $sum;
                    if(isset($prod->resourse_unit_id) && $prod->resourse_unit_id > 0){
                        if(isset($resourses[$prod->resourse_unit_id])){
                            $r_unit = $resourses[$prod->resourse_unit_id];
                        }
                    }
                    $cart['products'][] = ['id' => $c->id,
                        'price' => $price,
                        'qnt' => $c->qnt,
                        'total' => $sum,
                        'product' => [
                            'id' => $prod->id,
                            'title' => $prod->title,
                            'title_ua' => $prod->title_ua,
                            'title_en' => $prod->title_en,
                            'article' => $prod->article,
                            'slug' => $prod->slug,
                            'category_slug' => $prod->category_slug,
                            'picture' => $prod->picture,
                            'price' => $price,
                            'price_opt' => $prod->price_opt,
                            'opt_from' => $prod->opt_from,
                            'price_old' => $prod->price_old,
                            'discount' => $prod->discount,
                            'qnt' => $c->qnt,
                            'quantity' => $prod->quantity,
                            'total' => $sum,
                            'waiting_days' => $prod->waiting_days,
                            'resourse_unit' => (!empty($r_unit)) ? $r_unit : '']
                    ];
                }

            }
        }

        return $cart;
    }


    public static function getSmallCart()
    {
        $cart = [];
        $total = 0;
        $count = 0;
        $query = DB::table('cart')
            ->select()
            ->whereNull('deleted_at')
            ->where('session_id',session()->getId())->whereNull('order_id');
        $res = $query->get();
        $resourses = getResourseUnitsArray();
        foreach($res as $c){
            if($c->product_type == 'bundle'){
                $products = DB::table('products as p')
                    ->select('p.id','p.title','p.title_ua','p.title_en','p.slug','cat.slug as category_slug','p.picture',
                        'p.article','cb.qnt','cb.price','cb.total','p.opt_from',
                        'p.price_old', 'p.discount', 'p.price_opt', 'p.quantity','p.resourse_unit_id','p.waiting_days')
                    ->leftJoin('cart_bundles as cb','cb.product_id','=','p.id')
                    ->leftJoin('categories as cat','cat.id','=','p.category_id')
                    ->where('cb.cart_id','=',$c->id)
                    ->whereNull('p.deleted_at')
                    ->where('p.active','=',1)
                  //  ->where('p.quantity','>',0)
                    ->where('p.price','>',0)
                    ->get();

                foreach ($products as $prod){
                    if(isset($prod->resourse_unit_id) && $prod->resourse_unit_id > 0){
                        if(isset($resourses[$prod->resourse_unit_id])){
                            $r_unit = $resourses[$prod->resourse_unit_id];
                        }
                    }
                    $cart[] = ['id' => $prod->id,
                        'title' => $prod->title,
                        'url' => '/'.$prod->category_slug.'/'.$prod->slug,
                        'picture' => $prod->picture,
                        'price' => $prod->price,
                        'price_opt' => $prod->price_opt,
                        'opt_from' => $prod->opt_from,
                        'price_old' => $prod->price_old,
                        'discount' => $prod->discount,
                        'qnt' => $prod->qnt,
                        'quantity' => $prod->quantity,
                        'total' => $prod->total,
                        'waiting_days' => $prod->waiting_days,
                        'resourse_unit' => (!empty($r_unit)) ? $r_unit : ''
                    ];
                }
                $total += round(($total * $c->qnt),2);
                $count += $count;

            } else {

                $prod = DB::table('products as p')
                    ->select('p.id','p.title','p.title_ua','p.title_en','p.slug','cat.slug as category_slug','p.picture',
                        'p.price', 'p.price_old', 'p.discount', 'p.price_opt','p.article','p.opt_from',
                        'p.price_old', 'p.discount', 'p.price_opt', 'p.quantity','p.resourse_unit_id','p.waiting_days')
                    ->leftJoin('categories as cat','cat.id','=','p.category_id')
                    ->where('p.id','=',$c->product_id)
                    ->whereNull('p.deleted_at')
                    ->where('p.active','=',1)
                  //  ->where('p.quantity','>',0)
                    ->where('p.price','>',0)
                    ->first();
                if($prod){
                    if(isset($prod->resourse_unit_id) && $prod->resourse_unit_id > 0){
                        if(isset($resourses[$prod->resourse_unit_id])){
                            $r_unit = $resourses[$prod->resourse_unit_id];
                        }
                    }
                    $price = $prod->price;
                    $sum = round(($price * $c->qnt),2);
                    $count += $c->qnt;
                    $total += $sum;

                    $cart[] = ['id' => $c->id,
                        'price' => $price,
                        'price_old' => $prod->price_old,
                        'discount' => $prod->discount,
                        'price_opt' => $prod->price_opt,
                        'qnt' => $c->qnt,
                        'total' => $sum,
                        'product' => [
                            'id' => $prod->id,
                            'title' => $prod->title,
                            'url' => '/'.$prod->category_slug.'/'.$prod->slug,
                            'picture' => $prod->picture,
                            'price' => $price,
                            'price_opt' => $prod->price_opt,
                            'opt_from' => $prod->opt_from,
                            'price_old' => $prod->price_old,
                            'discount' => $prod->discount,
                            'qnt' => $c->qnt,
                            'quantity' => $prod->quantity,
                            'total' => $sum,
                            'waiting_days' => $prod->waiting_days,
                            'resourse_unit' => (!empty($r_unit)) ? $r_unit : ''
                        ]
                    ];
                }

            }
        }

        return ['products' => $cart, 'total' => $total, 'count' => $count];
    }


    public function delete(){
        DB::table('cart')->where('id','=',$this->id)->delete();
    }



    public static function syncWithOrder($id){
        if(auth()->id() > 0){
            DB::table('cart')
                ->whereNull('order_id')
                ->whereNull('deleted_at')
                ->where('session_id','=',session()->getId())
                ->update(['order_id' => $id, 'user_id' => auth()->id()]);
        } else {
            DB::table('cart')
                ->whereNull('order_id')
                ->whereNull('deleted_at')
                ->where('session_id','=',session()->getId())
                ->update(['order_id' => $id]);
        }

        $res = DB::table('cart as c')
            ->select('c.product_id','c.qnt','p.title','p.picture')
            ->leftJoin('products as p','p.id','=','c.product_id')
            ->where('c.order_id','=',$id)
            ->get();
        return $res;
    }



}
