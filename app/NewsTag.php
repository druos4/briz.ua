<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class NewsTag extends Model
{
    protected $table = 'news_tags';

    protected $dates = [
        'updated_at',
        'created_at'
    ];

    protected $fillable = [
        'news_id',
        'tag_id',
    ];

    public function tag()
    {
        return $this->belongsTo(Tag::class,'tag_id');
    }

    public function news()
    {
        return $this->belongsTo(News::class,'news_id');
    }

}
