<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Address extends Model
{
    protected $table = 'map_addresses';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'map_street_id',
        'dom',
        'map_dom_type_id',
        'inet',
        'ctv',
        'iptv',
    ];


    public function street()
    {
        return $this->belongsTo(Street::class,'map_street_id');
    }

    public function domType()
    {
        return $this->belongsTo(DomType::class,'map_dom_type_id');
    }

    public function scopeInet($query)
    {
        return $query->where('inet', 1);
    }

    public function scopeCtv($query)
    {
        return $query->where('ctv', 1);
    }

    public function scopeIptv($query)
    {
        return $query->where('iptv', 1);
    }

}
