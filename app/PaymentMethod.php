<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PaymentMethod extends Model
{
    protected $table = 'payment_methods';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'active',
        'code',
        'price',
        'type',
        'sort',
        'picture',
        'description_ru',
        'description_ua',
        'description_en',
    ];


    public function scopeVisible($query) {
        return $query->where('active', true);
    }



}
