<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'session_id',
        'phone',
        'sourse',
    ];

}
