<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductComment extends Model
{
    protected $table = 'product_comments';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'product_id',
        'parent_id',
        'rate',
        'name',
        'comment',
        'moderated_at',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function scopeModerated($query) {
        return $query->whereNotNull('moderated_at');
    }

    public function scopeIsParent($query) {
        return $query->whereNull('parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(ProductComment::class,'parent_id');
    }

    public function children()
    {
        return $this->hasMany(ProductComment::class,'parent_id')->orderBy('created_at','desc');
    }

}
