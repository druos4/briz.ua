<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = 'emails';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'email',
        'session_id',
    ];

}
