<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogLike extends Model
{
    protected $table = 'blogs_likes';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'blog_id',
        'session_id',
    ];

    public function blog()
    {
        return $this->belongsTo(Blog::class,'blog_id');
    }

}
