<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'commission',
        'icon',
        'icon_active',
        'active',
        'description_ru',
        'description_ua',
        'description_en',
        'sort',
    ];


    public function scopeActive($query) {
        return $query->where('active', true);
    }

}
