<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genres';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'code',
        'sort',
    ];

    public function channels()
    {
        return $this->hasManyThrough(Channel::class,'channels_genres','genre_id','channel_id');
    }



}
