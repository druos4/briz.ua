<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeedbackRequest extends FormRequest
{

    protected function prepareForValidation()
    {
        $phone = str_replace([' ','-','_','(',')'],'',$this->phone);
        $this->merge([
            'phone' => $phone,
        ]);
    }

    public function rules()
    {
        return [
            'phone' => 'required|size:12',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => 'Укажите номер телефона',
            'phone.size' => 'Укажите верный формат номера телефона',
        ];
    }
}
