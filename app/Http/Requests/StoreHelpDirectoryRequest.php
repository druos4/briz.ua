<?php

namespace App\Http\Requests;

use App\News;
use Illuminate\Foundation\Http\FormRequest;

class StoreHelpDirectoryRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('help_access');
    }

    public function rules()
    {
        return [
            'title_ru' => 'required',
            'title_ua' => 'required',
            'title_en' => 'required',
            'slug'    => 'required|unique:help_directories,slug',
            'image' => 'mimes:jpeg,jpg,png,gif,webp',
        ];
    }

    public function messages()
    {
        return [
            'slug.required' => 'SLUG обязателен для заполнения',
            'slug.unique' => 'SLUG должен быть уникальным',
            'title_ru.required' => 'Заголовок RU обязателен для заполнения',
            'title_ua.required' => 'Заголовок UA обязателен для заполнения',
            'title_en.required' => 'Заголовок EN обязателен для заполнения',
            'image.image' => 'Неверный формат файла',
            'image.mimes' => 'Допускаются форматы изображений: jpeg,jpg,png,gif,webp',
        ];
    }
}
