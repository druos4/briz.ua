<?php

namespace App\Http\Requests;

use App\PaymentMethod;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePaymentMethodRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('payment_method_edit');
    }

    public function rules()
    {
        return [
            'title_ru' => [
                'required',
            ],
        ];
    }
}
