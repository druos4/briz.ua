<?php

namespace App\Http\Requests;

use App\PaymentMethod;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePaymentsRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('payments_edit');
    }

    public function rules()
    {
        return [
            'title_ru' => 'required',
            'title_ua' => 'required',
            'title_en' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title_ru.required' => 'Заголовок RU обязателен для заполнения',
            'title_ua.required' => 'Заголовок UA обязателен для заполнения',
            'title_en.required' => 'Заголовок EN обязателен для заполнения',
        ];
    }
}
