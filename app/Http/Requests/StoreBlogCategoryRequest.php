<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBlogCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('news_create');
    }

    public function rules()
    {
        return [
            'title_ru' => 'required',
            'slug'    => 'required|unique:blogs,slug',
            'title_ua' => 'required',
            'title_en' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required' => 'SLUG обязателен для заполнения',
            'slug.unique' => 'SLUG должен быть уникальным',
            'title_ru.required' => 'Заголовок RU обязателен для заполнения',
            'title_ua.required' => 'Заголовок UA обязателен для заполнения',
            'title_en.required' => 'Заголовок EN обязателен для заполнения',
        ];
    }
}
