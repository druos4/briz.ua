<?php

namespace App\Http\Requests;

use App\News;
use Illuminate\Foundation\Http\FormRequest;

class StorePaymentMethodRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('payment_method_create');
    }

    public function rules()
    {
        return [
            'title_ru' => [
                'required',
            ],
        ];
    }
}
