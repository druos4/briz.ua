<?php

namespace App\Http\Requests;

use App\News;
use Illuminate\Foundation\Http\FormRequest;

class StoreNewsRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('news_create');
    }

    public function rules()
    {
        return [
            'title_ru' => 'required',
            'slug'    => 'required|unique:news,slug',
            'title_ua' => 'required',
            'title_en' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png,gif|dimensions:width=1905,height=516',
        ];
    }

    public function messages()
    {
        return [
            'slug.required' => 'SLUG обязателен для заполнения',
            'slug.unique' => 'SLUG должен быть уникальным',
            'title_ru.required' => 'Заголовок RU обязателен для заполнения',
            'title_ua.required' => 'Заголовок UA обязателен для заполнения',
            'title_en.required' => 'Заголовок EN обязателен для заполнения',
            'image.image' => 'Неверный формат файла',
            'image.dimensions' => 'Баннер должен быть 1905х516px',
            'image.mimes' => 'Допускаются форматы изображений: jpeg,jpg,png,gif,webp',
        ];
    }
}
