<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreServicesRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('services_create');
    }

    public function rules()
    {
        return [
            'code'    => [
                'required',
            ]
        ];
    }
}
