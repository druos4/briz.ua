<?php

namespace App\Http\Requests;

use App\Page;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePageRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('page_edit');
    }

    public function rules()
    {
        return [
            'title_ru' => 'required',
            'slug'    => 'required',
            'title_ua' => 'required',
            'title_en' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'slug.required' => 'URL обязателен для заполнения',
            'title_ru.required' => 'Заголовок RU обязателен для заполнения',
            'title_ua.required' => 'Заголовок UA обязателен для заполнения',
            'title_en.required' => 'Заголовок EN обязателен для заполнения',
        ];
    }
}
