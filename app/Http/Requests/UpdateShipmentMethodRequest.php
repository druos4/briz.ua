<?php

namespace App\Http\Requests;

use App\PaymentMethod;
use Illuminate\Foundation\Http\FormRequest;

class UpdateShipmentMethodRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('shipment_method_edit');
    }

    public function rules()
    {
        return [
            'title_ru' => [
                'required',
            ],
        ];
    }
}
