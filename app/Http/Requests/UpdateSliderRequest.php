<?php

namespace App\Http\Requests;

use App\Slider;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSliderRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('slider_edit');
    }

    public function rules()
    {
        return [
            'image' => 'mimes:jpeg,jpg,png,gif,webp|dimensions:width=1905,height=520',
        ];
    }

    public function messages()
    {
        return [
            'image.image' => 'Неверный формат файла',
            'image.dimensions' => 'Баннер должен быть 1905х520px',
            'image.mimes' => 'Допускаются форматы изображений: jpeg,jpg,png,gif,webp',
        ];
    }
}
