<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateServicesRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('services_edit');
    }

    public function rules()
    {
        return [
            'code'   => [
                'required',
            ]
        ];
    }
}
