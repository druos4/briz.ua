<?php

namespace App\Http\Requests;

use App\News;
use Illuminate\Foundation\Http\FormRequest;

class UpdateNewsRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('news_edit');
    }

    public function rules()
    {
        return [
            'title_ru' => 'required',
            'slug'    => 'required',
            'title_ua' => 'required',
            'title_en' => 'required',
            'image_ru' => 'image',
            'image_ua' => 'image',
            'image_en' => 'image',
            'image' => 'mimes:jpeg,jpg,png,gif,webp|dimensions:width=1905,height=516',
        ];
    }

    public function messages()
    {
        return [
            'slug.required' => 'SLUG обязателен для заполнения',
            'slug.unique' => 'SLUG должен быть уникальным',
            'title_ru.required' => 'Заголовок RU обязателен для заполнения',
            'title_ua.required' => 'Заголовок UA обязателен для заполнения',
            'title_en.required' => 'Заголовок EN обязателен для заполнения',
            'image_ru.image' => 'Неверный формат файла',
            'image_ua.image' => 'Неверный формат файла',
            'image_en.image' => 'Неверный формат файла',
            'image.image' => 'Неверный формат файла',
            'image.dimensions' => 'Баннер должен быть 1905х516px',
            'image.mimes' => 'Допускаются форматы изображений: jpeg,jpg,png,gif',
        ];
    }
}
