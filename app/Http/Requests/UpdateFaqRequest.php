<?php

namespace App\Http\Requests;

use App\News;
use Illuminate\Foundation\Http\FormRequest;

class UpdateFaqRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('faq_edit');
    }

    public function rules()
    {
        return [
            'title_ru' => 'required',
            'title_ua' => 'required',
            'title_en' => 'required',
            'image' => 'max:4096',
        ];
    }

    public function messages()
    {
        return [
            'title_ru.required' => 'Заголовок RU обязателен для заполнения',
            'title_ua.required' => 'Заголовок UA обязателен для заполнения',
            'title_en.required' => 'Заголовок EN обязателен для заполнения',
            'image.image' => 'Неверный формат файла',
            'image.mimes' => 'Допускаются форматы изображений: jpeg,jpg,png,gif,webp',
            'image.max' => 'Допускаются изображения не более 4MB',
        ];
    }
}
