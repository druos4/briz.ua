<?php

namespace App\Http\Requests;

use App\Product;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('product_edit');
    }

    public function rules()
    {
        return [
            'title_ru' => 'required',
            'title_ua' => 'required',
            'title_en' => 'required',
            'slug'    => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif,webp',
            'image_recomend' => 'mimes:jpeg,jpg,png,gif,webp',
        ];
    }

    public function messages()
    {
        return [
            'slug.required' => 'SLUG обязателен для заполнения',
            'slug.unique' => 'SLUG должен быть уникальным',
            'title_ru.required' => 'Заголовок RU обязателен для заполнения',
            'title_ua.required' => 'Заголовок UA обязателен для заполнения',
            'title_en.required' => 'Заголовок EN обязателен для заполнения',
            'image.required' => 'Загрузите фото товара',
            'image.image' => 'Неверный формат файла',
            'image_recomend.image' => 'Неверный формат файла',

        ];
    }
}
