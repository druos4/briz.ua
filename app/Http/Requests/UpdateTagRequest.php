<?php

namespace App\Http\Requests;

use App\Tag;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTagRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('tag_edit');
    }

    public function rules()
    {
        return [
            'title_ru'     => [
                'required',
            ],
            'title_ua'     => [
                'required',
            ],
            'code'    => [
                'required',
            ]
        ];
    }
}
