<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class StoreEmailRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email' => 'required|unique:emails|email',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => trans('custom.emails.validation.required'),
            'email.unique' => trans('custom.emails.validation.unique'),
            'email.email' => trans('custom.emails.validation.email'),
        ];
    }

}
