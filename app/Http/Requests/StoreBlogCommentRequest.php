<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBlogCommentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'author' => 'required',
            'comment' => 'required',
            'id' => 'required',
            'parent' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'author.required' => trans('custom.blog.comment.no-author'),
            'comment.required' => trans('custom.blog.comment.no-comment'),
            'id.required' => trans('custom.blog.comment.no-id'),
            'parent.required' => trans('custom.blog.comment.no-parent'),
        ];
    }
}
