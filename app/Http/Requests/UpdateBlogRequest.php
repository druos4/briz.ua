<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBlogRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('news_create');
    }

    public function rules()
    {
        return [
            'title_ru' => 'required',
            'slug'    => 'required',
            'title_ua' => 'required',
            'title_en' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|dimensions:min_width=765,min_height=500',
        ];
    }

    public function messages()
    {
        return [
            'slug.required' => 'SLUG обязателен для заполнения',
            'slug.unique' => 'SLUG должен быть уникальным',
            'title_ru.required' => 'Заголовок RU обязателен для заполнения',
            'title_ua.required' => 'Заголовок UA обязателен для заполнения',
            'title_en.required' => 'Заголовок EN обязателен для заполнения',
            'image.image' => 'Неверный формат файла',
            'image.dimensions' => 'Изображение должно быть не меньше 765х500px',
            'image.mimes' => 'Допускаются форматы изображений: jpeg,jpg,png,gif,webp',
        ];
    }
}
