<?php

namespace App\Http\Requests;

use App\Slider;
use Illuminate\Foundation\Http\FormRequest;

class StoreSliderRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('slider_create');
    }

    public function rules()
    {
        return [
            'image' => 'required|mimes:jpeg,jpg,png,gif|dimensions:width=1905,height=520',
        ];
    }

    public function messages()
    {
        return [
            'image.image' => 'Неверный формат файла',
            'image.required' => 'Укажите картинку баннера',
            'image.dimensions' => 'Баннер должен быть 1905х520px',
            'image.mimes' => 'Допускаются форматы изображений: jpeg,jpg,png,gif',
        ];
    }
}
