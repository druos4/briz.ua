<?php

namespace App\Http\Requests;

use App\PropertyValues;
use Illuminate\Foundation\Http\FormRequest;

class StorePropertyValueRequest extends FormRequest
{
    public function authorize()
    {
        return \Gate::allows('properties_create');
    }

    public function rules()
    {
        return [
            'value_ru' => [
                'required',
            ],
            'value_ua' => [
                'required',
            ],
            'value_en' => [
                'required',
            ],
        ];
    }

    public function messages()
    {
        return [
            'value_ru.required' => 'Значение RU обязательно для заполнения',
            'value_ua.required' => 'Значение UA обязательно для заполнения',
            'value_en.required' => 'Значение EN обязательно для заполнения',

        ];
    }
}
