<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApiLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->get('lang') != ''){
            switch ($request->get('lang')) {
                case "ua":
                    app()->setLocale('ua');
                    break;
                case "ru":
                    app()->setLocale('ru');
                    break;
                case "en":
                    app()->setLocale('en');
                    break;
            }
        }
        return $next($request);
    }
}
