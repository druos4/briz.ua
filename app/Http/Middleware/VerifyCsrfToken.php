<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/get-header-menu',
        '/check-for-remont',
        '/feedback',
        '/call-me',
        '/order-service',
        '/order-product',
        '/map-connect',
        '/cable-rent',
        '/connect-action',
        '/get-header-menu',
        '/get-header-menu',
        '/get-shop-menu',
        '/shop/product/comments/add',
        '/shop/product/comments/get',
        '/emails',
        '/blog/like',
        '/blog/comment',
        '/map/get-own-markers',
        '/api/news',
        '/api/shares',
    ];
}
