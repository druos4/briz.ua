<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSliderRequest;
use App\Http\Requests\UpdateSliderRequest;
use App\Log;
use App\Services\SliderService;
use App\Slider;
use App\Services\ImageService;
use Illuminate\Http\Request;
use App\Services\LogService;

class SlidersController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('slider_access'), 403);
        if($request->get('reset') == 'y'){
            session(['filtersSliders' => []]);
        }
        $service = new SliderService();
        $result = $service->getItemsForAdmin($request->all());
        $sliders = $result['items'];
        $filter = $result['filter'];

        return view('admin.sliders.index', compact('sliders','filter'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('slider_create'), 403);

        return view('admin.sliders.create');
    }

    public function store(StoreSliderRequest $request)
    {
        abort_unless(\Gate::allows('slider_create'), 403);
        $service = new SliderService();
        $slider = $service->storeItem($request);

        return redirect()->route('admin.sliders.index')->with('success','Баннер создан!');
    }

    public function edit(Slider $slider)
    {
        abort_unless(\Gate::allows('slider_edit'), 403);

        return view('admin.sliders.edit', compact( 'slider'));
    }

    public function update(UpdateSliderRequest $request, Slider $slider)
    {
        abort_unless(\Gate::allows('slider_edit'), 403);
        $service = new SliderService();
        $service->updateItem($request,$slider);

        return redirect()->route('admin.sliders.index')->with('success','Страница обновлена!');
    }

    public function show(Slider $slider)
    {
        abort_unless(\Gate::allows('slider_show'), 403);
        return view('admin.sliders.show', compact('slider'));
    }

    public function destroy(Slider $slider)
    {
        abort_unless(\Gate::allows('slider_delete'), 403);

        $slider->deleted_by = auth()->id();
        $slider->save();
        $slider->delete();

        return response()->json(['success'=>true, 'id' => $slider->id]);
    }

    public function history($id){
        $slider = Slider::find($id);
        if(!$slider){
            return redirect()->route('admin.sliders.index')->with('danger','Элемент не найден!');
        }
        $log = new LogService();
        $logs = $log->getLogs('Slider',$slider);

        return view('admin.sliders.history', compact('slider','logs'));
    }
}
