<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Category;
use App\Property;
use Illuminate\Http\Request;
use DB;
use App\CategoryProperty;

class CategoryController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('category_access'), 403);
        $categories = Category::where('parent_id', 0)
            ->with('children.children')->orderBy('sort')->get();

        $all_categories = Category::where('parent_id','!=',0)->orderBy('sort','asc')->get();

        foreach($all_categories as $key => $all_category){
            $check = Category::where('id',$all_category->parent_id)->where('parent_id',0)->first();
            if(isset($check->id) && $check->id > 0){
                unset($all_categories[$key]);
            } else {
                //ok
            }
        }

        return view('admin.categories.index', compact('categories','all_categories'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('category_create'), 403);
        $categories = Category::where('parent_id', 0)->orderBy('sort')->get();
        return view('admin.categories.create',compact('categories'));
    }

    public function store(StoreCategoryRequest $request)
    {
        abort_unless(\Gate::allows('category_create'), 403);

        if($request->get('active') == 1){
            //
        } else {
            $request['active'] = 0;
        }

        $maxSort = DB::table('categories')->where('parent_id','=',$request->get('parent_id'))->max('sort');
        $maxSort++;

        $request['sort'] = $maxSort;
        if ($request->hasFile('picture'))
        {
            $f_name = date('YmdHis').$request->file('picture')->getClientOriginalName();
            $file = $request->file('picture');
            $file->move(public_path('images/categories/'),$f_name);
            $request['picture'] = '/images/categories/'.$f_name;
        }

        $category = Category::create($request->all());

        return redirect()->route('admin.categories.index');
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('category_edit'), 403);

        $category = Category::with('properties')->find($id);
        if(!$category){
            return redirect()->route('admin.categories.index');
        }
        $hasProps = [];
        $exist_props = [];
        $categories = Category::where('parent_id', 0)
            ->where('id','!=',$id)
            ->orderBy('sort')
            ->get();

        if(isset($category->properties) && count($category->properties) > 0){
            foreach ($category->properties as $prop){
                $hasProps[] = $prop->id;
                $exist_props[] = 'exist-prop-'.$prop->id;
            }
        }

        $properties = Property::where('active',1)->orderBy('title_ru','asc')->get();

        return view('admin.categories.edit', compact('category','categories','properties','hasProps','exist_props'));
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        abort_unless(\Gate::allows('category_edit'), 403);
        $category = Category::find($id);
        if($request->get('active') == 1){
            $category->active = 1;
        } else {
            $category->active = 0;
        }
        if($request->get('slug') != '') {
            $category->slug = $request->get('slug');
        } else {
            $category->slug = Controller::makeSlug(trim($request->get('title')));
        }
        $category->title_ru = trim($request->get('title_ru'));
        $category->parent_id = $request->get('parent_id');
        $category->meta_title_ru = $request->get('meta_title_ru');
        $category->meta_description_ru = $request->get('meta_description_ru');
        $category->meta_keywords_ru = $request->get('meta_keywords_ru');

        $category->uslugy_title_ru = trim($request->get('uslugy_title_ru'));
        $category->uslugy_title_ua = trim($request->get('uslugy_title_ua'));
        $category->uslugy_title_en = trim($request->get('uslugy_title_en'));
        $category->uslugy_description_ru = trim($request->get('uslugy_description_ru'));
        $category->uslugy_description_ua = trim($request->get('uslugy_description_ua'));
        $category->uslugy_description_en = trim($request->get('uslugy_description_en'));

        if($request->get('title_ua') != ''){
            $category->title_ua = $request->get('title_ua');
            $category->meta_title_ua = $request->get('meta_title_ua');
            $category->meta_description_ua = $request->get('meta_description_ua');
            $category->meta_keywords_ua = $request->get('meta_keywords_ua');

        }
        if($request->get('title_en') != ''){
            $category->title_en = $request->get('title_en');
            $category->meta_title_en = $request->get('meta_title_en');
            $category->meta_description_en = $request->get('meta_description_en');
            $category->meta_keywords_en = $request->get('meta_keywords_en');

        }

        if($request->get('del_picture') == 1){
            $category->picture = '';
        }
        if ($request->hasFile('picture'))
        {
            $f_name = date('YmdHis').$request->file('picture')->getClientOriginalName();
            $file = $request->file('picture');
            $file->move(public_path('images/categories/'),$f_name);
            $category->picture = '/images/categories/'.$f_name;
        }

        $category->save();

        if($request->get('props_sort') != ''){
            CategoryProperty::updateSort($category->id,$request->get('props_sort'));
        }
        //CategoryProperty::updateInFilter($category->id,$request->get('use_in_filter'));
        //CategoryProperty::updateInProduct($category->id,$request->get('show_in_product'));
        //CategoryProperty::updateForSeria($category->id,$request->get('use_for_seria'));

        /*
        if($request->get('use_parent_props') == 1){
            $res = DB::table('categories')->select('id','parent_id')->where('parent_id','=',$category->id)->get();
            if(isset($res) && count($res) > 0){
                foreach ($res as $r){
                    CategoryProperty::copyProps($category->id,$r->id);
                    //CategoryProperty::updateInFilter($r->id,$request->get('use_in_filter'));
                    //CategoryProperty::updateInProduct($r->id,$request->get('show_in_product'));
                    $res2 = DB::table('categories')->select('id','parent_id')->where('parent_id','=',$r->id)->get();
                    if(isset($res2) && count($res2) > 0){
                        foreach ($res2 as $r2){
                            CategoryProperty::copyProps($category->id,$r2->id);
                            //CategoryProperty::updateInFilter($r2->id,$request->get('use_in_filter'));
                            //CategoryProperty::updateInProduct($r2->id,$request->get('show_in_product'));
                        }
                    }
                }
            }
        }
*/

        return redirect()->route('admin.categories.index');
    }

    public function show(Product $product)
    {
        abort_unless(\Gate::allows('product_show'), 403);

        return view('admin.products.show', compact('product'));
    }

    public function destroy(Product $product)
    {
        abort_unless(\Gate::allows('product_delete'), 403);

        $product->delete();

        return back();
    }

    public function massDestroy(MassDestroyProductRequest $request)
    {
        Product::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }



    public function updateActive(Request $request){
        $input = $request->all();
        if(isset($input['id']) && isset($input['active'])){
            if($input['active'] == 1){
                $active = 0;
            } else {
                $active = 1;
            }
            DB::table('categories')->where('id','=',$input['id'])->update(['active' => 0]);
            return response()->json(['success'=>true, 'id' => $input['id'], 'active' => $active]);
        }
    }



    public function saveSort(Request $request){
        foreach(\Request::get('serialized') as $catKey => $category) {
            if(isset($category['children'])) {
                foreach($category['children'] as $childKey => $child) {
                    DB::table('categories')->where('id','=',$child['id'])->update(['sort' => $childKey, 'parent_id' => $category['id']]);
                    /*$this->category->find($child['id'])
                        ->update(['order' => $childKey, 'parent_id' => $category['id'], 'deep' => 1]);
                    */
                    if(isset($child['children'])) {
                        foreach($child['children'] as $chKey => $chVal) {
                            DB::table('categories')->where('id','=',$chVal['id'])->update(['sort' => $chKey, 'parent_id' => $child['id']]);
                            /*$this->category->find($chVal['id'])
                                ->update(['order' => $chKey, 'parent_id' => $child['id'], 'deep' => 2]);*/
                        }
                    }
                }
            }
            //$this->category->find($category['id'])->update(['order' => $catKey, 'parent_id' => 0, 'deep' => 0]);
            DB::table('categories')->where('id','=',$category['id'])->update(['sort' => $catKey, 'parent_id' => 0]);
        }
        return ['success' => true];
    }



    public function delete(Request $request){
        if($request->get('id') > 0){
            $cats = Category::getCategoryWithChilds($request->get('id'));
            if(count($cats) > 0){
                foreach ($cats as $c){
                    DB::table('product_category')->where('category_id','=',$c)->delete();
                    DB::table('categories')->where('id','=',$c)->delete();
                }
            }

            return response()->json(['success'=>true, 'id' => $request->get('id')]);
        }
    }



    public function addProperty(Request $request){
/*
        if(!CategoryProperty::exist($request->get('category_id'), $request->get('property_id'))){
            $catprop = CategoryProperty::create($request->all());
            $prop = Property::find($request->get('property_id'));
            $html = '<tr id="exist-prop-'.$prop->id.'">
                                                <td>'.$prop->title_ru.'</td>
                                                <td>
                                                    <input type="checkbox" name="use_in_filter[]" class="form-control" value="'.$prop->id.'">
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="show_in_product[]" class="form-control" value="'.$prop->id.'">
                                                </td>
                                                <td>
                                                    <a class="btn btn-info btn-prop-del-from-cat" href="" data-id="'.$prop->id.'" role="button" title="Удалить свойство из категории">
                                                        <i class="fas fa-arrow-right"></i>
                                                    </a>
                                                </td>
                                            </tr>';
            return response()->json(['success'=>true, 'id' => $request->get('property_id'), 'html' => $html]);
        }
        */

        $prop = Property::find($request->get('property_id'));
        $ids = explode(',',$request->get('ids'));
        $ids[] = 'exist-prop-'.$prop->id;

        $html = '<tr id="exist-prop-'.$prop->id.'">
                                                <td>'.$prop->title_ru.'</td>';
                                                /*<td>
                                                    <input type="checkbox" name="use_in_filter[]" class="form-control" value="'.$prop->id.'">
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="show_in_product[]" class="form-control" value="'.$prop->id.'">
                                                </td>*/
                                                $html .= '<td>
                                                    <a class="btn btn-info btn-prop-del-from-cat" href="" data-id="'.$prop->id.'" role="button" title="Удалить свойство из категории">
                                                        <i class="fas fa-arrow-right"></i>
                                                    </a>
                                                </td>
                                            </tr>';

        return response()->json(['success'=>true, 'id' => $request->get('property_id'), 'html' => $html, 'ids' => implode(',',$ids)]);
    }



    public function removeProperty(Request $request){
        $prop = Property::find($request->get('property_id'));
        $ids = explode(',',$request->get('ids'));
        $k = 'exist-prop-'.$prop->id;
        if(!empty($ids)){
            foreach($ids as $key => $id){
                if($id == $k){
                    unset($ids[$key]);
                }
            }
        }
        /*
        $row = CategoryProperty::where('category_id',$request->get('category_id'))->where('property_id',$request->get('property_id'))->first();
        $row->delete();
        */
        return response()->json(['success'=>true, 'id' => $request->get('property_id'), 'ids' => implode(',',$ids)]);
    }



    public static function createCategory($title)
    {
        $import = DB::table('categories')->select('id')->where('title','=','Импорт из 1С')->first();
        if(isset($import->id) && $import->id > 0){
            $parent = $import->id;
        } else {
            $parent = 0;
        }
        $data = ['title' => trim($title),
            'active' => 0,
            'slug' => str_slug(trim($title), '-'),
            'parent_id' => $parent,
            'picture' => '',
            'sort' => 100,
            'meta_title' => '',
            'meta_description' => '',
            'meta_keywords' => '',
            'seo_text' => '',
        ];
        $category = Category::create($data);
        return $category->id;
    }

}
