<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreServicesRequest;
use App\Http\Requests\UpdateServicesRequest;
use App\Product;
use App\Service;
use App\ServiceProduct;
use App\Services\ImageService;
use App\Services\TariffService;
use App\Tariff;
use Illuminate\Http\Request;


class ServicesController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('services_access'), 403);

        $pages = Service::with('products')->orderBy('code','asc')->get();

        return view('admin.services.index', compact('pages'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('services_create'), 403);
        $categories = Category::with('children')->orderBy('sort','asc')->get();
        return view('admin.services.create',compact('categories'));
    }

    public function store(StoreServicesRequest $request)
    {
        abort_unless(\Gate::allows('services_create'), 403);

        $image = new ImageService();
        if ($request->hasFile('picture_ru')){
            $request['banner_ru'] = $image->moveImage($request,'picture_ru','Service');
        }
        if ($request->hasFile('picture_ua')){
            $request['banner_ua'] = $image->moveImage($request,'picture_ua','Service');
        }
        if ($request->hasFile('picture_en')){
            $request['banner_en'] = $image->moveImage($request,'picture_en','Service');
        }

        $service = Service::create($request->all());
        self::syncProducts($service, $request->get('products_in'));

        return redirect()->route('admin.services.index')->with('success','Услуга создана!');
    }

    public function edit(Service $service)
    {
        abort_unless(\Gate::allows('services_edit'), 403);
        $categories = Category::with('children')->orderBy('sort','asc')->get();
        $tariffs = Tariff::active()->orderBy('tariff_type', 'asc')->orderBy('price', 'asc')->get();
        return view('admin.services.edit', compact( 'service','categories','tariffs'));
    }

    public function update(UpdateServicesRequest $request, Service $service)
    {
        abort_unless(\Gate::allows('services_edit'), 403);

        if($request->get('del-pic_ru') == 1){
            $request['banner_ru'] = null;
        }
        if($request->get('del-pic_ua') == 1){
            $request['banner_ua'] = null;
        }
        if($request->get('del-pic_en') == 1){
            $request['banner_en'] = null;
        }

        $image = new ImageService();
        if ($request->hasFile('picture_ru')){
            $request['banner_ru'] = $image->moveImage($request,'picture_ru','Service');
        }
        if ($request->hasFile('picture_ua')){
            $request['banner_ua'] = $image->moveImage($request,'picture_ua','Service');
        }
        if ($request->hasFile('picture_en')){
            $request['banner_en'] = $image->moveImage($request,'picture_en','Service');
        }

        $service->update($request->all());
        self::syncProducts($service, $request->get('products_in'));

        return redirect()->route('admin.services.index')->with('success','Услуга обновлена!');
    }


    public function destroy(News $news)
    {
        abort_unless(\Gate::allows('services_delete'), 403);
        $service = new NewsService();
        $service->deleteItem($news);
        return response()->json(['success'=>true, 'id' => $news->id]);
    }

    public function getProds(Request $request)
    {
        if($request->get('id') != ''){
            $options = '';
            $res = Product::visible()->where('category_id',$request->get('id'))->orderBy('title_ru','asc')->get();
            if(!empty($res)){
                $options .= '<option value="0"> - выберите товар - </option>';
                foreach($res as $r){
                    $options .= '<option value="'.$r->id.'">'.$r->title_ru.' ('.$r->price.'грн) [ID:'.$r->id.']</option>';
                }
                return response()->json(['success' => true, 'options' => $options]);
            }
        }
        return response()->json(['success' => false]);
    }

    public function addProd(Request $request)
    {
        if($request->get('prodId') > 0){
            $product = Product::find($request->get('prodId'));
            if($product){
                $arr = explode('|',$request->get('ids'));
                if(!in_array($product->id, $arr)){
                    $arr[] = $product->id;

                    $html = view('admin.services.product-item',compact('product'))->render();
                    return response()->json(['success' => true, 'ids' => implode('|',$arr), 'html' => $html, 'id' => $product->id]);
                }
            }
        }
        return response()->json(['success' => false]);
    }

    public function removeProd(Request $request)
    {
        if($request->get('prodId') > 0){
            $arr = explode('|',$request->get('ids'));
            foreach($arr as $k => $v){
                if($v == $request->get('prodId')){
                    unset($arr[$k]);
                }
            }
            return response()->json(['success' => true, 'ids' => implode('|',$arr), 'id' => $request->get('prodId')]);
        }
        return response()->json(['success' => false]);
    }

    private function syncProducts($service, $products_in)
    {
        ServiceProduct::where('service_id',$service->id)->delete();
        $ids = explode('|',$products_in);
        if(!empty($ids)){
            $sort = 0;
            foreach($ids as $id){
                if($id > 0){
                    $sort++;
                    ServiceProduct::create([
                        'service_id' => $service->id,
                        'product_id' => $id,
                        'sort' => $sort,
                    ]);
                }
            }
        }
    }

    public function productsSaveSort(Request $request)
    {
        $ids = $request->get('serialized');
        $arr = [];
        if(!empty($ids)){
            foreach($ids as $id){
                $arr[] = $id['id'];
            }
            return response()->json(['success' => true, 'ids' => implode('|',$arr)]);
        }
        return response()->json(['success' => false]);
    }


    public function testService($code)
    {
        $page = Service::with('products.category')->where('code',$code)->first();
        if($page){
            $result = [
                'title' => $page->{'title' . getLocaleDBSuf()},
                'banner' => $page->{'banner' . getLocaleDBSuf()},
                'meta_title' => $page->{'meta_title' . getLocaleDBSuf()},
                'meta_description' => $page->{'meta_description' . getLocaleDBSuf()},
                'meta_keywords' => $page->{'meta_keywords' . getLocaleDBSuf()},
            ];

            if(!empty($page->products)){
                $category_id = 0;
                foreach($page->products as $product){
                    if($product->active == 1){
                        if($category_id == 0){
                            $category_id = $product->category_id;
                        }
                        $result['products'][] = [
                            'id' => $product->id,
                            'title' => $product->{'title' . getLocaleDBSuf()},
                            'url' => '/'.$product->category->slug.'/'.$product->slug,
                            'anons' => $product->{'anons' . getLocaleDBSuf()},
                            'price' => $product->price,
                            'picture' => $product->picture,
                        ];
                    }
                }

                if($category_id > 0){
                    $category = Category::find($category_id);
                    $result['category_title'] = $category->{'uslugy_title' . getLocaleDBSuf()};
                    $result['category_description'] = $category->{'uslugy_description' . getLocaleDBSuf()};
                }
            }

            $tariffService = new TariffService('internet');
            $result['services'] = $tariffService->getItemsForFront('home');

            echo '<pre>';
            print_r($result);
            echo '</pre>';


        }


    }
}
