<?php

namespace App\Http\Controllers\Admin;

use App\Genre;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Log;
use App\News;
use App\Services\ChannelsService;
use App\Services\ImageService;
use App\Services\TariffService;
use App\Tag;
use App\Tariff;
use Carbon\Carbon;
use Faker\Provider\File;
use Illuminate\Http\Request;
use App\Services\LogService;
use DB;

class GenresController extends Controller
{

    public function index()
    {
        abort_unless(\Gate::allows('tariffs_access'), 403);

        $service = new ChannelsService();
        $genres = $service->getGenresForAdmin();

        return view('admin.genres.index', compact('genres'));
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('tariffs_edit'), 403);
        $genre = Genre::find($id);
        return view('admin.genres.edit', compact( 'genre'));
    }

    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('tariffs_edit'), 403);
        $genre = Genre::find($id);
        $data = [
            'title_ru' => $request->get('title_ru'),
            'title_ua' => $request->get('title_ua'),
            'title_en' => $request->get('title_en'),
            'sort' => $request->get('sort'),
        ];
        $genre->update($data);
        return redirect()->route('admin.genres.index')->with('success','Жанр обновлен!');
    }


}
