<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\CategoryProperty;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyProductRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Product;
use App\ProductComment;
use App\ProductImage;
use App\Services\ProductCommentService;
use App\Services\ProductService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Images;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\File as File;

class ProductsCommentsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('product_access'), 403);
        if($request->get('reset') == 'y'){
            session(['filtersComments' => []]);
        }
        $service = new ProductCommentService();
        $emails = $service->getEmails();
        $result = $service->getItemsForAdmin($request->all());
        $comments = $result['items'];
        $filter = $result['filter'];
        $sort = $result['sort'];

        $products = DB::table('product_comments as pc')
            ->select('pc.product_id','p.title_ru')
            ->leftJoin('products as p','p.id','=','pc.product_id')
            ->where('p.id','>',0)
            ->groupBy('pc.product_id')
            ->orderBy('p.title_ru','asc')
            ->get();

        return view('admin.products.comments.index', compact('comments','filter','sort','products','emails'));
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('product_access'), 403);
        $comment = ProductComment::with('parent','product')->where('id',$id)->first();
        if(!$comment){
            return redirect('/admin/products-comments');
        }

        return view('admin.products.comments.detail', compact('comment'));
    }

    public function save(Request $request, $id)
    {
        abort_unless(\Gate::allows('product_access'), 403);
        $comment = ProductComment::where('id',$id)->first();
        if(!$comment){
            return redirect('/admin/products-comments');
        }
        $moderate = ($request->get('active') == 1) ? Carbon::now() : null;
        $comment->update([
            'name' => $request->get('name'),
            'rate' => $request->get('rate'),
            'comment' => $request->get('comment'),
            'moderated_at' => $moderate,
        ]);

        $rate_summ = ProductComment::where('product_id',$comment->product_id)->whereNull('parent_id')->sum('rate');
        $rate_count = ProductComment::where('product_id',$comment->product_id)->whereNull('parent_id')->count();
        if($rate_count > 0){
            $product_rate = round(($rate_summ / $rate_count),2);
        } else {
            $product_rate = null;
        }
        Product::where('id',$comment->product_id)->update(['rating' => $product_rate]);

        return redirect('/admin/products-comments')->with('success','Отзыв обновлен!');
    }

    public function delete($id)
    {
        abort_unless(\Gate::allows('product_access'), 403);
        $comment = ProductComment::where('id',$id)->first();
        if(!$comment){
            return redirect('/admin/products-comments');
        }
        ProductComment::where('parent_id',$comment->id)->delete();
        $comment->delete();

        return redirect('/admin/products-comments')->with('success','Отзыв удален!');
    }

    public function emailAdd(Request $request)
    {
        if($request->get('email') != ''){
            $service = new ProductCommentService();
            $service->saveEmail($request->get('email'));
            return redirect('/admin/products-comments')->with('success','E-mail добавлен!');
        }
        return redirect('/admin/products-comments');
    }

    public function emailDelete($id)
    {
        $service = new ProductCommentService();
        $service->deleteEmail($id);
        return redirect('/admin/products-comments')->with('success','E-mail удален!');
    }
}
