<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use App\BlogCategory;
use App\BlogCategoryIn;
use App\BlogComment;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlogCategoryRequest;
use App\Http\Requests\UpdateBlogCategoryRequest;
use App\Services\BlogService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BlogCommentsController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new BlogService();
    }

    public function index(Request $request)
    {
        abort_unless(\Gate::allows('news_access'), 403);
        $blogs = Blog::select('id','title_ru')->whereNull('deleted_by')->where('is_active',true)->orderBy('title_ru','asc')->get();
        $result = $this->service->getCommentsForAdmin($request->all());
        $comments = (!empty($result['comments'])) ? $result['comments'] : [];
        $filter = (!empty($result['filter'])) ? $result['filter'] : [];
        return view('admin.blogcomments.index', compact('comments','blogs','filter'));
    }

    public function destroy($id)
    {
        BlogComment::where('parent_id',$id)->delete();
        BlogComment::where('id',$id)->delete();
        return response()->json(['success' => true, 'id' => $id]);
    }

    public function moderate(Request $request, $id)
    {
        $comment = BlogComment::where('id',$id)->first();
        if($comment){
            if($request->get('moder') == 'y'){
                $comment->update(['moderated_at' => Carbon::now()]);

            } else {
                $comment->update(['moderated_at' => null]);
            }
        }
        return response()->json(['success' => true, 'id' => $id, 'moder' => $request->get('moder')]);
    }

    public function edit($id)
    {
        $comment = BlogComment::with('blog')->where('id',$id)->first();
        if(!$comment){
            return redirect('/admin/blogs/comments');
        }
        return view('admin.blogcomments.edit', compact('comment'));
    }

    public function update(Request $request, $id)
    {
        $comment = BlogComment::where('id',$id)->first();
        if($comment){
            $request['moderated_at'] = ($request->get('is_active') == 1) ? Carbon::now() : null;
            $comment->update($request->all());
        }
        return redirect('/admin/blogs/comments')->with('success','Комментарий сохранен!');
    }

}
