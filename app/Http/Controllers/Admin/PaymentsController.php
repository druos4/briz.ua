<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePaymentsRequest;
use App\Http\Requests\UpdatePaymentsRequest;
use App\Payment;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('payments_access'), 403);

        $payments = Payment::orderBy('sort','asc')->get();

        return view('admin.payments.index', compact('payments'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('payments_create'), 403);

        return view('admin.payments.create');
    }

    public function store(StorePaymentsRequest $request)
    {
        abort_unless(\Gate::allows('payments_create'), 403);
        $request['active'] = ($request->get('active') == 1) ? true : false;
        $request['commission'] = ($request->get('commission') == 1) ? true : false;
        if ($request->hasFile('image'))
        {
            $f_name = date('YmdHis').$request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(public_path('uploads/payments/'),$f_name);
            $request['icon'] = '/uploads/payments/'.$f_name;
        }
        if ($request->hasFile('image_active'))
        {
            $f_name = date('YmdHis').$request->file('image_active')->getClientOriginalName();
            $file = $request->file('image_active');
            $file->move(public_path('uploads/payments/'),$f_name);
            $request['icon_active'] = '/uploads/payments/'.$f_name;
        }
        Payment::create($request->all());
        return redirect('/admin/payments')->with('success','Метод оплаты создан!');
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('payment_method_edit'), 403);
        $payment = Payment::find($id);
        return view('admin.payments.edit', compact('payment'));
    }

    public function update(UpdatePaymentsRequest $request, Payment $payment)
    {
        abort_unless(\Gate::allows('payments_edit'), 403);
        $request['active'] = ($request->get('active') == 1) ? true : false;
        $request['commission'] = ($request->get('commission') == 1) ? true : false;
        if($request->get('del_pic') == 1){
            $request['icon'] = '';
        }
        if($request->get('del_pic_active') == 1){
            $request['icon_active'] = '';
        }

        if ($request->hasFile('image'))
        {
            $f_name = date('YmdHis').$request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(public_path('uploads/payments/'),$f_name);
            $request['icon'] = '/uploads/payments/'.$f_name;
        }
        if ($request->hasFile('image_active'))
        {
            $f_name = date('YmdHis').$request->file('image_active')->getClientOriginalName();
            $file = $request->file('image_active');
            $file->move(public_path('uploads/payments/'),$f_name);
            $request['icon_active'] = '/uploads/payments/'.$f_name;
        }

        $payment->update($request->all());

        return redirect('/admin/payments')->with('success','Метод оплаты обновлен!');
    }

    public function destroy(Payment $payment)
    {
        abort_unless(\Gate::allows('payments_delete'), 403);
        $payment->delete();
        return response()->json(['success' => true, 'id' => $payment->id]);
    }

}
