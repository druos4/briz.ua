<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use App\Jobs\MakeImagesSitemapJob;
use App\Services\SiteMapService;
use App\Slider;
use Illuminate\Support\Str;
use DB;

class HomeController
{
    public function __construct()
    {
        //abort_unless(\Gate::allows('news_access'), 403);
    }

    public function index()
    {
        $jobs = [];
        $res = DB::table('jobs')->get();
        if(!empty($res)){
            foreach($res as $r){
                $jobObj = json_decode($r->payload);
                if(strpos($jobObj->displayName,'SyncChannelsJob') !== false || strpos($jobObj->displayName,'ParseChannel') !== false){
                    if(!in_array('SyncChannelsJob',$jobs)){
                        $jobs[] = 'SyncChannelsJob';
                    }
                } elseif(strpos($jobObj->displayName,'MakeImagesSitemapJob') !== false){
                    if(!in_array('MakeImagesSitemap',$jobs)){
                        $jobs[] = 'MakeImagesSitemap';
                    }
                }
            }
        }

        return view('admin.dashboard', compact('jobs'));
    }

    public function filemanager()
    {
        return view('admin.filemanager');
    }

    public function test()
    {
        return view('admin.test');
    }

    public function clearCache()
    {
        \Cache::flush();
        return redirect('/admin')->with('success', 'Кеш сброшен!');
    }

    public function sitemap()
    {
        $file = public_path('sitemap.xml');
        $txt = '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<sitemap>
    <loc>https://www.briz.ua/sitemap-ru.xml</loc>
</sitemap>
<sitemap>
    <loc>https://www.briz.ua/sitemap-ua.xml</loc>
</sitemap>
<sitemap>
    <loc>https://www.briz.ua/sitemap-en.xml</loc>
</sitemap>
</sitemapindex>';
        file_put_contents($file, $txt);

        $service = new SiteMapService('ua');
        $service->generateMap();

        $service = new SiteMapService('ru');
        $service->generateMap();

        $service = new SiteMapService('en');
        $service->generateMap();

        return redirect('/admin')->with('success','Sitemap обновлен!');
    }

    public function imgSitemap()
    {
        abort_unless(\Gate::allows('tariffs_access'), 403);
        dispatch(new MakeImagesSitemapJob());
        return redirect('/admin')->with('success','Задача поставлена в очередь!');
    }
}
