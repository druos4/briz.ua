<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Price;
use Illuminate\Http\Request;

class PricesController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('tag_access'), 403);

        $prices = Price::orderBy('sort','asc')->paginate(30);

        return view('admin.prices.index', compact('prices'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('tag_create'), 403);

        return view('admin.prices.create');
    }

    public function store(StoreDocumentRequest $request)
    {
        abort_unless(\Gate::allows('tag_create'), 403);
        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['link'] = '';
        $request['active'] = ($request['active'] == 1) ? true : false;
        $price = Price::create($request->all());

        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/prices"))) {
                \File::makeDirectory(public_path("uploads/prices"));
            }
            $f_name = 'price-'.$price->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/prices/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $price->link = '/uploads/prices/'.$f_name;
            $price->save();
        }

        return redirect()->route('admin.prices.index')->with('success','Прайс создан!');
    }

    public function edit(Price $price)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        return view('admin.prices.edit', compact( 'price'));
    }

    public function update(UpdateDocumentRequest $request, Price $price)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['active'] = ($request['active'] == 1) ? true : false;
        if($request->get('del-pic') == 1){
            if(file_exists(public_path($price->link))){
                unlink(public_path($price->link));
            }
            $request['link'] = '';
        }
        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/prices"))) {
                \File::makeDirectory(public_path("uploads/prices"));
            }
            $f_name = 'price-'.$price->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/prices/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $request['link'] = '/uploads/prices/'.$f_name;
        }

        $price->update($request->all());


        return redirect()->route('admin.prices.index')->with('success','Прайс обновлен!');
    }

    public function show()
    {

    }

    public function destroy($id)
    {
        abort_unless(\Gate::allows('tag_delete'), 403);
        $price = Price::where('id',$id)->first();
        if($price->link != ''){
            if(file_exists(public_path($price->link))){
                unlink(public_path($price->link));
            }
        }
        $price->delete();

        return response()->json(['success'=>true, 'id' => $price->id]);
    }
}
