<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use Illuminate\Http\Request;

class DocumentsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('tag_access'), 403);

        $documents = Document::orderBy('sort','asc')->paginate(30);

        return view('admin.documents.index', compact('documents'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('tag_create'), 403);

        return view('admin.documents.create');
    }

    public function store(StoreDocumentRequest $request)
    {
        abort_unless(\Gate::allows('tag_create'), 403);
        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['link'] = '';
        $request['active'] = ($request['active'] == 1) ? true : false;
        $document = Document::create($request->all());

        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/documents"))) {
                \File::makeDirectory(public_path("uploads/documents"));
            }
            $f_name = 'document-'.$document->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/documents/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $document->link = '/uploads/documents/'.$f_name;
            $document->save();
        }

        return redirect()->route('admin.documents.index')->with('success','Документ создан!');
    }

    public function edit(Document $document)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        return view('admin.documents.edit', compact( 'document'));
    }

    public function update(UpdateDocumentRequest $request, Document $document)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['active'] = ($request['active'] == 1) ? true : false;
        if($request->get('del-pic') == 1){
            if(file_exists(public_path($document->link))){
                unlink(public_path($document->link));
            }
            $request['link'] = '';
        }
        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/documents"))) {
                \File::makeDirectory(public_path("uploads/documents"));
            }
            $f_name = 'document-'.$document->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/documents/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $request['link'] = '/uploads/documents/'.$f_name;
        }

        $document->update($request->all());


        return redirect()->route('admin.documents.index')->with('success','Документ обновлен!');
    }

    public function show()
    {

    }

    public function destroy($id)
    {
        abort_unless(\Gate::allows('tag_delete'), 403);
        $document = Document::where('id',$id)->first();
        if($document->link != ''){
            if(file_exists(public_path($document->link))){
                unlink(public_path($document->link));
            }
        }
        $document->delete();

        return response()->json(['success'=>true, 'id' => $document->id]);
    }
}
