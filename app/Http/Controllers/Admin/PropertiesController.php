<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyRoleRequest;
use App\Http\Requests\StorePropertyRequest;
use App\Http\Requests\StorePropertyValueRequest;
use App\Property;
use App\PropertyValues;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PropertiesController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('properties_access'), 403);

        $filter = [];

        $properties = Property::with('categories')->orderBy('sort','asc')->orderBy('title_ru','asc')->get();

        $categories = Category::where('parent_id', 0)
            ->with('children.children')->orderBy('sort')->get();

        return view('admin.properties.index', compact('properties','categories','filter'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('properties_create'), 403);
        return view('admin.properties.create');
    }

    public function store(StorePropertyRequest $request)
    {
        abort_unless(\Gate::allows('properties_create'), 403);
        $request['active'] = (!empty($request->get('active'))) ? $request->get('active') : 0;
        $request['multiple'] = (!empty($request->get('multiple'))) ? $request->get('multiple') : 0;
        $request['sort'] = (!empty($request->get('sort'))) ? $request->get('sort') : 100;
        $property = Property::create($request->all());

        return redirect('/admin/properties/'.$property->id.'/edit')->with('success','Свойство сохранено!');
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('properties_edit'), 403);
        $property = Property::find($id);
        if(!$property){
            return redirect('/admin/properties')->with('error','Свойство не найдено!');
        }
        $values = PropertyValues::where('property_id',$id)->orderBy('sort','asc')->orderBy('value_ru','asc')->get();

        return view('admin.properties.edit', compact('property','values'));
    }

    public function update(StorePropertyRequest $request, Property $property)
    {
        abort_unless(\Gate::allows('role_edit'), 403);

        $request['active'] = (!empty($request->get('active'))) ? $request->get('active') : 0;
        $request['multiple'] = (!empty($request->get('multiple'))) ? $request->get('multiple') : 0;
        $request['sort'] = (!empty($request->get('sort'))) ? $request->get('sort') : 100;
        $property->update($request->all());
        return redirect()->route('admin.properties.index')->with('success','Свойство сохранено!');
    }

    public function show(Role $role)
    {
        abort_unless(\Gate::allows('role_show'), 403);

        $role->load('permissions');

        return view('admin.roles.show', compact('role'));
    }

    public function destroy(Role $role)
    {
        abort_unless(\Gate::allows('role_delete'), 403);
        $role->delete();
        return back();
    }

    public function delete(Request $request)
    {
        abort_unless(\Gate::allows('properties_delete'), 403);
        DB::table('property_values')
            ->where('property_id','=',$request->get('id'))
            ->delete();
        DB::table('properties')
            ->where('id','=',$request->get('id'))
            ->delete();

        return response()->json(['success'=>true, 'id' => $request->get('id')]);
    }

    public function massDestroy(MassDestroyRoleRequest $request)
    {
        Role::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }



    public function propertyAddValue(StorePropertyValueRequest $request){
        $input = $request->all();
        $data = ['property_id' => $input['property_id'],
            'value' => $input['value'],
            'sort' => ($input['sort'] > 0) ? $input['sort'] : 100,
            'meta_keyword' => $input['meta_keyword'] ? $input['meta_keyword'] : '',
        ];
        if(isset($input['value_ua']) && trim($input['value_ua']) != ''){
            $data['value_ua'] = trim($input['value_ua']);
        }
        if(isset($input['value_en']) && trim($input['value_en']) != ''){
            $data['value_en'] = trim($input['value_en']);
        }
        $data['slug'] = Controller::createSlug(trim($input['value']));

        if(isset($input['value_id']) && $input['value_id'] > 0){

            DB::table('property_values')
                ->where('id','=',$input['value_id'])
                ->update($data);
            $html = '<tr id="row-'.$input['value_id'].'">
                <td>'.$data['value'].'</td>';
                foreach(getLangs() as $lang) {
                    if($lang->active == 1){
                        $html .= '<td>'.$data['value_'.$lang->code].'
                        </td>';
                    }
                }
                $html .= '<td>'.$data['slug'].'</td>
                <td>'.$data['meta_keyword'].'</td>
                <td>'.$data['sort'].'</td>
                <td><a class="btn btn-warning btn-value-edit" id="upd-prop-'.$input['value_id'].'" data-id="'.$input['value_id'].'"
                                                   data-value="'.$data['value'].'"';
                if(isset($data['value_ua'])){
                    $html .= 'data-value_ua="'.$data['value_ua'].'"';
                }
                if(isset($data['value_en'])){
                    $html .= 'data-value_en="'.$data['value_en'].'"';
                }
                $html .= 'data-slug="'.$data['slug'].'" data-meta_keyword="'.$data['meta_keyword'].'" data-sort="'.$data['sort'].'" href="" role="button" title="Редактировать"><i class="fas fa-pen"></i></a></td>
                <td><a class="btn btn-danger btn-val-del" href="" id="del-prop-'.$input['value_id'].'" data-id="'.$input['value_id'].'" role="button" title="Удалить"><i class="fas fa-trash"></i></a></td>
            </tr>';

            return response()->json(['success'=>true, 'upd' => 1, 'id' => $input['value_id'], 'html' => $html]);

        } else {

            $check = DB::table('property_values')
                ->where('property_id','=',$input['property_id'])
                ->where('value','=',$input['value'])
                ->first();
            if(isset($check->id) && $check->id > 0){

                return response()->json(['success'=>true, 'exist' => 1]);

            } else {

                $item = PropertyValues::create($data);
                $html = '<tr id="row-'.$item->id.'">
                    <td>'.$item->value.'</td>';
                    foreach(getLangs() as $lang) {
                        if($lang->active == 1){
                            $html .= '<td>
                                '.$item->{'value_' . $lang->code}.'
                            </td>';
                        }
                    }
                    $html .= '<td>'.$item->slug.'</td>
                    <td>'.$item->meta_keyword.'</td>
                    <td>'.$item->sort.'</td>
                    <td><a class="btn btn-warning btn-value-edit" id="upd-prop-'.$item->id.'" data-id="'.$item->id.'"
                                                   data-value="'.$item->value.'"';
                    if(isset($item->value_ua)){
                        $html .= 'data-value_ua="'.$item->value_ua.'"';
                    }
                    if(isset($item->value_en)){
                        $html .= 'data-value_en="'.$item->value_en.'"';
                    }
                    $html .= 'data-slug="'.$item->slug.'" data-meta_keyword="'.$item->meta_keyword.'" data-sort="'.$item->sort.'" href="" role="button" title="Редактировать"><i class="fas fa-pen"></i></a></td>
                    <td><a class="btn btn-danger btn-val-del" href="" id="del-prop-'.$item->id.'" data-id="'.$item->id.'" role="button" title="Удалить"><i class="fas fa-trash"></i></a></td>
                </tr>';
                return response()->json(['success'=>true, 'new' => 1, 'html' => $html, 'id' => $item->id]);

            }

        }
    }



    public function propertyDelValue(Request $request){
        DB::table('property_values')->where('id','=',$request->get('id'))->delete();
        return response()->json(['success'=>true, 'id' => $request->get('id')]);
    }


    public function propertyValueSave(Request $request){

        $input = $request->all();

        $data = ['property_id' => $input['prop_id'],
            'value_ru' => $input['value_ru'],
            'sort' => ($input['value_sort'] > 0) ? $input['value_sort'] : 100,
            //'meta_keyword' => $input['meta_keyword'] ? $input['meta_keyword'] : '',
        ];
        if(isset($input['value_ua']) && trim($input['value_ua']) != ''){
            $data['value_ua'] = trim($input['value_ua']);
        }
        if(isset($input['value_en']) && trim($input['value_en']) != ''){
            $data['value_en'] = trim($input['value_en']);
        }

        $data['slug'] = Str::slug(trim($request->get('value_ru')), '-');

        if(isset($input['del-icon']) && $input['del-icon'] == 1){
            $data['icon'] = null;
        }

        if ($request->hasFile('value_icon'))
        {
            $f_name = date('YmdHis').$request->file('value_icon')->getClientOriginalName();
            $file = $request->file('value_icon');
            $file->move(public_path('uploads/icons/'),$f_name);
            $data['icon'] = '/uploads/icons/'.$f_name;
        }

        if(isset($input['value_id']) && $input['value_id'] > 0){

            DB::table('property_values')
                ->where('id','=',$input['value_id'])
                ->update($data);

        } else {

            $check = DB::table('property_values')
                ->where('property_id','=',$input['prop_id'])
                ->where('value_ru','=',$input['value_ru'])
                ->first();
            if(isset($check->id) && $check->id > 0){

                //already exist

            } else {

                $item = PropertyValues::create($data);

            }

        }

        return redirect('/admin/properties/'.$request->get('prop_id').'/edit');
    }
}
