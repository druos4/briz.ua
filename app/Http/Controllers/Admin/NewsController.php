<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\News;
use App\Services\NewsService;
use App\Tag;
use Illuminate\Http\Request;
use App\Services\LogService;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('news_access'), 403);
        if($request->get('reset') == 'y'){
            session(['filtersNews' => []]);
        }
        $service = new NewsService('news');
        $result = $service->getItemsForAdmin($request->all());
        $news = $result['items'];
        $filter = $result['filter'];
        $sort = $result['sort'];
        $tags = Tag::orderBy('title_ua','asc')->get();

        return view('admin.news.index', compact('news','filter','tags','sort'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('news_create'), 403);
        $tags = Tag::orderBy('title_ua','asc')->get();
        $others = News::active()->where('entity_type','news')->orderBy('id','desc')->get();
        return view('admin.news.create',compact('tags','others'));
    }

    public function slug(Request $request)
    {
        $service = new NewsService('news');
        $res = $service->checkSlug($request);
        return response()->json(['success'=>true, 'slug' => $res]);
    }

    public function store(StoreNewsRequest $request)
    {
        abort_unless(\Gate::allows('news_create'), 403);
        $service = new NewsService('news');
        $check = $service->checkSlug($request);
        if($check == false){
            return \Redirect::back()->withErrors(['message', 'Slug должен быть уникальным!']);
        }
        $news = $service->storeItem($request);

        return redirect()->route('admin.news.index')->with('success','Новость создана!');
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('news_edit'), 403);
        $news = News::with('recomends')->find($id);
        $tags = Tag::orderBy('title_ua','asc')->get();
        $tags_in = [];
        if(!empty($news->tags)){
            foreach($news->tags as $tag_in){
                $tags_in[] = $tag_in->id;
            }
        }
        $others_in = [];
        if(!empty($news->recomends)){
            foreach($news->recomends as $recomend){
                $others_in[] = $recomend->id;
            }
        }
        $others = News::active()->where('entity_type',$news->entity_type)->where('id','!=',$news->id)->orderBy('id','desc')->get();

        return view('admin.news.edit', compact( 'news','tags','tags_in','others','others_in'));
    }

    public function update(UpdateNewsRequest $request, $id)
    {
        abort_unless(\Gate::allows('news_edit'), 403);
        $news = News::find($id);
        $service = new NewsService('news');
        $news = $service->updateItem($request,$news);

        return redirect()->route('admin.news.index')->with('success','Новость обновлена!');
    }

    public function show(Page $page)
    {
        abort_unless(\Gate::allows('news_show'), 403);
        return view('admin.news.show', compact('news'));
    }

    public function destroy(News $news)
    {
        abort_unless(\Gate::allows('news_delete'), 403);
        $service = new NewsService('news');
        $service->deleteItem($news);
        return response()->json(['success'=>true, 'id' => $news->id]);
    }

    public function history($id){
        $news = News::find($id);
        if(!$news){
            return redirect()->route('admin.news.index')->with('danger','Элемент не найден!');
        }
        $log = new LogService();
        $logs = $log->getLogs('News',$news);

        return view('admin.news.history', compact('news','logs'));
    }
}
