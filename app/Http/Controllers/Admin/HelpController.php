<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use App\FaqGroup;
use App\HelpArticle;
use App\HelpDirectory;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFaqRequest;
use App\Http\Requests\StoreHelpArticleRequest;
use App\Http\Requests\StoreHelpDirectoryRequest;
use App\Http\Requests\UpdateFaqRequest;
use App\Http\Requests\UpdateHelpArticleRequest;
use App\Http\Requests\UpdateHelpDirectoryRequest;
use App\News;
use App\Services\FaqService;
use App\Tariff;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HelpController extends Controller
{
    public function index(Request $request, $id = null)
    {
        abort_unless(\Gate::allows('help_access'), 403);

        if($id > 0){
            $items = HelpDirectory::select('id','title_ru','title_ua','title_en','icon','parent_id','sort','active','slug','articles_show_type','updated_at')
                ->where('parent_id',$id)->orderBy('sort','asc')->get();
            $parent = HelpDirectory::select('id','title_ru','title_ua','title_en','icon','parent_id','sort','active','slug','articles_show_type','updated_at')
                ->where('id',$id)->first();
        } else {
            $items = HelpDirectory::select('id','title_ru','title_ua','title_en','icon','parent_id','sort','active','slug','articles_show_type','updated_at')
                ->where('parent_id',0)
                ->orderBy('sort','asc')
                ->get();
            $parent = null;
        }

        if(!empty($items)){
            foreach($items as $item){
                $item->children_count = HelpDirectory::where('parent_id',$item->id)->count();
            }
        }

        return view('admin.help.index', compact('items','parent'));
    }

    public function directoryCreate(Request $request)
    {
        $parent_id = ($request->get('parent_id') > 0) ? $request->get('parent_id') : 0;
        $levels = HelpDirectory::select('id','parent_id','title_ru')->where('parent_id', 0)->where('active',1)->orderBy('sort')->get();
        if(!empty($levels)){
            foreach($levels as $level){
                $level->children = HelpDirectory::select('id','parent_id','title_ru')->where('parent_id', $level->id)->where('active',1)->orderBy('sort')->get();
            }
        }
        return view('admin.help.directory-create',compact('levels','parent_id'));
    }

    public function directoryStore(StoreHelpDirectoryRequest $request)
    {
        $request['active'] = (isset($request['active']) && $request['active'] == 1) ? 1 : 0;
        $help = HelpDirectory::create($request->all());
        if ($request->hasFile('image'))
        {
            if (! \File::exists(public_path("uploads/help"))) {
                \File::makeDirectory(public_path("uploads/help"));
            }
            $f_name = 'help-'.$help->id.$request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(public_path('uploads/help/'),$f_name);
            $help->icon = '/uploads/help/'.$f_name;
            $help->save();
        }

        if($help->parent_id > 0){
            $url = '/admin/help/'.$help->parent_id.'/children';
        } else {
            $url = '/admin/help';
        }
        return redirect($url)->with('success', 'Элемент создан!');
    }

    public function directoryEdit($id)
    {
        $helpDirectory = HelpDirectory::find($id);
        $levels = HelpDirectory::select('id','parent_id','title_ru')->where('parent_id', 0)->where('active',1)->orderBy('sort')->get();
        if(!empty($levels)){
            foreach($levels as $level){
                $level->children = HelpDirectory::select('id','parent_id','title_ru')->where('parent_id', $level->id)->where('active',1)->orderBy('sort')->get();
            }
        }

        return view('admin.help.directory-edit', compact('helpDirectory','levels'));
    }

    public function directoryUpdate(UpdateHelpDirectoryRequest $request, $id)
    {
        $helpDirectory = HelpDirectory::find($id);
        $request['active'] = (isset($request['active']) && $request['active'] == 1) ? 1 : 0;
        $helpDirectory->update($request->all());

        if($request->get('del_pic') == 1){
            if (\File::exists(public_path($helpDirectory->icon))) {
                unlink(public_path($helpDirectory->icon));
            }
            $helpDirectory->update([
                'icon' => null,
            ]);
        }

        if ($request->hasFile('image'))
        {
            if (! \File::exists(public_path("uploads/help"))) {
                \File::makeDirectory(public_path("uploads/help"));
            }
            $f_name = 'help-'.$helpDirectory->id.$request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(public_path('uploads/help/'),$f_name);
            $helpDirectory->icon = '/uploads/help/'.$f_name;
            $helpDirectory->save();
        }

        return redirect('/admin/help')->with('success', 'Элемент сохранен!');
    }

    public function directoryDestroy($id)
    {
        $helpDirectory = HelpDirectory::find($id);
        $children = HelpDirectory::where('parent_id',$helpDirectory->id)->get();
        if(!empty($children)){
            foreach($children as $child){
                $childs = HelpDirectory::where('parent_id',$child->id)->get();
                if(!empty($childs)){
                    foreach($childs as $ch){
                        $ch->delete();
                    }
                }
                $child->delete();
            }
        }
        $helpDirectory->delete();
        return response()->json(['success'=>true, 'id' => $helpDirectory->id]);
    }

    public function articles(Request $request, $id)
    {
        $parent = HelpDirectory::find($id);
        $articles = HelpArticle::where('help_directory_id',$parent->id)->orderBy('sort','asc')->get();
        return view('admin.help.articles', compact('parent','articles'));
    }

    public function articleCreate($id)
    {
        $parent = HelpDirectory::find($id);
        return view('admin.help.articles-create', compact('parent'));
    }

    public function articleStore(StoreHelpArticleRequest $request, $id)
    {
        $parent = HelpDirectory::find($id);
        if($parent){
            $request['active'] = (isset($request['active']) && $request['active'] == 1) ? 1 : 0;
            $request['help_directory_id'] = $parent->id;
            $article = HelpArticle::create($request->all());
            if ($request->hasFile('image'))
            {
                if (! \File::exists(public_path("uploads/help"))) {
                    \File::makeDirectory(public_path("uploads/help"));
                }
                $f_name = 'help-article-'.$article->id.$request->file('image')->getClientOriginalName();
                $file = $request->file('image');
                $file->move(public_path('uploads/help/'),$f_name);
                $article->icon = '/uploads/help/'.$f_name;
                $article->save();
            }
        }
        return redirect('/admin/help/'.$parent->id.'/articles')->with('success', 'Статья создана!');
    }

    public function articleEdit($id, $article_id)
    {
        $parent = HelpDirectory::find($id);
        $article = HelpArticle::find($article_id);
        return view('admin.help.articles-edit', compact('parent','article'));
    }

    public function articleUpdate(UpdateHelpArticleRequest $request, $id, $article_id)
    {
        $parent = HelpDirectory::find($id);
        $article = HelpArticle::find($article_id);
        $request['active'] = ($request->get('active') == 1) ? 1 : 0;
        $article->update($request->all());

        if($request->get('del_pic') == 1){
            if ($article->icon != '' && \File::exists(public_path($article->icon))) {
                unlink(public_path($article->icon));
            }
            $article->update([
                'icon' => null,
            ]);
        }

        if ($request->hasFile('image'))
        {
            if (! \File::exists(public_path("uploads/help"))) {
                \File::makeDirectory(public_path("uploads/help"));
            }
            if ($article->icon != '' && \File::exists(public_path($article->icon))) {
                unlink(public_path($article->icon));
            }
            $f_name = 'help-article-'.$article->id.$request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(public_path('uploads/help/'),$f_name);
            $article->icon = '/uploads/help/'.$f_name;
            $article->save();
        }

        return redirect('/admin/help/'.$parent->id.'/articles')->with('success', 'Статья сохранена!');
    }

    public function articleDestroy($id, $article_id)
    {
        $article = HelpArticle::where('id',$article_id)->where('help_directory_id',$id)->first();
        if($article){
            if ($article->icon != '' && \File::exists(public_path($article->icon))) {
                unlink(public_path($article->icon));
            }
            $article->delete();
        }
        return response()->json(['success'=>true, 'id' => $article->id]);
    }

}
