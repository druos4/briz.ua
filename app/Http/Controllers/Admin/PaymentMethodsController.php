<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePaymentMethodRequest;
use App\Http\Requests\UpdatePaymentMethodRequest;
use App\PaymentMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PaymentMethodsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('payment_method_access'), 403);

        $payments = PaymentMethod::get();

        return view('admin.payment-methods.index', compact('payments'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('payment_method_create'), 403);

        return view('admin.payment-methods.create');
    }

    public function store(StorePaymentMethodRequest $request)
    {
        abort_unless(\Gate::allows('payment_method_create'), 403);
        if($request->get('active') == 1){
            //ok
        } else {
            $request['active'] = 0;
        }

        if ($request->hasFile('image'))
        {
            $f_name = date('YmdHis').$request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(public_path('uploads/'),$f_name);
            $request['picture'] = '/uploads/'.$f_name;
        }

        $payment = PaymentMethod::create($request->all());
        return redirect('/admin/payment-methods')->with('success','Метод оплаты создан!');
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('payment_method_edit'), 403);
        $paymentMethod = PaymentMethod::find($id);
        return view('admin.payment-methods.edit', compact('paymentMethod'));
    }

    public function update(UpdatePaymentMethodRequest $request, PaymentMethod $paymentMethod)
    {
        abort_unless(\Gate::allows('news_edit'), 403);
        if($request->get('active') == 1){
            //ok
        } else {
            $request['active'] = 0;
        }
        if($request->get('del_pic') == 1){
            $request['picture'] = '';
        }

        if ($request->hasFile('image'))
        {
            $f_name = date('YmdHis').$request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(public_path('uploads/'),$f_name);
            $request['picture'] = '/uploads/'.$f_name;
        }

        $paymentMethod->update($request->all());

        return redirect('/admin/payment-methods')->with('success','Метод оплаты обновлен!');
    }

    public function show(News $news)
    {
        /*abort_unless(\Gate::allows('product_show'), 403);

        return view('admin.products.show', compact('product'));*/
    }

    public function delete(Request $request){
        if($request->get('id') > 0){
            DB::table('payment_methods')
                ->where('id','=',$request->get('id'))
                ->delete();
        }

        return response()->json(['success'=>true, 'id' => $request->get('id')]);
    }



    public function destroy(Product $product)
    {
       /* abort_unless(\Gate::allows('product_delete'), 403);

        $product->delete();

        return back();*/
    }

    public function massDestroy(MassDestroyProductRequest $request)
    {
     /*   Product::whereIn('id', request('ids'))->delete();

        return response(null, 204);*/
    }




}
