<?php

namespace App\Http\Controllers\Admin;

use App\DeletedUrl;
use App\Email;
use App\Http\Controllers\Controller;

class DeletedUrlsController extends Controller
{
    public function index()
    {
        $urls = DeletedUrl::orderBy('updated_at','desc')->paginate(50);
        return view('admin.deleted-urls.index', compact('urls'));
    }

}
