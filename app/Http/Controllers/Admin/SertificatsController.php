<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSertificatRequest;
use App\Http\Requests\UpdateSertificatRequest;
use App\Sertificat;
use Illuminate\Http\Request;

class SertificatsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('tag_access'), 403);

        $sertificats = Sertificat::orderBy('sort','asc')->paginate(30);

        return view('admin.sertificats.index', compact('sertificats'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('tag_create'), 403);

        return view('admin.sertificats.create');
    }

    public function store(StoreSertificatRequest $request)
    {
        abort_unless(\Gate::allows('tag_create'), 403);
        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['link'] = '';
        $request['active'] = ($request['active'] == 1) ? true : false;
        $sertificat = Sertificat::create($request->all());

        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/sertificats"))) {
                \File::makeDirectory(public_path("uploads/sertificats"));
            }
            $f_name = 'document-'.$sertificat->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/sertificats/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $sertificat->link = '/uploads/sertificats/'.$f_name;
            $sertificat->save();
        }

        return redirect()->route('admin.sertificats.index')->with('success','Сертификат создан!');
    }

    public function edit(Sertificat $sertificat)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        return view('admin.sertificats.edit', compact( 'sertificat'));
    }

    public function update(UpdateSertificatRequest $request, Sertificat $sertificat)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['active'] = ($request['active'] == 1) ? true : false;
        if($request->get('del-pic') == 1){
            if(file_exists(public_path($sertificat->link))){
                unlink(public_path($sertificat->link));
            }
            $request['link'] = '';
        }
        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/sertificats"))) {
                \File::makeDirectory(public_path("uploads/sertificats"));
            }
            $f_name = 'document-'.$sertificat->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/sertificats/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $request['link'] = '/uploads/sertificats/'.$f_name;
        }

        $sertificat->update($request->all());


        return redirect()->route('admin.sertificats.index')->with('success','Сертификат обновлен!');
    }

    public function show()
    {

    }

    public function destroy(Sertificat $sertificat)
    {
        abort_unless(\Gate::allows('tag_delete'), 403);

        if($sertificat->link != ''){
            if(file_exists(public_path($sertificat->link))){
                unlink(public_path($sertificat->link));
            }
        }
        $sertificat->delete();

        return response()->json(['success'=>true, 'id' => $sertificat->id]);
    }
}
