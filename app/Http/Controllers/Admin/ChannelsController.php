<?php

namespace App\Http\Controllers\Admin;

use App\Channel;
use App\Genre;
use App\Http\Controllers\Controller;
use App\Jobs\AfterParseChannels;
use App\Jobs\BeforeParseChannels;
use App\Jobs\ParseChannelUrl;
use App\Jobs\SyncChannelsJob;
use App\Services\ChannelsService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Artisan;

class ChannelsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('tariffs_access'), 403);
        if($request->get('reset') == 'y'){
            session(['filtersChannels' => []]);
        }
        $service = new ChannelsService();
        $result = $service->getChannelsForAdmin($request->all());

        $filter = $result['filter'];
        $sort = $result['sort'];
        $items = $result['items'];
        $tariffs = $service->getTariffs();
        $genres = Genre::orderBy('title_ru','asc')->orderBy('code','asc')->get();

        return view('admin.channels.index', compact('items','filter','sort','tariffs','genres'));
    }

    public function sync()
    {
        abort_unless(\Gate::allows('tariffs_access'), 403);
        dispatch(new SyncChannelsJob());
        return redirect('/admin')->with('success','Задача поставлена в очередь!');
    }

    public function getGenres()
    {
        $html = '<option value="0"> - выберите жанр - </option>';
        $genres = Genre::orderBy('title_ru','asc')->get();
        if(!empty($genres)){
            foreach($genres as $genre){
                $html .= '<option value="'.$genre->id.'">'.$genre->title_ru.' ('.$genre->code.')</option>';
            }
        }
        return response()->json(['success' => true, 'html' => $html]);
    }

    public function update(Request $request)
    {
        if($request->get('channel_id') != '' && $request->get('genre_id') != ''){
            if($request->get('genre_id') > 0){

                $exist = DB::table('channels_genres')->select()->where('channel_id','=',$request->get('channel_id'))->first();
                if(!empty($exist->id)){
                    DB::table('channels_genres')->where('id','=',$exist->id)->update([
                        'genre_id' => $request->get('genre_id'),
                        'updated_at' => Carbon::now(),
                    ]);
                } else {
                    DB::table('channels_genres')->insert([
                        'channel_id' => $request->get('channel_id'),
                        'genre_id' => $request->get('genre_id'),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]);
                }
                $genre = Genre::where('id',$request->get('genre_id'))->first();
                return response()->json(['success' => true, 'update' => true, 'id' => $request->get('channel_id'), 'title' => $genre->title_ru.' ('.$genre->code.')']);
            } elseif($request->get('genre_id') == 0){
                DB::table('channels_genres')->where('channel_id','=',$request->get('channel_id'))->delete();
                return response()->json(['success' => true, 'delete' => true, 'id' => $request->get('channel_id')]);
            }
        }
        return response()->json(['success' => false]);
    }

    public function clear()
    {
        DB::table('channels_genres')->where('id','>',0)->delete();
        DB::table('channels_tariffs')->where('id','>',0)->delete();
        Channel::where('id','>',0)->delete();
        return redirect()->route('admin.channels.index')->with('success','Каналы удалены!');
    }

}
