<?php

namespace App\Http\Controllers\Admin;

use App\Email;
use App\Http\Controllers\Controller;

class EmailsController extends Controller
{
    public function index()
    {
        $filter = request()->all();

        $query = Email::query();
        $query->when(request()->filled('email'), function ($subQuery) {
            return $subQuery->where('email','LIKE','%'.request()->get('email').'%');
        });
        if(!empty($filter['date-from']) && !empty($filter['date-to']) && $filter['date-to'] >= $filter['date-from']){
            $query->where('created_at','>=',$filter['date-from'].' 00:00:00')
                ->where('created_at','<=',$filter['date-to'].' 23:59:59');
        }
        $emails = $query->orderBy('created_at','desc')
            ->paginate(30);

        return view('admin.emails.index', compact('emails','filter'));
    }

}
