<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\ShipmentMethod;
use App\PaymentMethod;
use App\Order;
use App\OrderStatuses;
use App\Category;
use App\Cart;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class OrdersController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('orders_access'), 403);
        $input = $request->all();
        $filter = [];
        if(isset($input['id']) && $input['id'] > 0){
            $filter['id'] = $input['id'];
        }
        if(isset($input['status']) && $input['status'] != '' && $input['status'] != '0'){
            $filter['status'] = $input['status'];
        }
        if(isset($input['payment']) && $input['payment'] > 0){
            $filter['payment'] = $input['payment'];
        }
        if(isset($input['shipment']) && $input['shipment'] > 0){
            $filter['shipment'] = $input['shipment'];
        }
        if(isset($input['client']) && $input['client'] != '' && $input['client'] != '0'){
            $filter['client'] = $input['client'];
        }
        if(isset($input['from']) && $input['from'] != ''){
            $filter['from'] = $input['from'];
        }
        if(isset($input['to']) && $input['to'] != ''){
            $filter['to'] = $input['to'];
        }

        if(count($filter) == 0){
            $orders = Order::with('payment','shipment','status','client','manager')
                ->orderBy('id','desc')
                ->paginate(30);
        } else {
            $query = Order::with('payment','shipment','status','client','manager');
            if(isset($filter['id'])){
                $query->where('id',$filter['id']);
            }
            if(isset($filter['status'])){
                $query->where('status_id',$filter['status']);
            }
            if(isset($filter['payment'])){
                $query->where('payment_id',$filter['payment']);
            }
            if(isset($filter['shipment'])){
                $query->where('shipment_id',$filter['shipment']);
            }
            if(isset($filter['from']) && isset($filter['to'])){
                $query->where('created_at','>=',$filter['from'].' 00:00:00')->where('created_at','<=',$filter['to'].' 23:59:59');
            }
            if(isset($filter['client'])) {
                $query->where(function ($query2) use ($filter) {
                    $query2->where('email', '=', $filter['client'])
                        ->orWhere('first_name', '=', $filter['client'])
                        ->orWhere('last_name', '=', $filter['client']);
                });
            }

                $orders = $query->orderBy('id','desc')
                ->paginate(30);
        }


        $payments = PaymentMethod::visible()->get();
        $shipments = ShipmentMethod::visible()->get();
        $statuses = OrderStatuses::visible()->get();
        $managers = ['manager 1','manager 2','manager 3'];


        return view('admin.orders.index', compact('orders','payments','shipments','statuses','filter','managers'));
    }


    public function detail($id){
        abort_unless(\Gate::allows('orders_access'), 403);

        $order = Order::where('id',$id)->with('payment','shipment','status','client','manager')->first();
        if(!$order){
            return redirect('/admin/orders');
        }

        $cart = Cart::getBigCart($id);

        $payments = PaymentMethod::visible()->get();
        $shipments = ShipmentMethod::visible()->get();
        $statuses = OrderStatuses::visible()->get();
        return view('admin.orders.detail', compact('order','payments','shipments','statuses','cart'));
    }


    public function edit($id){
        abort_unless(\Gate::allows('orders_access'), 403);

        $order = Order::where('id',$id)->with('payment','shipment','status','client','manager','carts')->first();
        if(!$order){
            return redirect('/admin/orders');
        }

        $payments = PaymentMethod::visible()->get();
        $shipments = ShipmentMethod::visible()->get();
        $statuses = OrderStatuses::visible()->get();
        $categories = Category::where('parent_id', 0)->where('active',1)
            ->with('children.children')->orderBy('sort')->get();

        $cart = Cart::getBigCart($id);

        return view('admin.orders.edit', compact('order','payments','shipments','statuses','categories','cart'));
    }


    public function statusSave(Request $request, $id){
        abort_unless(\Gate::allows('orders_access'), 403);
        $row = DB::table('orders')->where('id','=',$id)->first();
        if(isset($row->manager_id) && $row->manager_id > 0){
            DB::table('orders')->where('id','=',$id)->update(['updated_at' => Carbon::now(),
                'status_id' => $request->get('status')]);
        } else {
            DB::table('orders')->where('id','=',$id)->update(['updated_at' => Carbon::now(),
                'status_id' => $request->get('status'),
                'manager_id' => auth()->id()]);
        }
        return redirect('/admin/orders/'.$id)->with('success','Статус обновлен!');
    }



    public function statuses(){
        $statuses = OrderStatuses::get();
        return view('admin.orders.statuses', compact('statuses'));
    }



    public function statusesSave(Request $request){
        $input = $request->all();
        DB::table('order_statuses')->where('id','>',0)->update(['active' => 0, 'goto' => '']);
        foreach($input['active'] as $k => $v){
            if($v == 1){
                DB::table('order_statuses')->where('id','=',$k)->update(['active' => 1]);
            }
        }
        foreach($input['goto'] as $k => $v){
            if(count($v) > 0){
                $ids = implode(',',$v);
                DB::table('order_statuses')->where('id','=',$k)->update(['goto' => $ids]);
            }
        }
        return redirect('/admin/order-status')->with('success','Статусы обновлены!');
    }


    public function deleteProductInOrder(Request $request){
        $input = $request->all();
        DB::table('cart')
            ->where('id','=',$input['cartid'])
            ->where('order_id','=',$input['id'])
            ->where('session_id','=',$input['sess'])
            ->delete();
        $cartTotal = 0;
        $rr = DB::table('cart')
            ->select()
            ->where('session_id',$input['sess'])
            ->where('order_id','=',$input['id'])
            ->whereNull('deleted_at')
            ->get();
        foreach ($rr as $r){
            $cartTotal+= $r->total;
        }
        return response()->json(['success'=>true, 'rowid' => $input['cartid'], 'carttotal' => $cartTotal]);
    }

    public function updateProductInOrder(Request $request){
        $input = $request->all();
        $cartrow = DB::table('cart')
            ->select()
            ->where('id','=',$input['cartid'])
            ->where('order_id','=',$input['id'])
            ->where('session_id','=',$input['sess'])
            ->first();
        $qnt = $input['val'];
        $rowtotal = round(($cartrow->price * $qnt),2);
        DB::table('cart')
            ->where('id','=',$input['cartid'])
            ->where('order_id','=',$input['id'])
            ->where('session_id','=',$input['sess'])
            ->update(['qnt' => $qnt, 'total' => $rowtotal]);
        $cartTotal = 0;
        $rr = DB::table('cart')
            ->select()
            ->where('session_id',$input['sess'])
            ->where('order_id','=',$input['id'])
            ->whereNull('deleted_at')
            ->get();
        foreach ($rr as $r){
            $cartTotal+= $r->total;
        }

        DB::table('orders')->where('id','=',$input['id'])->update(['total' => $cartTotal]);

        return response()->json(['success'=>true, 'rowid' => $input['cartid'], 'rowtotal' => $rowtotal, 'carttotal' => $cartTotal]);
    }

    public function addProductToOrder(Request $request){
        $input = $request->all();

        if(isset($input['product_id']) && $input['product_id'] > 0){
            $product = Product::visible()->where('id',$input['product_id'])->first();
        } elseif(isset($input['article']) && $input['article'] != ''){
            $product = Product::visible()->where('article',$input['article'])->first();
        }

        if($product){
            $exist = Cart::where('order_id','=',$input['id'])
                ->whereNull('deleted_at')
                ->where('session_id','=', $input['sess'])
                ->where('product_id','=', $product->id)
                ->first();
            if(isset($exist->id) && $exist->id > 0){
                $qnt = $exist->qnt + 1;
                $total = $product->price * $qnt;
                DB::table('cart')
                    ->where('session_id','=', $input['sess'])
                    ->where('order_id','=', $input['id'])
                    ->where('id','=', $exist->id)
                    ->update([
                        'price' => $product->price,
                        'qnt' => $qnt,
                        'total' => $total,
                        'updated_at' => Carbon::now()
                    ]);
                $id = $exist->id;
            } else {
                $total = $product->price * 1;

                $data = [
                    'session_id' => $input['sess'],
                    'order_id' => $input['id'],
                    'product_id' => $product->id,
                    'price' => $product->price,
                    'qnt' => 1,
                    'total' => $total,
                    'day' => Carbon::now(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'user_id' => $input['user'],
                ];
                $id = DB::table('cart')->insertGetId($data);
            }

            $cartTotal = 0;
            $rr = DB::table('cart')
                ->select()
                ->where('session_id',$input['sess'])
                ->where('order_id','=',$input['id'])
                ->whereNull('deleted_at')
                ->get();
            foreach ($rr as $r){
                $cartTotal+= $r->total;
            }

            DB::table('orders')->where('id','=',$input['id'])->update(['total' => $cartTotal]);

            $html = '<tr id="cart-'.$id.'">
                                <td>';
                                    if(isset($product->picture) && $product->picture != '') {
                                        $html .= '<img src="'.$product->picture.'" style="max-height:80px; max-width:120px;" />';
                                    }
                        $html .='</td>
                                <td>'.$product->title.'</td>
                                <td>'.$product->price.'грн</td>
                                <td>
                                    <input type="number" style="width:100px; display: inline-block;" id="qnt-'.$id.'" data-id="'.$id.'" value="1" class="form-control cart-qnt">шт.
                                </td>
                                <td><span id="cart-total-'.$id.'">'.$total.'</span>грн</td>
                                <td>
                                    <a class="btn btn-danger btn-prod-del" id="order-prod-del-'.$id.'" data-id="'.$id.'" data-title="'.$product->title.'" href="" role="button" title="Удалить товар"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>';
            return response()->json(['success'=>true, 'rowid' => $id, 'title' => $product->title, 'html' => $html, 'carttotal' => $cartTotal]);
        }
    }


    public function orderSave(Request $request, $id){
        $order = Order::find($id);
        if($order){
            $order->update($request->all());
            $order->save();
            return redirect('/admin/orders/'.$id)->with('success','Заказ обновлен!');
        } else {
            return redirect('/admin/orders/'.$id.'/edit')->with('warning','Ошибка сохранения заказа!');
        }
    }


    public function printOrder($id){
        abort_unless(\Gate::allows('orders_access'), 403);

        $order = Order::where('id',$id)->with('payment','shipment','status','client','manager')->first();
        if(!$order){
            return redirect('/admin/orders');
        }

        $cart = Cart::getBigCart($id);

        $payments = PaymentMethod::visible()->get();
        $shipments = ShipmentMethod::visible()->get();
        $statuses = OrderStatuses::visible()->get();
        return view('admin.orders.print', compact('order','payments','shipments','statuses','cart'));
    }

}
