<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use App\BlogCategory;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlogRequest;
use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateBlogRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\News;
use App\Services\BlogService;
use App\Services\NewsService;
use App\Tag;
use Illuminate\Http\Request;
use App\Services\LogService;

class BlogsController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new BlogService();
    }

    public function index(Request $request)
    {
        abort_unless(\Gate::allows('news_access'), 403);
        if($request->get('reset') == 'y'){
            session(['filtersNews' => []]);
        }
        $result = $this->service->getItemsForAdmin($request->all());
        $blogs = $result['blogs'];
        $filter = $result['filter'];
        $categories = BlogCategory::isActive()->orderBy('title_ru','asc')->get();

        return view('admin.blogs.index', compact('blogs','filter','categories'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('news_create'), 403);
        $categories = BlogCategory::isActive()->orderBy('title_ru','asc')->get();
        $others = Blog::select('id','title_ru')->isActive()->notDeleted()->orderBy('title_ru','asc')->get();
        return view('admin.blogs.create',compact('others','categories'));
    }

    public function slug(Request $request)
    {
        $res = $this->service->checkSlug($request);
        return response()->json(['success' => $res]);
    }

    public function store(StoreBlogRequest $request)
    {
        abort_unless(\Gate::allows('news_create'), 403);
        $this->service->storeItem($request);

        return redirect()->route('admin.blogs.index')->with('success','Блог создан!');
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('news_edit'), 403);
        $result = $this->service->getItemForEdit($id);
        $blog = $result['blog'];
        $langs = $result['langs'];
        $recomends = $result['recomends'];
        $categoriesIn = $result['categoriesIn'];
        $others = Blog::select('id','title_ru')->isActive()->notDeleted()->orderBy('title_ru','asc')->get();
        $categories = BlogCategory::isActive()->orderBy('title_ru','asc')->get();

        return view('admin.blogs.edit', compact( 'blog','langs','recomends','others','categories','categoriesIn'));
    }

    public function update(UpdateBlogRequest $request, $id)
    {
        abort_unless(\Gate::allows('news_edit'), 403);
        $this->service->updateItem($request,$id);

        return redirect()->route('admin.blogs.index')->with('success','Блог сохранен!');
    }

    public function show(Page $page)
    {
    }

    public function destroy($id)
    {
        abort_unless(\Gate::allows('news_delete'), 403);
        $this->service->deleteItem($id);
        return response()->json(['success'=>true, 'id' => $id]);
    }

}
