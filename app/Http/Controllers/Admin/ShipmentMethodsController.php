<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreShipmentMethodRequest;
use App\Http\Requests\UpdateShipmentMethodRequest;
use App\ShipmentMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ShipmentMethodsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('shipment_method_access'), 403);

        $shipments = ShipmentMethod::get();
        return view('admin.shipment-methods.index', compact('shipments'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('shipment_method_create'), 403);
        return view('admin.shipment-methods.create');
    }

    public function store(StoreShipmentMethodRequest $request)
    {
        abort_unless(\Gate::allows('shipment_method_create'), 403);
        if($request->get('active') == 1){
            //ok
        } else {
            $request['active'] = 0;
        }
        if ($request->hasFile('image'))
        {
            $f_name = date('YmdHis').$request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(public_path('uploads/'),$f_name);
            $request['picture'] = '/uploads/'.$f_name;
        }

        $shipment = ShipmentMethod::create($request->all());
        return redirect('/admin/shipment-methods')->with('success','Доставка создана!');
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('shipment_method_edit'), 403);
        $shipmentMethod = ShipmentMethod::find($id);
        return view('admin.shipment-methods.edit', compact('shipmentMethod'));
    }

    public function update(UpdateShipmentMethodRequest $request, ShipmentMethod $shipmentMethod)
    {
        abort_unless(\Gate::allows('shipment_method_edit'), 403);
        if($request->get('active') == 1){
            //ok
        } else {
            $request['active'] = 0;
        }
        if($request->get('del_pic') == 1){
            $request['picture'] = '';
        }

        if ($request->hasFile('image'))
        {
            $f_name = date('YmdHis').$request->file('image')->getClientOriginalName();
            $file = $request->file('image');
            $file->move(public_path('uploads/'),$f_name);
            $request['picture'] = '/uploads/'.$f_name;
        }

        $shipmentMethod->update($request->all());

        return redirect('/admin/shipment-methods')->with('success','Доставка обновлена!');
    }

    public function show(News $news)
    {
        /*abort_unless(\Gate::allows('product_show'), 403);

        return view('admin.products.show', compact('product'));*/
    }

    public function delete(Request $request){
        abort_unless(\Gate::allows('shipment_method_delete'), 403);
        if($request->get('id') > 0){
            DB::table('shipment_methods')
                ->where('id','=',$request->get('id'))
                ->delete();
        }

        return response()->json(['success'=>true, 'id' => $request->get('id')]);
    }



    public function destroy(Product $product)
    {
       /* abort_unless(\Gate::allows('product_delete'), 403);

        $product->delete();

        return back();*/
    }

    public function massDestroy(MassDestroyProductRequest $request)
    {
     /*   Product::whereIn('id', request('ids'))->delete();

        return response(null, 204);*/
    }




}
