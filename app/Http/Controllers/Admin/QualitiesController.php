<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Quality;
use Illuminate\Http\Request;

class QualitiesController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('tag_access'), 403);

        $qualities = Quality::orderBy('sort','asc')->paginate(30);

        return view('admin.qualities.index', compact('qualities'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('tag_create'), 403);

        return view('admin.qualities.create');
    }

    public function store(StoreDocumentRequest $request)
    {
        abort_unless(\Gate::allows('tag_create'), 403);
        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['link'] = '';
        $request['active'] = ($request['active'] == 1) ? true : false;
        $quality = Quality::create($request->all());

        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/qualities"))) {
                \File::makeDirectory(public_path("uploads/qualities"));
            }
            $f_name = 'quality-'.$quality->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/qualities/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $quality->link = '/uploads/qualities/'.$f_name;
            $quality->save();
        }

        return redirect()->route('admin.qualities.index')->with('success','Отчет создан!');
    }

    public function edit(Quality $quality)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        return view('admin.qualities.edit', compact( 'quality'));
    }

    public function update(UpdateDocumentRequest $request, Quality $quality)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['active'] = ($request['active'] == 1) ? true : false;
        if($request->get('del-pic') == 1){
            if(file_exists(public_path($quality->link))){
                unlink(public_path($quality->link));
            }
            $request['link'] = '';
        }
        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/qualities"))) {
                \File::makeDirectory(public_path("uploads/qualities"));
            }
            $f_name = 'quality-'.$quality->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/qualities/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $request['link'] = '/uploads/qualities/'.$f_name;
        }

        $quality->update($request->all());


        return redirect()->route('admin.qualities.index')->with('success','Отчет обновлен!');
    }

    public function show()
    {

    }

    public function destroy(Quality $quality)
    {
        abort_unless(\Gate::allows('tag_delete'), 403);

        if($quality->link != ''){
            if(file_exists(public_path($quality->link))){
                unlink(public_path($quality->link));
            }
        }
        $quality->delete();

        return response()->json(['success'=>true, 'id' => $quality->id]);
    }
}
