<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class ContactsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('contacts_access'), 403);



        return view('admin.contacts.index');
    }

    /*
    public function getMarkerById(Request $request)
    {
        if($request->get('id') > 0){
            $marker = MapMarker::find($request->get('id'));
            if($marker){
                return response()->json(['success'=>true, 'marker' => $marker]);
            }
        }
        return response()->json(['success' => false]);
    }

*/
}
