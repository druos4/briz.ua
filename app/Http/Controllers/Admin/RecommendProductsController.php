<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\News;
use App\Services\NewsService;
use App\Tag;
use Illuminate\Http\Request;
use App\Services\LogService;

class RecommendProductsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('product_access'), 403);

        $categories = Category::where('parent_id',0)->orderBy('sort','asc')->get();

        return view('admin.products.recommend', compact('categories'));
    }


    public function update(Request $request)
    {
        abort_unless(\Gate::allows('product_access'), 403);

        Category::where('parent_id',0)->update([
            'recommend_products' => null
        ]);

        if(!empty($request->get('categories'))){
            foreach($request->get('categories') as $category){
                Category::where('parent_id',0)
                    ->where('id',$category)
                    ->update([
                        'recommend_products' => true
                    ]);
            }
        }

        return redirect()->route('admin.recommend-products')->with('success','Настройки сохранены!');
    }

}
