<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTagRequest;
use App\Http\Requests\UpdateTagRequest;
use App\Services\TagService;
use App\Tag;
use Illuminate\Http\Request;
use App\Services\LogService;

class TagsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('tag_access'), 403);

        if($request->get('reset') == 'y'){
            session(['filtersTags' => []]);
        }

        $service = new TagService();
        $result = $service->getItemsForAdmin($request->all());
        $tags = $result['items'];
        $filter = $result['filter'];
        $sort = $result['sort'];

        return view('admin.tags.index', compact('tags','filter','sort'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('tag_create'), 403);

        return view('admin.tags.create');
    }

    public function store(StoreTagRequest $request)
    {
        abort_unless(\Gate::allows('tag_create'), 403);

        $request['created_by'] = $request['updated_by'] = auth()->id();
        $tag = Tag::create($request->all());
        $log = new LogService();
        $log->saveLog('Tag',$tag);

        return redirect()->route('admin.tags.index')->with('success','Тег создан!');
    }

    public function edit(Tag $tag)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        return view('admin.tags.edit', compact( 'tag'));
    }

    public function update(UpdateTagRequest $request, Tag $tag)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);
        $request['updated_by'] = auth()->id();
        $log = new LogService();
        $log->saveLog('Tag',$tag, $request->all());
        $tag->update($request->all());

        return redirect()->route('admin.tags.index')->with('success','Тег обновлен!');
    }

    public function show(Tag $tag)
    {
        abort_unless(\Gate::allows('tag_show'), 403);
        return view('admin.tags.show', compact('tag'));
    }

    public function destroy(Tag $tag)
    {
        abort_unless(\Gate::allows('tag_delete'), 403);

        $tag->deleted_by = auth()->id();
        $tag->save();
        $tag->delete();

        return response()->json(['success'=>true, 'id' => $tag->id]);
    }

    public function history($id){
        $tag = Tag::find($id);
        if(!$tag){
            return redirect()->route('admin.tags.index')->with('danger','Элемент не найден!');
        }
        $log = new LogService();
        $logs = $log->getLogs('Tag',$tag);

        return view('admin.tags.history', compact('tag','logs'));
    }
}
