<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MapMarker;
use App\Services\MapService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class MapsController extends Controller
{

    public function sync()
    {

    }

    public function index(Request $request)
    {
        abort_unless(\Gate::allows('map_access'), 403);

        $map = new MapService();
        $result = $map->importMarkersFromBilling();
        return redirect('/admin')->with('success','Карта покрытия обновлена!');
    }

    public function create()
    {
        abort_unless(\Gate::allows('map_create'), 403);

        $houses = DB::connection('mysql2')
            ->table('AddrMap as a')
            ->select('a.id','a.StreetID','a.Dom','s.Name')
            ->leftJoin('Street as s','s.id','=','a.StreetID')
            ->whereNull('a.GeoID')
            ->orderBy('s.Name','asc')
            ->orderBy('a.Dom','asc')
            ->get();

        return view('admin.maps.create', compact('houses'));
    }

    public function getMarkerById(Request $request)
    {
        if($request->get('id') > 0){
            $marker = MapMarker::find($request->get('id'));
            if($marker){
                return response()->json(['success'=>true, 'marker' => $marker]);
            }
        }
        return response()->json(['success' => false]);
    }


}
