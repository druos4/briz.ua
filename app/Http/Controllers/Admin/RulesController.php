<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Price;
use App\Rule;
use Illuminate\Http\Request;

class RulesController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('tag_access'), 403);

        $rules = Rule::orderBy('sort','asc')->paginate(30);

        return view('admin.rules.index', compact('rules'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('tag_create'), 403);

        return view('admin.rules.create');
    }

    public function store(StoreDocumentRequest $request)
    {
        abort_unless(\Gate::allows('tag_create'), 403);
        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['link'] = '';
        $request['active'] = ($request['active'] == 1) ? true : false;
        $rule = Rule::create($request->all());

        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/rules"))) {
                \File::makeDirectory(public_path("uploads/rules"));
            }
            $f_name = 'price-'.$rule->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/rules/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $rule->link = '/uploads/rules/'.$f_name;
            $rule->save();
        }

        return redirect()->route('admin.rules.index')->with('success','Правило создано!');
    }

    public function edit(Rule $rule)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        return view('admin.rules.edit', compact( 'rule'));
    }

    public function update(UpdateDocumentRequest $request, Rule $rule)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);

        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['active'] = ($request['active'] == 1) ? true : false;
        if($request->get('del-pic') == 1){
            if(file_exists(public_path($rule->link))){
                unlink(public_path($rule->link));
            }
            $request['link'] = '';
        }
        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/rules"))) {
                \File::makeDirectory(public_path("uploads/rules"));
            }
            $f_name = 'price-'.$rule->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/rules/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $request['link'] = '/uploads/rules/'.$f_name;
        }

        $rule->update($request->all());


        return redirect()->route('admin.rules.index')->with('success','Правило обновлено!');
    }

    public function show()
    {

    }

    public function destroy($id)
    {
        abort_unless(\Gate::allows('tag_delete'), 403);
        $rule = Rule::where('id',$id)->first();
        if($rule->link != ''){
            if(file_exists(public_path($rule->link))){
                unlink(public_path($rule->link));
            }
        }
        $rule->delete();

        return response()->json(['success'=>true, 'id' => $rule->id]);
    }
}
