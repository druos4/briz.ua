<?php

namespace App\Http\Controllers\Admin;

use App\Core\Lib\Billing\BillingApi;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Log;
use App\News;
use App\Services\ImageService;
use App\Services\TariffService;
use App\Tag;
use App\Tariff;
use Carbon\Carbon;
use Faker\Provider\File;
use Illuminate\Http\Request;
use App\Services\LogService;
use DB;

class TariffsController extends Controller
{
    public function test()
    {
        abort_unless(\Gate::allows('tariffs_access'), 403);
        /*
        $service = new TariffService('internet');
        $tariffs = $service->getItemsForFront('home');
*/
        /*
        $service = new TariffService('internet');
        $tariffs = $service->getItemsForFront('business');
        */
/*
        $service = new TariffService('ctv');
        $tariffs = $service->getItemsForFront();
*/
/*
        $service = new TariffService('iptv');
        $tariffs = $service->getItemsForFront();
*/

        $service = new TariffService('union');
        $tariffs = $service->getItemsForFront('inet+tv');
        //$tariffs = $service->getItemsForFront('inet+iptv');
        //$tariffs = $service->getItemsForFront('inet+tvmax');

        echo '<pre>';
        print_r($tariffs);
        echo '</pre>';
    }

    public function index(Request $request)
    {
        abort_unless(\Gate::allows('tariffs_access'), 403);
        if($request->get('reset') == 'y'){
            session(['filtersTariffs' => []]);
        }
        $service = new TariffService();
        $result = $service->getItemsForAdmin($request->all());
        $index_groups = $service->getIndexGroups();
        $business_group = $service->getBusinessGroups();

        $filter = $result['filter'];
        $sort = $result['sort'];
        $items = $result['items'];
        $serviceTypes = $service->getServiceTypes();

        return view('admin.tariffs.index', compact('items','filter','sort','index_groups','business_group','serviceTypes'));
    }

    public function copy($id)
    {
        abort_unless(\Gate::allows('tariffs_edit'), 403);
        $tariff = Tariff::find($id);
        $others = Tariff::where('id','!=',$tariff->id)
            ->where('active',true)
            ->orderBy('title_ru','asc')
            ->get();
        return view('admin.tariffs.copy', compact( 'tariff','others'));
    }

    public function copyTariff(Request $request, $id)
    {
        $service = new TariffService();
        $service->copyTariffInfo($request->get('parent'), $id);
        return redirect('/admin/tariffs/'.$id.'/edit')->with('success','Тариф обновлен!');
    }


    public function edit($id)
    {
        abort_unless(\Gate::allows('tariffs_edit'), 403);
        $tariff = Tariff::find($id);
        $services = new TariffService();
        $index_groups = $services->getIndexGroups();
        $business_groups = $services->getBusinessGroups();
        $tvs = [];
        $baseTvsIds = getBaseTvsPackets();
        if(!empty($baseTvsIds)){
            $tvs = Tariff::select('id','title_ru')
                ->whereIn('tariff_type',['ctv','iptv'])
                ->whereIn('billing_id',$baseTvsIds)
                ->where('active',1)
                ->get();
        }
        $serviceTypes = $services->getServiceTypes();

        return view('admin.tariffs.edit', compact( 'tariff','index_groups','business_groups','tvs','serviceTypes'));
    }

    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('tariffs_edit'), 403);
        $tariff = Tariff::find($id);

        //return redirect()->back()->with('warning','Показ первым!!!');

        $index_group = null;
        if($request->get('index_group') != "" && $request->get('index_group') != "0"){
            $index_group = $request->get('index_group');
        }
        $union_group = null;
        if($request->get('union_group') != "" && $request->get('union_group') != "0"){
            $union_group = $request->get('union_group');
        }
        $business_group = null;
        if($request->get('business_group') != "" && $request->get('business_group') != "0"){
            $business_group = $request->get('business_group');
        }

        $show_from = (!empty($request->get('show_from'))) ? str_replace('T',' ',$request->get('show_from')).':00' : null;
        $show_to = (!empty($request->get('show_to'))) ? str_replace('T',' ',$request->get('show_to')).':00' : null;

        $data = [
            'title_ru' => $request->get('title_ru'),
            'title_ua' => $request->get('title_ua'),
            'title_en' => $request->get('title_en'),
            'bonus_payment' => ($request->get('bonus_payment') == 1) ? true : false,
            'get_bonus' => ($request->get('get_bonus') == 1) ? true : false,
            'can_seven_days' => ($request->get('can_seven_days') == 1) ? true : false,
            'prepay_discount' => ($request->get('prepay_discount') == 1) ? true : false,
            'description_ru' => $request->get('description_ru'),
            'description_ua' => $request->get('description_ua'),
            'description_en' => $request->get('description_en'),
            'active' => ($request->get('active') == 1) ? true : false,
            'first_to_show' => ($request->get('first_to_show') == 1) ? true : false,
            'sort' => $request->get('sort'),
            'updated_at' => Carbon::now(),
            'power' => ($request->get('power') != '') ? $request->get('power') : null,
            'union_group' => $union_group,
            'index_group' => $index_group,
            'business_group' => $business_group,
            'price_old' => ($request->get('price_old') != "" && $request->get('price_old') > $tariff->price) ? $request->get('price_old') : null,
            'channels_from' => (!empty($request->get('channels_from'))) ? $request->get('channels_from') : null,
            'show_from' => $show_from,
            'show_to' => $show_to,
            'has_films' => (!empty($request->get('has_films'))) ? true : false,
            'films_title_ru' => $request->get('films_title_ru'),
            'films_title_ua' => $request->get('films_title_ua'),
            'films_title_en' => $request->get('films_title_en'),
            'films_price' => $request->get('films_price'),
        ];

        if($request->get('del-pic') == 1){
            if(file_exists(public_path($tariff->picture))){
                unlink(public_path($tariff->picture));
            }
            $data['picture'] = null;
        }
        if ($request->hasFile('image'))
        {
            if (! \File::exists(public_path("uploads/tariffs"))) {
                \File::makeDirectory(public_path("uploads/tariffs"));
            }
            $f_name = 'tariff-'.$tariff->id.'-'.date('YmdHis').'.'.$request->file('image')->getClientOriginalExtension();
            $path = 'uploads/tariffs/'.$f_name;
            move_uploaded_file( $request->file('image'), $path);
            $data['picture'] = '/uploads/tariffs/'.$f_name;
        }

        $log = new LogService();
        $log->saveLog('Tariff',$tariff, $request->all());

        $tariff->update($data);
        Tariff::where('index_group',$index_group)->where('id','!=',$tariff->id)->update(['first_to_show' => null]);

        return redirect()->route('admin.tariffs.index')->with('success','Тариф обновлен!');
    }


    public function sync()
    {
        abort_unless(\Gate::allows('tariffs_access'), 403);
        $arr = [
            [
                'endpoint' => 'internetServices',
                'type' => 'internet',
            ],
            [
                'endpoint' => 'ctvServices',
                'type' => 'ctv',
            ],
            [
                'endpoint' => 'iptvServices',
                'type' => 'iptv',
            ],
            [
                'endpoint' => 'unionServices',
                'type' => 'union',
            ],
        ];

        foreach($arr as $a){
            try {
                $billingAPI = BillingApi::get();
                $response = $billingAPI->execute($a['endpoint'], []);
            } catch (ApiException $e) {
                print_r($e->getMessage());
                die;
            }

            if(!empty($response)){
                $service = new TariffService($a['type']);
                $service->importFromBilling($response);
            }
        }


        return redirect()->route('admin.tariffs.index')->with('success','Тарифы сихронизированы!');
    }

    public function history($id){
        $news = Tariff::find($id);
        if(!$news){
            return redirect()->route('admin.tariffs.index')->with('danger','Элемент не найден!');
        }
        $log = new LogService();
        $logs = $log->getLogs('Tariff',$news);

        return view('admin.tariffs.history', compact('news','logs'));
    }

    public function logs(Request $request)
    {
        $log_service = new LogService();
        $logs = $log_service->getTariffsLogs('Tariff');
        return view('admin.tariffs.logs', compact('logs'));
    }

}
