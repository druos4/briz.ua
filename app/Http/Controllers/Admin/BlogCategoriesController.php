<?php

namespace App\Http\Controllers\Admin;

use App\BlogCategory;
use App\BlogCategoryIn;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlogCategoryRequest;
use App\Http\Requests\UpdateBlogCategoryRequest;
use App\Services\BlogService;
use Illuminate\Http\Request;

class BlogCategoriesController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new BlogService();
    }

    public function index(Request $request)
    {
        abort_unless(\Gate::allows('news_access'), 403);
        $filter = $request->all();
        $query = BlogCategory::query();
        if(isset($request['active']) && $request['active'] == 'y'){
            $query->isActive();
        } elseif(isset($request['active']) && $request['active'] == 'n') {
            $query->isInActive();
        }
        if(!empty($request['slug'])){
            $query->where('slug','LIKE','%'.$request['slug'].'%');
        }
        if(!empty($request['title'])){
            $query->where(function ($query2) use ($request) {
                $query2->where('title_ru', 'LIKE', '%'.$request['title'].'%')
                    ->orWhere('title_ua', 'LIKE', '%'.$request['title'].'%')
                    ->orWhere('title_en', 'LIKE', '%'.$request['title'].'%');
            });
        }
        if(!empty($request['id'])){
            $query->where('id',$request['id']);
        }
        $categories = $query->orderBy('id','desc')->paginate(30);
        return view('admin.blogcategories.index', compact('categories','filter'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('news_create'), 403);
        return view('admin.blogcategories.create');
    }

    public function slug(Request $request)
    {
        $res = $this->service->checkSlug($request);
        return response()->json(['success' => $res]);
    }

    public function store(StoreBlogCategoryRequest $request)
    {
        abort_unless(\Gate::allows('news_create'), 403);
        $request['is_active'] = ($request->get('is_active') == 1) ? true : false;
        BlogCategory::create($request->all());
        return redirect()->route('admin.blogcategories.index')->with('success','Категория блога создана!');
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('news_edit'), 403);
        $blogCategory = BlogCategory::find($id);
        return view('admin.blogcategories.edit', compact( 'blogCategory'));
    }

    public function update(UpdateBlogCategoryRequest $request, $id)
    {
        abort_unless(\Gate::allows('news_edit'), 403);
        $request['is_active'] = ($request->get('is_active') == 1) ? true : false;
        $blogCategory = BlogCategory::where('id',$id)->first();
        $blogCategory->update($request->all());
        return redirect()->route('admin.blogcategories.index')->with('success','Категория блога сохранена!');
    }

    public function show(Page $page)
    {
    }

    public function destroy($id)
    {
        abort_unless(\Gate::allows('news_delete'), 403);
        BlogCategory::destroy($id);
        BlogCategoryIn::where('blog_category_id',$id)->delete();
        return response()->json(['success' => true, 'id' => $id]);
    }

}
