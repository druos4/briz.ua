<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Log;
use App\Services\MenuService;
use Illuminate\Http\Request;
use App\Services\LogService;
use App\Menu;
use App\MenuItem;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('menu_access'), 403);
        $menus = Menu::get();
        return view('admin.menu.index', compact('menus'));
    }

    public function create(Request $request)
    {
        if($request->get('title') != '' && $request->get('code') != ''){
            $row = Menu::where('code',$request->get('code'))->first();
            if(!$row){
               $data = [
                   'title' => $request->get('title'),
                   'code' => $request->get('code'),
                   'active' => 1
               ];
               Menu::create($data);
            }
        }
        return redirect('/admin/menu');
    }

    public function edit($id){
        abort_unless(\Gate::allows('menu_edit'), 403);
        $menu = Menu::find($id);

        $items = MenuItem::where('menu_id',$menu->id)
            ->where('parent_id', 0)
            ->with('children.children')->orderBy('sort')->get();

        /*$categories = Category::where('parent_id', 0)->where('active',1)
            ->with('children.children')->orderBy('sort')->get();*/
        $categories = [];

        return view('admin.menu.edit', compact('menu','items','categories'));
    }


    public function addItem(Request $request, $id){
        $service = new MenuService($id);
        $result = $service->storeMenuItem($request);
        $html = $result['html'];
        $item = $result['item'];
        $existed = $result['existed'];

        return response()->json(['success'=>true, 'id' => $item->id, 'html' => $html, 'parent_id' => $item->parent_id, 'existed' => $existed]);
    }



    public function itemsSaveSort(Request $request, $id){
        $service = new MenuService($id);
        $service->saveItemsSorting($request);
        return ['success' => true];
    }


    public function delItem(Request $request, $id){
        if($request->get('item_id') > 0){
            $service = new MenuService($id);
            $service->deleteMenuItem($request);
            return response()->json(['success'=>true, 'id' => $request->get('item_id')]);
        }
    }



    public function show(){
        abort_unless(\Gate::allows('menu_show'), 403);
        /*
        $menu = DB::table('menus')->select()->where('code','=','product_menu')->first();
        $items = MenuItem::where('menu_id',$menu->id)
            ->where('parent_id', 0)
            ->with('children.children')->orderBy('sort')->get();

        $menuService = DB::table('menus')->select()->where('code','=','service_menu')->first();
        $itemsService = MenuItem::where('menu_id',$menuService->id)
            ->where('parent_id', 0)
            ->with('children.children')->orderBy('sort')->get();


        return view('client.menu.preview', compact('menu','items','menuService','itemsService'));
        */
    }

    public static function getServiceMenu(){
        $menu = DB::table('menus')->select()->where('code','=','service_menu')->first();
        $items = MenuItem::where('menu_id',$menu->id)
            ->where('parent_id', 0)
            ->with('children.children')->orderBy('sort')->get();
        return view('client.menu.service', compact('items'))->render();
    }
    public static function getProductMenu(){
        $menu = DB::table('menus')->select()->where('code','=','product_menu')->first();
        $items = MenuItem::where('menu_id',$menu->id)
            ->where('parent_id', 0)
            ->with('children.children')->orderBy('sort')->get();
        return view('client.menu.product', compact('items'))->render();
    }

    public function generate($id){
        $menu = DB::table('menus')->select()->where('id','=',$id)->first();
        $items = MenuItem::where('menu_id',$menu->id)
            ->where('parent_id', 0)
            ->with('children.children')->orderBy('sort')->get();

        $lang = 'ru';
        if($menu->code == 'product_menu'){
            $html = view('client.menu.product', compact('items','lang'))->render();
        } elseif($menu->code == 'service_menu') {
            $html = view('client.menu.service', ['itemsService' => $items, 'lang' => $lang])->render();
        }
        DB::table('menus')
            ->where('id','=',$id)
            ->update(['updated_at' => Carbon::now(), 'render' => $html]);

        $lang = 'ua';
        if($menu->code == 'product_menu'){
            $html = view('client.menu.product', compact('items','lang'))->render();
        } elseif($menu->code == 'service_menu') {
            $html = view('client.menu.service', ['itemsService' => $items, 'lang' => $lang])->render();
        }
        DB::table('menus')
            ->where('id','=',$id)
            ->update(['updated_at' => Carbon::now(), 'render_ua' => $html]);

        $lang = 'en';
        if($menu->code == 'product_menu'){
            $html = view('client.menu.product', compact('items','lang'))->render();
        } elseif($menu->code == 'service_menu') {
            $html = view('client.menu.service', ['itemsService' => $items, 'lang' => $lang])->render();
        }
        DB::table('menus')
            ->where('id','=',$id)
            ->update(['updated_at' => Carbon::now(), 'render_en' => $html]);

        return redirect('admin/menu/'.$id.'/edit')->with('success','Меню сгенерировано!');
    }

    public static function getSubCategories($id){
        return DB::table('categories')
            ->select()
            ->where('active','=',1)
            ->where('parent_id','=',$id)
            ->orderBy('sort','asc')
            ->get();

    }
    public static function getItem(Request $request, $id){
        $prod = [];
        $html = '';
        $item = MenuItem::where('menu_id',$id)
            ->where('id', '=',$request->get('item_id'))
            ->first();
        if($item->item_type == 'product'){
            $prod = DB::table('products')->select('id','category_id')->where('id','=',$item->options)->first();
            $pcats = [];
            $res = DB::table('product_category')->select()->where('category_id','=',$prod->category_id)->get();
            if(isset($res) && count($res) > 0){
                foreach ($res as $r){
                    $pcats[] = $r->product_id;
                }
            }
            $query = DB::table('products as p')
                ->select('p.id','p.title','p.article','p.price');
            if(count($pcats) > 0){
                $query->where(function ($query2) use ($prod,$pcats) {
                    $query2->where('p.category_id', '=', $prod->category_id)
                        ->orWhereIn('p.id', $pcats);
                });
            } else {
                $query->where('p.category_id', '=', $prod->category_id);
            }
            $prods = $query->whereNull('p.deleted_at')
                ->where('p.active','=',1)
                ->orderBy('p.title','asc')
                ->get();
            $html = '<option value="0"> - выберите товар - </option>';
            foreach($prods as $p){
                $html .= '<option value="'.$p->id.'">'.$p->title;
                if($p->article != ''){
                    $html .= ' ('.$p->article.')';
                }
                $html .= ' ['.$p->price.'грн]</option>';
            }
        }
        return response()->json(['success'=>true, 'id' => $request->get('item_id'), 'item' => $item, 'prod' => $prod, 'html' => $html]);
    }
}
