<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\CategoryProperty;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyProductRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Product;
use App\ProductBuyWith;
use App\ProductImage;
use App\Services\ProductService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Images;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\File as File;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('product_access'), 403);
        if($request->get('reset') == 'y'){
            session(['filtersProducts' => []]);
        }
        $service = new ProductService();
        $result = $service->getItemsForAdmin($request->all());
        $products = $result['items'];
        $filter = $result['filter'];
        $sort = $result['sort'];

        $categories = Category::where('parent_id', 0)
            ->with('children.children')->orderBy('sort')->get();

        return view('admin.products.index', compact('products','filter','categories','sort'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('product_create'), 403);
        $categories = Category::where('parent_id', 0)->where('active',1)
            ->with('children.children')->orderBy('sort')->get();
        $warranties = trans('custom.warranty');
        return view('admin.products.create',compact('categories','warranties'));
    }

    public function store(StoreProductRequest $request)
    {
        abort_unless(\Gate::allows('product_create'), 403);

        $service = new ProductService();
        $product_id = $service->storeProduct($request);

        return redirect('/admin/products/'.$product_id.'/edit')->with('success','Товар сохранен!');
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('product_edit'), 403);
        $product = Product::find($id);
        if(!$product){
            return redirect('/admin/products')->with('warning','Товар не найден!');
        }
        $gallery = ProductImage::where('product_id',$id)->with('image')->get();
        $categories = Category::where('parent_id', 0)->where('active',1)
            ->with('children.children')->orderBy('sort','asc')->get();
        $properties = CategoryProperty::getCatProps($product->category_id);
        $propertyValues = Product::getProductPropertyValues($product->id);
        $moreCategories = Product::getProductMoreCategories($product->id);
        $buywith = ProductBuyWith::with('target')->where('product_id',$product->id)->get();

        $warranties = trans('custom.warranty');

        return view('admin.products.edit', compact('product','categories','properties','propertyValues','moreCategories','gallery','buywith','warranties'));
    }

    public function update(UpdateProductRequest $request, $id)
    {
        abort_unless(\Gate::allows('product_edit'), 403);

        $service = new ProductService();
        $product_id = $service->updateProduct($request,$id);
        return redirect('/admin/products/'.$id.'/edit')->with('success','Товар обновлен!');
    }

    public function show(Product $product)
    {
        abort_unless(\Gate::allows('product_show'), 403);

        return view('admin.products.show', compact('product'));
    }

    public function delete(Request $request){
        if($request->get('id') > 0){
            DB::table('products')
                ->where('id','=',$request->get('id'))
                ->update(['deleted_at' => Carbon::now()]);
            ProductBuyWith::where('buywith_id',$request->get('id'))->delete();
        }
        return response()->json(['success'=>true, 'id' => $request->get('id')]);
    }



    public function destroy(Product $product)
    {
        abort_unless(\Gate::allows('product_delete'), 403);

        $product->delete();

        return back();
    }

    public function massDestroy(MassDestroyProductRequest $request)
    {
        Product::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }



    public function moreCategoriesSet(Request $request){
        $input = $request->all();
        if(isset($input['id']) && isset($input['product_id']) && isset($input['state'])){
            DB::table('product_category')
                ->where('product_id','=',$input['product_id'])
                ->where('category_id','=',$input['id'])
                ->delete();
            if($input['state'] == 1){
                DB::table('product_category')->insert(['product_id' => $input['product_id'], 'category_id' => $input['id']]);
            }
        }
        return response()->json(['success'=>true, 'id' => $input['id'], 'state' => $input['state']]);
    }




    public function pictureUpload(Request $request){
        echo '<pre>';
        print_r($request->all());
        echo '</pre>';
    }



    public function videoAdd(Request $request, $product_id){
        if($request->get('code') != ''){
            $input = $request->all();
            $check = false;
            $res = DB::table('product_videos')
                ->select()
                ->where('product_id','=',$product_id)
                ->where('code','=',$input['code'])->first();
            if(isset($res->id) && $res->id > 0){
                $check = true;
            }
            $html = '';
            if($check == false){
                $id = DB::table('product_videos')->insertGetId(['product_id' => $product_id,
                    'code' => $input['code'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')]);
                $html = '<tr id="vrow-'.$id.'">
                    <td>'.$input['code'].'</td>
                    <td><iframe width="200" height="100" src="https://www.youtube.com/embed/'.$input['code'].'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></td>
                    <td>
                        <a class="btn btn-danger btn-video-del" id="vdel-'.$id.'" href="" role="button" data-id="'.$id.'" title="Удалить"><i class="fas fa-remove"></i></a>
                    </td>
                </tr>';
            }
            return response()->json(['success'=>true, 'id' => $id, 'code' => $input['code'], 'html' => $html]);
        }
    }

    public function videoDelete(Request $request, $product_id){
        if($request->get('id') != ''){
            DB::table('product_videos')
                ->where('product_id','=',$product_id)
                ->where('id','=',$request->get('id'))
                ->delete();
            return response()->json(['success'=>true, 'id' => $request->get('id')]);
        }
    }


    public function documentUpload(Request $request, $id){
        if ($request->hasFile('doc'))
        {
            $data = ['product_id' => $id, 'title' => $request->get('title')];
            $f_name = $request->file('doc')->getClientOriginalName();
            $file = $request->file('doc');
            $file->move(public_path('uploads/'),$f_name);
            $data['url'] = '/uploads/'.$f_name;
            $d = ProductDocument::create($data);
            $html = '<li class="list-group-item" id="doc-item-'.$d->id.'">'.$d->title.' <a href="'.$data['url'].'" target="_blank">'.$data['url'].'</a>
                <a class="btn btn-danger btn-xs doc-del-btn" data-id="'.$d->id.'" id="doc-del-'.$d->id.'" href="" role="button" title="Удалить" style="float:right;"><i class="fas fa-remove"></i></a>
            </li>';

            return response()->json(['success'=>true, 'id' => $d->id, 'html' => $html]);
        }
    }


    public function documentDelete(Request $request){
        if($request->get('id') > 0){
            ProductDocument::where('id',$request->get('id'))->delete();
            return response()->json(['success'=>true, 'id' => $request->get('id')]);
        }
    }


    public function galleryUpload(Request $request, $id){
        if ($request->hasFile('files'))
        {

            $fileArray = array('image' => $request->file('files')[0]);
            $rules = array(
                'image' => 'mimes:jpeg,jpg,png,gif|required|max:5000'
            );
            $validator = \Validator::make($fileArray, $rules);
            if ($validator->fails())
            {
                return response()->json(['success' => false]);
            }

            $data = [];
            $f_name = date('YmdHis').$request->file('files')[0]->getClientOriginalName();
            $file = $request->file('files')[0];
            $file->move(public_path('uploads/original/'),$f_name);
            $data['original'] = '/uploads/original/'.$f_name;
            $data['thumbnail'] = '/uploads/thumbnails/'.$f_name;
            $row = Images::create($data);

            Image::make(public_path('uploads/original/'.$f_name))
                ->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save(public_path('uploads/thumbnails/'.$f_name));

            $html = '<div class="gal-img" id="'.$row->id.'">
                <img src="'.$data['thumbnail'].'" />
                <a class="btn btn-danger btn-xs" data-id="'.$row->id.'" id="gal-del-'.$row->id.'" href="" role="button" title="Удалить"><i class="fas fa-remove"></i></a>
                </div>';

            return response()->json(['success'=>true, 'id' => $row->id, 'pic' => $html]);
        }
    }


    public function galleryUpload360(Request $request, $id){
        if ($request->hasFile('files360'))
        {
            $data = [];
            $dir = public_path().'/uploads/products360/'.$id;
            echo $dir;
            if(!File::exists($dir)){
                File::makeDirectory($dir, 0775, true, true);
                echo ' MAKE';
            }
            $f_name = $request->file('files360')[0]->getClientOriginalName();
            $file = $request->file('files360')[0];
            $file->move($dir.'/',$f_name);
            $html = '<div class="gal-img photo360" id="'.$f_name.'">
                <img src="'.$dir.'/'.$f_name.'" />
                <a class="btn btn-danger btn-xs" data-id="'.$f_name.'" id="gal-del-'.$f_name.'" href="" role="button" title="Удалить"><i class="fas fa-remove"></i></a>
                </div>';
            /*
            $f_name = date('YmdHis').$request->file('files')[0]->getClientOriginalName();
            $file = $request->file('files')[0];
            $file->move(public_path('uploads/original/'),$f_name);
            $data['original'] = '/uploads/original/'.$f_name;
            $data['thumbnail'] = '/uploads/thumbnails/'.$f_name;
            $row = Images::create($data);

            Image::make(public_path('uploads/original/'.$f_name))
                ->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save(public_path('uploads/thumbnails/'.$f_name));

            $html = '<div class="gal-img" id="'.$row->id.'">
                <img src="'.$data['thumbnail'].'" />
                <a class="btn btn-danger btn-xs" data-id="'.$row->id.'" id="gal-del-'.$row->id.'" href="" role="button" title="Удалить"><i class="fas fa-remove"></i></a>
                </div>';
*/
            return response()->json(['success'=>true, 'id' => $f_name, 'pic' => $html]);
        }
    }


    public function galleryDelete(Request $request, $product_id, $row_id){
        $img = Images::find($row_id);
        if(isset($img->id)){
            //del pic
            File::delete(public_path($img->original));
            File::delete(public_path($img->thumbnail));
        }
        $img->delete();
        DB::table('product_images')
            ->where('product_id','=',$product_id)
            ->where('image_id','=',$row_id)
            ->delete();
        $ids = explode(',',$request->get('ids'));
        foreach ($ids as $k => $v){
            if($v == $row_id){
                unset($ids[$k]);
            } elseif($v == ''){
                unset($ids[$k]);
            }
        }
        return response()->json(['success'=>true, 'id' => $row_id, 'ids' => implode(',',$ids)]);
    }



    public function getByCatId(Request $request){
        $input = $request->all();
        if(isset($input['catid']) && $input['catid'] > 0){
            $pcats = [];
            $res = DB::table('product_category')->select()->where('category_id','=',$input['catid'])->get();
            if(isset($res) && count($res) > 0){
                foreach ($res as $r){
                    $pcats[] = $r->product_id;
                }
            }
            $query = DB::table('products as p')
                ->select('p.id','p.title','p.article','p.price');
            if(count($pcats) > 0){
                $query->where(function ($query2) use ($input,$pcats) {
                    $query2->where('p.category_id', '=', $input['catid'])
                        ->orWhereIn('p.id', $pcats);
                });
            } else {
                $query->where('p.category_id', '=', $input['catid']);
            }
            $prods = $query->whereNull('p.deleted_at')
                ->where('p.active','=',1)
                ->orderBy('p.title','asc')
                ->get();
            $html = '<option value="0"> - выберите товар - </option>';
            foreach($prods as $p){
                $html .= '<option value="'.$p->id.'">'.$p->title;
                if($p->article != ''){
                    $html .= ' ('.$p->article.')';
                }
                $html .= ' ['.$p->price.'грн]</option>';
            }
            return response()->json(['success'=>true, 'html' => $html]);
        }
    }


    public function finishing(){

        $products = Product::with('category')->where('finishing',1)->orderBy('sort','asc')->orderBy('id','desc')->get();
        $categories = Category::where('parent_id', 0)->where('active',1)
            ->with('children.children')->orderBy('sort')->get();

        return view('admin.products.finishing', compact('products','categories'));

    }


    public function finishingDelete(Request $request){
        if($request->get('id') > 0){
            DB::table('products')->where('id','=',$request->get('id'))->update(['finishing' => null]);
            return response()->json(['success'=>true, 'id' => $request->get('id')]);
        } else {
            return response()->json(['success'=>true]);
        }
    }


    public function getProductsList(Request $request){
        if($request->get('catid') > 0){
            $catId = $request->get('catid');
            $pcats = [];
            $res = DB::table('product_category')->select()->where('category_id','=',$catId)->get();
            if(isset($res) && count($res) > 0){
                foreach ($res as $r){
                    $pcats[] = $r->product_id;
                }
            }

            $query = DB::table('products')
                ->select('id','title','xml_id','price','quantity');
            if(count($pcats) > 0){
                $query->where(function ($query2) use ($catId,$pcats) {
                    $query2->where('category_id', '=', $catId)
                        ->orWhereIn('id', $pcats);
                });
            } else {
                $query->where('category_id','=',$catId);
            }
            $res = $query->where('active','=',1)
                ->whereNull('deleted_at')
                ->orderBy('title','asc')
                ->get();
            $html = '<option value="0"> - выберите товар - </option>';
            if(count($res) > 0){
                foreach($res as $r){
                    $html .= '<option value="'.$r->id.'">'.$r->title.' ('.$r->xml_id.') '.$r->price.'грн '.$r->quantity.'шт</option>';
                }
            }

            return response()->json(['success'=>true, 'html' => $html]);
        } else {
            return response()->json(['success'=>true]);
        }
    }

    public function finishingAdd(Request $request){
        $input = $request->all();
        if(isset($input['xml_id']) && $input['xml_id'] != ''){
            $prod = DB::table('products')
                ->select('id')
                ->where('active','=',1)
                ->whereNull('deleted_at')
                ->where('xml_id','=',$input['xml_id'])
                ->first();
        } elseif(isset($input['product']) && $input['product'] > 0){
            $prod = DB::table('products')
                ->select('id')
                ->where('active','=',1)
                ->whereNull('deleted_at')
                ->where('id','=',$input['product'])
                ->first();
        }
        if(isset($prod->id) && $prod->id > 0){
            DB::table('products')->where('id','=',$prod->id)->update(['finishing' => 1]);
        }
        return redirect('/admin/products-finishing');
    }

    public function syncWithBilling()
    {
        $service = new ProductService();
        $updated = $service->importProductsFromBilling();
        return redirect('/admin/products')->with('success','Синхронизация завершена! Обновлено товаров: '.$updated);
    }

    public function getBuyWith(Request $request)
    {
        if($request->get('id') > 0 && $request->get('ownid') > 0){
            $prods = Product::where('category_id',$request->get('id'))
                ->where('id','!=',$request->get('ownid'))
                ->active()
                ->orderBy('title_ru','asc')
                ->get();
            $html = '';
            if(!empty($prods)){
                foreach($prods as $prod){
                    $html .= '<li class="list-group-item">';
                    $html .= '<a class="btn btn-success btn-add-buywith btn-sm" href="#" data-id="'.$prod->id.'" role="button"><i class="fas fa-arrow-left"></i></a>';
                    $html .= '<img src="'.$prod->thumbnail.'" class="buywith-pic">';
                    $html .= $prod->title_ru.' ('.$prod->price.'грн)';
                    $html .= '</li>';
                }
                return response()->json(['success' => true, 'html' => $html]);
            }
        }
        return response()->json(['success' => false]);
    }

    public function addToBuyWith(Request $request)
    {
        if($request->get('id') > 0 && $request->get('ownid') > 0){
            $prod = Product::where('id',$request->get('id'))
                ->active()
                ->first();
            if($prod){
                $limit = ProductBuyWith::where('product_id',$request->get('ownid'))->count();
                if($limit < 3){
                    $check = ProductBuyWith::where('product_id',$request->get('ownid'))->where('buywith_id',$prod->id)->first();
                    if(isset($check->id) && $check->id > 0){
                        //exist
                    } else {
                        ProductBuyWith::create([
                            'product_id' => $request->get('ownid'),
                            'buywith_id' => $request->get('id'),
                        ]);
                        $html = '<li class="list-group-item" id="buywith-'.$prod->id.'">';
                        $html .= '<a class="btn btn-danger btn-del-buywith btn-sm" href="#" data-id="'.$prod->id.'" role="button"><i class="fas fa-arrow-right"></i></a>';
                        $html .= '<img src="'.$prod->thumbnail.'" class="buywith-pic">';
                        $html .= $prod->title_ru.' ('.$prod->price.'грн)';
                        $html .= '</li>';
                        return response()->json(['success' => true, 'html' => $html, 'limit' => false]);
                    }
                } else {
                    return response()->json(['success' => true, 'html' => '', 'limit' => true]);
                }
            }
        }
        return response()->json(['success' => false]);
    }

    public function delFromBuyWith(Request $request)
    {
        if($request->get('id') > 0 && $request->get('ownid') > 0){
            $check = ProductBuyWith::where('product_id',$request->get('ownid'))->where('buywith_id',$request->get('id'))->first();
            if(isset($check->id) && $check->id > 0){
                $check->delete();
                return response()->json(['success' => true, 'id' => $request->get('id')]);
            }
        }
        return response()->json(['success' => false]);
    }

}
