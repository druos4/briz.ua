<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePageRequest;
use App\Http\Requests\UpdatePageRequest;
use App\Log;
use App\Page;
use Illuminate\Http\Request;
use App\Services\LogService;

class PagesController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('page_access'), 403);
        if($request->get('reset') == 'y'){
            session(['filtersPages' => []]);
        }
        $filter = [];

        if($request->get('active') != ''){ $filter['active'] = $request->get('active');}
        if($request->get('id') != '') {$filter['id'] = $request->get('id');}
        if($request->get('slug') != '') {$filter['slug'] = $request->get('slug');}
        if($request->get('title') != '') {$filter['title'] = $request->get('title');}
        if($request->get('deleted') != '') {$filter['deleted'] = $request->get('deleted');}

        $query = Page::with('updater');
        if(isset($filter['active'])) {
            if($filter['active'] == 1){
                $query->where('active',1);
            } else {
                $query->where(function ($query2) {
                    $query2->where('active', 0)
                        ->orWhereNull('active');
                });
            }
        }
        if(!empty($filter['id'])) {$query->where('id',$filter['id']);}
        if(!empty($filter['slug'])) {$query->where('slug','LIKE', '%'.$filter['slug'].'%');}
        if(!empty($filter['title'])){
            $query->where(function ($query2) use ($filter) {
                $query2->where('title_ru', 'LIKE', '%'.$filter['title'].'%')
                    ->orWhere('title_ua', 'LIKE', '%'.$filter['title'].'%')
                    ->orWhere('title_en', 'LIKE', '%'.$filter['title'].'%');
            });
        }
        $pages = $query->paginate(30);

        return view('admin.pages.index', compact('pages','filter'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('page_create'), 403);

        return view('admin.pages.create');
    }

    public function store(StorePageRequest $request)
    {
        abort_unless(\Gate::allows('page_create'), 403);
        $request['created_by'] = $request['updated_by'] = auth()->id();
        $request['detail_ru'] = (!empty($request->get('detail_ru'))) ? $request->get('detail_ru') : '';
        $request['detail_ua'] = (!empty($request->get('detail_ua'))) ? $request->get('detail_ua') : '';
        $request['detail_en'] = (!empty($request->get('detail_en'))) ? $request->get('detail_en') : '';
        $request['active'] = 1;
        $page = Page::create($request->all());
        $log = new LogService();
        $log->saveLog('Page',$page);

        return redirect()->route('admin.pages.index')->with('success','Страница создана!');
    }

    public function edit(Page $page)
    {
        abort_unless(\Gate::allows('page_edit'), 403);

        return view('admin.pages.edit', compact( 'page'));
    }

    public function update(UpdatePageRequest $request, Page $page)
    {
        abort_unless(\Gate::allows('page_edit'), 403);
        $request['updated_by'] = auth()->id();
        //$request['active'] = ($request->get('active') == 1) ? 1 : 0;
        $request['active'] = 1;
        $log = new LogService();
        $log->saveLog('Page',$page, $request->all());
        $page->update($request->all());

        return redirect()->route('admin.pages.index')->with('success','Страница обновлена!');
    }

    public function show(Page $page)
    {
        abort_unless(\Gate::allows('page_show'), 403);
        return view('admin.pages.show', compact('page'));
    }

    public function destroy(Page $page)
    {
        abort_unless(\Gate::allows('page_delete'), 403);
        $page->delete();
        return response()->json(['success'=>true, 'id' => $page->id]);
    }

    public function history($id){
        $page = Page::find($id);
        if(!$page){
            return redirect()->route('admin.pages.index')->with('danger','Элемент не найден!');
        }
        $log = new LogService();
        $logs = $log->getLogs('Page',$page);

        return view('admin.pages.history', compact('page','logs'));
    }

    public function clearDeleted()
    {
        Page::whereNotNull('deleted_at')->delete();
        return redirect()->route('admin.pages.index')->with('success','Очищено!');
    }
}
