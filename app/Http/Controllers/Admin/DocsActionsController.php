<?php

namespace App\Http\Controllers\Admin;

use App\DocAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use Illuminate\Http\Request;

class DocsActionsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('tag_access'), 403);

        $docActions = DocAction::orderBy('sort','asc')->paginate(30);

        return view('admin.docactions.index', compact('docActions'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('tag_create'), 403);

        return view('admin.docactions.create');
    }

    public function store(StoreDocumentRequest $request)
    {
        abort_unless(\Gate::allows('tag_create'), 403);
        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['link'] = '';
        $request['active'] = ($request['active'] == 1) ? true : false;
        $docAction = DocAction::create($request->all());

        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/docactions"))) {
                \File::makeDirectory(public_path("uploads/docactions"));
            }
            $f_name = 'docactions-'.$docAction->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/docactions/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $docAction->link = '/uploads/docactions/'.$f_name;
            $docAction->save();
        }

        return redirect()->route('admin.docactions.index')->with('success','Документ создан!');
    }

    public function edit($id)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);
        $docAction = DocAction::find($id);
        return view('admin.docactions.edit', compact( 'docAction'));
    }

    public function update(UpdateDocumentRequest $request, $id)
    {
        abort_unless(\Gate::allows('tag_edit'), 403);
        $docAction = DocAction::find($id);
        $request['sort'] = ($request['sort'] > 0) ? $request['sort'] : 10;
        $request['active'] = ($request['active'] == 1) ? true : false;
        if($request->get('del-pic') == 1){
            if(file_exists(public_path($docAction->link))){
                unlink(public_path($docAction->link));
            }
            $request['link'] = '';
        }
        if ($request->hasFile('file'))
        {
            if (! \File::exists(public_path("uploads/docactions"))) {
                \File::makeDirectory(public_path("uploads/docactions"));
            }
            $f_name = 'docactions-'.$docAction->id.'-'.date('YmdHis').'.'.$request->file('file')->getClientOriginalExtension();
            $path = 'uploads/docactions/'.$f_name;
            move_uploaded_file( $request->file('file'), $path);

            $request['link'] = '/uploads/docactions/'.$f_name;
        }

        $docAction->update($request->all());


        return redirect()->route('admin.docactions.index')->with('success','Документ обновлен!');
    }

    public function show()
    {

    }

    public function destroy($id)
    {
        abort_unless(\Gate::allows('tag_delete'), 403);
        $docAction = DocAction::find($id);
        if($docAction->link != ''){
            if(file_exists(public_path($docAction->link))){
                unlink(public_path($docAction->link));
            }
        }
        $docAction->delete();

        return response()->json(['success'=>true, 'id' => $docAction->id]);
    }
}
