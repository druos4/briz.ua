<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use App\FaqGroup;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFaqRequest;
use App\Http\Requests\UpdateFaqRequest;
use App\Services\FaqService;
use App\Tariff;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FaqsController extends Controller
{
    public function index(Request $request)
    {
        abort_unless(\Gate::allows('faq_access'), 403);

        $faq_groups = FaqGroup::with('children')->orderBy('sort', 'asc')->get();

        return view('admin.faq.index', compact('faq_groups'));
    }

    public function groupCreate()
    {
        abort_unless(\Gate::allows('faq_create'), 403);
        return view('admin.faq.group-create');
    }

    public function groupStore(StoreFaqRequest $request)
    {
        abort_unless(\Gate::allows('faq_create'), 403);
        $request['active'] = ($request->get('active') == 1) ? true : false;
        $request['slug'] = Str::slug(trim($request->get('title_ru')), '-');

        $faq = FaqGroup::create($request->all());
/*
        if ($request->hasFile('image'))
        {
            if (! \File::exists(public_path("uploads/faq"))) {
                \File::makeDirectory(public_path("uploads/faq"));
            }
            $f_name = 'faq-'.$faq->id.'-'.date('YmdHis').'.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make($request->file('image'));
            $path = 'uploads/faq/'.$f_name;
            $img->save(public_path($path), 100);
            $faq->update([
                'icon' => '/uploads/faq/'.$f_name,
            ]);
        }
*/

        if ($request->hasFile('image'))
        {
            if (! \File::exists(public_path("uploads/faq"))) {
                \File::makeDirectory(public_path("uploads/faq"));
            }
            $f_name = 'faq-'.$faq->id.'-'.date('YmdHis').'.'.$request->file('image')->getClientOriginalExtension();
            $path = 'uploads/faq/'.$f_name;
            move_uploaded_file( $request->file('image'), $path);
            $faq->update([
                'icon' => '/uploads/faq/'.$f_name,
            ]);
        }


        return redirect('/admin/faqs')->with('success', 'Группа создана!');
    }

    public function groupEdit($id)
    {
        abort_unless(\Gate::allows('faq_edit'), 403);
        $faq_group = FaqGroup::find($id);
        if (!$faq_group) {
            return redirect('/admin/faqs');
        }

        return view('admin.faq.group-edit', compact('faq_group'));
    }

    public function groupUpdate(UpdateFaqRequest $request, $id)
    {
        abort_unless(\Gate::allows('faq_edit'), 403);
        $faq_group = FaqGroup::find($id);
        if (!$faq_group) {
            return redirect(back());
        }
        $request['active'] = ($request->get('active') == 1) ? true : false;
        $request['slug'] = Str::slug(trim($request->get('title_ru')), '-');

        if($request->get('del-pic') == 1){
            if(file_exists(public_path($faq_group->icon))){
                unlink(public_path($faq_group->icon));
            }
            $request['icon'] = null;
        }
        /*
        if ($request->hasFile('image'))
        {
            if (! \File::exists(public_path("uploads/faq"))) {
                \File::makeDirectory(public_path("uploads/faq"));
            }
            $f_name = 'faq-'.$faq_group->id.'-'.date('YmdHis').'.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make($request->file('image'));
            $path = 'uploads/faq/'.$f_name;
            $img->save(public_path($path), 100);
            $request['icon'] = '/uploads/faq/'.$f_name;
        }
        */
        if ($request->hasFile('image'))
        {
            if (! \File::exists(public_path("uploads/faq"))) {
                \File::makeDirectory(public_path("uploads/faq"));
            }
            $f_name = 'faq-'.$faq_group->id.'-'.date('YmdHis').'.'.$request->file('image')->getClientOriginalExtension();
            $path = 'uploads/faq/'.$f_name;
            move_uploaded_file( $request->file('image'), $path);
            $request['icon'] = '/uploads/faq/'.$f_name;
        }
        $faq_group->update($request->all());
        return redirect('/admin/faqs')->with('success', 'Группа сохранена!');
    }

    public function groupDelete($id)
    {
        abort_unless(\Gate::allows('faq_delete'), 403);
        $faq_group = FaqGroup::find($id);
        if (!$faq_group) {
            return response()->json(['success' => false]);
        }
        Faq::where('faq_group_id', $faq_group->id)->delete();
        $faq_group->delete();
        return response()->json(['success' => true, 'id' => $faq_group->id]);
    }

    public function groupElements($group_id)
    {
        abort_unless(\Gate::allows('faq_access'), 403);
        $faq_group = FaqGroup::find($group_id);
        if (!$faq_group) {
            return redirect(back());
        }
        $faqs = Faq::where('faq_group_id', $faq_group->id)->orderBy('sort', 'asc')->get();

        return view('admin.faq.elements', compact('faq_group', 'faqs'));
    }

    public function elementCreate($group_id)
    {
        abort_unless(\Gate::allows('faq_access'), 403);
        $faq_group = FaqGroup::find($group_id);
        if (!$faq_group) {
            return redirect(back());
        }

        $tariffs = Tariff::active()->orderBy('tariff_type', 'asc')->orderBy('price', 'asc')->get();

        return view('admin.faq.element-create', compact('faq_group', 'tariffs'));
    }

    public function elementStore(StoreFaqRequest $request, $group_id)
    {
        abort_unless(\Gate::allows('faq_create'), 403);
        $request['active'] = ($request->get('active') == 1) ? true : false;
        $request['faq_group_id'] = $group_id;
        $request['slug'] = Str::slug(trim($request->get('title_ua')), '-');
        $faq = Faq::create($request->all());
        return redirect('/admin/faqs/' . $group_id . '/elements')->with('success', 'Вопрос создан!');
    }

    public function elementEdit($group_id, $element_id)
    {
        abort_unless(\Gate::allows('faq_edit'), 403);
        $faq_group = FaqGroup::find($group_id);
        if (!$faq_group) {
            return redirect('/admin/faqs');
        }
        $faq = Faq::find($element_id);
        if (!$faq) {
            return redirect('/admin/faqs');
        }

        $tariffs = Tariff::active()->orderBy('tariff_type', 'asc')->orderBy('price', 'asc')->get();

        return view('admin.faq.element-edit', compact('faq_group', 'faq', 'tariffs'));
    }

    public function elementUpdate(UpdateFaqRequest $request, $group_id, $id)
    {
        abort_unless(\Gate::allows('faq_edit'), 403);
        $faq_group = FaqGroup::find($group_id);
        if (!$faq_group) {
            return redirect(back());
        }
        $faq = Faq::find($id);
        if (!$faq) {
            return redirect(back());
        }
        $request['slug'] = Str::slug(trim($request->get('title_ua')), '-');
        $request['active'] = ($request->get('active') == 1) ? true : false;
        $faq->update($request->all());

        return redirect('/admin/faqs/' . $group_id . '/elements')->with('success', 'Вопрос сохранен!');
    }

    public function elementDelete($group_id, $id)
    {
        abort_unless(\Gate::allows('faq_delete'), 403);
        $faq_group = FaqGroup::find($group_id);
        if (!$faq_group) {
            return response()->json(['success' => false]);
        }
        $faq = Faq::find($id);
        if (!$faq) {
            return response()->json(['success' => false]);
        }
        $faq->delete();
        return response()->json(['success' => true, 'id' => $faq->id]);
    }

    public function test()
    {
        $faq_service = new FaqService();
        $faqs = $faq_service->getFaqForFront();
        echo '<pre>';
        print_r($faqs);
        echo '</pre>';
        die;
    }

}
