<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\NewsService;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    private $service;

    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }

    public function getNews(Request $request)
    {
        $items = $this->service->getNewsForApi($request);
        return response()->json(['items' => $items],200);
    }

    public function getShares(Request $request)
    {
        $items = $this->service->getSharesForApi($request);
        return response()->json(['items' => $items],200);
    }

}
