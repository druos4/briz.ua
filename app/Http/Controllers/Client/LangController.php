<?php

namespace App\Http\Controllers\Client;

use App\Core\Lib\Billing\BillingApi;
use App\Http\Controllers\Controller;
use App\Service;
use App\Services\FeedbackService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class LangController extends Controller
{

    public function switchLang($lang)
    {
        $prev = \URL::previous();
        $arr = parse_url($prev);
        if(!empty($arr['path']) && in_array($arr['path'],['/en','/ru'])) {
            $new_url = '';
        } elseif(!empty($arr['path'])){
            $new_url = str_replace(['/ru/','/en/'],'/',$arr['path']);
        }
        if($lang != 'ua'){
            $new_url = '/'.$lang.$new_url;
        }
        return redirect($new_url);
    }

}
