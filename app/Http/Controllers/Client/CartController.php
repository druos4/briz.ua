<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Cart;
use App\Services\CartService;
use Illuminate\Http\Request;
use DB;

class CartController extends Controller
{

    public function index()
    {

        $cart = new CartService(session()->getId(), auth()->id());
        $result = $cart->getCartItems();
        $items = $result['items'];
        $summ = $result['summ'];
        return response()->json(['success' => true, 'items' => $items, 'summ' => $summ]);

    }

    public function getSmallCart()
    {
        $html = '';
        $summ = 0;

        $cart = new CartService(session()->getId(), auth()->id());
        $result = $cart->getCartItems();
        $summ = $result['summ'];
        if(empty($result['items'])){

            $html = 'Ваша корзина пуста';

        } else {

            $html = view('client.cart.small-cart', ['items' => $result['items'], 'summ' => $summ])->render();

        }


        return response()->json(['success' => true, 'html' => $html, 'summ' => $summ]);
    }

    public function add(Request $request)
    {
        $product = Product::visible()->find($request->get('product_id'));
        if(!$product){
            return response()->json('Can\'t find product', 404);
        }
        if($product->price <= 0){
            return response()->json('Неверная цена товара', 422);
        }
        if($request->get('qnt') <= 0){
            return response()->json('Кол-во товаров должно быть больше нуля', 422);
        }

        $cart = new CartService(session()->getId(), auth()->id());
        $cart_item = $cart->addOrUpdateProduct($product->id);
        if($cart_item !== false){
            $cart_count = $cart->getCartCount();
            return response()->json(['success' => true, 'count' => $cart_count]);
        } else {
            return response()->json(['success' => false]);
        }
    }

    public function getCartCount()
    {
        $cart = new CartService(session()->getId(), auth()->id());
        $cart_count = $cart->getCartCount();
        return response()->json(['success' => true, 'count' => $cart_count]);
    }

    public function delete(Request $request)
    {
        if($request->get('id') > 0){
            $cart = new CartService(session()->getId(), auth()->id());
            $cart->deleteCartItem($request->get('id'));
        }
        $cart_count = $cart->getCartCount();
        $cart_summ = $cart->getCartSumm();

        return response()->json(['success' => true, 'id' => $request->get('id'), 'count' => $cart_count, 'summ' => $cart_summ]);
    }

    public function update(Request $request)
    {
        if($request->get('id') > 0 && $request->get('qnt') > 0){
            $cart = new CartService(session()->getId(), auth()->id());
            $total = $cart->updateCartItem($request->get('id'), $request->get('qnt'));
            if($total !== false){
                $cart_count = $cart->getCartCount();
                $cart_summ = $cart->getCartSumm();

                return response()->json(['success' => true, 'id' => $request->get('id'), 'count' => $cart_count, 'summ' => $cart_summ, 'total' => $total]);
            }
        }
    }

}
