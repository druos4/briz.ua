<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\CallBackRequest;
use App\Http\Requests\FeedbackRequest;
use App\Services\Feedback\Feedback;
use App\Services\FeedbackService;
use Illuminate\Http\Request;
use Inertia\Inertia;

class FeedbackController extends Controller
{

    public function callMe(CallBackRequest $request)
    {
        $service = new Feedback('callMe', false);
        $response = $service->send($request->all());
        return response()->json($this->makeResponse($response));
    }

    public function feedback(FeedbackRequest $request)
    {
        $service = new Feedback('feedback', true);
        $response = $service->send($request->all());
        return response()->json($this->makeResponse($response));
    }

    public function orderService(Request $request)
    {
        $service = new Feedback('orderService', true);
        $response = $service->send($request->all());
        return response()->json($this->makeResponse($response));
    }

    public function connectAction(Request $request)
    {
        $service = new Feedback('connectAction', true);
        $response = $service->send($request->all());
        return response()->json($this->makeResponse($response));
    }

    public function orderDevice(Request $request)
    {
        $service = new Feedback('orderDevice', true);
        $response = $service->send($request->all());
        return response()->json($this->makeResponse($response));
    }

    public function mapConnect(Request $request)
    {
        $service = new FeedbackService();
        $response = $service->sendMapConnect($request, 'mapConnect');
        return response()->json($this->makeResponse($response));
    }

    public function cableRent(Request $request)
    {
        $service = new FeedbackService();
        $response = $service->sendCableRent($request, 'cableRent');
        return response()->json($this->makeResponse($response));
    }

    private function makeResponse($response = [])
    {
        if(isset($response['success']) && $response['success'] === true){
            return [
                'success' => true,
                'sorse' => (!empty($response['sourse'])) ? $response['sourse'] : 'feedback',
            ];
        }
        return [
            'errors' => $response,
        ];
    }

}
