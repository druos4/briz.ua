<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlogCommentRequest;
use App\Services\BlogService;
use App\Services\BreadcrumbsService;
//use App\Services\Markup\Markup;
use App\Services\Markup\Markup;
use App\Services\Markup\MarkupBlog;
use App\Services\Markup\MarkupBreadcrumbs;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BlogController extends Controller
{
    private $service;
    private $markup;

    public function __construct(Markup $markup, BlogService $blogService)
    {
        $this->markup = $markup;
        $this->service = $blogService;
    }

    public function index($params = null)
    {
        $result = $this->service->getBlogsForFront($params);
        $categories = $this->service->getCategoriesListForFront();
        $blogs = (!empty($result['blogs'])) ? $result['blogs'] : [];
        $sort = (!empty($result['sort'])) ? $result['sort'] : [];
        $meta = (!empty($result['meta'])) ? $result['meta'] : [];
        $pagination = (!empty($result['pagination'])) ? $result['pagination'] : [];
        $breads = new BreadcrumbsService('blog');
        $breadcrumbs = $breads->getBreadcrumbs();

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('BlogItems', [
            'categories' => $categories,
            'blogs' => $blogs,
            'sort' => $sort,
            'meta' => $meta,
            'pagination' => $pagination,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function category($slug, $params = null)
    {
        $result = $this->service->getBlogsForFront($params, $slug);
        if(empty($result)){
            storeDeletedUrl(request()->fullUrl());
            return redirect(getLocaleHrefPrefix().'/blog')->setStatusCode(301);
        }
        $categories = $this->service->getCategoriesListForFront($slug);
        $blogs = (!empty($result['blogs'])) ? $result['blogs'] : [];
        $sort = (!empty($result['sort'])) ? $result['sort'] : [];
        $meta = (!empty($result['meta'])) ? $result['meta'] : [];
        $pagination = (!empty($result['pagination'])) ? $result['pagination'] : [];
        $title = (!empty($result['meta']['title'])) ? $result['meta']['title'] : '';
        $breads = new BreadcrumbsService('blog');
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('BlogItems', [
            'categories' => $categories,
            'blogs' => $blogs,
            'sort' => $sort,
            'meta' => $meta,
            'pagination' => $pagination,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function article($slug)
    {
        $categories = $this->service->getCategoriesListForFront($slug);
        $this->service->addViewToBlog($slug);
        $result = $this->service->getBlogDetailForFront($slug);
        if(empty($result)){
            storeDeletedUrl(request()->fullUrl());
            return redirect(getLocaleHrefPrefix().'/blog')->setStatusCode(301);
        }
        $blog = $result['blog'];

        $recomends = $result['recomends'];
        $title = (!empty($blog['title'])) ? $blog['title'] : '';
        $breads = new BreadcrumbsService('blog');
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));
        $this->markup->addMarkup(new MarkupBlog($result['micro']));

        return Inertia::render('BlogArticle', [
            'categories' => $categories,
            'blog' => $blog,
            'recomends' => $recomends,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($blog))->withViewData('markup',$this->markup->render());
    }

    public function setLike(Request $request)
    {
        $result = $this->service->addLikeToBlog($request);
        return response()->json(['success' => $result]);
    }

    public function storeComment(StoreBlogCommentRequest $request)
    {
        $result = $this->service->addCommentToBlog($request);
        return response()->json(['success' => $result]);
    }

    public function getComments(Request $request)
    {
        $result = $this->service->getCommentsForFront($request->all());
        return response()->json([
            'success' => $result
        ]);
    }

}
