<?php

namespace App\Http\Controllers\Client;

use App\DocAction;
use App\Document;
use App\Http\Controllers\Controller;
use App\Payment;
use App\Price;
use App\Quality;
use App\Rule;
use App\Sertificat;
use App\Services\BreadcrumbsService;
use App\Services\ChannelsService;
use App\Services\FaqService;
use App\Services\Markup\MarkupBreadcrumbs;
use App\Services\MenuService;
use App\Services\NewsService;
use App\Services\SliderService;
use App\Services\TariffService;
use Illuminate\Http\Request;
use Inertia\Inertia;
use League\CommonMark\Inline\Element\Emphasis;
use App\Services\Markup\Markup;

class FaqController extends Controller
{
    private $markup;

    public function __construct(Markup $markup)
    {
        $this->markup = $markup;
    }

    public function index()
    {
        $meta = getPageMeta();
        $faq_service = new FaqService();
        $questionsFullList = $faq_service->getQuestionsFullList();
        $items = $faq_service->getFaqGroupsForFront();
        $meta['lastmod'] = $faq_service->getItemsLastmod();

        $title = (!empty($meta['meta-title'])) ? $meta['meta-title'] : '';
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Faq',[
            'items' => $items,
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
            'questionsFullList' => $questionsFullList,
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function groups(Request $request, $slug)
    {
        $faq_service = new FaqService();
        $item = $faq_service->getFaqForFront($slug);
        if(empty($item)){
            return redirect(getLocaleHrefPrefix().'/faq')->setStatusCode(301);
        }
        $questions = $faq_service->getQuestionsForFront($item);
        $questionsFullList = $faq_service->getQuestionsFullList();
        $title = (!empty($item['title'])) ? $item['title'] : '';
        $breads = new BreadcrumbsService('faq');
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('FaqParent',[
            'item' => $item,
            'questions' => $questions,
            'breadcrumbs' => $breadcrumbs,
            'questionsFullList' => $questionsFullList,
        ])->withViewData(prepareMeta($item))->withViewData('markup',$this->markup->render());
    }

    public function items(Request $request, $slug, $itemslug)
    {
        $faq_service = new FaqService();
        $answer = $faq_service->getFaqAnswerForFront($slug,$itemslug);
        if(empty($answer)){
            return redirect(getLocaleHrefPrefix().'/faq')->setStatusCode(301);
        }
        $item = $faq_service->getFaqForFront($slug);
        $questions = $faq_service->getQuestionsForFront($item, $answer);
        $questionsFullList = $faq_service->getQuestionsFullList();
        $breads = new BreadcrumbsService('faq');
        $breadcrumbs = $breads->getBreadcrumbs($answer['title'],$item);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('FaqParent',[
            'item' => $item,
            'answer' => $answer,
            'questions' => $questions,
            'breadcrumbs' => $breadcrumbs,
            'questionsFullList' => $questionsFullList,
        ])->withViewData(prepareMeta($answer))->withViewData('markup',$this->markup->render());
    }

    public function faqSearch(Request $request)
    {
        $faq_service = new FaqService();
        $result = $faq_service->search($request);

        if(!empty($result)){
            return response()->json(['success' => true, 'result' => $result]);
        }
        return response()->json(['success' => false]);
    }

    public function faqSearchFront(Request $request)
    {
        $meta = \Cache::remember(getLocale().'faq-search-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });
        $faq_service = new FaqService();
        $result = $faq_service->searchFull($request);

        $faq_service = new FaqService();
        $questions = $faq_service->getQuestionsForFront();

        $breads = new BreadcrumbsService('faq');
        $breadcrumbs = $breads->getBreadcrumbs(trans('breadcrumbs.search'));

        return Inertia::render('FaqSearch',[
            'search' => $request->get('search'),
            'result' => $result,
            'count' => count($result),
            'meta' => $meta,
            'questions' => $questions,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

}
