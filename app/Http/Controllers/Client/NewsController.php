<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\BreadcrumbsService;
use App\Services\Markup\MarkupBreadcrumbs;
use App\Services\NewsService;
use App\Services\ProductService;
use App\Services\RemontService;
use App\Services\SliderService;
use App\Tag;
use Illuminate\Http\Request;
use App\News;
use DB;
use Inertia\Inertia;
use App\Services\Markup\Markup;

class NewsController extends Controller
{
    private $markup;

    public function __construct(Markup $markup)
    {
        $this->markup = $markup;
    }

    public function index(Request $request)
    {
        if(!empty($request->get('page')) && $request->get('page') > 1){
            $product = false;
            $sliders = false;
        } else {
            $product_service = new ProductService();
            $product = $product_service->getRandomProduct();

            $slider_service = new SliderService();
            $sliders = $slider_service->getItemsForFront();
        }

        $page = ($request->get('page') != '') ? $request->get('page') : 1;
        $result = \Cache::remember(getLocale().'news-items-'.$page, 86400, function () use ($request) {
            $news_service = new NewsService('news');
            $result = $news_service->getItemsForFront($request);
            return $result;
        });
        $news = (!empty($result['items'])) ? $result['items'] : false;
        $has_more = (!empty($result['has_more'])) ? $result['has_more'] : false;

        $meta = \Cache::remember(getLocale().'news-index-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });

        $news_service = new NewsService('news');
        $lastmod = $news_service->getItemsLastmod();
        $meta['lastmod'] = $lastmod;

        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs(trans('breadcrumbs.news'));

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('News', [
            'news' => $news,
            'has_more' => $has_more,
            'sliders' => $sliders,
            'product' => $product,
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }


    public function detail($slug)
    {
        $news = \Cache::remember(getLocale().'news-detail-'.$slug, 86400, function () use ($slug) {
            $news_service = new NewsService('news');
            $news = $news_service->getItemsForFrontBySlug($slug);
            return $news;
        });
        if ($news === false) {
            storeDeletedUrl(\request()->fullUrl());
            return redirect(getLocaleHrefPrefix().'/news')->setStatusCode(301);
        }

        $product_service = new ProductService();
        $product = $product_service->getRandomProduct();

        $sliders = \Cache::remember(getLocale().'sliders', 86400, function () {
            $slider_service = new SliderService();
            $sliders = $slider_service->getItemsForFront();
            return $sliders;
        });

        $breads = new BreadcrumbsService('news');
        $breadcrumbs = $breads->getBreadcrumbs($news['title']);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('NewsDetail', [
            'news' => $news,
            'sliders' => $sliders,
            'product' => $product,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($news))->withViewData('markup',$this->markup->render());
    }

    public function getMoreItems(Request $request)
    {
        $page = ($request->get('page') != '') ? $request->get('page') : 1;
        $result = \Cache::remember(getLocale().'news-get-more-'.$page, 86400, function () use ($request) {
            $news_service = new NewsService('news');
            $result = $news_service->getItemsForFront($request);
            return $result;
        });
        $news = (!empty($result['items'])) ? $result['items'] : false;
        $has_more = (!empty($result['has_more'])) ? $result['has_more'] : false;
        return response()->json(['success' => true, 'news' => $news, 'has_more' => $has_more]);
    }

    public function checkForRemont()
    {
        $remont_service = new RemontService();
        $check = $remont_service->checkForRemont();
        return response()->json(['success' => true, 'remont' => $check]);
    }

    public function remont(Request $request)
    {
        $sliders = \Cache::remember(getLocale().'sliders', 86400, function () {
            $slider_service = new SliderService();
            $sliders = $slider_service->getItemsForFront();
            return $sliders;
        });

        $remont_service = new RemontService();
        $news = $remont_service->getRemonts($request);
        return Inertia::render('NewsDetail', [
            'news' => $news,
            'sliders' => $sliders,
            'product' => [],
        ]);
    }

}
