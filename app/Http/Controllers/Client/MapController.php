<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\MapMarker;
use App\Page;
use App\Services\BreadcrumbsService;
use App\Services\MapService;
use App\Services\Markup\MarkupBreadcrumbs;
use App\Services\NewsService;
use App\Services\ProductService;
use App\Services\SliderService;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Services\Markup\Markup;

class MapController extends Controller
{
    private $markup;

    public function __construct(Markup $markup)
    {
        $this->markup = $markup;
    }

    public function index()
    {
        $meta = \Cache::remember(getLocale().'map-index-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });

        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($meta['meta-title']);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Map', [
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData([
            'meta_title' => (!empty($meta['meta-title'])) ? $meta['meta-title'] : '',
            'meta_description' => (!empty($meta['meta-description'])) ? $meta['meta-description'] : '',
            'meta_keywords' => (!empty($meta['meta-keywords'])) ? $meta['meta-keywords'] : '',
        ])->withViewData('markup',$this->markup->render());
    }

    public function loadMarkers()
    {
        $markers = \Cache::remember(getLocale().'map-load-markers', 86400, function () {
            $service = new MapService();
            $markers = $service->getMarkersForFront();
            return $markers;
        });

        if(!empty($markers)){
            /*if(env('APP_ENV') == 'production'){
                return response()->json([
                    'success' => true,
                    'markers' => $markers,
                ])->withHeaders([
                    'Content-Type' => 'application/json',
                    'Content-Encoding' => 'gzip',
                ]);
            } else {
                return response()->json([
                    'success' => true,
                    'markers' => $markers,
                ]);
            }*/
            return response()->json([
                'success' => true,
                'markers' => $markers,
            ]);
        }
        return response()->json([
            'success' => false,
        ]);
    }

    public function getStreet(Request $request)
    {
        $service = new MapService();
        $result = $service->getAllStreet($request);
        /*if(env('APP_ENV') == 'production'){
            return response()->json(['success' => true, 'result' => $result])->withHeaders([
                'Content-Type' => 'application/json',
                'Content-Encoding' => 'gzip',
            ]);
        } else {
            return response()->json(['success' => true, 'result' => $result]);
        }*/
        return response()->json(['success' => true, 'result' => $result]);
    }

    public function getHouses(Request $request)
    {
            $result = [];
            $street_id = $request->get('street_id');
            if($street_id == 0){
                $service = new MapService();
                $streets = $service->searchStreet($request);
                if(!empty($streets[0])){
                    $street_id = $streets[0]['id'];
                }
            }

            $res = \Cache::remember(getLocale().'map-doms-'.$street_id, 86400, function () use ($street_id) {
                $res = MapMarker::where('street_id', $street_id)->orderBy('addr_sort', 'asc')->orderBy('address_num', 'asc')->get();
                return $res;
            });

            if(!empty($res)){
                foreach($res as $r){
                    $result[] = [
                        'id' => $r->id,
                        'dom' => $r->address_num,
                    ];
                }
                return response()->json(['success' => true, 'result' => $result]);
            }

        return response()->json(['success' => false]);
    }

    public function getAddress(Request $request)
    {
        if(!empty($request->get('street')) && !empty($request->get('dom'))){
            $service = new MapService();
            $result = $service->searchDom($request);
            return response()->json(['success' => true, 'result' => $result]);
        }
        return response()->json(['success' => false]);
    }

    public function getMarker(Request $request)
    {
        if(!empty($request->get('street')) && !empty($request->get('dom'))){
            $service = new MapService();
            $result = $service->searchMarker($request);
            return response()->json(['success' => true, 'result' => $result]);
        }
        return response()->json(['success' => false]);
    }

    public function findByAddress(Request $request)
    {
        $dom = $request->get('num');
        $street = $request->get('address');
        $street = str_replace(' '.$dom,'',$street);
        $street = str_replace(' улица','',$street);
        $street = str_replace(' вулиця','',$street);
        $arr = explode(' ',$street);
        $markers = [];
        if(!empty($arr)){
            foreach($arr as $a){
                $marker = MapMarker::where(function($query) use ($dom) {
                    $query->where('address_num', $dom)
                        ->orWhere('address_num', '*');
                })->where(function($query2) use ($a) {
                    $query2->where('street_ru', $a)
                        ->orWhere('street_ua', $a);
                })->first();
                if(!empty($marker->id)){
                    $markers[] = $marker;
                }

            }
        }
        if(!empty($markers)){
            return response()->json(['success' => true, 'find' => true]);
        } else {
            return response()->json(['success' => true, 'find' => false]);
        }
    }

}
