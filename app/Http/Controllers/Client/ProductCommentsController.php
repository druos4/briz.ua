<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\ProductComment;
use App\Services\FilterService;
use App\Services\ProductCommentService;
use App\Services\ShopService;
use Illuminate\Http\Request;
use DB;
use Inertia\Inertia;
use App\Services\Markup\Markup;

class ProductCommentsController extends Controller
{

    public function createComment(Request $request)
    {
        if($request->get('name') != '' && $request->get('comment') != ''){
            $errors = [];
            if(strlen($request->get('name')) > 256){
                $errors[] = trans('custom.comments.add.name-max');
            }
            if(strlen($request->get('comment')) > 1024){
                $errors[] = trans('custom.comments.add.comment-max');
            }
            if(!empty($errors)){
                return response()->json(['success' => false, 'errors' => $errors]);
            }

            $data = [
                'product_id' => $request->get('product'),
                'parent_id' => ($request->get('parent') > 0) ? $request->get('parent') : null,
                'rate' => ($request->get('rate') > 0) ? $request->get('rate') : null,
                'name' => $request->get('name'),
                'comment' => $request->get('comment'),
            ];
            $comment = ProductComment::create($data);

            $service = new ProductCommentService();
            $service->informToEmails($comment);

            return response()->json(['success' => true]);
        }


        return response()->json(['success' => false]);
    }

    public function getComments(Request $request)
    {
        if($request->get('product') > 0){
            $service = new ProductCommentService();
            $result = $service->getCommentsForFront($request);

            return response()->json(['success' => false]);
        }
        return response()->json(['success' => false]);
    }

}
