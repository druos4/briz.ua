<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\BreadcrumbsService;
use App\Services\HelpService;
use App\Services\Markup\MarkupBreadcrumbs;
use App\Services\NewsService;
use App\Services\ProductService;
use App\Services\RemontService;
use App\Services\SliderService;
use App\Tag;
use Illuminate\Http\Request;
use App\News;
use DB;
use Inertia\Inertia;
use App\Services\Markup\Markup;

class HelpController extends Controller
{
    private $markup;

    public function __construct(Markup $markup)
    {
        $this->markup = $markup;
    }

    public function index()
    {
        $meta = getPageMeta();
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs(trans('breadcrumbs.help'));

        $service = new HelpService();
        $themes = $service->getParentDirectories();

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Help/Help', [
            'themes' => $themes,
            'breadcrumbs' => $breadcrumbs,
            'meta' => $meta,
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function directory($directory_slug)
    {
        $service = new HelpService();
        $help_menu = $service->getMenu();

        $theme = $service->getDirectory($directory_slug);
        if(!$theme){
            storeDeletedUrl(\request()->fullUrl());
            return redirect(getLocaleHrefPrefix().'/help')->setStatusCode(301);
        }

        $breads = new BreadcrumbsService('help');
        $breadcrumbs = $breads->getBreadcrumbs($theme['title'],$theme['parent0'],$theme['parent']);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('HelpDirectory', [
            'menu' => $help_menu,
            'theme' => $theme,
            'breadcrumbs' => $breadcrumbs,
            'meta' => $theme,
        ])->withViewData(prepareMeta($theme))->withViewData('markup',$this->markup->render());
    }

}
