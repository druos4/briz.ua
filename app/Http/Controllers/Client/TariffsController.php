<?php

namespace App\Http\Controllers\Client;

use App\Category;
use App\Http\Controllers\Controller;
use App\Payment;
use App\Service;
use App\Services\BreadcrumbsService;
use App\Services\Markup\MarkupBreadcrumbs;
use App\Services\MenuService;
use App\Services\SliderService;
use App\Services\TariffService;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Services\Markup\Markup;

class TariffsController extends Controller
{
    private $markup;

    public function __construct(Markup $markup)
    {
        $this->markup = $markup;
    }

    public function inetBusiness()
    {
        $service = new TariffService();

        $pppoe = \Cache::remember(getLocale().'inetBusiness-pppoe', 86400, function () use ($service) {
            $pppoe = $service->getItemsForFront('business_pppoe');
            return $pppoe;
        });

        $vlan = \Cache::remember(getLocale().'inetBusiness-vlan', 86400, function () use ($service) {
            $vlan = $service->getItemsForFront('business_vlan');
            return $vlan;
        });

        $iptv = \Cache::remember(getLocale().'inetBusiness-iptv', 86400, function () use ($service) {
            $iptv = $service->getItemsForFront('business_iptv');
            return $iptv;
        });

        $page = \Cache::remember(getLocale().'inetBusiness-page', 86400, function () use ($service) {
            $page = $service->getPageForFront('internet-dlya-biznesa');
            return $page;
        });

        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($page['meta_title']);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Tariffs/InetBusiness', [
            'page' => $page,
            'pppoe' => $pppoe,
            'vlan' => $vlan,
            'iptv' => $iptv,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($page))->withViewData('markup',$this->markup->render());
    }

    public function tv()
    {
        $service = new TariffService('tv');
        //$tariffs = \Cache::remember(getLocale().'tv-tariffs', 86400, function () use ($service) {
        $tariffs = $service->getItemsForFront();
           // return $tariffs;
        //});



        $sliders = \Cache::remember(getLocale().'sliders', 86400, function () {
            $slider_service = new SliderService();
            $sliders = $slider_service->getItemsForFront();
            return $sliders;
        });

        $page = \Cache::remember(getLocale().'tv-page', 86400, function () use ($service) {
            $page = $service->getPageForFront('televidenie');
            return $page;
        });

        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($page['meta_title']);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Tariffs/Tv', [
            'sliders' => $sliders,
            'page' => $page,
            'tariffs' => $tariffs,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($page))->withViewData('markup',$this->markup->render());
    }

    public function inetHome()
    {
        $service = new TariffService('internet');
        $tariffs = \Cache::remember(getLocale().'inetHome-tariffs', 86400, function () use ($service) {
            $tariffs = $service->getItemsForFront('home');
            return $tariffs;
        });

        $daily = \Cache::remember(getLocale().'inetHome-daily', 86400, function () use ($service) {
            $daily = $service->getItemsForFront('daily');
            return $daily;
        });

        $page = \Cache::remember(getLocale().'inetHome-page', 86400, function () use ($service) {
            $page = $service->getPageForFront('internet-home');
            return $page;
        });

        $sliders = \Cache::remember('sliders', 86400, function () {
            $slider_service = new SliderService();
            $sliders = $slider_service->getItemsForFront();
            return $sliders;
        });

        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($page['meta_title']);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Tariffs/InetHome', [
            'tariffs' => $tariffs,
            'daily' => $daily,
            'sliders' => $sliders,
            'page' => $page,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($page))->withViewData('markup',$this->markup->render());
    }

    public function tvInet()
    {
        $service = new TariffService('union');
        $tv = \Cache::remember(getLocale().'tvInet-tariffs', 86400, function () use ($service) {
            $tv = $service->getItemsForFront('inet+tv');
            return $tv;
        });

        $iptv = \Cache::remember(getLocale().'tvInet-iptv', 86400, function () use ($service) {
            $iptv = $service->getItemsForFront('inet+iptv');
            return $iptv;
        });

        $tvmax = \Cache::remember(getLocale().'tvInet-tvmax', 86400, function () use ($service) {
            $tvmax = $service->getItemsForFront('inet+tvmax');
            return $tvmax;
        });

        $briztv = \Cache::remember(getLocale().'tvInet-briztv', 86400, function () use ($service) {
            $briztv = $service->getItemsForFront('inet+briztv');
            return $briztv;
        });

        $page = \Cache::remember(getLocale().'tvInet-page', 86400, function () use ($service) {
            $page = $service->getPageForFront('televidenie-i-internet');
            return $page;
        });

        $sliders = \Cache::remember(getLocale().'sliders', 86400, function () {
            $slider_service = new SliderService();
            $sliders = $slider_service->getItemsForFront();
            return $sliders;
        });

        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($page['meta_title']);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Tariffs/TvInet', [
            'page' => $page,
            'tv' => $tv,
            'iptv' => $iptv,
            'tvmax' => $tvmax,
            'briztv' => $briztv,
            'sliders' => $sliders,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($page))->withViewData('markup',$this->markup->render());
    }

}
