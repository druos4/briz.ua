<?php

namespace App\Http\Controllers\Client;

use App\DocAction;
use App\Document;
use App\Http\Controllers\Controller;
use App\Payment;
use App\Price;
use App\Quality;
use App\Rule;
use App\Sertificat;
use App\Services\BreadcrumbsService;
use App\Services\ChannelsService;
use App\Services\FaqService;
use App\Services\Markup\MarkupBreadcrumbs;
use App\Services\MenuService;
use App\Services\NewsService;
use App\Services\SiteMapService;
use App\Services\SliderService;
use App\Services\TariffService;
use Carbon\Carbon;
use Illuminate\Filesystem\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Browser;
use App\Services\Markup\Markup;

class MainController extends Controller
{
    private $markup;

    public function __construct(Markup $markup)
    {
        $this->markup = $markup;
    }

    public function index()
    {
        $meta = \Cache::remember(getLocale().'main-page-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });

        $slider_service = new SliderService();
        $sliders = $slider_service->getItemsForFront();

        $services = new TariffService();
        $tarifs = $services->getTarifsForIndexPage();

        $news_service = new NewsService();
        $news = $news_service->getLast3ForMainPage();

        $meta['lastmod'] = Carbon::now();

        return Inertia::render('Index', [
            'sliders' => $sliders,
            'tarifs' => $tarifs,
            'news' => $news,
            'meta' => $meta,
        ])->withViewData(prepareMeta($meta));
    }

    public function error500()
    {
        $a = geta();
    }

    public function contacts()
    {
        $meta = \Cache::remember(getLocale().'contacts-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });

        $title = (!empty($meta['meta-title'])) ? $meta['meta-title'] : '';
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Contacts', [
            'captcha' => captcha_img(),
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function ott()
    {
        $meta = \Cache::remember(getLocale().'contacts-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });
        $meta['nofollow'] = true;

        $title = (!empty($meta['meta-title'])) ? $meta['meta-title'] : '';
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Ott', [
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function politicaBriztv()
    {
        $meta = \Cache::remember(getLocale().'politicaBriztv-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });
        return Inertia::render('Politica', [
            'meta' => $meta,
        ]);
    }

    public function usloviyaIPravila()
    {
        $meta = \Cache::remember(getLocale().'usloviyaIPravila-meta', 86400, function () {
            $meta = getPageMeta(true);
            return $meta;
        });
        return Inertia::render('Usloviya', [
            'meta' => $meta,
        ]);
    }

    public function payment()
    {
        $meta = \Cache::remember(getLocale().'payment-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });

        $payments = \Cache::remember(getLocale().'payments', 86400, function () {
            $payments = [];
            $res = Payment::active()->orderBy('sort', 'asc')->get();
            if (!empty($res)) {
                foreach ($res as $k => $r) {
                    $payments[] = [
                        'title' => $r->{'title' . getLocaleDBSuf()},
                        'commission' => $r->commission,
                        'icon' => $r->icon,
                        'icon_active' => $r->icon_active,
                        'description' => addNofollowToLink($r->{'description' . getLocaleDBSuf()}),
                    ];
                }
            }
            return $payments;
        });

        $title = (!empty($meta['meta-title'])) ? $meta['meta-title'] : '';
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Payment', [
            'payments' => $payments,
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function documents()
    {
        $meta = \Cache::remember(getLocale().'documents-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });

        $documents = \Cache::remember(getLocale().'documents', 86400, function () {
            $documents = [];
            $res = Document::active()->orderBy('sort','asc')->get();
            if(!empty($res)){
                foreach($res as $r){
                    $documents[] = [
                        'title' => $r->{'title' . getLocaleDBSuf()},
                        'link' => $r->link,
                    ];
                }
            }
            return $documents;
        });

        $prices = \Cache::remember(getLocale().'prices', 86400, function () {
            $prices = [];
            $res = Price::active()->orderBy('sort','asc')->get();
            if(!empty($res)){
                foreach($res as $r){
                    $prices[] = [
                        'title' => $r->{'title' . getLocaleDBSuf()},
                        'link' => $r->link,
                    ];
                }
            }
            return $prices;
        });

        $docactions = \Cache::remember(getLocale().'docactions', 86400, function () {
            $docactions = [];
            $res = DocAction::active()->orderBy('sort','asc')->get();
            if(!empty($res)){
                foreach($res as $r){
                    $docactions[] = [
                        'title' => $r->{'title' . getLocaleDBSuf()},
                        'link' => $r->link,
                    ];
                }
            }
            return $docactions;
        });

        $qualities = \Cache::remember(getLocale().'qualities', 86400, function () {
            $qualities = [];
            $res = Quality::active()->orderBy('sort','asc')->get();
            if(!empty($res)){
                foreach($res as $r){
                    $qualities[] = [
                        'title' => $r->{'title' . getLocaleDBSuf()},
                        'link' => $r->link,
                    ];
                }
            }
            return $qualities;
        });

        $sertificats = \Cache::remember(getLocale().'sertificats', 86400, function () {
            $sertificats = [];
            $res = Sertificat::active()->orderBy('sort','asc')->get();
            if(!empty($res)){
                foreach($res as $r){
                    $sertificats[] = [
                        'title' => $r->{'title' . getLocaleDBSuf()},
                        'link' => $r->link,
                    ];
                }
            }
            return $sertificats;
        });

        $title = (!empty($meta['meta-title'])) ? $meta['meta-title'] : '';
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Documents', [
            'documents' => $documents,
            'docactions' => $docactions,
            'sertificats' => $sertificats,
            'qualities' => $qualities,
            'prices' => $prices,
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function getHeaderMenu()
    {
        $menu = \Cache::remember(getLocale().'main_menu', 86400, function () {
            $service = new MenuService('main_menu');
            $menu = $service->getMenuForFront();
            return $menu;
        });
        return response()->json(['success' => true, 'menu' => $menu]);
    }

    public function getShopMenu()
    {
        $menu = \Cache::remember(getLocale().'shop_menu', 86400, function () {
            $service = new MenuService('shop_menu');
            $menu = $service->getMenuForFront();
            return $menu;
        });
        return response()->json(['success' => true, 'menu' => $menu]);
    }

    public function channels($id)
    {
        $meta = \Cache::remember(getLocale().'channels-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });

        $tariff = \Cache::remember(getLocale().'channels-tariff-'.$id, 86400, function () use ($id) {
            $service_tariff = new TariffService('tv');
            $tariff = $service_tariff->getItemForFront($id);
            return $tariff;
        });
        $channels = \Cache::remember(getLocale().'channels-items-'.$id, 86400, function () use ($id) {
            $service = new ChannelsService();
            $channels = $service->getForFront($id);
            return $channels;
        });
        $count = \Cache::remember(getLocale().'channels-items-count-'.$id, 86400, function () use ($id) {
            $service = new ChannelsService();
            $count = $service->countChannelsInTariff($id);
            return $count;
        });

        return Inertia::render('Channels',[
            'channels' => $channels,
            'count' => $count,
            'tariff' => $tariff,
            'meta' => $meta,
        ]);
    }



    public function sliderTest($id)
    {
        if(isAdmin()){
            $slider_service = new SliderService();
            $sliders = $slider_service->getTestSlider($id);
            return Inertia::render('SliderTest', [
                'sliders' => $sliders,
            ]);
        } else {
            return Inertia::render('404');
        }
    }

    public function sitemap()
    {
        $sitemap = \Cache::remember(getLocale().'sitemap', 86400, function () {
            $service = new SiteMapService(app()->getLocale());
            $sitemap = $service->getPublicSiteMap();
            return $sitemap;
        });

        $meta = \Cache::remember(getLocale().'channels-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });

        return Inertia::render('Sitemap',[
            'sitemap' => $sitemap,
            'meta' => $meta,
        ]);
    }

    public function ieBrowser()
    {
//        $language = \App::getLocale();

       if(Browser::isMobile() || Browser::isTablet()){
                   if(Browser::isAndroid()){
                       $chrome = 'https://apps.apple.com/ua/app/chrome-%D0%B1%D1%80%D0%B0%D1%83%D0%B7%D0%B5%D1%80-%D0%BE%D1%82-google/id535886823?l=ru';
                       $mozila = 'https://apps.apple.com/ua/app/%D0%B2%D0%B5%D0%B1-%D0%B1%D1%80%D0%B0%D1%83%D0%B7%D0%B5%D1%80-firefox/id989804926?l=ru';
                       $edge = 'https://apps.apple.com/ua/app/microsoft-edge-web-browser/id1288723196?l=ru';
                       $opera = 'https://apps.apple.com/ua/app/opera-%D1%81%D0%BA%D0%BE%D1%80%D0%BE%D1%81%D1%82%D1%8C-%D0%B8-%D0%B1%D0%B5%D0%B7%D0%BE%D0%BF%D0%B0%D1%81%D0%BD%D0%BE%D1%81%D1%82%D1%8C/id1411869974?l=ru';
                   } else {
                       $chrome = 'https://play.google.com/store/apps/details?id=com.android.chrome&hl=ru&gl=US';
                       $mozila = 'https://play.google.com/store/apps/details?id=org.mozilla.firefox&hl=ru&gl=US';
                       $edge = 'https://play.google.com/store/apps/details?id=com.microsoft.emmx&hl=ru&gl=US';
                       $opera = 'https://play.google.com/store/apps/details?id=com.opera.browser&hl=ru&gl=US';
                   }
               } else {
                   $chrome = "https://www.google.ru/intl/ru/chrome/";
                   $mozila = 'https://www.mozilla.org/ru/';
                   $edge = 'https://www.microsoft.com/uk-ua/edge';
                   $opera = 'https://www.opera.com/ru';
               }
               return view('errors.ie', compact('chrome','mozila','edge','opera'));
    }
}
