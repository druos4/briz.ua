<?php

namespace App\Http\Controllers\Client;

use App\Email;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEmailRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmailsController extends Controller
{
    public function addEmail(StoreEmailRequest $request)
    {
        $minuteBefore = Carbon::now()->subMinute();
        $last = Email::where('session_id',session()->getId())
            ->where('created_at','>=',$minuteBefore)
            ->orderBy('created_at','desc')
            ->first();
        if($last){
            $delta = Carbon::createFromDate($last->created_at)->diffInSeconds($minuteBefore);
            return response()->json([
                'success' => false,
                'errors' => trans('custom.emails.validation.less-1-minute').' '.$delta.' '.trans('custom.emails.validation.sec'),
            ]);
        }

        Email::create([
            'email' => $request->get('email'),
            'session_id' => session()->getId(),
        ]);

        return response()->json([
            'success' => true,
        ]);
    }

}
