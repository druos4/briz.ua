<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Services\BreadcrumbsService;
use App\Services\Markup\MarkupBreadcrumbs;
use App\Services\NewsService;
use App\Services\ProductService;
use App\Services\SliderService;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Services\Markup\Markup;

class ActionsController extends Controller
{
    private $markup;

    public function __construct(Markup $markup)
    {
        $this->markup = $markup;
    }

    public function index(Request $request)
    {
        if(!empty($request->get('page')) && $request->get('page') > 1){
            $product = false;
            $sliders = false;
        } else {
            $product_service = new ProductService();
            $product = $product_service->getRandomProduct();
            $sliders = \Cache::remember(getLocale().'sliders', 86400, function () {
                $slider_service = new SliderService();
                $sliders = $slider_service->getItemsForFront();
                return $sliders;
            });
        }

        $page = ($request->get('page') != '') ? $request->get('page') : 1;
        $result = \Cache::remember(getLocale().'actions-index-result-'.$page, 86400, function () use ($request) {
            $news_service = new NewsService('actions');
            $result = $news_service->getItemsForFront($request);
            return $result;
        });
        $actions = (!empty($result['items'])) ? $result['items'] : false;
        $has_more = (!empty($result['has_more'])) ? $result['has_more'] : false;

        $meta = \Cache::remember(getLocale().'actions-index-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });

        $news_service = new NewsService('actions');
        $lastmod = $news_service->getItemsLastmod();
        $meta['lastmod'] = $lastmod;

        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs(trans('breadcrumbs.actions'));

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Actions', [
            'actions' => $actions,
            'has_more' => $has_more,
            'sliders' => $sliders,
            'product' => $product,
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function getMoreItems(Request $request)
    {
        $page = ($request->get('page') != '') ? $request->get('page') : 1;
        $result = \Cache::remember(getLocale().'actions-get-more-'.$page, 86400, function () use ($request) {
            $news_service = new NewsService('actions');
            $result = $news_service->getItemsForFront($request);
            return $result;
        });
        $actions = (!empty($result['items'])) ? $result['items'] : false;
        $has_more = (!empty($result['has_more'])) ? $result['has_more'] : false;
        return response()->json(['success' => true, 'actions' => $actions, 'has_more' => $has_more]);
    }


    public function detail($slug)
    {
        //$action = \Cache::remember(getLocale().'action-detail-'.$slug, 86400, function () use ($slug) {
            $news_service = new NewsService('actions');
            $action = $news_service->getItemsForFrontBySlug($slug);
          //  return $action;
       // });
        if($action === false){
            storeDeletedUrl(\request()->fullUrl());
            return redirect(getLocaleHrefPrefix().'/actions')->setStatusCode(301);
        }

        $product_service = new ProductService();
        $product = $product_service->getRandomProduct();

        $sliders = \Cache::remember(getLocale().'sliders', 86400, function () use ($slug) {
            $slider_service = new SliderService();
            $sliders = $slider_service->getItemsForFront();
            return $sliders;
        });

        $breads = new BreadcrumbsService('actions');
        $breadcrumbs = $breads->getBreadcrumbs($action['title']);
        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));
        return Inertia::render('ActionDetail', [
            'action' => $action,
            'sliders' => $sliders,
            'product' => $product,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData(prepareMeta($action))->withViewData('markup',$this->markup->render());
    }


}
