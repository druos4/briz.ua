<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\Category;
use App\Cart;
use App\Services\CartService;
use App\User;
use Illuminate\Http\Request;
use DB;

class CabinetController extends Controller
{

    public function index()
    {
        $user = User::find(auth()->id());
        if(!$user){
            return redirect(getLocaleHrefPrefix().'/');
        }

        $breadcrumbs = [
            ['title' => 'Личный кабинет', 'url' => '']
        ];

        return view('client.cabinet.index',compact('breadcrumbs','user'));
    }
    public function profile()
    {

    }
    public function profileSave()
    {

    }
    public function orders()
    {
        $user = User::find(auth()->id());
        if(!$user){
            return redirect(getLocaleHrefPrefix().'/');
        }

        $orders = Order::where('user_id',$user->id)->orderBy('id','desc')->paginate(10);

        $breadcrumbs = [
            ['title' => 'Личный кабинет', 'url' => getLocaleHrefPrefix().'/cabinet'],
            ['title' => 'Мои заказы', 'url' => ''],
        ];

        return view('client.cabinet.orders',compact('breadcrumbs','user','orders'));
    }
    public function orderDetail($order_id)
    {
        $user = User::find(auth()->id());
        if(!$user){
            return redirect(getLocaleHrefPrefix().'/cabinet/orders');
        }

        $order = Order::with('carts','payment','shipment')->where('user_id',$user->id)->where('id',$order_id)->first();
        if(!$user){
            return redirect(getLocaleHrefPrefix().'/cabinet/orders');
        }

        $breadcrumbs = [
            ['title' => 'Личный кабинет', 'url' => getLocaleHrefPrefix().'/cabinet'],
            ['title' => 'Мои заказаы', 'url' => getLocaleHrefPrefix().'/cabinet/orders'],
            ['title' => 'Заказ подробно', 'url' => ''],
        ];

        return view('client.cabinet.order-detail',compact('breadcrumbs','user','order'));
    }

}
