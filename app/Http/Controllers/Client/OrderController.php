<?php

namespace App\Http\Controllers\Client;

use App\Core\Lib\Billing\BillingApi;
use App\Feedback;
use App\Http\Controllers\Controller;
use App\PaymentMethod;
use App\Product;
use App\Category;
use App\Cart;
use App\Order;
use App\Services\CartService;
use App\ShipmentMethod;
use App\User;
use Illuminate\Http\Request;
use DB;

class OrderController extends Controller
{

    public function make()
    {

        $cart = new CartService(session()->getId(), auth()->id());
        $result = $cart->getCartItems();
        $items = $result['items'];
        $summ = $result['summ'];

        $payments = PaymentMethod::visible()->orderBy('sort','asc')->get();
        $shipments = ShipmentMethod::visible()->orderBy('sort','asc')->get();

        if(auth()->id() > 0){
            $user = User::find(auth()->id());
        } else {
            $user = false;
        }

        $breadcrumbs = [
            ['title' => 'Оформление заказа', 'url' => '']
        ];

        return view('client.order.make',compact('breadcrumbs','items','summ','payments','shipments','user'));
    }

    public function createOrder(Request $request)
    {
        $cart = new CartService(session()->getId(), auth()->id());
        $result = $cart->getCartItems();
        $items = $result['items'];
        $summ = $result['summ'];

        if(!empty($items)){

            $data = [
                'user_id' => auth()->id(),
                'total' => $summ,
                'status_id' => 'N',
                'payment_id' => null,
                'shipment_id' => null,
                'email' => null,
                'first_name' => $request->get('name'),
                'last_name' => null,
                'middle_name' => null,
                'phone' => $request->get('phone'),
                'comments' => $request->get('comment'),
                'delivery_address' => $request->get('address'),
                'session_id' => session()->getId(),
            ];
            $order = Order::create($data);
            $cart->syncCartWithOrder($order->id);

            $phone = clearPhoneInput($data["phone"]);
            if(strlen($phone) == 12){
                $phone = substr($phone,2,10);
            }
            $comment = 'Заказ оборудования через магазин
Клиент: '.$data["first_name"].'
Телефон: '.$phone.'
Адрес: '.$data["delivery_address"].'
Комментарий: '.$data["comments"].'

Товары:
';
            if(!empty($items)){
                foreach($items as $item){
                    $comment .= $item->product->title_ru.', цена '.$item->price.'грн, кол-во '.$item->qnt.', сумма '.$item->total.'грн
';
                }
            }
            $comment .= '
Общая сумма '.$summ.'грн';
            $data_send = [
                'Time' => time(),
                //'ToTime' => 'date_format:U',
                'Dom' => 127,
                'Flat' => 1,
                'StreetID' => 147,
                'Comment' => $comment,
                'Phone' => $phone,
                'Name' => $data['first_name'],
                'IP' => '127.0.0.1',
                'skipAstra' => true,
            ];

            try {
                $billingAPI = BillingApi::get();
                $response = $billingAPI->execute('send-feedback', $this->getData());

                Feedback::create([
                    'session_id' => session()->getId(),
                    'phone' => $request->get('phone')
                ]);
            } catch (ApiException $e) {
            }
            if(isset($response['astra']) && $response['astra'] == true){
                return ['success' => true, 'sourse' => 'astra'];
            }

            return response()->json(['success' => true, 'order_id' => $order->id]);
        }
        return response()->json(['success' => false]);

    }

    public function done($order_id)
    {
        $order = Order::where('id',$order_id)->where('user_id',auth()->id())->where('session_id',session()->getId())->first();
        if(!$order){
            return redirect(getLocaleHrefPrefix().'/shop');
        }
        return view('client.order.done', compact('order_id'));
    }



    public function update(Request $request)
    {
        if($request->get('id') > 0 && $request->get('qnt') > 0){
            $cart = new CartService(session()->getId(), auth()->id());
            $total = $cart->updateCartItem($request->get('id'), $request->get('qnt'));
            if($total !== false){
                $cart_count = $cart->getCartCount();
                $cart_summ = $cart->getCartSumm();

                return response()->json(['success' => true, 'id' => $request->get('id'), 'count' => $cart_count, 'summ' => $cart_summ, 'total' => $total]);
            }
        }
    }

}
