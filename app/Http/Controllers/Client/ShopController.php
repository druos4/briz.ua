<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\ProductComment;
use App\Services\BreadcrumbsService;
use App\Services\FilterService;
use App\Services\Markup\MarkupBreadcrumbs;
use App\Services\ProductCommentService;
use App\Services\ProductService;
use App\Services\ShopService;
use App\Services\SliderService;
use Illuminate\Http\Request;
use DB;
use Inertia\Inertia;
use App\Services\Markup\Markup;

class ShopController extends Controller
{
    private $markup;

    public function __construct(Markup $markup)
    {
        $this->markup = $markup;
    }

    public function index(Request $request, $category_slug = 'all')
    {
        /*
        $slider_service = new SliderService();
        $sliders = $slider_service->getItemsForShopFront();
        */
        $sliders = [];

        $title = trans('custom.equipment.title');
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($title);
        $service = new ShopService($request->all(),$category_slug);
        $categories = $service->getAllData();
        $buttons = $service->getButtons();
        $meta = getPageMeta();
        $result = $service->getProducts();
        $meta['lastmod'] = $service->getItemsLastmod();

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('ShopCategory', [
            'sliders' => $sliders,
            'breadcrumbs' => $breadcrumbs,
            'categories' => $categories,
            'buttons' => $buttons,
            'meta' => $meta,
            'products' => $result['products'],
            'has_more' => $result['has_more'],
            'page' => $result['page'],
            'next_page' => $result['next_page'],
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function category(Request $request, $category_slug)
    {
        $sliders = [];

        $service = new ShopService($request->all(),$category_slug);
        $exist = $service->categoryExist();
        if(!$exist){
            return Inertia::render('404');
        }
        $meta = $service->getCategoryMeta();
        $breads = new BreadcrumbsService('equipment');
        $breadcrumbs = $breads->getBreadcrumbs($meta['title']);
        $categories = $service->getAllData();
        $buttons = $service->getButtons();
        $result = $service->getProducts();
        $meta['lastmod'] = $service->getItemsLastmod();

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('ShopCategory', [
            'sliders' => $sliders,
            'breadcrumbs' => $breadcrumbs,
            'categories' => $categories,
            'buttons' => $buttons,
            'meta' => $meta,
            'products' => $result['products'],
            'has_more' => $result['has_more'],
            'page' => $result['page'],
            'next_page' => $result['next_page'],
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

   public function categoryTop(Request $request)
    {
        $sliders = [];

        $service = new ShopService($request->all(),'top');
        $meta = \Cache::remember(getLocale().'products-top', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });
        $breads = new BreadcrumbsService('equipment');
        $breadcrumbs = $breads->getBreadcrumbs($meta['title']);

        $categories = $service->getAllData();
        $buttons = $service->getButtons();

        $result = $service->getProducts();
        $meta['lastmod'] = $service->getItemsLastmod();

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('ShopCategory', [
            'sliders' => $sliders,
            'breadcrumbs' => $breadcrumbs,
            'categories' => $categories,
            'buttons' => $buttons,
            'meta' => $meta,
            'products' => $result['products'],
            'has_more' => $result['has_more'],
            'page' => $result['page'],
            'next_page' => $result['next_page'],
        ])->withViewData(prepareMeta($meta))->withViewData('markup',$this->markup->render());
    }

    public function getCategory(Request $request, $category_slug)
    {
        $service = new ShopService();
        $result = $service->getCategory($category_slug, $request);
        $items = $result['items'];
        $has_more = $result['has_more'];
        return response()->json(['success' => true, 'products' => $items,'has_more_items' => $has_more]);
    }

    public function filter(Request $request)
    {
        $html = '';
        $filter = new FilterService($request->get('category_id'));



        $result = $filter->getFilteredProducts($request->all());
        $products = $result['products'];
        $has_more = $result['has_more'];
        $new_page = $result['page'];
        $counter_new = $result['counter_new'];
        $counter_self = $result['counter_self'];

        if(!empty($products)){
            foreach($products as $product){
                $html .= view('client.shop.category-product-item',compact('product'))->render();
            }
        }

        return response()->json([
            'success' => true,
            'html' => $html,
            'has_more' => $has_more,
            'new_page' => $new_page,
            'counter_new' => $counter_new,
            'counter_self' => $counter_self
        ]);
    }

    public function product($product_slug)
    {
        $service = new ShopService();
        $product = $service->getProductForFront($product_slug);
        if(!$product){
            storeDeletedUrl(request()->fullUrl());
            return redirect(getLocaleHrefPrefix().'/equipment')->setStatusCode(301);
            //return Inertia::render('404');
        }
        $buywith = $service->getProductBuyWithForFront($product);
        $product_properties = $service->getProductProperties($product);
        //$comments_count = ProductComment::where('product_id',$product['id'])->moderated()->count();

        $sublevel = [
            'title' => $product['category_title'],
            'url' => '/equipment/'.$product['category_slug'],
        ];
        $title = $product['title'];
        $breads = new BreadcrumbsService('equipment');
        $breadcrumbs = $breads->getBreadcrumbs($title,$sublevel);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('ShopProduct', [
            'product' => $product,
            'properties' => $product_properties,
            'breadcrumbs' => $breadcrumbs,
            'buywith' => $buywith,
            //'comments_count' => $comments_count,
        ])->withViewData(prepareMeta($product))->withViewData('markup',$this->markup->render());
    }

}
