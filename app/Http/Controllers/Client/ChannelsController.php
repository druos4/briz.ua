<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Payment;
use App\Services\BreadcrumbsService;
use App\Services\ChannelsService;
use App\Services\FaqService;
use App\Services\Markup\MarkupBreadcrumbs;
use App\Services\MenuService;
use App\Services\NewsService;
use App\Services\SliderService;
use App\Services\TariffService;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Services\Markup\Markup;

class ChannelsController extends Controller
{
    private $markup;

    public function __construct(Markup $markup)
    {
        $this->markup = $markup;
    }

    public function channel41()
    {
        $meta = \Cache::remember(getLocale().'channel-41-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });
        $id = 41;

        $tariff = \Cache::remember(getLocale().'channel-tariff-'.$id, 86400, function () use ($id) {
            $service_tariff = new TariffService('tv');
            $tariff = $service_tariff->getItemForFront($id);
            return $tariff;
        });

        $channels = \Cache::remember(getLocale().'channel-items-'.$id, 86400, function () use ($id) {
            $service = new ChannelsService();
            $service->setBaseUrl('/spisok-kanalov-paket-bazovyiy-iptv');
            $channels = $service->getForFront($id);
            return $channels;
        });

        $count = \Cache::remember(getLocale().'channel-items-count-'.$id, 86400, function () use ($id) {
            $service = new ChannelsService();
            $count = $service->countChannelsInTariff($id);
            return $count;
        });

        $title = (!empty($meta['meta-title'])) ? $meta['meta-title'] : '';
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Channels',[
            'channels' => $channels,
            'count' => $count,
            'tariff' => $tariff,
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData([
            'meta_title' => (!empty($meta['meta-title'])) ? $meta['meta-title'] : '',
            'meta_description' => (!empty($meta['meta-description'])) ? $meta['meta-description'] : '',
            'meta_keywords' => (!empty($meta['meta-keywords'])) ? $meta['meta-keywords'] : '',
            'lastmod' => date('Y-m-d').' 06:00:00',
        ])->withViewData('markup',$this->markup->render());
    }

    public function channel42()
    {
        $meta = \Cache::remember(getLocale().'channel-42-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });
        $id = 42;

        $tariff = \Cache::remember(getLocale().'channel-tariff-'.$id, 86400, function () use ($id) {
            $service_tariff = new TariffService('tv');
            $tariff = $service_tariff->getItemForFront($id);
            return $tariff;
        });

        $channels = \Cache::remember(getLocale().'channel-items-'.$id, 86400, function () use ($id) {
            $service = new ChannelsService();
            $service->setBaseUrl('/spisok-kanalov-paket-rasshirennyiy-iptv');
            $channels = $service->getForFront($id);
            return $channels;
        });

        $count = \Cache::remember(getLocale().'channel-items-count-'.$id, 86400, function () use ($id) {
            $service = new ChannelsService();
            $count = $service->countChannelsInTariff($id);
            return $count;
        });

        $title = (!empty($meta['meta-title'])) ? $meta['meta-title'] : '';
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Channels',[
            'channels' => $channels,
            'count' => $count,
            'tariff' => $tariff,
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData([
            'meta_title' => (!empty($meta['meta-title'])) ? $meta['meta-title'] : '',
            'meta_description' => (!empty($meta['meta-description'])) ? $meta['meta-description'] : '',
            'meta_keywords' => (!empty($meta['meta-keywords'])) ? $meta['meta-keywords'] : '',
            'lastmod' => date('Y-m-d').' 06:00:00',
        ])->withViewData('markup',$this->markup->render());
    }

    public function channel38()
    {
        $meta = \Cache::remember(getLocale().'channel-38-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });
        $id = 38;

        $tariff = \Cache::remember(getLocale().'channel-tariff-'.$id, 86400, function () use ($id) {
            $service_tariff = new TariffService('tv');
            $tariff = $service_tariff->getItemForFront($id);
            return $tariff;
        });

        $channels = \Cache::remember(getLocale().'channel-items-'.$id, 86400, function () use ($id) {
            $service = new ChannelsService();
            $service->setBaseUrl('/spisok-kanalov-paket-bazovyiy');
            $channels = $service->getForFront($id);
            return $channels;
        });

        $count = \Cache::remember(getLocale().'channel-items-count-'.$id, 86400, function () use ($id) {
            $service = new ChannelsService();
            $count = $service->countChannelsInTariff($id);
            return $count;
        });

        $title = (!empty($meta['meta-title'])) ? $meta['meta-title'] : '';
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Channels',[
            'channels' => $channels,
            'count' => $count,
            'tariff' => $tariff,
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData([
            'meta_title' => (!empty($meta['meta-title'])) ? $meta['meta-title'] : '',
            'meta_description' => (!empty($meta['meta-description'])) ? $meta['meta-description'] : '',
            'meta_keywords' => (!empty($meta['meta-keywords'])) ? $meta['meta-keywords'] : '',
            'lastmod' => date('Y-m-d').' 06:00:00',
        ])->withViewData('markup',$this->markup->render());
    }

    public function channel39()
    {
        $meta = \Cache::remember(getLocale().'channel-39-meta', 86400, function () {
            $meta = getPageMeta();
            return $meta;
        });
        $id = 39;

        $tariff = \Cache::remember(getLocale().'channel-tariff-'.$id, 86400, function () use ($id) {
            $service_tariff = new TariffService('tv');
            $tariff = $service_tariff->getItemForFront($id);
            return $tariff;
        });

        $channels = \Cache::remember(getLocale().'channel-items-'.$id, 86400, function () use ($id) {
            $service = new ChannelsService();
            $service->setBaseUrl('/spisok-kanalov-paket-premium');
            $channels = $service->getForFront($id);
            return $channels;
        });

        $count = \Cache::remember(getLocale().'channel-items-count-'.$id, 86400, function () use ($id) {
            $service = new ChannelsService();
            $count = $service->countChannelsInTariff($id);
            return $count;
        });

        $title = (!empty($meta['meta-title'])) ? $meta['meta-title'] : '';
        $breads = new BreadcrumbsService();
        $breadcrumbs = $breads->getBreadcrumbs($title);

        $this->markup->addMarkup(new MarkupBreadcrumbs($breadcrumbs));

        return Inertia::render('Channels',[
            'channels' => $channels,
            'count' => $count,
            'tariff' => $tariff,
            'meta' => $meta,
            'breadcrumbs' => $breadcrumbs,
        ])->withViewData([
            'meta_title' => (!empty($meta['meta-title'])) ? $meta['meta-title'] : '',
            'meta_description' => (!empty($meta['meta-description'])) ? $meta['meta-description'] : '',
            'meta_keywords' => (!empty($meta['meta-keywords'])) ? $meta['meta-keywords'] : '',
            'lastmod' => date('Y-m-d').' 06:00:00',
        ])->withViewData('markup',$this->markup->render());
    }
}
