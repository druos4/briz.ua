<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mews\Captcha\Captcha;

//use Mews\Captcha\Captcha;

class CaptchaServiceController extends Controller
{
    public function capthcaFormValidate(Request $request)
    {
        if(\Captcha::check($request->get('captcha'))){
            return response()->json(['valid' => true]);
        } else {
            return response()->json(['valid' => false]);
        }
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
