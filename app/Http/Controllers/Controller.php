<?php

namespace App\Http\Controllers;

use App\Services\RemontService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Str;
use DB;
use App\MapMarker;
use League\CommonMark\Inline\Element\Emphasis;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {

    }

    public function export()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
/*
        $rows = DB::connection('mysql2')
            ->table('AddrMap as a')
            ->select('id','GeoID','GeoLat','GeoLon')
            ->get();
        */
        $rows = DB::connection('mysql2')
            ->table('AddrMapTV as a')
            ->select('id','GeoID','GeoLat','GeoLon')
            ->get();

        $columns = array('id', 'GeoID', 'GeoLat', 'GeoLon');

        $callback = function() use ($rows, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns,';');

            foreach($rows as $row) {
                fputcsv($file, array($row->id, $row->GeoID, $row->GeoLat, $row->GeoLon),';');
            }
            fclose($file);
        };
        return \Response::stream($callback, 200, $headers);
    }

    public function test(){
        return view('test');
    }

    public function makeSlug(Request $request)
    {
        $slug = Str::slug(trim($request->get('title')), '-');
        return response()->json(['success' => true, 'slug' => $slug]);
    }

    public function map()
    {
        return view('map');
    }

    public function mapGoogle()
    {
        return view('map-google');
    }

     public function mapLeaf()
    {
        return view('map-leaf');
    }

    public function getOwnMarkers(Request $request)
    {
        $streets = MapMarker::active()->get();

        return response()->json(['success' => true, 'streets' => $streets]);
    }

    public function getMarkers(Request $request)
    {
        /*
        $streets = DB::connection('mysql2')
            ->table('Street')
            ->select('GeoName','GeoLat','GeoLon')
            ->whereNotNull('GeoID')
            ->get();
*/

        $streets = DB::connection('mysql2')
            ->table('AddrMap')
            ->select('GeoName','GeoLat','GeoLon','Dom')
            ->whereNotNull('GeoID')
            //->where('Dom','=','*')
            ->get();
        return response()->json(['success' => true, 'streets' => $streets]);
    }

    public function getStreet(Request $request)
    {
        print_r($request->all());

        if(!empty($request->get('street'))){

            $arr = explode(' вулиця ',$request->get('street'));


        } else {
            return response()->json(['success' => false]);
        }
    }
}
