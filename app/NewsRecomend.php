<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class NewsRecomend extends Model
{
    protected $table = 'news_recomends';

    protected $dates = [
        'updated_at',
        'created_at'
    ];

    protected $fillable = [
        'news_id',
        'recomend',
    ];

}
