<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'channels';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title',
        'logo',
        'string',
        'sort',
        'billing_id',
        'billing_logo',
        'is_analog',
        'parsed',
    ];

    public function genres()
    {
        return $this->belongsToMany(Genre::class,'channels_genres','channel_id','genre_id');
    }

    public function tariffs()
    {
        return $this->belongsToMany(Tariff::class,'channels_tariffs','channel_id','tariff_id');
    }

}
