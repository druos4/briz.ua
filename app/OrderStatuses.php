<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderStatuses extends Model
{
    protected $table = 'order_statuses';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'code',
        'active',
        'goto',
    ];


    public function scopeVisible($query) {
        return $query->where('active', true);
    }

}
