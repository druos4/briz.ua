<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class MenuItem extends Model
{
    protected $table = 'menu_items';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'menu_id',
        'parent_id',
        'item_type',
        'title_ru',
        'title_ua',
        'title_en',
        'url',
        'blank',
        'color',
        'bold',
        'options',
        'sort',
    ];



    public function children()
    {
        return $this->hasMany(static::class, 'parent_id')->orderBy('sort');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }



    public function hasParent(){
        return !empty($this->parent) ? true : false;
    }



    public function hasChildren(){
        return !empty($this->children) ? true : false;
    }



    public static function getItemsWithChilds($id){
        $result = [];
        $cat = DB::table('categories')
            ->select('id','parent_id')
            ->where('id','=',$id)
            ->first();
        if(isset($cat->id)){
            $result[] = $cat->id;
            $res = DB::table('categories')
                ->select('id','parent_id')
                ->where('parent_id','=',$cat->id)
                ->get();
            if(isset($res) && count($res) > 0){
                foreach ($res as $r){
                    $result[] = $r->id;
                    $res2 = DB::table('categories')
                        ->select('id','parent_id')
                        ->where('parent_id','=',$r->id)
                        ->get();
                    if(isset($res2) && count($res2) > 0){
                        foreach ($res2 as $r2){
                            $result[] = $r2->id;
                            $res3 = DB::table('categories')
                                ->select('id','parent_id')
                                ->where('parent_id','=',$r2->id)
                                ->get();
                            if(isset($res3) && count($res3) > 0){
                                foreach ($res3 as $r3){
                                    $result[] = $r3->id;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }
}
