<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Throwable;
use Inertia\Inertia;
use Illuminate\Http\JsonResponse;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    // public function render($request, Throwable $exception)
    // {
    //     return parent::render($request, $exception);
    // }

    public function render($request, Throwable $e)
    {
        $response = parent::render($request, $e);


        if($e instanceof ThrottleRequestsException){
            return response()->json([
                'success' => false,
                'errors' => 'Too many requests',
            ]);
        }

        if(strpos($request->fullUrl(),'/admin/') === false){
            if ($e instanceof \Illuminate\Validation\ValidationException) {
                //return new JsonResponse($e->errors(), 422);
                return response()->json([
                    'success' => false,
                    'errors' => $e->errors(),
                ]);
            }
            if (in_array($response->status(), [404, 403])) {
                return Inertia::render('404', ['status' => $response->status()])
                    ->withViewData(['noindex' => true])
                    ->toResponse($request)
                    ->setStatusCode($response->status());
            } else if(in_array($response->status(), [500, 503, 504])) {
                return Inertia::render('500', ['status' => $response->status()])
                    ->toResponse($request)
                    ->setStatusCode($response->status());
            } else if ($response->status() === 419) {
                return back()->with([
                    'message' => 'The page expired, please try again.',
                ]);
            }
        }

        return $response;
    }
}
