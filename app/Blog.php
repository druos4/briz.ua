<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';

    protected $dates = [
        'updated_at',
        'created_at'
    ];

    protected $fillable = [
        'slug',
        'title_ru',
        'title_ua',
        'title_en',
        'is_active',
        'picture',
        'thumbnail',
        'anons_ru',
        'anons_ua',
        'anons_en',
        'likes_count',
        'views_count',
        'read_time',
        'published_at',
        'updated_by',
    ];

    public function scopeIsActive($query)
    {
        return $query->where('is_active',true);
    }

    public function scopeIsInActive($query)
    {
        return $query->where('is_active',false);
    }


    public function scopeIsDeleted($query)
    {
        return $query->whereNotNull('deleted_at');
    }

    public function scopeNotDeleted($query)
    {
        return $query->whereNull('deleted_at');
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', Carbon::now())->whereNull('deleted_at');
    }

    public function updater()
    {
        return $this->belongsTo(User::class,'updated_by');
    }

    public function deleter()
    {
        return $this->belongsTo(User::class,'deleted_by');
    }

    public function langs()
    {
        return $this->hasMany(BlogLang::class,'blog_id');
    }

    public function recomends()
    {
        return $this->hasMany(BlogRecomend::class,'blog_id');
    }

    public function categories()
    {
        return $this->HasMany(BlogCategoryIn::class,'blog_id');
    }

}
