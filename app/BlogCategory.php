<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $table = 'blogs_categories';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'slug',
        'title_ru',
        'title_ua',
        'title_en',
        'is_active',
        'meta_title_ru',
        'meta_title_ua',
        'meta_title_en',
        'meta_description_ru',
        'meta_description_ua',
        'meta_description_en',
        'meta_keywords_ru',
        'meta_keywords_ua',
        'meta_keywords_en',
    ];

    public function scopeIsActive($query)
    {
        return $query->where('is_active',true);
    }

    public function scopeIsInActive($query)
    {
        return $query->where('is_active',false);
    }

}
