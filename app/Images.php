<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Images extends Model
{
    protected $table = 'images';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'original',
        'thumbnail',
    ];


    public function products()
    {
        return $this->belongsToMany(Product::class,'product_images', 'image_id', 'product_id');
    }



    public static function getThumbnail($path, $width = null, $height = null)
    {
        $newpath = $path;
        $width = ($width !== null) ? $width : 300;
        $height = ($height !== null) ? $height : 200;

        if(file_exists(public_path($newpath))){
            $arr = explode('/',$newpath);
            $index = count($arr) - 1;
            $new_name = 'thumb-'.$width.'-'.$height.'-'.$arr[$index];

            if(file_exists(public_path('/thumbnails/'.$new_name))){
                $newpath = '/thumbnails/'.$new_name;
            } else {
                $img = \Image::make(public_path($newpath));
                $img->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $img->save(public_path('thumbnails/'.$new_name));
                $newpath = '/thumbnails/'.$new_name;
            }

        }
        return $newpath;
    }
}
