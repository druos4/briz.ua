<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $table = 'categories';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',

        'slug',
        'parent_id',
        'active',
        'picture',
        'sort',

        'meta_title_ru',
        'meta_title_ua',
        'meta_title_en',
        'meta_description_ru',
        'meta_description_ua',
        'meta_description_en',
        'meta_keywords_ru',
        'meta_keywords_ua',
        'meta_keywords_en',

        'uslugy_title_ru',
        'uslugy_title_ua',
        'uslugy_title_en',
        'uslugy_description_ru',
        'uslugy_description_ua',
        'uslugy_description_en',
        'recommend_products',
    ];



    public function children()
    {
        return $this->hasMany(static::class, 'parent_id')->orderBy('sort');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }



    public function hasParent(){
        return !empty($this->parent) ? true : false;
    }



    public function hasChildren(){
        return !empty($this->children) ? true : false;
    }


    public function scopeVisible($query){
        return $query->where('active', true)->where('parent_id',0);
    }



    public function properties()
    {
        return $this->belongsToMany(Property::class, 'category_properties')
            ->withPivot('sort', 'use_in_filter','show_in_product')
            ->orderBy('pivot_sort','asc');
    }


    public function products()
    {
        return $this->hasMany('App\Product');
    }



    public function activeProducts(){
        return $this->hasMany('App\Product')->visible()->whereNull('deleted_at');
    }



    public static function getCategoryWithChilds($id){
        $result = [];
        $cat = DB::table('categories')
            ->select('id','parent_id')
            ->where('id','=',$id)
            ->first();
        if(isset($cat->id)){
            $result[] = $cat->id;
            $res = DB::table('categories')
                ->select('id','parent_id')
                ->where('parent_id','=',$cat->id)
                ->get();
            if(isset($res) && count($res) > 0){
                foreach ($res as $r){
                    $result[] = $r->id;
                    $res2 = DB::table('categories')
                        ->select('id','parent_id')
                        ->where('parent_id','=',$r->id)
                        ->get();
                    if(isset($res2) && count($res2) > 0){
                        foreach ($res2 as $r2){
                            $result[] = $r2->id;
                            $res3 = DB::table('categories')
                                ->select('id','parent_id')
                                ->where('parent_id','=',$r2->id)
                                ->get();
                            if(isset($res3) && count($res3) > 0){
                                foreach ($res3 as $r3){
                                    $result[] = $r3->id;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }



    public static function filters($category_id){

        $filters = DB::table('properties as p')
            ->select('p.title','p.title_ua','p.title_en','p.slug','cp.property_id')
            ->leftJoin('category_properties as cp','cp.property_id','=','p.id')
            ->where('p.active','=',1)
            ->where('cp.use_in_filter','=',1)
            ->where('cp.category_id','=',$category_id)
            ->orderBy('cp.sort','asc')
            ->get();
        $ids = [];
        $res = DB::table('product_category')
            ->select('product_id')
            ->where('category_id','=',$category_id)
            ->get();
        if(isset($res) && count($res) > 0){
            foreach($res as $r){
                $ids[] = $r->product_id;
            }
        }

        if(isset($filters) && count($filters) > 0){
            foreach($filters as $key => $filter){
                $query = DB::table('property_values as pv')
                    ->select('pv.id','pv.value','pv.value_ua','pv.value_en','pv.slug','pv.meta_keyword', DB::raw("count(pp.id) as count"))
                    ->leftJoin('product_properties as pp','pp.value_id','=','pv.id')
                    ->leftJoin('products as p','p.id','=','pp.product_id')
                    ->where('pv.property_id','=',$filter->property_id)
                    ->where('pp.property_id','=',$filter->property_id);
                if(count($ids) > 0){
                    $query->where(function ($query2) use ($category_id,$ids) {
                        $query2->where('p.category_id','=',$category_id)
                            ->orWhereIn('p.id', $ids);
                    });
                } else {
                    $query->where('p.category_id','=',$category_id);
                }
                $filter->values = $query->whereNull('p.deleted_at')
                    ->where('p.active','=',1)
                    ->groupBy('pv.id')
                    ->orderBy('pv.value','asc')
                    ->get();

                if(count($filter->values) == 0){
                    unset($filters[$key]);
                }
            }
        }

        return $filters;
    }



    public static function getPropsFromCategory($id){
        return DB::table('properties as p')
            ->select('p.id','p.title')
            ->leftJoin('category_properties as cp','cp.property_id','=','p.id')
            ->where('cp.category_id','=',$id)
            ->where('p.active','=',1)
            ->orderBy('cp.sort','asc')
            ->orderBy('p.title','asc')
            ->get();
    }



    public static function countItemsInCategory($id){
        $count = 0;
        $count += DB::table('products')->whereNull('deleted_at')->where('active','=',1)->where('category_id','=',$id)->count();
        $count += DB::table('products as p')
            ->leftJoin('product_category as pc','pc.product_id','=','p.id')
            ->whereNull('p.deleted_at')
            ->where('p.active','=',1)
            ->where('pc.category_id','=',$id)
            ->count();
        return $count;
    }

    public function itemsCount()
    {
        $count = 0;
        $count += DB::table('products')
            ->whereNull('deleted_at')
            ->where('active','=',1)
            ->where('category_id','=',$this->id)
            ->count();
        $count += DB::table('products as p')
            ->leftJoin('product_category as pc','pc.product_id','=','p.id')
            ->whereNull('p.deleted_at')
            ->where('p.active','=',1)
            ->where('pc.category_id','=',$this->id)
            ->count();
        return $count;
    }
}
