<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Property;

class ProductImage extends Model
{
    protected $table = 'product_images';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'product_id',
        'image_id',
        'sort',
    ];



    public function image()
    {
        return $this->belongsTo(Images::class);
    }


    public function product()
    {
        return $this->belongsTo(Product::class);
    }




}
