<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChannelGenre extends Model
{
    protected $table = 'channels_genres';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'channel_id',
        'genre_id'
    ];

    public function genre()
    {
        return $this->belongsTo(Genre::class,'genre_id');
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class,'channel_id');
    }


}
