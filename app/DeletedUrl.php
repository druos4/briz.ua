<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeletedUrl extends Model
{
    protected $table = 'deleted_urls';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'url',
    ];

}
