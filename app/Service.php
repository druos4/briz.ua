<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;


class Service extends Model
{
    protected $table = 'services';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'code',
        'title_ru',
        'title_ua',
        'title_en',

        'banner_ru',
        'banner_ua',
        'banner_en',

        'category_id',

        'meta_title_ru',
        'meta_title_ua',
        'meta_title_en',
        'meta_description_ru',
        'meta_description_ua',
        'meta_description_en',
        'meta_keywords_ru',
        'meta_keywords_ua',
        'meta_keywords_en',
        'union_description_ru',
        'union_description_ua',
        'union_description_en',
    ];

    public function category() {
        return $this->belongsTo(Category::class,'category_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class,'services_products','service_id','product_id')
            ->withPivot('sort')
            ->orderBy('pivot_sort','asc');
    }

}
