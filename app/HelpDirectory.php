<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpDirectory extends Model
{
    protected $table = 'help_directories';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'slug',
        'icon',
        'parent_id',
        'sort',
        'active',
        'anons_ru',
        'anons_ua',
        'anons_en',
        'detail_ru',
        'detail_ua',
        'detail_en',
        'meta_title_ru',
        'meta_title_ua',
        'meta_title_en',
        'meta_description_ru',
        'meta_description_ua',
        'meta_description_en',
        'meta_keywords_ru',
        'meta_keywords_ua',
        'meta_keywords_en',
        'articles_show_type',
    ];

    public function children()
    {
        return $this->hasMany(HelpDirectory::class, 'parent_id')->active()->orderBy('sort','asc');
    }

    public function parent()
    {
        return $this->belongsTo(HelpDirectory::class, 'parent_id');
    }

    public function scopeActive($query){
        return $query->where('active', true);
    }

    public function articles()
    {
        return $this->hasMany(HelpArticle::class,'help_directory_id')->active()->orderBy('sort','asc');
    }

}
