<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    protected $table = 'news';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at'
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'slug',
        'active',
        'show_from',
        'picture',
        'picture_ru',
        'picture_ua',
        'picture_en',
        'anons_ru',
        'anons_ua',
        'anons_en',
        'detail_ru',
        'detail_ua',
        'detail_en',
        'on_top',
        'user_id',
        'meta_title_ru',
        'meta_title_ua',
        'meta_title_en',
        'meta_description_ru',
        'meta_description_ua',
        'meta_description_en',
        'meta_keywords_ru',
        'meta_keywords_ua',
        'meta_keywords_en',
        'created_by',
        'updated_by',
        'deleted_by',
        'entity_type',
        'picture_tablet',
        'picture_phone',
        'thumbnail_first',
        'thumbnail',
        'can_connect',
    ];

    public function scopeIsNews($query)
    {
        return $query->where('entity_type','news');
    }

    public function scopeIsActions($query)
    {
        return $query->where('entity_type','actions');
    }

    public function scopePublished($query)
    {
        return $query->where('show_from', '<=', Carbon::now())->whereNull('deleted_at');
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1)->whereNull('deleted_at');
    }

    public function scopeInActive($query)
    {
        return $query->where('active', '!=',1)->whereNull('deleted_at');
    }

    public function scopeTop($query)
    {
        return $query->where('on_top', 1);
    }

    public function scopeWithoutTop($query)
    {
        return $query->where('on_top', '!=', 1);
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class,'updated_by');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class,'news_tags','news_id','tag_id');
    }

    public function recomends()
    {
        return $this->belongsToMany(News::class,'news_recomends','news_id','recomend')->active()->published();
    }
}
