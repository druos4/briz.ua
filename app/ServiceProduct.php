<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;


class ServiceProduct extends Model
{
    protected $table = 'services_products';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'service_id',
        'product_id',
        'sort',
    ];

}
