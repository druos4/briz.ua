<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'logs';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'user_id',
        'entity_name',
        'entity_id',
        'log',
    ];

    public function author()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function tariff()
    {
        return $this->belongsTo(Tariff::class,'entity_id');
    }

}
