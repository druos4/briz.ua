<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\ProducResourseUnit;
use \App\ProductComment;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title_ru',
        'slug',
        'active',
        'category_id',
        'picture',
        'thumbnail',
        'sort',
        'article',
        'anons_ru',
        'description_ru',
        'price',
        'price_old',
        'quantity',
        'rating',
        'meta_title_ru',
        'meta_description_ru',
        'meta_keywords_ru',
        'views',
        'created_by',
        'updated_by',
        'title_ua',
        'title_en',
        'meta_title_ua',
        'meta_description_ua',
        'meta_keywords_ua',
        'meta_title_en',
        'meta_description_en',
        'meta_keywords_en',
        'anons_ua',
        'description_ua',
        'anons_en',
        'description_en',
        'discount',
        'new',
        'hit',
        'picture_recomend_hd',
        'picture_recomend_tablet',
        'picture_recomend_phone',
        'billing_name',
        'section_description_ru',
        'section_description_ua',
        'section_description_en',
        'promo',
        'warranty',
    ];

    public function scopeActive($query) {
        return $query->where('active', true)->where('category_id','>',0)->where('quantity','>',0);
    }

    public function scopeNotActive($query) {
        return $query->where('active', '!=', true);
    }

    public function scopeNew($query) {
        return $query->where('new', true);
    }

    public function scopeNotNew($query) {
        return $query->where('new', '!=', true);
    }

    public function scopePromo($query) {
        return $query->where('promo', true);
    }

    public function scopeNotPromo($query) {
        return $query->where('promo', '!=', true);
    }

    public function scopeHit($query) {
        return $query->where('hit', true);
    }

    public function scopeNotHit($query) {
        return $query->where('hit', '!=', true);
    }

    public function scopeInStock($query) {
        return $query->where('quantity','>', 0);
    }

    public function scopeNotInStock($query) {
        return $query->where('quantity','=', 0);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }


    public function resourseUnit(){
        return $this->belongsTo(ProducResourseUnit::class,'resourse_unit_id');
    }

    public function categories(){
        return $this->belongsToMany(Category::class, 'product_category', 'product_id', 'category_id');
    }

    public function properties(){
        return $this->belongsToMany(PropertyValues::class,'product_properties','product_id','value_id')
            ->with('property');
    }

    public function getPublicProperties()
    {
        $props = CategoryProperty::with('property')
            ->where('category_id',$this->category_id)
            ->where('show_in_product',1)
            ->orderBy('sort','asc')
            ->get();
        if(!empty($props)){
            foreach($props as $prop){
                $prop->values = DB::table('product_properties as pp')
                    ->select('pv.*')
                    ->leftJoin('property_values as pv','pv.id','=','pp.value_id')
                    ->where('pp.property_id','=',$prop->property_id)
                    ->where('pp.product_id','=',$this->id)
                    ->orderBy('pv.sort','asc')
                    ->get();
            }
        }
        return $props;
    }

    public function getProductsProperies(){
        $res = DB::table('category_properties as cp')
            ->select('p.id','p.title','p.slug','p.type')
            ->leftJoin('properties as p','p.id','=','cp.property_id')
            ->where('cp.category_id','=',$this->category_id)
            ->where('cp.show_in_product','=',1)
            ->where('p.active','=',1)
            ->orderBy('cp.sort','asc')
            ->get();
        if(isset($res) && count($res) > 0){
            foreach($res as $r){
                $r->values = DB::table('product_properties as pp')
                    ->select('pp.id','pv.value','pv.slug','pp.value_id')
                    ->join('property_values as pv','pv.id','=','pp.value_id')
                    ->where('pp.product_id','=',$this->id)
                    ->where('pp.property_id','=',$r->id)
                    ->where('pv.property_id','=',$r->id)
                    ->orderBy('pv.sort','asc')
                    ->get();
            }
            return $res;
        } else {
            return [];
        }
    }



    public static function getProductMoreCategories($id){
        $cats = [];
        $res = DB::table('product_category')
            ->select()
            ->where('product_id','=',$id)
            ->get();
        if(isset($res) && count($res) > 0){
            foreach ($res as $r){
                $cats[] = $r->category_id;
            }
        }
        return $cats;
    }



    public static function getProductPropertyValues($id){
        $vals = [];
        $res = DB::table('product_properties')
            ->select()
            ->where('product_id','=',$id)
            ->get();
        if(isset($res) && count($res) > 0){
            foreach ($res as $r){
                $vals[] = $r->value_id;
            }
        }
        return $vals;
    }



    public function canBuy(){
        if($this->active == 1 && $this->price > 0 && $this->quantity > 0){
            return true;
        } else {
            return false;
        }
    }



    public function scopeVisible($query) {
        return $query->where('active', true)
            ->where('price', '>', 0)
            ->whereNull('deleted_at')
            ->has('category','>', 0);
    }




    public static function recalculate($prod, $usdRate){
        $prod->price = round(($prod->price_usd * $usdRate),2);
        if($prod->discount > 0){
            $prod->price_old = round(($prod->price / (100 - $prod->discount) * 100),2);
        } elseif($prod->price_old > 0 && $prod->price_old > $prod->price){
            $prod->discount = 100 - round($prod->price * 100 / $prod->price_old);
        }
        DB::table('products')
            ->where('id','=',$prod->id)
            ->update(['price' => $prod->price,
                'price_old' => $prod->price_old,
                'discount' => $prod->discount]);
    }



    public static function syncPhotos($input_ids,$product_id){
        $ids = explode(',',$input_ids);
        DB::table('product_images')
            ->where('product_id','=',$product_id)
            ->delete();
        if(isset($ids) && count($ids) > 0){
            $i = 0;
            foreach ($ids as $id){
                if($id > 0){
                    $i++;
                    DB::table('product_images')->insert(['product_id' => $product_id, 'image_id' => $id, 'sort'=> $i]);
                }
            }
        }
    }



    public function videos(){
        return $this->hasMany(ProductVideo::class);
    }


    public function documents(){
        return $this->hasMany(ProductDocument::class);
    }


    public static function getCategoryProducts($category_id){






    }



    public function gallery()
    {
        return $this->belongsToMany(Images::class,'product_images','product_id','image_id')
            ->withPivot('sort')
            ->orderBy('pivot_sort','asc');
    }



    public static function getSameCatProds($category_id, $product_id){
        $resourses = getResourseUnitsArray();
        $res = DB::table('products as p')
            ->select('p.id','p.slug','p.title','p.title_ua','p.title_en','p.price', 'p.price_old', 'p.discount',
                'p.opt_from','p.size_name',
                'p.picture','p.hit','p.new','c.slug as category_slug','p.quantity','p.resourse_unit_id')
            ->leftJoin('categories as c','c.id','=','p.category_id')
            ->where('p.category_id','=',$category_id)
            ->where('p.id','!=',$product_id)
            ->where('p.active','=',1)
            ->whereNull('p.deleted_at')
            ->orderBy('p.sort','asc')
            ->limit(12)
            ->get();
        if(!empty($res)) {
            foreach ($res as $r) {
                if ($r->picture != '' && env('APP_ENV') == 'prod') {
                    $r->picture = 'https://img.dvor.ua' . $r->picture;
                }
                if (isset($r->resourse_unit_id) && $r->resourse_unit_id > 0) {
                    if (isset($resourses[$r->resourse_unit_id])) {
                        $r->resourseUnit = $resourses[$r->resourse_unit_id];
                    }
                }
            }
        }
        return $res;
    }



    public static function getViewedProds($ids,$id){
        $res =  DB::table('products as p')
            ->select('p.id','p.slug','p.title','p.title_ua','p.title_en','p.price', 'p.price_old', 'p.discount',
                'p.opt_from','p.size_name',
                'p.picture','p.hit','p.new',
                'c.slug as category_slug','p.quantity','p.resourse_unit_id')
            ->leftJoin('categories as c','c.id','=','p.category_id')
            ->whereIn('p.id',$ids)
            ->where('p.active','=',1)
            ->whereNull('p.deleted_at')
            ->where('p.id','!=',$id)
            ->orderBy('p.sort','asc')
            ->get();
        if(count($res) > 0){
            $resourses = getResourseUnitsArray();
            foreach ($res as $r){
                $r->in_favourite = Product::inFavourite($r->id);
                $r->picture_original = $r->picture;
                $r->picture = ($r->picture != '') ? Images::getThumbnail($r->picture, 191, 140) : '';
                if($r->picture != '' && env('APP_ENV') == 'prod'){
                    $r->picture = 'https://img.dvor.ua'.$r->picture;
                }
                if($r->picture_original != '' && env('APP_ENV') == 'prod'){
                    $r->picture_original = 'https://img.dvor.ua'.$r->picture_original;
                }
                $r->has_photo = Product::hasPhotos($r->id);
                $r->has_video = Product::hasVideos($r->id);
                $r->has_panorama = false;
                if(isset($r->pano360) && $r->pano360 != ''){
                    $r->has_panorama = true;
                } else {
                    $r->has_panorama = Product::hasPano($r->id);
                }
                $r->in_favourite = Product::inFavourite($r->id);
                if(isset($r->resourse_unit_id) && $r->resourse_unit_id > 0){
                    if(isset($resourses[$r->resourse_unit_id])){
                        $r->resourseUnit = $resourses[$r->resourse_unit_id];
                    }
                }
                $r->rating = Product::getProductRating($r->id);
                $r->commentsCount = Product::getCommentsCount($r->id);
               // $r->resourseUnit = ProducResourseUnit::find($r->resourse_unit_id);
            }
        }

        return $res;
    }

    public static function getCommentsCount($id){
        $res = DB::table('product_comments')
            ->where('product_id','=',$id)
            ->whereNotNull('moderated_at')
            ->count();
        return $res;
    }

    public static function getProductRating($id){
        $rating = 0;
        $res = ProductComment::where('product_id',$id)->whereNotNull('moderated_at')->whereNotNull('rating')->get();
        if(!empty($res)){
            $s = 0;
            $c = 0;
            foreach($res as $r){
                $s += $r->rating;
                $c++;
            }
            if($c > 0){
                $rating = round(($s / $c), 2);
            } else {
                $rating = 0;
            }

        }
        return $rating;
    }

    public static function getBuyWithIt($id){
        $res = DB::table('cart as c')
            ->select('c.order_id')
            ->whereNotNull('c.order_id')
            ->where('c.product_id','=',$id)
            ->get();

        $ids = [];
        foreach ($res as $r){
            if(!in_array($r->order_id,$ids)){
                $ids[] = $r->order_id;
            }
        }

        if(count($ids) > 0){
            $res = DB::table('products as p')
                ->select('p.id','p.slug','p.title','p.title_ua','p.title_en','p.price', 'p.price_old',
                    'p.opt_from','p.size_name',
                    'p.discount', 'p.picture','p.hit','p.new',
                    'c.slug as category_slug','p.quantity',DB::raw('count(cc.id) as count'))
                ->leftJoin('categories as c','c.id','=','p.category_id')
                ->leftJoin('cart as cc','cc.product_id','=','p.id')
                ->whereIn('cc.order_id',$ids)
                ->where('p.active','=',1)
                ->whereNull('p.deleted_at')
                ->where('p.id','!=',$id)
                ->groupBy('p.id')
                ->orderBy('count','desc')
                ->limit(20)
                ->get();
            if(!empty($res)){
                foreach($res as $r){
                    if($r->picture != '' && env('APP_ENV') == 'prod'){
                        $r->picture = 'https://img.dvor.ua'.$r->picture;
                    }
                }
            }
            return $res;

        } else {
            return [];
        }
    }


    public static function getNewProducts(){
        $res = DB::table('products as p')
            ->select('p.id','p.slug','p.title','p.title_ua','p.title_en',
                'p.opt_from','p.size_name',
                'p.price', 'p.price_old', 'p.discount', 'p.price_opt', 'p.picture','p.hit','p.new','c.slug as category_slug','p.quantity',
                'p.pano360','p.resourse_unit_id')
            ->leftJoin('categories as c','c.id','=','p.category_id')
            ->where('p.new','=',1)
            ->whereNull('p.deleted_at')
            ->where('p.active','=',1)
            ->orderBy('p.id','desc')
            ->limit(12)
            ->get();
        if(count($res) > 0){
            $resourses = getResourseUnitsArray();
            foreach ($res as $r){
                $r->in_favourite = Product::inFavourite($r->id);

                $r->picture_original = $r->picture;
                $r->picture = ($r->picture != '') ? Images::getThumbnail($r->picture, 191, 140) : '';
                if($r->picture != '' && env('APP_ENV') == 'prod'){
                    $r->picture = 'https://img.dvor.ua'.$r->picture;
                }
                if($r->picture_original != '' && env('APP_ENV') == 'prod'){
                    $r->picture_original = 'https://img.dvor.ua'.$r->picture_original;
                }

                $r->has_photo = Product::hasPhotos($r->id);
                $r->has_video = Product::hasVideos($r->id);
                $r->has_panorama = false;
                if(isset($r->pano360) && $r->pano360 != ''){
                    $r->has_panorama = true;
                } else {
                    $r->has_panorama = Product::hasPano($r->id);
                }
                $r->in_favourite = Product::inFavourite($r->id);
                if(isset($r->resourse_unit_id) && $r->resourse_unit_id > 0){
                    if(isset($resourses[$r->resourse_unit_id])){
                        $r->resourseUnit = $resourses[$r->resourse_unit_id];
                    }
                }
                $r->rating = Product::getProductRating($r->id);
                $r->commentsCount = Product::getCommentsCount($r->id);
               // $r->resourseUnit = ProducResourseUnit::find($r->resourse_unit_id);
            }
        }

        return $res;
    }


    public static function getFinishingProducts(){
        $res = DB::table('products as p')
            ->select('p.id','p.slug','p.title','p.title_ua','p.title_en','p.opt_from','p.size_name',
                'p.price', 'p.price_old', 'p.discount', 'p.price_opt', 'p.picture','p.hit','p.new','c.slug as category_slug','p.quantity','p.resourse_unit_id')
            ->leftJoin('categories as c','c.id','=','p.category_id')
            ->where('p.finishing','=',1)
            ->where('p.quantity','>',0)
            ->where('p.active','=',1)
            ->whereNull('p.deleted_at')
            ->orderBy('p.id','desc')
            ->get();
        if(count($res) > 0){
            $resourses = getResourseUnitsArray();
            foreach ($res as $r){
                $r->in_favourite = Product::inFavourite($r->id);

                $r->picture_original = $r->picture;
                $r->picture = ($r->picture != '') ? Images::getThumbnail($r->picture, 191, 140) : '';
                if($r->picture != '' && env('APP_ENV') == 'prod'){
                    $r->picture = 'https://img.dvor.ua'.$r->picture;
                }
                if($r->picture_original != '' && env('APP_ENV') == 'prod'){
                    $r->picture_original = 'https://img.dvor.ua'.$r->picture_original;
                }

                $r->has_photo = Product::hasPhotos($r->id);
                $r->has_video = Product::hasVideos($r->id);
                $r->has_panorama = false;
                if(isset($r->pano360) && $r->pano360 != ''){
                    $r->has_panorama = true;
                } else {
                    $r->has_panorama = Product::hasPano($r->id);
                }
                $r->in_favourite = Product::inFavourite($r->id);
                if(isset($r->resourse_unit_id) && $r->resourse_unit_id > 0){
                    if(isset($resourses[$r->resourse_unit_id])){
                        $r->resourseUnit = $resourses[$r->resourse_unit_id];
                    }
                }
                $r->rating = Product::getProductRating($r->id);
                $r->commentsCount = Product::getCommentsCount($r->id);
                //$r->resourseUnit = ProducResourseUnit::find($r->resourse_unit_id);
            }
        }

        return $res;
    }


    public static function getHitProducts(){
        $res = DB::table('products as p')
            ->select('p.id','p.slug','p.title','p.title_ua','p.title_en',
                'p.price', 'p.price_old', 'p.discount', 'p.price_opt',
                'p.opt_from','p.size_name',
                'p.picture','p.hit','p.new','c.slug as category_slug','p.quantity','p.pano360','p.resourse_unit_id')
            ->leftJoin('categories as c','c.id','=','p.category_id')
            ->where('p.hit','=',1)
            ->where('p.active','=',1)
            ->whereNull('p.deleted_at')
            ->orderBy('p.id','desc')
            ->limit(12)
            ->get();
        if(count($res) > 0){
            $resourses = getResourseUnitsArray();
            foreach ($res as $r){
                $r->in_favourite = Product::inFavourite($r->id);

                $r->picture_original = $r->picture;
                $r->picture = ($r->picture != '') ? Images::getThumbnail($r->picture, 191, 140) : '';
                if($r->picture != '' && env('APP_ENV') == 'prod'){
                    $r->picture = 'https://img.dvor.ua'.$r->picture;
                }
                if($r->picture_original != '' && env('APP_ENV') == 'prod'){
                    $r->picture_original = 'https://img.dvor.ua'.$r->picture_original;
                }

                $r->has_photo = Product::hasPhotos($r->id);
                $r->has_video = Product::hasVideos($r->id);
                $r->has_panorama = false;
                if(isset($r->pano360) && $r->pano360 != ''){
                    $r->has_panorama = true;
                } else {
                    $r->has_panorama = Product::hasPano($r->id);
                }
                $r->in_favourite = Product::inFavourite($r->id);
                if(isset($r->resourse_unit_id) && $r->resourse_unit_id > 0){
                    if(isset($resourses[$r->resourse_unit_id])){
                        $r->resourseUnit = $resourses[$r->resourse_unit_id];
                    }
                }
                $r->rating = Product::getProductRating($r->id);
                $r->commentsCount = Product::getCommentsCount($r->id);
               // $r->resourseUnit = ProducResourseUnit::find($r->resourse_unit_id);
            }
        }

        return $res;
    }



    public static function getSearchProducts($s){
        $ids = [];
        $arrStr = explode(' ',$s);
        if(count($arrStr) == 1){

            $query = DB::table('products as p')
                ->select('p.id','p.slug','p.title','p.title_ua','p.title_en','p.price', 'p.price_old', 'p.discount',
                    'p.opt_from','p.size_name',
                    'p.picture','p.hit','p.new','c.slug as category_slug','p.quantity')
                ->leftJoin('categories as c','c.id','=','p.category_id');
            $query->where(function ($query2) use ($s) {
                $query2->where('p.title', 'LIKE', '%'.$s.'%')
                   // ->orWhere('p.title_ua', 'LIKE', '%'.$s.'%')
                   // ->orWhere('p.title_en', 'LIKE', '%'.$s.'%')
                    ->orWhere('p.article', 'LIKE', '%'.$s.'%');
            });
            $result = $query
                ->whereNull('p.deleted_at')
                ->where('p.active','=',1)
                ->orderBy('p.id','desc')
                ->get();

        } elseif(count($arrStr) > 1){
            $result = [];

            $query = DB::table('products as p')
                ->select('p.id');
            $query->where(function ($query2) use ($s) {
                $query2->where('p.title', 'LIKE', '%'.$s.'%')
                    //->orWhere('p.title_ua', 'LIKE', '%'.$s.'%')
                    //->orWhere('p.title_en', 'LIKE', '%'.$s.'%')
                    ->orWhere('p.article', 'LIKE', '%'.$s.'%');
            });
            $res = $query
                ->whereNull('p.deleted_at')
                ->where('p.active','=',1)
                ->orderBy('p.id','desc')->get();
            if(isset($res) && count($res) > 0){
                $result = $res;
                foreach($res as $r){
                    if(!in_array($r->id,$ids)){
                        $ids[] = $r->id;
                    }
                }
            }

            $query = DB::table('products as p')
                ->select('p.id');
            foreach($arrStr as $str){
                $query->where('p.title', 'LIKE', '%'.$str.'%');
            }
            $res = $query->where('p.active','=',1)
                ->whereNull('p.deleted_at')
                ->orderBy('p.id','desc')
                ->get();
            if(isset($res) && count($res) > 0){
                foreach($res as $r){
                    if(!in_array($r->id,$ids)){
                        $ids[] = $r->id;
                    }
                }
            }

            if(count($ids) == 0){
                $query = DB::table('products as p')
                    ->select('p.id');
                if(count($ids) > 0){
                    $query->whereNotIn('p.id',$ids);
                }
                $query->where(function ($query2) use ($arrStr) {
                    $query2->where('p.title', 'LIKE', '%'.$arrStr[0].'%');
                    for($i = 1; $i < count($arrStr); $i++){
                        $query2->orWhere('p.title_ua', 'LIKE', '%'.$arrStr[$i].'%');
                    }
                });
                $res = $query->where('p.active','=',1)
                    ->whereNull('p.deleted_at')
                    ->orderBy('p.id','desc')
                    ->get();
                if(isset($res) && count($res) > 0){
                    foreach($res as $r){
                        if(!in_array($r->id,$ids)){
                            $ids[] = $r->id;
                        }
                    }
                }
            }

            if(count($ids) > 0){
                $query = DB::table('products as p')
                    ->select('p.id','p.slug','p.title','p.title_ua','p.title_en','p.price', 'p.price_old', 'p.discount',
                        'p.opt_from','p.size_name',
                        'p.picture','p.hit','p.new','c.slug as category_slug','p.quantity')
                    ->leftJoin('categories as c','c.id','=','p.category_id');
                $query->whereIn('p.id',$ids);
                $result = $query
                    ->whereNull('p.deleted_at')
                    ->where('p.active','=',1)
                    ->orderBy('p.id','desc')
                    ->get();
            }

        }

        if(!empty($result) > 0){
            $resourses = getResourseUnitsArray();
            foreach ($result as $r){
                if($r->picture != '' && env('APP_ENV') == 'prod'){
                    $r->picture = 'https://img.dvor.ua'.$r->picture;
                }
                $r->rating = Product::getProductRating($r->id);
                $r->has_photo = Product::hasPhotos($r->id);
                $r->has_video = Product::hasVideos($r->id);
                $r->has_panorama = false;
                if(isset($r->pano360) && $r->pano360 != ''){
                    $r->has_panorama = true;
                } else {
                    $r->has_panorama = Product::hasPano($r->id);
                }
                $r->in_favourite = Product::inFavourite($r->id);
                if(isset($r->resourse_unit_id) && $r->resourse_unit_id > 0){
                    if(isset($resourses[$r->resourse_unit_id])){
                        $r->resourseUnit = $resourses[$r->resourse_unit_id];
                    }
                }
            }
        }

        return $result;
    }


    public static function getSearchProductsFromCategory($s){
        $res = DB::table('categories')
            ->select('id')
            ->where(function ($query2) use ($s) {
                $query2->where('title', 'LIKE', '%'.$s.'%')
                    ->orWhere('title_ua', 'LIKE', '%'.$s.'%')
                    ->orWhere('title_en', 'LIKE', '%'.$s.'%');
            })
            ->get();
        $ids = [];
        if(isset($res) && count($res) > 0){
            foreach ($res as $r){
                $ids[] = $r->id;
            }
        }
        if(count($ids) > 0){
            $query = DB::table('products as p')
                ->select('p.id','p.slug','p.title','p.title_ua','p.title_en','p.price', 'p.price_old', 'p.discount',
                    'p.opt_from','p.size_name',
                    'p.picture','p.hit','p.new',
                    'c.slug as category_slug','p.quantity')
                ->leftJoin('categories as c','c.id','=','p.category_id')
                ->leftJoin('product_category as pc','pc.product_id','=','p.id');
            $query->where(function ($query2) use ($ids) {
                $query2->whereIn('p.category_id', $ids)
                    ->orWhereIn('pc.category_id', $ids);
            });

            $res = $query->whereNull('p.deleted_at')->orderBy('p.id','desc')
                ->limit(40)
                ->get();

            if(!empty($res) > 0){
                $resourses = getResourseUnitsArray();
                foreach ($res as $r){
                    if($r->picture != '' && env('APP_ENV') == 'prod'){
                        $r->picture = 'https://img.dvor.ua'.$r->picture;
                    }
                    $r->rating = Product::getProductRating($r->id);
                    $r->has_photo = Product::hasPhotos($r->id);
                    $r->has_video = Product::hasVideos($r->id);
                    $r->has_panorama = false;
                    if(isset($r->pano360) && $r->pano360 != ''){
                        $r->has_panorama = true;
                    } else {
                        $r->has_panorama = Product::hasPano($r->id);
                    }
                    $r->in_favourite = Product::inFavourite($r->id);
                    if(isset($r->resourse_unit_id) && $r->resourse_unit_id > 0){
                        if(isset($resourses[$r->resourse_unit_id])){
                            $r->resourseUnit = $resourses[$r->resourse_unit_id];
                        }
                    }
                }
            }

            return $res;
        } else {
            return [];
        }
    }


    public function comments(){
        return $this->hasMany(ProductComment::class)->visible()->with('media')->orderBy('created_at','asc');
    }


    public static function getProductUrl($id){
        $res = DB::table('products as p')
            ->select('p.id','p.slug','c.slug as category_slug')
            ->leftJoin('categories as c','c.id','=','p.category_id')
            ->whereNull('p.deleted_at')
            ->where('p.active','=',1)
            ->where('p.id','=',$id)
            ->first();
        if(isset($res->id) && $res->id > 0){
            return '/'.$res->category_slug.'/'.$res->slug;
        } else {
            return false;
        }
    }


    public static function exist($xmlId){
        $exist = DB::table('products')->select('id')->where('xml_id','=',$xmlId)->first();
        if(isset($exist->id) && $exist->id > 0){
            return true;
        } else {
            return false;
        }
    }


    public static function getProdsForPortfolio($id){
        return DB::table('products as p')
            ->select('pp.id','p.title','p.xml_id','p.price','p.picture')
            ->leftJoin('portfolio_products as pp','pp.product_id','=','p.id')
            ->where('pp.portfolio_id','=',$id)
            ->orderBy('pp.id','asc')
            ->get();
    }


    public static function getProd360Photos($id){
        $photos360 = [];
        $dir = public_path().'/uploads/products360/'.$id;
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if(strlen($file) > 5){
                        if(env('APP_ENV') == 'prod'){
                            $photos360[] = 'https://img.dvor.ua/uploads/products360/'.$id.'/'.$file;
                        } else {
                            $photos360[] = '/uploads/products360/'.$id.'/'.$file;
                        }
                    }
                }
                closedir($dh);
            }
        }
        if(count($photos360) > 0){
            asort($photos360);
            return $photos360;
        } else {
            return false;
        }
    }


    public static function hasPhotos($id){
        $count = DB::table('product_images')->where('product_id','=',$id)->count();
        if($count > 0){
            return true;
        } else {
            return false;
        }
    }
    public static function hasPano($id){
        $photos360 = 0;
        $dir = public_path().'/uploads/products360/'.$id;
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if(strlen($file) > 5){
                        $photos360++;
                    }
                }
                closedir($dh);
            }
        }
        if($photos360 > 0){
            return true;
        } else {
            return false;
        }
    }
    public static function hasVideos($id){
        $count = DB::table('product_videos')
            ->where('product_id','=',$id)->where('code','!=','')->count();
        if($count > 0){
            return true;
        } else {
            return false;
        }
    }

    public function getSeriaProductProps($props){
        $arr = [];
        $in = [];
        if(!empty($props)){
            foreach($props as $k => $v){
                $in[] = $v->slug;
            }
        }

        if(!empty($in) && !empty($this->properties)){
            foreach($this->properties as $k => $v){
                if(in_array($v->slug,$in)){
                    $arr[$v->slug] = $v;
                }
            }
        }
        return $arr;
    }

    public function getProductsSeria(){

        if($this->seria != ''){
            //ok
        } else {
            return false;
        }

        $props = Property::getSeriasProperties($this->category_id);

        if(count($props) > 0){
            $cat = DB::table('categories')->select('id','slug')->where('id','=',$this->category_id)->first();

            if(count($props) == 1){

                foreach($props as $key => $prop){
                    $res = DB::table('products as p')
                        ->select('p.id','p.title','p.slug','pp.value_id','pv.value','pv.slug as value_slug','pv.icon')
                        ->leftJoin('product_properties as pp','pp.product_id','=','p.id')
                        ->leftJoin('property_values as pv','pv.id','=','pp.value_id')
                        ->where('p.seria','=', $this->seria)
                        ->where('pp.property_id','=',$prop->id)
                        ->where('pv.property_id','=',$prop->id)
                        ->whereNull('p.deleted_at')
                        ->where('p.active','=',1)
                        ->orderBy('pv.sort','asc')
                        ->get();
                    if(isset($res) && count($res) > 0){

                        foreach($res as $r){
                            $r->url = '/'.$cat->slug.'/'.$r->slug;
                            if($r->id == $this->id){
                                $r->current = true;
                            } else {
                                $r->current = false;
                            }
                            $prop->values[] = $r;
                        }
                    } else {
                        unset($props[$key]);
                    }
                }

            } else {

                $ownProps = self::getSeriaProductProps($props);

                foreach($props as $key => $prop){

                    $query = DB::table('products as p')
                        ->select('p.id','p.title','p.slug','pp.value_id','pv.value','pv.slug as value_slug','pv.icon');

                    $query->leftJoin('product_properties as pp','pp.product_id','=','p.id')
                        ->leftJoin('property_values as pv','pv.id','=','pp.value_id')
                        ->where('pp.property_id','=',$prop->id)
                        ->where('pv.property_id','=',$prop->id);
                    $j = 0;
                    foreach($props as $k => $p){
                        if($prop->id != $p->id && isset($ownProps[$p->slug])){

                            $vals = [];
                            foreach($ownProps[$p->slug]->values as $ownProp){
                                $vals[] = $ownProp->value_id;
                            }
                            if(count($vals) > 0){
                                $j++;


                                $query->leftJoin('product_properties as pp'.$j,'pp'.$j.'.product_id','=','p.id')
                                    ->where('pp'.$j.'.property_id','=',$p->id)
                                    ->whereIn('pp'.$j.'.value_id',$vals);

                            }
                        }
                    }
                    $query->where('p.seria','=', $this->seria)
                        ->whereNull('p.deleted_at')
                        ->where('p.active','=',1);


                    $res = $query->orderBy('pv.sort','asc')
                        ->get();
                    if(isset($res) && count($res) > 0){

                        foreach($res as $r){
                            $r->url = '/'.$cat->slug.'/'.$r->slug;
                            if($r->id == $this->id){
                                $r->current = true;
                            } else {
                                $r->current = false;
                            }
                            $prop->values[] = $r;
                        }
                    } else {
                        unset($props[$key]);
                    }
                }

            }

            if(count($props) > 0){
                return $props;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public static function inFavourite($id){
        if(auth()->id() > 0){
            $res = DB::table('user_favourites')
                ->select()
                ->where('user_id','=',auth()->id())
                ->where('product_id','=',$id)
                ->first();
            if(isset($res) && $res->id > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
