<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    protected $table = 'tariffs';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'tariff_type',
        'service_type',
        'monthly',
        'daily',
        'user_type',
        'title_ru',
        'title_ua',
        'title_en',
        'price',
        'price_union',
        'bonus_payment',
        'get_bonus',
        'can_seven_days',
        'prepay_discount',
        'description_ru',
        'description_ua',
        'description_en',
        'billing_id',
        'active',
        'sort',
        'power',
        'union_group',
        'index_group',
        'price_old',
        'picture',
        'business_group',
        'description_old_ru',
        'description_old_ua',
        'description_old_en',
        'first_to_show',
        'channels_from',
        'show_from',
        'show_to',
        'has_films',
        'films_title_ru',
        'films_title_ua',
        'films_title_en',
        'films_price',
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeInActive($query)
    {
        return $query->where('active','!=', 1);
    }

    public function scopeInet($query)
    {
        return $query->where(function($query2) {
            $query2->where('tariff_type', 'internet')
                ->orWhere(function($query3) {
                    $query3->where('tariff_type', 'union')
                        ->whereIn('service_type', [3,4]);
                });
        });
    }

    public function scopeCtv($query)
    {
        return $query->where('tariff_type', 'ctv');
    }

    public function scopeIptv($query)
    {
        return $query->where('tariff_type', 'iptv');
    }

    public function scopeUnion($query)
    {
        return $query->where('tariff_type', 'union');
    }

    public function scopePlainUser($query)
    {
        return $query->where('user_type', 1);
    }

    public function scopeDaily($query)
    {
        return $query->where('daily', 1);
    }

    public function scopeMonthly($query)
    {
        return $query->where('monthly', 1);
    }

    public function scopeLegalUser($query)
    {
        return $query->where('user_type', 2);
    }

    public function scopeNotLegalUser($query)
    {
        return $query->where('user_type','!=', 2);
    }

    public function scopeInetTv($query)
    {
        return $query->where('union_group', 'tv');
    }

    public function scopeInetTvmax($query)
    {
        return $query->where('union_group', 'tvmax');
    }

    public function scopeInetBrizTv($query)
    {
        return $query->where('union_group', 'briztv');
    }

    public function scopeInetIptv($query)
    {
        return $query->where('union_group', 'iptv');
    }

}
