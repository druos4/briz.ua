<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MapMarker extends Model
{
    protected $table = 'map_markers';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'address_name',
        'address_num',
        'lat',
        'lon',
        'has_inet',
        'has_ctv',
        'has_iptv',
        'has_giga',
        'addr_id',
        'addrtv_id',
        'street_id',
        'street_ru',
        'street_ua',
        'street_en',
        'addr_sort',
    ];

    public function scopeActive($query)
    {
        return $query->where('lat', '!=', '')->where('lon', '!=', '');
    }

}
