<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\MenuItem;

class Menu extends Model
{
    protected $table = 'menus';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title',
        'code',
        'active',
        'render',
        'render_ua',
        'render_en',
    ];



    public function scopeVisible($query) {
        return $query->where('active', true);
    }

    public function items(){
        return $this->hasMany(MenuItem::class,'menu_id');
    }
}
