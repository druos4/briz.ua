<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqGroup extends Model
{
    protected $table = 'faqs_groups';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'sort',
        'active',
        'icon',
        'slug',
        'description_ru',
        'description_ua',
        'description_en',
        'meta_title_ru',
        'meta_title_ua',
        'meta_title_en',
        'meta_description_ru',
        'meta_description_ua',
        'meta_description_en',
        'meta_keywords_ru',
        'meta_keywords_ua',
        'meta_keywords_en',
    ];

    public function children()
    {
        return $this->hasMany(Faq::class, 'faq_group_id')->active()->orderBy('sort','asc');
    }

    public function scopeActive($query){
        return $query->where('active', true);
    }

}
