<?php

namespace App\Console;

use App\Console\Commands\ClearCache;
use App\Console\Commands\GenerateImgSiteMap;
use App\Console\Commands\GenerateSiteMap;
use App\Console\Commands\ImportActions;
use App\Console\Commands\ImportChannels;
use App\Console\Commands\ImportFromBilling;
use App\Console\Commands\ImportMapMarkers;
use App\Console\Commands\ImportProducts;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\UkrPoshtaParse;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UkrPoshtaParse::class,
        ImportFromBilling::class,
        ImportMapMarkers::class,
        ImportActions::class,
        ImportChannels::class,
        ImportProducts::class,
        GenerateSiteMap::class,
        ClearCache::class,
        GenerateImgSiteMap::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        //$schedule->command('importFromBilling')->dailyAt('04:00');
        $schedule->command('importMapMarkers')->dailyAt('04:20');
        $schedule->command('importProducts')->dailyAt('04:30');
        $schedule->command('importChannels')->dailyAt('04:40');
        $schedule->command('generateSiteMap')->dailyAt('04:50');
        $schedule->command('clearCache')->dailyAt('05:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
