<?php

namespace App\Console\Commands;

use App\Core\Lib\Billing\BillingApi;
use App\MapMarker;
use App\Services\MapService;
use App\Services\TariffService;
use App\Service;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clearCache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'clear cache after import';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Cache::flush();
    }



}
