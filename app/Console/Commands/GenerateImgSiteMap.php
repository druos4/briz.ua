<?php

namespace App\Console\Commands;

use App\Jobs\MakeImagesSitemapJob;
use Illuminate\Console\Command;

class GenerateImgSiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generateImgSiteMap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate images site map';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        MakeImagesSitemapJob::dispatch();

        $this->info("MakeImagesSitemapJob add to queue!");

        return Command::SUCCESS;

    }
}
