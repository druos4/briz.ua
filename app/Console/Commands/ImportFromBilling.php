<?php

namespace App\Console\Commands;

use App\Core\Lib\Billing\BillingApi;
use App\Services\TariffService;
use App\Service;
use Illuminate\Console\Command;

class ImportFromBilling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importFromBilling';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'mport data from billing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        self::getservices('internetServices','internet');
        self::getservices('ctvServices','ctv');
        self::getservices('iptvServices','iptv');
        self::getservices('unionServices','union');

        echo 'done';
    }

    private function getservices($endpoint, $type)
    {
        try {
            $billingAPI = BillingApi::get();
            $response = $billingAPI->execute($endpoint, []);
        } catch (ApiException $e) {
            print_r($e->getMessage());
            die;
        }

        if(!empty($response)){
            $service = new TariffService($type);
            $service->importFromBilling($response);
        }
    }
}
