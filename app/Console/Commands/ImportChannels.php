<?php

namespace App\Console\Commands;

use App\Jobs\AfterParseChannels;
use App\Jobs\BeforeParseChannels;
use App\Jobs\ParseChannelUrl;
use App\Jobs\SyncChannelsJob;
use Illuminate\Console\Command;

class ImportChannels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importChannels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import channels';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dispatch(new SyncChannelsJob());
        echo 'queue set!';
    }
}
