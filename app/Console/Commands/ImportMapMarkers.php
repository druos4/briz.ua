<?php

namespace App\Console\Commands;

use App\Core\Lib\Billing\BillingApi;
use App\MapMarker;
use App\Services\MapService;
use App\Services\TariffService;
use App\Service;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ImportMapMarkers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importMapMarkers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import map';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $map = new MapService();
        $result = $map->importMarkersFromBilling();
        echo 'done updating map markers';
    }



}
