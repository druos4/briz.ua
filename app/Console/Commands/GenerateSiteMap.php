<?php

namespace App\Console\Commands;

use App\Core\Lib\Billing\BillingApi;
use App\Product;
use App\Services\ProductService;
use App\Services\SiteMapService;
use App\Services\TariffService;
use App\Service;
use Illuminate\Console\Command;
use Spatie\Sitemap\Sitemap;

class GenerateSiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generateSiteMap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate sitemap.xml';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new SiteMapService('ua');
        $service->generateMap();

        $service = new SiteMapService('ru');
        $service->generateMap();

        $service = new SiteMapService('en');
        $service->generateMap();

        echo 'DONE';
    }


}
