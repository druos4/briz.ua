<?php

namespace App\Console\Commands;

use App\Core\Lib\Billing\BillingApi;
use App\MapMarker;
use App\Services\TariffService;
use App\Service;
use Illuminate\Console\Command;

class ImportActions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importActions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import actions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $billingAPI = BillingApi::get();
            $response = $billingAPI->execute('getActions', []);

            print_r($response);
            echo 'after';
            die;

        } catch (ApiException $e) {
            return false;
        }



        die;
    }

    private function createMapInet($input)
    {
        $data = [
            'address_name' => $input['street_ru'],
            'address_num' => $input['dom'],
            'lat' => $input['geo_lat'],
            'lon' => $input['geo_lon'],
            'has_inet' => 1,
            'has_iptv' => 1,
            //'has_giga' => $input[''],
            'addr_id' => $input['id'],
            'street_id' => $input['street_id'],
            'street_ru' => $input['street_ru'],
            'street_ua' => $input['street_ua'],
        ];
        MapMarker::create($data);
    }

    private function updateMapInet($input, $marker)
    {
        $data = [
            'address_name' => $input['street_ru'],
            'address_num' => $input['dom'],
            'lat' => $input['geo_lat'],
            'lon' => $input['geo_lon'],
            'has_inet' => 1,
            'has_iptv' => 1,
            //'has_giga' => $input[''],
            'street_id' => $input['street_id'],
            'street_ru' => $input['street_ru'],
            'street_ua' => $input['street_ua'],
        ];
        $marker->update($data);
    }

    private function parseInet($arr)
    {
        foreach($arr as $a){
            $marker = MapMarker::where('addr_id',$a['id'])->first();
            if(!empty($marker->id)){
                $this->updateMapInet($a, $marker);
            } else {
                $this->createMapInet($a);
            }
        }
    }

    private function createMapCtv($input)
    {
        $data = [
            'address_name' => $input['street_ru'],
            'address_num' => $input['dom'],
            'lat' => $input['geo_lat'],
            'lon' => $input['geo_lon'],
            'has_ctv' => 1,
            'addrtv_id' => $input['id'],
            'street_id' => $input['street_id'],
            'street_ru' => $input['street_ru'],
            'street_ua' => $input['street_ua'],
        ];
        MapMarker::create($data);
    }

    private function updateMapCtv($input, $marker)
    {
        $data = [
            'address_name' => $input['street_ru'],
            'address_num' => $input['dom'],
            'lat' => $input['geo_lat'],
            'lon' => $input['geo_lon'],
            'has_ctv' => 1,
            'street_id' => $input['street_id'],
            'street_ru' => $input['street_ru'],
            'street_ua' => $input['street_ua'],
        ];
        $marker->update($data);
    }

    private function parseCtv($arr)
    {
        foreach($arr as $a){
            $marker = MapMarker::where('addrtv_id',$a['id'])->first();
            if(!empty($marker->id)){
                $this->updateMapCtv($a, $marker);
            } else {
                $this->createMapCtv($a);
            }
        }
    }

    private function getFromBilling()
    {
        try {
            $billingAPI = BillingApi::get();
            $response = $billingAPI->execute('getAllMarkers', []);

            if(!empty($response)){
                if(!empty($response['inet'])){
                    print_r($response['inet'][0]);
                }
            }
            echo 'after';
            die;

        } catch (ApiException $e) {
            return false;
        }
        return $response;
    }

}
