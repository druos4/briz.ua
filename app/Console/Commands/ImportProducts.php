<?php

namespace App\Console\Commands;

use App\Core\Lib\Billing\BillingApi;
use App\Product;
use App\Services\ProductService;
use App\Services\TariffService;
use App\Service;
use Illuminate\Console\Command;

class ImportProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importProducts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'import products price and stocks from billing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new ProductService();
        $updated = $service->importProductsFromBilling();
        echo 'done sync prods - '.$updated.' items updated';
    }


}
