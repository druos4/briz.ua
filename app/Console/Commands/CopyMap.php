<?php

namespace App\Console\Commands;

use App\Address;
use App\DomType;
use App\Street;
use Illuminate\Console\Command;
use DB;
use App\MapMarker;

class CopyMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copyMap';
    protected $api_key = 'AIzaSyDJMPznZQ2EFzmExhKaQFB09bS1p0-Me_Q';
    protected $api_url = 'https://maps.googleapis.com/maps/api/geocode/json?';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function getFromMap($json)
    {
        $point = [
            'id' => '',
            'address' => [
                'area_1' => '',
                'area_2' => '',
                'route' => '',
                'street_number' => '',
            ],
            'lat' => '',
            'lon' => '',
        ];
        $dec = json_decode($json);
/*
        print_r($dec);
        die;
*/
        if(!empty($dec->results[0])){
            $res = $dec->results[0];

                if(!empty($res->address_components)){
                    foreach($res->address_components as $component){
                        if(in_array('street_number',$component->types)){
                            $point['address']['street_number'] = $component->long_name;
                        }
                        if(in_array('route',$component->types)){
                            $point['address']['route'] = $component->long_name;
                        }
                        if(in_array('administrative_area_level_2',$component->types)){
                            $point['address']['area_2'] = $component->long_name;
                        }
                        if(in_array('sublocality_level_1',$component->types)){
                            $point['address']['area_2'] = $component->long_name;
                        }
                        if(in_array('administrative_area_level_1',$component->types)){
                            $point['address']['area_1'] = $component->long_name;
                        }
                    }

                }


                if(!empty($res->geometry)){
                    $point['lat'] = $res->geometry->location->lat;
                    $point['lon'] = $res->geometry->location->lng;
                }


            if(!empty($res->place_id)){
                $point['id'] = $res->place_id;
            }
        }

        return $point;
    }

    private function sendCurl($url)
    {
        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_HTTPHEADER, ['Accept-Language: uk']);
        $point = self::getFromMap(curl_exec($curlSession));
        curl_close($curlSession);
        return $point;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $i = 0;
        $houses = DB::connection('mysql2')
            ->table('AddrMap as a')
            ->select('a.id','a.StreetID','a.Dom','s.Name','a.GeoName','a.GeoLat','a.GeoLon')
            ->leftJoin('Street as s','s.id','=','a.StreetID')
            ->whereNotNull('a.GeoID')
            ->orderBy('s.Name','asc')
            ->orderBy('a.Dom','asc')
            ->get();
        if(!empty($houses)){
            foreach($houses as $house){
                $check = MapMarker::where('addr_id',$house->id)->first();
                if(!$check){
                    $data = [
                        'address_name' => $house->GeoName,
                        'address_num' => $house->Dom,
                        'lat' => $house->GeoLat,
                        'lon' => $house->GeoLon,
                        'has_inet' => 1,
                        'addr_id' => $house->id,
                    ];
                    MapMarker::create($data);
                }

                $i++;
                if($i % 10 == 0){
                    echo ' '.$i.' ';
                }
            }
        }



        die;




/*
        $streets_ids = [81,2,74,9];
*/

        $streets_ids = [854,847,536,644,652,653,654,656,641,855,848,537,650,642,856,849,651,788,850,646,851,647,852,853,657,540,623,630,588,631,624,622,625,632,626,633,627,634,584,628,
            629,583,648,649,533,872,694,643,527,519,520,84,846,525,528,524,535,740,521,842,845,844,538,526,534,733,473,539];

/*
        $streets_ids = [131,637,813,603,815,867,814,868,767,347,10,11,12,13,518,240,14,15,160,220,728,710,715,714,17,820,821,817,835,818,750,751,752,793,871,834,640,711,823,
            756,824,639,717,819,865,754,822,16,866,713,864,753,755,749];
*/
/*
        $streets_ids = [816,789,184,185,186,248,775,776,777,778,779,780,781,782,783,784,785,156,157,159,152,213,170,171,162,178,858,151,149,180,153,150,158,129];
*/
        $houses = DB::connection('mysql2')
            ->table('AddrMap as a')
            ->select('a.id','a.StreetID','a.Dom','s.Name')
            ->leftJoin('Street as s','s.id','=','a.StreetID')
            ->whereIn('a.StreetID',$streets_ids)
            ->whereNull('a.GeoID')
            ->orderBy('Dom','asc')
            ->limit(100)
            ->get();

        $i = 0;
        if(!empty($houses)){
            foreach ($houses as $house){
                $s_name = $house->Name;

                $s_name = str_replace(' (Днепропетровская дор.)','',$s_name);
                $s_name = str_replace('Частный сектор,','',$s_name);
                $s_name = str_replace('Шевченко 3,','',$s_name);
                $s_name = str_replace('/1,','',$s_name);

                $s_name = str_replace(' ','+',$s_name);
                $title = 'Одесса,'.$s_name;
                if($house->Dom != '*'){
                    $title .= '+'.$house->Dom;
                }
                $url = $this->api_url.'address='.$title.'&key='.$this->api_key;
                //echo $title;
                echo $url;

                $point = self::sendCurl($url);
                if($i % 30 == 0){
                    sleep(1);
                }

                if(!empty($point['id']) && !empty($point['lat']) && !empty($point['lon'])){
                    $name = $point['address']['area_1'];
                    if(isset($point['address']['area_2']) && $point['address']['area_2'] != ''){$name .= ', '.$point['address']['area_2'];}
                    if(isset($point['address']['route']) && $point['address']['route'] != ''){$name .= ', '.$point['address']['route'];}
                    //if(isset($point['address']['street_number']) && $point['address']['street_number'] != ''){$name .= ', '.$point['address']['street_number'];}

                    DB::connection('mysql2')
                        ->table('AddrMap')
                        ->where('id','=',$house->id)
                        ->update([
                            'GeoID' => $point['id'],
                            'GeoLat' => $point['lat'],
                            'GeoLon' => $point['lon'],
                            'GeoName' => $name
                        ]);
                }

                if($i % 10 == 0){
                    echo ' '.$i.' ';
                }
                $i++;

            }
        }





        die;
        $areas = DB::connection('mysql2')
            ->table('StreetArea')
            ->select()
            //->where('GeoDistrict','=','Одесса')
            ->where('GeoDistrict','!=','Одесса')
            ->whereNotNull('GeoDistrict')
            //->where('id','=',27)
            ->orderBy('Name','asc')
            ->get();
        $i = 0;
        if(!empty($areas)){
            foreach($areas as $area){

                $streets = DB::connection('mysql2')
                    ->table('Street')
                    ->select()
                    ->where('StreetAreaID','=',$area->id)
                    //->whereNull('GeoID')
                    ->orderBy('Name','asc')
                    //->limit(10)
                    ->get();
                if(!empty($streets)){
                    foreach ($streets as $street){

                        $houses = DB::connection('mysql2')
                            ->table('AddrMap')
                            ->select()
                            ->where('StreetID','=',$street->id)
                            ->whereNull('GeoID')
                            //->where('Dom','!=','*')
                            ->where('Dom','=','*')
                            ->orderBy('Dom','asc')
                            ->limit(20)
                            ->get();
                        if(!empty($houses)){
                            foreach ($houses as $house){

                                $title = 'Одесса,'.str_replace(' ','+',$street->Name);
                                if($house->Dom != '*'){
                                    $title .= '+'.$house->Dom;
                                }
                                $url = $this->api_url.'address='.$title.'&key='.$this->api_key;
                                $point = self::sendCurl($url);
                                if($i % 30 == 0){
                                    sleep(1);
                                }

                                if(!empty($point['id']) && !empty($point['lat']) && !empty($point['lon'])){
                                    $name = $point['address']['area_1'];
                                    if(isset($point['address']['area_2']) && $point['address']['area_2'] != ''){$name .= ', '.$point['address']['area_2'];}
                                    if(isset($point['address']['route']) && $point['address']['route'] != ''){$name .= ', '.$point['address']['route'];}
                                    //if(isset($point['address']['street_number']) && $point['address']['street_number'] != ''){$name .= ', '.$point['address']['street_number'];}

                                    DB::connection('mysql2')
                                        ->table('AddrMap')
                                        ->where('id','=',$house->id)
                                        ->update([
                                            'GeoID' => $point['id'],
                                            'GeoLat' => $point['lat'],
                                            'GeoLon' => $point['lon'],
                                            'GeoName' => $name
                                    ]);
                                }

                                if($i % 10 == 0){
                                    echo ' '.$i.' ';
                                }
                                $i++;
                            }
                        }

                    }
                }

            }
        }



    }
}
