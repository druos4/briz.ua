<?php

namespace App\Console\Commands;

use App\Club;
use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Str;
use Aspera\Spreadsheet\XLSX\Reader;
use Aspera\Spreadsheet\XLSX\SharedStringsConfiguration;
use App\Services\UkrPoshtaService;

class UkrPoshtaParse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ukrPoshtaParse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service = new UkrPoshtaService();
        //$service->getRegions();
        //$service->getDistricts();
        //$service->getCities();

        $city = DB::table('map_cities')
            ->select()
            ->where('id','=',16131)
            ->first();
        $service->getStreet($city);

        echo ' done';
    }
}
