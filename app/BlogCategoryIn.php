<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BlogCategoryIn extends Model
{
    protected $table = 'blogs_categories_in';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'blog_id',
        'blog_category_id',
    ];

    public function blog()
    {
        return $this->belongsTo(Blog::class,'blog_id');
    }

    public function category()
    {
        return $this->belongsTo(BlogCategory::class,'blog_category_id')->isActive();
    }

}
