<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChannelTarif extends Model
{
    protected $table = 'channels_tariffs';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'channel_id',
        'tariff_id'
    ];

    public function tariff()
    {
        return $this->belongsTo(Tariff::class,'tariff_id');
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class,'channel_id');
    }


}
