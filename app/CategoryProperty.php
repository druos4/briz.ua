<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Category;
use App\Property;

class CategoryProperty extends Model
{
    protected $table = 'category_properties';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'category_id',
        'property_id',
        'sort',
        'use_in_filter',
        'show_in_product',
        'use_for_seria',
    ];



    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    public function property()
    {
        return $this->belongsTo(Property::class)->active();
    }



    public static function getCatProps($category_id){
        $res = CategoryProperty::where('category_id',$category_id)->get();
        $ids = [];
        if(isset($res) && count($res) > 0){
            foreach ($res as $r){
                $ids[] = $r->property_id;
            }
            return \App\Property::whereIn('id',$ids)->with('values')->orderBy('sort','asc')->orderBy('title_ru','asc')->get();
        } else {
            return [];
        }
    }


    public static function exist($category_id,$property_id){
        $row = CategoryProperty::where('category_id',$category_id)->where('property_id',$property_id)->first();
        if(isset($row->id) && $row->id > 0){
            return true;
        } else {
            return false;
        }
    }



    public static function updateSort($category_id,$input){
        $arr = explode(',',$input);
        $ids = [];
        if(!empty($arr)){
            foreach($arr as $id){
                $ids[] = str_replace('exist-prop-','',$id);
            }
        }
        DB::table('category_properties')
            ->where('category_id','=',$category_id)
            ->whereNotIn('property_id',$ids)
            ->delete();
        if(!empty($ids)){
            $i = 0;
            foreach($ids as $id){
                $i++;
                $exist = DB::table('category_properties')
                    ->select()
                    ->where('category_id','=',$category_id)
                    ->where('property_id','=',$id)
                    ->first();
                if(isset($exist->id) && $exist->id > 0){
                    DB::table('category_properties')
                        ->where('id','=',$exist->id)
                        ->update([
                            'sort' => $i,
                            'show_in_product' => 1,
                            'updated_at' => Carbon::now(),
                        ]);
                } else {
                    DB::table('category_properties')->insert([
                        'category_id' => $category_id,
                        'property_id' => $id,
                        'sort' => $i,
                        'show_in_product' => 1,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]);
                }
            }
        }






        /*
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
        die;
        DB::table('category_properties')
            ->where('category_id','=',$category_id)
            ->update(['sort' => 100]);
        if(count($arr) > 0){
            $i = 0;
            foreach ($arr as $a){
                $i++;
                DB::table('category_properties')
                    ->where('category_id','=',$category_id)
                    ->where('property_id','=',str_replace('exist-prop-','',$a))
                    ->update(['sort' => $i]);
            }
        }*/
    }


    public static function copyProps($parent_id, $child_id){
        DB::table('category_properties')->where('category_id','=',$child_id)->delete();
        $res = DB::table('category_properties')
            ->select()
            ->where('category_id','=',$parent_id)
            ->get();
        if(isset($res) && count($res) > 0){
            foreach ($res as $r){
                $data = ['category_id' => $child_id,
                    'property_id' => $r->property_id,
                    'sort' => $r->sort,
                    'use_in_filter' => $r->use_in_filter,
                    'show_in_product' => $r->show_in_product,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()];
                DB::table('category_properties')->insert($data);
            }
        }
    }


    public static function updateInFilter($category_id,$input){
        DB::table('category_properties')
            ->where('category_id','=',$category_id)
            ->update(['use_in_filter' => 0]);
        if(isset($input) && count($input) > 0){
            foreach ($input as $a){
                DB::table('category_properties')
                    ->where('category_id','=',$category_id)
                    ->where('property_id','=',$a)
                    ->update(['use_in_filter' => 1]);
            }
        }
    }



    public static function updateInProduct($category_id,$input){
        DB::table('category_properties')
            ->where('category_id','=',$category_id)
            ->update(['show_in_product' => 0]);
        if(isset($input) && count($input) > 0){
            foreach ($input as $a){
                DB::table('category_properties')
                    ->where('category_id','=',$category_id)
                    ->where('property_id','=',$a)
                    ->update(['show_in_product' => 1]);
            }
        }
    }

    public static function updateForSeria($category_id,$input){
        DB::table('category_properties')
            ->where('category_id','=',$category_id)
            ->update(['use_for_seria' => 0]);
        if(isset($input) && count($input) > 0){
            foreach ($input as $a){
                DB::table('category_properties')
                    ->where('category_id','=',$category_id)
                    ->where('property_id','=',$a)
                    ->update(['use_for_seria' => 1]);
            }
        }
    }


}
