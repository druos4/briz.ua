<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Property extends Model
{
    protected $table = 'properties';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'slug',
        'active',
        'type',
        'sort',
        'multiple',
    ];



    public function values()
    {
        return $this->hasMany(PropertyValues::class)->orderBy('sort');
    }

    public function scopeActive($query) {
        return $query->where('active', true)
            ->where('active',1);
    }

    public function categories(){
        return $this->belongsToMany(Category::class, 'category_properties')->orderBy('title_ru','asc');
    }


    public static function showInProduct($id, $categoryId){
        $res = DB::table('category_properties')
            ->select()
            ->where('category_id','=',$categoryId)
            ->where('property_id','=',$id)
            ->first();
        if(isset($res->show_in_product) && $res->show_in_product == 1){
            return true;
        } else {
            return false;
        }
    }


    public static function getSeriasProperties($category_id){
        $result = DB::table('properties as p')
            ->select('p.id','p.title_ru','p.slug')
            ->leftJoin('category_properties as cp','cp.property_id','=','p.id')
            ->where('cp.category_id','=',$category_id)
            ->where('cp.use_for_seria','=',1)
            ->where('p.active','=',1)
            ->orderBy('cp.sort','asc')
            ->get();
        if(isset($result) && count($result) > 0){
            return $result;
        } else {
            return [];
        }
    }

}
