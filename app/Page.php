<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    protected $table = 'pages';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'slug',
        'active',
        'detail_ru',
        'detail_ua',
        'detail_en',
        'meta_title_ru',
        'meta_title_ua',
        'meta_title_en',
        'meta_description_ru',
        'meta_description_ua',
        'meta_description_en',
        'meta_keywords_ru',
        'meta_keywords_ua',
        'meta_keywords_en',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class,'updated_by');
    }
}
