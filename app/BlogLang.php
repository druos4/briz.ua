<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BlogLang extends Model
{
    protected $table = 'blogs_langs';

    protected $dates = [
        'updated_at',
        'created_at'
    ];

    protected $fillable = [
        'blog_id',
        'field',
        'value',
        'lang',
    ];

    public function blog()
    {
        return $this->belongsTo(Blog::class,'blog_id');
    }

}
