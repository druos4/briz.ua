<?php

namespace App\Services;

use App\Menu;
use App\MenuItem;
use App\Product;
use DB;

class MenuService
{
    private $id;

    public function __construct($id)
    {
        $this->setId($id);
    }

    private function setId($id)
    {
        $this->id = $id;
    }

    private function getId()
    {
        return $this->id;
    }

    public function getMenu()
    {
        $items = [];
        $menu = Menu::where('code', 'main_menu')->first();
        if ($menu) {
            $items = MenuItem::with('children')
                ->where('menu_id', $menu->id)
                ->where('parent_id', 0)
                ->orderBy('sort', 'asc')
                ->get();
        }

        return $items;
    }

    public function storeMenuItem($request)
    {
        if ($request->get('item_type') == 'simple') {
            $res = $this->storeSimpleItem($request);
        } elseif($request->get('item_type') == 'product' && $request->get('product_id') > 0){
            $res = $this->storeProductItem($request);
        }

        return $res;
    }

    private function itemUpdateOrCreate($data)
    {
        $existed = false;
        if(!empty($data['item_id'])){
            $item = MenuItem::find($data['item_id']);
            $item->update($data);
            $existed = true;
        } else {
            $item = MenuItem::create($data);
        }

        return ['item' => $item, 'existed' => $existed];
    }

    private function storeProductItem($request)
    {
        $product = Product::with('category')->find($request->get('product_id'));

        $data = ['menu_id' => $this->getId(),
            'parent_id' => $request->get('parent_id'),
            'item_type' => $request->get('item_type'),
            'title_ru' => $request->get('title_ru'),
            'title_ua' => $request->get('title_ua'),
            'title_en' => $request->get('title_en'),
            'url' => '/' . $product->category->slug . '/' . $product->slug,
            'sort' => 100,
            'options' => $product->id,
        ];
        $data['blank'] = ($request->get('blank') == 1) ? 1 : 0;
        $data['bold'] = ($request->get('bold') == 1) ? 1 : 0;
        $data['item_id'] = ($request->get('item_id') != '') ? $request->get('item_id') : null;

        $res = $this->itemUpdateOrCreate($data);
        $html = $this->makeItemHtml($res['item']);
        return ['html' => $html, 'existed' => $res['existed'], 'item' => $res['item']];
    }

    private function storeSimpleItem($request)
    {
        $data = ['menu_id' => $this->getId(),
            'parent_id' => $request->get('parent_id'),
            'item_type' => $request->get('item_type'),
            'title_ru' => $request->get('title_ru'),
            'title_ua' => $request->get('title_ua'),
            'title_en' => $request->get('title_en'),
            'url' => $request->get('url'),
            'sort' => ($request->get('sort') != '') ? $request->get('sort') : 100,
        ];
        $data['blank'] = ($request->get('blank') == 1) ? 1 : 0;
        $data['bold'] = ($request->get('bold') == 1) ? 1 : 0;
        $data['options'] = ($request->get('options') != '') ? $request->get('options') : null;
        $data['item_id'] = ($request->get('item_id') != '') ? $request->get('item_id') : null;

        $res = $this->itemUpdateOrCreate($data);
        $html = $this->makeItemHtml($res['item']);
        return ['html' => $html, 'existed' => $res['existed'], 'item' => $res['item']];
    }

    private function makeItemHtml($item)
    {
        $title = $item->title_ru;
        if($item->item_type == 'product'){
            $title .= ' (товар)';
        }
        $html = '<li class="dd-item" id="item-'.$item->id.'" data-id="'.$item->id.'">
                            <div class="dd-handle">
                                &nbsp;&nbsp;
                                '.$title.'

                            </div>
                        </li>';

        return $html;
    }

    public function saveItemsSorting($request)
    {
        foreach(\Request::get('serialized') as $catKey => $category) {

            if(isset($category['children'])) {
                foreach($category['children'] as $childKey => $child) {
                    MenuItem::where('menu_id','=',$this->getId())
                        ->where('id',$child['id'])
                        ->update([
                            'sort' => $childKey,
                            'parent_id' => $category['id']
                        ]);

                    if(isset($child['children'])) {
                        foreach($child['children'] as $chKey => $chVal) {
                            MenuItem::where('menu_id','=',$this->getId())
                                ->where('id',$chVal['id'])
                                ->update([
                                    'sort' => $chKey,
                                    'parent_id' => $child['id']
                                ]);

                            if(isset($chVal['children'])) {
                                foreach($chVal['children'] as $chKey2 => $chVal2) {
                                    MenuItem::where('menu_id','=',$this->getId())
                                        ->where('id',$chVal2['id'])
                                        ->update([
                                            'sort' => $chKey2,
                                            'parent_id' => $chVal['id']
                                        ]);

                                }
                            }

                        }
                    }
                }
            }
            MenuItem::where('menu_id','=',$this->getId())
                ->where('id',$category['id'])
                ->update([
                    'sort' => $catKey,
                    'parent_id' => 0
                ]);
        }
    }

    public function deleteMenuItem($request)
    {
        MenuItem::where('menu_id','=',$this->getId())
            ->where('id',$request->get('item_id'))
            ->delete();
        MenuItem::where('menu_id','=',$this->getId())
            ->where('parent_id',$request->get('item_id'))
            ->delete();
    }

    public function getMenuForFront()
    {
        $items = [];
        $menu = Menu::where('code', $this->getId())->first();
        if (!empty($menu)) {
            $res = MenuItem::with('children')
                ->where('menu_id', $menu->id)
                ->where('parent_id', 0)
                ->orderBy('sort', 'asc')
                ->get();
            if(!empty($res)){
                foreach($res as $r){
                    $sub_items = [];
                    if(!empty($r->children)){
                        foreach($r->children as $child){
                            $sub_items[] = [
                                'title' => $child->{'title' . getLocaleDBSuf()},
                                'type' => $child->item_type,
                                'url' => ($child->blank == 1) ? $child->url : getLocaleHrefPrefix().$child->url,
                                'blank' => $child->blank,
                                'bold' => $child->bold
                            ];
                        }
                    }
                    $items[] = [
                        'title' => $r->{'title' . getLocaleDBSuf()},
                        'type' => $r->item_type,
                        'url' => ($r->blank == 1) ? $r->url : getLocaleHrefPrefix().$r->url,
                        'blank' => $r->blank,
                        'bold' => $r->bold,
                        'chilren' => $sub_items,
                    ];

                }
            }
        }

        return $items;
    }
}
