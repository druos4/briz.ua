<?php


namespace App\Services\ImagesMap\Parser;

use Illuminate\Support\Facades\Http;
use DB;
use voku\helper\HtmlDomParser;
use Illuminate\Support\Facades\App;

class Parser implements ParserInterface
{
    private $imgsitemap;
    private $pages = [];

    public function __construct($pages)
    {
        $this->imgsitemap = App::make("sitemap");
        $this->setPages($pages);
    }

    private function setPages($pages)
    {
        $this->pages = $pages;
    }

    private function getPages()
    {
        return $this->pages;
    }

    /**
     * @return true
     */
    public function process()
    {
        $pages = $this->getPages();
        if (!empty($pages)) {
            foreach ($pages as $page) {
                $this->parsePage($page);
            }
            $this->saveSiteMap();
        }
        return true;
    }

    /*
     * Для указанного адреса собираем все изобрадения
    */
    private function parsePage($url)
    {

        $url = str_replace(env('APP_URL'), 'https://briz.ua', $url);
        $parsedUrl = parse_url($url);
        $domain = (!empty($parsedUrl['scheme'])) ? $parsedUrl['scheme'] . '://' . $parsedUrl['host'] : '';
        $dom = HtmlDomParser::file_get_html($url);
        $els = $dom->findMulti('img');

        if (!empty($els)) {
            $imgs = [];
            foreach ($els as $el) {
                $imgSrc = $el->getAttribute('src');
                if (strpos($imgSrc, 'facebook.com') !== false || $imgSrc == '') {
                    continue;
                }

                $innerImageLink = $el->getAttribute('src');
                $parsedInnerImageLink = parse_url($innerImageLink);

                if (empty($parsedInnerImageLink['path'])) {
                    continue;
                }

                if (empty($parsedInnerImageLink['scheme'])) {
                    $innerImageLink = $domain . $innerImageLink;
                }

                $imgs[] = [
                    'url' => $innerImageLink,
                ];
            }
            if (!empty($imgs)) {
                $this->imgsitemap->add($url, null, null, null, $imgs);
            }
        }
    }

    /*
     * Сохраняем карту в файл
    */
    private function saveSiteMap()
    {
        $this->imgsitemap->store('xml', 'sitemap-images');
    }

}
