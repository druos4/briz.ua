<?php


namespace App\Services\ImagesMap\Parser;


interface ParserInterface
{

    public function process();

}
