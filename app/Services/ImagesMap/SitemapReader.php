<?php


namespace App\Services\ImagesMap;


class SitemapReader
{
    public function get()
    {
        $pages = [];
        try {
            $xmlString = file_get_contents(public_path("/sitemap-ua.xml"));
            $xmlObject = simplexml_load_string($xmlString);
            $json = json_encode($xmlObject);
            $array = json_decode($json, true);
        } catch (\Exception $exception){
        }
        if(!empty($array)){
            foreach ($array as $urls){
                if(!empty($urls)){
                    foreach($urls as $url){
                        $pages[] = $url['loc'];
                    }
                }
            }
        }
        return $pages;
    }

}
