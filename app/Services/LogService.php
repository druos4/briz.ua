<?php

namespace App\Services;

use App\Log;

class LogService
{

    protected $log;


    public function __construct()
    {
        $this->log = new Log();
    }

    public function saveLog($entity_name, $entity, $update = false){
        if($update == false){

            $data = [
                'user_id' => auth()->id(),
                'entity_name' => $entity_name,
                'entity_id' => $entity->id,
                'log' => 'Создана новая запись',
            ];

        } else {

            $fields = [];

            if(!empty($update)){
                foreach($update as $field => $value){
                    if(in_array($field,['_token','_method','updated_by','tags'])){
                        continue;
                    }
                    if(isset($entity->{$field}) && !is_array($value) && $entity->{$field} != $value){
                        $fields[] = '<b>'.$field.'</b>: '.$entity->{$field}.' --> '.$value;
                    }
                }
            }

            if(!empty($fields)){
                $text = 'Изменены следующие поля:<br />';
                $text.= implode('<br />',$fields);
            } else {
                $text = 'Элемент сохранен без изменений';
            }
            $data = [
                'user_id' => auth()->id(),
                'entity_name' => $entity_name,
                'entity_id' => $entity->id,
                'log' => $text,
            ];
        }
        $this->log->create($data);
    }

    public function getLogs($entity_name, $entity){
        return $this->log::with('author')
            ->where('entity_name',$entity_name)
            ->where('entity_id',$entity->id)
            ->orderBy('id','desc')
            ->get();
    }

    public function getTariffsLogs($entity_name){
        return $this->log::with('author','tariff')
            ->where('entity_name',$entity_name)
            ->orderBy('id','desc')
            ->paginate(50);
    }



}
