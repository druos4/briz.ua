<?php

namespace App\Services;

use App\Core\Lib\Billing\BillingApi;
use App\Feedback;
use App\News;
use App\Product;
use App\Service;
use App\Tariff;
use Carbon\Carbon;

class FeedbackService
{
    private $data, $send_time_delta = 300;

    public function __construct()
    {

    }

    private function setData($data)
    {
        $this->data = $data;
    }

    private function getData()
    {
        return $this->data;
    }

    private function getTimeDelta()
    {
        return $this->send_time_delta;
    }

    private function validateSiteFeedback($request)
    {
        $errors = [];
        /*
        $rules = ['captcha' => 'required|captcha'];
        if($request['ghost'] != ''){
            $errors[] = trans('feedback.errors.invalid-ghost');
        }
        $validator = validator()->make($request->all(), $rules);
        if ($validator->fails()) {
            $errors[] = trans('feedback.errors.invalid-captcha');
        }*/
        $phone = clearPhoneInput($request['phone']);
        $phone_first = substr($phone,0,3);
        if(strlen($phone) != 12 || $phone_first != '380'){
            $errors[] = trans('feedback.errors.invalid-phone');
        }
        if(!empty($errors)){
            return $errors;
        } else {
            return true;
        }
    }

    private function validateMapConnect($request)
    {
        $errors = [];
        $phone = clearPhoneInput($request['phone']);
        $phone_first = substr($phone,0,3);
        if(strlen($phone) != 12 || $phone_first != '380'){
            $errors[] = trans('feedback.errors.invalid-phone');
        }
        if(!empty($errors)){
            return $errors;
        } else {
            return true;
        }
    }

    private function validateDeviceOrder($request)
    {
        $errors = [];
        $phone = clearPhoneInput($request['phone']);
        $phone_first = substr($phone,0,3);
        if(strlen($phone) != 12 || $phone_first != '380'){
            $errors[] = trans('feedback.errors.invalid-phone');
        }
        if($request->get('product') > 0){
            $product = Product::where('id',$request->get('product'))->first();
            if(!$product){
                $errors[] = trans('feedback.errors.invalid-product');
            }
        }
        if(!empty($errors)){
            return $errors;
        } else {
            return true;
        }
    }

    private function validateTariffOrder($request)
    {
        $errors = [];
        $phone = clearPhoneInput($request['phone']);
        $phone_first = substr($phone,0,3);
        if(strlen($phone) != 12 || $phone_first != '380'){
            $errors[] = trans('feedback.errors.invalid-phone');
        }
        if($request->get('service') > 0){
            $service = Tariff::where('id',$request->get('service'))->first();
            if(!$service){
                $errors[] = trans('feedback.errors.invalid-service');
            }
        }
        if(!empty($errors)){
            return $errors;
        } else {
            return true;
        }
    }

    private function validateCallToMe($request)
    {
        $errors = [];
        $phone = clearPhoneInput($request['phone']);
        $phone_first = substr($phone,0,3);
        if(strlen($phone) != 12 || $phone_first != '380'){
            $errors[] = trans('feedback.errors.invalid-phone');
        }
        if(!empty($errors)){
            return $errors;
        } else {
            return true;
        }
    }

    private function prepareDataFeedback($request, $skip_astra = false, $sourse = '')
    {
        $comment = (!empty($request['address'])) ? 'Адрес: '.strip_tags($request['address']).'<br />' : '';
        if(!empty($request['comment'])){
            $comment .= 'Комментарий: '.strip_tags($request['comment']);
        } elseif(!empty($request['name'])) {
            $comment .= 'Заявка на перезвон от: '.strip_tags($request['name']);
        }
        $phone = clearPhoneInput($request['phone']);
        if(strlen($phone) == 12){
            $phone = substr($phone,2,10);
        }
        $data = [
            'Dom' => 127,
            'Flat' => 1,
            'StreetID' => 147,
            'Comment' => $comment,
            'Phone' => $phone,
            'Name' => (!empty($request['name'])) ? strip_tags($request['name']) : 'Аноним',
            'IP' => '127.0.0.1',
            'Time' => strtotime(Carbon::now()),
            'skipAstra' => $skip_astra,
            'Source' => $sourse,
        ];
        $this->setData($data);
    }

    private function prepareDataOrder($request, $skip_astra = false, $sourse = '')
    {
        $phone = clearPhoneInput($request['phone']);
        if(strlen($phone) == 12){
            $phone = substr($phone,2,10);
        }
        $service = Tariff::find($request->get('service'));
        $comment = 'Заявка на подключение пакета &laquo;'.$service->title_ru.'&raquo;';
        if($request->get('name') != ''){
            $comment .= ' от клиента '.$request->get('name');
        }
        $data = [
            'Time' => time(),
            //'ToTime' => 'date_format:U',
            'Dom' => 127,
            'Flat' => 1,
            'StreetID' => 147,
            'Comment' => $comment,
            'Phone' => $phone,
            'Name' => $request['name'],
            'IP' => '127.0.0.1',
            'skipAstra' => true,
            'Source' => $sourse,
        ];
        $this->setData($data);
    }

    private function prepareDataAction($request, $skip_astra = false, $sourse = '')
    {
        $phone = clearPhoneInput($request['phone']);
        if(strlen($phone) == 12){
            $phone = substr($phone,2,10);
        }
        $service = News::active()->where('id',$request->get('id'))->first();
        $comment = 'Заявка на подключение акции &laquo;'.$service->title_ru.'&raquo;';
        if($request->get('name') != ''){
            $comment .= ' от клиента '.$request->get('name');
        }
        $data = [
            'Time' => time(),
            //'ToTime' => 'date_format:U',
            'Dom' => 127,
            'Flat' => 1,
            'StreetID' => 147,
            'Comment' => $comment,
            'Phone' => $phone,
            'Name' => $request['name'],
            'IP' => '127.0.0.1',
            'skipAstra' => true,
            'Source' => $sourse,
        ];
        $this->setData($data);
    }

    private function prepareDataDeviceOrder($request, $skip_astra = false, $sourse = '')
    {
        $phone = clearPhoneInput($request['phone']);
        if(strlen($phone) == 12){
            $phone = substr($phone,2,10);
        }
        $product = Product::find($request->get('product'));
        $comment = $product->title_ru.' заявка на заказ от клиента '.$request['name'];
        $data = [
            'Time' => time(),
            //'ToTime' => 'date_format:U',
            'Dom' => 127,
            'Flat' => 1,
            'StreetID' => 147,
            'Comment' => $comment,
            'Phone' => $phone,
            'Name' => $request['name'],
            'IP' => '127.0.0.1',
            'skipAstra' => true,
            'Source' => $sourse,
        ];
        $this->setData($data);
    }

    private function prepareDataMapConnect($request, $skip_astra = false, $sourse = '')
    {
        $phone = clearPhoneInput($request['phone']);
        if(strlen($phone) == 12){
            $phone = substr($phone,2,10);
        }

        if($request['exist'] == true){
            $comment = 'Клиент '.$request['name'].' хочет поключиться к сети по адресу: '.$request['address'];
        } else {
            $comment = 'Клиент '.$request['name'].' хочет, чтобы его уведомили о доступности подключения к сети по адресу: '.$request['address'];
        }

        $data = [
            'Time' => time(),
            //'ToTime' => 'date_format:U',
            'Dom' => 127,
            'Flat' => 1,
            'StreetID' => 147,
            'Comment' => $comment,
            'Phone' => $phone,
            'Name' => $request['name'],
            'IP' => '127.0.0.1',
            'skipAstra' => true,
            'Source' => $sourse,
        ];
        $this->setData($data);
    }

    private function prepareCableRentConnect($request)
    {
        $phone = clearPhoneInput($request['phone']);
        if(strlen($phone) == 12){
            $phone = substr($phone,2,10);
        }

        $comment = 'Клиент '.$request['name'].' хочет арендовать оптическое волокно';

        $data = [
            'Time' => time(),
            //'ToTime' => 'date_format:U',
            'Dom' => 127,
            'Flat' => 1,
            'StreetID' => 147,
            'Comment' => $comment,
            'Phone' => $phone,
            'Name' => $request['name'],
            'IP' => '127.0.0.1',
            'skipAstra' => true,
        ];
        $this->setData($data);
    }

    private function checkTimes($datetime)
    {
        $now = Carbon::now();
        $dt_now = strtotime($now);
        $dt_created = strtotime($datetime);
        $delta = $dt_now - $dt_created;
        if($delta > $this->getTimeDelta()){
            //delta between time is more than 5 minutes
            return true;
        } else {
            //delta between time is less than 5 minutes
            $remain = $this->send_time_delta - $delta;
            return $remain;
        }
    }

    private function checkBySession()
    {
        $row = Feedback::where('session_id',session()->getId())->orderBy('id','desc')->first();
        if(!empty($row->created_at)){
            //row exist - checking by datetime
            return $this->checkTimes($row->created_at);
        } else {
            //row not exist - can send
            return true;
        }
    }

    private function checkByPhone($request,$sourse = null)
    {
        $row = Feedback::where('phone',$request->get('phone'))
            ->where('sourse',$sourse)
            ->orderBy('id','desc')
            ->first();
        if(!empty($row->created_at)){
            //row exist - checking by datetime
            return $this->checkTimes($row->created_at);
        } else {
            //row not exist - can send
            return true;
        }
    }

    private function canSendFeedback($request, $sourse = null)
    {
        /*
        $check_phone = $this->checkByPhone($request, $sourse);
        if($check_phone !== true){
            return $check_phone;
        }
        $check_session = $this->checkBySession();
        if($check_session !== true){
            return $check_session;
        }
        */
        return true;
    }

    public function sendOrderDevice($request, $sourse = null)
    {
        $valid = $this->validateDeviceOrder($request);
        if($valid !== true){
            return ['message' => $valid];
        }

        $can_send = $this->canSendFeedback($request, $sourse);
        if($can_send !== true){
            return ['message' => trans('feedback.errors.invalid-times'), 'seconds-left' => $can_send];
        }

        $result = $this->sendDeviceOrderToApi($request, $sourse);
        return $result;
    }

    public function sendMapConnect($request, $sourse = null)
    {
        $valid = $this->validateMapConnect($request);
        if($valid !== true){
            return ['message' => $valid];
        }

        $can_send = $this->canSendFeedback($request, $sourse);
        if($can_send !== true){
            return ['message' => trans('feedback.errors.invalid-times'), 'seconds-left' => $can_send];
        }

        $result = $this->sendMapConnectToApi($request, $sourse);
        return $result;
    }

    public function sendCableRent($request, $sourse = null)
    {
        $valid = $this->validateMapConnect($request);
        if($valid !== true){
            return ['message' => $valid];
        }

        $can_send = $this->canSendFeedback($request, $sourse);
        if($can_send !== true){
            return ['message' => trans('feedback.errors.invalid-times'), 'seconds-left' => $can_send];
        }

        $result = $this->sendCableRentToApi($request, $sourse);
        return $result;
    }

    public function sendOrderFeedback($request, $sourse = null)
    {
        $valid = $this->validateTariffOrder($request);
        if($valid !== true){
            return ['message' => $valid];
        }

        $can_send = $this->canSendFeedback($request, $sourse);
        if($can_send !== true){
            return ['message' => trans('feedback.errors.invalid-times'), 'seconds-left' => $can_send];
        }

        $result = $this->sendTariffOrderToApi($request, $sourse);
        return $result;
    }

    public function sendConnectAction($request, $sourse = null)
    {
        $valid = $this->validateTariffOrder($request);
        if($valid !== true){
            return ['message' => $valid];
        }

        $can_send = $this->canSendFeedback($request, $sourse);
        if($can_send !== true){
            return ['message' => trans('feedback.errors.invalid-times'), 'seconds-left' => $can_send];
        }

        $result = $this->sendConnectActionToApi($request, $sourse);
        return $result;
    }

    public function sendSiteFeedback($request, $skip_astra = false, $sourse = null)
    {
        $valid = $this->validateSiteFeedback($request);
        if($valid !== true){
            //return $valid;
            return ['message' => $valid];
        }

        $can_send = $this->canSendFeedback($request,$sourse);
        if($can_send !== true){
            return ['message' => trans('feedback.invalid-times'), 'seconds-left' => $can_send];
        }

        $result = $this->sendFeedBackToApi($request, $skip_astra, $sourse);
        return $result;
    }

    public function sendTariffOrder($request, $sourse = null)
    {
        $valid = $this->validateTariffOrder($request);
        if($valid !== true){
            //return $valid;
            return ['message' => $valid];
        }

        $can_send = $this->canSendFeedback($request, $sourse);
        if($can_send !== true){
            return ['message' => trans('feedback.invalid-times'), 'seconds-left' => $can_send];
        }

        $result = $this->sendTariffOrderToApi($request, $sourse);
        return $result;
    }

    public function sendCallToMe($request, $sourse = null)
    {
        $valid = $this->validateCallToMe($request);
        if($valid !== true){
            return ['message' => $valid];
        }

        $can_send = $this->canSendFeedback($request,$sourse);
        if($can_send !== true){
            return ['message' => trans('feedback.errors.invalid-times'), 'seconds-left' => $can_send];
        }

        $result = $this->sendFeedBackToApi($request, false, $sourse);
        return $result;
    }

    private function sendFeedBackToApi($request, $skip_astra = false, $sourse = null)
    {
        $this->prepareDataFeedback($request, $skip_astra, $sourse);
        try {
            $billingAPI = BillingApi::get();
            $response = $billingAPI->execute('send-feedback', $this->getData());

/*
                Feedback::create([
                    'session_id' => session()->getId(),
                    'phone' => $request->get('phone'),
                    'sourse' => $sourse
                ]);
*/
        } catch (ApiException $e) {
            return '';
        }
        if(isset($response['success']) && $response['success'] == false){
            return ['success' => false, 'error' => $response['error']];
        }
        if(isset($response['astra']) && $response['astra'] == true){
            return ['success' => true, 'sourse' => 'astra'];
        }
        return ['success' => true, 'sourse' => 'feedback'];
    }

    private function sendTariffOrderToApi($request, $sourse = null)
    {
        $serv = Tariff::find($request->get('service'));
        if($serv->user_type == 2){
            $endpoint = 'send-legal-feedback';
        } else {
            $endpoint = 'send-feedback';
        }
        $this->prepareDataOrder($request, true, $sourse);

            try {
                $billingAPI = BillingApi::get();
                $response = $billingAPI->execute($endpoint, $this->getData());

/*
                Feedback::create([
                    'session_id' => session()->getId(),
                    'phone' => $request->get('phone'),
                    'sourse' => $sourse
                ]);
*/
            } catch (ApiException $e) {

            }
        if(isset($response['success']) && $response['success'] == false){
            return ['success' => false, 'error' => $response['error']];
        }
            if(isset($response['astra']) && $response['astra'] == true){
                return ['success' => true, 'sourse' => 'astra'];
            }
        return ['success' => true, 'sourse' => 'feedback'];
    }

    private function sendConnectActionToApi($request,$sourse = null)
    {
        $endpoint = 'send-feedback';
        $this->prepareDataAction($request, true, $sourse);
            try {
                $billingAPI = BillingApi::get();
                $response = $billingAPI->execute($endpoint, $this->getData());

/*
                Feedback::create([
                    'session_id' => session()->getId(),
                    'phone' => $request->get('phone'),
                    'sourse' => $sourse
                ]);
*/
            } catch (ApiException $e) {

            }
        if(isset($response['success']) && $response['success'] == false){
            return ['success' => false, 'error' => $response['error']];
        }
            if(isset($response['astra']) && $response['astra'] == true){
                return ['success' => true, 'sourse' => 'astra'];
            }
        return ['success' => true, 'sourse' => 'feedback'];
    }

    private function sendDeviceOrderToApi($request, $sourse = null)
    {
        $this->prepareDataDeviceOrder($request, true, $sourse);
            try {
                $billingAPI = BillingApi::get();
                $response = $billingAPI->execute('send-feedback', $this->getData());

                /*
                Feedback::create([
                    'session_id' => session()->getId(),
                    'phone' => $request->get('phone'),
                    'sourse' => $sourse
                ]);
                */
            } catch (ApiException $e) {

            }
        if(isset($response['success']) && $response['success'] == false){
            return ['success' => false, 'error' => $response['error']];
        }
            if(isset($response['astra']) && $response['astra'] == true){
                return ['success' => true, 'sourse' => 'astra'];
            }
        return ['success' => true, 'sourse' => 'feedback'];
    }

    private function sendMapConnectToApi($request, $sourse = null)
    {
        $this->prepareDataMapConnect($request, true, $sourse);
            try {
                $billingAPI = BillingApi::get();
                $response = $billingAPI->execute('send-feedback', $this->getData());

/*
                Feedback::create([
                    'session_id' => session()->getId(),
                    'phone' => $request->get('phone'),
                    'sourse' => $sourse
                ]);
*/
            } catch (ApiException $e) {

            }
        if(isset($response['success']) && $response['success'] == false){
            return ['success' => false, 'error' => $response['error']];
        }
            if(isset($response['astra']) && $response['astra'] == true){
                return ['success' => true, 'sourse' => 'astra'];
            }
        return ['success' => true, 'sourse' => 'feedback'];
    }

    private function sendCableRentToApi($request, $sourse = null)
    {
        $this->prepareCableRentConnect($request);
            try {
                $billingAPI = BillingApi::get();
                $response = $billingAPI->execute('send-feedback', $this->getData());

                /*
                Feedback::create([
                    'session_id' => session()->getId(),
                    'phone' => $request->get('phone'),
                    'sourse' => $sourse
                ]);
                */
            } catch (ApiException $e) {

            }
        if(isset($response['success']) && $response['success'] == false){
            return ['success' => false, 'error' => $response['error']];
        }
            if(isset($response['astra']) && $response['astra'] == true){
                return ['success' => true, 'sourse' => 'astra'];
            }
        return ['success' => true, 'sourse' => 'feedback'];
    }


}
