<?php

namespace App\Services;

use App\Faq;
use App\FaqGroup;
use App\Tariff;

class FaqService
{
    private $answer, $tariffs = [];

    public function __construct()
    {

    }

    private function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    private function getAnswer()
    {
        return $this->answer;
    }

    private function parseAnswer($answer)
    {
        $this->setAnswer($answer);

        $this->parseForService();
        $this->parseForDiff();

        return $this->getAnswer();
    }

    private function parseForDiff()
    {
        while ($this->checkForDiff() == true){
            $this->changeDiff();
        }
    }

    private function parseForService()
    {
        while ($this->checkForService() == true){
            $this->changeService();
        }
    }

    private function checkForDiff()
    {
        if(strpos($this->getAnswer(),'{diff:') !== false){
            return true;
        } else {
            return false;
        }
    }

    private function checkForService()
    {
        if(strpos($this->getAnswer(),'{service:') !== false){
            return true;
        } else {
            return false;
        }
    }

    private function changeDiff()
    {
        $txt = $this->getAnswer();
        $p_start = strpos($txt,'{diff:');
        $txt1 = substr($txt,$p_start);
        $p_finish = strpos($txt1,'}');
        $len = $p_finish + 1;
        $sub_str = substr($txt,$p_start,$len);
        $diff = $this->getTariffsDiffs($sub_str);
        if($diff !== false){
            $new_txt = str_replace($sub_str,$diff,$txt);
            $this->setAnswer($new_txt);
        } else {
            $new_txt = str_replace($sub_str,'',$txt);
            $this->setAnswer($new_txt);
        }
    }

    private function changeService()
    {
        $txt = $this->getAnswer();
        $p_start = strpos($txt,'{service:');
        $txt1 = substr($txt,$p_start);
        $p_finish = strpos($txt1,'}');
        $len = $p_finish + 1;
        $sub_str = substr($txt,$p_start,$len);

        $tariff = $this->getTariffInfo($sub_str);
        if($tariff !== false){
            $this->setAnswer(str_replace($sub_str,$tariff,$txt));
        } else {
            $this->setAnswer(str_replace($sub_str,'',$txt));
        }
    }

    private function getTariffsDiffs($sub_str)
    {
        $sub_str = str_replace(['{','}'],'',$sub_str);
        $arr = explode(':',$sub_str);
        $tariff1 = $this->getFromRememberFaq($arr[1]);
        $tariff2 = $this->getFromRememberFaq($arr[2]);
        $tariff0 = $this->getFromRememberFaq($arr[3]);

        if($tariff1 && $tariff2 && $tariff0){
            $res = ($tariff1->price + $tariff2->price) - $tariff0->price;
            return $res;
        }

        return false;
    }

    private function getTariffInfo($sub_str)
    {
        $sub_str = str_replace(['{','}'],'',$sub_str);
        $arr = explode(':',$sub_str);
        $tariff = $this->getFromRememberFaq($arr[1]);
        if(!empty($tariff)){
            if($arr[2] == 'title'){
                $res = $tariff->{'title' . getLocaleDBSuf()};
            } elseif($arr[2] == 'price'){
                $res = $tariff->price;
            }
        }
        if(isset($res)){
            return $res;
        } else {
            return false;
        }
    }

    private function rememberTariff($tariff)
    {
        $this->tariffs[$tariff->id] = $tariff;
    }

    private function getFromRememberFaq($id)
    {
        if(!isset($this->faqs[$id])){
            $tariff = Tariff::active()->where('id',$id)->first();
            if($tariff){
                $this->rememberTariff($tariff);
            } else {
                return false;
            }
        }
        if(!empty($this->tariffs[$id])){
            return $this->tariffs[$id];
        }
        return false;
    }

    public function parseUnionDescription($description)
    {
        $answer = $this->parseAnswer($description);
        return $answer;
    }

    public function getItemsLastmod()
    {
        $res = Faq::active()->orderBy('updated_at','desc')->first();
        $lastmod = (!empty($res->updated_at)) ? $res->updated_at : false;
        return $lastmod;
    }

    public function getQuestionsForFront($parent = null, $current = null)
    {
        $items = [];
        $res = Faq::active()->where('faq_group_id',$parent['id'])->get();
        if(!empty($res)){
            foreach($res as $r){
                $items[] = [
                    'id' => $r->id,
                    'title' => $r->{'title' . getLocaleDBSuf()},
                    'slug' => $r->slug,
                    'url' => getLocaleHrefPrefix().'/faq/'.$r->parent->slug.'/'.$r->slug,
                    'current' => (!empty($current) && $current['id'] == $r->id) ? true : false,
                ];
            }
        }
        return $items;
    }

    public function getQuestionsFullList()
    {
        $items = [];
        $res = Faq::active()->get();
        if(!empty($res)){
            foreach($res as $r){
                $items[] = [
                    'id' => $r->id,
                    'title' => $r->{'title' . getLocaleDBSuf()},
                    'slug' => $r->slug,
                    'url' => getLocaleHrefPrefix().'/faq/'.$r->parent->slug.'/'.$r->slug,
                ];
            }
        }
        return $items;
    }

    public function getFaqGroupsForFront()
    {
        $faqs = [];
        $groups = FaqGroup::active()->orderBy('sort','asc')->get();
        if(!empty($groups)){
            foreach($groups as $group){
                $count = Faq::where('faq_group_id',$group->id)->count();
                if($count > 0){
                    $faqs[] = [
                        'id' => $group->id,
                        'title' => $group->{'title' . getLocaleDBSuf()},
                        'icon' => $group->icon,
                        'slug' => $group->slug,
                        'url' => getLocaleHrefPrefix().'/faq/'.$group->slug,
                    ];
                }
            }
        }
        return $faqs;
    }

    public function getFaqForFront($slug)
    {
        $group = FaqGroup::active()->where('slug',$slug)->first();
        if($group){
            $faq = [
                'id' => $group->id,
                'title' => $group->{'title' . getLocaleDBSuf()},
                'icon' => $group->icon,
                'slug' => $group->slug,
                'url' => getLocaleHrefPrefix().'/faq/'.$group->slug,
                'detail' => $group->{'description' . getLocaleDBSuf()},
                'meta_title' => (!empty($group->{'meta_title' . getLocaleDBSuf()})) ? $group->{'meta_title' . getLocaleDBSuf()} : $group->{'title' . getLocaleDBSuf()}.' - '.trans('faq.meta-title'),
                'meta_description' => (!empty($group->{'meta_description' . getLocaleDBSuf()})) ? $group->{'meta_description' . getLocaleDBSuf()} : trans('faq.meta-description-1').' "'.$group->{'title' . getLocaleDBSuf()}.'" '.trans('faq.meta-description-2'),
                'meta_keywords' => $group->{'meta_keywords' . getLocaleDBSuf()},
                'lastmod' => $group->updated_at,
            ];
            return $faq;
        }
        return [];
    }

    public function getFaqAnswerForFront($parentSlug, $slug)
    {
        $parent = FaqGroup::active()->where('slug',$parentSlug)->first();
        $res = Faq::active()->where('slug',$slug)->where('faq_group_id',$parent->id)->first();
        if($res){
            $answer = [
                'id' => $res->id,
                'title' => $res->{'title' . getLocaleDBSuf()},
                'slug' => $res->slug,
                'url' => getLocaleHrefPrefix().'/faq/'.$res->slug,
                'detail' => $res->{'answer' . getLocaleDBSuf()},
                'meta_title' => (!empty($res->{'meta_title' . getLocaleDBSuf()})) ? $res->{'meta_title' . getLocaleDBSuf()} : $res->{'title' . getLocaleDBSuf()}.': '.$parent->{'title' . getLocaleDBSuf()}.' - '.trans('faq.child-meta-title'),
                'meta_description' => (!empty($res->{'meta_description' . getLocaleDBSuf()})) ? $res->{'meta_description' . getLocaleDBSuf()} : $res->{'title' . getLocaleDBSuf()}.'. '.trans('faq.child-meta-description'),
                'meta_keywords' => $res->{'meta_keywords' . getLocaleDBSuf()},
                'lastmod' => $res->updated_at,
            ];
            return $answer;
        }
        return [];
    }

    public function search($request)
    {
        $res = [];
        if($request->get('search') != ''){
            $arr = explode(' ',$request->get('search'));
            if(!empty($arr)) {
                $query_ru = Faq::active();
                $query_ua = Faq::active();
                foreach ($arr as $a) {
                    if ($a != '') {
                        $query_ru->where('title_ru', 'LIKE', '%' . $a . '%');
                        $query_ua->where('title_ua', 'LIKE', '%' . $a . '%');
                    }
                }
                $res_ru = $query_ru->get();
                $res_ua = $query_ua->get();
                if (!empty($res_ru)) {
                    foreach ($res_ru as $r) {
                        if (!isset($res[$r->id])) {
                            $res[$r->id] = [
                                'id' => $r->id,
                                'group_id' => $r->faq_group_id,
                                'title' => $r->{'title' . getLocaleDBSuf()},
                            ];
                        }
                    }
                }
                if (!empty($res_ua)) {
                    foreach ($res_ua as $r) {
                        if (!isset($res[$r->id])) {
                            $res[$r->id] = [
                                'id' => $r->id,
                                'group_id' => $r->faq_group_id,
                                'title' => $r->{'title' . getLocaleDBSuf()},
                            ];
                        }
                    }
                }
            }

            $query_ru = Faq::active()->where('search', 'LIKE', '%' . $request->get('search') . '%');
            $query_ua = Faq::active()->where('search', 'LIKE', '%' . $request->get('search') . '%');
            $res_ru = $query_ru->get();
            $res_ua = $query_ua->get();
            if (!empty($res_ru)) {
                foreach ($res_ru as $r) {
                    if (!isset($res[$r->id])) {
                        $res[$r->id] = [
                            'id' => $r->id,
                            'group_id' => $r->faq_group_id,
                            'title' => $r->{'title' . getLocaleDBSuf()},
                        ];
                    }
                }
            }
            if (!empty($res_ua)) {
                foreach ($res_ua as $r) {
                    if (!isset($res[$r->id])) {
                        $res[$r->id] = [
                            'id' => $r->id,
                            'group_id' => $r->faq_group_id,
                            'title' => $r->{'title' . getLocaleDBSuf()},
                        ];
                    }
                }
            }
        }
        return $res;
    }

    public function searchFull($request)
    {
        $res = [];
        if($request->get('search') != ''){
            $arr = explode(' ',$request->get('search'));
            if(!empty($arr)) {
                $query_ru = Faq::with('parent')->active();
                $query_ua = Faq::with('parent')->active();
                $query_en = Faq::with('parent')->active();
                foreach ($arr as $a) {
                    if ($a != '') {
                        $query_ru->where('title_ru', 'LIKE', '%' . $a . '%');
                        $query_ua->where('title_ua', 'LIKE', '%' . $a . '%');
                        $query_en->where('title_en', 'LIKE', '%' . $a . '%');
                    }
                }
                $res_ru = $query_ru->get();
                $res_ua = $query_ua->get();
                $res_en = $query_en->get();
                if (!empty($res_ru)) {
                    foreach ($res_ru as $r) {
                        if (!isset($res[$r->id])) {
                            $res[$r->id] = [
                                'id' => $r->id,
                                'group_id' => $r->faq_group_id,
                                'title' => $r->{'title' . getLocaleDBSuf()},
                                'answer' => $r->{'answer' . getLocaleDBSuf()},
                                'category_title' => $r->parent->{'title' . getLocaleDBSuf()},
                                'url' => getLocaleHrefPrefix().'/faq/'.$r->parent->slug,
                            ];
                        }
                    }
                }
                if (!empty($res_ua)) {
                    foreach ($res_ua as $r) {
                        if (!isset($res[$r->id])) {
                            $res[$r->id] = [
                                'id' => $r->id,
                                'group_id' => $r->faq_group_id,
                                'title' => $r->{'title' . getLocaleDBSuf()},
                                'answer' => $r->{'answer' . getLocaleDBSuf()},
                                'category_title' => $r->parent->{'title' . getLocaleDBSuf()},
                                'url' => getLocaleHrefPrefix().'/faq/'.$r->parent->slug,
                            ];
                        }
                    }
                }
                if (!empty($res_en)) {
                    foreach ($res_en as $r) {
                        if (!isset($res[$r->id])) {
                            $res[$r->id] = [
                                'id' => $r->id,
                                'group_id' => $r->faq_group_id,
                                'title' => $r->{'title' . getLocaleDBSuf()},
                                'answer' => $r->{'answer' . getLocaleDBSuf()},
                                'category_title' => $r->parent->{'title' . getLocaleDBSuf()},
                                'url' => getLocaleHrefPrefix().'/faq/'.$r->parent->slug,
                            ];
                        }
                    }
                }
            }


            $query = Faq::with('parent')->active()->where('search', 'LIKE', '%' . $request->get('search') . '%');
            $resmore = $query->get();
            if (!empty($resmore)) {
                foreach ($resmore as $r) {
                    if (!isset($res[$r->id])) {
                        $res[$r->id] = [
                            'id' => $r->id,
                            'group_id' => $r->faq_group_id,
                            'title' => $r->{'title' . getLocaleDBSuf()},
                            'answer' => $r->{'answer' . getLocaleDBSuf()},
                            'category_title' => $r->parent->{'title' . getLocaleDBSuf()},
                            'url' => getLocaleHrefPrefix().'/faq/'.$r->parent->slug,
                        ];
                    }
                }
            }
        }
        return $res;
    }

}
