<?php

namespace App\Services;

use App\Category;
use App\Core\Lib\Billing\BillingApi;
use App\Product;
use App\ProductComment;
use Carbon\Carbon;
use DB;

class ProductCommentService
{
    const ADMIN_PAGINATE = 30;
    const FRONT_PER_PAGE = 5;

    private $filters, $sort = ['field' => 'created_at', 'direction' => 'desc'], $id;

    public function __construct()
    {

    }

    private function setId($id)
    {
        $this->id = $id;
    }

    private function getId()
    {
        return $this->id;
    }

    public function getEmails()
    {
        $res = DB::table('product_comments_emails')->select()->orderBy('email','asc')->get();
        return $res;
    }

    public function saveEmail($email)
    {
        $row = DB::table('product_comments_emails')->select()->where('email','=',$email)->first();
        if(isset($row->id)){
            //exist
        } else {
            DB::table('product_comments_emails')->insert([
                'email' => $email,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }

    public function deleteEmail($id)
    {
        DB::table('product_comments_emails')->where('id','=',$id)->delete();
    }

    public function informToEmails($comment)
    {
        $emails = DB::table('product_comments_emails')->select()->get();
        if(!empty($emails)){
            $product = Product::select('id','title_ru')->where('id',$comment->product_id)->first();
            foreach($emails as $email){
                if($email->email != ''){
                    $this->sendEmail($email, $comment, $product);
                }
            }
        }
    }

    private function sendEmail($email, $comment, $product)
    {
        $to = $email->email;
        $subject = "Новый отзыв на товар";
        $message = '
<html>
<head>
 <title>Новый отзыв на товар '.$product->title_ru.'</title>
</head>
<body>
<p>Новый отзыв на товар '.$product->title_ru.'</p>
<p>Автор: '.$comment->name.'</p>
<p>Отзыв: '.$comment->comment.'</p>
</body>
</html>
';
        $headers= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

        $headers .= "From: Briz <admin@briz.ua>\r\n";
        if(mail($to, $subject, $message, $headers)){
            echo 'send to '.$to;
        } else {
            echo 'send failed to '.$to;
        }

    }

    public function getCommentsForFront($request)
    {
        $comments = [];
        $page = ($request->get('page') > 0) ? $request->get('page') : 1;
        $items = ProductComment::with('children')->isParent()->moderated()->orderBy('created_at','desc')->paginate(self::FRONT_PER_PAGE);
        $all_count = ProductComment::isParent()->moderated()->count();
        $pages = ceil($all_count / self::FRONT_PER_PAGE);
        $has_more = ($page < $pages) ? true : false;
        if(!empty($items)){
            foreach($items as $comment){
                $children = [];
                if(!empty($comment->children)){
                    foreach($comment->children as $child){
                        $children[] = $this->prepareDataForFront($child);
                    }
                }
                $comments[] = $this->prepareDataForFront($comment,$children);
            }
        }

        return [
            'comments' => $comments,
            'page' => $page,
            'has_more' => $has_more,
        ];
    }

    private function prepareDataForFront($comment, $children = [])
    {
        $dt = strtotime($comment->created_at);
        $item = [
            'id' => $comment->id,
            'name' => $comment->name,
            'comment' => $comment->comment,
            'date' => date('H:i:s d.m.Y',$dt),
            'rate' => $comment->rate,
            'children' => $children,
        ];
        return $item;
    }

    public function getItemsForAdmin($request)
    {
        $this->setFilter($request);
        $this->setSorting($request);

        $items = $this->buildQuery()->paginate(self::ADMIN_PAGINATE);

        return ['items' => $items, 'filter' => $this->getFilters(), 'sort' => $this->getSorting()];
    }

    private function buildQuery()
    {
        $query = ProductComment::with('product');


        if(isset($this->filters['moderated'])){
            if($this->filters['moderated'] == 1){
                $query->whereNotNull('moderated_at');
            } else {
                $query->whereNull('moderated_at');
            }
        }

        if(!empty($this->filters['id'])){
            $query->where('id',$this->filters['id']);
        }

        if(!empty($this->filters['product_id'])){
            $query->where('product_id',$this->filters['product_id']);
        }

        if(isset($this->filters['search'])){
            $query->where(function ($query2) {
                $query2->where('name', 'LIKE', '%'.$this->filters['search'].'%')
                    ->orWhere('comment', 'LIKE', '%'.$this->filters['search'].'%');
            });
        }

        $query->orderBy($this->sort['field'],$this->sort['direction']);
        return $query;
    }

    private function setFilter($input)
    {
        $upd = false;
        if(isset($input['id']))
        {
            $this->filters['id'] = $input['id'];
            $upd = true;
        }
        if(isset($input['product_id']))
        {
            $this->filters['product_id'] = $input['product_id'];
            $upd = true;
        }
        if(isset($input['search']))
        {
            $this->filters['search'] = $input['search'];
            $upd = true;
        }
        if(isset($input['moderated']))
        {
            $this->filters['moderated'] = $input['moderated'];
            $upd = true;
        }
        if($upd == true){
            session(['filtersComments' => $this->filters]);
        } else {
            $sess_filter = session('filtersComments');
            if(!empty($sess_filter)){
                $this->filters = $sess_filter;
            }
        }
    }

    private function getFilters()
    {
        return $this->filters;
    }

    private function setSorting($input)
    {
        if(!empty($input['field']) && !empty($input['direction'])){
            $this->sort = ['field' => $input['field'], 'direction' => $input['direction']];
        }
    }

    private function getSorting()
    {
        return $this->sort;
    }







}
