<?php


namespace App\Services\Channels;

use App\Channel;
use App\ChannelGenre;
use App\Services\Channels\Dto\GenreDto;
use App\Services\Channels\Image\Saver;
use App\Services\Genre\GenreDBSaver;

class ChannelGenreSaver
{
    public function save($dbGenre,$channelModel)
    {
        $genreSaver = new GenreDBSaver();
        $genre = $genreSaver->save($dbGenre);

        $data = [
            'channel_id' => $channelModel->id,
            'genre_id' => $genre->id
        ];
        ChannelGenre::updateOrCreate($data, ['channel_id' => $channelModel->id, 'genre_id' => $genre->id]);

        return true;
    }

}
