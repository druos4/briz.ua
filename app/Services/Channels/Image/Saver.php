<?php


namespace App\Services\Channels\Image;


use App\Channel;
use App\Services\Channels\Dto\ChannelDto;
use App\Services\Channels\Dto\GenreDto;

class Saver implements SaverInterface
{

    protected $path = "uploads/channels";

    public function __construct()
    {
        $this->createDir();
    }

    public function save(ChannelDto $channel): string
    {
        try {

            $img = \Image::make($channel->getBillingLogo());
            $mime = $img->mime();
            $arr = explode('/', $mime);
            $analog = ($channel->getIsAnalog() == true) ? '1' : '0';
            $new_path = "{$this->path}/channel-" . $channel->getBillingId() . '-'.$analog.'-' . date('YmdHis') . '.' . $arr[1];
            $img->save(public_path($new_path), 100);

            $this->deleteOld($channel);

            return '/' . $new_path;
        } catch (\Exception $exception) {
            throw new \Exception($channel->title . ' [' . $channel->getBillingId() . '] --> ' . $channel->getLogo() . " {$exception->getMessage()}");
        }
    }

    protected function createDir()
    {
        if (!\File::exists(public_path($this->path))) {
            \File::makeDirectory(public_path($this->path));
        }
    }

    protected function deleteOld($channel)
    {
        $channel = Channel::where('billing_id', $channel->getBillingId())->where('is_analog',$channel->getIsAnalog())->first();
        if (empty($channel)) {
            return true;
        }

        if (!empty($channel->logo) && file_exists(public_path($channel->logo))) {
            return unlink(public_path($channel->logo));
        }

        return true;

    }

}
