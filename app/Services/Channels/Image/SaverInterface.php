<?php


namespace App\Services\Channels\Image;


use App\Services\Channels\Dto\ChannelDto;

interface SaverInterface
{

    public function save(ChannelDto $channel): string;

}
