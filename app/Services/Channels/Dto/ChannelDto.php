<?php


namespace App\Services\Channels\Dto;


class ChannelDto
{
    public $title = null;

    public $logo = null;

    public $string = null;

    public $sort = null;

    public $billing_id = null;

    public $billing_logo = null;

    public $is_analog = null;

    public $parsed = null;

    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param null $logo
     */
    public function setLogo($logo): void
    {
        $this->logo = $logo;
    }

    /**
     * @return null
     */
    public function getString()
    {
        return $this->string;
    }

    /**
     * @param null $string
     */
    public function setString($string): void
    {
        $this->string = $string;
    }

    /**
     * @return null
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param null $sort
     */
    public function setSort($sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return null
     */
    public function getBillingId()
    {
        return $this->billing_id;
    }

    /**
     * @param null $billing_id
     */
    public function setBillingId($billing_id): void
    {
        $this->billing_id = $billing_id;
    }

    /**
     * @return null
     */
    public function getBillingLogo()
    {
        return $this->billing_logo;
    }

    /**
     * @param null $billing_logo
     */
    public function setBillingLogo($billing_logo): void
    {
        $this->billing_logo = $billing_logo;
    }

    /**
     * @return null
     */
    public function getIsAnalog()
    {
        return $this->is_analog;
    }

    /**
     * @param null $is_analog
     */
    public function setIsAnalog($is_analog): void
    {
        $this->is_analog = $is_analog;
    }

    /**
     * @return null
     */
    public function getParsed()
    {
        return $this->parsed;
    }

    /**
     * @param null $parsed
     */
    public function setParsed($parsed): void
    {
        $this->parsed = $parsed;
    }

    public function toArray()
    {
        return [
            'title' => $this->getTitle(),
            'logo' => $this->getLogo(),
            'string' => $this->getString(),
            'sort' => $this->getSort(),
            'billing_id' => $this->getBillingId(),
            'billing_logo' => $this->getBillingLogo(),
            'is_analog' => $this->getIsAnalog(),
            'parsed' => $this->getParsed()
        ];
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray());
    }

}
