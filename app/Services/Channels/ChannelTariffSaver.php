<?php


namespace App\Services\Channels;

use App\Channel;
use App\ChannelGenre;
use App\ChannelTarif;
use App\Services\Channels\Dto\GenreDto;
use App\Services\Channels\Image\Saver;
use App\Services\Genre\GenreDBSaver;
use App\Tariff;

class ChannelTariffSaver
{
    public function save($channelModel,$billingIds)
    {
        if(!empty($billingIds)){
            foreach($billingIds as $billingId){
                $tariff = Tariff::select('id')->where('billing_id',$billingId)->first();
                $data = [
                    'channel_id' => $channelModel->id,
                    'tariff_id' => $tariff->id
                ];
                ChannelTarif::updateOrCreate($data, ['channel_id' => $channelModel->id, 'tariff_id' => $tariff->id]);
            }
        }

        return true;
    }

}
