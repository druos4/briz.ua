<?php


namespace App\Services\Channels;


use App\Channel;
use App\Services\Channels\Dto\ChannelDto;
use App\Services\Channels\Dto\GenreDto;

abstract class ChannelSaverDecorator implements ChannelSaverInterface
{

    public $decoree = null;

    public function __construct(ChannelSaverInterface $decoree)
    {

        $this->decoree = $decoree;

    }

    public function save(ChannelDto $channel): Channel
    {
        return $this->decoree->save($channel);
    }


}
