<?php


namespace App\Services\Channels\Parser;


use App\Channel;
use App\ChannelGenre;
use App\ChannelTarif;
use App\Services\Channels\Parser\Dto\ChannelsCollection;
use App\Services\Channels\Parser\Dto\ChannelDto;
use Illuminate\Support\Facades\Http;
use DB;

class Parser implements ParserInterface
{

    /**
     * @return array
     */
    public function process($url): ChannelsCollection
    {
        $response = Http::get($url);
        $arr = $response->json();

        $channelCollection = new ChannelsCollection();

        if (!empty($arr['data'])) {
            foreach ($arr['data'] as $item) {

                $channelDto = new ChannelDto();
                $channelDto->setId($item['id']);
                $channelDto->setGenre($item['Genre']);
                $channelDto->setName($item['Name']);
                $channelDto->setLogo($item['Logo']);
                $channelDto->setOrderNum($item['OrderNum']);

                $channelCollection->add($channelDto);
            }
        }

        return $channelCollection;
    }

    /**
     * установка метки обработки в 0
     * @return boolean
     */
    public function beforeProcess()
    {
        Channel::query()->update(['parsed' => 0]);
        return true;
    }

    /**
     * очистка не актуальных каналов, которые не обнеовлялись
     * @return int
     */
    public function afterProcess()
    {
        $channels = Channel::where('parsed',0)->pluck('id');
        if(!empty($channels)){
            ChannelTarif::whereIn('channel_id',$channels)->delete();
            ChannelGenre::whereIn('channel_id',$channels)->delete();
            Channel::where('parsed',0)->delete();
        }
        return count($channels);
    }
}
