<?php


namespace App\Services\Channels\Parser;


interface ParserInterface
{

    public function process($url);

}
