<?php


namespace App\Services\Channels\Parser\Dto;


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;


class ChannelDto implements Arrayable, Jsonable
{

    public $id = null;

    public $Name = null;

    public $Genre = null;

    /**
     * Это поле отвечающее за сортировку
     */
    public $OrderNum = null;

    public $Logo = null;

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param null $Name
     */
    public function setName($Name): void
    {
        $this->Name = $Name;
    }

    /**
     * @return null
     */
    public function getGenre()
    {
        return $this->Genre;
    }

    /**
     * @param null $Genre
     */
    public function setGenre($Genre): void
    {
        $this->Genre = $Genre;
    }

    /**
     * @return null
     */
    public function getOrderNum()
    {
        return $this->OrderNum;
    }

    /**
     * @param null $OrderNum
     */
    public function setOrderNum($OrderNum): void
    {
        $this->OrderNum = $OrderNum;
    }

    /**
     * @return null
     */
    public function getLogo()
    {
        return $this->Logo;
    }

    /**
     * @param null $Logo
     */
    public function setLogo($Logo): void
    {
        $this->Logo = $Logo;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'Name' => $this->getName(),
            'Genre' => $this->getGenre(),
            'OrderNum' => $this->getOrderNum(),
            'Logo' => $this->getLogo()
        ];
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray());
    }


}
