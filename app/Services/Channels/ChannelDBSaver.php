<?php


namespace App\Services\Channels;


use App\Channel;
use App\Services\Channels\Dto\ChannelDto;
use App\Services\Channels\Dto\GenreDto;

class ChannelDBSaver implements ChannelSaverInterface
{
    public function save(ChannelDto $channel): Channel
    {
        return Channel::updateOrCreate(['billing_id' => $channel->getBillingId(), 'is_analog' => $channel->getIsAnalog()],$channel->toArray());
    }

}
