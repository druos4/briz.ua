<?php


namespace App\Services\Channels;


use App\Channel;
use App\Services\Channels\Dto\ChannelDto;

interface ChannelSaverInterface
{

    public function save(ChannelDto $channel): Channel;

}
