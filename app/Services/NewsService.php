<?php

namespace App\Services;

use App\News;
use App\NewsRecomend;
use App\NewsTag;
use App\Tag;
use App\User;
use Carbon\Carbon;

class NewsService
{
    const API_LIMIT_NEWS = 2;
    const API_LIMIT_SHARES = 1;
    const PAGINATION_ADMIN = 30;
    const FISRT_LIMIT = 4;
    const PAGINATION_CLIENT = 6;

    const TABLET_WIDTH = 1260;
    const TABLET_HEIGHT = 520;
    const TABLET_X = 543;
    const TABLET_Y = 0;
    const PHONE_WIDTH = 820;
    const PHONE_HEIGHT = 420;
    const PHONE_X = 770;
    const PHONE_Y = 50;
    const PREV_FIRST_WIDTH = 612;
    const PREV_FIRST_HEIGHT = 312;
    const PREV_WIDTH = 400;
    const PREV_HEIGHT = 204;

    private $news, $entity_type, $filters, $sort = ['field' => 'id', 'direction' => 'desc'], $id;

    public function __construct($entity_type = null)
    {
        $this->setEntityType($entity_type);
    }

    private function setEntityType($entity_type)
    {
        $this->entity_type = $entity_type;
    }

    private function getEntityType()
    {
        return $this->entity_type;
    }

    private function setId($id)
    {
        $this->id = $id;
    }

    private function getId()
    {
        return $this->id;
    }

    public function checkSlug($request, $id = '')
    {
        $query = News::where('slug',$request->get('slug'));
        if($this->getEntityType() == 'news'){
            $query->isNews();
        } elseif($this->getEntityType() == 'actions'){
            $query->isActions();
        }
        if($request->get('id') > 0){
            $query->where('id','!=',$request->get('id'));
        }
        $item = $query->first();
        if(isset($item->id) && $item->id > 0){
            return false;
        }
        return true;
    }

    private function buildQuery()
    {
        $query = News::with('updater','tags');

        if($this->getEntityType() == 'news'){
            $query->isNews();
        } elseif($this->getEntityType() == 'actions'){
            $query->isActions();
        }

        if(isset($this->filters['tag'])){
            $query = $query->whereHas('tags', function ($subquery) {
                return $subquery->whereIn('tags.id',$this->filters['tag']);
            });
        }

        if(isset($this->filters['active'])){
            if($this->filters['active'] == 1){
                $query->active();
            } else {
                $query->inActive();
            }
        }

        if(isset($this->filters['top'])){
            if($this->filters['top'] == 1){
                $query->top();
            } else {
                $query->withoutTop();
            }
        }

        if(isset($this->filters['id'])){
            $query->where('id',$this->filters['id']);
        }

        if(isset($this->filters['slug'])){
            $query->where('slug','LIKE', '%'.$this->filters['slug'].'%');
        }

        if(isset($this->filters['deleted']) && $this->filters['deleted'] == 1){
            $query->whereNotNull('deleted_at');
        } else {
            $query->whereNull('deleted_at');
        }

        if(isset($this->filters['title'])){
            $query->where(function ($query2) {
                $query2->where('title_ru', 'LIKE', '%'.$this->filters['title'].'%')
                    ->orWhere('title_ua', 'LIKE', '%'.$this->filters['title'].'%')
                    ->orWhere('title_en', 'LIKE', '%'.$this->filters['title'].'%');
            });
        }

        $query->orderBy($this->sort['field'],$this->sort['direction']);
        return $query;
    }

    public function getItemsForAdmin($input)
    {
        $this->setFilter($input);
        $this->setSorting($input);

        $items = $this->buildQuery()->paginate(self::PAGINATION_ADMIN);

        return ['items' => $items, 'filter' => $this->getFilters(), 'sort' => $this->getSorting()];
    }

    public function getFilterForAdmin()
    {
        return $this->getFilters();
    }

    public function getSortingForAdmin()
    {
        return $this->getSorting();
    }

    private function setFilter($input)
    {
        $upd = false;
        if(isset($input['active']))
        {
            $this->filters['active'] = $input['active'];
            $upd = true;
        }
        if(isset($input['top']))
        {
            $this->filters['top'] = $input['top'];
            $upd = true;
        }
        if(!empty($input['id']))
        {
            $this->filters['id'] = $input['id'];
            $upd = true;
        }
        if(!empty($input['slug']))
        {
            $this->filters['slug'] = $input['slug'];
            $upd = true;
        }
        if(!empty($input['title']))
        {
            $this->filters['title'] = $input['title'];
            $upd = true;
        }
        if(!empty($input['deleted']))
        {
            $this->filters['deleted'] = $input['deleted'];
            $upd = true;
        }
        if(!empty($input['tag']))
        {
            $this->filters['tag'] = $input['tag'];
            $upd = true;
        }
        if($this->getEntityType() == 'actions'){
            $sess_name = 'filtersActions';
        } else {
            $sess_name = 'filtersNews';
        }

        if($upd == true){
            session([$sess_name => $this->filters]);
        } else {
            $sess_filter = session($sess_name);
            if(!empty($sess_filter)){
                $this->filters = $sess_filter;
            }
        }

    }

    private function getFilters()
    {
        return $this->filters;
    }

    private function setSorting($input)
    {
        if(!empty($input['field']) && !empty($input['direction'])){
            $this->sort = ['field' => $input['field'], 'direction' => $input['direction']];
        }
    }

    private function getSorting()
    {
        return $this->sort;
    }



    public function getNewsList()
    {
        $items = [];
        $items['top'] = News::active()->top()->orderBy('show_from','desc')->get();
        return $items;
    }

    public function getNewsByTag($tag_id)
    {
        return News::active()->whereHas('tags', function ($subquery) use ($tag_id) {
            return $subquery->where('tags.id',$tag_id);
        })->orderBy('show_from','desc')->paginate(10);
    }

    public function getNewsTags()
    {
        return Tag::whereNull('deleted_at')
            ->orderBy('title_ua','asc')
            ->get();
    }

    private function savePicture($request)
    {
        if ($request->hasFile('image'))
        {
            $suf = date('YmdHis');
            $f_name = strtolower('News').'-'.$this->getId().'-'.$suf.'-hd.'.$request->file('image')->getClientOriginalExtension();
            $file = $request->file('image');
            $path = '/uploads/news/';
            $file->move(public_path($path),$f_name);
            $data['picture'] = $path.$f_name;
            /*
            $img = \Image::make($request->file('image'));
            $img->resize(1920, 520, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/news/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture'] = $path;
            */


            $f_name = strtolower('News').'-'.$this->getId().'-'.$suf.'-tablet.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make(public_path($data['picture']));
            $img->crop(self::TABLET_WIDTH, self::TABLET_HEIGHT, self::TABLET_X, self::TABLET_Y);
            $path = '/uploads/news/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture_tablet'] = $path;

            $f_name = strtolower('News').'-'.$this->getId().'-'.$suf.'-phone.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make(public_path($data['picture']));
            $img->crop(self::PHONE_WIDTH, self::PHONE_HEIGHT, self::PHONE_X, self::PHONE_Y);
            $path = '/uploads/news/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture_phone'] = $path;

            $f_name = strtolower('News').'-'.$this->getId().'-'.$suf.'-prevf.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make(public_path($data['picture']));
            $img->crop(self::PHONE_WIDTH, self::PHONE_HEIGHT, self::PHONE_X, self::PHONE_Y);
            $img->resize(self::PREV_FIRST_WIDTH, self::PREV_FIRST_HEIGHT, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/news/'.$f_name;
            $img->save(public_path($path), 100);
            $data['thumbnail_first'] = $path;

            $f_name = strtolower('News').'-'.$this->getId().'-'.$suf.'-prev.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make(public_path($data['picture']));
            $img->crop(self::PHONE_WIDTH, self::PHONE_HEIGHT, self::PHONE_X, self::PHONE_Y);
            $img->resize(self::PREV_WIDTH, self::PREV_HEIGHT, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/news/'.$f_name;
            $img->save(public_path($path), 100);
            $data['thumbnail'] = $path;

            News::where('id',$this->getId())->update($data);
        }
        return true;
    }

    public function storeItem($input)
    {
        $input['detail_ru'] = (!empty($input['detail_ru'])) ? $input['detail_ru'] : '';
        $input['created_by'] = $input['updated_by'] = auth()->id();
        $input['active'] = (isset($input['active']) && $input['active'] == 1) ? 1 : 0;
        $input['can_connect'] = (isset($input['can_connect']) && $input['can_connect'] == 1) ? true : false;
        $input['on_top'] = (isset($input['on_top']) && $input['on_top'] == 1) ? 1 : 0;
        $input['entity_type'] = $this->getEntityType();

        if($input['publish_date'] != '' && $input['publish_time'] != ''){
            $input['show_from'] = $input['publish_date'].' '.$input['publish_time'].':00';
        } else {
            $input['show_from'] = Carbon::now();
        }

        $news = News::create($input->all());
        $this->setId($news->id);

        $this->savePicture($input);

        $this->syncRecomends($input);

        $log = new LogService();
        $log->saveLog('News',$news);

        if(!empty($input['tags'])){
            $this->syncWithTags($input['tags']);
        }

        return $this->getId();
    }

    private function syncWithTags($tags)
    {
        $this->unsyncTags();
        foreach($tags as $tag){
            $data = [
                'news_id' => $this->getId(),
                'tag_id' => $tag,
            ];
            NewsTag::create($data);
        }
    }

    private function clearTags($tags)
    {
        $this->unsyncTags();
    }

    private function clearPics($news)
    {
        if(file_exists(public_path($news->picture))){
            unlink(public_path($news->picture));
        }
        if(file_exists(public_path($news->picture_tablet))){
            unlink(public_path($news->picture_tablet));
        }
        if(file_exists(public_path($news->picture_phone))){
            unlink(public_path($news->picture_phone));
        }
        if(file_exists(public_path($news->thumbnail_first))){
            unlink(public_path($news->thumbnail_first));
        }
        if(file_exists(public_path($news->thumbnail))){
            unlink(public_path($news->thumbnail));
        }
    }

    public function updateItem($input, $news)
    {
        $this->setId($news->id);

        $input['detail_ru'] = (!empty($input['detail_ru'])) ? $input['detail_ru'] : '';
        $input['created_by'] = $input['updated_by'] = auth()->id();
        $input['active'] = (isset($input['active']) && $input['active'] == 1) ? 1 : 0;
        $input['can_connect'] = (isset($input['can_connect']) && $input['can_connect'] == 1) ? true : false;
        $input['on_top'] = (isset($input['on_top']) && $input['on_top'] == 1) ? 1 : 0;

        if($input->get('del-pic') == 1){
            $this->clearPics($news);
            $input['picture'] = null;
            $input['picture_tablet'] = null;
            $input['picture_phone'] = null;
            $input['thumbnail_first'] = null;
            $input['thumbnail'] = null;
        }

        if ($input->hasFile('image')) {
            $this->savePicture($input);
        }
        if($input['publish_date'] != '' && $input['publish_time'] != ''){
            $input['show_from'] = $input['publish_date'].' '.$input['publish_time'].':00';
        } else {
            $input['show_from'] = Carbon::now();
        }



        $this->syncRecomends($input);

        $log = new LogService();
        $log->saveLog('News',$news, $input->all());

        if(!empty($input['tags'])){
            $this->syncWithTags($input['tags']);
        } else {
            $this->unsyncTags();
        }


        $news->update($input->all());
        return $this->getId();
    }

    private function syncRecomends($input)
    {
        NewsRecomend::where('news_id',$this->getId())->delete();
        if(!empty($input['recomends'])){
            foreach($input['recomends'] as $in){
                NewsRecomend::create([
                    'news_id' => $this->getId(),
                    'recomend' => $in
                ]);
            }
        }
    }

    private function unsyncTags()
    {
        NewsTag::where('news_id',$this->getId())->delete();
    }

    public function deleteItem($news)
    {
        $this->setId($news->id);
        $this->unsyncTags();
        $news->deleted_by = auth()->id();
        $news->save();
        $news->delete();
    }

    private function prepareDataForFront($item)
    {
        $recomends = [];
        if(!empty($item->recomends)){
            foreach($item->recomends as $recomend){
                $url = getLocaleHrefPrefix().'/'.$recomend->entity_type.'/'.$recomend->slug;
                $published = (!empty($recomend->show_from)) ? $recomend->show_from : $recomend->created_at;
                $dt = strtotime($published);
                $recomends[] = [
                    'id' => $recomend->id,
                    'title' => $recomend->{'title' . getLocaleDBSuf()},
                    'slug' => $recomend->slug,
                    'url' => $url,
                    'anons' => addNofollowToLink($recomend->{'anons' . getLocaleDBSuf()}),
                    'thumbnail' => $recomend->thumbnail,
                    'thumbnail_first' => $recomend->thumbnail_first,
                    'published' => date('d.m.Y',$dt),
                ];
            }
        }
        $url = getLocaleHrefPrefix().'/'.$item->entity_type.'/'.$item->slug;
        $published = (!empty($item->show_from)) ? $item->show_from : $item->created_at;
        $dt = strtotime($published);

        switch ($item->entity_type){
            case 'news':
                $metaTitle = $item->{'title' . getLocaleDBSuf()}.' - '.trans('custom.metas.news.title');
                $metaDescription = $item->{'title' . getLocaleDBSuf()}.'. '.trans('custom.metas.news.description');
                break;
            case 'actions':
                $metaTitle = $item->{'title' . getLocaleDBSuf()}.' - '.trans('custom.metas.actions.title');
                $metaDescription = $item->{'title' . getLocaleDBSuf()}.'. '.trans('custom.metas.actions.description');
                break;
            default:
                $metaTitle = $item->{'title' . getLocaleDBSuf()}.' - '.trans('custom.metas.news.title');
                $metaDescription = $item->{'title' . getLocaleDBSuf()}.'. '.trans('custom.metas.news.description');
                break;
        }

        return [
            'id' => $item->id,
            'title' => $item->{'title' . getLocaleDBSuf()},
            'slug' => $item->id,
            'url' => $url,
            'anons' => addNofollowToLink($item->{'anons' . getLocaleDBSuf()}),
            'picture' => $item->picture,
            'detail' => addNofollowToLink($item->{'detail' . getLocaleDBSuf()}),
            'meta_title' => $metaTitle,
            'meta_description' => $metaDescription,
            'meta_keywords' => $item->{'meta_keywords' . getLocaleDBSuf()},
            'recomends' => $recomends,
            'thumbnail' => $item->thumbnail,
            'thumbnail_first' => $item->thumbnail_first,
            'published' => date('d.m.Y',$dt),
            'picture_tablet' => $item->picture_tablet,
            'picture_phone' => $item->picture_phone,
            'can_connect' => ($item->can_connect == true) ? 1 : 0,
            'lastmod' => $item->updated_at,
        ];
    }

    /*
     * Форматирование данных для выдачи в API
     * @param obj $item
     * @return array
     */
    private function prepareDataForApi($item)
    {
        $url = env('APP_URL').getLocaleHrefPrefix().'/'.$item->entity_type.'/'.$item->slug;
        $published = (!empty($item->show_from)) ? $item->show_from : $item->created_at;
        return [
            'title' => $item->{'title' . getLocaleDBSuf()},
            'url' => $url,
            'thumbnail' => env('APP_URL').$item->thumbnail,
            'published' => strtotime($published),
        ];
    }

    private function prepareDataForPreview($item)
    {
        $url = getLocaleHrefPrefix().'/'.$item->entity_type.'/'.$item->slug;
        $published = (!empty($item->show_from)) ? $item->show_from : $item->created_at;
        $dt = strtotime($published);
        return [
            'id' => $item->id,
            'title' => $item->{'title' . getLocaleDBSuf()},
            'slug' => $item->id,
            'url' => $url,
            'anons' => addNofollowToLink($item->{'anons' . getLocaleDBSuf()}),
            'thumbnail' => $item->thumbnail,
            'thumbnail_first' => $item->thumbnail_first,
            'published' => date('d.m.Y',$dt),
        ];
    }

    private function checkForMoreItems($request)
    {
        $query = News::active()->published();
        if($this->getEntityType() == 'news'){
            $query->isNews();
        } elseif($this->getEntityType() == 'actions'){
            $query->isActions();
        }
        $all_count = $query->count();
        $cur_page = (!empty($request->get('page'))) ? $request->get('page') : 1;
        $curShow = ($cur_page * self::PAGINATION_CLIENT) - 2;
        if($all_count > $curShow){
            return true;
        }
        return false;
    }

    /*
     * Получение элементов новостей
     * @param str $entity
     * @return array
     */
    public function getNewsForApi($request = null)
    {
        $limit = (!empty($request->get('limit')) && intval($request->get('limit')) > 0) ? $request->get('limit') : $this::API_LIMIT_NEWS;

        $items = \Cache::remember(getLocale().'api-news-limit-'.$limit, 120, function () use ($limit) {
            $query = News::active()->published()->isNews();
            $res = $query->orderBy('show_from','desc')->limit($limit)->get();
            $items = [];
            if(!empty($res)){
                foreach ($res as $r){
                    $items[] = $this->prepareDataForApi($r);
                }
            }
            return $items;
        });

        return $items;
    }

    /*
     * Получение элементов акций
     * @param str $entity
     * @return array
     */
    public function getSharesForApi($request = null)
    {
        $limit = (!empty($request->get('limit')) && intval($request->get('limit')) > 0) ? $request->get('limit') : $this::API_LIMIT_SHARES;

        $items = \Cache::remember(getLocale().'api-shares-limit-'.$limit, 120, function () use ($limit) {
            $query = News::active()->published()->isActions();
            $res = $query->orderBy('show_from','desc')->limit($limit)->get();
            $items = [];
            if(!empty($res)){
                foreach ($res as $r){
                    $items[] = $this->prepareDataForApi($r);
                }
            }
            return $items;
        });

        return $items;
    }


    public function getItemsForFront($request)
    {
        $query = News::active()->published();
        if($this->getEntityType() == 'news'){
            $query->isNews();
        } elseif($this->getEntityType() == 'actions'){
            $query->isActions();
        }
        $page = ($request->get('page') != '') ? $request->get('page') : 1;
        if($page > 1){
            $offset = (($page - 1) * self::PAGINATION_CLIENT) - 2;
            $limit = self::PAGINATION_CLIENT;
        } else {
            $offset = 0;
            $limit = self::FISRT_LIMIT;
        }
        $res = $query->orderBy('show_from','desc')->offset($offset)->limit($limit)->get();

        $items = [];
        if(!empty($res)){
            foreach ($res as $r){
                $items[] = $this->prepareDataForFront($r);
            }
        }
        $result = [
            'items' => (!empty($items)) ? $items : false,
            'has_more' => $this->checkForMoreItems($request)
        ];
        return $result;
    }

    public function getItemsLastmod()
    {
        $query = News::active()->published();
        if($this->getEntityType() == 'news'){
            $query->isNews();
        } elseif($this->getEntityType() == 'actions'){
            $query->isActions();
        }
        $res = $query->orderBy('updated_at','desc')->first();
        $lastmod = (!empty($res->updated_at)) ? $res->updated_at : false;
        return $lastmod;
    }

    public function getItemsForFrontBySlug($slug)
    {
        if(isAdmin()){
            $item = News::with('recomends')->where('slug',$slug)->first();
        } else {
            $item = News::with('recomends')->where('slug',$slug)->active()->published()->first();
        }
        return (!empty($item)) ? $this->prepareDataForFront($item) : false;
    }


    public function getLast3ForMainPage()
    {
        $items = \Cache::remember(getLocale().'news-main-page', 86400, function () {
            $items = [];
            $res = News::active()->published()->orderBy('show_from', 'desc')->limit(3)->get();
            if (!empty($res)) {
                foreach ($res as $r) {
                    $items[] = $this->prepareDataForPreview($r);
                }
            }
            return $items;
        });
        return $items;
    }

}
