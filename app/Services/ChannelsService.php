<?php

namespace App\Services;

use App\Channel;
use App\Genre;
use App\Tariff;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use DB;
use Intervention\Image\Image;
use League\CommonMark\Inline\Element\Emphasis;

class ChannelsService
{
    const PAGINATION_ADMIN = 30;

    private $filters,$sort = ['field' => 'sort', 'direction' => 'asc'];
    private $tariff_id;
    private $parsed_ids = [];
    private $parsed_ids_analog = [];
    private $parsed_ids_digital = [];

    private $analog = false;

    private $base_url = '';

    /*urls for stalker*/
    private $urls = [

        [
            'url' => 'https://bil-api.briz.ua/sat/ctv',
            'billing_ids' => [43,48],
        ],
        [
            'url' => 'https://bil-api.briz.ua/sat/dtv/13',
            'billing_ids' => [43,48],
        ],
        [
            'url' => 'https://bil-api.briz.ua/sat/dtv/21',
            'billing_ids' => [48],
        ],

        [
            'url' => 'https://bil-api.briz.ua/sat/iptv/54',
            'billing_ids' => [54],
        ],

        [
            'url' => 'https://bil-api.briz.ua/sat/iptv/55',
            'billing_ids' => [55],
        ],

    ];

    private $errors = [];

    public function __construct()
    {

    }

    public function setBaseUrl($url)
    {
        $this->base_url = $url;
    }

    private function getBaseUrl()
    {
        return $this->base_url;
    }

    private function isAnalog($url)
    {
        $arr_analog = ['https://bil-api.briz.ua/sat/ctv'];
        if(in_array($url,$arr_analog)){
            $this->analog = true;
        } else {
            $this->analog = false;
        }
    }

    private function checkIsAnalog()
    {
        if($this->analog == true){
            return true;
        }
        return false;
    }

    private function addToParsed($id)
    {
        if(!in_array($id,$this->parsed_ids)){
            $this->parsed_ids[] = $id;
        }
    }

    private function getParsedIds()
    {
        return $this->parsed_ids;
    }

    private function clearNotParsedChannels()
    {
        $ids = $this->getParsedIds();
        $res = Channel::whereNotIn('billing_id',$ids)->get();
        if(!empty($res)){
            foreach($res as $r){
                DB::table('channels_tariffs')
                    ->where('channel_id','=',$r->id)
                    ->delete();
                if($r->logo != ''){
                    if(file_exists(public_path($r->logo))){
                        unlink(public_path($r->logo));
                    }
                }
                $r->delete();
            }
        }
    }

    private function getUrls()
    {
        return $this->urls;
    }

    private function startParsing()
    {
        Channel::update(['parsed' => 0]);
    }

    private function endParsing()
    {
        $res = Channel::where('parsed',0)->get();
        if(!empty($res)){
            foreach($res as $r){
                DB::table('channels_tariffs')->where('channel_id','=',$r->id)->delete();
                DB::table('channels_genres')->where('channel_id','=',$r->id)->delete();
            }
            Channel::where('parsed',0)->delete();
        }
    }

    private function getChannels($url)
    {
        $tariff = Tariff::where('billing_id',$b_id)->first();
        $this->tariff_id = $tariff->id;
        if(!empty($url)){
            foreach($url as $path){
                $this->requestChannels($path);
                //$items = $this->requestChannels($path);
                //$this->syncChannelsWithtariff($b_id, $items);
            }
        }
    }

    private function syncChannelWithTariff($channel, $billing_ids)
    {
        if(!empty($billing_ids) && !empty($channel)){
            foreach($billing_ids as $billing_id){
                $tariff = Tariff::where('billing_id',$billing_id)->first();
                if($tariff){
                    $exist = DB::table('channels_tariffs')
                        ->select()
                        ->where('channel_id','=',$channel->id)
                        ->where('tariff_id','=',$tariff->id)
                        ->first();
                    if(empty($exist)){
                        DB::table('channels_tariffs')->insert([
                            'channel_id' => $channel->id,
                            'tariff_id' => $tariff->id,
                        ]);
                        echo ' sync '.$channel->id.' --> '.$tariff->id.' | ';
                    }
                }
            }
        }
        return true;
    }

    private function requestChannels($path)
    {
        $this->isAnalog($path['url']);
        $response = Http::get($path['url']);
        $arr = $response->json();
        $count = 0;
        if(!empty($arr['data'])){
            foreach($arr['data'] as $item){
                $channel = $this->createOrUpdateChannel($item,$path['billing_ids']);
                if($channel !== false){
                    $count++;
                    $this->syncChannelWithTariff($channel,$path['billing_ids']);
                    $genre = $this->createGenre($channel);
                    $this->syncChannelWithGenre($channel, $genre);
                }
            }
        }
        return $count;
    }

    private function syncChannelWithGenre($channel, $genre)
    {
        if(!empty($channel->id) && !empty($genre->id)){

            $exist = DB::table('channels_genres')
                ->select()
                ->where('channel_id','=',$channel->id)
                ->where('genre_id','=',$genre->id)
                ->first();
            DB::table('channels_genres')
                ->where('channel_id','=',$channel->id)
                ->where('genre_id','!=',$genre->id)
                ->delete();
            if(isset($exist->id) && $exist->id > 0){
                //exist
            } else {
                DB::table('channels_genres')->insert([
                    'channel_id' => $channel->id,
                    'genre_id' => $genre->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }
        return true;
    }

    private function createGenre($channel)
    {
        if(!empty($channel->string)){
            $genre = Genre::where('code',$channel->string)->first();
            if(!$genre){
                $genre = Genre::create([
                    'code' => $channel->string,
                ]);
            }
            return $genre;
        }
        return false;
    }

    private function syncChannelsWithtariff($tariff_id, $ids)
    {
        if(!empty($ids)){
            $tariff = Tariff::where('billing_id',$tariff_id)->first();
            foreach($ids as $channel_id){
                $exist = DB::table('channels_tariffs')->select()->where('channel_id','=',$channel_id)->where('tariff_id','=',$tariff->id)->first();
                if(!empty($exist->id)){
                    //ok - exist
                } else {
                    DB::table('channels_tariffs')->insert([
                        'channel_id' => $channel_id,
                        'tariff_id' => $tariff->id,
                    ]);
                }
            }
        }
    }

    private function createOrUpdateChannel($item,$billing_ids = [])
    {
        $query = Channel::where('billing_id',$item['id']);
        if($this->checkIsAnalog() == true){
            $query->where('is_analog',1);
        } else {
            $query->where('is_analog','!=',1);
        }
        $row = $query->first();
        if(isset($row->id)){
            $data = [];
            if($item['Name'] != $row->title){$data['title'] = $item['Name'];}
            $data['billing_logo'] = $item['Logo'];
            $data['parsed'] = 1;
            if($item['OrderNum'] != $row->sort){$data['sort'] = $item['OrderNum'];}
            if($item['Genre'] != $row->string){$data['string'] = $item['Genre'];}
            $data['is_analog'] = ($this->checkIsAnalog() == true) ? 1 : 0;
            if(!empty($data)){
                $row->update($data);
                echo 'UPD:'.$item['Name'].' ';
            }
        } else {
            $data = [
                'title' => $item['Name'],
                'logo' => '',
                'string' => $item['Genre'],
                'sort' => $item['OrderNum'],
                'billing_id' => $item['id'],
                'billing_logo' => $item['Logo'],
                'parsed' => 1,
                'is_analog' => ($this->checkIsAnalog() == true) ? 1 : 0,
            ];
            $row = Channel::create($data);
            echo 'NEW:'.$item['Name'].' ';
        }
        $this->getLogo($row, $item['Logo']);
        return $row;
    }

    private function getLogo($channel, $path)
    {
        if (! \File::exists(public_path("uploads/channels"))) {
            \File::makeDirectory(public_path("uploads/channels"));
        }
        try {
            if(!empty($channel->logo) && file_exists(public_path($channel->logo))){
                unlink(public_path($channel->logo));
            }
        } catch (\Exception $exception){
            $this->errors[] = $channel->title.' ['.$channel->id.'] --> '.$path;
        }
        try {
            $img = \Image::make($path);
            $mime = $img->mime();
            $arr = explode('/',$mime);
            $new_path = 'uploads/channels/channel-'.$channel->id.'-'.date('YmdHis').'.'.$arr[1];
            $img->save(public_path($new_path), 100);
            $channel->update([
                'logo' => '/'.$new_path,
            ]);
        } catch (\Exception $exception){
            $this->errors[] = $channel->title.' ['.$channel->id.'] --> '.$path;
        }
        return true;
    }

    public function importChannels()
    {
        $urls = $this->getUrls();
        $this->startParsing();
        if(!empty($urls)){
            foreach($urls as $url){
                $this->requestChannels($url);
            }
        }
        $this->endParsing();
        \Cache::flush();
        echo 'Done parsing channels!';
        return true;
    }

    public function getGenresForAdmin()
    {
        $items = Genre::orderBy('sort','asc')->get();
        return $items;
    }

    private function setFilter($input)
    {
        $upd = false;
        if(isset($input['id']))
        {
            $this->filters['id'] = $input['id'];
            $upd = true;
        }
        if(isset($input['title']))
        {
            $this->filters['title'] = $input['title'];
            $upd = true;
        }
        if(isset($input['tariff']))
        {
            $this->filters['tariff'] = $input['tariff'];
            $upd = true;
        }
        if(isset($input['genre']))
        {
            $this->filters['genre'] = $input['genre'];
            $upd = true;
        }
        if($upd == true){
            session(['filtersChannels' => $this->filters]);
        } else {
            $sess_filter = session('filtersChannels');
            if(!empty($sess_filter)){
                $this->filters = $sess_filter;
            }
        }
    }

    private function getFilters()
    {
        return $this->filters;
    }

    private function setSorting($input)
    {
        if(!empty($input['sort']) && !empty($input['direction'])){
            $this->sort = ['field' => $input['sort'], 'direction' => $input['direction']];
        }
    }

    private function getSorting()
    {
        return $this->sort;
    }

    private function buildQuery()
    {
        $query = Channel::with('genres','tariffs')->orderBy($this->sort['field'],$this->sort['direction']);

        if(!empty($this->filters['id'])){
            $query->where('id',$this->filters['id']);
        }

        if(!empty($this->filters['title'])){
            $query->where('title','LIKE', '%'.$this->filters['title'].'%');
        }

        if(!empty($this->filters['tariff'])){
            $ids = [];
            $res = DB::table('channels_tariffs')->select()->where('tariff_id','=',$this->filters['tariff'])->get();
            if(!empty($res)){
                foreach($res as $r){
                    $ids[] = $r->channel_id;
                }
                $query->whereIn('id',$ids);
            }
        }

        if(!empty($this->filters['genre'])){
            $ids = [];
            $res = DB::table('channels_genres')->select()->where('genre_id','=',$this->filters['genre'])->get();
            if(!empty($res)){
                foreach($res as $r){
                    $ids[] = $r->channel_id;
                }
                $query->whereIn('id',$ids);
            }
        }

        return $query;
    }

    public function getChannelsForAdmin($input)
    {
        $this->setFilter($input);
        $this->setSorting($input);
        $items = $this->buildQuery()->paginate(self::PAGINATION_ADMIN);

        return ['items' => $items, 'filter' => $this->getFilters(), 'sort' => $this->getSorting()];
    }

    public function getTariffs()
    {
        $tariffs = [];
        $urls = $this->getUrls();
        foreach($urls as $key => $vals){
            if(!empty($vals['billing_ids'])){
                foreach($vals['billing_ids'] as $bil_id){
                    $tariffs[$bil_id] = Tariff::where('billing_id',$bil_id)->first();
                }
            }
        }
        return $tariffs;
    }

    private function getChannelsByTariff($tariff_id)
    {
        $ids = [];
        $res = DB::table('channels_tariffs')
            ->select()
            ->where('tariff_id','=',$tariff_id)
            ->get();
        if(!empty($res)){
            foreach($res as $r){
                if(!in_array($r->channel_id,$ids)){
                    $ids[] = $r->channel_id;
                }
            }
            $channels = Channel::with('genres')->whereIn('id',$ids)->orderBy('is_analog','desc')->get();
            return $this->prepareChannelsForFront($channels);
        }
        return false;
    }



    private function prepareChannelsForFront($channels)
    {
        $ids = [];
        $result = [];
        /*
        if(!empty($channels)){
            foreach($channels as $channel){
                    if(count($channel->genres) > 0){
                        foreach($channel->genres as $genre){
                            if(!in_array($channel->id,$ids)) {
                                $ids[] = $channel->id;
                                if (!isset($result[$genre->id])) {
                                    $result[$genre->id]['genre_title'] = $genre->{'title' . getLocaleDBSuf()};
                                    $result[$genre->id]['sort'] = $genre->sort;
                                }
                                $result[$genre->id]['channels'][] = [
                                    'title' => $channel->title,
                                    'icon' => $channel->logo,
                                    'is_analog' => ($channel->is_analog == 1) ? true : false,
                                ];
                            }
                        }
                    } else {
                        if(!in_array($channel->id,$ids)) {
                            $ids[] = $channel->id;
                            $result[0]['channels'][] = [
                                'title' => $channel->title,
                                'icon' => $channel->logo,
                                'is_analog' => ($channel->is_analog == 1) ? true : false,
                            ];
                        }
                    }
            }
        }
        if(!empty($result[0]['channels'])){
            $result[0]['sort'] = 9999;
        }
        */
        if(!empty($channels)){
            foreach($channels as $channel){

                    if(count($channel->genres) > 0){
                        foreach($channel->genres as $genre){
                            if (!isset($result[$genre->id])) {
                                $result[$genre->id]['genre_title'] = $genre->{'title' . getLocaleDBSuf()};
                                $result[$genre->id]['sort'] = $genre->sort;
                                $result[$genre->id]['url'] = getLocaleHrefPrefix().$this->getBaseUrl().'/'.$genre->code;
                            }
                            $result[$genre->id]['channels'][] = [
                                'title' => $channel->title,
                                'icon' => $channel->logo,
                                'is_analog' => ($channel->is_analog == 1) ? true : false,
                            ];
                        }
                    }

                    $result[0]['channels'][] = [
                        'title' => $channel->title,
                        'icon' => $channel->logo,
                        'is_analog' => ($channel->is_analog == 1) ? true : false,
                    ];
                    if($channel->is_analog == 1){
                        $result['analog']['channels'][] = [
                            'title' => $channel->title,
                            'icon' => $channel->logo,
                            'is_analog' => true,
                        ];
                    } else {
                        $result['digital']['channels'][] = [
                            'title' => $channel->title,
                            'icon' => $channel->logo,
                            'is_analog' => false,
                        ];
                    }

            }
        }

        if(!empty($result['analog']['channels'])){
            $result['analog']['genre_title'] = trans('custom.channels.analog');
            $result['analog']['sort'] = 999998;
            $result['digital']['genre_title'] = trans('custom.channels.digital');
            $result['digital']['sort'] = 999999;
            $result['analog']['url'] = getLocaleHrefPrefix().$this->getBaseUrl().'/analog';
            $result['digital']['url'] = getLocaleHrefPrefix().$this->getBaseUrl().'/digital';
        } else {
            unset($result['analog']);
            unset($result['digital']);
        }

        $result[0]['genre_title'] = trans('custom.channels.all');
        $result[0]['sort'] = 0;
        $result[0]['url'] = getLocaleHrefPrefix().$this->getBaseUrl();
        uasort($result, 'cmpChannels');

        $new_result = [];
        if(!empty($result)){
            foreach($result as $item){
                $item['count'] = (!empty($item['channels'])) ? count($item['channels']) : 0;
                $new_result[] = $item;
            }
        }
        return $new_result;
    }

    public function countChannelsInTariff($id)
    {
        $res = DB::table('channels_tariffs as ct')
            ->select('c.id')
            ->leftJoin('channels as c','c.id','=','ct.channel_id')
            ->where('ct.tariff_id','=',$id)
            ->where('c.id','>',0)
            ->groupBy('c.id')
            ->get();
        return count($res);
    }

    public function getForFront($id)
    {
        return $this->getChannelsByTariff($id);
    }

}
