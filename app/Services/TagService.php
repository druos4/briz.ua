<?php

namespace App\Services;

use App\Tag;
use Carbon\Carbon;

class TagService
{
    const PAGINATION_ADMIN = 30;
    const PAGINATION_CLIENT = 10;

    private $tags, $filters, $sort = ['field' => 'id', 'direction' => 'desc'], $id;

    public function __construct()
    {

    }

    private function setId($id)
    {
        $this->id = $id;
    }

    private function getId()
    {
        return $this->id;
    }

    private function buildQuery()
    {
        $query = Tag::with('updater');

        if(!empty($this->filters['id'])){
            $query->where('id',$this->filters['id']);
        }

        if(!empty($this->filters['code'])){
            $query->where('code','LIKE', '%'.$this->filters['code'].'%');
        }

        if(!empty($this->filters['title'])){
            $query->where(function ($query2) {
                $query2->where('title_ru', 'LIKE', '%'.$this->filters['title'].'%')
                    ->orWhere('title_ua', 'LIKE', '%'.$this->filters['title'].'%')
                    ->orWhere('title_en', 'LIKE', '%'.$this->filters['title'].'%');
            });
        }

        $query->orderBy($this->sort['field'],$this->sort['direction']);
        return $query;
    }

    public function getItemsForAdmin($input)
    {
        $this->setFilter($input);
        $this->setSorting($input);

        $items = $this->buildQuery()->paginate(self::PAGINATION_ADMIN);

        return ['items' => $items, 'filter' => $this->getFilters(), 'sort' => $this->getSorting()];
    }

    public function getFilterForAdmin()
    {
        return $this->getFilters();
    }

    public function getSortingForAdmin()
    {
        return $this->getSorting();
    }

    private function setFilter($input)
    {
        $upd = false;
        if(isset($input['id']))
        {
            $this->filters['id'] = $input['id'];
            $upd = true;
        }
        if(isset($input['code']))
        {
            $this->filters['code'] = $input['code'];
            $upd = true;
        }
        if(!empty($input['title']))
        {
            $this->filters['title'] = $input['title'];
            $upd = true;
        }
        if($upd == true){
            session(['filtersTags' => $this->filters]);
        } else {
            $sess_filter = session('filtersTags');
            if(!empty($sess_filter)){
                $this->filters = $sess_filter;
            }
        }
    }

    private function getFilters()
    {
        return $this->filters;
    }

    private function setSorting($input)
    {
        if(!empty($input['field']) && !empty($input['direction'])){
            $this->sort = ['field' => $input['field'], 'direction' => $input['direction']];
        }
    }

    private function getSorting()
    {
        return $this->sort;
    }

}
