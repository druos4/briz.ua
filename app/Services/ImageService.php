<?php

namespace App\Services;

use Intervention\Image\Image;

class ImageService
{
    protected $dirs = [
        'News' => 'uploads/news/',
        'Slider' => 'uploads/sliders/',
        'Service' => 'uploads/services/',
        'Product' => 'uploads/products/',
    ];
    protected $path, $name;


    public function __construct()
    {

    }

    private function getName()
    {
        return $this->name;
    }

    private function setName($entity_name, $ext = 'jpg', $thumb = false)
    {
        if($thumb == true){
            $name = strtolower($entity_name).'thumb-'.date('YmdHis').'-'.rand(0,999).'.'.$ext;
        } else {
            $name = strtolower($entity_name).'-'.date('YmdHis').'-'.rand(0,999).'.'.$ext;
        }
        $this->name = $name;
    }

    private function getPath()
    {
        return $this->path;
    }

    private function setPath($entity_name)
    {
        if(isset($this->dirs[$entity_name])){
            $this->path = $this->dirs[$entity_name];
        } else {
            $this->path = 'uploads/';
        }
    }

    public function storeImage($file, $entity_name)
    {
        $this->setPath($entity_name);
        if(!empty($file)){
            $data = explode(',', $file)[1];
            $this->setName($entity_name);
            $fileStream = fopen(public_path($this->getPath().$this->getName()) , "wb");
            fwrite($fileStream, base64_decode($data));
            fclose($fileStream);
            return '/'.$this->getPath().$this->getName();
        } else {
            return false;
        }
    }

    public function storeOriginalImage($input_file, $entity_name)
    {
        $this->setPath($entity_name);
        if(!empty($input_file)){
            $this->setName($entity_name);
            $file = $input_file;
            $file->move(public_path($this->getPath(),$this->getName()));
            return '/'.$this->getPath().$this->getName();
        } else {
            return false;
        }
    }

    public function deleteImage($path)
    {
        if(file_exists(public_path($path))){
            unlink(public_path($path));
        }
        return true;
    }

    public function moveImage($request, $field_name, $entity = null)
    {
        if ($request->hasFile($field_name))
        {
            $f_name = date('YmdHis').$request->file($field_name)->getClientOriginalName();
            $this->setPath($entity);
            $file = $request->file($field_name);
            $file->move(public_path($this->getPath()),$f_name);
            return '/'.$this->getPath().$f_name;
        }
        return false;
    }


    public function copyImage($file, $entity_name)
    {
        $img = \Image::make($file);
        $this->setPath($entity_name);
        $this->setName($entity_name,$file->getClientOriginalExtension());
        $img->save(public_path($this->getPath().$this->getName()), 100);
        return '/'.$this->getPath().$this->getName();
    }

    public function storeThumbnail($file, $entity_name, $original)
    {
        $this->setPath($entity_name);
        if(!empty($file)){
            $ext = $original->getClientOriginalExtension();
            $this->setName($entity_name,$ext,true);
            $img = \Image::make(public_path($file));
            $img->resize(500, 500, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $img->save(public_path($this->getPath().$this->getName()), 100);
            return '/'.$this->getPath().$this->getName();
        }
        return false;
    }

}
