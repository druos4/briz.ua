<?php

namespace App\Services;

use App\Category;
use App\CategoryProperty;
use App\News;
use App\NewsTag;
use App\Product;
use App\ProductBuyWith;
use App\Property;
use App\PropertyValues;
use App\Slider;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ShopService
{
    private $filters, $request, $sort = ['field' => 'price', 'direction' => 'asc'], $id, $current_page;
    private $selected_category = false;
    private $active_category = false;
    const PRODUCTS_PER_PAGE = 6;
    const PAGINATE = false;

    private $arr_sorts = ['expensive','cheap'];

    private $arr_filters = [
        'all' => [
            'skorost-lan-portov' => '1-gbits',
            'castota-raboty-wi-fi' => '5-ggc',
            'new' => 1,
            'discount' => 1,
            'promo' => 1,
        ],
        'top' => [
            'new' => 1,
            'discount' => 1,
            'promo' => 1,
        ],
        'routery' => [
            'skorost-lan-portov' => '1-gbits',
            'castota-raboty-wi-fi' => '5-ggc',
            'new' => 1,
            'discount' => 1,
            'promo' => 1,
        ],
        'kommutatori' => [
            'skorost-lan-portov' => '1-gbits',
            'new' => 1,
            'discount' => 1,
            'promo' => 1,
        ],
        'wi-fi-tocki-dostupa' => [
            'skorost-lan-portov' => '1-gbits',
            'castota-raboty-wi-fi' => '5-ggc',
            'new' => 1,
            'discount' => 1,
            'promo' => 1,
        ],
        'iptv-pristavki' => [
            'new' => 1,
            'discount' => 1,
            'promo' => 1,
        ],
        'cable' => [
            'new' => 1,
            'discount' => 1,
            'promo' => 1,
        ],
        'drugoe' => [
            'new' => 1,
            'discount' => 1,
            'promo' => 1,
        ],
    ];

    public function __construct($request = [],$category_slug = 'all')
    {
        $this->active_category = $category_slug;
        if(!empty($request)){
            $this->request = $request;
        }
    }

    public function categoryExist()
    {
        $category = Category::where('slug',$this->active_category)->first();
        if(isset($category->id) && $category->id > 0){
            return true;
        }
        return false;
    }

    public function getAllData()
    {
        $categories = $this->getCategoriesList();
        return $categories;
    }

    public function getCategoryMeta()
    {
        $meta = [
            'title' => '',
            'meta_title' => '',
            'meta_description' => '',
            'meta_keywords' => '',
        ];
        $category = Category::select('id','title_ru','title_ua','title_en',
            'meta_title_ru','meta_title_ua','meta_title_en',
            'meta_description_ru','meta_description_ua','meta_description_en',
            'meta_keywords_ru','meta_keywords_ua','meta_keywords_en')
            ->where('slug',$this->active_category)
            ->first();
        if($category){
            $meta = [
                'title' => $category->{'title' . getLocaleDBSuf()},
                'meta_title' => $category->{'meta_title' . getLocaleDBSuf()},
                'meta_description' => $category->{'meta_description' . getLocaleDBSuf()},
                'meta_keywords' => $category->{'meta_keywords' . getLocaleDBSuf()},
            ];
        }
        return $meta;
    }

    public function getItemsLastmod()
    {
        $res = Product::orderBy('updated_at','desc')->first();
        $lastmod = (!empty($res->updated_at)) ? $res->updated_at : false;
        $res = Product::orderBy('deleted_at','desc')->first();
        if($res->deleted_at > $lastmod){
            $lastmod = $res->deleted_at;
        }
        return $lastmod;
    }

    public function getProducts()
    {
        $result = [
            'products' => [],
            'has_more' => false,
            'page' => 1,
            'next_page' => 2,
        ];
        $all_count = 0;
        $page = 1;
        $offset = ($page - 1) * self::PRODUCTS_PER_PAGE;
        $limit = self::PRODUCTS_PER_PAGE;

        $query = Product::select('id','title_ru','title_ua','title_en','slug','picture','thumbnail','price','price_old','quantity','rating',
        'anons_ru','anons_ua','anons_en','new','hit','promo','section_description_ru','section_description_ua','section_description_en')
            ->active()
            ->whereNull('deleted_at');
        $query_count = Product::active()
            ->whereNull('deleted_at');

        if(!in_array($this->active_category,['all','top'])){
            $category = Category::select('id')->where('slug',$this->active_category)->first();
            $query->where('category_id',$category->id);
            $query_count->where('category_id',$category->id);
        }

        if($this->active_category == 'top'){
            $query->hit();
            $query_count->hit();
        }

        $arr_request = $this->request;

        $sort = [
            'field' => 'price',
            'order' => 'asc',
        ];
        if(!empty($arr_request['sort'])){
            if($arr_request['sort'] == 'expensive'){
                $sort = [
                    'field' => 'price',
                    'order' => 'desc',
                ];
            }
        }

        if(!empty($arr_request)){
            foreach($arr_request as $req_key => $req_val){
                if($req_key == 'new') {
                    $query->new();
                    $query_count->new();
                } elseif($req_key == 'promo'){
                    $query->promo();
                    $query_count->promo();
                } elseif($req_key == 'discount'){
                    $query->where('price_old','>','price');
                    $query_count->where('price_old','>','price');
                } elseif(in_array($req_key,['skorost-lan-portov','castota-raboty-wi-fi'])) {
                    $property = Property::select('id')->where('slug',$req_key)->first();
                    $value = PropertyValues::select('id')->where('property_id',$property->id)->where('slug',$req_val)->first();
                    $ids = [];
                    if(!empty($property->id) && !empty($value->id)){
                        $res = DB::table('product_properties')
                            ->select()
                            ->where('property_id','=',$property->id)
                            ->where('value_id','=',$value->id)
                            ->get();
                        if(!empty($res)){
                            foreach($res as $r){
                                $ids[] = $r->product_id;
                            }
                            $query->whereIn('id',$ids);
                            $query_count->whereIn('id',$ids);
                        }
                    }
                }
            }
        }
        $query->orderBy($sort['field'],$sort['order']);
        if(self::PAGINATE == true){
            $res = $query->offset($offset)->limit($limit)->get();
        } else {
            $res = $query->get();
        }

        if(!empty($res)){
            foreach($res as $prod){
                $result['products'][] = $this->prepareProductSectionForFront($prod);
            }
        }
        $all_count = $query_count->count();
        $passed = $page * self::PRODUCTS_PER_PAGE;
        if($passed < $all_count){
            $result['has_more'] = true;
            $result['next_page'] = $page + 1;
        }

        return $result;
    }

    public function getButtons()
    {
        $buttons = [
            'filter' => [],
            'sort' => [],
        ];
        $arr_filters = $this->arr_filters;
        $category_slug = $this->active_category;
        if(!empty($arr_filters[$category_slug])){

            foreach($arr_filters[$this->active_category] as $key => $val){
                $count = 0;

                $query = DB::table('products as p')
                    ->whereNull('p.deleted_at')
                    ->where('p.active','=',1)
                    ->where('p.quantity','>',0);

                if($key == 'promo'){
                    $query->where('p.promo','=',1);

                } elseif($key == 'discount'){
                    $query->where('p.price_old','>','p.price');

                } elseif($key == 'new'){
                    $query->where('p.new','=',1);

                } else {
                    $property = Property::where('slug',$key)->first();
                    $value = PropertyValues::where('slug',$val)->first();
                    $query->leftJoin('product_properties as pp','pp.product_id','=','p.id')
                        ->where('pp.property_id','=',$property->id)
                        ->where('pp.value_id','=',$value->id);

                }

                if(!in_array($this->active_category,['all','top'])){
                    $category = Category::where('slug',$this->active_category)->first();
                    $query->where('p.category_id','=',$category->id);
                } elseif($this->active_category == 'top') {
                    $query->where('p.hit','=',1);
                }

                $count = $query->count();
                if($count == 0){
                    continue;
                }

                $params = [];
                $active = false;
                $url = '/equipment';
                if($this->active_category != 'all'){
                    $url .= '/'.$this->active_category;
                }

                $arr_reqs = $this->request;
                if(!empty($arr_reqs[$key])){
                    //current filter is set
                    $active = true;
                    if(!empty($arr_reqs)) {
                        foreach ($arr_reqs as $req_key => $req_val) {
                            if ($key != $req_key) {
                                $params[] = $req_key . '=' . $req_val;
                            }
                        }
                    }
                } else {
                    $params[] = $key.'='.$val;
                    if(!empty($arr_reqs)) {
                        foreach ($arr_reqs as $req_key => $req_val) {
                            $params[] = $req_key . '=' . $req_val;
                        }
                    }
                }

                if(!empty($params)){
                    $url .= '?'.implode('&',$params);
                }
                $buttons['filter'][] = [
                    'title' => trans('custom.equipment.'.$key),
                    'url' => getLocaleHrefPrefix().$url,
                    'active' => $active,
                ];
            }
        }

        $url = '/equipment';
        $params = [];
        $arr_reqs = $this->request;
        if($this->active_category != 'all'){
            $url .= '/'.$this->active_category;
        }
        if(!empty($arr_reqs['sort'])){
            //sort exist
            if($arr_reqs['sort'] == 'cheap'){
                if(!empty($arr_reqs)){
                    foreach($arr_reqs as $req_key => $req_val){
                        if($req_key != 'sort'){
                            $params[] = $req_key.'='.$req_val;
                        }
                    }
                }
                $params[] = 'sort=expensive';
                $url .= '?'.implode('&',$params);
                $buttons['sort'] = [
                    'title' => trans('custom.category.sort.cheap'),
                    'url' => getLocaleHrefPrefix().$url,
                ];
            } else {
                if(!empty($arr_reqs)){
                    foreach($arr_reqs as $req_key => $req_val){
                        if($req_key != 'sort'){
                            $params[] = $req_key.'='.$req_val;
                        }
                    }
                }
                $params[] = 'sort=cheap';
                $url .= '?'.implode('&',$params);
                $buttons['sort'] = [
                    'title' => trans('custom.category.sort.expensive'),
                    'url' => getLocaleHrefPrefix().$url,
                ];
            }
        } else {
            //default sort
            if(!empty($arr_reqs)){
                foreach($arr_reqs as $req_key => $req_val){
                    $params[] = $req_key.'='.$req_val;
                }
            }

            $params[] = 'sort=expensive';
            $sortTitle = trans('custom.category.sort.cheap');

            $url .= '?'.implode('&',$params);
            $buttons['sort'] = [
                'title' => $sortTitle,
                'url' => getLocaleHrefPrefix().$url,
            ];
        }

        return $buttons;
    }

    public function getFilter($request)
    {
        $filters = [];
        $arr_filters = $this->arr_filters;
        $category_slug = $this->active_category;
        if(!empty($arr_filters[$category_slug])){
            foreach($arr_filters['all'] as $key => $val){
                $params = [];
                $active = false;
                if(!empty($request[$key])){
                    //current filter is set
                    $active = true;

                } else {
                    $url = '/equipment';
                    if($this->active_category != 'all'){
                        $url .= '/'.$this->active_category;
                    }
                    $params[] = $key.'='.$val;
                    $url .= '?'.implode('&',$params);

                }
                $filters[] = [
                    'title' => trans('custom.equipment.'.$key),
                    'url' => $url,
                    'active' => $active,
                ];
            }
        }


        /*
        echo '<pre>';
        print_r($filters);
        echo '</pre>';
        die;
        */
        return $filters;
    }

    public function getSort($input)
    {
        $items = [];
        $arr = $this->arr_sorts;
        print_r($arr);
        print_r($input);

        if(!empty($arr)){
            foreach($arr as $a){
                $active = false;
                $items[] = [
                    'title' => trans('custom.category.sort.'.$a),
                    'code' => $a,
                    'active' => $active,
                    'action' => ($a == 'expensive') ? 'cheap' : 'expensive',
                ];
            }
        }
        echo '<pre>';
        print_r($items);
        echo '</pre>';
        die;
        return $items;


    }

    private function getCategoriesList()
    {
        $list = [];
        $list[] = [
            'id' => 0,
            'title' => trans('custom.equipment.allproducts'),
            'slug' => 'all',
            'url' => getLocaleHrefPrefix().'/equipment',
            'picture' => '/images/allCategory.svg',
            'selected' => ($this->active_category == 'all') ? true : false,
        ];

        $list[] = [
            'id' => '-1',
            'title' => trans('custom.equipment.top-products'),
            'slug' => 'top',
            'url' => getLocaleHrefPrefix().'/equipment/top',
            'picture' => '/images/topCategory.svg',
            'selected' => ($this->active_category == 'top') ? true : false,
        ];

        $res = Category::visible()->orderBy('sort','asc')->get();
        if(!empty($res)){
            foreach($res as $r){
                $list[] = [
                    'id' => $r->id,
                    'title' => $r->{'title' . getLocaleDBSuf()},
                    'slug' => $r->slug,
                    'url' => getLocaleHrefPrefix().'/equipment/'.$r->slug,
                    'picture' => (!empty($r->picture)) ? $r->picture : '',
                    'selected' => ($this->active_category == $r->slug) ? true : false,
                ];
            }
        }
        return $list;
    }


    private function getFiltersFields($is_selected = false)
    {

        $filters = [];
        $arr_filters = $this->arr_filters;
        $cat = (!empty($this->getSelectedCategory())) ? $this->getSelectedCategory() : 'all';
        if(!empty($arr_filters[$cat])){


            foreach($arr_filters['all'] as $key => $val){
                $active = false;
                $filters[] = [
                    'title' => trans('custom.equipment.'.$key),
                    'slug' => $key,
                    'code' => $val,
                    'active' => $active,
                ];
            }
        }
        return $filters;
    }

    private function getSortFields()
    {
        $items = [];
        $arr = $this->arr_sorts;
        if(!empty($arr)){
            foreach($arr as $a){
                $active = false;
                if($a == 'expensive' && $this->sort['field'] == 'price' && $this->sort['direction'] == 'desc'){
                    $active = true;
                } elseif($a == 'cheap' && $this->sort['field'] == 'price' && $this->sort['direction'] == 'asc'){
                    $active = true;
                }
                $items[] = [
                    'title' => trans('custom.category.sort.'.$a),
                    'code' => $a,
                    'active' => $active,
                    ];
            }
        }
        return $items;
    }

    private function setId($id)
    {
        $this->id = $id;
    }

    private function getId()
    {
        return $this->id;
    }

    private function setCurrentPage($request)
    {
        $page = ($request->get('page') > 1) ? $request->get('page') : 1;
        $this->current_page = $page;
    }

    private function getCurrentPage()
    {
        return $this->current_page;
    }

    private function setCategorySort($request)
    {
        if($request->get('sort') == 'rating'){
            $this->sort = [
                'field' => 'rating',
                'direction' => 'desc',
            ];
        } elseif($request->get('sort') == 'cheap'){
            $this->sort = [
                'field' => 'price',
                'direction' => 'asc',
            ];
        } elseif($request->get('sort') == 'expensive'){
            $this->sort = [
                'field' => 'price',
                'direction' => 'desc',
            ];
        } else {
            $this->sort = [
                'field' => 'price',
                'direction' => 'desc',
            ];
        }
    }

    private function setCategoryFilter($request)
    {
        $this->request = $request->all();
        $filters = [];
        if($request->get('castota-raboty-wi-fi') != ''){

            $property = Property::where('slug','castota-raboty-wi-fi')->first();
            if($property){
                $value = PropertyValues::where('property_id',$property->id)->where('slug',$request->get('castota-raboty-wi-fi'))->first();
                if($value){
                    $filters[] = [
                        'property_id' => $property->id,
                        'value_id' => $value->id,
                    ];
                }
            }
        }
        if($request->get('skorost-lan-portov') != ''){
            $property = Property::where('slug','skorost-lan-portov')->first();
            if($property){
                $value = PropertyValues::where('property_id',$property->id)->where('slug',$request->get('skorost-lan-portov'))->first();
                if($value){
                    $filters[] = [
                        'property_id' => $property->id,
                        'value_id' => $value->id,
                    ];
                }
            }
        }
        if($request->get('new') != ''){
            $filters[] = [
                'property_id' => 'new',
                'value_id' => 1,
            ];
        }
        if($request->get('discount') != ''){
            $filters[] = [
                'property_id' => 'discount',
                'value_id' => 1,
            ];
        }
        if(!empty($filters)){
            foreach($filters as $filter){
                $ids = [];
                if(!in_array($filter['property_id'],['new','discount'])){
                    $res = DB::table('product_properties')
                        ->select()
                        ->where('property_id','=',$filter['property_id'])
                        ->where('value_id','=',$filter['value_id'])
                        ->get();
                    if(!empty($res)){
                        foreach($res as $r){
                            $ids[] = $r->product_id;
                        }
                        $this->filters[] = $ids;
                    }
                } elseif($filter['property_id'] == 'new') {
                    $res = Product::select('id')->new()->active()->get();
                    if(!empty($res)){
                        foreach($res as $r){
                            $ids[] = $r->id;
                        }
                        $this->filters[] = $ids;
                    }
                } elseif($filter['property_id'] == 'discount') {
                    $res = Product::select('id')->where('price_old','>','price')->active()->get();
                    if(!empty($res)){
                        foreach($res as $r){
                            $ids[] = $r->id;
                        }
                        $this->filters[] = $ids;
                    }
                }
            }
        }
    }

    private function setSelectedCategory($category_slug)
    {
        if($category_slug != false){
            $category = Category::select('id','slug')->where('slug',$category_slug)->first();
            if($category){
                $this->selected_category = $category->id;
            }
        }
    }

    private function getSelectedCategory()
    {
        return $this->selected_category;
    }

    public function getCategories($request, $category_slug = false)
    {
        $this->setSelectedCategory($category_slug);
        $this->setCategorySort($request);
        $this->setCategoryFilter($request);
        $this->setCurrentPage($request);

        $res = Category::visible()->orderBy('sort','asc')->get();
        $result = $this->prepareCategoryForFront($res, $category_slug);

        return $result;
    }

    private function getAllProducts()
    {
        $cat = $this->getSelectedCategory();
        $data = [
            'title' => trans('custom.equipment.allproducts'),
            'slug' => 'all',
            'url' => '/equipment',
            'products' => $this->getCategoryProducts(true),
            'has_more_items' => $this->checkMoreItems(),
            'selected' => (!empty($cat)) ? false : true,
            'buttons' => [
                'sort' => $this->getSortFields(),
                'filter' => $this->getFiltersFields(),
            ],
        ];
        return $data;
    }

    private function checkMoreItems()
    {
        $category = $this->getSelectedCategory();
        if(empty($category)){
            $count = Product::active()->whereNull('deleted_at')->count();
        } else {
            $count = Product::active()->whereNull('deleted_at')->where('category_id',$category)->count();
        }
        $passed = $this->getCurrentPage() * self::PRODUCTS_PER_PAGE;

        if($count > $passed){
            return true;
        }
        return false;
    }

    private function prepareCategoryForFront($categories, $category_slug = false)
    {
        $arr_categories = [];
        $arr_categories[] = $this->getAllProducts();
        if(!empty($categories)){
            foreach($categories as $category){
                $url = getLocaleHrefPrefix().'/equipment/'.$category->slug;
                $this->setSelectedCategory($category->slug);

                $is_selected = ($category_slug != false && $category_slug == $category->slug) ? true : false;
                $data = [
                    'title' => $category->{'title' . getLocaleDBSuf()},
                    'slug' => $category->slug,
                    'url' => $url,
                    'meta_title' => $category->{'meta_title' . getLocaleDBSuf()},
                    'meta_description' => $category->{'meta_description' . getLocaleDBSuf()},
                    'meta_keywords' => $category->{'meta_keywords' . getLocaleDBSuf()},
                    'products' => $this->getCategoryProducts(),
                    'has_more_items' => $this->checkMoreItems(),
                    'selected' => $is_selected,
                    'buttons' => [
                        'sort' => $this->getSortFields(),
                        'filter' => $this->getFiltersFields($is_selected),
                    ],
                ];
                $arr_categories[] = $data;
            }
        }

        return $arr_categories;
    }

    public function getCategory($category_slug, $request)
    {
        $this->setSelectedCategory($category_slug);
        $this->setCategorySort($request);
        $this->setCategoryFilter($request);
        $this->setCurrentPage($request);
        $items = $this->getCategoryProducts();
        $has_more = $this->checkMoreItems();
        return ['items' => $items, 'has_more' => $has_more];
    }

    private function getCategoryProducts($all = false)
    {
        $products = [];
        $offset = ($this->getCurrentPage() - 1) * self::PRODUCTS_PER_PAGE;
        $limit = self::PRODUCTS_PER_PAGE;
        $query = Product::select('id','title_ru','title_ua','title_en','slug','picture','thumbnail','price','price_old','quantity','rating',
            'anons_ru','anons_ua','anons_en','new','section_description_ru','section_description_ua','section_description_en')
            ->active()
            ->whereNull('deleted_at');
        if($all == false){
            $cat_id = $this->getSelectedCategory();
            if(!empty($cat_id)){
                $query->where('category_id',$this->getSelectedCategory());
            }
        }

        $filters = $this->filters;
        if(!empty($filters)){
            foreach($filters as $filter){
                $query->whereIn('id',$filter);
            }
        }
        $query->orderBy($this->sort['field'],$this->sort['direction']);
        $res = $query->offset($offset)->limit($limit)->get();
        if(!empty($res)){
            foreach($res as $prod){
                $products[] = $this->prepareProductSectionForFront($prod);
            }
        }
        return $products;
    }

    private function prepareProductSectionForFront($product)
    {
        $data = [
            'id' => $product->id,
            'title' => $product->{'title' . getLocaleDBSuf()},
            'slug' => $product->slug,
            'url' => getLocaleHrefPrefix().'/equipment/product/'.$product->slug,
            'picture' => ($product->thumbnail != '') ? $product->thumbnail : $product->picture,
            'price' => $product->price,
            'price_old' => $product->price_old,
            'quantity' => $product->quantity,
            'can_buy' => ($product->price > 0 && $product->quantity > 0) ? true : false,
            'new' => ($product->new == 1) ? true : false,
            'top' => ($product->hit == 1) ? true : false,
            'promo' => ($product->promo == 1) ? true : false,
            'has_discount' => (!empty($product->price_old) && $product->price_old > $product->price) ? true : false,
            'anons' => $product->{'anons' . getLocaleDBSuf()},
            'rating' => ceil($product->rating),
            'section_description' => $product->{'section_description' . getLocaleDBSuf()},
        ];
        return $data;
    }

    public function getProductProperties($product)
    {
        $result = [];
        $props = CategoryProperty::with('property')
            ->where('category_id',$product['category_id'])
            ->where('show_in_product',1)
            ->orderBy('sort','asc')
            ->get();
        if(!empty($props)){
            foreach($props as $prop){
                if(!empty($prop->property)){
                    $vals = [];
                    $values = DB::table('product_properties as pp')
                        ->select('pv.*')
                        ->leftJoin('property_values as pv','pv.id','=','pp.value_id')
                        ->where('pp.property_id','=',$prop->property_id)
                        ->where('pp.product_id','=',$product['id'])
                        ->orderBy('pv.sort','asc')
                        ->get();
                    if(!empty($values)){
                        foreach($values as $v){
                            $vals[] = [
                                'value' => $v->{'value' . getLocaleDBSuf()},
                            ];
                        }
                    }
                    if(!empty($vals)){
                        $result[] = [
                            'title' => $prop->property->{'title' . getLocaleDBSuf()},
                            'code' => $prop->property->slug,
                            'multiple' => $prop->property->multiple,
                            'values' => $vals,
                        ];
                    }
                }
            }
        }

        return $result;
    }

    public function getProductBuyWithForFront($product)
    {
        $items = [];
        $res = ProductBuyWith::with('target')->where('product_id',$product['id'])->get();
        if(!empty($res)){
            foreach($res as $r){
                if(!empty($r->target)){
                    if($r->target->quantity > 0 && $r->target->active == true){
                        $items[] = $this->prepareProductSectionForFront($r->target);
                    }
                }
            }
        }
        return $items;
    }

    public function getProductForFront($product_slug)
    {
        if(isAdmin()){
            $res = Product::with('gallery')->where('slug',$product_slug)->first();
        } else {
            $res = Product::visible()->with('gallery')->where('slug',$product_slug)->first();
        }
        if($res){
            $product = $this->prepareProductDataForFront($res);
            return $product;
        }
        return false;
    }

    private function prepareProductDataForFront($product)
    {
        $gallery = [];
        if(!empty($product->gallery)){
            foreach($product->gallery as $gal){
                $gallery[] = [
                    'original' => $gal->original,
                    'thumbnail' => $gal->thumbnail,
                ];
            }
        }
        $category = Category::where('id',$product->category_id)->first();
        $warranty = ($product->warranty != '') ? trans('custom.warranty.'.$product->warranty) : null;

        $metaTitle = $product->{'title' . getLocaleDBSuf()}.': '.trans('custom.metas.shop.title');
        $metaDescription = $product->{'title' . getLocaleDBSuf()}.' '.trans('custom.metas.shop.description');

        $data = [
            'id' => $product->id,
            'title' => $product->{'title' . getLocaleDBSuf()},
            'slug' => $product->slug,
            'url' => getLocaleHrefPrefix().'/equipment/product/'.$product->slug,
            'category_id' => $product->category_id,
            'category_title' => $category->{'title' . getLocaleDBSuf()},
            'category_slug' => $category->slug,
            'picture' => $product->picture,
            'thumbnail' => $product->thumbnail,
            'price' => $product->price,
            'price_old' => $product->price_old,
            'quantity' => $product->quantity,
            'can_buy' => ($product->price > 0 && $product->quantity > 0) ? true : false,
            'new' => ($product->new == 1) ? true : false,
            'top' => ($product->hit == 1) ? true : false,
            'promo' => ($product->promo == 1) ? true : false,
            'has_discount' => (!empty($product->price_old) && $product->price_old > $product->price) ? true : false,
            'anons' => $product->{'anons' . getLocaleDBSuf()},
            'description' => $product->{'description' . getLocaleDBSuf()},
            'rating' => ceil($product->rating),
            'section_description' => $product->{'section_description' . getLocaleDBSuf()},
            'meta_title' => $metaTitle,
            'meta_description' => $metaDescription,
            'meta_keywords' => $product->{'meta_keywords' . getLocaleDBSuf()},
            'gallery' => $gallery,
            'warranty' => $warranty,
            'lastmod' => $product->updated_at,
        ];
        return $data;
    }
}
