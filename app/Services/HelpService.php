<?php

namespace App\Services;

use App\Faq;
use App\FaqGroup;
use App\HelpArticle;
use App\HelpDirectory;
use App\Tariff;

class HelpService
{
    private $answer, $tariffs = [];

    public function __construct()
    {

    }

    public function getArticleDetail($slug)
    {
        $res = HelpArticle::active()->where('slug',$slug)->first();
        $article = $this->prepareArticleDetailForFront($res);
        return $article;
    }

    public function getDirectory($slug)
    {
        $res = HelpDirectory::with('children')->active()->where('slug',$slug)->first();
        if(!$res){
            return [];
        }
        $directory = $this->prepareDirectoryForFront($res);
        $directory['children'] = $this->getDirectoryChildren($res);
        return $directory;
    }

    private function getDirectoryArticles($res)
    {
        if(!empty($res->articles)){
            $data = [];
            foreach($res->articles as $item){
                $data[] = $this->prepareArticleForFront($item,$res->slug);
            }
            return $data;
        }
        return false;
    }

    private function getDirectoryChildren($res)
    {
        if(!empty($res->children)){
            $data = [];
            foreach($res->children as $item){
                $data[] = $this->prepareDirectoryForFront($item, true);
            }
            return $data;
        }
        return false;
    }

    public function getMenu()
    {
        $items = [];
        $res = HelpDirectory::active()->where('parent_id',0)->orderBy('sort','asc')->get();
        if(!empty($res)){
            foreach($res as $item){
                $items[] = [
                    'id' => $item->id,
                    'title' => $item->{'title' . getLocaleDBSuf()},
                    'slug' => $item->slug,
                    'url' => getLocaleHrefPrefix().'/help/'.$item->slug,
                    'icon' => ($item->icon != '') ? $item->icon : '',
                ];
            }
        }
        return $items;
    }

    public function getParentDirectories()
    {
        $items = [];
        $res = HelpDirectory::active()->where('parent_id',0)->orderBy('sort','asc')->get();
        if(!empty($res)){
            foreach($res as $r){
                $items[] = $this->prepareDirectoryForFront($r);
            }
        }
        return $items;
    }

    private function prepareArticleForFront($item,$parent_slug)
    {
        $data = [
            'id' => $item->id,
            'title' => $item->{'title' . getLocaleDBSuf()},
            'anons' => $item->{'anons' . getLocaleDBSuf()},
            'slug' => $item->slug,
            'url' => getLocaleHrefPrefix().'/help/'.$parent_slug.'/'.$item->slug,
            'icon' => ($item->icon != '') ? $item->icon : '',
        ];
        return $data;
    }

    private function prepareDirectoryForFront($item, $isChild = false)
    {
        if($item->parent_id > 0){
            $parent = HelpDirectory::where('id',$item->parent_id)->first();
            if($parent){
                $data_parent = [
                    'title' => $parent->{'title' . getLocaleDBSuf()},
                    'url' => '/help/'.$parent->slug,
                ];
                if($parent->parent_id > 0){
                    $parent0 = HelpDirectory::where('id',$parent->parent_id)->first();
                    if($parent0){
                        $data_parent0 = [
                            'title' => $parent0->{'title' . getLocaleDBSuf()},
                            'url' => '/help/'.$parent0->slug,
                        ];
                    }
                }
            }

            $meta_title = $item->{'title' . getLocaleDBSuf()}.': '.$data_parent['title'].' - '.trans('custom.metas.help.title-child');
            $meta_description = $item->{'title' . getLocaleDBSuf()}.' - '.trans('custom.metas.help.description-child');
        } else {
            $meta_title = $item->{'title' . getLocaleDBSuf()}.' - '.trans('custom.metas.help.title');
            $meta_description = $item->{'title' . getLocaleDBSuf()}.' - '.trans('custom.metas.help.description');
        }
        $check = '';
        if($isChild == false){
            $check = strip_tags($item->{'detail' . getLocaleDBSuf()});
            $check = str_replace(' ','',$check);
            $check = str_replace('&nbsp;','',$check);

            $detail = addNofollowToLink($item->{'detail' . getLocaleDBSuf()});
            $detail = addAltToPics($detail, $item->{'title' . getLocaleDBSuf()});
        }

        $data = [
            'id' => $item->id,
            'title' => $item->{'title' . getLocaleDBSuf()},
            'slug' => $item->slug,
            'url' => getLocaleHrefPrefix().'/help/'.$item->slug,
            'icon' => ($item->icon != '') ? $item->icon : '',
            'anons' => addNofollowToLink($item->{'anons' . getLocaleDBSuf()}),
            'detail' => (!empty($detail)) ? $detail : '',
            'detail_empty' => (strlen($check) > 1) ? false : true,
            'children_show' => $item->articles_show_type,
            'meta_title' => (!empty($item->{'meta_title' . getLocaleDBSuf()})) ? $item->{'meta_title' . getLocaleDBSuf()} : $meta_title,
            'meta_description' => (!empty($item->{'meta_description' . getLocaleDBSuf()})) ? $item->{'meta_description' . getLocaleDBSuf()} : $meta_description,
            'meta_keywords' => $item->{'meta_keywords' . getLocaleDBSuf()},
            'parent' => (!empty($data_parent)) ? $data_parent : null,
            'parent0' => (!empty($data_parent0)) ? $data_parent0 : null,
        ];

        return $data;
    }

    private function prepareArticleDetailForFront($item)
    {
        $data = [
            'id' => $item->id,
            'title' => $item->{'title' . getLocaleDBSuf()},
            'slug' => $item->slug,
            'icon' => ($item->icon != '') ? $item->icon : '',
            'detail' => $item->{'detail' . getLocaleDBSuf()},
            'meta_title' => $item->{'meta_title' . getLocaleDBSuf()},
            'meta_description' => $item->{'meta_description' . getLocaleDBSuf()},
            'meta_keywords' => $item->{'meta_keywords' . getLocaleDBSuf()},
        ];

        $dir = HelpDirectory::where('id',$item->help_directory_id)->first();
        if($dir){
            $data['parent'] = [
                'title' => $dir->{'title' . getLocaleDBSuf()},
                'url' => '/help/'.$dir->slug,
            ];
            if($dir->parent_id > 0){
                $parent0 = HelpDirectory::where('id',$dir->parent_id)->first();
                if($parent0){
                    $data['parent0'] = [
                        'title' => $parent0->{'title' . getLocaleDBSuf()},
                        'url' => '/help/'.$parent0->slug,
                    ];
                    if($parent0->parent_id > 0){
                        $parent00 = HelpDirectory::where('id',$parent0->parent_id)->first();
                        if($parent00){
                            $data['parent00'] = [
                                'title' => $parent00->{'title' . getLocaleDBSuf()},
                                'url' => '/help/'.$parent00->slug,
                            ];

                        }
                    }
                }
            }
        }

        return $data;
    }
}
