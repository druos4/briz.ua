<?php

namespace App\Services;

use App\Cart;
use App\Product;
use DB;

class CartService
{
    protected $session_id, $user_id;

    public function __construct($session_id, $user_id)
    {
        $this->setSessionId($session_id);
        $this->setUserId($user_id);
    }

    private function setSessionId($session_id)
    {
        $this->session_id = $session_id;
    }

    private function getSessionId()
    {
        return $this->session_id;
    }

    private function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    private function getUserId()
    {
        return $this->user_id;
    }

    private function productExist($product_id)
    {
        $row = Cart::active()->noOrder()
            ->where('product_id',$product_id)
            ->where('session_id',$this->getSessionId())
            ->first();
        if($row){
            return $row;
        } else {
            return false;
        }
    }

    public function getCartCount()
    {
        return Cart::active()->noOrder()
            ->where('session_id',$this->getSessionId())
            ->sum('qnt');
    }

    public function getCartSumm()
    {
        return Cart::active()->noOrder()
            ->where('session_id',$this->getSessionId())
            ->sum('total');
    }

    public function addOrUpdateProduct($product_id)
    {
        $product = Product::visible()->find($product_id);
        if($product){
            $cart_item = $this->productExist($product_id);
            if($cart_item !== false){
                $cart_item->qnt += 1;
                $cart_item->total = $cart_item->price * $cart_item->qnt;
                $cart_item->save();
            } else {
                $data = [
                    'session_id' => $this->getSessionId(),
                    'user_id' => $this->getUserId(),
                    'product_id' => $product->id,
                    'price' => $product->price,
                    'qnt' => 1,
                    'total' => $product->price,
                    'day' => date('Y-m-d'),
                ];
                $cart_item = Cart::create($data);
            }
            return $cart_item->product_id;

        } else {

            return false;

        }
    }

    public function updateCartItem($id, $qnt)
    {
        if($id > 0 && $qnt > 0){
            $cart = Cart::active()->noOrder()->where('id',$id)->where('session_id',$this->getSessionId())->first();
            if($cart){
                $cart->qnt = $qnt;
                $cart->total = $cart->price * $cart->qnt;
                $cart->save();
                return $cart->total;
            }
        }
        return false;
    }

    public function getCartItems()
    {
        $summ = 0;
        $result = [];
        $res = Cart::with('product.category')->active()->noOrder()
            ->where('session_id',$this->getSessionId())
            ->get();
        if(!empty($res)){
            foreach($res as $r){
                $summ += $r->total;
                $result[] = $this->prepareCartItemForFron($r);
            }
        }

        return ['items' => $result, 'summ' => $summ];
    }

    private function prepareCartItemForFron($arr)
    {
        $item = [
            'id' => $arr->product->id,
            'title' => $arr->product->{'title' . getLocaleDBSuf()},
            'picture' => $arr->product->thumbnail,
            'price' => $arr->price,
            'price_old' => $arr->product->price_old,
            'qnt' => $arr->qnt,
            'total' => $arr->total,
        ];
        return $item;
    }

    public function deleteCartItem($id)
    {
        $row = Cart::where('id',$id)->active()->noOrder()
            ->where('session_id',$this->getSessionId())
            ->first();
        if($row){
            $row->delete();
        }
        return true;
    }

    public function syncCartWithOrder($order_id)
    {
        $result = Cart::with('product.category')->active()->noOrder()
            ->where('session_id',$this->getSessionId())
            ->get();
        if(!empty($result)){
            foreach($result as $cart_item){
                $cart_item->order_id = $order_id;
                $cart_item->save();
            }
        }
        return true;
    }


}
