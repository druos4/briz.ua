<?php


namespace App\Services\Genre;


use App\Genre;
use App\Services\Genre\Dto\GenreDto;

abstract class GenreSaverDecorator implements GenreSaverInterface
{

    public $decoree = null;

    public function __construct(GenreSaverInterface $decoree)
    {

        $this->decoree = $decoree;

    }

    public function save(GenreDto $genre): Genre
    {
        return $this->decoree->save($genre);
    }


}
