<?php


namespace App\Services\Genre\Dto;


class GenreDto
{
    public $code = null;

    /**
     * @return null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param null $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    public function toArray()
    {
        return [
            'code' => $this->getCode()
        ];
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray());
    }

}
