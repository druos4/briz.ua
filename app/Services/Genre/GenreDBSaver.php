<?php


namespace App\Services\Genre;


use App\Genre;
use App\Services\Genre\Dto\GenreDto;

class GenreDBSaver implements GenreSaverInterface
{
    public function save(GenreDto $genre): Genre
    {
        return Genre::updateOrCreate($genre->toArray(), ['code' => $genre->getCode()]);
    }

}
