<?php


namespace App\Services\Genre;


use App\Genre;
use App\Services\Genre\Dto\GenreDto;

interface GenreSaverInterface
{

    public function save(GenreDto $genre): Genre;

}
