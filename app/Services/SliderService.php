<?php

namespace App\Services;

use App\News;
use App\NewsTag;
use App\Post;
use App\Slider;
use App\Tag;
use Carbon\Carbon;
use mysql_xdevapi\Exception;
use Tinify\Tinify;

class SliderService
{
    const PAGINATION_ADMIN = 30;
    const PAGINATION_CLIENT = 10;
    const TABLET_WIDTH = 1260;
    const TABLET_HEIGHT = 520;
    const TABLET_X = 543;
    const TABLET_Y = 0;
    const PHONE_WIDTH = 820;
    const PHONE_HEIGHT = 420;
    const PHONE_X = 770;
    const PHONE_Y = 50;
    const PREV_WIDTH = 40;
    const PREV_HEIGHT = 25;
    const PANDA_KEY = 'VSNCzncxXy07wzmRvBwwbbgSVhffgJqw';

    private $news, $filters, $sort = ['field' => 'sort', 'direction' => 'asc'], $id;

    public function __construct()
    {

    }

    private function setId($id)
    {
        $this->id = $id;
    }

    private function getId()
    {
        return $this->id;
    }

    private function buildQuery()
    {
        $query = Slider::with('updater');

        if(isset($this->filters['active'])){
            if($this->filters['active'] == 1){
                $query->active();
            } else {
                $query->inActive();
            }
        }

        if(isset($this->filters['id'])){
            $query->where('id',$this->filters['id']);
        }

        if(isset($this->filters['title'])){
            $query->where(function ($query2) {
                $query2->where('title_ru', 'LIKE', '%'.$this->filters['title'].'%')
                    ->orWhere('title_ua', 'LIKE', '%'.$this->filters['title'].'%')
                    ->orWhere('title_en', 'LIKE', '%'.$this->filters['title'].'%');
            });
        }

        $query->orderBy($this->sort['field'],$this->sort['direction']);
        return $query;
    }

    public function getItemsForAdmin($input)
    {
        $this->setFilter($input);

        $items = $this->buildQuery()->paginate(self::PAGINATION_ADMIN);

        return ['items' => $items, 'filter' => $this->getFilters()];
    }

    public function getFilterForAdmin()
    {
        return $this->getFilters();
    }

    public function getSortingForAdmin()
    {
        return $this->getSorting();
    }

    private function setFilter($input)
    {
        $upd = false;
        if(isset($input['active']))
        {
            $this->filters['active'] = $input['active'];
            $upd = true;
        }
        if(isset($input['id']))
        {
            $this->filters['id'] = $input['id'];
            $upd = true;
        }
        if(isset($input['title']))
        {
            $this->filters['title'] = $input['title'];
            $upd = true;
        }
        if($upd == true){
            session(['filtersSliders' => $this->filters]);
        } else {
            $sess_filter = session('filtersSliders');
            if(!empty($sess_filter)){
                $this->filters = $sess_filter;
            }
        }
    }

    private function getFilters()
    {
        return $this->filters;
    }

    private function savePicture($request)
    {
        if ($request->hasFile('image'))
        {
            $f_name = strtolower('Slider').'-'.date('YmdHis').'-'.$this->getId().'-hd.'.$request->file('image')->getClientOriginalExtension();
            $file = $request->file('image');
            $path = '/uploads/sliders/';
            $file->move(public_path($path),$f_name);
            $data['picture'] = $path.$f_name;
            /*
            $f_name = strtolower('Slider').'-'.date('YmdHis').'-'.$this->getId().'-hd.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make($request->file('image'));
            $img->resize(1920, 520, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/sliders/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture'] = $path;
            */

            $f_name = strtolower('Slider').'-'.date('YmdHis').'-'.$this->getId().'-tablet.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make(public_path($data['picture']));
            $img->crop(self::TABLET_WIDTH, self::TABLET_HEIGHT, self::TABLET_X, self::TABLET_Y);
            $path = '/uploads/sliders/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture_tablet'] = $path;

            $f_name = strtolower('Slider').'-'.date('YmdHis').'-'.$this->getId().'-phone.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make(public_path($data['picture']));
            $img->crop(self::PHONE_WIDTH, self::PHONE_HEIGHT, self::PHONE_X, self::PHONE_Y);
            $path = '/uploads/sliders/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture_phone'] = $path;

            $f_name = strtolower('Slider').'-'.date('YmdHis').'-'.$this->getId().'-prev.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make(public_path($data['picture_phone']));
            $img->resize(self::PREV_WIDTH, self::PREV_HEIGHT, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/sliders/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture_prev'] = $path;

            /*
            $f_name = strtolower('Slider').'-'.date('YmdHis').'-1-'.$this->getId().'-hd.'.$request->file('image')->getClientOriginalExtension();
            $img = \Image::make(public_path($data['picture']));
            $img->resize(1905, 516, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/sliders/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture'] = $path;
*/


/*
            \Tinify\setKey(self::PANDA_KEY);
            $source = \Tinify\fromFile(public_path($data['picture']));
            $new_path = str_replace('slider-','sliders-',$data['picture']);
            $source->toFile(public_path($new_path));
            $data['picture'] = $new_path;

            $source = \Tinify\fromFile(public_path($data['picture_phone']));
            $new_path = str_replace('slider-','sliders-',$data['picture_phone']);
            $source->toFile(public_path($new_path));
            $data['picture_phone'] = $new_path;
*/

            Slider::where('id',$this->getId())->update($data);
        }
        return true;
    }

    public function storeItem($request)
    {
        $request['created_by'] = $request['updated_by'] = auth()->id();
        $request['active'] = ($request->get('active') == 1) ? 1 : 0;
        //$request['show_on_main'] = ($request->get('show_on_main') == 1) ? 1 : 0;
        $request['show_on_shop'] = ($request->get('show_on_shop') == 1) ? 1 : 0;

        $request['show_on_main'] = 1;

        $request['sort'] = ($request->get('sort') != '') ? $request->get('sort') : 10;

        if($request->get('btn-more-ch') == 1){
            //ok
        } else {
            $request['url'] = null;
        }

        $slider = Slider::create($request->all());
        $this->setId($slider->id);

        $this->savePicture($request);

        $log = new LogService();
        $log->saveLog('Slider',$slider);

        return $this->getId();
    }

    public function updateItem($request, $slider)
    {
        $this->setId($slider->id);

        $request['updated_by'] = $request['updated_by'] = auth()->id();
        $request['active'] = ($request->get('active') == 1) ? 1 : 0;
        //$request['show_on_main'] = ($request->get('show_on_main') == 1) ? 1 : 0;
        $request['show_on_main'] = 1;
        $request['show_on_shop'] = ($request->get('show_on_shop') == 1) ? 1 : 0;
        $request['sort'] = ($request->get('sort') != '') ? $request->get('sort') : 10;

        if($request->get('btn-more-ch') == 1){
            //ok
        } else {
            $request['url'] = null;
        }

        if($request->get('del-pic') == 1){
            $image = new ImageService();
            $image->deleteImage($slider->picture);
            $request['picture'] = null;
            $image->deleteImage($slider->picture_tablet);
            $request['picture_tablet'] = null;
            $image->deleteImage($slider->picture_phone);
            $request['picture_phone'] = null;
        }

        $this->savePicture($request);

        $log = new LogService();
        $log->saveLog('Slider',$slider, $request->all());

        $slider->update($request->all());

        return $this->getId();
    }

    public function getItemsForFront()
    {
        $items = \Cache::remember(getLocale().'sliders', 86400, function () {
            $res = Slider::active()->index()->orderBy('sort', 'asc')->orderBy('id', 'desc')->get();
            $items = [];
            if (!empty($res)) {
                foreach ($res as $r) {
                    $prev_pic = str_replace('-phone','-phone-prev',$r->picture_phone);
                    $items[] = [
                        'id' => $r->id,
                        'title' => $r->{'title' . getLocaleDBSuf()},
                        'pictureHd' => $r->picture,
                        'pictureTablet' => $r->picture_tablet,
                        'picturePhone' => $r->picture_phone,
                        'picture_prev' => $r->picture_prev,
                        'url' => (strpos($r->url,'http') !== false) ? $r->url : getLocaleHrefPrefix() . $r->url,
                        'anons' => $r->{'anons' . getLocaleDBSuf()},
                        'titleColor' => $r->title_color,
                        'anonsColor' => $r->anons_color,
                        'btnTitleColor' => $r->btn_title_color,
                        'btnBgColor' => $r->btn_bg_color,
                        'show_btn' => ($r->url == null) ? false : true,
                    ];
                }
            }
            return $items;
        });

        return (!empty($items)) ? $items : false;
    }

    public function getItemsForShopFront()
    {
        $items = \Cache::remember(getLocale().'shopsliders', 86400, function () {
            $res = Slider::active()->shop()->orderBy('sort', 'asc')->orderBy('id', 'desc')->get();
            $items = [];
            if (!empty($res)) {
                foreach ($res as $r) {
                    $prev_pic = str_replace('-phone','-phone-prev',$r->picture_phone);
                    $items[] = [
                        'id' => $r->id,
                        'title' => $r->{'title' . getLocaleDBSuf()},
                        'pictureHd' => $r->picture,
                        'pictureTablet' => $r->picture_tablet,
                        'picturePhone' => $r->picture_phone,
                        'picture_prev' => $r->picture_prev,
                        'url' => (strpos($r->url,'http') !== false) ? $r->url : getLocaleHrefPrefix() . $r->url,
                        'anons' => $r->{'anons' . getLocaleDBSuf()},
                        'titleColor' => $r->title_color,
                        'anonsColor' => $r->anons_color,
                        'btnTitleColor' => $r->btn_title_color,
                        'btnBgColor' => $r->btn_bg_color,
                        'show_btn' => ($r->url == null) ? false : true,
                    ];
                }
            }
            return $items;
        });

        return (!empty($items)) ? $items : false;
    }

    public function getTestSlider($id)
    {
        $r = Slider::where('id',$id)->first();
        $items = [];
        if(!empty($r)){
            $prev_pic = str_replace('-phone','-phone-prev',$r->picture_phone);
            $items[] = [
                'id' => $r->id,
                'title' => $r->{'title' . getLocaleDBSuf()},
                'pictureHd' => $r->picture,
                'pictureTablet' => $r->picture_tablet,
                'picturePhone' => $r->picture_phone,
                'picture_prev' => $r->picture_prev,
                'url' => (strpos($r->url,'http') !== false) ? $r->url : getLocaleHrefPrefix() . $r->url,
                'anons' => $r->{'anons' . getLocaleDBSuf()},
                'titleColor' => $r->title_color,
                'anonsColor' => $r->anons_color,
                'btnTitleColor' => $r->btn_title_color,
                'btnBgColor' => $r->btn_bg_color,
                'show_btn' => ($r->url == null) ? false : true,
            ];
        }

        return (!empty($items)) ? $items : false;
    }

}
