<?php

namespace App\Services\Feedback\Sender;

use App\Core\Lib\Billing\BillingApi;
use App\Services\Feedback\Sender\Dto\FeedbackDto;

class Sender implements SenderInterface
{

    /**
     * @return array
     */
    public function process(FeedbackDto $feedbackDto, $endpoint)
    {
        try {
            $billingAPI = BillingApi::get();
            $response = $billingAPI->execute($endpoint, $feedbackDto->toArray());
        } catch (ApiException $e) {
            return ['success' => false, 'error' => $e->getMessage()];
        }

        if(isset($response['success']) && $response['success'] == false){
            return ['success' => false, 'error' => $response['error']];
        }
        if(isset($response['astra']) && $response['astra'] == true){
            return ['success' => true, 'source' => 'astra'];
        }
        return ['success' => true, 'source' => 'feedback'];
    }

}
