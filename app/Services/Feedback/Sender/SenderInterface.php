<?php


namespace App\Services\Feedback\Sender;


use App\Services\Feedback\Sender\Dto\FeedbackDto;

interface SenderInterface
{

    public function process(FeedbackDto $feedbackDto, $endpoint);

}
