<?php

namespace App\Services\Feedback\Sender\Dto;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

class FeedbackDto implements Arrayable, Jsonable
{
    public $Dom = 127;
    public $Flat = 1;
    public $StreetID = 147;
    public $Comment = '';
    public $Phone = '';
    public $Name = 'Аноним';
    public $IP = '127.0.0.1';
    public $Time = null;
    public $skipAstra = true;
    public $Source = '';

    /**
     * @return null
     */
    public function getDom()
    {
        return $this->Dom;
    }

    /**
     * @param null $Dom
     */
    public function setDom($Dom): void
    {
        $this->Dom = $Dom;
    }

    /**
     * @return null
     */
    public function getFlat()
    {
        return $this->Flat;
    }

    /**
     * @param null $Flat
     */
    public function setFlat($Flat): void
    {
        $this->Flat = $Flat;
    }

    /**
     * @return null
     */
    public function getStreetID()
    {
        return $this->StreetID;
    }

    /**
     * @param null $StreetID
     */
    public function setStreetID($StreetID): void
    {
        $this->StreetID = $StreetID;
    }

    /**
     * @return null
     */
    public function getComment()
    {
        return $this->Comment;
    }

    /**
     * @param null $Comment
     */
    public function setComment($Comment): void
    {
        $this->Comment = $Comment;
    }

    /**
     * @return null
     */
    public function getPhone()
    {
        return $this->Phone;
    }

    /**
     * @param null $Phone
     */
    public function setPhone($Phone): void
    {
        $this->Phone = $this->adaptPhoneField($Phone);
    }

    /**
     * изменение номера телефона под 10 символов длины
     * @param string $phone
     * @return string
     */
    private function adaptPhoneField($phone)
    {
        if(strlen($phone) > 10){
            $start = strlen($phone) - 10;
            $phone = substr($phone,$start,10);
        }
        return $phone;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param null $Name
     */
    public function setName($Name): void
    {
        $this->Name = $Name;
    }

    /**
     * @return null
     */
    public function getIP()
    {
        return $this->IP;
    }

    /**
     * @param null $IP
     */
    public function setIP($IP): void
    {
        $this->IP = $IP;
    }

    /**
     * @return null
     */
    public function getTime()
    {
        return $this->Time;
    }

    /**
     * @param null $Time
     */
    public function setTime($Time): void
    {
        $this->Time = $Time;
    }

    /**
     * @return null
     */
    public function getSkipAstra()
    {
        return $this->skipAstra;
    }

    /**
     * @param null $skipAstra
     */
    public function setSkipAstra($skipAstra): void
    {
        $this->skipAstra = $skipAstra;
    }

    /**
     * @return null
     */
    public function getSource()
    {
        return $this->Source;
    }

    /**
     * @param null $Source
     */
    public function setSource($Source): void
    {
        $this->Source = $Source;
    }

    public function toArray()
    {
        return [
            'Dom' => $this->getDom(),
            'Flat' => $this->getFlat(),
            'StreetID' => $this->getStreetID(),
            'Comment' => $this->getComment(),
            'Phone' => $this->getPhone(),
            'Name' => $this->getName(),
            'IP' => $this->getIP(),
            'Time' => $this->getTime(),
            'skipAstra' => $this->getSkipAstra(),
            'Source' => $this->getSource(),
        ];
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray());
    }

}
