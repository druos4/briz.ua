<?php

namespace App\Services\Feedback;


use App\News;
use App\Product;
use App\Services\Feedback\Sender\Dto\FeedbackDto;
use App\Services\Feedback\Sender\Sender;
use App\Tariff;
use Carbon\Carbon;

class Feedback
{
    private $source;
    private $skipAstra;
    private $endpoint;

    public function __construct($source = '', $skipAstra = true)
    {
        $this->setSource($source);
        $this->setSkipAstra($skipAstra);
    }

    private function setSource($source)
    {
        $this->source = $source;
    }

    private function getSource()
    {
        return $this->source;
    }

    private function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    private function getEndpoint()
    {
        return $this->endpoint;
    }

    private function setSkipAstra($skipAstra)
    {
        $this->skipAstra = $skipAstra;
    }

    private function getSkipAstra()
    {
        return $this->skipAstra;
    }

    public function send($input = [])
    {
        switch ($this->getSource()){
            case 'callMe':
                $dataToSend = $this->makeCallMeDto($input);
                $this->setEndpoint('send-feedback');
                break;
            case 'feedback':
                $dataToSend = $this->makeFeedbackDto($input);
                $this->setEndpoint('send-feedback');
                break;
            case 'orderService':
                $tariff = Tariff::find($input['service']);
                if($tariff->user_type == 2){
                    $this->setEndpoint('send-legal-feedback');
                } else {
                    $this->setEndpoint('send-feedback');
                }
                $dataToSend = $this->makeOrderServiceDto($input, $tariff);
                break;
            case 'connectAction':
                $dataToSend = $this->makeConnectActionDto($input);
                $this->setEndpoint('send-feedback');
                break;
            case 'orderDevice':
                $dataToSend = $this->makeOrderDeviceDto($input);
                $this->setEndpoint('send-feedback');
                break;
            default:
                $dataToSend = $this->makeCallMeDto($input);
                $this->setEndpoint('send-feedback');
                break;
        }
        $sender = new Sender();
        $response = $sender->process($dataToSend, $this->getEndpoint());
        return $response;
    }

    private function makeCallMeDto($input = [])
    {
        $dataToSend = new FeedbackDto();
        $dataToSend->setSkipAstra($this->getSkipAstra());
        $dataToSend->setPhone($input['phone']);
        $dataToSend->setTime(strtotime(Carbon::now()));
        $dataToSend->setSource($this->getSource());
        $dataToSend->setComment('Заявка на перезвон');
        return $dataToSend;
    }

    private function makeFeedbackDto($input = [])
    {
        $dataToSend = new FeedbackDto();
        $dataToSend->setSkipAstra($this->getSkipAstra());
        $dataToSend->setPhone($input['phone']);
        $dataToSend->setTime(strtotime(Carbon::now()));
        $dataToSend->setSource($this->getSource());
        if(!empty($input['name'])){
            $dataToSend->setName($input['name']);
        }
        $comment = '';
        $comment .= (!empty($input['address'])) ? 'Адрес: '.strip_tags($input['address']).'<br />' : '';
        $comment .= (!empty($input['name'])) ? 'Имя: '.strip_tags($input['name']).'<br />' : '';
        $comment .= (!empty($input['comment'])) ? 'Комментарий: '.strip_tags($input['comment']).'<br />' : '';
        $dataToSend->setComment($comment);

        return $dataToSend;
    }

    private function makeOrderServiceDto($input = [], $tariff)
    {
        $dataToSend = new FeedbackDto();
        $dataToSend->setSkipAstra($this->getSkipAstra());
        $dataToSend->setPhone($input['phone']);
        $dataToSend->setTime(strtotime(Carbon::now()));
        $dataToSend->setSource($this->getSource());
        $name = (!empty($input['name'])) ? $input['name'] : 'Аноним';
        $dataToSend->setName($name);
        if(!empty($input['films'])){
            $comment = 'Заявка на подключение пакета &laquo;'.$tariff->title_ru.'&raquo; + ФИЛЬМЫ от клиента '.$name;
        } else {
            $comment = 'Заявка на подключение пакета &laquo;'.$tariff->title_ru.'&raquo; от клиента '.$name;
        }

        $dataToSend->setComment($comment);

        return $dataToSend;
    }

    private function makeConnectActionDto($input = [])
    {
        $dataToSend = new FeedbackDto();
        $dataToSend->setSkipAstra($this->getSkipAstra());
        $dataToSend->setPhone($input['phone']);
        $dataToSend->setTime(strtotime(Carbon::now()));
        $dataToSend->setSource($this->getSource());
        $name = (!empty($input['name'])) ? $input['name'] : 'Аноним';
        $dataToSend->setName($name);
        $action = News::active()->where('id',$input['id'])->first();
        $comment = 'Заявка на подключение акции &laquo;'.$action->title_ru.'&raquo; от клиента '.$name;
        $dataToSend->setComment($comment);

        return $dataToSend;
    }

    private function makeOrderDeviceDto($input = [])
    {
        $dataToSend = new FeedbackDto();
        $dataToSend->setSkipAstra($this->getSkipAstra());
        $dataToSend->setPhone($input['phone']);
        $dataToSend->setTime(strtotime(Carbon::now()));
        $dataToSend->setSource($this->getSource());
        $name = (!empty($input['name'])) ? $input['name'] : 'Аноним';
        $dataToSend->setName($name);

        $product = Product::find($input['product']);
        $comment = $product->title_ru.' заявка на заказ от клиента '.$name;
        $dataToSend->setComment($comment);

        return $dataToSend;
    }

}
