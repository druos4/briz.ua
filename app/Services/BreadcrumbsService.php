<?php

namespace App\Services;

use App\Faq;
use App\FaqGroup;
use App\Tariff;

class BreadcrumbsService
{
    private $entity;
    private $items = [];
    private $levels = [
        'news' => [
            'title' => 'news',
            'url' => '/news',
        ],
        'actions' => [
            'title' => 'actions',
            'url' => '/actions',
        ],
        'faq' => [
            'title' => 'faq',
            'url' => '/faq',
        ],
        'equipment' => [
            'title' => 'equipment',
            'url' => '/equipment',
        ],
        'help' => [
            'title' => 'help',
            'url' => '/help',
        ],
        'blog' => [
            'title' => 'blog',
            'url' => '/blog',
        ],
    ];

    public function __construct($entity_type = null)
    {
        $this->setEntity($entity_type);
    }

    private function pushToItems($arr)
    {
        $this->items[] = $arr;
    }

    private function getItems()
    {
        return $this->items;
    }

    private function getLevels()
    {
        return $this->levels;
    }

    private function setEntity($entity)
    {
        $this->entity = $entity;
    }

    private function getEntity()
    {
        return $this->entity;
    }

    public function getBreadcrumbs($current_item = null, $sublevel = null, $sublevel2 = null)
    {
        if(app()->getLocale() != 'ua'){
            $this->pushToItems([
                'title' => trans('breadcrumbs.main'),
                'url' => getLocaleHrefPrefix(),
            ]);
        } else {
            $this->pushToItems([
                'title' => trans('breadcrumbs.main'),
                'url' => '/',
            ]);
        }

        $levels = $this->getLevels();
        if(isset($levels[$this->getEntity()])){
            $this->pushToItems([
                'title' => trans('breadcrumbs.'.$levels[$this->getEntity()]['title']),
                'url' => getLocaleHrefPrefix().$levels[$this->getEntity()]['url'],
            ]);
            if(!empty($sublevel)){
                if(app()->getLocale() != 'ua'){
                    $subLvlUrl = (strpos($sublevel['url'],'/'.app()->getLocale().'/') !== false) ? $sublevel['url'] : getLocaleHrefPrefix().$sublevel['url'];
                } else {
                    $subLvlUrl = getLocaleHrefPrefix().$sublevel['url'];
                }
                $this->pushToItems([
                    'title' => $sublevel['title'],
                    'url' => $subLvlUrl,
                ]);
            }
            if(!empty($sublevel2)){
                $this->pushToItems([
                    'title' => $sublevel2['title'],
                    'url' => getLocaleHrefPrefix().$sublevel2['url'],
                ]);
            }

        }
        if(!empty($current_item)){
            $arr = parse_url(url()->current());
            $this->pushToItems([
                'title' => $current_item,
                'url' => (!empty($arr['path'])) ? $arr['path'] : '',
            ]);
        }

        return $this->getItems();
    }

}
