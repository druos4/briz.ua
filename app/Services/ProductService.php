<?php

namespace App\Services;

use App\Category;
use App\Core\Lib\Billing\BillingApi;
use App\Product;
use App\ProductBuyWith;
use Carbon\Carbon;
use DB;

class ProductService
{
    const CATEGORIESFORRANDOM = [10,9,7,6,5];
    const ADMIN_PAGINATE = 30;

    const HD_WIDTH = 1920;
    const HD_HEIGHT = 520;
    const TABLET_WIDTH = 1260;
    const TABLET_HEIGHT = 520;
    const PHONE_WIDTH = 820;
    const PHONE_HEIGHT = 420;

    const REC_HD_WIDTH = 500;
    const REC_HD_HEIGHT = 500;
    const REC_TABLET_WIDTH = 500;
    const REC_TABLET_HEIGHT = 500;
    const REC_PHONE_WIDTH = 500;
    const REC_PHONE_HEIGHT = 500;

    private $products, $filters, $sort = ['field' => 'sort', 'direction' => 'asc'], $id;

    public function __construct()
    {

    }

    private function setId($id)
    {
        $this->id = $id;
    }

    private function getId()
    {
        return $this->id;
    }

    private function getCategoriesWithRecommendProducts()
    {
        $ids = [];
        $categories = Category::where('parent_id',0)->where('recommend_products',true)->get();
        if(!empty($categories)){
            foreach($categories as $category){
                $ids[] = $category->id;
            }
        }
        return $ids;
    }

    private function prepareDataForFront($product)
    {
        $item = [
            'id' => $product->id,
            'title' => $product->{'title' . getLocaleDBSuf()},
            'slug' => $product->slug,
            'url' => getLocaleHrefPrefix().'/'.$product->category->slug.'/'.$product->slug,
            'picture' => $product->picture,
            'anons' => $product->{'anons' . getLocaleDBSuf()},
            'price' => $product->price,
            'discount' => $product->discount,
            'price_old' => $product->price_old,
            'category_title' => $product->category->{'uslugy_title' . getLocaleDBSuf()},
            'category_text' => $product->category->{'uslugy_description' . getLocaleDBSuf()},
        ];
        return $item;
    }

    public function getRandomProduct()
    {
        if(!empty($this->getCategoriesWithRecommendProducts())){
            $count = Product::visible()->whereIn('category_id',$this->getCategoriesWithRecommendProducts())->count();
            $product = Product::with('category')->visible()->whereIn('category_id',$this->getCategoriesWithRecommendProducts())->offset(rand(0,$count - 1))->first();
        }
        return (!empty($product)) ? $this->prepareDataForFront($product) : false;
    }

    public function getItemsForAdmin($request)
    {
        $this->setFilter($request);
        $this->setSorting($request);

        $items = $this->buildQuery()->paginate(self::ADMIN_PAGINATE);
        if(!empty($items)){
            foreach($items as $item){
                $item->buywith = ProductBuyWith::where('product_id',$item->id)->count();
            }
        }
        return ['items' => $items, 'filter' => $this->getFilters(), 'sort' => $this->getSorting()];
    }

    private function buildQuery()
    {
        $query = Product::with('category');

        if(isset($this->filters['active'])){
            if($this->filters['active'] == 1){
                $query->active();
            } else {
                $query->NotActive();
            }
        }

        if(isset($this->filters['new'])){
            if($this->filters['new'] == 1){
                $query->new();
            } else {
                $query->NotNew();
            }
        }

        if(isset($this->filters['hit'])){
            if($this->filters['hit'] == 1){
                $query->hit();
            } else {
                $query->NotHit();
            }
        }

        if(!empty($this->filters['id'])){
            $query->where('id',$this->filters['id']);
        }

        if(isset($this->filters['search'])){
            $query->where(function ($query2) {
                $query2->where('title_ru', 'LIKE', '%'.$this->filters['search'].'%')
                    ->orWhere('title_ua', 'LIKE', '%'.$this->filters['search'].'%')
                    ->orWhere('title_en', 'LIKE', '%'.$this->filters['search'].'%');
            });
        }

        if(isset($this->filters['stock'])){
            if($this->filters['stock'] == 1){
                $query->inStock();
            } else {
                $query->NotInStock();
            }
        }

        if(!empty($this->filters['category'])){
            $query->where('category_id',$this->filters['category']);
        }

        $query->orderBy($this->sort['field'],$this->sort['direction']);
        return $query;
    }

    private function setFilter($input)
    {
        $upd = false;
        if(isset($input['id']))
        {
            $this->filters['id'] = $input['id'];
            $upd = true;
        }
        if(isset($input['search']))
        {
            $this->filters['search'] = $input['search'];
            $upd = true;
        }
        if(isset($input['hit']))
        {
            $this->filters['hit'] = $input['hit'];
            $upd = true;
        }
        if(isset($input['new']))
        {
            $this->filters['new'] = $input['new'];
            $upd = true;
        }
        if(isset($input['stock']))
        {
            $this->filters['stock'] = $input['stock'];
            $upd = true;
        }
        if(isset($input['active']))
        {
            $this->filters['active'] = $input['active'];
            $upd = true;
        }
        if(isset($input['category']))
        {
            $this->filters['category'] = $input['category'];
            $upd = true;
        }
        if($upd == true){
            session(['filtersProducts' => $this->filters]);
        } else {
            $sess_filter = session('filtersProducts');
            if(!empty($sess_filter)){
                $this->filters = $sess_filter;
            }
        }
    }

    private function getFilters()
    {
        return $this->filters;
    }

    private function setSorting($input)
    {
        if(!empty($input['field']) && !empty($input['direction'])){
            $this->sort = ['field' => $input['field'], 'direction' => $input['direction']];
        }
    }

    private function getSorting()
    {
        return $this->sort;
    }


    private function updateProductProperties($request)
    {
        DB::table('product_properties')->where('product_id','=',$this->getId())->delete();
        foreach ($request->all() as $key => $req){
            if(strpos($key,'property-') !== false && is_array($req) && count($req) > 0){
                $aKey = explode('-',$key);
                foreach ($req as $k => $v){
                    if($v > 0){
                        DB::table('product_properties')->insert(['product_id' => $this->getId(),
                            'property_id' => $aKey[1],
                            'value_id' => $v]);
                    }
                }
            }
        }
    }

    private function clearPics($product, $type = '')
    {
        if($type == 'main'){
            if(file_exists(public_path($product->picture))){
                unlink(public_path($product->picture));
            }
            if(file_exists(public_path($product->thumbnail))){
                unlink(public_path($product->thumbnail));
            }
        } elseif($type == 'recomend') {
            if(file_exists(public_path($product->picture_recomend_hd))){
                unlink(public_path($product->picture_recomend_hd));
            }
            if(file_exists(public_path($product->picture_recomend_tablet))){
                unlink(public_path($product->picture_recomend_tablet));
            }
            if(file_exists(public_path($product->picture_recomend_phone))){
                unlink(public_path($product->picture_recomend_phone));
            }
        }
    }

    public function updateProduct($request, $id)
    {
        $this->setId($id);
        $product = Product::find($id);
        $request['active'] = ($request->get('active') == 1) ? 1 : 0;
        $request['new'] = ($request->get('new') == 1) ? 1 : 0;
        $request['promo'] = ($request->get('promo') == 1) ? 1 : 0;
        $request['hit'] = ($request->get('hit') == 1) ? 1 : 0;
        $request['top'] = ($request->get('top') == 1) ? 1 : 0;
        $request['quantity'] = (!empty($request['quantity'])) ? $request['quantity'] : 0;
        $request['title_en'] = (!empty($request['title_en'])) ? $request['title_en'] : '';
        $request['price'] = (!empty($request['price'])) ? $request['price'] : 0;

        if(isset($request['del-pic']) && $request['del-pic'] == 1){
            $this->clearPics($product, 'main');
            $request['picture'] = null;
            $request['thumbnail'] = null;
        }

        if(isset($request['del-pic-rec']) && $request['del-pic-rec'] == 1){
            $this->clearPics($product, 'recomend');
            $request['picture_recomend_hd'] = null;
            $request['picture_recomend_tablet'] = null;
            $request['picture_recomend_phone'] = null;
        }

        if ($request->hasFile('image'))
        {
            $image = new ImageService();
            $request['picture'] = $image->copyImage($request->file('image'),'Product');
            $request['thumbnail'] = $image->storeThumbnail($request['picture'],'Product', $request->file('image'));
            if($request['picture'] === false){
                unset($request['picture']);
            }
            if($request['thumbnail'] === false){
                unset($request['thumbnail']);
            }
        }

        $product->update($request->all());
        Product::syncPhotos($request->get('gallery-photos'),$this->getId());

        $this->updateProductProperties($request);
        $this->savePicture($request);

        return $this->getId();
    }

    public function storeProduct($request)
    {
        $request['active'] = (!empty($request['active'])) ? 1 : 0;
        $request['new'] = (!empty($request['new'])) ? 1 : 0;
        $request['promo'] = (!empty($request['promo'])) ? 1 : 0;
        $request['hit'] = (!empty($request['hit'])) ? 1 : 0;
        $request['top'] = (!empty($request['top'])) ? 1 : 0;
        $request['quantity'] = (!empty($request['quantity'])) ? $request['quantity'] : 0;
        $request['title_en'] = (!empty($request['title_en'])) ? $request['title_en'] : '';
        $request['price'] = (!empty($request['price'])) ? $request['price'] : 0;

        if ($request->hasFile('image'))
        {
            $image = new ImageService();
            $request['picture'] = $image->copyImage($request->file('image'),'Product');
            $request['thumbnail'] = $image->storeThumbnail($request['picture'],'Product',$request->file('image'));
            if($request['picture'] === false){
                unset($request['picture']);
            }
            if($request['thumbnail'] === false){
                unset($request['thumbnail']);
            }
        }

        $product = Product::create($request->all());
        $this->setId($product->id);

        $this->savePicture($request);

        return $this->getId();
    }

    private function savePicture($request)
    {
        if ($request->hasFile('image_recomend'))
        {
            $f_name = strtolower('product').'-'.$this->getId().'rec-hd.'.$request->file('image_recomend')->getClientOriginalExtension();
            $img = \Image::make($request->file('image_recomend'));

            $img->resize(self::REC_HD_WIDTH, self::REC_HD_HEIGHT, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/products/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture_recomend_hd'] = $path;

            $f_name = strtolower('product').'-'.$this->getId().'rec-tablet.'.$request->file('image_recomend')->getClientOriginalExtension();
            $img = \Image::make(public_path($data['picture_recomend_hd']));
            $img->resize(self::REC_TABLET_WIDTH, self::REC_TABLET_HEIGHT, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/sliders/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture_recomend_tablet'] = $path;

            $f_name = strtolower('product').'-'.$this->getId().'rec-phone.'.$request->file('image_recomend')->getClientOriginalExtension();
            $img = \Image::make(public_path($data['picture_recomend_hd']));
            $img->resize(self::REC_PHONE_WIDTH, self::REC_PHONE_HEIGHT, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/sliders/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture_recomend_phone'] = $path;

            Product::where('id',$this->getId())->update($data);
        }
        return true;
    }

    public function importProductsFromBilling()
    {
        $endpoint = 'getProducts';
        try {
            $billingAPI = BillingApi::get();
            $response = $billingAPI->execute($endpoint, []);
        } catch (ApiException $e) {
            print_r($e->getMessage());
            die;
        }

        $updated = 0;
        if(!empty($response)){
            foreach($response as $arr){
                $upd = $this->updateProductPrice($arr);
                if($upd == true){
                    $updated++;
                }
            }
        }
        return $updated;
    }

    private function updateProductPrice($arr){
        if(isset($arr['name']) && isset($arr['price']) && isset($arr['count'])){
            $product = Product::where('billing_name',$arr['name'])->first();
            if($product){
                $product->price = $arr['price'];
                $product->quantity = $arr['count'];
                $product->save();
                return true;
            }
        }
        return false;
    }

}
