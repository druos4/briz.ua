<?php

namespace App\Services;

use App\Blog;
use App\BlogCategory;
use App\BlogCategoryIn;
use App\BlogComment;
use App\BlogLang;
use App\BlogLike;
use App\BlogRecomend;
use App\BlogView;
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\Boolean;
use voku\helper\HtmlDomParser;

class BlogService
{
    const PAGINATION_ADMIN = 30;
    const PAGINATION_CLIENT = 12;
    const PAGINATION_COMMENTS_CLIENT = 2;
    const PAGINATION_COMMENTS_CLIENT_FIRST = 2;
    const UPLOAD_PATH = '/uploads/blogs/';
    const FIELDS = ['detail', 'meta_title', 'meta_description', 'meta_keywords'];
    const SORT = ['new', 'popular'];
    const PICTURE_WIDTH = 765;
    const PICTURE_HEIGHT = 500;
    const THUMBNAIL_WIDTH = 400;
    const THUMBNAIL_HEIGHT = 262;
    const BASE_SORT = 'new';



    private $blog, $filters, $sort = ['field' => 'id', 'direction' => 'desc'], $id;
    private $langs = [];
    private $currentSort = 'new';
    private $currentPage = 1;
    private $categoryId = null;
    private $categorySlug = null;
    private $blogId = null;


    public function __construct()
    {
        $this->setLangs(getLangsCodes());
    }

    /**
     * @param int $id
     */
    private function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    private function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    private function setBlogId($id): void
    {
        $this->blogId = $id;
    }

    /**
     * @return int
     */
    private function getBlogId()
    {
        return $this->blogId;
    }

    /**
     * @param int $id
     */
    private function setCategorySlug($slug): void
    {
        $this->categorySlug = $slug;
    }

    /**
     * @return int
     */
    private function getCategorySlug()
    {
        return $this->categorySlug;
    }

    /**
     * @param string $slug
     */
    private function setCategoryId($slug): bool
    {
        $this->setCategorySlug($slug);
        if (!empty($slug)) {
            $category = BlogCategory::where('slug', $slug)->isActive()->first();
            if ($category) {
                $this->categoryId = $category->id;
                return true;
            }
        }
        return false;
    }

    /**
     * @return int
     */
    private function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param string $sortCode
     */
    private function setCurrentSort($sortCode): void
    {
        $this->currentSort = $sortCode;
    }

    /**
     * @return string
     */
    private function getCurrentSort()
    {
        return $this->currentSort;
    }

    /**
     * @param string $pageNum
     */
    private function setCurrentPage($pageNum): void
    {
        $this->currentPage = $pageNum;
    }

    /**
     * @return string
     */
    private function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @param array $langs
     */
    private function setLangs($langs): void
    {
        $this->langs = $langs;
    }

    /**
     * @return array
     */
    private function getLangs()
    {
        return $this->langs;
    }

    /**
     * Получение статей и активного фильтра для админки
     * @return array
     */
    public function getItemsForAdmin($request = [])
    {
        $query = Blog::with('categories.category');
        if (!empty($request['category'])) {
            $res = BlogCategoryIn::where('blog_category_id', $request['category'])->get();
            $ids = [];
            if (!empty($res)) {
                foreach ($res as $r) {
                    $ids[] = $r->blog_id;
                }
                $query->whereIn('id', $ids);
            }
        }

        if (isset($request['active']) && $request['active'] == 'y') {
            $query->isActive();
        } elseif (isset($request['active']) && $request['active'] == 'n') {
            $query->isInActive();
        }
        if (!empty($request['slug'])) {
            $query->where('slug', 'LIKE', '%' . $request['slug'] . '%');
        }
        if (!empty($request['title'])) {
            $query->where(function ($query2) use ($request) {
                $query2->where('title_ru', 'LIKE', '%' . $request['title'] . '%')
                    ->orWhere('title_ua', 'LIKE', '%' . $request['title'] . '%')
                    ->orWhere('title_en', 'LIKE', '%' . $request['title'] . '%');
            });
        }
        if (!empty($request['id'])) {
            $query->where('id', $request['id']);
        }

        $blogs = $query->orderBy('id', 'desc')->paginate($this::PAGINATION_ADMIN);

        return ['blogs' => $blogs, 'filter' => $request];
    }

    /**
     * Получение статей и активного фильтра для админки
     * @return array
     */
    public function getCommentsForAdmin($request = [])
    {
        $query = BlogComment::query();
        if (!empty($request['blog'])) {
            $query->where('blog_id', $request['blog']);
        }
        if (isset($request['moderated']) && $request['moderated'] == 'y') {
            $query->whereNotNull('moderated_at');
        } elseif (isset($request['moderated']) && $request['moderated'] == 'n') {
            $query->whereNull('moderated_at');
        }
        if (!empty($request['author'])) {
            $query->where('author', 'LIKE', '%' . $request['author'] . '%');
        }
        if (!empty($request['id'])) {
            $query->where('id', $request['id']);
        }
        $comments = $query->orderBy('id', 'desc')->paginate(30);
        return ['comments' => $comments, 'filter' => $request];
    }

    /**
     * Проверка SLUG на уникальность среди стетей и категорий
     * @return boolean
     */
    public function checkSlug($request)
    {
        $query = Blog::where('slug', $request->get('slug'));
        if (!empty($request->get('id')) && $request->get('entity') == 'blog') {
            $query->where('id', '!=', $request->get('id'));
        }
        $item = $query->first();
        if (!empty($item)) {
            return false;
        }

        $query = BlogCategory::where('slug', $request->get('slug'));
        if (!empty($request->get('id')) && $request->get('entity') == 'category') {
            $query->where('id', '!=', $request->get('id'));
        }
        $item = $query->first();
        if (!empty($item)) {
            return false;
        }

        return true;
    }

    /**
     * Сохранение новой статьи
     * @return object
     */
    public function storeItem($request)
    {
        $request['is_active'] = ($request->get('is_active') == 1) ? true : false;
        if ($request->get('publish_date') != '' && $request->get('publish_time') != '') {
            $request['published_at'] = $request->get('publish_date') . ' ' . $request->get('publish_time') . ':00';
        } else {
            $request['published_at'] = Carbon::now();
        }
        $request['updated_by'] = auth()->id();
        $blog = Blog::create($request->all());
        $this->setId($blog->id);
        $this->saveLangs($request);
        $this->savePicture($request);
        $this->syncRecomend($request);
        $this->syncCategories($request);

        return $blog;
    }

    /**
     * Удаление основной картинки статьи
     * @param object $blog
     */
    private function deletePicture($blog)
    {
        if ($blog->picture != '') {
            if (file_exists(public_path($blog->picture))) {
                unlink(public_path($blog->picture));
            }
        }
        if ($blog->thumbnail != '') {
            if (file_exists(public_path($blog->thumbnail))) {
                unlink(public_path($blog->thumbnail));
            }
        }
        return true;
    }

    /**
     * Сохранение статьи
     * @param int $id
     */
    public function updateItem($request, $id)
    {
        $blog = Blog::find($id);
        if (!$blog) {
            return false;
        }
        $this->setId($blog->id);
        $request['is_active'] = ($request->get('is_active') == 1) ? true : false;
        if ($request->get('publish_date') != '' && $request->get('publish_time') != '') {
            $request['published_at'] = $request->get('publish_date') . ' ' . $request->get('publish_time') . ':00';
        } else {
            $request['published_at'] = Carbon::now();
        }
        $request['updated_by'] = auth()->id();
        if (!empty($request->get('del-pic'))) {
            $request['picture'] = null;
            $this->deletePicture($blog);
        }
        $blog->update($request->all());

        $this->saveLangs($request);
        $this->savePicture($request);
        $this->syncRecomend($request);
        $this->syncCategories($request);

        return $blog;
    }

    /**
     * Сохранение подробного текста и мета-тегов статьи
     * @return void
     */
    private function saveLangs($request)
    {
        $fields = $this::FIELDS;
        if (!empty($fields)) {
            foreach ($fields as $field) {
                $this->saveField($field, $request);
            }
        }
    }

    /**
     * Сохранение в БД конкретной записи подробного текста или мета-тега
     * @param string $field
     */
    private function saveField($field, $request)
    {
        if (!empty($this->getLangs())) {
            foreach ($this->getLangs() as $lang) {

                $key = $field . '_' . $lang;
                if (isset($request[$key])) {
                    $data = [
                        'blog_id' => $this->getId(),
                        'field' => $field,
                        'value' => $request[$key],
                        'lang' => $lang,
                    ];
                    BlogLang::updateOrCreate(['blog_id' => $this->getId(), 'field' => $key, 'lang' => $lang], $data);
                }
            }
        }
        return true;
    }

    /**
     * Соранение файла-картинки и обновление статьи
     * @return void
     */
    private function savePicture($request)
    {
        if ($request->hasFile('image')) {
            if (!\File::exists(public_path($this::UPLOAD_PATH))) {
                \File::makeDirectory(public_path($this::UPLOAD_PATH));
            }
            $suf = date('YmdHis');

            $f_name = strtolower('Blog') . '-' . $this->getId() . '-' . $suf . '.' . $request->file('image')->getClientOriginalExtension();
            $img = \Image::make($request->file('image'));
            $img->resize(self::PICTURE_WIDTH, self::PICTURE_HEIGHT, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/blogs/'.$f_name;
            $img->save(public_path($path), 100);
            $data['picture'] = $path;

            $f_name = strtolower('Blog') . '-thumb-' . $this->getId() . '-' . $suf . '.' . $request->file('image')->getClientOriginalExtension();
            $img = \Image::make($request->file('image'));
            $img->resize(self::THUMBNAIL_WIDTH, self::THUMBNAIL_HEIGHT, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $path = '/uploads/blogs/'.$f_name;
            $img->save(public_path($path), 100);
            $data['thumbnail'] = $path;

            Blog::where('id', $this->getId())->update($data);
        }
    }

    /**
     * Сохранение списка рекомендуемых статей
     * @return void
     */
    private function syncRecomend($request)
    {
        BlogRecomend::where('blog_id', $this->getId())->delete();
        if (!empty($request->get('recomends'))) {
            foreach ($request->get('recomends') as $recomendId) {
                BlogRecomend::create([
                    'blog_id' => $this->getId(),
                    'target_id' => $recomendId,
                ]);
            }
        }
    }

    /**
     * Сохранение категорий статьи
     * @return void
     */
    private function syncCategories($request)
    {
        BlogCategoryIn::where('blog_id', $this->getId())->delete();
        if (!empty($request->get('categories'))) {
            foreach ($request->get('categories') as $categoryId) {
                BlogCategoryIn::create([
                    'blog_id' => $this->getId(),
                    'blog_category_id' => $categoryId,
                ]);
            }
        }
    }

    /**
     * Удаление статьи
     * @param int $id
     */
    public function deleteItem($id)
    {
        Blog::where('id',$id)->delete();
    }

    /**
     * Получение статьи для редактирования
     * @param int $id
     * @return array
     */
    public function getItemForEdit($id)
    {
        $this->setId($id);
        $blog = Blog::find($this->getId());
        if (!$blog) {
            return false;
        }
        $dt = strtotime($blog->published_at);
        $blog->publish_date = date('Y-m-d', $dt);
        $blog->publish_time = date('H:i', $dt);
        $langs = $this->getBlogLangs();
        $recomends = $this->getRecomendIds();
        $categoriesIn = $this->getCategoriesInIds();

        return ['blog' => $blog, 'langs' => $langs, 'recomends' => $recomends, 'categoriesIn' => $categoriesIn];
    }

    /**
     * Получение подробного описания статьи и мета-тегов
     * @return array
     */
    private function getBlogLangs()
    {
        $langs = [];
        $res = BlogLang::where('blog_id', $this->getId())->get();
        if (!empty($res)) {
            foreach ($res as $r) {
                $langs[$r->field][$r->lang] = $r->value;
            }
        }
        return $langs;
    }

    /**
     * Получение ID рекомендуемых статей
     * @return array
     */
    private function getRecomendIds()
    {
        $ids = [];
        $res = BlogRecomend::where('blog_id', $this->getId())->get();
        if (!empty($res)) {
            foreach ($res as $r) {
                $ids[] = $r->target_id;
            }
        }
        return $ids;
    }

    /**
     * Получение ID категорий статьи
     * @return array
     */
    private function getCategoriesInIds()
    {
        $ids = [];
        $res = BlogCategoryIn::where('blog_id', $this->getId())->get();
        if (!empty($res)) {
            foreach ($res as $r) {
                $ids[] = $r->blog_category_id;
            }
        }
        return $ids;
    }

    /**
     * Сохрание просмотра статьи
     * @param string $slug
     */
    public function addViewToBlog($slug)
    {
        $blog = Blog::isActive()->notDeleted()->where('slug', $slug)->first();
        if ($blog) {
            $existView = BlogView::where('blog_id', $blog->id)
                ->where('session_id', session()->getId())
                ->first();
            if (!$existView) {
                BlogView::create([
                    'blog_id' => $blog->id,
                    'session_id' => session()->getId(),
                ]);
                $count = $blog->views_count + 1;
                $blog->update(['views_count' => $count]);
            }
        }
        return true;
    }

    /**
     * Сохранение лайка к статье
     * @return boolean
     */
    public function addLikeToBlog($request)
    {
        if (!empty($request->get('blog'))) {
            $blog = Blog::isActive()->notDeleted()->where('id', $request->get('blog'))->first();
            if ($blog) {
                $existLike = BlogLike::where('blog_id', $blog->id)
                    ->where('session_id', session()->getId())
                    ->first();
                if (!$existLike) {
                    BlogLike::create([
                        'blog_id' => $blog->id,
                        'session_id' => session()->getId(),
                    ]);
                    $count = $blog->likes_count + 1;
                    $blog->update(['likes_count' => $count]);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Сохранение комментария к статье
     * @return boolean
     */
    public function addCommentToBlog($request)
    {
        if (!empty($request->get('id'))) {
            $blog = Blog::isActive()->notDeleted()->where('id', $request->get('id'))->first();
            if ($blog) {
                if($request->get('parent') > 0){
                    $parentComment = BlogComment::where('blog_id',$blog->id)
                        ->where('id',$request->get('parent'))
                        ->first();
                }
                $parentId = (!empty($parentComment)) ? $parentComment->id : 0;
                $lvl = (!empty($parentComment)) ? ($parentComment->lvl + 1) : 0;

                BlogComment::create([
                    'blog_id' => $request->get('id'),
                    'parent_id' => $parentId,
                    'lvl' => $lvl,
                    'author' => $request->get('author'),
                    'comment' => $request->get('comment'),
                ]);
                return true;
            }
        }
        return false;
    }

    /**
     * Получение списка категорий для публичной части
     * @param string $slug
     * @return array
     */
    public function getCategoriesListForFront($slug = null)
    {
        $categories[] = [
            'title' => trans('custom.blog.all-articles'),
            'slug' => '',
            'url' => getLocaleHrefPrefix() . '/blog',
            'selected' => (empty($slug)) ? true : false,
        ];
        $res = BlogCategory::isActive()->get();
        if (!empty($res)) {
            foreach ($res as $r) {
                $categories[] = $this->prepareCategoryDataForFront($r, $slug);
            }
        }
        return $categories;
    }

    /**
     * Подготовка данных категории для публичной стороны
     * @param object $item
     * @param string $slug
     * @return array
     */
    private function prepareCategoryDataForFront($item, $slug = null)
    {
        $data = [
            'title' => $item->{'title' . getLocaleDBSuf()},
            'slug' => $item->slug,
            'url' => getLocaleHrefPrefix() . '/blog/category/' . $item->slug,
            'selected' => (!empty($slug) && $slug == $item->slug) ? true : false,
        ];
        return $data;
    }

    /**
     * Получение подробного текста и мета-тегов для публичной части
     * @return array
     */
    private function getBlogLangsForFront()
    {
        $langs = [];
        $res = BlogLang::where('blog_id', $this->getBlogId())->where('lang',app()->getLocale())->get();
        if (!empty($res)) {
            foreach ($res as $r) {
                $langs[$r->field] = $r->value;
            }
        }
        return $langs;
    }

    /**
     * Подготовка данных статьи для пуличной части
     * @param object $item
     * @return array
     */
    private function prepareBlogDetailForFront($item)
    {
        $categories = [];
        $arrTitles = [];
        if(!empty($item->categories)){
            foreach($item->categories as $category){
                if(!empty($category->category)){
                    $categories[] = $this->prepareCategoryDataForFront($category->category);
                }
            }
        }

        $metaTitle = $item->{'title' . getLocaleDBSuf()}.' - '.trans('custom.metas.blog.title');
        $metaDescription = $item->{'title' . getLocaleDBSuf()}.'. '.trans('custom.metas.blog.description');

        $lastComment = $this->getBlogLastCommentDate($item->id);
        $lastMod = Carbon::now();

        $data = [
            'id' => $item->id,
            'title' => $item->{'title' . getLocaleDBSuf()},
            'slug' => $item->slug,
            'url' => getLocaleHrefPrefix().'/blog/'.$item->slug,
            'picture' => $item->picture,
            'anons' => $item->{'anons' . getLocaleDBSuf()},
            'likes_count' => $item->likes_count,
            'like_is_setted' => $this->checkSettedLike($item->id),
            'views_count' => $item->views_count,
            'read_time' => $item->read_time,
            'published_at' => drawDate($item->published_at),
            'comments_count' => $this->getBlogCommentsCount($item->id),
            'categories' => $categories,
            'titles' => [],
            'lastmod' => $lastMod,
        ];

        $langs = $this->getBlogLangsForFront();
        $data = array_merge($data, $langs);
        $data['meta_title'] = $metaTitle;
        $data['meta_description'] = $metaDescription;

        if(!empty($data['detail'])){
            $res = $this->getTitleList($data['detail']);
            if(!empty($res)){
                $data['titles'] = $res['titles'];
                $data['detail'] = $res['detail'];
            }
        }

        $data['detail'] = addNofollowToLink($data['detail']);
        $data['detail'] = addAltToPics($data['detail'], $data['title']);
        $micro = [
           'url' => env('APP_URL').$data['url'],
           'title' => $data['title'],
           'pics' => getPicsForMicro($data),
           'created' => drawShortDate($item->published_at),
           'updated' => drawShortDate($lastMod),
           'description' => $data['meta_description']
        ];

        $recomends = [];
        if(!empty($item->recomends)){
            foreach($item->recomends as $recomend){
                if(!empty($recomend->target)){
                    if($recomend->target->is_active == true){
                        $recomends[] = $this->prepareBlogDataForFront($recomend->target);
                    }
                }
            }
        }
        return ['blog' => $data, 'recomends' => $recomends, 'titles' => $arrTitles, 'micro' => $micro];
    }

    /**
     * Получение кол-ва одобренных комментарев
     * @param int $blogId
     * @return int
     */
    private function getBlogCommentsCount($blogId)
    {
        $count = BlogComment::where('blog_id',$blogId)->isModerated()->count();
        return $count;
    }


    /**
     * Получение даты последнего комментария
     * @param int $blogId
     * @return int
     */
    private function getBlogLastCommentDate($blogId)
    {
        $res = BlogComment::where('blog_id',$blogId)->orderBy('updated_at','desc')->first();
        if($res){
            return $res->updated_at;
        }
        return '0000-00-00 00:00:00';
    }

    /**
     * Проверка уже установленного лайка
     * @param int $blogId
     * @return bool
     */
    private function checkSettedLike($blogId)
    {
        $existLike = BlogLike::where('blog_id', $blogId)
            ->where('session_id', session()->getId())
            ->first();
        if($existLike){
            return true;
        }
        return false;
    }

    /**
     * Парсинг текста для создания содержания
     * @param string $detail
     * @return array
     */
    private function getTitleList($detail)
    {
        if(empty($detail)){
            return [];
        }
        $arrTitles = [];
        $dom = new HtmlDomParser();
        $dom->load(mb_convert_encoding($detail,'HTML-ENTITIES','UTF-8'));
        $titles = $dom->findMulti('.title-ankor');
        if(!empty($titles)){
            $i = 0;
            foreach($titles as $title){
                $i++;
                $arrTitles[] = [
                    'title' => mb_convert_encoding($title->text,'UTF-8','HTML-ENTITIES'),
                    'ankor' => 'ankor-'.$i,
                ];
                $title->setAttribute('id','ankor-'.$i);
                $detail = $dom->save();
            }
        }
        return [
            'titles' => $arrTitles,
            'detail' => $detail,
        ];
    }



    /*if(strpos($txt,'<img') !== false){
        $dom = new DOMDocument();
        $dom->loadHTML(mb_convert_encoding($txt,'HTML-ENTITIES','UTF-8'));
        $pics = $dom->getElementsByTagName('img');
        if(!empty($pics)){
            $i = 0;
            foreach($pics as $pic){
                $i++;
                $printTitle = (count($pics) > 1) ? $title.','.$i : $title;
                $printAlt = (count($pics) > 1) ? $title.','.$i.' - '.trans('custom.helpBriz') : $title.' - '.trans('custom.helpBriz');
                $pic->setAttribute('alt', $printAlt);
                $pic->setAttribute('title', $printTitle);
            }
            $txt = $dom->saveHTML();
        }
    }
    return $txt;*/

    /**
     * Получение детальной информации статьи для публичной части
     * @param string $slug
     * @return array
     */
    public function getBlogDetailForFront($slug)
    {
        $blog = Blog::with('categories.category','recomends.target')->where('slug',$slug)->notDeleted()->isActive()->where('published_at','<=',Carbon::now())->first();
        if($blog){
            $this->setBlogId($blog->id);
            $blogData = $this->prepareBlogDetailForFront($blog);
            return $blogData;
        }
        return [];
    }

    /**
     * распарсивание параметров
     * @param string $params
     */
    private function extractParams($params = null)
    {
        if(!empty($params)){
            $array = explode(';',$params);
            if(!empty($array)){
                foreach($array as $arr){
                    $arrParam = explode('=',$arr);
                    if(!empty($arrParam[0]) && !empty($arrParam[1])){
                        switch ($arrParam[0]) {
                            case 'page':
                                $this->setCurrentPage($arrParam[1]);
                                break;
                            case 'sort':
                                $this->setCurrentSort($arrParam[1]);
                                break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Получение списка статей для публичной части
     * @param string $slug
     * @return array
     */
    public function getBlogsForFront($params = null, $slug = null)
    {
        $categoryFinded = $this->setCategoryId($slug);
        if($categoryFinded == false && $slug != null){
            return [];
        }
        $this->extractParams($params);
        $blogs = $this->getBlogsPortion();
        $sort = $this->prepareSortBtns();
        $pagination = $this->getPagination();
        if(!empty($slug)){
            $meta = $this->getCategoryMeta();
        } else {
            $meta = getPageMeta();
        }
        $meta['lastmod'] = Carbon::now();
        return ['sort' => $sort, 'blogs' => $blogs, 'pagination' => $pagination, 'meta' => $meta];
    }

    /**
     * Получение мета-данных категории
     * @return array
     */
    private function getCategoryMeta()
    {
        $meta = [];
        $category = BlogCategory::where('id',$this->getCategoryId())->first();
        if($category){
            $meta = [
                'title' => $category->{'title' . getLocaleDBSuf()},
                'meta-title' => $category->{'title' . getLocaleDBSuf()}.' - '.trans('custom.metas.blogCategory.title'),
                'meta-description' => $category->{'title' . getLocaleDBSuf()}.' '.trans('custom.metas.blogCategory.description'),
                'meta-keywords' => $category->{'meta_keywords' . getLocaleDBSuf()},
                'lastmod' => (!empty($category->updated_at)) ? $category->updated_at : false,
            ];
        }
        return $meta;
    }

    /**
     * Получение пагинации статей для публичной части
     * @return array
     */
    private function getPagination($request = [])
    {
        $pagination = [];
        $baseUrl = (!empty($this->getCategorySlug())) ? getLocaleHrefPrefix().'/blog/category/'.$this->getCategorySlug() : getLocaleHrefPrefix().'/blog';

        $query = Blog::isActive()->notDeleted()->where('published_at','<=',Carbon::now());
        $ids = $this->getCategoryBlogIds();
        if(!empty($ids)){
            $query->whereIn('id',$ids);
        }
        $countAll = $query->count();

        $page = $this->getCurrentPage();
        $page = ($page < 1) ? 1 : $page;

        $pages = ceil($countAll / $this::PAGINATION_CLIENT);
        $next = ($pages > 1) ? $page + 1 : 1;
        $back = ($pages > 1) ? $page - 1 : 1;

        $params = [];
        if($back > 1){
            $params[] = 'page='.$back;
        }
        if($this->getCurrentSort() != '' && $this->getCurrentSort() != $this::BASE_SORT){
            $params[] = 'sort='.$this->getCurrentSort();
        }
        $pagination['back'] = [
            'title' => trans('custom.blog.pagination.back'),
            'url' => $baseUrl.'/'.implode(';',$params),
            'disabled' => ($page == 1) ? true : false,
        ];

        $params = [];
        $params[] = 'page='.$next;
        if($this->getCurrentSort() != '' && $this->getCurrentSort() != $this::BASE_SORT){
            $params[] = 'sort='.$this->getCurrentSort();
        }
        $pagination['forward'] = [
            'title' => trans('custom.blog.pagination.forward'),
            'url' => $baseUrl.'/'.implode(';',$params),
            'disabled' => ($page == $pages) ? true : false,
        ];

        for($i = 1; $i <= $pages; $i++){
            $params = [];
            if($i > 1){
                $params[] = 'page='.$i;
            }
            if($this->getCurrentSort() != '' && $this->getCurrentSort() != $this::BASE_SORT){
                $params[] = 'sort='.$this->getCurrentSort();
            }
            $pagination['pages'][] = [
                'title' => $i,
                'url' => $baseUrl.'/'.implode(';',$params),
                'current' => ($i == $page) ? true : false,
            ];
        }
        return $pagination;
    }

    /**
     * Получание списка ID статей категории
     * @return array
     */
    private function getCategoryBlogIds()
    {
        $ids = [];
        $categoryId = $this->getCategoryId();
        if(!empty($categoryId)){
            $res = BlogCategoryIn::where('blog_category_id',$categoryId)->get();
            if(!empty($res)){
                foreach($res as $r){
                    $ids[] = $r->blog_id;
                }
            }
        }
        return $ids;
    }

    /**
     * Получение пагинированной порции статей
     * @return array
     */
    private function getBlogsPortion()
    {
        $blogs = [];
        $query = Blog::with('categories.category')->isActive()->notDeleted()->where('published_at','<=',Carbon::now());
        $ids = $this->getCategoryBlogIds();
        if(!empty($ids)){
            $query->whereIn('id',$ids);
        }

        $sort = $this->getCurrentSort();
        switch ($sort) {
            case 'new':
                $query->orderBy('published_at','desc');
                break;
            case 'popular':
                $query->orderBy('views_count','desc');
                break;
            default:
                $query->orderBy('published_at','desc');
                break;
        }

        $page = $this->getCurrentPage();
        $page = ($page < 1) ? 1 : $page;
        $offset = ($page - 1) * $this::PAGINATION_CLIENT;
        $res = $query->offset($offset)->limit($this::PAGINATION_CLIENT)->get();
        if(!empty($res)){
            foreach($res as $r){
                $blogs[] = $this->prepareBlogDataForFront($r);
            }
        }
        return $blogs;
    }

    /**
     * Подготовка данных статьи для публичной части
     * @param object $item
     * @return array
     */
    private function prepareBlogDataForFront($item)
    {
        $categories = [];
        if(!empty($item->categories)){
            foreach($item->categories as $category){
                if(!empty($category->category)){
                    $categories[] = $this->prepareCategoryDataForFront($category->category);
                }
            }
        }
        $blog = [
            'title' => $item->{'title' . getLocaleDBSuf()},
            'slug' => $item->slug,
            'url' => getLocaleHrefPrefix().'/blog/article/'.$item->slug,
            'picture' => (!empty($item->thumbnail)) ? $item->thumbnail : $item->picture,
            'anons' => $item->{'anons' . getLocaleDBSuf()},
            'likes_count' => $item->likes_count,
            'views_count' => $item->views_count,
            'read_time' => $item->read_time,
            'published_at' => drawDate($item->published_at),
            'categories' => $categories,
        ];
        return $blog;
    }

    /**
     * Подготовка данных кнопок сортировки для публичной части
     * @return array
     */
    private function prepareSortBtns()
    {
        $baseUrl = (!empty($this->getCategorySlug())) ? '/blog/category/'.$this->getCategorySlug() : '/blog';
        $items = [];
        $arr = $this::SORT;
        if(!empty($arr)){
            foreach($arr as $a){
                $url = ($this::BASE_SORT == $a) ? getLocaleHrefPrefix().$baseUrl : getLocaleHrefPrefix().$baseUrl.'/sort='.$a;
                $items[] = [
                    'title' => trans('custom.blog.sort.'.$a),
                    'code' => $a,
                    'selected' => ($a == $this->getCurrentSort()) ? true : false,
                    'url' => $url,
                ];
            }
        }
        return $items;
    }

    /**
     * Получение порции комментариев на публичную часть
     * @return array
     */
    public function getCommentsForFront($request)
    {
        if(empty($request['id'])){
            return [];
        }
        $allCount = BlogComment::where('blog_id',$request['id'])
            ->isModerated()
            ->where('parent_id',0)
            ->count();
        $pages = ceil($allCount / $this::PAGINATION_COMMENTS_CLIENT);
        $currentPage = (!empty($request['page'])) ? $request['page'] : 1;
        $currentPage = ($currentPage < 1) ? 1 : $currentPage;
        $hasMore = ($currentPage < $pages) ? true : false;

        $offset = ($currentPage - 1) * $this::PAGINATION_COMMENTS_CLIENT;
        $hasMoreCount = $allCount - ($offset + $this::PAGINATION_COMMENTS_CLIENT);
        $hasMoreCount = ($hasMoreCount < $this::PAGINATION_COMMENTS_CLIENT) ? $hasMoreCount : $this::PAGINATION_COMMENTS_CLIENT;

        $comments = [];
        $res = BlogComment::with('children')
            ->where('blog_id',$request['id'])
            ->isModerated()
            ->where('parent_id',0)
            ->offset($offset)
            ->limit($this::PAGINATION_COMMENTS_CLIENT)
            ->orderBy('created_at','asc')
            ->get();
        if(!empty($res)){
            foreach($res as $r){
                $comments[] = $this->prepareCommentForFront($r);
            }
        }

        return [
            'currentPage' => $currentPage,
            'hasMore' => $hasMore,
            'hasMoreCount' => $hasMoreCount,
            'comments' => $comments,
        ];
    }

    /**
     * Подготовка данных комментария для публичной части
     * @param object $item
     * @return array
     */
    private function prepareCommentForFront($item)
    {
        $children = [];
        if(!empty($item->children)){
            foreach($item->children as $child){
                $children[] = $this->prepareCommentForFront($child);
            }
        }
        $data = [
            'id' => $item->id,
            'picture' => mb_substr($item->author,0,1),
            'author' => $item->author,
            'comment' => $item->comment,
            'day' => drawDate($item->created_at),
            'lvl' => $item->lvl,
            'children' => $children,
        ];
        return $data;
    }
}
