<?php

namespace App\Services;

use App\Product;
use App\Category;
use App\CategoryProperty;
use App\Property;
use DB;

class FilterService
{
    protected $category_id, $take = 3;

    public function __construct($category_id)
    {
        $this->setCategoryId($category_id);
    }

    private function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }

    private function getCategoryId()
    {
        return $this->category_id;
    }


    public function getFilteredProducts($input)
    {
        $filters = [];
        foreach($input as $key => $val){
            $arr = explode('-', $key);
            if(isset($arr[0]) && isset($arr[1]) && $arr[1] > 0 && $arr[0] == 'filter'){
                $filters[$arr[1]] = $val;
            }
        }

        $query = DB::table('products as p')
            ->select('p.id','p.title_ru');

        if(!empty($filters)){
            foreach($filters as $property_id => $values){
                $table_alias = 'pp'.$property_id;
                $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
                    ->where($table_alias.'.property_id','=',$property_id)
                    ->whereIn($table_alias.'.value_id',$values);
            }
        }

        $res = $query->whereNull('p.deleted_at')
            ->where('p.active','=',1)
            ->where('p.category_id','=',$input['category_id'])
            ->groupBy('p.id','p.title_ru')
            ->get();

        $ids = [];
        if(!empty($res)){
            foreach($res as $r){
                $ids[] = $r->id;
            }
        }

        $page = ($input['page'] > 1) ? $input['page'] : 1;

        $skip = ($page - 1) * $this->take;

        $query = Product::visible()
            ->whereIn('id',$ids);
        switch ($input['sort']) {
            case 'new':
                $query->orderBy('id','desc');
                break;
            case 'price_asc':
                $query->orderBy('price','asc');
                break;
            case 'price_desc':
                $query->orderBy('price','desc');
                break;
            case 'title':
                $query->orderBy('title_ru','asc');
                break;
            default:
                $query->orderBy('id','desc');
                break;
        }
        $products = $query->offset($skip)
            ->limit($this->take)
            ->get();

        $count = Product::visible()
            ->whereIn('id',$ids)
            ->count();

        $taken = $skip + $this->take;
        if($count > $taken){
            $has_more = true;
        } else {
            $has_more = false;
        }

        $res = $this->getFilterCounters($filters);
        $counter_new = $res['new'];
        $counter_self = $res['self'];

        $page++;

        return ['products' => $products, 'has_more' => $has_more, 'page' => $page, 'counter_new' => $counter_new, 'counter_self' => $counter_self];
    }

    private function countForSingle($prop_id, $value_id, $filters)
    {
        $count = [];
        $self = false;

        $query = DB::table('products as p')
            ->select('p.id');

        if(!empty($filters)){
            //фильтр установлен
            foreach($filters as $fil_property_id => $fil_values){
                if($fil_property_id != $prop_id){
                    $table_alias = 'pp'.$fil_property_id;
                    $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
                        ->where($table_alias.'.property_id','=',$fil_property_id)
                        ->whereIn($table_alias.'.value_id',$fil_values);
                } else {
                    if(!in_array($value_id,$fil_values)){
                        $self = true;
                    }
                }

            }
        }
        $table_alias = 'pp0';
        $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
            ->where($table_alias.'.property_id','=',$prop_id)
            ->where($table_alias.'.value_id',$value_id);

        $count = $query->whereNull('p.deleted_at')
            ->where('p.active','=',1)
            ->where('p.category_id','=',$this->getCategoryId())
            ->get();

        return ['count' => count($count), 'self' => $self];
    }

    private function countForMultiple($prop_id, $value_id, $filters)
    {
        $count = [];
        $self = false;

        $query = DB::table('products as p')
            ->select('p.id');

        $get_minus = false;

        if(!empty($filters)){
            //фильтр установлен
            foreach($filters as $fil_property_id => $fil_values){

                if($fil_property_id != $prop_id){

                    $table_alias = 'pp'.$fil_property_id;
                    $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
                        ->where($table_alias.'.property_id','=',$fil_property_id)
                        ->whereIn($table_alias.'.value_id',$fil_values);

                } else {


                    if(!in_array($value_id,$fil_values)){
                        $self = true;
                        $get_minus = true;
                    }

                }

            }
        }


        $table_alias = 'pp0';
        $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
            ->where($table_alias.'.property_id','=',$prop_id)
            ->where($table_alias.'.value_id',$value_id);

        $count = $query->whereNull('p.deleted_at')
            ->where('p.active','=',1)
            ->where('p.category_id','=',$this->getCategoryId())
            ->groupBy('p.id')
            ->get();
        $count_prods = count($count);

        /* ------------------------------------ */

        if($get_minus == true){
            $query = DB::table('products as p')
                ->select('p.id');
            foreach($filters as $fil_property_id => $fil_values){
                if($fil_property_id != $prop_id){
                    $table_alias = 'pp'.$fil_property_id;
                    $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
                        ->where($table_alias.'.property_id','=',$fil_property_id)
                        ->whereIn($table_alias.'.value_id',$fil_values);
                } else {
                    $table_alias = 'pp'.$fil_property_id;
                    $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
                        ->where($table_alias.'.property_id','=',$fil_property_id)
                        ->whereIn($table_alias.'.value_id',$fil_values);
                }
            }
            $count = $query->whereNull('p.deleted_at')
                ->where('p.active','=',1)
                ->where('p.category_id','=',$this->getCategoryId())
                ->groupBy('p.id')
                ->get();
            $count_minus = count($count);
        }

        if(isset($count_minus) && $count_prods > $count_minus){
            $count_prods -= $count_minus;
        } else {
            $self = false;
        }

        return ['count' => $count_prods, 'self' => $self];
    }

    private function getFilterCounters($filters)
    {
        $result = [
            'new' => [],
            'self' => [],
        ];
        $props = $this->getFilters();


        if(!empty($props)){
            foreach($props as $prop_id => $property){
                if(!empty($property->values)){
                    foreach($property->values as $key => $value){

                        if($property->multiple == true){

                            $c = $this->countForMultiple($prop_id, $value->id, $filters);
                            if($c['self'] == true){
                                $result['self'][$value->id] = $c['count'];
                            } else {
                                $result['new'][$value->id] = $c['count'];
                            }

                        } else {

                            $c = $this->countForSingle($prop_id, $value->id, $filters);
                            if($c['self'] == true){
                                $result['self'][$value->id] = $c['count'];
                            } else {
                                $result['new'][$value->id] = $c['count'];
                            }

                        }

                    }
                }
            }
        }









        /*
        if(!empty($props)){
            foreach($props as $prop_id => $property){
                if(!empty($property->values)){
                    foreach($property->values as $key => $value){
                        $new = false;
                        if(!empty($filters)){
                            //фильтр установлен

                            $query = DB::table('products as p')
                                ->select('p.id');

                            $exist = false;
                            foreach($filters as $fil_property_id => $fil_values){


                                    if($fil_property_id == $prop_id){


                                        if($property->multiple){


                                        } else {

                                            if(!in_array($value->id,$fil_values)){
                                                $new = true;
                                            } else {
                                                $new = false;
                                            }

                                            foreach($fil_values as $k => $v){
                                                if($v == $value->id){
                                                    unset($fil_values[$k]);
                                                }
                                            }

                                            $exist = true;

                                            $table_alias = 'pp'.$fil_property_id;
                                            $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
                                                ->where($table_alias.'.property_id','=',$fil_property_id)
                                                ->whereNotIn($table_alias.'.value_id',$fil_values);


                                        }



                                    } else {

                                        $table_alias = 'pp'.$fil_property_id;
                                        $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
                                            ->where($table_alias.'.property_id','=',$fil_property_id)
                                            ->whereIn($table_alias.'.value_id',$fil_values);

                                    }

                            }

                            $table_alias = 'pp0';
                            $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
                                ->where($table_alias.'.property_id','=',$prop_id)
                                ->where($table_alias.'.value_id',$value->id);

                            if(!$exist){

                            }

                            $count = $query->whereNull('p.deleted_at')
                                ->where('p.active','=',1)
                                ->where('p.category_id','=',$this->getCategoryId())
                                //->groupBy('p.id')
                                ->get();


                        } else {

                            $query = DB::table('products as p')
                                ->select('p.id');
                            $table_alias = 'pp'.$prop_id;
                            $query->join('product_properties as '.$table_alias, $table_alias.'.product_id','=','p.id')
                                        ->where($table_alias.'.property_id','=',$prop_id)
                                        ->where($table_alias.'.value_id',$value->id);
                            $count = $query->whereNull('p.deleted_at')
                                ->where('p.active','=',1)
                                ->where('p.category_id','=',$this->getCategoryId())
                                ->get();

                        }

                        if($new){
                            $result['new'][$value->id] = count($count);
                        } else {
                            $result['checked'][$value->id] = count($count);
                        }

                    }
                }
            }
        }
*/
/*
        print_r($filters);
        print_r($result);
        die;
*/
        return $result;
    }


    public function getFilters()
    {
        $props = [];
        $res = CategoryProperty::with('property')
            ->where('category_id',$this->getCategoryId())
            ->where('use_in_filter',1)
            ->orderBy('sort','asc')
            ->get();
        if(!empty($res)){
            foreach($res as $r){
                if(!empty($r->property)){
  /*
                    $r->property->values = DB::table('product_properties as pp')
                        ->select('pv.id','pv.value_ru','pv.value_ua','pv.value_en','pv.slug',DB::raw('count(pv.id) as count'))
                        ->join('products as p','p.id','=','pp.product_id')
                        ->join('property_values as pv','pv.id','=','pp.value_id')
                        ->where('p.category_id','=',$this->getCategoryId())
                        ->where('pv.property_id','=',$r->property->id)
                        ->whereNull('p.deleted_at')
                        ->where('p.active','=',1)
                        ->groupBy('pv.id','pv.value_ru','pv.value_ua','pv.value_en','pv.slug')
                        ->orderBy('pv.sort','asc')
                        ->get();
*/
                    $r->property->values = DB::table('product_properties as pp')
                        ->select('pv.id','pv.value_ru','pv.value_ua','pv.value_en','pv.slug')
                        ->join('property_values as pv','pv.id','=','pp.value_id')
                        ->where('pv.property_id','=',$r->property->id)
                        ->groupBy('pv.id','pv.value_ru','pv.value_ua','pv.value_en','pv.slug')
                        ->orderBy('pv.sort','asc')
                        ->get();
                    $props[$r->property->id] = $r->property;
                }
            }
        }
        return $props;
    }

    private function productExist($product_id)
    {
        $row = Cart::active()->noOrder()
            ->where('product_id',$product_id)
            ->where('session_id',$this->getSessionId())
            ->first();
        if($row){
            return $row;
        } else {
            return false;
        }
    }

    public function getCartCount()
    {
        return Cart::active()->noOrder()
            ->where('session_id',$this->getSessionId())
            ->sum('qnt');
    }

    public function getCartSumm()
    {
        return Cart::active()->noOrder()
            ->where('session_id',$this->getSessionId())
            ->sum('total');
    }

    public function addOrUpdateProduct($product_id)
    {
        $product = Product::visible()->find($product_id);
        if($product){
            $cart_item = $this->productExist($product_id);
            if($cart_item !== false){
                $cart_item->qnt += 1;
                $cart_item->total = $cart_item->price * $cart_item->qnt;
                $cart_item->save();
            } else {
                $data = [
                    'session_id' => $this->getSessionId(),
                    'user_id' => $this->getUserId(),
                    'product_id' => $product->id,
                    'price' => $product->price,
                    'qnt' => 1,
                    'total' => $product->price,
                    'day' => date('Y-m-d'),
                ];
                $cart_item = Cart::create($data);
            }
            return $cart_item->product_id;

        } else {

            return false;

        }
    }

    public function updateCartItem($id, $qnt)
    {
        if($id > 0 && $qnt > 0){
            $cart = Cart::active()->noOrder()->where('id',$id)->where('session_id',$this->getSessionId())->first();
            if($cart){
                $cart->qnt = $qnt;
                $cart->total = $cart->price * $cart->qnt;
                $cart->save();
                return $cart->total;
            }
        }
        return false;
    }

    public function getCartItems()
    {
        $summ = 0;
        $result = Cart::with('product.category')->active()->noOrder()
            ->where('session_id',$this->getSessionId())
            ->get();
        if(!empty($result)){
            foreach($result as $r){
                $summ += $r->total;
            }
        }

        return ['items' => $result, 'summ' => $summ];
    }

    public function deleteCartItem($id)
    {
        $row = Cart::where('id',$id)->active()->noOrder()
            ->where('session_id',$this->getSessionId())
            ->first();
        if($row){
            $row->delete();
        }
        return true;
    }

    public function syncCartWithOrder($order_id)
    {
        $result = Cart::with('product.category')->active()->noOrder()
            ->where('session_id',$this->getSessionId())
            ->get();
        if(!empty($result)){
            foreach($result as $cart_item){
                $cart_item->order_id = $order_id;
                $cart_item->save();
            }
        }
        return true;
    }


}
