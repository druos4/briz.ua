<?php

namespace App\Services;

use App\Address;
use App\Core\Lib\Billing\BillingApi;
use App\MapMarker;
use App\Street;
use App\DomType;
use Carbon\Carbon;
use DB;

class MapService
{
    private $ids_parsed = [];

    public function __construct()
    {

    }

    private function addToParsed($id)
    {
        $this->ids_parsed[] = $id;
    }

    private function getParsedIds()
    {
        return $this->ids_parsed;
    }

    private function prepareDataForFront($item)
    {
         return [
            //'id' => $item->id,
            'lat' => $item->lat,
            'lon' => $item->lon,
            'street' => $item->{'street' . getLocaleDBSuf()},
            'dom' => $item->address_num,
            //'has_inet' => ($item->has_inet == 1) ? true : false,
            //'has_ctv' => ($item->has_ctv == 1) ? true : false,
        ];
    }

    public function getMarkersForFront()
    {
        $items = [];
        $res = MapMarker::where('lat','!=','')->where('lon','!=','')->where('street_id','!=',147)->where('addr_id','!=',2395)->get();
        if(!empty($res)){
            foreach($res as $r){
                $items[] = $this->prepareDataForFront($r);
            }
        }
        return $items;
    }

    private function prepareDomForFront($marker)
    {
        return [
            'id' => $marker->id,
            'dom' => $marker->address_num,
        ];
    }

    private function prepareStreetForFront($street)
    {
        return [
            'id' => $street->street_id,
            'street' => $street->{'street' . getLocaleDBSuf()},
        ];
    }

    public function searchStreet($request)
    {
        $items = [];
        $res = MapMarker::select('street_id','street_ru','street_ua')
            ->where('street_ru','LIKE','%'.strip_tags($request->get('street')).'%')
            ->orWhere('street_ua','LIKE','%'.strip_tags($request->get('street')).'%')
            ->groupBy('street_id')
            ->orderBy('street_ru','asc')
            ->get();
        if(!empty($res)){
            foreach($res as $r){
                $items[] = $this->prepareStreetForFront($r);
            }
        }
        return $items;
    }

    public function getAllStreet()
    {
        $items = [];
        $res = MapMarker::select('street_id','street_ru','street_ua','street_en')
            ->where('street_ru','!=','')
            ->where('street_ua','!=','')
            ->where('lat','!=','')
            ->where('lon','!=','')
            ->where('street_id','!=',147)
            ->where('addr_id','!=',2395)
            ->groupBy('street_id')
            ->orderBy('street_ru','asc')
            ->get();
        if(!empty($res)){
            foreach($res as $r){
                $items[] = $this->prepareStreetForFront($r);
            }
        }
        return $items;
    }

    private function getStreetForDom($request)
    {
        $res = MapMarker::select('street_id','street_ru','street_ua')
            ->where('street_ru','LIKE','%'.strip_tags($request->get('street')).'%')
            ->orWhere('street_ua','LIKE','%'.strip_tags($request->get('street')).'%')
            ->groupBy('street_id')
            ->orderBy('street_ru','asc')
            ->first();
        if(!empty($res->street_id)){
            return $res->street_id;
        }
        return false;
    }

    public function searchDom($request)
    {
        if($request->get('street_id') > 0){
            $street_id = $request->get('street_id');
        } else {
            $street_id = $this->getStreetForDom($request);
        }
        if($street_id !== false){
            $marker = MapMarker::where('street_id',$street_id)
                ->where('address_num',$request->get('dom'))
                ->first();
            if(!empty($marker)){
                return $this->prepareDomForFront($marker);
            }
        }
        return false;
    }

    public function searchMarker($request)
    {
        if($request->get('dom_id') > 0){
            $marker = MapMarker::where('id',$request->get('dom_id'))
                ->where('lat','!=','')
                ->where('lon','!=','')
                ->first();
        } else {
            if($request->get('street_id') > 0){
                $street_id = $request->get('street_id');
            } else {
                $street_id = $this->getStreetForDom($request);
            }
            if($street_id !== false){
                $marker = MapMarker::where('street_id',$street_id)
                    ->where('lat','!=','')
                    ->where('lon','!=','')
                    ->where('address_num',$request->get('dom'))
                    ->first();
                if(empty($marker->id)){
                    $marker = MapMarker::where('street_id',$street_id)
                        ->where('address_num','*')
                        ->where('lat','!=','')
                        ->where('lon','!=','')
                        ->first();
                }
            }
        }
        if(!empty($marker)){
            return $this->prepareDataForFront($marker);
        }
        return false;
    }

    public function importMarkersFromBilling()
    {
        $count = [
            'inet' => 0,
            'ctv' => 0,
        ];
        $result = $this->getFromBilling();
        if(!empty($result['inet'])){
            $count['inet'] = $this->parseInet($result['inet']);
        }
        /*if(!empty($result['ctv'])){
            $count['ctv'] = $this->parseCtv($result['ctv']);
        }*/

        $this->clearOldMarkers();
        return $count;
    }

    private function clearOldMarkers()
    {
        $ids = $this->getParsedIds();
        if(count($ids) > 0){
            MapMarker::whereNotIn('id',$ids)->delete();
        }
    }

    private function createMapInet($input)
    {
        $data = [
            'address_name' => $input['street_ru'],
            'address_num' => $input['dom'],
            'lat' => $input['geo_lat'],
            'lon' => $input['geo_lon'],
            'has_inet' => 1,
            'has_iptv' => 1,
            //'has_giga' => $input[''],
            'addr_id' => $input['id'],
            'street_id' => $input['street_id'],
            'street_ru' => $input['street_ru'],
            'street_ua' => $input['street_ua'],
            'street_en' => (!empty($input['street_en'])) ? $input['street_en'] : '',
            'addr_sort' => intval($input['dom']),
        ];
        $marker = MapMarker::create($data);
        $this->addToParsed($marker->id);
    }

    private function updateMapInet($input, $marker)
    {
        $data = [
            'address_name' => $input['street_ru'],
            'address_num' => $input['dom'],
            'lat' => $input['geo_lat'],
            'lon' => $input['geo_lon'],
            'has_inet' => 1,
            'has_iptv' => 1,
            //'has_giga' => $input[''],
            'street_id' => $input['street_id'],
            'street_ru' => $input['street_ru'],
            'street_ua' => $input['street_ua'],
            'street_en' => (!empty($input['street_en'])) ? $input['street_en'] : '',
            'updated_at' => Carbon::now(),
            'addr_sort' => intval($input['dom']),
        ];

        $marker->update($data);
        $this->addToParsed($marker->id);
    }

    private function parseInet($arr)
    {
        foreach($arr as $a){
            if($a['street_id'] == 147){
                continue;
            }
            $marker = MapMarker::where('addr_id',$a['id'])->first();
            if(!empty($marker->id)){
                $this->updateMapInet($a, $marker);
            } else {
                $this->createMapInet($a);
            }
        }
    }

    private function createMapCtv($input)
    {
        $data = [
            'address_name' => $input['street_ru'],
            'address_num' => $input['dom'],
            'lat' => $input['geo_lat'],
            'lon' => $input['geo_lon'],
            'has_ctv' => 1,
            'addrtv_id' => $input['id'],
            'street_id' => $input['street_id'],
            'street_ru' => $input['street_ru'],
            'street_ua' => $input['street_ua'],
            'addr_sort' => intval($input['dom']),
        ];
        MapMarker::create($data);
    }

    private function updateMapCtv($input, $marker)
    {
        $data = [
            'address_name' => $input['street_ru'],
            'address_num' => $input['dom'],
            'lat' => $input['geo_lat'],
            'lon' => $input['geo_lon'],
            'has_ctv' => 1,
            'street_id' => $input['street_id'],
            'street_ru' => $input['street_ru'],
            'street_ua' => $input['street_ua'],
            'addrtv_id' => $input['id'],
            'addr_sort' => intval($input['dom']),
        ];
        $marker->update($data);
    }

    private function parseCtv($arr)
    {
        foreach($arr as $a){
            $marker = MapMarker::where('addrtv_id',$a['id'])->first();
            if(!empty($marker->id)){
                $this->updateMapCtv($a, $marker);
            } else {
                $marker = MapMarker::where('street_id',$a['street_id'])->where('address_num',$a['dom'])->first();
                if(!empty($marker->id)){
                    $this->updateMapCtv($a, $marker);
                } else {
                    $this->createMapCtv($a);
                }
            }
        }
    }

    private function getFromBilling()
    {
        try {
            $billingAPI = BillingApi::get();
            $response = $billingAPI->execute('getAllMarkers', []);
        } catch (ApiException $e) {
            return false;
        }
        return $response;
    }


}
