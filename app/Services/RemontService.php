<?php

namespace App\Services;

use App\Core\Lib\Billing\BillingApi;
use App\Feedback;
use App\Product;
use App\Service;
use App\Tariff;
use Carbon\Carbon;

class RemontService
{
    const API_URL = 'getSiteRepairs';
    const CACHE_TIME = 30;

    public function __construct()
    {

    }

    private function prepareDataForFront($arr)
    {
        $data = '';
        if(!empty($arr)){
            foreach($arr as $a){
                if(!empty($a["localization"][app()->getLocale()])){
                    //$data .= '<div class="repairs-news">'.$a["localization"][app()->getLocale()].'</div>';
                    $data .= $a["localization"][app()->getLocale()];
                }
            }
        }
        $url = getLocaleHrefPrefix().'/news/recomend';
        $news_data = [
            'id' => 0,
            'title' => trans('custom.remont.title'),
            'slug' => 'remont',
            'url' => $url,
            'anons' => '',
            'picture' => '/images/remont.png',
            'detail' => $data,
            'meta_title' => trans('custom.remont.title'),
            'meta_description' => '',
            'meta_keywords' => '',
            'recomends' => [],
            'thumbnail' => '/images/remont.png',
            'thumbnail_first' => '/images/remont.png',
            'published' => date('d.m.Y'),
            'picture_tablet' => '/images/remont.png',
            'picture_phone' => '/images/remont.png',
        ];
        return $news_data;
    }

    public function getRemonts($request)
    {
        $key = app()->getLocale().'-get-remonts';
        $result = \Cache::remember($key, self::CACHE_TIME, function () {
            $res = $this->getFromBilling();
            $result = $this->prepareDataForFront($res);
            return $result;
        });
        return $result;
    }


    public function checkForRemont()
    {
        $key = app()->getLocale().'-check-for-remont';
        $result = \Cache::remember($key, self::CACHE_TIME, function () {
            $result = $this->getFromBilling();
            if(!empty($result) && count($result) > 0){
                return true;
            } else {
                return false;
            }
        });
        return $result;
    }

    private function getFromBilling()
    {
        $response = [];
        $data = [
            'locale' => app()->getLocale(),
        ];
        try {
            $billingAPI = BillingApi::get();
            $response = $billingAPI->execute(self::API_URL,$data);
        } catch (ApiException $e) {
        }
        return $response;
    }



}
