<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Str;
use DB;

class UkrPoshtaService
{
    const API_AUTH_ENDPOINT_URL = 'https://ukrposhta.ua/address-classifier/get_regions_by_region_ua?region_name=&region_name_en=';
    const BEARER_TOKEN = '8200728d-9719-3a59-b2f8-b278adf5f3b7';
    const API_REGIONS_ENDPOINT_URL = 'https://ukrposhta.ua/address-classifier/get_districts_by_region_id_and_district_ua?region_id=';
    const API_CITIES_ENDPOINT_URL = 'https://ukrposhta.ua/address-classifier/get_city_by_region_id_and_district_id_and_city_ua?district_id=';
    const API_STRRETS_ENDPOINT_URL = 'https://ukrposhta.ua/address-classifier/get_street_by_region_id_and_district_id_and_city_id_and_street_ua?';
    const API_FIND_STRRETS_ENDPOINT_URL = 'https://ukrposhta.ua/address-classifier/get_street_by_name?';
    const API_BRANCHES_ENDPOINT_URL = 'https://ukrposhta.ua/address-classifier/get_postoffices_by_city_id?city_id=';

    public function __construct()
    {

    }



    public function getRegions()
    {
        $data = [];
        $c = 0;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_AUTH_ENDPOINT_URL);
        $headers = ['Authorization' => 'Bearer ' . self::BEARER_TOKEN];
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($output, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        foreach ($array['Entry'] as $key => $arr){
            $check = DB::table('map_regions')->select()->where('ref','=',$arr['REGION_ID'])->first();
            if(isset($check->id) && $check->id > 0){
                //ok - pass
            } else {
                $dataIn = ['title_ru' => $arr['REGION_RU'],
                    'title_ua' => $arr['REGION_UA'],
                    'title_en' => $arr['REGION_EN'],
                    'ref' => $arr['REGION_ID'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()];
                DB::table('map_regions')->insert($dataIn);
            }
            $c++;
        }
        return $c;
    }

    public function getDistricts()
    {
        $regions = DB::table('map_regions')->select()->get();
        if(!empty($regions)){
            foreach($regions as $region){

                $this->getDistrict($region);

            }
        }
    }

    private function getDistrict($region)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_REGIONS_ENDPOINT_URL . $region->ref.'&district_ua=');
        $headers = ['Authorization' => 'Bearer ' . self::BEARER_TOKEN];
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($output, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
        foreach ($array['Entry'] as $key => $arr){
            if(isset($arr['DISTRICT_ID']) && $arr['DISTRICT_ID'] > 0){
                $check = DB::table('map_districts')->select()->where('ref','=',$arr['DISTRICT_ID'])->first();
                if(isset($check->id) && $check->id > 0){
                    //ok - pass
                } else {
                    $dataIn = ['title_ru' => $arr['DISTRICT_RU'],
                        'title_ua' => $arr['DISTRICT_UA'],
                        'title_en' => $arr['DISTRICT_EN'],
                        'region_id' => $region->id,
                        'ref' => $arr['DISTRICT_ID'],
                        'ref_parent' => $arr['REGION_ID'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),];
                    DB::table('map_districts')->insert($dataIn);
                }
            }
        }
        return true;
    }

    public function getCities()
    {
        $districts = DB::table('map_districts as md')
            ->select('md.*','mr.ref as region_ref')
            ->leftJoin('map_regions as mr','mr.id','=','md.region_id')
            ->whereNotNull('md.ref')->get();
        if(!empty($districts)){
            foreach($districts as $district){

                $this->getCity($district);

            }
        }


    }

    private function getCity($district)
    {
        $endpoint = self::API_CITIES_ENDPOINT_URL . $district->ref.'&region_id='.$district->region_ref.'&city_ua=';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        $headers = ['Authorization' => 'Bearer ' . self::BEARER_TOKEN ];
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($output, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);

        if(isset($array['Entry']['CITY_ID']) && $array['Entry']['CITY_ID'] > 0){
            $check = DB::table('map_cities')->select()->where('ref','=',$array['Entry']['CITY_ID'])->first();
            if(isset($check->id) && $check->id > 0){
                //ok - pass
            } else {
                $dataIn = ['title_ru' => $array['Entry']['CITY_RU'],
                    'title_ua' => $array['Entry']['CITY_UA'],
                    'title_en' => $array['Entry']['CITY_EN'],
                    'ref' => $array['Entry']['CITY_ID'],
                    'region_ref' => $array['Entry']['REGION_ID'],
                    'district_ref' => $array['Entry']['DISTRICT_ID'],
                    'region_id' => $district->region_id,
                    'district_id' => $district->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()];
                DB::table('map_cities')->insert($dataIn);
            }
        } elseif(isset($array['Entry']) && is_array($array['Entry']) && !empty($array['Entry'])) {

            foreach ($array['Entry'] as $key => $arr){
                $check = DB::table('map_cities')->select()->where('ref','=',$arr['CITY_ID'])->first();
                if(isset($check->id) && $check->id > 0){
                    //ok - pass
                } else {
                    $dataIn = ['title_ru' => $arr['CITY_RU'],
                        'title_ua' => $arr['CITY_UA'],
                        'title_en' => $arr['CITY_EN'],
                        'ref' => $arr['CITY_ID'],
                        'region_ref' => $arr['REGION_ID'],
                        'district_ref' => $arr['DISTRICT_ID'],
                        'region_id' => $district->region_id,
                        'district_id' => $district->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()];
                    DB::table('map_cities')->insert($dataIn);
                }
            }

        }
        return true;
    }

    private function findStreet($name)
    {
        $street = DB::table('map_streets')
            ->select()
            ->where('name_ua','LIKE','')
            ->first();
    }

    public function getStreet($city)
    {
        $district = DB::table('map_districts')->select()->where('id','=',$city->district_id)->first();

        //$endpoint = self::API_STRRETS_ENDPOINT_URL . 'region_id='.$district->ref_parent.'&district_id='.$district->ref.'&city_id='.$city->ref.'&street_ua=';
        $endpoint = self::API_FIND_STRRETS_ENDPOINT_URL . 'city_id='.$city->ref.'&street_name=сахарова&lang=UA&fuzzy=usefuzzyserch';
echo $endpoint;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        $headers = ['Authorization' => 'Bearer ' . self::BEARER_TOKEN ];
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($output, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);

        print_r($array);
        die;

        $i = 0;
        if(isset($array['Entry']) && is_array($array['Entry']) && !empty($array['Entry'])) {
            foreach ($array['Entry'] as $key => $arr) {
                $i++;
/*
                if($i < 97){
                    continue;
                } else {
                    print_r($arr);
                    $data = [
                        'street_ref' => $arr['STREET_ID'],
                        'city_id' => $city->id,
                        'city_ref' => $arr['CITY_ID'],
                        'name_ru' => '',
                        'name_ua' => $arr['STREET_UA'],
                        'name_en' => (is_array($arr['STREET_EN'])) ? '' : $arr['STREET_EN'],
                        'type_ru' => $arr['STREETTYPE_RU'],
                        'type_ua' => $arr['STREETTYPE_UA'],
                        'type_en' => $arr['STREETTYPE_EN'],
                        'type_short' => $arr['SHORTSTREETTYPE_UA'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                    print_r($data);

                }
*/
                $check = DB::table('map_streets_new')->select()->where('street_ref','=',$arr['STREET_ID'])->first();
                if(!$check){
                    $data = [
                        'street_ref' => $arr['STREET_ID'],
                        'city_id' => $city->id,
                        'city_ref' => $arr['CITY_ID'],
                        'name_ru' => '',
                        'name_ua' => $arr['STREET_UA'],
                        'name_en' => (is_array($arr['STREET_EN'])) ? '' : $arr['STREET_EN'],
                        'type_ru' => $arr['STREETTYPE_RU'],
                        'type_ua' => $arr['STREETTYPE_UA'],
                        'type_en' => $arr['STREETTYPE_EN'],
                        'type_short' => $arr['SHORTSTREETTYPE_UA'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                    DB::table('map_streets_new')->insert($data);
                }
                echo $i.' ';
            }
        }

        echo $endpoint;
        die;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        $headers = ['Authorization' => 'Bearer ' . self::BEARER_TOKEN ];
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($output, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);

        if(isset($array['Entry']['CITY_ID']) && $array['Entry']['CITY_ID'] > 0){
            $check = DB::table('map_cities')->select()->where('ref','=',$array['Entry']['CITY_ID'])->first();
            if(isset($check->id) && $check->id > 0){
                //ok - pass
            } else {
                $dataIn = ['title_ru' => $array['Entry']['CITY_RU'],
                    'title_ua' => $array['Entry']['CITY_UA'],
                    'title_en' => $array['Entry']['CITY_EN'],
                    'ref' => $array['Entry']['CITY_ID'],
                    'region_ref' => $array['Entry']['REGION_ID'],
                    'district_ref' => $array['Entry']['DISTRICT_ID'],
                    'region_id' => $district->region_id,
                    'district_id' => $district->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()];
                DB::table('map_cities')->insert($dataIn);
            }
        } elseif(isset($array['Entry']) && is_array($array['Entry']) && !empty($array['Entry'])) {

            foreach ($array['Entry'] as $key => $arr){
                $check = DB::table('map_cities')->select()->where('ref','=',$arr['CITY_ID'])->first();
                if(isset($check->id) && $check->id > 0){
                    //ok - pass
                } else {
                    $dataIn = ['title_ru' => $arr['CITY_RU'],
                        'title_ua' => $arr['CITY_UA'],
                        'title_en' => $arr['CITY_EN'],
                        'ref' => $arr['CITY_ID'],
                        'region_ref' => $arr['REGION_ID'],
                        'district_ref' => $arr['DISTRICT_ID'],
                        'region_id' => $district->region_id,
                        'district_id' => $district->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()];
                    DB::table('map_cities')->insert($dataIn);
                }
            }

        }
        return true;
    }


}
