<?php

namespace App\Services;

use App\Blog;
use App\BlogCategory;
use App\Category;
use App\Faq;
use App\FaqGroup;
use App\HelpDirectory;
use App\News;
use App\Product;
use App\Service;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use DB;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\SitemapIndex;
use Spatie\Sitemap\Tags\Url;
use App\Page;


class SiteMapService
{
    private $sitemap;
    private $lang;
    private $htmlsitemap = [];
    private $langs = [
        'ua' => '',
        'ru' => '/ru',
        'en' => '/en',
    ];
    private $tvs = [
        '/spisok-kanalov-paket-rasshirennyiy-iptv',
        '/spisok-kanalov-paket-bazovyiy-iptv',
        '/spisok-kanalov-paket-premium',
        '/spisok-kanalov-paket-bazovyiy',
    ];

    private $urls = [
        '/domashniy-internet',
        '/televidenie',
        '/televidenie-i-internet',
        '/internet-dlya-biznesa',
        '/map',
        '/news',
        '/actions',
        '/payment',
        '/contacts',
        '/faq',
        '/help',
        '/documents',
        '/spisok-kanalov-paket-rasshirennyiy-iptv',
        '/spisok-kanalov-paket-bazovyiy-iptv',
        '/spisok-kanalov-paket-premium',
        '/spisok-kanalov-paket-bazovyiy',
        '/equipment',
        '/equipment/top',
        '/usloviya-i-pravila',
        '/blog',
    ];

    private function addLangPrefix()
    {
        $langs = $this->langs;
        return $langs[$this->lang];
    }

    public function __construct($lang = 'ua')
    {
        $this->lang = $lang;

        $this->sitemap = Sitemap::create()->add($this->addLangPrefix() . '/');
    }

    private function addToHtmlSitemap($entity)
    {
        $this->htmlsitemap[] = $entity;
    }

    private function getHtmlSitemap()
    {
        return $this->htmlsitemap;
    }

    private function getStaticUrls()
    {
        return $this->urls;
    }

    public function getPublicSiteMap()
    {
        $this->addStaticUrlsToHtmlSitemap();
        return $this->getHtmlSitemap();
    }

    public function generateMap()
    {
        $sitemapPath = public_path('sitemap-' . $this->lang . '.xml');
        $this->addStaticUrlsToMap();
        $this->addNewsToMap();
        $this->addActionsToMap();
        $this->addFaqsToMap();
        $this->addHelpsToMap();
        $this->addShopToMap();
        $this->addBlogToMap();
        $this->sitemap->writeToFile($sitemapPath);
    }

    private function getPageTitleByUrl($url)
    {
        $title = '';
        $page = Page::select('id', 'slug', 'title_ru', 'title_ua', 'title_en')->where('slug', $url)->first();
        if (!empty($page)) {
            $title = $page->{'title' . getLocaleDBSuf()};
        }
        return $title;
    }

    private function getTariffsInfo($url)
    {
        $code = str_replace('/', '', $url);
        $code = str_replace('domashniy-internet', 'internet-home', $code);
        $page = Service::where('code', $code)->first();
        if ($page) {
            return $page->{'title' . getLocaleDBSuf()};
        }
        return false;
    }

    private function getNewsLinks($type)
    {
        $arr = [];
        $query = News::select('id', 'entity_type', 'slug', 'title_ru', 'title_ua', 'title_en')->active()->published();
        if ($type == 'news') {
            $query->isNews();
        } elseif ($type == 'actions') {
            $query->isActions();
        }
        $items = $query->get();
        if (!empty($items)) {
            foreach ($items as $item) {
                $arr[] = [
                    'url' => $this->addLangPrefix() . '/' . $item->entity_type . '/' . $item->slug,
                    'title' => $item->{'title' . getLocaleDBSuf()},
                ];
            }
        }
        return $arr;
    }

    private function getTVLinks()
    {
        $arr = [];
        if (!empty($this->tvs)) {
            foreach ($this->tvs as $tv) {
                $arr[] = [
                    'url' => $this->addLangPrefix() . $tv,
                    'title' => $this->getPageTitleByUrl($tv),
                ];
            }
        }
        return $arr;
    }

    private function getEquipmentLinks()
    {
        $arr = [];
        $arr[] = [
            'url' => $this->addLangPrefix() . '/equipment/top',
            'title' => $this->getPageTitleByUrl('/equipment/top'),
        ];
        $categories = Category::select('id', 'slug', 'title_ru', 'title_ua', 'title_en')->where('active', 1)->get();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $c = [
                    'url' => $this->addLangPrefix() . '/equipment/' . $category->slug,
                    'title' => $category->{'title' . getLocaleDBSuf()},
                ];
                $products = Product::select('id', 'slug', 'title_ru', 'title_ua', 'title_en')->where('category_id', $category->id)->where('active', 1)->whereNull('deleted_at')->get();
                if (!empty($products)) {
                    foreach ($products as $product) {
                        $p = [
                            'url' => $this->addLangPrefix() . '/equipment/product/' . $product->slug,
                            'title' => $product->{'title' . getLocaleDBSuf()},
                        ];
                        $c['children'][] = $p;
                    }
                }
                $arr[] = $c;
            }
        }
        return $arr;
    }

    private function getFaqLinks()
    {
        $arr = [];
        $items = FaqGroup::select('id', 'slug', 'title_ru', 'title_ua', 'title_en')->active()->get();
        if (!empty($items)) {
            foreach ($items as $item) {
                $f = [
                    'url' => $this->addLangPrefix() . '/faq/' . $item->slug,
                    'title' => $item->{'title' . getLocaleDBSuf()},
                ];
                $faqs = Faq::select('id', 'slug', 'title_ru', 'title_ua', 'title_en')->active()->where('faq_group_id', $item->id)->get();
                if (!empty($faqs)) {
                    foreach ($faqs as $faq) {
                        $f['children'][] = [
                            'url' => $this->addLangPrefix().'/faq/'.$item->slug.'/'.$faq->slug,
                            'title' => $faq->{'title' . getLocaleDBSuf()},
                        ];
                    }
                }
                $arr[] = $f;
            }
        }

        return $arr;
    }

    private function getHelpLinks()
    {
        $arr = [];
        $items = HelpDirectory::select('id', 'slug', 'title_ru', 'title_ua', 'title_en')->active()->where('parent_id', 0)->orderBy('sort', 'asc')->get();
        foreach ($items as $item) {
            $children = [];
            $res = HelpDirectory::select('id', 'slug', 'title_ru', 'title_ua', 'title_en')->active()->where('parent_id', $item->id)->orderBy('sort', 'asc')->get();
            foreach ($res as $r) {
                $subChildren = [];
                $subRes = HelpDirectory::select('id', 'slug', 'title_ru', 'title_ua', 'title_en')->active()->where('parent_id', $r->id)->orderBy('sort', 'asc')->get();
                foreach ($subRes as $s) {
                    $subChildren[] = [
                        'url' => $this->addLangPrefix() . '/help/' . $s->slug,
                        'title' => $s->{'title' . getLocaleDBSuf()},
                    ];
                }
                $children[] = [
                    'url' => $this->addLangPrefix() . '/help/' . $r->slug,
                    'title' => $r->{'title' . getLocaleDBSuf()},
                    'children' => $subChildren,
                ];
            }
            $arr[] = [
                'url' => $this->addLangPrefix() . '/help/' . $item->slug,
                'title' => $item->{'title' . getLocaleDBSuf()},
                'children' => $children,
            ];
        }
        return $arr;
    }

    private function getBlogLinks()
    {
        $arr = [];
        $categories = BlogCategory::isActive()->get();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $arr[] = [
                    'url' => $this->addLangPrefix() . '/blog/category/' . $category->slug,
                    'title' => $category->{'title' . getLocaleDBSuf()},
                    'children' => [],
                ];
            }
        }

        $articles = Blog::isActive()->notDeleted()->published()->get();
        if (!empty($articles)) {
            foreach ($articles as $article) {
                $arr[] = [
                    'url' => $this->addLangPrefix() . '/blog/article/' . $article->slug,
                    'title' => $article->{'title' . getLocaleDBSuf()},
                    'children' => [],
                ];
            }
        }

        return $arr;
    }

    private function addStaticUrlsToHtmlSitemap()
    {
        $urls = $this->getStaticUrls();
        if (!empty($urls)) {
            foreach ($urls as $url) {
                if (in_array($url, $this->tvs)) {
                    continue;
                }
                if (in_array($url, ['/equipment/top'])) {
                    continue;
                }
                if (!empty($url)) {
                    if (in_array($url, ['/internet-dlya-biznesa', '/televidenie', '/domashniy-internet', '/televidenie-i-internet'])) {
                        $links = [
                            'url' => $this->addLangPrefix() . $url,
                            'title' => $this->getTariffsInfo($url),
                        ];
                    } else {
                        $links = [
                            'url' => $this->addLangPrefix() . $url,
                            'title' => $this->getPageTitleByUrl($url),
                        ];
                    }

                    if ($url == '/news') {
                        $links['children'] = $this->getNewsLinks('news');
                    } elseif ($url == '/actions') {
                        $links['children'] = $this->getNewsLinks('actions');
                    } elseif ($url == '/televidenie') {
                        $links['children'] = $this->getTVLinks();
                    } elseif ($url == '/equipment') {
                        $links['children'] = $this->getEquipmentLinks();
                    } elseif ($url == '/faq') {
                        $links['children'] = $this->getFaqLinks();
                    } elseif ($url == '/help') {
                        $links['children'] = $this->getHelpLinks();
                    } elseif ($url == '/blog') {
                        $links['children'] = $this->getBlogLinks();
                    }
                    $this->addToHtmlSitemap($links);
                }
            }
        }
        return true;
    }

    private function addStaticUrlsToMap()
    {
        $urls = $this->getStaticUrls();
        if (!empty($urls)) {
            foreach ($urls as $url) {
                if (!empty($url)) {
                    $this->sitemap->add(Url::create($this->addLangPrefix() . $url)->setLastModificationDate(Carbon::now()));
                }
            }
        }
    }

    private function addNewsToMap()
    {
        $items = News::select('id', 'entity_type', 'slug')->active()->published()->isNews()->get();
        if (!empty($items)) {
            foreach ($items as $item) {
                $url = $this->addLangPrefix() . '/' . $item->entity_type . '/' . $item->slug;
                $this->sitemap->add(Url::create($url)->setLastModificationDate(Carbon::now()));
            }
        }
    }

    private function addActionsToMap()
    {
        $items = News::select('id', 'entity_type', 'slug')->active()->published()->isActions()->get();
        if (!empty($items)) {
            foreach ($items as $item) {
                $url = $this->addLangPrefix() . '/' . $item->entity_type . '/' . $item->slug;
                $this->sitemap->add(Url::create($url)->setLastModificationDate(Carbon::now()));
            }
        }
    }

    private function addFaqsToMap()
    {
        $items = FaqGroup::select('id', 'slug')->active()->get();
        if (!empty($items)) {
            foreach ($items as $item) {
                $url = $this->addLangPrefix() . '/faq/' . $item->slug;
                $this->sitemap->add(Url::create($url)->setLastModificationDate(Carbon::now()));
                $children = Faq::select('id', 'slug')->where('faq_group_id',$item->id)->active()->get();
                if(!empty($children)){
                    foreach($children as $child){
                        $url = $this->addLangPrefix() . '/faq/' . $item->slug . '/' . $child->slug;
                        $this->sitemap->add(Url::create($url)->setLastModificationDate(Carbon::now()));
                    }
                }
            }
        }
    }

    private function addHelpsToMap()
    {
        $items = HelpDirectory::select('id', 'slug')->active()->orderBy('parent_id', 'asc')->get();
        if (!empty($items)) {
            foreach ($items as $item) {
                $url = $this->addLangPrefix() . '/help/' . $item->slug;
                $this->sitemap->add(Url::create($url)->setLastModificationDate(Carbon::now()));
            }
        }
    }

    private function addShopToMap()
    {
        $categories = Category::where('active', 1)->get();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $url = $this->addLangPrefix() . '/equipment/' . $category->slug;
                $this->sitemap->add(Url::create($url)->setLastModificationDate(Carbon::now()));
            }
        }
        $products = Product::where('active', 1)->whereNull('deleted_at')->get();
        if (!empty($products)) {
            foreach ($products as $product) {
                $url = $this->addLangPrefix() . '/equipment/product/' . $product->slug;
                $this->sitemap->add(Url::create($url)->setLastModificationDate(Carbon::now()));
            }
        }
    }

    private function addBlogToMap()
    {
        $categories = BlogCategory::isActive()->get();
        if (!empty($categories)) {
            foreach ($categories as $category) {
                $url = $this->addLangPrefix() . '/blog/category/' . $category->slug;
                $this->sitemap->add(Url::create($url)->setLastModificationDate(Carbon::now()));
            }
        }

        $articles = Blog::select('slug')->isActive()->notDeleted()->published()->get();
        if (!empty($articles)) {
            foreach ($articles as $article) {
                $url = $this->addLangPrefix() . '/blog/article/' . $article->slug;
                $this->sitemap->add(Url::create($url)->setLastModificationDate(Carbon::now()));
            }
        }
    }


}
