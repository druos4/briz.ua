<?php

namespace App\Services;

use App\Category;
use App\Service;
use App\Tariff;
use Carbon\Carbon;
use League\CommonMark\Inline\Element\Emphasis;

class TariffService
{
    const PAGINATION_ADMIN = 30;

    private $tariff_type,$filters,$sort = ['field' => 'sort', 'direction' => 'asc'];
    private $serviceTypes = [
        '1' => 'Интернет+IPTV',
        '2' => 'Интернет TV',
        '3' => 'Интернет',
        '4' => 'Интернет посуточно',
        '5' => 'Интернет+TVMax',
        '6' => 'Интернет+BrizTv',
    ];

    private $index_groups = [
        'home_inet' => 'Домашний интеренет',
        'inet_tv' => 'Интернет + TV',
        'tv' => 'Телевидение',
        'business' => 'Для бизнеса',
    ];

    private $business_groups = [
        'pppoe' => 'PPPoE',
        'vlan' => 'VLAN',
        'iptv' => 'IPTV',
    ];

    public function __construct($tariff_type = null)
    {
        $this->setTariffType($tariff_type);
    }

    public function getServiceTypes()
    {
        return $this->serviceTypes;
    }

    public function getIndexGroups()
    {
        return $this->index_groups;
    }

    public function getBusinessGroups()
    {
        return $this->business_groups;
    }

    private function setTariffType($tariff_type)
    {
        $this->tariff_type = $tariff_type;
    }

    private function getTariffType()
    {
        return $this->tariff_type;
    }

    public function importFromBilling($input)
    {
        if(!empty($input)){
            foreach($input as $item){
                $this->createOrUpdate($item);
            }
        }
    }

    private function createOrUpdate($item)
    {
        $tariff = Tariff::where('billing_id',$item['id'])->where('tariff_type',$this->getTariffType())->first();
        if(!empty($tariff)){
            $this->updateItem($item, $tariff);
        } else {
            $this->createNewItem($item);
        }
    }

    private function updateItem($item, $tariff)
    {
        $price = $item['Price'];
        if($this->getTariffType() == 'internet' && $item['UserType'] == 2){
            $price += 30;
        }
        $data = [
            'tariff_type' => $this->getTariffType(),
            'service_type' => (!empty($item['ServiceType'])) ? $item['ServiceType'] : null,
            'monthly' => $item['IntMonth'],
            'daily' => $item['IntDay'],
            'user_type' => $item['UserType'],
            'price' => $price,
            'price_union' => (isset($item['Union'])) ? $item['Union'] : null,
            'bonus_payment' => $item['isBonusPayment'],
            'description_old_ru' => $item['Description'],
            'description_old_ua' => $item['Description'],
            'description_old_en' => $item['Description'],
        ];
        $tariff->update($data);
    }

    private function createNewItem($item)
    {
        $price = $item['Price'];
        if($this->getTariffType() == 'internet' && $item['UserType'] == 2){
            $price += 30;
        }
        $data = [
            'tariff_type' => $this->getTariffType(),
            'service_type' => (!empty($item['ServiceType'])) ? $item['ServiceType'] : null,
            'monthly' => $item['IntMonth'],
            'daily' => $item['IntDay'],
            'user_type' => $item['UserType'],
            'title_ru' => $item['Name'],
            'title_ua' => $item['Name'],
            'title_en' => $item['Name'],
            'price' => $price,
            'price_union' => (isset($item['Union'])) ? $item['Union'] : null,
            'bonus_payment' => $item['isBonusPayment'],
            'get_bonus' => false,
            'can_seven_days' => false,
            'prepay_discount' => false,
            'description_ru' => $item['Description'],
            'description_ua' => $item['Description'],
            'description_en' => $item['Description'],
            'description_old_ru' => $item['Description'],
            'description_old_ua' => $item['Description'],
            'description_old_en' => $item['Description'],
            'billing_id' => $item['id'],
            'active' => false,
            'sort' => ceil($item['Price'] / 10),
        ];
        Tariff::create($data);
    }

    public function getPage($code)
    {
        return $this->getServiceModel($code);
    }

    private function prepareProductData($product)
    {
        $item = [
            'id' => $product->id,
            'title' => $product->{'title' . getLocaleDBSuf()},
            'slug' => $product->slug,
            'url' => getLocaleHrefPrefix().'/'.$product->category->slug.'/'.$product->slug,
            'picture' => $product->picture,
            'anons' => $product->{'anons' . getLocaleDBSuf()},
            'price' => $product->price,
            'discount' => $product->discount,
            'price_old' => $product->price_old,
            'category_title' => $product->category->{'uslugy_title' . getLocaleDBSuf()},
            'category_text' => $product->category->{'uslugy_description' . getLocaleDBSuf()},
        ];
        return $item;
    }

    private function getServiceModel($code)
    {
        $result = [
            'meta' => [],
            'products' => [],
        ];
        $item = Service::with('products')->where('code',$code)->first();
        if($item){

            $result['meta']['title'] = $item->{'title' . getLocaleDBSuf()};
            $result['meta']['meta-title'] = $item->{'meta_title' . getLocaleDBSuf()};
            $result['meta']['meta-description'] = $item->{'meta_description' . getLocaleDBSuf()};
            $result['meta']['meta-keywords'] = $item->{'meta_keywords' . getLocaleDBSuf()};

            if(!empty($item->products)){
                foreach($item->products as $prod){
                    $result['products'][] = $this->prepareProductData($prod);
                }
            }
        }
        return $result;
    }



    public function getItemsForFront($option = null)
    {
        return $this->buildQueryForFront($option);
    }

    public function getItemForFront($id)
    {
        $res = Tariff::where('id',$id)->first();
        if($res){
            $description = $res->{'description' . getLocaleDBSuf()};
            $description = str_replace('https://www.briz.ua/','/',$description);
            $description = str_replace('target="_blank"','',$description);
            $description = str_replace('href="','href="'.getLocaleHrefPrefix(),$description);

            $description = str_replace('/spisok-kanalov-paket-rasshirennyiy-iptv',getLocaleHrefPrefix().'/spisok-kanalov-paket-rasshirennyiy-iptv',$description);


            $item = [
                'id' => $res->id,
                'title' => $res->{'title' . getLocaleDBSuf()},
                'description' => addNofollowToLink($description),
                'price' => $res->price,
                'price_old' => $res->price_old,
                'first_to_show' => $res->first_to_show,
                'bonuses' => ($res->daily == 1) ? false : round(($res->price / 20),2),
            ];
            return $item;
        }
        return false;
    }

    private function checkForShowConditions($items)
    {
        if(!empty($items)){
            foreach($items as $key => $item){
                if(empty($item->show_from) && empty($item->show_to)){
                    continue;
                } else {
                    if(!empty($item->show_to) && $item->show_to < Carbon::now()){
                        unset($items[$key]);
                    } elseif(!empty($item->show_from) && $item->show_from > Carbon::now()){
                        unset($items[$key]);
                    }
                }
            }
        }
        return $items;
    }

    private function buildQueryForFront($option)
    {
        $items = [];
        $query = Tariff::active();
        if($this->getTariffType() == 'internet'){
            $query->inet();
        } elseif($this->getTariffType() == 'ctv'){
            $query->ctv();
        } elseif($this->getTariffType() == 'iptv'){
            $query->iptv();
        } elseif($this->getTariffType() == 'union'){
            $query->union();
        } elseif($this->getTariffType() == 'tv'){
            $query->where(function($query2) {
                $query2->where('tariff_type', 'ctv')
                    ->orWhere('tariff_type', 'iptv');
            })->notLegalUser();
        }

        switch ($option) {
            case 'daily':
                //$query->plainUser()->daily();
                $query->whereIn('user_type',[0,1])->daily();
                break;
            case 'home':
                //$query->plainUser()->monthly();
                $query->whereIn('user_type',[0,1])->monthly();
                break;
            case 'business':
                //$query->legalUser();
                $query->whereIn('user_type',[0,2]);
                break;
            case 'inet+tv':
                $query->inetTv();
                break;
            case 'inet+iptv':
                $query->inetIptv();
                break;
            case 'inet+tvmax':
                $query->inetTvmax();
                break;
            case 'inet+briztv':
                $query->inetBrizTv();
                break;
            case 'index_home_inet':
                $query->where('index_group','home_inet');
                break;
            case 'index_business':
                $query->where('index_group','business');
                break;
            case 'index_tv':
                $query->where('index_group','tv');
                break;
            case 'index_inet_tv':
                $query->where('index_group','inet_tv');
                break;
            case 'business_pppoe':
                $query->where('business_group','pppoe');
                break;
            case 'business_vlan':
                $query->where('business_group','vlan');
                break;
            case 'business_iptv':
                $query->where('business_group','iptv');
                break;
        }

        $sort = $this->getSorting();
        $res = $query->orderBy($sort['field'],$sort['direction'])->get();
        $res = $this->checkForShowConditions($res);

        $inet_daily = false;
        if(!empty($res)){
            foreach($res as $r){
                if($r->billing_id == 188){
                    $inet_daily = true;
                }
            }
        }

        $bgs = [];
        $count = count($res);
        //$count = 101;
        if($inet_daily){
            $count--;
        }
        $colors = [
            [99,167,247, 1,13],
            [70,199,190, 1,21.35],
            [60,199,99, 1,42.71],
            [250,194,61, 1,61.98],
            [249,163,63, 1,80.59],
            [250,88,88, 1,100],
            [217,217,217,1,100]
        ];
        if($count > 1){
            $delta = round((87 / ($count - 1)),2);
            $p = 100;

            for($j = $count; $j > 0; $j--){
                if($p == 100){
                    $str = 'linear-gradient(90deg,
    rgba('.$colors[0][0].','.$colors[0][1].','.$colors[0][2].','.$colors[0][3].') '.$colors[0][4].'%,
    rgba('.$colors[1][0].','.$colors[1][1].','.$colors[1][2].','.$colors[1][3].') '.$colors[1][4].'%,
    rgba('.$colors[2][0].','.$colors[2][1].','.$colors[2][2].','.$colors[2][3].') '.$colors[2][4].'%,
    rgba('.$colors[3][0].','.$colors[3][1].','.$colors[3][2].','.$colors[3][3].') '.$colors[3][4].'%,
    rgba('.$colors[4][0].','.$colors[4][1].','.$colors[4][2].','.$colors[4][3].') '.$colors[4][4].'%,
    rgba('.$colors[5][0].','.$colors[5][1].','.$colors[5][2].','.$colors[5][3].') '.$colors[5][4].'%,
    rgba('.$colors[6][0].','.$colors[6][1].','.$colors[6][2].','.$colors[6][3].') '.$colors[6][4].'%
    )';
                } elseif($j == 1){
                    $str = 'linear-gradient(90deg,
    rgba('.$colors[0][0].','.$colors[0][1].','.$colors[0][2].','.$colors[0][3].') '.$colors[0][4].'%,
    rgba('.$colors[1][0].','.$colors[1][1].','.$colors[1][2].','.$colors[1][3].') '.$colors[0][4].'%,
    rgba('.$colors[2][0].','.$colors[2][1].','.$colors[2][2].','.$colors[2][3].') '.$colors[0][4].'%,
    rgba('.$colors[3][0].','.$colors[3][1].','.$colors[3][2].','.$colors[3][3].') '.$colors[0][4].'%,
    rgba('.$colors[4][0].','.$colors[4][1].','.$colors[4][2].','.$colors[4][3].') '.$colors[0][4].'%,
    rgba('.$colors[5][0].','.$colors[5][1].','.$colors[5][2].','.$colors[5][3].') '.$colors[0][4].'%,
    rgba('.$colors[6][0].','.$colors[6][1].','.$colors[6][2].','.$colors[6][3].') '.$colors[0][4].'%
    )';
                } else {


                    if($p >= 80.59){
                        $a = (100 - $p) * (100 / 19.41);
                        $d_r = 250 - floor((250 - 249) / 100 * $a);
                        $d_g = 88 + floor((163 - 88) / 100 * $a);
                        $d_b = 88 - floor((88 - 63) / 100 * $a);
                        $d_l = $p;
                        $str = 'linear-gradient(90deg,
                            rgba('.$colors[0][0].','.$colors[0][1].','.$colors[0][2].','.$colors[0][3].') '.$colors[0][4].'%,
                            rgba('.$colors[1][0].','.$colors[1][1].','.$colors[1][2].','.$colors[1][3].') '.$colors[1][4].'%,
                            rgba('.$colors[2][0].','.$colors[2][1].','.$colors[2][2].','.$colors[2][3].') '.$colors[2][4].'%,
                            rgba('.$colors[3][0].','.$colors[3][1].','.$colors[3][2].','.$colors[3][3].') '.$colors[3][4].'%,
                            rgba('.$colors[4][0].','.$colors[4][1].','.$colors[4][2].','.$colors[4][3].') '.$colors[4][4].'%,
                            rgba('.$d_r.','.$d_g.','.$d_b.','.$colors[5][3].') '.$d_l.'%,
                            rgba('.$colors[6][0].','.$colors[6][1].','.$colors[6][2].','.$colors[6][3].') '.$d_l.'%
                            )';
                    } elseif($p < 80.59 && $p >= 61.98){
                        $a = (80.59 - $p) * (100 / 18.61);
                        $d_r = 249 + floor((250 - 249) / 100 * $a);
                        $d_g = 163 + floor((194 - 163) / 100 * $a);
                        $d_b = 63 - floor((63 - 61) / 100 * $a);
                        $d_l = $p;
                        $str = 'linear-gradient(90deg,
                            rgba('.$colors[0][0].','.$colors[0][1].','.$colors[0][2].','.$colors[0][3].') '.$colors[0][4].'%,
                            rgba('.$colors[1][0].','.$colors[1][1].','.$colors[1][2].','.$colors[1][3].') '.$colors[1][4].'%,
                            rgba('.$colors[2][0].','.$colors[2][1].','.$colors[2][2].','.$colors[2][3].') '.$colors[2][4].'%,
                            rgba('.$colors[3][0].','.$colors[3][1].','.$colors[3][2].','.$colors[3][3].') '.$colors[3][4].'%,
                            rgba('.$d_r.','.$d_g.','.$d_b.','.$colors[4][3].') '.$d_l.'%,
                            rgba('.$colors[5][0].','.$colors[5][1].','.$colors[5][2].','.$colors[5][3].') '.$d_l.'%,
                            rgba('.$colors[6][0].','.$colors[6][1].','.$colors[6][2].','.$colors[6][3].') '.$d_l.'%
                            )';
                    } elseif($p < 61.98 && $p >= 42.71){
                        $a = (61.98 - $p) * (100 / 19.27);
                        $d_r = 250 - floor((250 - 60) / 100 * $a);
                        $d_g = 194 + floor((199 - 194) / 100 * $a);
                        $d_b = 61 + floor((99 - 61) / 100 * $a);
                        $d_l = $p;
                        $str = 'linear-gradient(90deg,
                            rgba('.$colors[0][0].','.$colors[0][1].','.$colors[0][2].','.$colors[0][3].') '.$colors[0][4].'%,
                            rgba('.$colors[1][0].','.$colors[1][1].','.$colors[1][2].','.$colors[1][3].') '.$colors[1][4].'%,
                            rgba('.$colors[2][0].','.$colors[2][1].','.$colors[2][2].','.$colors[2][3].') '.$colors[2][4].'%,
                            rgba('.$d_r.','.$d_g.','.$d_b.','.$colors[3][3].') '.$d_l.'%,
                            rgba('.$colors[4][0].','.$colors[4][1].','.$colors[4][2].','.$colors[4][3].') '.$d_l.'%,
                            rgba('.$colors[5][0].','.$colors[5][1].','.$colors[5][2].','.$colors[5][3].') '.$d_l.'%,
                            rgba('.$colors[6][0].','.$colors[6][1].','.$colors[6][2].','.$colors[6][3].') '.$d_l.'%
                            )';
                    } elseif($p < 42.71 && $p >= 21.35){
                        $a = (42.71 - $p) * (100 / 21.36);
                        $d_r = 60 + floor((70 - 60) / 100 * $a);
                        $d_g = 199 - floor((199 - 199) / 100 * $a);
                        $d_b = 99 + floor((190 - 99) / 100 * $a);
                        $d_l = $p;
                        $str = 'linear-gradient(90deg,
                            rgba('.$colors[0][0].','.$colors[0][1].','.$colors[0][2].','.$colors[0][3].') '.$colors[0][4].'%,
                            rgba('.$colors[1][0].','.$colors[1][1].','.$colors[1][2].','.$colors[1][3].') '.$colors[1][4].'%,
                            rgba('.$d_r.','.$d_g.','.$d_b.','.$colors[2][3].') '.$d_l.'%,
                            rgba('.$colors[3][0].','.$colors[3][1].','.$colors[3][2].','.$colors[3][3].') '.$d_l.'%,
                            rgba('.$colors[4][0].','.$colors[4][1].','.$colors[4][2].','.$colors[4][3].') '.$d_l.'%,
                            rgba('.$colors[5][0].','.$colors[5][1].','.$colors[5][2].','.$colors[5][3].') '.$d_l.'%,
                            rgba('.$colors[6][0].','.$colors[6][1].','.$colors[6][2].','.$colors[6][3].') '.$d_l.'%
                            )';
                    } elseif($p < 21.35 && $p >= 13){
                        $a = (21.35 - $p) * (100 / 8.35);
                        $d_r = 70 + floor((99 - 70) / 100 * $a);
                        $d_g = 199 - floor((199 - 167) / 100 * $a);
                        $d_b = 190 + floor((247 - 190) / 100 * $a);
                        $d_l = $p;
                        $str = 'linear-gradient(90deg,
                            rgba('.$colors[0][0].','.$colors[0][1].','.$colors[0][2].','.$colors[0][3].') '.$colors[0][4].'%,
                            rgba('.$d_r.','.$d_g.','.$d_b.','.$colors[1][3].') '.$d_l.'%,
                            rgba('.$colors[2][0].','.$colors[2][1].','.$colors[2][2].','.$colors[2][3].') '.$d_l.'%,
                            rgba('.$colors[3][0].','.$colors[3][1].','.$colors[3][2].','.$colors[3][3].') '.$d_l.'%,
                            rgba('.$colors[4][0].','.$colors[4][1].','.$colors[4][2].','.$colors[4][3].') '.$d_l.'%,
                            rgba('.$colors[5][0].','.$colors[5][1].','.$colors[5][2].','.$colors[5][3].') '.$d_l.'%,
                            rgba('.$colors[6][0].','.$colors[6][1].','.$colors[6][2].','.$colors[6][3].') '.$d_l.'%
                            )';
                    }
                }

                $p -= $delta;
                if($p < 0){
                    $p = 0;
                }
                $bgs[] = $str;
            }
        } else {
            $p = 100;
            $str = 'linear-gradient(90deg,
    rgba('.$colors[0][0].','.$colors[0][1].','.$colors[0][2].','.$colors[0][3].') '.$colors[0][4].'%,
    rgba('.$colors[1][0].','.$colors[1][1].','.$colors[1][2].','.$colors[1][3].') '.$colors[1][4].'%,
    rgba('.$colors[2][0].','.$colors[2][1].','.$colors[2][2].','.$colors[2][3].') '.$colors[2][4].'%,
    rgba('.$colors[3][0].','.$colors[3][1].','.$colors[3][2].','.$colors[3][3].') '.$colors[3][4].'%,
    rgba('.$colors[4][0].','.$colors[4][1].','.$colors[4][2].','.$colors[4][3].') '.$colors[4][4].'%,
    rgba('.$colors[5][0].','.$colors[5][1].','.$colors[5][2].','.$colors[5][3].') '.$colors[5][4].'%,
    rgba('.$colors[6][0].','.$colors[6][1].','.$colors[6][2].','.$colors[6][3].') '.$colors[6][4].'%
    )';
            $bgs[] = $str;
        }

        if(!empty($res)){

            $i = 0;
            foreach($res as $r){
                if($inet_daily == true && $r->billing_id == 177){
                    $inet100 = (!empty($bgs[$i])) ? $bgs[$i] : null;
                }
                if(isset($inet100) && $inet_daily == true && $r->billing_id == 188){
                    $gradient = $inet100;
                } else {
                    $gradient = (!empty($bgs[$i])) ? $bgs[$i] : null;
                }
                $disc = [];
                if($r->prepay_discount){
                    $disc[] = [
                        'month' => 12,
                        'discount' => '20%',
                        'amount' => ceil($r->price * 0.8),
                        'full_price' => ceil(($r->price * 12) * 0.8),
                        ];
                    $disc[] = [
                        'month' => 6,
                        'discount' => '10%',
                        'amount' => ceil($r->price * 0.9),
                        'full_price' => ceil(($r->price * 6) * 0.9),
                    ];
                    $disc[] = [
                        'month' => 3,
                        'discount' => '5%',
                        'amount' => ceil($r->price * 0.95),
                        'full_price' => ceil(($r->price * 3) * 0.95),
                    ];
                } else {
                    $disc = [];
                }

                $description = $r->{'description' . getLocaleDBSuf()};
                $description = str_replace('https://www.briz.ua/','/',$description);
                $description = str_replace('target="_blank"','',$description);
                $description = str_replace('href="','href="'.getLocaleHrefPrefix(),$description);
                if(in_array($r->tariff_type,['union','iptv','ctv'])){
                    if($r->tariff_type == 'union'){
                        $cId = (!empty($r->channels_from)) ? $r->channels_from : null;
                    } else {
                        $cId = $r->id;
                    }
                    $description = $this->replaceChannelsCount($description,$cId);
                }

                $bonuses = (in_array($r->tariff_type,['internet','union','iptv'])) ? round(($r->price / 20),2) : 0;
                if($r->daily == 1){
                    $bonuses = false;
                }
                $lang = app()->getLocale();
                if($lang == ''){
                    $lang = 'ua';
                }
                $items[] = [
                    'id' => $r->id,
                    'billing_id' => $r->billing_id,
                    'title' => $r->{'title' . getLocaleDBSuf()},
                    'description' => $description,
                    'price' => $r->price,
                    'price_old' => $r->price_old,
                    'bonuses' => $bonuses,
                    'can_pay_bonuses' => $r->bonus_payment,
                    'can_get_bonuses' => $r->get_bonus,
                    'can_seven_days' => $r->can_seven_days,
                    'can_prepay_discount' => $r->prepay_discount,
                    'power' => ($r->power != '') ? $r->power : null,
                    'discounts' => $disc,
                    'picture' => $r->picture,
                    'background' => (!empty($gradient)) ? $gradient : null,
                    'first_to_show' => $r->first_to_show,
                    'seo_id' => $r->tariff_type.'-'.$lang.'-'.$r->id.'-'.$r->billing_id,
                    'has_films' => $r->has_films,
                    'films_price' => $r->films_price,
                    'films_title' => $r->{'films_title' . getLocaleDBSuf()},
                ];
                $i++;
            }
        }

        return $items;
    }

    private function replaceChannelsCount($description, $id)
    {
        if(!empty($id)){
            if(strpos($description,'<a') !== false && strpos($description,'XXX') !== false && strpos($description,'<a') < strpos($description,'XXX')){
                $ch_service = new ChannelsService();
                $count = $ch_service->countChannelsInTariff($id);
                if(!empty($count)){
                    $description = str_replace('XXX',$count,$description);
                }
            } elseif(strpos($description,'<a') !== false && strpos($description,'xxx') !== false && strpos($description,'<a') < strpos($description,'xxx')){
                $ch_service = new ChannelsService();
                $count = $ch_service->countChannelsInTariff($id);
                if(!empty($count)){
                    $description = str_replace('xxx',$count,$description);
                }
            } elseif(strpos($description,'<a') !== false && strpos($description,'ХХХ') !== false && strpos($description,'<a') < strpos($description,'ХХХ')){
                $ch_service = new ChannelsService();
                $count = $ch_service->countChannelsInTariff($id);
                if(!empty($count)){
                    $description = str_replace('ХХХ',$count,$description);
                }
            } elseif(strpos($description,'<a') !== false && strpos($description,'ххх') !== false && strpos($description,'<a') < strpos($description,'ххх')){
                $ch_service = new ChannelsService();
                $count = $ch_service->countChannelsInTariff($id);
                if(!empty($count)){
                    $description = str_replace('ххх',$count,$description);
                }
            }
        }
        return $description;
    }

    public function getItemsForAdmin($input)
    {
        $this->setFilter($input);
        $this->setSorting($input);
        $items = $this->buildQuery()->paginate(self::PAGINATION_ADMIN);

        return ['items' => $items, 'filter' => $this->getFilters(), 'sort' => $this->getSorting()];
    }

    private function buildQuery()
    {
        $query = Tariff::orderBy($this->sort['field'],$this->sort['direction']);;
        if(isset($this->filters['active'])){
            if($this->filters['active'] == 1){
                $query->active();
            } else {
                $query->inActive();
            }
        }

        if(!empty($this->filters['id'])){
            $query->where('id',$this->filters['id']);
        }

        if(!empty($this->filters['title'])){
            $query->where(function ($query2) {
                $query2->where('title_ru', 'LIKE', '%'.$this->filters['title'].'%')
                    ->orWhere('title_ua', 'LIKE', '%'.$this->filters['title'].'%')
                    ->orWhere('title_en', 'LIKE', '%'.$this->filters['title'].'%');
            });
        }

        if(!empty($this->filters['user_type'])){
            $query->where(function ($query2) {
                $query2->where('user_type',$this->filters['user_type'])
                    ->orWhere('user_type',0);
            });
        }

        if(!empty($this->filters['index_group'])){
            $query->where('index_group',$this->filters['index_group']);
        }

        if(!empty($this->filters['business_group'])){
            $query->where('business_group',$this->filters['business_group']);
        }

        if(!empty($this->filters['period'])){
            if($this->filters['period'] == 'month'){
                $query->where('monthly',true);
            } elseif($this->filters['period'] == 'day'){
                $query->where('daily',true);
            }
        }

        if(!empty($this->filters['tariff_type'])){
            if($this->filters['tariff_type'] == 'internet'){
                $query->inet();
            } elseif($this->filters['tariff_type'] == 'ctv'){
                $query->ctv();
            } elseif($this->filters['tariff_type'] == 'iptv'){
                $query->iptv();
            } elseif($this->filters['tariff_type'] == 'union'){
                $query->union();
            }
        }


        return $query;
    }

    private function setFilter($input)
    {
        $upd = false;
        if(isset($input['id']))
        {
            $this->filters['id'] = $input['id'];
            $upd = true;
        }
        if(isset($input['tariff_type']))
        {
            $this->filters['tariff_type'] = $input['tariff_type'];
            $upd = true;
        }
        if(isset($input['title']))
        {
            $this->filters['title'] = $input['title'];
            $upd = true;
        }
        if(isset($input['active']))
        {
            $this->filters['active'] = $input['active'];
            $upd = true;
        }
        if(isset($input['period']))
        {
            $this->filters['period'] = $input['period'];
            $upd = true;
        }
        if(isset($input['user_type']))
        {
            $this->filters['user_type'] = $input['user_type'];
            $upd = true;
        }
        if(isset($input['index_group']))
        {
            $this->filters['index_group'] = $input['index_group'];
            $upd = true;
        }
        if(isset($input['business_group']))
        {
            $this->filters['business_group'] = $input['business_group'];
            $upd = true;
        }

        if($upd == true){
            session(['filtersTariffs' => $this->filters]);
        } else {
            $sess_filter = session('filtersTariffs');
            if(!empty($sess_filter)){
                $this->filters = $sess_filter;
            }
        }

    }

    private function getFilters()
    {
        return $this->filters;
    }

    private function setSorting($input)
    {
        $this->sort = ['field' => 'id', 'direction' => 'desc'];
        if(!empty($input['sort']) && !empty($input['direction'])){
            $this->sort = ['field' => $input['sort'], 'direction' => $input['direction']];
        }
    }

    private function getSorting()
    {
        return $this->sort;
    }

    public function getPageForFront($code)
    {
        $page = Service::with('products.category')->where('code',$code)->first();
        if($page){
            $faq_service = new FaqService();
            $union_description = $faq_service->parseUnionDescription($page->{'union_description' . getLocaleDBSuf()});

            $result = [
                'title' => $page->{'title' . getLocaleDBSuf()},
                'banner' => $page->{'banner' . getLocaleDBSuf()},
                'union_description' => $union_description,
                'meta_title' => $page->{'meta_title' . getLocaleDBSuf()},
                'meta_description' => $page->{'meta_description' . getLocaleDBSuf()},
                'meta_keywords' => $page->{'meta_keywords' . getLocaleDBSuf()},
                'products' => [],
            ];

            if(!empty($page->products)){
                $category_id = 0;
                foreach($page->products as $product){
                    if($product->category_id > 0 && $product->active == 1){
                        if($category_id == 0){
                            $category_id = $product->category_id;
                        }
                        $result['products'][] = [
                            'id' => $product->id,
                            'title' => $product->{'title' . getLocaleDBSuf()},
                            'slug' => $product->slug,
                            'url' => getLocaleHrefPrefix().'/'.$product->category->slug.'/'.$product->slug,
                            'anons' => $product->{'anons' . getLocaleDBSuf()},
                            'price' => $product->price,
                            'picture' => (!empty($product->picture_recomend_hd)) ? $product->picture_recomend_hd : $product->picture,
                        ];
                    }
                }

                if($category_id > 0){
                    $category = Category::find($category_id);
                    $result['category_title'] = $category->{'uslugy_title' . getLocaleDBSuf()};
                    $result['category_description'] = $category->{'uslugy_description' . getLocaleDBSuf()};
                }
            }
            return $result;
        }
        return false;
    }

    public function getTarifsForIndexPage()
    {
        //$result = \Cache::remember(getLocale().'tariffs-main-page', 86400, function () {
            $result = [
                'homeInet' => [],
                'union' => [],
                'tv' => [],
                'business' => [],
            ];

            $this->setSorting([
                    'sort' => 'sort',
                    'direction' => 'desc']
            );

            $result['homeInet'] = $this->getItemsForFront('index_home_inet');
            $result['business'] = $this->getItemsForFront('index_business');
            $result['tv'] = $this->getItemsForFront('index_tv');
            $result['union'] = $this->getItemsForFront('index_inet_tv');
            return $result;
        //});
        return $result;
    }

    public function copyTariffInfo($parentId, $targetId)
    {
        $parent = Tariff::find($parentId);
        if($parent){
            Tariff::where('id',$targetId)->update([
                'title_ru' => $parent->title_ru,
                'title_ua' => $parent->title_ua,
                'title_en' => $parent->title_en,
                'bonus_payment' => $parent->bonus_payment,
                'get_bonus' => $parent->get_bonus,
                'can_seven_days' => $parent->can_seven_days,
                'prepay_discount' => $parent->prepay_discount,
                'description_ru' => $parent->description_ru,
                'description_ua' => $parent->description_ua,
                'description_en' => $parent->description_en,
                'sort' => $parent->sort,
                'power' => $parent->power,
                'union_group' => $parent->union_group,
                'index_group' => $parent->index_group,
                'price_old' => $parent->price_old,
                'picture' => $parent->picture,
                'business_group' => $parent->business_group,
                'channels_from' => $parent->channels_from,
            ]);
        }
    }

}


