<?php


namespace App\Services\Markup;

interface MarkupInterface
{

    public function render(): string;

}
