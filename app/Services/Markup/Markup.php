<?php

namespace App\Services\Markup;

class Markup implements MarkupInterface
{
    private $markups = [];

    public function addMarkup(MarkupInterface $markup)
    {
        array_push($this->markups, $markup);
    }

    public function render(): string
    {
        $html = '';
        foreach($this->markups as $markup){
            $html .= $markup->render();
        }
        return $html;
    }
}
