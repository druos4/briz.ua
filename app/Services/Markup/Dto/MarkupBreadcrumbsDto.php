<?php


namespace App\Services\Markup\Dto;


class MarkupBreadcrumbsDto
{
    public $url = '';

    public $title = '';

    /**
     * @return null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param null $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function buildDto(array $array)
    {
        if (!empty($array['url'])) {
            $this->setUrl($array['url']);
        }
        if(!empty($array['title'])) {
            $this->setTitle($array['title']);
        }
    }

    public function toArray()
    {
        return [
            'url' => $this->getUrl(),
            'title' => $this->getTitle(),
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

}
