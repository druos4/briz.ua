<?php


namespace App\Services\Markup\Dto;


class MarkupBlogDto
{
    public $url = '';

    public $title = '';

    public $pics = [];

    public $created = '';

    public $updated = '';

    public $logo = '';

    public $description = '';

    /**
     * @return null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param null $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return null
     */
    public function getPics()
    {
        return $this->pics;
    }

    /**
     * @param null $pics
     */
    public function setPics($pics): void
    {
        $this->pics = $pics;
    }

    /**
     * @return null
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param null $created
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }

    /**
     * @return null
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param null $updated
     */
    public function setUpdated($updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return null
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param null $url
     */
    public function setLogo($logo): void
    {
        $this->logo = $logo;
    }

     /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function buildDto(array $array)
    {
        if (!empty($array['url'])) {
            $this->setUrl($array['url']);
        }
        if(!empty($array['title'])) {
            $this->setTitle($array['title']);
        }
        if(!empty($array['pics'])){
        $this->setPics($array['pics']);
        }
        if(!empty($array['created'])) {
            $this->setCreated($array['created']);
        }
        if(!empty($array['updated'])) {
            $this->setUpdated($array['updated']);
        }
        if(!empty($array['logo'])) {
            $this->setLogo($array['logo']);
        }
        if(!empty($array['description'])) {
            $this->setDescription($array['description']);
        }
    }

    public function toArray()
    {
        return [
            'url' => $this->getUrl(),
            'title' => $this->getTitle(),
            'pics' => $this->getPics(),
            'created' => $this->getCreated(),
            'updated' => $this->getUpdated(),
            'logo' => $this->getLogo(),
            'description' => $this->getDescription(),
        ];
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }

}
