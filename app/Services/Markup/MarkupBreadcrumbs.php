<?php

namespace App\Services\Markup;

use App\Services\Markup\Dto\MarkupBreadcrumbsDto;

class MarkupBreadcrumbs implements MarkupInterface
{
    private $array = [];

    public function __construct(array $array)
    {
        if(!empty($array)){
            foreach ($array as $arr){
                $dto = new MarkupBreadcrumbsDto();
                $dto->buildDto($arr);
                array_push($this->array, $dto->toArray());
            }
        }
    }

    public function render(): string
    {
        $html = '';
        if(!empty($this->array)){
            $html .= '
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement":
    [';
            $i = 0;
            foreach($this->array as $item){
                $i++;
                $html .= '
        {
            "@type": "ListItem",
            "position": '.$i.',
            "item":
                {
                    "@id": "'.env('APP_URL').$item['url'].'",
                    "name": "'.strip_tags(str_replace('"','',$item['title'])).'"
                }
            }';
                if($i < count($this->array)){$html .= ',';}
            }
        }
        $html .= '
    ]
}
</script>';
        return $html;
    }


}
