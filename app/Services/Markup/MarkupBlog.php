<?php

namespace App\Services\Markup;

use App\Services\Markup\Dto\MarkupBlogDto;

class MarkupBlog implements MarkupInterface
{
    private $array;

    public function __construct(array $array)
    {
        $dto = new MarkupBlogDto();
        $dto->buildDto($array);
        $this->array = $dto->toArray();
    }

    public function render(): string
    {
        $html = '';
        if(!empty($this->array)){
            $html .= '
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Article",
        "mainEntityOfPage":
            {
                "@type": "WebPage",
                "@id": "'.$this->array['url'].'"
            },
        "headline": "'.$this->array['title'].'",';
            $html .= '"image": [';
            if(!empty($this->array['pics'])){
                $html .= implode(",",$this->array['pics']);
            }
            $html .= '],';
            $html .= '"datePublished": "'.$this->array['created'].'",
            "dateModified": "'.$this->array['updated'].'",
            "author":
                {    "@type": "Person",    "name": "Author"  },
            "publisher": {
                "@type": "Organization",
                "name": "Briz",
                "logo":
                    {
                        "@type": "ImageObject",
                        "url": "https://www.briz.ua/images/logo-header.svg"
                    }
                },
            "description": "'.$this->array['description'].'"
        }
</script>';
        }
        return $html;
    }


}
