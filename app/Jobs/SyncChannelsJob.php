<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncChannelsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $urls = config('channels');
        if(!empty($urls)){
            BeforeParseChannels::dispatch();
            foreach($urls as $url){
                ParseChannelUrl::dispatch($url['url'], $url['tarif_ids'], $url['is_analog']);
            }
            AfterParseChannels::dispatch();
            \Cache::flush();
        }
    }
}
