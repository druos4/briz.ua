<?php

namespace App\Jobs;

use App\Services\ImagesMap\Parser\Parser;
use App\Services\ImagesMap\SitemapReader;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MakeImagesSitemapJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sitemapReader = new SitemapReader();
        $pages = $sitemapReader->get();
        if (!empty($pages)) {
            $parser = new Parser($pages);
            $parser->process();
        }

        echo(get_class($this) . " done\n");
    }
}
