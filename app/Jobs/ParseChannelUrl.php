<?php

namespace App\Jobs;

use App\Channel;
use App\Services\Channels\ChannelDBSaver;
use App\Services\Channels\ChannelTariffSaver;
use App\Services\Channels\GenreDBSaver;
use App\Services\Channels\ChannelGenreSaver;
use App\Services\Channels\ChannelLogoSaver;
use App\Services\Channels\Dto\ChannelGenreDto;
use App\Services\Channels\Image\Saver;
use App\Services\Channels\Parser\Dto\ChannelDto;
use App\Services\Channels\Parser\Parser;
use App\Services\ChannelsService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ParseChannelUrl implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $url;
    private $billingIds;
    private $isAnalog;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $billingIds, $isAnalog = false)
    {
        $this->url = $url;
        $this->billingIds = $billingIds;
        $this->isAnalog = $isAnalog;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Parser $parser)
    {

        $channelsCollection = $parser->process($this->url);
        if ($channelsCollection->isEmpty()) {
            throw new \Exception('nothing to parse');
        }

        $saver = new ChannelLogoSaver(new ChannelDBSaver());
        $genreSaver = new ChannelGenreSaver();
        $tariffSaver = new ChannelTariffSaver();
        $channelsCollection->map(function (\App\Services\Channels\Parser\Dto\ChannelDto $item) use ($saver,$genreSaver,$tariffSaver) {

            $dbChannel = new \App\Services\Channels\Dto\ChannelDto();
            $dbChannel->setBillingId($item->getId());
            $dbChannel->setBillingLogo($item->getLogo());
            $dbChannel->setSort($item->getOrderNum());
            $dbChannel->setString($item->getGenre());
            $dbChannel->setTitle($item->getName());
            $dbChannel->setIsAnalog($this->isAnalog);
            $dbChannel->setParsed(1);

            $channelModel = $saver->save($dbChannel);

            $dbGenre = new \App\Services\Genre\Dto\GenreDto();
            $dbGenre->setCode($item->getGenre());
            $genreSaver->save($dbGenre,$channelModel);

            $tariffSaver->save($channelModel,$this->billingIds);
        });

        //info('Parsed: ' . $channelsCollection->count());
        echo 'Parsed: ' . $channelsCollection->count().' ';
    }

}
