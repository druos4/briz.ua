<?php

namespace App\Jobs;

use App\Channel;
use App\Services\Channels\GenreDBSaver;
use App\Services\Channels\ChannelLogoSaver;
use App\Services\Channels\Dto\GenreDto;
use App\Services\Channels\Image\Saver;
use App\Services\Channels\Parser\Parser;
use App\Services\ChannelsService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BeforeParseChannels implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Parser $parser)
    {
        $parser->beforeProcess();
        echo 'Channels ready for parse';
    }
}
