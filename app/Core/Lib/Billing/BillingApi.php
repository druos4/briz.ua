<?php


namespace App\Core\Lib\Billing;

use App\Core\Lib\Brizclient\BrizClient;

class BillingApi
{
    public static function get()
    {
        $config = [
            'api_baseuri' => env('BRIZ_API_ENDPOINT'),
            'api_key' => env('BRIZ_API_KEY')
        ];

        return new BrizClient($config);
    }
}
