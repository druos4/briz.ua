<?php
namespace App\Core\Lib\Brizclient;

class BillingClient extends Request
{
    private $url;
    private $token;

    protected $commandMethods = [
        'getDevices' => 'GET'
    ];

    public function __construct(Array $config)
    {
        $this->url = $config['api_endpoint'];
        $this->token = $config['api_token'];
    }

    public function execute($target, $command, $identifier, $data = [])
    {
        $method = count($data) ? 'POST' : 'GET';

        if (in_array($command, array_keys($this->commandMethods))) {
            $method = $this->commandMethods[$command];
        }

        $url = $this->url . '?' . http_build_query([
            'token' => $this->token,
            'id' => $identifier,
            'act' => $command,
            'type' => $target,
        ]);

        return $this->send($method, $url, $data);
    }

}
