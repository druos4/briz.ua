<?php
namespace App\Core\Lib\Brizclient;

use App\Core\Lib\Brizclient\Exceptions;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response as GuzzleResponse;

/**
 *  Request
 */
abstract class Request
{

    protected function send($method, $url, $data = null, $headers = [])
    {
        $client = new Client([
            'timeout' => 10
        ]);

        $params = [];

        if (in_array(strtolower($method), ['get', 'delete'])) {
            $url .= '&' . http_build_query($data);
        } else {
            $params = array_merge($params, [
                'form_params' => $data
            ]);
        }

        $response = $client->request($method, $url, $params);

        $body = $response->getBody();

        if (!$body->getContents()) {
            throw new Exceptions\ApiException();
        }

        $result = json_decode($body, true);

        if ($this->isFailed($response)) {
            $message = is_array($result['error']) ? $result['error'][0]['Text'] : null;
            throw new Exceptions\ApiResponseException($message);
        }

        return $result;
    }


    protected function isFailed(GuzzleResponse $response)
    {

        if ($response->getStatusCode() >= 400) {
            return true;
        }

        $result = json_decode($response->getBody(), true);

        if (json_last_error() != JSON_ERROR_NONE) {
            return true;
        }

        if (isset($result['error'])) {
            if ($result['error'] != false) {
                return true;
            }
        }

        if (isset($result['status'])) {
            return ($result['status'] == 'error');
        }

        return false;
    }
}
