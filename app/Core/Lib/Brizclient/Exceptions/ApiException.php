<?php

namespace App\Core\Lib\Brizclient\Exceptions;

use Exception;

/**
 *  Exceptions_ApiException
 */
class ApiException extends Exception
{

    public function __construct($message = 'Unexpected service Error', $statusCode = 502)
    {
        parent::__construct($message);
    }
}
