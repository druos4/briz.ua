<?php

namespace App\Core\Lib\Brizclient\Exceptions;

use Exception;

/**
 *  Exceptions_ApiResponseException
 */
class ApiResponseException extends ApiException
{

    public function __construct($message = 'Unexpected API Error', $statusCode = 400)
    {
        parent::__construct($message);
    }
}
