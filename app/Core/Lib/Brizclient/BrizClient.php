<?php

namespace App\Core\Lib\Brizclient;

use App\Http\Tools\ApiResponse;
//use Brizlibs\BillingClients\Exceptions\ApiException;
use App\Core\Lib\Brizclient\Exceptions\ApiException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use App\Core\Lib\Brizclient\Exceptions;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use mysql_xdevapi\Exception;

/**
 *  BrizClient
 */
class BrizClient extends Request
{
    private $url;
    private $apikey;
    private $client;
    private $commands = [
        'socialConnect' => ['uri' => '/buser/%s/socials/%s', 'method' => 'POST'],
        'socialDisable' => ['uri' => '/buser/%s/socials/%s', 'method' => 'DELETE'],
        'emailConfirm' => ['uri' => '/buser/auth/%s', 'method' => 'POST'],
        'userAuth' => ['uri' => '/buser/auth/%s', 'method' => 'POST'],
        'userAccountList' => ['uri' => '/buser/%s/accounts', 'method' => 'GET'],
        'userInetAccountList' => ['uri' => '/buser/%s/internets', 'method' => 'GET'],
        'userIptvAccountList' => ['uri' => '/buser/%s/iptvs', 'method' => 'GET'],
        'userUnionAccountList' => ['uri' => '/buser/%s/unions', 'method' => 'GET'],
        'userCtvAccountList' => ['uri' => '/buser/%s/ctvs', 'method' => 'GET'],
        'userInfo' => ['uri' => '/buser/%s', 'method' => 'GET'],
        'userPhoneSetting' => ['uri' => '/buser/%d/edit/phone', 'method' => 'PUT'],
        'userPhoneDelete' => ['uri' => '/buser/%d/delete/phone/%d', 'method' => 'DELETE'],
        'userPrioritiesInfo' => ['uri' => '/buser/%d/payments/priorities', 'method' => 'GET'],
        'userPrioritiesSetting' => ['uri' => '/buser/%d/payments/priorities', 'method' => 'POST'],
        'smsInfoSendConfirmation' => ['uri' => '/sms/confirm', 'method' => 'POST'],
        'smsInfoSendCheck' => ['uri' => '/sms/confirm/check', 'method' => 'POST'],
        'smsInfoAdd' => ['uri' => '/buser/%d/sms/save', 'method' => 'POST'],
        'smsInfoSettings' => ['uri' => '/buser/%d/edit/sms', 'method' => 'PUT'],
        'smsInfoTelegramIdAdd' => ['uri' => '/buser/%d/sms/telegram', 'method' => 'POST'],
        'smsInfoDelete' => ['uri' => '/buser/%d/sms/delete', 'method' => 'DELETE'],
        'internetAccountPrepaidsList' => ['uri' => '/internet/%d/prepaids', 'method' => 'GET'],
        'iptvAccountPrepaidsList' => ['uri' => '/iptv/%d/prepaids', 'method' => 'GET'],
        'ctvAccountPrepaidsList' => ['uri' => '/ctv/%d/prepaids', 'method' => 'GET'],
        'iptvAccountDeviceList' => ['uri' => '/iptv/%d/devices', 'method' => 'GET'],
        'iptvAccountDeviceInfo' => ['uri' => '/iptv/%d/devices/%d', 'method' => 'GET'],
        'internetGetMac' => ['uri' => '/internet/%d/mac', 'method' => 'GET'],
        'internetChangeMac' => ['uri' => '/internet/%d/mac', 'method' => 'PATCH'],
        'internetServices' => ['uri' => '/internet/services', 'method' => 'GET'],
        'ctvServices' => ['uri' => '/ctv/services', 'method' => 'GET'],
        'iptvServices' => ['uri' => '/iptv/services', 'method' => 'GET'],
        'unionServices' => ['uri' => '/unions/services', 'method' => 'GET'],

        'allowedServicesByServiceAndAccount' => ['uri' => '/buser/%d/%s/%d/services/%s', 'method' => 'GET'],
        'allowedServicesByService' => ['uri' => '/buser/%d/%s/services', 'method' => 'GET'],
        'serviceAccount' => ['uri' => '/buser/%d/%s/%d', 'method' => 'GET'],
        'internetAccount' => ['uri' => '/buser/%d/internets/%d', 'method' => 'GET'],
        'internetAccountUpdate' => ['uri' => '/buser/%d/internets/%d', 'method' => 'PUT'],
        //'internetSessions' => ['uri' => '/buser/%d/internets/%d/sessions', 'method' => 'GET'],
        'internetSessions' => ['uri' => '/internet/%d/log-sessions', 'method' => 'GET'],
        'internetOnlineSessions' => ['uri' => '/internet/%d/online-sessions', 'method' => 'GET'],
        'internetStartOnlineSessions' => ['uri' => '/internet/%d/online-start-sessions', 'method' => 'GET'],
        'iptvAccount' => ['uri' => '/buser/%d/iptvs/%d', 'method' => 'GET'],
        'iptvCreateDevice' => ['uri' => '/iptv/%d/device', 'method' => 'POST'],
        'iptvCreateDeviceByMAC' => ['uri' => '/iptv/%d/device/mac', 'method' => 'POST'],
        'iptvRemoveDevice' => ['uri' => '/iptv/%d/device/%d', 'method' => 'DELETE'],
        'iptvChangeDevice' => ['uri' => '/iptv/%d/device/%d', 'method' => 'PUT'],
        'internetServicesByAccountType' => ['uri' => '/internet/types/%d/services', 'method' => 'GET'],
        'createFeedBack' => ['uri' => '/order/feedback', 'method' => 'POST'],
        'addMoneyCard' => ['uri' => '/buser/%d/money/card', 'method' => 'POST'],
        'payments' => ['uri' => '/buser/%d/%s/%d/payments/%s', 'method' => 'POST'],
        'paymentsHistory' => ['uri' => '/buser/%d/payments/history', 'method' => 'GET'],
        'paymentsHistoryForCustomers' => ['uri' => '/buser/%d/payments/customer/history', 'method' => 'GET'],
        'bonusHistory' => ['uri' => '/buser/%d/payments/history/bonus', 'method' => 'GET'],
        'allServicePaymentInit' => ['uri' => '/buser/%d/payments/init/all', 'method' => 'GET'],
        'internetInit' => ['uri' => '/buser/%d/payments/init/inet', 'method' => 'GET'],
        'iptvInit' => ['uri' => '/buser/%d/payments/init/iptv', 'method' => 'GET'],
        'ctvInit' => ['uri' => '/buser/%d/payments/init/ctv', 'method' => 'GET'],
        'unionInit' => ['uri' => '/buser/%d/payments/init/hybrid', 'method' => 'GET'],
        'emailInfoServiceAdd' => ['uri' => '/buser/%d/email/add', 'method' => 'POST'],
        'emailInfoServiceResend' => ['uri' => '/buser/%d/email/resend', 'method' => 'POST'],
        'emailInfoServiceOptions' => ['uri' => '/buser/%d/email/options', 'method' => 'POST'],
        //'emailInfoServiceConfirm' => ['uri' => '/buser/%d/email/check/code', 'method' => 'POST'],
        'emailInfoServiceConfirm' => ['uri' => '/buser/email/check/code', 'method' => 'POST'],
        'emailInfoServiceEdit' => ['uri' => '/buser/%d/email/update', 'method' => 'PUT'],
        'resetUserPassword' => ['uri' => '/buser/password/reset', 'method' => 'POST'],
        'changeUserPassword' => ['uri' => '/buser/password/change', 'method' => 'PUT'],
        'viewAllUserPaymentCards' => ['uri' => '/buser/%d/payments/cards', 'method' => 'GET'],
        'regularPaymentSettings' => ['uri' => '/buser/%d/payments/regular', 'method' => 'GET'],
        'enableAutoPays' => ['uri' => '/buser/%d/payments/regular/enable', 'method' => 'POST'],
        'disableAutoPays' => ['uri' => '/buser/%d/payments/regular/disable', 'method' => 'POST'],
        'internetPaymentsCalculate' => ['uri' => '/buser/%d/internets/%d/payments/calculate', 'method' => 'POST'],
        'internetPaymentsPay' => ['uri' => '/buser/%d/internets/%d/payments/pay', 'method' => 'POST'],
        'iptvPaymentsCalculate' => ['uri' => '/buser/%d/iptvs/%d/payments/calculate', 'method' => 'POST'],
        'iptvPaymentsPay' => ['uri' => '/buser/%d/iptvs/%d/payments/pay', 'method' => 'POST'],
        'ctvPaymentsCalculate' => ['uri' => '/buser/%d/ctvs/%d/payments/calculate', 'method' => 'POST'],
        'ctvPaymentsPay' => ['uri' => '/buser/%d/ctvs/%d/payments/pay', 'method' => 'POST'],
        'ctvAccount' => ['uri' => '/buser/%d/ctvs/%d', 'method' => 'GET'],
        'unionAccount' => ['uri' => '/buser/%d/unions/%d', 'method' => 'GET'],
        'unionPaymentsCalculate' => ['uri' => '/buser/%d/unions/%d/payments/calculate', 'method' => 'POST'],
        'unionPaymentsPay' => ['uri' => '/buser/%d/unions/%d/payments/pay', 'method' => 'POST'],
        'userPaymentsCalculate' => ['uri' => '/buser/%d/payments/calculate/all', 'method' => 'POST'],
        'userPaymentsPay' => ['uri' => '/buser/%d/payments/pay/all', 'method' => 'POST'],
        'userNeedPay' => ['uri' => '/buser/%d/payments/needPay', 'method' => 'GET'],
        'userToPay' => ['uri' => '/buser/%d/payments/toPay', 'method' => 'GET'],
        'paymentsCalculateAll' => ['uri' => '/buser/%d/payments/calculate/all', 'method' => 'POST'],
        '7daysAccounts' => ['uri' => '/buser/%d/internets/7days/available', 'method' => 'GET'],
        '7daysActivate' => ['uri' => '/buser/%d/internets/7days/activate', 'method' => 'PUT'],
        'ctvCalculateRecovery' => ['uri' => '/ctv/accounts/%d/calcRecovery', 'method' => 'GET'],
        'newsNotify' => ['uri' => '/news/notify', 'method' => 'GET'],
        'deletePlatonCard' => ['uri' => '/buser/%d/payments/cards/%d', 'method' => 'DELETE'],
        'activateInet100' => ['uri' => '/buser/%d/activateInet100/%d', 'method' => 'POST'],
        'sendCredentials' => ['uri' => '/buser/auth/credentials', 'method' => 'POST'],
        'updateUserPassword' => ['uri' => '/buser/%d/password/update', 'method' => 'PUT'],
        'firebaseConnectDevice' => ['uri' => '/firebase', 'method' => 'POST'],
        'firebaseGetMessages' => ['uri' => '/firebase', 'method' => 'GET'],
        'firebaseMarkReadedMessages' => ['uri' => '/firebase/read', 'method' => 'PUT'],
        'testCurl' => ['uri' => '/test-curl', 'method' => 'POST'],
        'send-feedback' => ['uri' => '/order/feedback-site', 'method' => 'POST'],
        'send-legal-feedback' => ['uri' => '/order/legal/feedback', 'method' => 'POST'],
        'getAllMarkers' => ['uri' => '/addrmap/getAllMarkers', 'method' => 'GET'],
        'getActions' => ['uri' => '/shares/get', 'method' => 'GET'],

        'getProducts' => ['uri' => '/s_section/site/available', 'method' => 'GET'],

        'getSiteRepairs' => ['uri' => '/news/site-repairs', 'method' => 'GET'],//new url
        //'getSiteRepairs' => ['uri' => '/news/site', 'method' => 'GET'],//old url

    ];

    public function __construct(Array $config)
    {
        $this->url = $config['api_baseuri'];
        $this->apikey = $config['api_key'];

        $this->client = new Client([
            'timeout' => 10,
//            'headers' => [
//                'X-Authorization' => $this->apikey
//            ]
        ]);
    }

    protected function __produceErrorsMessage($response, $result)
    {

        //var_dump("isFailed: ", $this->isFailed($response));
        if ($this->isFailed($response)) {

            if (isset($result['errors'])) {

                if (!is_array($result['errors'])) {
                    $message = $result['errors']; //var_dump($message);
                    throw new Exceptions\ApiResponseException($message);
                }

                if (is_array($result['errors']) && !empty($result['errors'][0])) {
                    $message = (!empty($result['errors'][0]["Text"])) ? $result['errors'][0]["Text"] : $result['errors'][0];
                    throw new Exceptions\ApiResponseException($message);
                }

                throw new Exceptions\ApiResponseException(null);
            }

        }

    }


    public function execute($command, Array $data = null, ...$identity)
    {
        if (!in_array($command, array_keys($this->commands))) {
            throw new Exceptions\ApiException('failed_command_executing');
        }

        $url = $this->url . sprintf($this->commands[$command]['uri'], ...$identity);

        $params = [
            'headers' => [
                'X-Authorization' => $this->apikey
            ],
            'verify' => true,
        ];

        if (in_array(strtolower($this->commands[$command]['method']), ['get', 'delete'])) {
            $params = array_merge($params, [
                'query' => http_build_query($data)
            ]);
        } else {
            $params = array_merge($params, [
                'form_params' => $data
            ]);
        }

        try {
            $response = $this->client->request($this->commands[$command]['method'], $url, $params);
        } catch (GuzzleException $e)
        {
            $statusCode = $e->getCode();
            $err = json_decode($e->getMessage());
            if(strpos($err,'error 28') !== false){
                $err = 'Ошибка сервера';
            }
            throw new Exceptions\ApiException($err);
        }
        if(empty($response)){
            throw new Exceptions\ApiException('Response error');
        }
        $body = $response->getBody();
        $result = json_decode($body, true);
        if(!empty($result['errorCode']) && $result['errorCode'] == 429){
            return ['success' => false, 'error' => $result['errors']];
        }
        $this->__produceErrorsMessage($response, $result);
        return (isset($result['data'])) ? $result['data'] : $result;
    }

}
