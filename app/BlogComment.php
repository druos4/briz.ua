<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    protected $table = 'blogs_comments';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'blog_id',
        'parent_id',
        'lvl',
        'author',
        'comment',
        'moderated_at',
    ];

    public function scopeIsModerated($query)
    {
        return $query->whereNotNull('moderated_at');
    }

    public function scopeIsNotModerated($query)
    {
        return $query->whereNull('moderated_at');
    }

    public function blog()
    {
        return $this->belongsTo(Blog::class,'blog_id');
    }

    public function parent()
    {
        return $this->belongsTo(BlogComment::class,'parent_id');
    }

    public function children()
    {
        return $this->hasMany(BlogComment::class,'parent_id')->isModerated();
    }

}
