<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use SoftDeletes;

    protected $table = 'sliders';

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'picture',
        'picture_ru',
        'picture_ua',
        'picture_en',
        'url',
        'anons_ru',
        'anons_ua',
        'anons_en',
        'sort',
        'active',
        'created_by',
        'updated_by',
        'deleted_by',
        'title_color',
        'anons_color',
        'btn_title_color',
        'btn_bg_color',
        'picture_tablet',
        'picture_phone',
        'picture_prev',
        'show_on_main',
        'show_on_shop',
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeIndex($query)
    {
        return $query->where('show_on_main', 1);
    }

    public function scopeShop($query)
    {
        return $query->where('show_on_shop', 1);
    }

    public function scopeInActive($query)
    {
        return $query->where('active','!=', 1);
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'created_by');
    }

    public function updater()
    {
        return $this->belongsTo(User::class,'updated_by');
    }

    public function deleter()
    {
        return $this->belongsTo(User::class,'deleted_by');
    }
}
