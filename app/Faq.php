<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faqs';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'faq_group_id',
        'title_ru',
        'title_ua',
        'title_en',
        'answer_ru',
        'answer_ua',
        'answer_en',
        'sort',
        'active',
        'search',
        'slug',
        'meta_title_ru',
        'meta_title_ua',
        'meta_title_en',
        'meta_description_ru',
        'meta_description_ua',
        'meta_description_en',
        'meta_keywords_ru',
        'meta_keywords_ua',
        'meta_keywords_en',
    ];

    public function parent()
    {
        return $this->belongsTo(FaqGroup::class, 'faq_group_id');
    }

    public function scopeActive($query){
        return $query->where('active', true);
    }

}
