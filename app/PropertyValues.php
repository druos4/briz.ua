<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PropertyValues extends Model
{
    protected $table = 'property_values';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'property_id',
        'value_ru',
        'value_ua',
        'value_en',
        'slug',
        'sort',
        'meta_keyword_ru',
        'meta_keyword_ua',
        'meta_keyword_en',
        'icon',
    ];



    public function property()
    {
        return $this->belongsTo(Property::class);
    }







}
