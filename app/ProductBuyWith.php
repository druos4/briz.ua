<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductBuyWith extends Model
{
    protected $table = 'product_buy_with';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'product_id',
        'buywith_id',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function target()
    {
        return $this->belongsTo(Product::class,'buywith_id');
    }


}
