<?php

namespace App;

use App\PaymentMethod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\ShipmentMethod;
use App\OrderStatuses;
use App\Product;
use App\User;

class Order extends Model
{
    protected $table = 'orders';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'user_id',
        'total',
        'status_id',
        'payment_id',
        'shipment_id',
        'email',
        'first_name',
        'last_name',
        'middle_name',
        'phone',
        'dont_call_me',
        'promocode',
        'comments',
        'delivery_address',
        'session_id',
        'delivery_city',
    ];



    public function carts()
    {
        return $this->hasMany(Cart::class)->with('product');
    }



    public function payment(){
        return $this->belongsTo(PaymentMethod::class);
    }




    public function shipment(){
        return $this->belongsTo(ShipmentMethod::class);
    }



    public function status(){
        return $this->belongsTo(OrderStatuses::class,'status_id','code');
    }


    public function client(){
        return $this->belongsTo(User::class,'user_id');
    }


    public function manager(){
        return $this->belongsTo(User::class,'manager_id');
    }
}
