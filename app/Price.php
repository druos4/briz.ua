<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'prices';

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'title_ru',
        'title_ua',
        'title_en',
        'active',
        'sort',
        'link',
    ];

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}
