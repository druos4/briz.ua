chmod 777 -R ../storage
docker exec -it site_php cp .env.example .env
docker exec -it site_php chmod 666 .env
docker exec -it site_php composer update
docker exec -it site_php php artisan key:generate
docker exec -it site_php php artisan config:clear
docker exec -it site_php php artisan config:cache
docker exec -it site_php npm install
chmod 777 -R ../storage
