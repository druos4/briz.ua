@php

    if(detectIE())
        {
           $url = getLocaleHrefPrefix()."/ie-browser";
            header('Location: '.$url);
            exit;
        }


    try {
        $ssr = Http::post('http://localhost:'.env('SSR_PORT',8080).'/render', $page)->throw()->json();
    } catch (Exception $e) {
        $ssr = null;
    }

    $lastMod = (!empty($lastmod)) ? $lastmod : '2021-05-01 12:00:00';

    $lastModifiedTs = strtotime($lastMod); // время последнего изменения страницы
    $ifModifiedSinceTs = false;

    if (isset($_ENV['HTTP_IF_MODIFIED_SINCE'])) {
        $ifModifiedSinceTs = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
    }

    if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
        $ifModifiedSinceTs = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
    }

    header('Last-Modified: '. \Carbon\Carbon::createFromTimestamp($lastModifiedTs)->format("D, d M Y H:i:s \G\M\T"));
@endphp
    <!DOCTYPE html>
<html lang="{{ app()->getLocale()}}">
<html prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
    {{--<?=drawLastMod($lastmod)?>--}}
    <?=drawAltLangs()?>
    <?=drawRelCanonicalTag()?>
    @if(!empty($nofollow))
        <meta name="robots" content="noindex, nofollow"/>
    @endif
    @if(!empty($noindex))
        <meta name="robots" content="noindex, follow"/>
    @endif

<!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push(
                {'gtm.start': new Date().getTime(), event: 'gtm.js'}
            );
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NLG2J3C');</script>
    <!-- End Google Tag Manager -->

    <link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css"/>
    <link rel="preload" href="/fonts/Roboto/Roboto-Regular.ttf" as="font" type="font/ttf" crossorigin/>

    <link
        rel="stylesheet"
        href="https://unpkg.com/react-leaflet-markercluster/dist/styles.min.css"
    />
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <script src="{{ mix('/js/manifest.js') }}" defer></script>
    <script src="{{ mix('/js/vendor.js') }}" defer></script>
    <script src="{{ mix('/js/app.js') }}" defer></script>

    <script>
        let app = {
            getLang: function () {
                return document.querySelector('html').lang
            }
        }
    </script>

    @foreach($ssr['head'] ?? [] as $element)
        {!! $element !!}
    @endforeach

    <?php
    $title = (!empty($meta_title)) ? $meta_title : trans('custom.title');
    $description = (!empty($meta_description)) ? $meta_description : '';
    $keywords = (!empty($meta_keywords)) ? $meta_keywords : '';
    $picture = (!empty($meta_picture)) ? env('APP_URL') . $meta_picture : env('APP_URL') . '/favicon-96x96.png';
    ?>
    <title>{{ $title }}</title>
    <meta name="description" content="{{ $description }}"/>
    <meta name="keywords" content="{{ $keywords }}"/>
    <meta property="og:title" content="{{ $title }}"/>
    <meta property="og:description" content="{{ $description }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ getOgUrl() }}"/>
    <meta property="og:image" content="{{ $picture }}"/>
    @if(app()->getLocale() == 'ru')
        <meta property="og:locale" content="ru_UA"/>
    @elseif(app()->getLocale() == 'en')
        <meta property="og:locale" content="en"/>
    @else
        <meta property="og:locale" content="uk_UA"/>
    @endif

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <style>
        @font-face {
            font-family: RobotoRegular;
            src: url(/fonts/Roboto/Roboto-Regular.ttf) format('ttf');
            font-weight: 400;
            font-style: normal;
            font-display: swap;
        }

        body {
            font-family: RobotoRegular, sans-serif;
            font-weight: 400;
            font-style: normal;
        }

        #app {
            display: none;
        }
    </style>

    {{-- rbuc-723 --}}
    @if(env('APP_ENV') == 'production')
    <!-- chat for prod -->
        @if(app()->getLocale() == 'en')
            <script src="//code-eu1.jivosite.com/widget/OrnYVrdG3C" async></script>
        @elseif(app()->getLocale() == 'ru')
            <script src="//code-eu1.jivosite.com/widget/5byNTsnlVD" async></script>
        @else
            <script src="//code-eu1.jivosite.com/widget/dDEY10S1hV" async></script>
    @endif

@else
    <!-- chat for dev -->
        <script src="//code-eu1.jivosite.com/widget/EtgorrpYvl" async></script>
@endif

<!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            }
            ;
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '233581278274078');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=233581278274078&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->


    <meta name="facebook-domain-verification" content="sojgr3kcx5ya6h0mvn1cq3wra3jdwu"/>
</head>
<body>
<script type="application/ld+json">
  {
    "@context": "http://schema.org/",
    "@type": "Organization",
    "url": "https://www.briz.ua/",
    "logo":"https://www.briz.ua/images/logo-header.svg"
  }

</script>
<script type="application/ld+json">
  {
    "@context" : "http://schema.org",
    "@type" : "Organization",
    "name" : "Briz",
    "url" : "https://www.briz.ua/",
    "sameAs" : [  "https://www.instagram.com/briz_official/" ,
                  "https://www.facebook.com/brizodessa/" ,
                  "https://invite.viber.com/?g2=AQBP6T8C0GnSf0zICnMwJglZHVgGiKRa7zlioveG8yysTGbXk39nRbU14kQCy1Wm",
                  "https://t.me/brizinfo"
                ]
  }

</script>

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLG2J3C"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

@if ($ssr)
    {!! $ssr['body'] !!}
@else
    @inertia
@endif

</body>
<script>
    window.addEventListener('DOMContentLoaded', function () {
        var divApp = document.querySelector('#app');
        divApp.style.display = "block";
    })
</script>

@if(!empty($markup))
    <?=$markup?>
@endif
</html>
