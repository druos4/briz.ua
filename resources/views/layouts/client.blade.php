<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title')</title>
    @yield('meta')


    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">

    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Google fonts - Roboto-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,700">
    <!-- Bootstrap Select-->
    <link rel="stylesheet" href="{{ asset('/vendor/bootstrap-select/css/bootstrap-select.min.css') }}">
    <!-- owl carousel-->
    <link rel="stylesheet" href="{{ asset('/vendor/owl.carousel/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/owl.carousel/assets/owl.theme.default.css') }}">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('/css/client/style.default.css') }}" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('/css/client/custom.css') }}">
    <link rel="stylesheet" href="{{asset('/css/app.css') }}">
    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="/img/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/apple-touch-icon-152x152.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('style')
</head>
<body>
    @include('client.partials.header')

    @include('client.partials.breadcrumbs')
    <div id="app"></div>


    @yield('content')


    @include('client.partials.footer')

<!-- Javascript files-->
<script src="{{ asset('js/website.js') }}" type="text/javascript"></script>

<script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('/vendor/popper.js/umd/popper.min.js') }}"> </script>
<script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendor/jquery.cookie/jquery.cookie.js') }}"> </script>
<script src="{{ asset('/vendor/waypoints/lib/jquery.waypoints.min.js') }}"> </script>
<script src="{{ asset('/vendor/jquery.counterup/jquery.counterup.min.js') }}"> </script>
<script src="{{ asset('/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('/vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.min.js') }}"></script>
<script src="{{ asset('/js/client/jquery.parallax-1.1.3.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('/vendor/jquery.scrollto/jquery.scrollTo.min.js') }}"></script>
    <script async>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
<script src="{{ asset('/js/client/front.js') }}"></script>

    @yield('script')
    <!-- @include('client.modals.small-cart')


    @if (\Route::current()->getName() == 'client.cart.index')
        <script src="{{ asset('/js/client/big-cart.js') }}"></script>
    @else
        <script src="{{ asset('/js/client/small-cart.js') }}"></script>
    @endif -->

</body>
</html>
