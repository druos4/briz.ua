<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link href="/ie/style.css" rel="stylesheet">
</head>
<body>

<div class="section__ie ie__wrapper">
    <div class="ie__banner">
        <img class="ie__banner--img" src="/images/ie/ie-banner.png" alt="Update your browser">
    </div>
    <div class="ie__action">
        <h1 class="ie__title">
            {{trans('ie.title')}}
        </h1>
        <p class="ie__description">
             {{trans('ie.description')}}
        </p>
        <ul class="ie__browsers-list">
         <div class="ie__browsers-first">
            <li class="ie__browsers-item">
                <a class="ie__list-link" href="{{$chrome}}">
                    <img class="ie__list--img" src="/images/ie/google-img.png" alt="Google">
                </a>
            </li>
            <li class="ie__browsers-item">
                <a class="ie__list-link" href="{{$opera}}">
                    <img class="ie__list-img" src="/images/ie/opera-img.png" alt="Opera">
                </a>
            </li>
             </div>
             <div class="ie__browsers-second">
            <li class="ie__browsers-item">
                <a class="ie__list-link" href="{{$mozila}}">
                    <img class="ie__list-img" src="/images/ie/mazila-img.png" alt="Mozilla firefox">
                </a>
            </li>
            <li class="ie__browsers-item">
                <a class="ie__list-link" href="{{$edge}}">
                    <img class="ie__list-img" src="/images/ie/edg-img.png" alt="Microsoft edge">
                </a>
            </li>
             </div>
        </ul>
    </div>
</div>

</body>
</html>
