@extends('layouts.client')
@section('title')
    Новости
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('style')



@endsection
@section('content')

    <div id="content">
        <div class="container">


            <div id="map" style="width:1000px; height:800px"></div>



        </div>
    </div>


@endsection
@section('script')
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJMPznZQ2EFzmExhKaQFB09bS1p0-Me_Q&callback=initMap&libraries=&v=weekly"
        defer
    ></script>
    <script>
        "use strict";

        let map;
        let myLatLng;
        let marker;

        function initMap() {
            map = new google.maps.Map(document.getElementById("map"), {
                center: {
                    lat: 46.575650003075424,
                    lng: 30.806850032325286
                },
                zoom: 15
            });

            myLatLng = {lat: 46.575503, lng: 30.8064758};
            marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });

            $.ajax({
                type:'POST',
                url:'/map/markers',
                data:{},
                success:function(data) {
                    $.each(data.streets, function( index, value ) {
                        //console.log("lat: " + value.GeoLat + ", lng: " + value.GeoLon);
                        myLatLng = {lat: parseFloat(value.GeoLat), lng: parseFloat(value.GeoLon)};
                        marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            title: value.GeoName + ' ' +value.Dom
                        });
                    });
                }
            });

        }





    </script>
@endsection
