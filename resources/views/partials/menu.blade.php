<div class="sidebar" style="width:250px;">
    <nav class="sidebar-nav  ps--active-y" style="width:250px;">

        <ul class="nav" style="width:240px;">
            <li class="nav-item">
                <a href="{{ route("admin.home") }}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt">

                    </i>
                    Админпанель
                </a>
            </li>

            @can('content_access')
            <li class="nav-item nav-dropdown {{ request()->is('admin/faqs*') || request()->is('admin/services*') || request()->is('admin/payments*') ||
request()->is('admin/tariffs*') || request()->is('admin/tags*') || request()->is('admin/pages*') || request()->is('admin/news*') || request()->is('admin/genres*') ||
request()->is('admin/channels*') || request()->is('admin/sliders*') ? 'open' : '' }}">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-file nav-icon">

                    </i>
                    Контент
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route("admin.actions.index") }}" class="nav-link {{ request()->is('admin/actions*') ? 'active' : '' }}">
                            <i class="fas fa-bullhorn nav-icon"></i> Акции
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("admin.news.index") }}" class="nav-link {{ request()->is('admin/news*') ? 'active' : '' }}">
                            <i class="fas fa-newspaper nav-icon"></i> Новости
                        </a>
                    </li>
                    {{--
                    <li class="nav-item">
                        <a href="{{ route("admin.tags.index") }}" class="nav-link {{ request()->is('admin/tags*') ? 'active' : '' }}">
                            <i class="fas fa-tags nav-icon"></i> Теги
                        </a>
                    </li>
                    --}}
                    <li class="nav-item">
                        <a href="{{ route("admin.sliders.index") }}" class="nav-link {{ request()->is('admin/slider*') ? 'active' : '' }}">
                            <i class="fas fa-images nav-icon"></i> Баннера
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/admin/payments" class="nav-link {{ request()->is('admin/payments*') ? 'active' : '' }}">
                            <i class="fas fa-money-check-alt nav-icon"></i> Оплата
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/admin/tariffs" class="nav-link {{ request()->is('admin/tariffs*') ? 'active' : '' }}">
                            <i class="fas fa-cubes nav-icon"></i> Тарифы
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/services" class="nav-link {{ request()->is('admin/services*') ? 'active' : '' }}">
                            <i class="fas fa-cogs nav-icon"></i> Сервисы
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/faqs" class="nav-link {{ request()->is('admin/faqs*') ? 'active' : '' }}">
                            <i class="fas fa-question nav-icon"></i> F.A.Q.
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/genres" class="nav-link {{ request()->is('admin/genres*') ? 'active' : '' }}">
                            <i class="fas fa-bookmark nav-icon"></i> Жанры каналов
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/channels" class="nav-link {{ request()->is('admin/channels*') ? 'active' : '' }}">
                            <i class="fas fa-tv nav-icon"></i> Каналы
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route("admin.pages.index") }}" class="nav-link {{ request()->is('admin/pages*') ? 'active' : '' }}">
                            <i class="fas fa-file nav-icon"></i> Страницы
                        </a>
                    </li>
                </ul>
            </li>

                <li class="nav-item nav-dropdown {{ request()->is('admin/blogs*') || request()->is('admin/blogcategories*') || request()->is('admin/blogs/comments*') ? 'open' : '' }}">
                    <a class="nav-link  nav-dropdown-toggle">
                        <i class="fas fa-newspaper nav-icon">

                        </i>
                        Блог
                    </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a href="/admin/blogs" class="nav-link {{ request()->is('admin/blogs') ? 'active' : '' }}">
                                <i class="fas fa-newspaper nav-icon"></i> Статьи блога
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/admin/blogcategories" class="nav-link {{ request()->is('admin/blogcategories*') ? 'active' : '' }}">
                                <i class="fas fa-folder nav-icon"></i> Категории блога
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/admin/blogs/comments" class="nav-link {{ request()->is('admin/blogs/comments*') ? 'active' : '' }}">
                                <i class="fas fa-comments nav-icon"></i> Комментарии
                            </a>
                        </li>
                    </ul>
                </li>

            <li class="nav-item nav-dropdown {{ request()->is('admin/documents*') || request()->is('admin/rules*') || request()->is('admin/docactions*') || request()->is('admin/sertificats*') || request()->is('admin/prices*') || request()->is('admin/qualities*') ? 'open' : '' }}">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-copy nav-icon">

                    </i>
                    Документы
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/admin/prices" class="nav-link {{ request()->is('admin/prices*') ? 'active' : '' }}">
                            <i class="fas fa-file nav-icon"></i> Прайсы
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/documents" class="nav-link {{ request()->is('admin/documents*') ? 'active' : '' }}">
                            <i class="fas fa-file nav-icon"></i> Договоры
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/docactions" class="nav-link {{ request()->is('admin/docactions*') ? 'active' : '' }}">
                            <i class="fas fa-file nav-icon"></i> Правила акций
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/sertificats" class="nav-link {{ request()->is('admin/sertificats*') ? 'active' : '' }}">
                            <i class="fas fa-images nav-icon"></i> Сертификаты
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/qualities" class="nav-link {{ request()->is('admin/qualities*') ? 'active' : '' }}">
                            <i class="fas fa-images nav-icon"></i> Качество
                        </a>
                    </li>


                </ul>
            </li>
            @endcan
{{--
            <li class="nav-item nav-dropdown {{ request()->is('admin/orders*') || request()->is('admin/payment-methods*') || request()->is('admin/shipment-methods*') || request()->is('admin/order-status*') ? 'open' : '' }}">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-cart-arrow-down nav-icon">

                    </i>
                    Магазин
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/admin/orders" class="nav-link {{ request()->is('admin/orders*') ? 'active' : '' }}">
                            <i class="fas fa-shopping-cart nav-icon"></i> Заказы
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/payment-methods" class="nav-link {{ request()->is('admin/payment-methods*') ? 'active' : '' }}">
                            <i class="fas fa-money-check-alt nav-icon"></i> Типы оплаты
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/shipment-methods" class="nav-link {{ request()->is('admin/shipment-methods*') ? 'active' : '' }}">
                            <i class="fas fa-shipping-fast nav-icon"></i> Типы доставки
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/order-status" class="nav-link {{ request()->is('admin/order-status*') ? 'active' : '' }}">
                            <i class="fas fa-flag nav-icon"></i> Статусы заказов
                        </a>
                    </li>
                </ul>
            </li>
--}}
            @can('content_access')
            <li class="nav-item nav-dropdown {{ request()->is('admin/categories*') || request()->is('admin/products*') || request()->is('admin/properties*') || request()->is('admin/products-comments*') ? 'open' : '' }}">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-cubes nav-icon">

                    </i>
                    Товары
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/admin/categories" class="nav-link {{ request()->is('admin/categories*') ? 'active' : '' }}">
                            <i class="fas fa-folder nav-icon"></i> Категории
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/products" class="nav-link {{ request()->is('admin/products*') ? 'active' : '' }}">
                            <i class="fas fa-cube nav-icon"></i> Список товаров
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/properties" class="nav-link {{ request()->is('admin/properties*') ? 'active' : '' }}">
                            <i class="fas fa-cog nav-icon"></i> Свойства
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/recommend-products" class="nav-link {{ request()->is('admin/recommend-products*') ? 'active' : '' }}">
                            <i class="fas fa-dice nav-icon"></i> Рекомендуемый товар
                        </a>
                    </li>
                    {{--
                    <li class="nav-item">
                        <a href="/admin/products-comments" class="nav-link {{ request()->is('admin/products-comments*') ? 'active' : '' }}">
                            <i class="fas fa-comments nav-icon"></i> Комментарии
                        </a>
                    </li>
                    --}}
                </ul>
            </li>
            @endcan
{{--
            <li class="nav-item nav-dropdown {{ request()->is('admin/maps*') ? 'open' : '' }}">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-map-marker-alt nav-icon">

                    </i>
                    Карта
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="/admin/maps/add" class="nav-link {{ request()->is('admin/maps/add*') ? 'active' : '' }}">
                            <i class="fas fa-map-marked-alt nav-icon"></i> Добавить дом
                        </a>
                    </li>

                </ul>
            </li>
--}}


            @can('help_access')
            <li class="nav-item">
                <a href="/admin/help" class="nav-link {{ request()->is('admin/help*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-info">

                    </i>
                    Помощь
                </a>
            </li>
            @endcan


            <li class="nav-item">
                <a href="/admin/emails" class="nav-link {{ request()->is('admin/emails*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-envelope">

                    </i>
                    Emails
                </a>
            </li>


            <li class="nav-item">
                <a href="/admin/filemanager" class="nav-link {{ request()->is('admin/filemanager*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-file">

                    </i>
                    Файловый менеджер
                </a>
            </li>


            @can('content_access')
            <li class="nav-item">
                <a href="/admin/menu" class="nav-link {{ request()->is('admin/menu*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-list">

                    </i>
                    Меню
                </a>
            </li>
            <li class="nav-item">
                <a href="/admin/deleted-urls" class="nav-link {{ request()->is('admin/deleted-urls*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-exclamation">

                    </i>
                    URL 404
                </a>
            </li>

            @endcan

{{--
            <li class="nav-item">
                <a href="/admin/contacts" class="nav-link {{ request()->is('admin/contacts*') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-envelope">

                    </i>
                    Контакты
                </a>
            </li>
--}}
            @can('content_access')
            <li class="nav-item nav-dropdown {{ request()->is('admin/permissions*') || request()->is('admin/roles*') || request()->is('admin/users*') ? 'open' : '' }}">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-users nav-icon">

                    </i>
                    Пользователи
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                            <i class="fas fa-unlock-alt nav-icon">

                            </i>
                            Права
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                            <i class="fas fa-briefcase nav-icon">

                            </i>
                            Роли
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                            <i class="fas fa-user nav-icon">

                            </i>
                            Пользователи
                        </a>
                    </li>
                </ul>
            </li>
            @endcan

            <li class="nav-item">
                <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-sign-out-alt">

                    </i>
                    Выйти
                </a>
            </li>
        </ul>
{{--
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 869px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 415px;"></div>
        </div>
        --}}
    </nav>
    {{--<button class="sidebar-minimizer brand-minimizer" type="button"></button>--}}
</div>
