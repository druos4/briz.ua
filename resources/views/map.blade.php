@extends('layouts.client')
@section('title')
    Новости
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('style')



@endsection
@section('content')

    <div id="content">
        <div class="container">


            <div id="map" style="width:1000px; height:800px"></div>



        </div>
    </div>


@endsection
@section('script')
    <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
    <script>


        DG.then(function () {
            map = DG.map('map', {
                center: [46.575650003075424, 30.806850032325286],
                zoom: 15,
                geoclicker: true
            });


            console.log('MAPA');

            map.on('click', function(e) {
                console.log(e.latlng.lat + ', ' + e.latlng.lng);
                var lat = e.latlng.lat;
                var lon = e.latlng.lng;

            });
            DG.marker(["46.575503", "30.8064758"]).addTo(map).bindPopup("вул. Бочарова 39");

            $.ajax({
                type:'POST',
                url:'/map/markers',
                data:{},
                success:function(data) {

                    //console.log(data.streets);
                    $.each(data.streets, function( index, value ) {

                        DG.marker([value.GeoLat, value.GeoLon]).addTo(map).bindPopup(value.GeoName + ' ' +value.Dom);
                    });
                    //DG.marker([46.575550826009376, 30.806581377983097]).addTo(map).bindPopup('Сахарова 26');

                }
            });

/*
            DG.marker([46.328934, 30.628638]).addTo(map).bindPopup('Александровка, Центральная');
            DG.marker([46.575550826009376, 30.806581377983097]).addTo(map).bindPopup('Сахарова 26');

 */
        });
    </script>
@endsection
