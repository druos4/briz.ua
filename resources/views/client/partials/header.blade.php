
<div id="all">
    <!-- Top bar-->
    <div class="top-bar">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-6 d-md-block d-none">
                    <p>Contact us on +420 777 555 333 or hello@universal.com.</p>
                </div>
                <div class="col-md-6">
                    <div class="d-flex justify-content-md-end justify-content-between">
                        <ul class="list-inline contact-info d-block d-md-none">
                            <li class="list-inline-item"><a href="#"><i class="fa fa-phone"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                        <div class="login">
                            @if(auth()->id() > 0)
                                <a href="{{ getLocaleHrefPrefix() }}/cabinet" class="login-btn"><i class="fa fa-user"></i><span class="d-none d-md-inline-block">Кабинет</span></a>
                            @else
                                <a href="#" data-toggle="modal" data-target="#login-modal" class="login-btn"><i class="fa fa-sign-in"></i><span class="d-none d-md-inline-block">Sign In</span></a>
                                <a href="customer-register.html" class="signup-btn"><i class="fa fa-user"></i><span class="d-none d-md-inline-block">Sign Up</span></a>
                            @endif




                            <a href="" class="btn-small-cart"><i class="fa fa-shopping-cart"></i> <span class="d-none d-md-inline-block">Корзина</span>
                                <span class="badge badge-success hide small-cart-counter">0</span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top bar end-->

    @include('client.partials.login-modal')

    @include('client.partials.menu')
