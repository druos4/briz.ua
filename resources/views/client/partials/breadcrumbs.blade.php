@if(!empty($breadcrumbs))

    <div id="heading-breadcrumbs" class="border-top-0 border-bottom-0">
        <div class="container">
            <div class="row d-flex align-items-center flex-wrap">
                <div class="col-md-7">
                    <h1 class="h2">{{ $breadcrumbs[array_key_last($breadcrumbs)]['title'] }}</h1>
                </div>
                <div class="col-md-5">
                    <ul class="breadcrumb d-flex justify-content-end">
                        <li class="breadcrumb-item"><a href="{{ getLocaleHrefPrefix() }}/">Главная</a></li>
                        @foreach($breadcrumbs as $key => $item)
                            @if($key == array_key_last($breadcrumbs))
                                <li class="breadcrumb-item active">{{ $item['title'] }}</li>
                            @else
                                <li class="breadcrumb-item"><a href="{{ getLocaleHrefPrefix() }}{{ $item['url'] }}">{{ $item['title'] }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
