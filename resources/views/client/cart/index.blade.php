@extends('layouts.client')
@section('title')
    Корзина
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('styles')
@endsection
@section('content')

    <div id="content">
        <div class="container">
            <div class="row bar">

                <div id="basket" class="col-lg-12">
                    <div class="box mt-0 pb-0 no-horizontal-padding">
                        <form method="get" action="shop-checkout1.html">
                            <div class="table-responsive">
                                @if(!empty($items))


                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Товар</th>
                                        <th>Кол-во</th>
                                        <th>Цена</th>
                                        <th colspan="2">Сумма</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($items as $item)
                                        @include('client.cart.big-cart-item',['item' => $item])
                                    @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="4">Всего</th>
                                        <th colspan="2"><span id="bigcart-total">{{ $summ }}</span> грн.</th>
                                    </tr>
                                    </tfoot>
                                </table>

                                @else

                                    Ваша корзина пуста

                                @endif
                            </div>
                            <div class="box-footer d-flex justify-content-between align-items-center">
                                <div class="left-col">
                                    <a href="{{ getLocaleHrefPrefix() }}/shop" class="btn btn-secondary mt-0"><i class="fa fa-chevron-left"></i> Продолжить покупки</a>
                                </div>
                                <div class="right-col">
                                    @if(!empty($items))
                                        <a href="{{ getLocaleHrefPrefix() }}/order" class="btn btn-template-outlined">Оформить заказ <i class="fa fa-chevron-right"></i></a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div>


@endsection
@section('scripts')
@endsection
