<div class="row" id="cart-row-{{ $item->id }}">
    <div class="col-md-2">
        @if($item->product->picture != '')
            <img src="{{ $item->product->picture }}" alt="{{ $item->product->{'title' . getLocaleDBSuf()} }}" class="img-fluid">
        @endif
    </div>
    <div class="col-md-4">
        <a href="">{{ $item->product->{'title' . getLocaleDBSuf()} }}</a>
    </div>
    <div class="col-md-3">
        {{ $item->price }}грн <input type="number" min="1" max="999" class="form-control cart-item-qnt" data-id="{{ $item->id }}" value="{{ $item->qnt }}"
                                     style="width:70px; display:inline-block; margin-left:10px;">
    </div>
    <div class="col-md-3" style="text-align: right;">
        <span id="cart-item-total-{{ $item->id }}">{{ $item->total }}</span>грн <a href="" class="btn-cart-del-item" data-id="{{ $item->id }}" title="Удалить товар" style="margin-left:10px;"><i class="fa fa-trash"></i></a>
    </div>
</div>
