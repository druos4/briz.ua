@if(!empty($items))
    @foreach($items as $item)
        @include('client.cart.small-cart-item', ['item' => $item])
    @endforeach
@endif
<div class="row" style="border-top:1px solid #E1E1E1; padding-top:10px;">
    <div class="col-md-6">
        <a class="btn btn-secondary btn-cart-continue" href="#" role="button">Продолжить покупки</a>
    </div>
    <div class="col-md-6" style="text-align: right;">
        Сумма: <span id="small-cart-summ">{{ $summ }}</span> грн<br />
        <a class="btn btn-success btn-cart-make-order" href="{{ getLocaleHrefPrefix() }}/order" role="button">Оформить заказ</a>
    </div>
</div>

