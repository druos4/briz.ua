<tr id="bigcart-item-{{ $item->id }}">
    <td>
        @if($item->product->picture != '')
            <a href="{{ getLocaleHrefPrefix() }}/shop/{{ $item->product->category->slug }}/{{ $item->product->slug }}">
                <img src="{{ $item->product->picture }}" alt="{{ $item->product->{'title' . getLocaleDBSuf()} }}" class="img-fluid">
            </a>
        @endif
    </td>
    <td><a href="{{ getLocaleHrefPrefix() }}/shop/{{ $item->product->category->slug }}/{{ $item->product->slug }}">{{ $item->product->{'title' . getLocaleDBSuf()} }}</a></td>
    <td>
        <input id="big-item-qnt-{{ $item->id }}" type="number" min="1" max="999" value="{{ $item->qnt }}" class="form-control bigcart-item-qnt" data-id="{{ $item->id }}">
    </td>
    <td>
        {{ $item->price }} грн.
    </td>
    <td>
        <span id="bigcart-item-total-{{ $item->id }}">{{ $item->total }}</span> грн.
    </td>
    <td>
        <a href="" class="bigcart-item-delete" data-id="{{ $item->id }}"><i class="fa fa-trash-o"></i></a>
    </td>
</tr>
