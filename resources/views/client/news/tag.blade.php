@extends('layouts.client')
@section('title')
    Новости
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('styles')
@endsection
@section('content')

    <div id="content">
        <div class="container">
            <div class="row bar">

                <div id="blog-listing-small" class="col-lg-9">

                    <div class="row">
                        @if(!empty($news))
                            @foreach($news as $item)

                                <div class="col-lg-4 col-md-6">
                                    <div class="home-blog-post">
                                        <div class="image">
                                            @if($item->picture != '')
                                                <a href="{{ getLocaleHrefPrefix() }}/news/{{ $item->slug }}">
                                                    <img src="{{ $item->picture }}" alt="{{ $item->{'title' . getLocaleDBSuf()} }}" class="img-fluid">
                                                </a>
                                            @endif
                                            {{--<div class="overlay d-flex align-items-center justify-content-center"><a href="blog-post.html" class="btn btn-template-outlined-white"><i class="fa fa-chain"> </i> Read More</a></div>--}}
                                        </div>
                                        <div class="text">
                                            <h4><a href="{{ getLocaleHrefPrefix() }}/news/{{ $item->slug }}">{{ $item->{'title' . getLocaleDBSuf()} }}</a></h4>
                                            {{--<p class="intro">Fifth abundantly made Give sixth hath. Cattle creature i be don't them behold green moved fowl Moved life us beast good yielding. Have bring.</p><a href="blog-post.html" class="btn btn-template-outlined">Continue Reading</a>--}}
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        @endif
                    </div>


                    <div class="row">
                        <div class="md-12">
                            {{ $news->appends(request()->except('page'))->links() }}
                        </div>
                    </div>

                </div>

                <div class="col-lg-3">
                    @if(!empty($tags))
                        <div class="panel sidebar-menu">
                            <div class="panel-heading">
                                <h3 class="h4 panel-title">Теги</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="tag-cloud list-inline">
                                    @foreach($tags as $tag)
                                        <li class="list-inline-item @if($tag->id == $tag_selected->id) active @endif">
                                            <a href="{{ getLocaleHrefPrefix() }}/news/tag/{{ $tag->code }}">
                                                <i class="fa fa-tags"></i> {{ $tag->{'title' . getLocaleDBSuf()} }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
@endsection
