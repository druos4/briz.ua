@extends('layouts.client')
@section('title')
    Новости
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('styles')
@endsection
@section('content')

    <div id="content">
        <div class="container">
            <div class="row bar">

                <div id="blog-post" class="col-md-9">
                    <p class="text-muted text-uppercase mb-small text-right text-sm">{{ $news->show_from }}</p>
                    <div id="post-content">
                        <?=$news->{'detail' . getLocaleDBSuf()}?>
                    </div>
                    <div class="post-tags">
                        @if(!empty($news->tags))
                            <ul class="tag-cloud list-inline">
                                @foreach($news->tags as $tag)
                                    <li class="list-inline-item"><a href="{{ getLocaleHrefPrefix() }}/news/tag/{{ $tag->code }}"><i class="fa fa-tags"></i> {{ $tag->{'title' . getLocaleDBSuf()} }}</a></li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>

                <div class="col-md-3">
                    @if(!empty($tags))
                        <div class="panel sidebar-menu">
                            <div class="panel-heading">
                                <h3 class="h4 panel-title">Теги</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="tag-cloud list-inline">
                                    @foreach($tags as $tag)
                                        <li class="list-inline-item"><a href="{{ getLocaleHrefPrefix() }}/news/tag/{{ $tag->code }}"><i class="fa fa-tags"></i> {{ $tag->{'title' . getLocaleDBSuf()} }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
@endsection
