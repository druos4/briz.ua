<div class="row">
    <div class="col-md-4">
        @if($item->product->picture != '')
            <a href="{{ getLocaleHrefPrefix() }}/shop/{{ $item->product->category->slug }}/{{ $item->product->slug }}">
                <img src="{{ $item->product->picture }}" alt="{{ $item->product->{'title' . getLocaleDBSuf()} }}" class="img-fluid">
            </a>
        @endif
    </div>
    <div class="col-md-8">
        <a href="{{ getLocaleHrefPrefix() }}/shop/{{ $item->product->category->slug }}/{{ $item->product->slug }}">{{ $item->product->{'title' . getLocaleDBSuf()} }}</a>
    </div>

    <div class="col-md-4">
        {{ $item->price }} грн.
    </div>
    <div class="col-md-4">
        {{ $item->qnt }} шт.
    </div>
    <div class="col-md-4">
        {{ $item->total }} грн.
    </div>
</div>
