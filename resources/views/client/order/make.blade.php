@extends('layouts.client')
@section('title')
    Оформление заказа
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('styles')
@endsection
@section('content')

    <div id="content">
        <div class="container">
            <div class="row">
                <div id="checkout" class="col-lg-8">
                    <h3>Данные покупателя</h3>
                    <form id="order-form">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="first_name">Фамилия</label>
                                <input id="first_name" name="first_name" type="text" class="form-control" @if($user) value="{{ $user->surname }}" @endif>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="last_name">Имя</label>
                                <input id="last_name" name="last_name" type="text" class="form-control" @if($user) value="{{ $user->name }}" @endif>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="middle_name">Отчество</label>
                                <input id="middle_name" name="middle_name" type="text" class="form-control" @if($user) value="{{ $user->middlename }}" @endif>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="phone">Телефон</label>
                                <input id="phone" name="phone" type="text" class="form-control" @if($user) value="{{ $user->phone }}" @endif>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" max="email" type="email" class="form-control" @if($user) value="{{ $user->email }}" @endif>
                            </div>
                        </div>
                    </div>

                    <hr />

                    <h3>Тип оплаты</h3>
                    @if(!empty($payments))
                        @foreach($payments as $payment)
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="payment_id" id="payment_id" value="{{ $payment->id }}">
                                <label class="form-check-label" for="payment_id">
                                    {{ $payment->{'title' . getLocaleDBSuf()} }}
                                </label>
                            </div>
                        @endforeach
                    @endif

                    <hr />

                    <h3>Тип доставки</h3>
                    @if(!empty($shipments))
                        @foreach($shipments as $shipment)
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="shipment_id" id="shipment_id" value="{{ $shipment->id }}">
                                <label class="form-check-label" for="shipment_id">
                                    {{ $shipment->{'title' . getLocaleDBSuf()} }}
                                </label>
                            </div>
                        @endforeach
                    @endif

                    <hr />

                    <h3>Комменатрий к заказу</h3>
                    <textarea class="form-control" rows="3" name="comments"></textarea>
                    </form>
                </div>
                <div class="col-lg-4">



                        <h3>Состав заказа</h3>

                        @if(!empty($items))
                            @foreach($items as $item)
                                @include('client.order.cart-item',['item' => $item])
                            @endforeach
                        @endif


                        <div class="row">
                            <div class="col-md-6">
                                Итого:
                            </div>
                            <div class="col-md-6">
                                {{ $summ }} грн.
                            </div>
                        </div>



                </div>
            </div>


            <div class="box-footer d-flex flex-wrap align-items-center justify-content-between" style="margin:30px 0px;">
                <div class="left-col"><a href="{{ getLocaleHrefPrefix() }}/cart" class="btn btn-secondary mt-0"><i class="fa fa-chevron-left"></i>Назад в корзину</a></div>
                <div class="right-col">

                    <button type="button" class="btn btn-template-main btn-make-order">Оформить заказ<i class="fa fa-chevron-right"></i></button>
                </div>
            </div>

        </div>
    </div>


@endsection
@section('script')

    <script>
        $('.btn-make-order').click(function (e) {
            e.preventDefault();
            var form = $('#order-form').serialize();
            $.ajax({
                type:'POST',
                url:'/order',
                data:form,
                success:function(data) {

                    if(data.order_id > 0){
                        window.location.href = "{{ getLocaleHrefPrefix() }}/order/" + data.order_id;
                    }

                }
            });
        });

    </script>

@endsection
