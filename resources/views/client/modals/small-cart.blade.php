<div class="modal fade" id="smallCartModal" tabindex="-1" role="dialog" aria-labelledby="smallCartModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="smallCartModalLabel">Корзина</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="small-cart-inner">
                </div>
            </div>
        </div>
    </div>
</div>
