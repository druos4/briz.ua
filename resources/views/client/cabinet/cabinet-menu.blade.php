<div class="panel panel-default sidebar-menu">
    <div class="panel-body">
        <ul class="nav nav-pills flex-column text-sm">
            <li class="nav-item"><a href="{{ getLocaleHrefPrefix() }}/cabinet/orders" class="nav-link {{ request()->is('cabinet/orders*') ? 'active' : '' }}"><i class="fa fa-list"></i> Мои заказы</a></li>
            <li class="nav-item"><a href="{{ getLocaleHrefPrefix() }}/cabinet/profile" class="nav-link {{ request()->is('cabinet/profile*') ? 'active' : '' }}"><i class="fa fa-user"></i> Мой профиль</a></li>
            <li class="nav-item"><a href="" class="nav-link btn-logout"><i class="fa fa-sign-out"></i> Выход</a></li>
        </ul>
    </div>
</div>
