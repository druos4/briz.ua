@extends('layouts.client')
@section('title')
    Мои заказы
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('styles')
@endsection
@section('content')

    <div id="content">
        <div class="container">
            <div class="row bar mb-0">
                <div id="customer-orders" class="col-md-9">

                    <div class="box mt-0 mb-lg-0">
                        <div class="table-responsive">
                            @if(!empty($orders))
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>№ заказа</th>
                                    <th>Дата</th>
                                    <th>Статус</th>
                                    <th>Сумма</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <th>{{ $order->id }}</th>
                                        <td>{{ $order->created_at }}</td>
                                        <td><span class="badge badge-info">{{ $order->status_id }}</span></td>
                                        <td>{{ $order->total }} грн.</td>
                                        <td><a href="{{ getLocaleHrefPrefix() }}/cabinet/orders/{{ $order->id }}" class="btn btn-template-outlined btn-sm">Подробнее</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else

                                У Вас еще нет созданных заказов.

                            @endif
                        </div>
                    </div>

                </div>
                <div class="col-md-3 mt-4 mt-md-0">
                    @include('client.cabinet.cabinet-menu')
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
@endsection
