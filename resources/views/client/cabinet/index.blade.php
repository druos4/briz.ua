@extends('layouts.client')
@section('title')
    Личный кабинет
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('styles')
@endsection
@section('content')

    <div id="content">
        <div class="container">
            <div class="row bar mb-0">
                <div id="customer-orders" class="col-md-9">

                    {{ $user->surname }} {{ $user->name }}

                </div>
                <div class="col-md-3 mt-4 mt-md-0">
                    @include('client.cabinet.cabinet-menu')
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
@endsection
