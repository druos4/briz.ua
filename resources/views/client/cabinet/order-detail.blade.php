@extends('layouts.client')
@section('title')
    Заказ подробно
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('styles')
@endsection
@section('content')

    <div id="content">
        <div class="container">
            <div class="row bar mb-0">
                <div id="customer-orders" class="col-md-9">

                    <p class="lead">Заказ №{{ $order->id }} создан <strong>{{ $order->created_at }}</strong>. Статус: <strong>{{ $order->status_id }}</strong>.</p>
                    <div class="box">
                        <div class="table-responsive">
                            @if(!empty($order->carts))
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2" class="border-top-0">Товар</th>
                                        <th class="border-top-0">Цена</th>
                                        <th class="border-top-0">Кол-во</th>
                                        <th class="border-top-0">Сумма</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($order->carts as $cart)
                                            <tr>
                                                <td>
                                                    @if($cart->product->picture != '')
                                                        <a href="{{ getLocaleHrefPrefix() }}/shop/">
                                                            <img src="{{ $cart->product->picture }}" alt="{{ $cart->product->{'title' . getLocaleDBSuf()} }}" class="img-fluid">
                                                        </a>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="#">{{ $cart->product->{'title' . getLocaleDBSuf()} }}</a>
                                                </td>

                                                <td>
                                                    {{ $cart->price }} грн.
                                                </td>
                                                <td>
                                                    {{ $cart->qnt }} шт.
                                                </td>
                                                <td>
                                                    {{ $cart->total }} грн.
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="4" class="text-right">Итого</th>
                                        <th>
                                            {{ $order->total }} грн.
                                        </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                Оплата: {{ $order->payment->{'title' . getLocaleDBSuf()} }}
                            </div>
                            <div class="col-md-12">
                                Доставка: {{ $order->shipment->{'title' . getLocaleDBSuf()} }}
                            </div>
                        </div>
                    </div>





                </div>
                <div class="col-md-3 mt-4 mt-md-0">
                    @include('client.cabinet.cabinet-menu')
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
@endsection
