<?php
$col_class = 'col-lg-4 col-md-6';
if(isset($cols)){
    if($cols == 4){
        $col_class = 'col-lg-3 col-md-6';
    }
}
?>
<div class="{{ $col_class }}col-lg-4 col-md-6">
    <div class="product">
        <div class="image">
            @if($product->picture != '')
                <a href="{{ getLocaleHrefPrefix() }}/shop/{{ $product->category->slug }}/{{ $product->slug }}">
                    <img src="{{ $product->picture }}" alt="{{ $product->{'title' . getLocaleDBSuf()} }}" class="img-fluid image1">
                </a>
            @endif
        </div>
        <div class="text">
            <h3 class="h5">
                <a href="{{ getLocaleHrefPrefix() }}/shop/{{ $product->category->slug }}/{{ $product->slug }}">{{ $product->{'title' . getLocaleDBSuf()} }}</a>
            </h3>
            <p class="price">
                @if($product->price_old > $product->price) <del>{{ $product->price_old }}</del> @endif {{ $product->price }}грн.
            </p>
            <a href="" class="btn btn-template-main btn-product-add-to-cart" data-id="{{ $product->id }}">
                В корзину
            </a>
        </div>
    </div>
</div>
