@extends('layouts.client')
@section('title')
    {{ $category->{'title' . getLocaleDBSuf()} }}
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('styles')
@endsection
@section('content')

    <div id="content">
        <div class="container">
            <div class="row bar">
                <div class="col-md-3">

                    @include('client.shop.sidebar')


                </div>
                <div class="col-md-9">
                    <div class="row products-sorting">
                        Сортировать по: <select class="bs-select" id="sorting">
                            <option value="new">по новизне</option>
                            <option value="price_asc">по цене вверх</option>
                            <option value="price_desc">по цене вниз</option>
                            <option value="title">по названию</option>
                        </select>
                    </div>


                    <div class="row products products-big" id="category-filtered-products">

                    </div>

                    <div class="pages">
                        <nav class="d-flex justify-content-center" id="category-pagination">
                            <button type="button" class="btn btn-primary" id="btn-load-more">Показать еще</button>
                        </nav>
                        <div class="loader">Loading...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('script')
    <script src="{{ asset('/js/client/sidebar-filter.js') }}"></script>
@endsection
