@if(!empty($categories))
    <div class="panel panel-default sidebar-menu">
        <div class="panel-heading">
            <h3 class="h4 panel-title">Категории</h3>
        </div>
        <div class="panel-body">
            <ul class="nav nav-pills flex-column text-sm category-menu">
                @foreach($categories as $item)
                    <li class="nav-item">
                        <a href="{{ getLocaleHrefPrefix() }}/shop/{{ $item->slug }}" class="nav-link @if($item->id == $category->id) active @endif d-flex align-items-center justify-content-between">
                            <span>{{ $item->{'title' . getLocaleDBSuf()} }} </span><span class="badge badge-secondary">{{ $item->itemsCount() }}</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endif


@include('client.shop.category-filter')
