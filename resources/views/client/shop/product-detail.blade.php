@extends('layouts.client')
@section('title')
    {{ $product->{'title' . getLocaleDBSuf()} }}
@endsection
@section('meta')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
@endsection
@section('styles')
@endsection
@section('content')

    <div id="content">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <div id="productMain" class="row">
                        <div class="col-sm-6">
                            <div data-slider-id="1" class="owl-carousel shop-detail-carousel">
                                @if($product->picture != '')
                                    <div> <img src="{{ $product->picture }}" alt="{{ $product->{'title' . getLocaleDBSuf()} }}" class="img-fluid"></div>
                                @endif
                                @if(!empty($product->gallery))
                                    @foreach($product->gallery as $picture)
                                        <div> <img src="{{ $picture->original }}" alt="{{ $product->{'title' . getLocaleDBSuf()} }}" class="img-fluid"></div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="box">
                                    <p class="price">{{ $product->price }} грн.</p>
                                    <p class="text-center">
                                        <button class="btn btn-template-outlined btn-product-add-to-cart" data-id="{{ $product->id }}"><i class="fa fa-shopping-cart"></i> В корзину</button>
                                        <button data-toggle="tooltip" data-placement="top" title="Добавить в избранное" class="btn btn-default"><i class="fa fa-heart-o"></i></button>
                                    </p>

                            </div>
                            <div data-slider-id="1" class="owl-thumbs">
                                @if($product->picture != '')

                                    <button class="owl-thumb-item"><img src="{{ $product->picture }}" alt="{{ $product->{'title' . getLocaleDBSuf()} }}" class="img-fluid"></button>
                                @endif
                                @if(!empty($product->gallery))
                                    @foreach($product->gallery as $picture)
                                        <button class="owl-thumb-item"><img src="{{ $picture->thumbnail }}" alt="{{ $product->{'title' . getLocaleDBSuf()} }}" class="img-fluid"></button>
                                    @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                    <div id="details" class="box mb-4 mt-4">
                        <?=$product->{'description' . getLocaleDBSuf()}?>
                    </div>

                    @if(!empty($product_properties))
                        <div class="panel-heading">
                            <h3 class="h4 panel-title">Характеристики</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @foreach($product_properties as $property)
                                    @if(!empty($property->values))
                                        <p>{{ $property->property->{'title' . getLocaleDBSuf()} }}:
                                            @foreach($property->values as $value)
                                                <span>{{ $value->{'value' . getLocaleDBSuf()} }}</span>
                                            @endforeach
                                        </p>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif


                    @if(!empty($same_products))
                    <div class="panel-heading">
                        <h3 class="h4 panel-title">Похожие товары</h3>
                    </div>
                    <div class="row">
                        @foreach($same_products as $same_product)
                            @include('client.shop.category-product-item',['product' => $same_product, 'cols' => 4])
                        @endforeach
                    </div>
                    @endif


                </div>

            </div>
        </div>
    </div>


@endsection
@section('script')
    <script src="{{ asset('/js/client/sidebar-filter.js') }}"></script>
@endsection
