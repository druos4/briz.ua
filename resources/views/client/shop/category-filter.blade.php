<?php $filters = getCategoryFilter($category->id); ?>

@if(!empty($filters))
    <form id="sidebar-filter-form">
        <input type="hidden" name="category_id" value="{{ $category->id }}">
        <input type="hidden" id="page" name="page" value="1">
        @foreach($filters as $property_id => $property)
            @if(!empty($property->values))

                <div class="panel panel-default sidebar-menu" id="filter-property-{{ $property_id }}">
                    <div class="panel-heading d-flex align-items-center justify-content-between">
                        <h3 class="h4 panel-title">{{ $property->{'title' . getLocaleDBSuf()} }}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            @foreach($property->values as $value)
                                <div class="checkbox">
                                    <label id="filter-label-{{ $value->id }}">
                                        <input type="checkbox" class="filter-value" name="filter-{{ $property_id }}[]" id="filter-value-{{ $value->id }}" value="{{ $value->id }}"> {{ $value->{'value' . getLocaleDBSuf()} }}
                                        (<span id="filter-count-{{ $value->id }}">
                                        </span>)
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            @endif
        @endforeach
    </form>
@endif
