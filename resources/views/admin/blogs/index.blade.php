@extends('layouts.admin')
@section('title')
    Блог
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .select2{
            /*min-width:150px!important;*/
            width: 200px!important;
        }
    </style>
@endsection
@section('content')
    @can('news_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.blogs.create") }}">
                    <i class="fas fa-plus"></i> Создать Блог
                </a>
            </div>
        </div>
    @endcan

    <div class="card">
        <div class="card-header">
            Фильтр
        </div>

        <div class="card-body">

            <form id="form-filter" action="{{ route("admin.blogs.index") }}" method="GET" enctype="multipart/form-data">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label>ID</label>
                        <input type="number" min="1" class="form-control w-80" name="id" @if(!empty($filter['id'])) value="{{ $filter['id'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>Название</label>
                        <input type="text" class="form-control w-160" name="title" @if(!empty($filter['title'])) value="{{ $filter['title'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>SLUG</label>
                        <input type="text" class="form-control w-160" name="slug" @if(!empty($filter['slug'])) value="{{ $filter['slug'] }}" @endif>
                    </div>

                    <div class="col-auto">
                        <label>Категория</label>
                        <select class="form-control" name="category">
                            <option value="">Все</option>
                            @if(!empty($categories))
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" @if(!empty($filter['category']) && $filter['category'] == $category->id) selected @endif>{{ $category->title_ru }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="col-auto">
                        <label>Активность</label>
                        <select class="form-control" name="active">
                            <option value="">Все</option>
                            <option value="y" @if(isset($filter['active']) && $filter['active'] == 'y') selected @endif>Да</option>
                            <option value="n" @if(isset($filter['active']) && $filter['active'] == 'n') selected @endif>Нет</option>
                        </select>
                    </div>

                    <div class="col-auto">
                        <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/blogs" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <div class="card">
        <div class="card-header">
            Блог
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Картинка</th>
                            <th style="min-width: 150px">
                                Названия
                            </th>
                            <th>SLUG</th>
                            <th style="min-width: 120px">
                                Акт.
                            </th>
                            <th>
                                Категории
                            </th>
                            <th width="130px">
                                Публикация
                            </th>
                            <th>Обновлено</th>
                            <th style="min-width: 60px">ID</th>
                        </tr>
                    </thead>
                <tbody>
                @if(!empty($blogs))
                    @foreach($blogs as $key => $item)
                        <tr id="item-{{ $item->id }}">
                            <td>
                                <div class="btn-group">
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-bars"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/blog/article/{{$item->slug}}" target="_blank" title="Просмотреть на сайте"><i class="fas fa-eye"></i> Просмотр на сайте</a>
                                    @can('news_edit')
                                        <a class="dropdown-item" href="/admin/blogs/{{$item->id}}/edit" role="button" title="Редактировать"><i class="fas fa-pencil-alt"></i> Редактировать</a>
                                    @endcan
                                    <div class="dropdown-divider"></div>
                                    @can('news_delete')
                                        <a class="dropdown-item btn-item-del" href=""
                                           data-id="{{$item->id}}" data-title="{{ $item->title_ru }} [{{ $item->id }}]" data-url="/admin/blogs/{{ $item->id }}"
                                           role="button" title="Удалить"><i class="fas fa-trash"></i> Удалить</a>
                                    @endcan
                                </div>
                            </div>
                            </td>
                            <td>
                                @if(!empty($item->picture)) <img src="{{ $item->picture }}" /> @endif
                            </td>
                            <td>
                                {{ $item->title_ru ?? '' }}
                                <br />{{ $item->title_ua ?? '' }}
                                <br />{{ $item->title_en ?? '' }}
                            </td>
                            <td>
                                {{ $item->slug }}
                            </td>
                            <td>
                                @if($item->is_active == 1)
                                    <span class="badge badge-success">Да</span>
                                @else
                                    <span class="badge badge-danger">Нет</span>
                                @endif
                            </td>
                            <td>
                                @if(!empty($item->categories))
                                    @foreach($item->categories as $category)
                                        @if(!empty($category->category))
                                            <span class="badge badge-info">{{ $category->category->title_ru }}</span><br />
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                {{ $item->published_at }}
                            </td>
                            <td>
                                {{ $item->updated_at }}<br />
                                @if(!empty($item->updater)) {{ $item->updater->surname }} {{ $item->updater->name }} @endif [{{ $item->updated_by }}]
                            </td>
                            <td>
                                {{ $item->id }}
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
 <div>

 {{ $blogs->appends(request()->except('page'))->links() }}

 </div>
 </div>
 </div>


     @section('scripts')
     <script>
         $('.i-sort').click(function (e) {
             e.preventDefault();
             var field = $(this).data('field');
             var direction = $(this).data('direction');
             var form = $('#form-filter').serialize();
             form = form + '&sort=' + field + '&direction=' + direction;
             window.location.href = "/admin/blogs?" + form;
         });
     </script>
     @endsection
 @endsection
