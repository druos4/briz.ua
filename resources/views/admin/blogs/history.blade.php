@extends('layouts.admin')
@section('title')
    Логи изменения новости
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
    </style>
@endsection
@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-secondary" href="{{ route("admin.news.index") }}">
                <i class="fas fa-chevron-left"></i> Список новостей
            </a>
            <a class="btn btn-warning" href="/admin/news/{{ $news->id }}/edit">
                <i class="fas fa-pencil-alt"></i> Редактировать новость
            </a>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Логи изменений новости ID {{ $news->id }}
        </div>

        <div class="card-body">
            @if(!empty($logs))
                <table class="table table-striped">
                    <thead>
                        <th>Дата</th>
                        <th>Автор</th>
                        <th>Изменения</th>
                    </thead>
                    <tbody>
                        @foreach($logs as $log)
                            <tr>
                                <td>
                                    <i class="fas fa-clock"></i> {{ $log->created_at }}
                                </td>
                                <td>
                                    <i class="fas fa-user"></i> {{ $log->author->surname }} {{ $log->author->name }} [{{ $log->author->id }}]
                                </td>
                                <td>
                                    <?=$log->log?>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>


    @section('scripts')

    @endsection
@endsection
