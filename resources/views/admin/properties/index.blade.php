@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')
    @can('category_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.properties.create") }}">
                    <i class="fas fa-plus"></i> Новое свойство
                </a>
            </div>
        </div>
    @endcan
{{--
    <div class="card">
        <div class="card-header">
            Фильтр
        </div>
        <div class="card-body">
            <form id="form-filter" action="/admin/properties" method="GET" class="form-inline">
                {!! csrf_field() !!}

                <div class="filter-item">
                    Категория<br />
                    <select name="category" class="form-control" style="width:300px;">
                        <option value="0">Все</option>
                        @if(isset($categories) && count($categories) > 0)
                            <?php $a0 = 0; ?>
                            @foreach($categories as $key => $category)
                                <?php $a0++; ?>
                                <option value="{{ $category->id }}" @if(isset($filter['category']) && $filter['category'] == $category->id) selected @endif>{{$a0}}. {{ $category->title_ru }}</option>
                                @if(count($category->children))
                                    <?php $a1 = 0; ?>
                                    @foreach($category->children as $child)
                                        <?php $a1++; ?>
                                        <option value="{{ $child->id }}" @if(isset($filter['category']) && $filter['category'] == $child->id) selected @endif>&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title_ru }}</option>
                                        <?php $a2 = 0; ?>
                                        @foreach($child->children as $ch)
                                            <?php $a2++; ?>
                                            <option value="{{ $ch->id }}" @if(isset($filter['category']) && $filter['category'] == $ch->id) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title_ru }}</option>
                                        @endforeach
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>

                <div class="filter-item">
                    Активность<br />
                    <select name="active" class="form-control" style="width:100px;">
                        <option value="">Все</option>
                        <option value="1" @if(isset($filter['active']) && $filter['active'] == 1) selected @endif>Да</option>
                        <option value="0" @if(isset($filter['active']) && $filter['active'] == 0) selected @endif>Нет</option>
                    </select>
                </div>

                <div class="filter-item">
                    Множественное<br />
                    <select name="multi" class="form-control" style="width:110px;">
                        <option value="">Все</option>
                        <option value="1" @if(isset($filter['multi']) && $filter['multi'] == 1) selected @endif>Да</option>
                        <option value="0" @if(isset($filter['multi']) && $filter['multi'] == 0) selected @endif>Нет</option>
                    </select>
                </div>


                <div class="filter-item">
                    Поиск<br />
                    @if(isset($filter['search']) && $filter['search'] != '')
                        <input type="text" name="search" class="form-control" value="{{$filter['search']}}">
                    @else
                        <input type="text" name="search" class="form-control" value="">
                    @endif
                </div>

                <div class="filter-item">
                    ID<br />
                    @if(isset($filter['id']) && $filter['id'] != '')
                        <input type="text" name="id" class="form-control" value="{{$filter['id']}}">
                    @else
                        <input type="text" name="id" class="form-control" value="">
                    @endif
                </div>


                <div class="filter-item"><br />
                    <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                    <a class="btn btn-default" href="/admin/properties?reset=y" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                </div>
            </form>
        </div>
    </div>
--}}
    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Список свойств
        </div>
        <div class="card-body">

        <table class="table table-striped">
            <thead>
                <th width="60"></th>
                <th>Названия</th>
                <th>Активность</th>
                <th>Множественное</th>
                <th>Категории</th>
                <th>Сортировка</th>
                <th>Обновлено</th>
                <th>ID</th>
            </thead>
            <tbody>
                @if(isset($properties) && count($properties) > 0)
                    @foreach($properties as $key => $prop)
                        <tr id="row-{{$prop->id}}">
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="/admin/properties/{{$prop->id}}/edit">Редактировать</a>
                                        <a class="dropdown-item btn-property-del" data-id="{{$prop->id}}" data-title="{{$prop->title_ru}}" href="">Удалить</a>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="/admin/properties/{{$prop->id}}/edit" title="Редактировать">{{ $prop->title_ru }}</a>
                                @if(!empty($prop->title_ua)) <br /><a href="/admin/properties/{{$prop->id}}/edit" title="Редактировать">{{ $prop->title_ua }}</a> @endif
                                @if(!empty($prop->title_en)) <br /><a href="/admin/properties/{{$prop->id}}/edit" title="Редактировать">{{ $prop->title_en }}</a> @endif
                            </td>
                            <td>
                                @if($prop->active == 1)
                                    <span class="badge badge-success">Да</span>
                                @else
                                    <span class="badge badge-danger">Нет</span>
                                @endif
                            </td>
                            <td>
                                @if($prop->multiple == 1)
                                    <span class="badge badge-success">Да</span>
                                @else
                                    <span class="badge badge-danger">Нет</span>
                                @endif
                            </td>
                            <td>
                                @if(isset($prop->categories) && count($prop->categories) > 0)
                                    @foreach($prop->categories as $cat)
                                        <a href="/admin/categories/{{ $cat->id }}/edit" title="Перейти к категории" target="_blank"><span class="badge badge-info">{{ $cat->title_ru }}</span></a>
                                    @endforeach
                                @endif
                            </td>
                            <td>{{ $prop->sort }}</td>
                            <td>
                                {{ $prop->updated_at }}
                            </td>
                            <td>
                                <a href="/admin/properties/{{$prop->id}}/edit" title="Редактировать">{{ $prop->id }}</a>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>

        </div>
    </div>
    @section('scripts')
        @parent



        <!-- Modal -->
        <div class="modal fade" id="propertyDelModal" tabindex="-1" role="dialog" aria-labelledby="propertyDelModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="categoryDelModalLabel">Удалить свойство</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="del-id" value="0">
                        <p>Действительно удалить свойство &laquo;<span id="del-title"></span>&raquo;?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <button type="button" class="btn btn-danger btn-property-delete">Удалить</button>
                    </div>
                </div>
            </div>
        </div>





        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.btn-property-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');
                $('#del-id').val(id);
                $('#del-title').html(title);
                $('#propertyDelModal').modal('show');
            });

            $('.btn-property-delete').click(function(e) {
                e.preventDefault();
                var id = $('#del-id').val();
                $.ajax({
                    type:'POST',
                    url:'/admin/properties-delete',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){
                            $('#row-' + data.id).remove();
                            $('#propertyDelModal').modal('hide');
                        }
                    }
                });
            });






        </script>


    @endsection
@endsection
