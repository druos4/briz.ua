@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')
    <a class="btn btn-default" href="{{ route('admin.properties.index') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a>
    <div class="card">
        <div class="card-header bg-info">
            Редактировать свойство
        </div>

        <div class="card-body">
            <form action="{{ route('admin.properties.update',$property->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                            <label for="title_ru">Название свойства RU*</label>
                            <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru', isset($property) ? $property->title_ru : '') }}">
                            @if($errors->has('title_ru'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('title_ru') }}
                                </em>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="title_ua">Название свойства UA*</label>
                            <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', isset($property) ? $property->title_ua : '') }}">
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="title_en">Название свойства EN*</label>
                            <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', isset($property) ? $property->title_en : '') }}">
                        </div>


                        <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                            <label for="slug">SLUG свойства*</label>
                            <a href="" class="btn-make-slug">Сгенерировать SLUG<i class="fas fa-arrow-down"></i></a>
                            <input type="text" id="slug" name="slug" class="form-control" required value="{{ old('slug', isset($property) ? $property->slug : '') }}">
                            @if($errors->has('slug'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('slug') }}
                                </em>
                            @endif
                        </div>

                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <div class="toggle-label">Активность</div>
                            <div class="toggle-btn @if($property->active == 1) active @endif">
                                <input type="checkbox" class="cb-value" name="active" value="1" @if($property->active == 1) checked @endif />
                                <span class="round-btn"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="sort">Сортировка</label>
                            <input type="number" id="sort" name="sort" class="form-control" min="1" max="99999" value="{{ old('sort', isset($property) ? $property->sort : '100') }}">
                        </div>


                        <div class="form-group">
                            <div class="toggle-label">Множественный выбор значений</div>
                            <div class="toggle-btn @if($property->multiple == 1) active @endif">
                                <input type="checkbox" class="cb-value" name="multiple" value="1" @if($property->multiple == 1) checked @endif />
                                <span class="round-btn"></span>
                            </div>
                        </div>

                    </div>

                </div>
                <input class="btn btn-success" type="submit" value="Сохранить">
            </form>


            <div class="row">
                 <div class="col-md-12">
                        <hr />
                        <form action="/admin/property-value-save" id="new-prop" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="prop_id" value="{{ $property->id }}">
                            <div class="form-group">
                                <label for="slug">Новое значение свойства</label><br />
                                <input type="hidden" id="value_id" name="value_id" value="0">
                                <span>RU:</span> <input type="text" id="value_ru" name="value_ru" class="form-control" value="" style="display: inline-block; width:300px;">
                                <span>UA:</span> <input type="text" id="value_ua" name="value_ua" class="form-control" value="" style="display: inline-block; width:300px;">
                                <span>EN:</span> <input type="text" id="value_en" name="value_en" class="form-control" value="" style="display: inline-block; width:300px;">


                                {{--<br /><span>Meta keyword:</span> <input type="text" id="meta_keyword" name="meta_keyword" class="form-control" value="" style="display: inline-block; width:300px;">--}}
                                <span>Сортировка:</span> <input type="number" id="value_sort" name="value_sort" class="form-control" min="1" max="99999" value="100" style="display: inline-block; width:100px;">
                                {{--<span>Иконка:</span> <input type="file" id="value_icon" name="value_icon" style="display: inline-block;">
                                <div id="div-icon" style="display:inline-block;"></div>--}}

                                <input class="btn btn-info btn-save-value" type="submit" value="Сохранить значение">
                            </div>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <div class="alert-inner"></div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </form>

                        <table class="table table-striped">
                            <thead>
                            <th>Значение RU</th>
                            <th>Значение UA</th>
                            <th>Значение EN</th>
                            <th>Slug</th>
                            <th>Сортировка</th>
                            {{--<th>Иконка</th>--}}
                            <th></th>
                            <th></th>
                            </thead>
                            <tbody id="prop-values">
                                @if(isset($values) && count($values) > 0)
                                    @foreach($values as $key => $val)
                                        <tr id="row-{{ $val->id }}">
                                            <td>{{ $val->value_ru }}</td>
                                            <td>{{ $val->value_ua }}</td>
                                            <td>{{ $val->value_en }}</td>

                                            <td>{{ $val->slug }}</td>
                                            <td>{{ $val->sort }}</td>
                                            {{--<td>@if(isset($val->icon) && $val->icon != '') <img src="{{ $val->icon }}" style="max-width:60px; msx-height:60px;
"> @endif</td>--}}
                                            <td>
                                                <a class="btn btn-warning btn-value-edit" href="" role="button" title="Редактировать"
                                                   data-id="{{$val->id}}"
                                                   data-value_ru="{{$val->value_ru}}" data-value_ua="{{$val->value_ua}}" data-value_en="{{$val->value_en}}"
                                                   data-slug="{{$val->slug}}"
                                                    data-icon="{{$val->icon}}"><i class="fas fa-pen"></i></a>
                                            </td>
                                            <td>
                                                <a class="btn btn-danger btn-val-del" data-id="{{$val->id}}" href="" role="button" title="Удалить"><i class="fas fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>

                    </div>
                </div>
        </div>
    </div>




@endsection
@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.alert-danger').hide();

        $('.btn-save-value').click(function (e) {
            e.preventDefault();
            $('.alert-danger').hide();
            $('.alert-inner').html('');
            var valueru = $('#value_ru').val();
            var valueua = $('#value_ua').val();
            var valueen = $('#value_en').val();
            var valuesort = parseInt($('#value_sort').val());
            var errors = '';
            if(valueru == ''){
                errors = errors + 'Заполните значение *RU*<br />';
            }
            if(valueua == ''){
                errors = errors + 'Заполните значение *UA*<br />';
            }
            if(valueen == ''){
                errors = errors + 'Заполните значение *EN*<br />';
            }
            if(valuesort < 1 || $('#value_sort').val() == ''){
                errors = errors + 'Укажите корректное значение сортировки от 1 до 99999<br />';
            }

            if(errors != ''){
                $('.alert-inner').html(errors);
                $('.alert-danger').show();
            } else {
                $('#new-prop').submit();
            }
        });

        function initBtns(){
            $('.btn-val-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    type:'POST',
                    url:'/admin/properties-delete-value',
                    data:{id:id},
                    success:function(data) {
                        $('#row-' + data.id).remove();
                    }
                });
            });


            $('.btn-value-edit').click(function(e) {
                e.preventDefault();
                var value_id = $(this).data('id');
                var value_ru = $(this).data('value_ru');
                var value_ua = $(this).data('value_ua');
                var value_en = $(this).data('value_en');
                var value_sort = $(this).data('value_sort');
               // var meta_keyword = $(this).data('meta_keyword');
                var icon = $(this).data('icon');

                $('#value_id').val(value_id);
                $('#value_ru').val(value_ru);
                $('#value_ua').val(value_ua);
                $('#value_en').val(value_en);
                $('#value_sort').val(value_sort);
                //$('#meta_keyword').val(meta_keyword);
                $('#div-icon').html('<img src="' + icon + '" style=max-width:60px; max-height:60px;> <input type="checkbox" name="del-icon" value="1">удалить иконку ');
                $('#div-icon').show();
            });


            $('.prop-value-add').click(function(e) {
                e.preventDefault();
                var value_id = $('#value_id').val();
                var property_id = '{{ $property->id }}';
                var value_ru = $('#value_ru').val();
                var value_ua = $('#value_ua').val();
                var value_en = $('#value_en').val();
                var value_sort = $('#value_sort').val();
               // var meta_keyword = $('#meta_keyword').val();
                var value_icon = $('#value_icon').val();

                $('#value_id').val(0);
                $('#value_ru').val('');
                $('#value_ua').val('');
                $('#value_en').val('');
                $('#value_sort').val('');
                //$('#meta_keyword').val('');
                $('#value_icon').val('');

                if(title == ''){
                    alert('Заполните значение свойства!');
                } else {

                    $.ajax({
                        type:'POST',
                        url:'/admin/properties-add-value',
                        contentType: false,
                        cache: false,
                        processData:false,
                        //data:{value_id:value_id,property_id:property_id,value:value,value_ua:value_ua,value_en:value_en,meta_keyword:meta_keyword,sort:value_sort,value_icon:value_icon},
                        data:$('#new-prop').serialize(),
                        success:function(data) {
                            if(data.upd == 1){

                                $('#row-' + data.id).remove();
                                $('#prop-values').append(data.html);
                                initBtns();

                            } else if(data.new){

                                $('#prop-values').append(data.html);
                                initBtns();

                            }
                        }
                    });
                }
            });
        }

        initBtns();

    </script>
@endsection
