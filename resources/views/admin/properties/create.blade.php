@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
    </style>
@endsection
@section('content')
<a class="btn btn-default" href="{{ route('admin.properties.index') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a>
<div class="card">
    <div class="card-header bg-info">
        Новое свойство
    </div>

    <div class="card-body">
        <form action="{{ route("admin.properties.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                        <label for="title_ru">Название свойства RU*</label>
                        <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru', isset($property) ? $property->title_ru : '') }}">
                        @if($errors->has('title_ru'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title_ru') }}
                            </em>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="title_ua">Название свойства UA*</label>
                        <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', isset($property) ? $property->title_ua : '') }}">
                    </div>

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="title_en">Название свойства EN*</label>
                        <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', isset($property) ? $property->title_en : '') }}">
                    </div>

                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                        <label for="slug">SLUG свойства*</label>
                        <a href="" class="btn-make-slug">Сгенерировать SLUG<i class="fas fa-arrow-down"></i></a>
                        <input type="text" id="slug" name="slug" class="form-control" required value="{{ old('slug', isset($property) ? $property->slug : '') }}">
                        @if($errors->has('slug'))
                            <em class="invalid-feedback">
                                {{ $errors->first('slug') }}
                            </em>
                        @endif
                    </div>

                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div class="toggle-label">Активность</div>
                        <div class="toggle-btn">
                            <input type="checkbox" class="cb-value" name="active" value="1" />
                            <span class="round-btn"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="sort">Сортировка</label>
                        <input type="number" id="sort" name="sort" class="form-control" min="1" max="99999" value="{{ old('sort', isset($property) ? $property->sort : '100') }}">
                    </div>

                    <div class="form-group">
                        <div class="toggle-label">Множественный выбор значений</div>
                        <div class="toggle-btn">
                            <input type="checkbox" class="cb-value" name="multiple" value="1" />
                            <span class="round-btn"></span>
                        </div>
                    </div>


                </div>
            </div>

            <div style="margin-top:50px;">
                <hr />
                <input class="btn btn-success" type="submit" value="Сохранить">
            </div>
        </form>
    </div>
</div>



@endsection
@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


    </script>
@endsection
