@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <style>
        .alert-block{
            display:none;
        }
    </style>
@endsection
@section('content')

<div class="card">
    <div class="card-header">
        Редактирование страницы
    </div>

    <div class="card-body">
        <form action="{{ route("admin.pages.update", [$page->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')


            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основное</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="true">Подробно</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru', ($page->title_ru) ? $page->title_ru : '') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ua">Заголовок UA*</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', ($page->title_ua) ? $page->title_ua : '') }}">
                                @if($errors->has('title_ua'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_en">Заголовок EN*</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', ($page->title_en) ? $page->title_en : '') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                <label for="slug">URL*</label>
                                <input type="text" id="slug" name="slug" class="form-control" required value="{{ old('slug', ($page->slug) ? $page->slug : '') }}">
                                @if($errors->has('slug'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('slug') }}
                                    </em>
                                @endif
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="meta_title_ru">Meta title RU</label>
                                <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru', ($page->meta_title_ru) ? $page->meta_title_ru : '') }}">
                            </div>
                            <div class="form-group">
                                <label for="meta_title">Meta title UA</label>
                                <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua', ($page->meta_title_ua) ? $page->meta_title_ua : '') }}">
                            </div>
                            <div class="form-group">
                                <label for="meta_title_en">Meta title EN</label>
                                <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en', ($page->meta_title_en) ? $page->meta_title_en : '') }}">
                            </div>
                            <hr />
                            <div class="form-group">
                                <label for="meta_description_ru">Meta description RU</label>
                                <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru', ($page->meta_description_ru) ? $page->meta_description_ru : '') }}">
                            </div>
                            <div class="form-group">
                                <label for="meta_description">Meta description UA</label>
                                <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua', ($page->meta_description_ua) ? $page->meta_description_ua : '') }}">
                            </div>
                            <div class="form-group">
                                <label for="meta_description_en">Meta description EN</label>
                                <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en', ($page->meta_description_en) ? $page->meta_description_en : '') }}">
                            </div>
                            <hr />
                            <div class="form-group">
                                <label for="meta_keywords_ru">Meta keywords RU</label>
                                <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru', ($page->meta_keywords_ru) ? $page->meta_keywords_ru : '') }}">
                            </div>
                            <div class="form-group">
                                <label for="meta_keywords">Meta keywords UA</label>
                                <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua', ($page->meta_keywords_ua) ? $page->meta_keywords_ua : '') }}">
                            </div>
                            <div class="form-group">
                                <label for="meta_keywords_en">Meta keywords EN</label>
                                <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en', ($page->meta_keywords_en) ? $page->meta_keywords_en : '') }}">
                            </div>

                        </div>
                    </div>

                </div>
                <div class="tab-pane fade show" id="detail" role="tabpanel" aria-labelledby="detail-tab">

                    <label for="detail_ru">Подробный текст RU</label>
                    <textarea name="detail_ru" id="detail_ru" class="form-control" rows="12"><?=$page->detail_ru?></textarea>
                    <hr />
                    <label for="detail_ua">Подробный текст UA</label>
                    <textarea name="detail_ua" id="detail_ua" class="form-control" rows="12"><?=$page->detail_ua?></textarea>
                    <hr />
                    <label for="detail_en">Подробный текст EN</label>
                    <textarea name="detail_en" id="detail_en" class="form-control" rows="12"><?=$page->detail_en?></textarea>

                </div>
            </div>






            <div>
                <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/pages" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>

@endsection
@section('scripts')
    <script>
        CKEDITOR.replace( 'detail_ru' );
        CKEDITOR.replace( 'detail_ua' );
        CKEDITOR.replace( 'detail_en' );
    </script>
@endsection
