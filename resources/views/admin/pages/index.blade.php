@extends('layouts.admin')
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table-responsive{
            padding-bottom: 50px;
        }

    </style>
@endsection
@section('content')
    @can('page_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.pages.create") }}">
                    <i class="fas fa-plus"></i> Создать страницу
                </a>
            </div>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            Фильтр
        </div>

        <div class="card-body">

            <form action="{{ route("admin.pages.index") }}" method="GET" enctype="multipart/form-data" class="form-inline">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label>ID</label>
                        <input type="number" min="1" class="form-control w-80" name="id" @if(!empty($filter['id'])) value="{{ $filter['id'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>Название</label>
                        <input type="text" class="form-control w-160" name="title" @if(!empty($filter['title'])) value="{{ $filter['title'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>URL</label>
                        <input type="text" class="form-control w-160" name="slug" @if(!empty($filter['slug'])) value="{{ $filter['slug'] }}" @endif>
                    </div>
{{--
                    <div class="col-auto">
                        <label>Активность</label>
                        <select class="form-control" name="active">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['active']) && $filter['active'] == 1) selected @endif>Да</option>
                            <option value="0" @if(isset($filter['active']) && $filter['active'] == 0) selected @endif>Нет</option>
                        </select>
                    </div>
--}}
                    <div class="col-auto">
                        <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/pages?reset=y" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <div class="card">
        <div class="card-header">
            Страницы
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Название RU</th>
                            <th>Название UA</th>
                            <th>Название EN</th>
                            <th>URL</th>
                            <th>Обновлено</th>
                            <th>ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pages as $key => $page)
                            <tr id="item-{{ $page->id }}">
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="{{$page->slug}}" target="_blank"><i class="fas fa-eye"></i> Просмотреть на сайте</a>
                                            @can('news_edit')
                                                <a class="dropdown-item" href="/admin/pages/{{$page->id}}/edit"><i class="fas fa-pencil-alt"></i> Редактировать</a>
                                            @endcan
                                            @can('news_delete')
                                                <a class="dropdown-item btn-item-del" href="" target="_blank" data-id="{{$page->id}}" data-title="{{ $page->title_ru }} [{{ $page->id }}]" data-url="/admin/pages/{{ $page->id }}"><i class="fas fa-trash"></i> Удалить</a>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{ $page->title_ru ?? '' }}
                                </td>
                                <td>
                                    {{ $page->title_ua ?? '' }}
                                </td>
                                <td>
                                    {{ $page->title_en ?? '' }}
                                </td>
                                <td>
                                    {{ $page->slug }}
                                </td>

                                <td>
                                    {{ $page->updated_at }}<br />
                                    @if(!empty($page->updater)) {{ $page->updater->surname }} {{ $page->updater->name }} [{{ $page->updater->id }}] @else [{{ $page->updated_by }}] @endif;
                                </td>
                                <td>
                                    {{ $page->id }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div>
                {{ $pages->appends(request()->except('page'))->links() }}
            </div>

        </div>
    </div>


    @section('scripts')

    @endsection
@endsection
