@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .sync-alert{
            position: fixed;
            z-index: 999;
            top:50%;
            left:50%;
            margin: -54px 0px 0px -210px;
            text-align: center;
            display:none;
        }
        .sync-alert-done{
            position: fixed;
            z-index: 999;
            top:50%;
            left:50%;
            margin: -62px 0px 0px -136px;
            text-align: center;
            display:none;
        }

        .sync-alert-prods, .sync-alert-prods-done{
            position: fixed;
            z-index: 999;
            top:50%;
            left:50%;
            margin: -62px 0px 0px -136px;
            text-align: center;
            display:none;
        }
        .cat-pic{
            max-height: 40px;
            max-width: 50px;
        }
    </style>
@endsection
@section('content')
    @can('category_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.categories.create") }}">
                    <i class="fas fa-plus"></i> Новая категория
                </a>
                <a href="#" id="saveOrder" class="btn btn-primary"><i class="fas fa-save"></i> Сохранить порядок категорий</a>
            </div>
        </div>
    @endcan
    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Список категорий
        </div>

        <div class="dd" id="nestable">
            <ol class="dd-list">

                @if(isset($categories) && count($categories) > 0)
                    @foreach($categories as $key => $category)
                        <li class="dd-item" id="cat-{{ $category->id }}" data-id="{!! $category->id !!}">
                            <div class="dd-handle">
                                @if($category->active)
                                    <i class="fas fa-eye"></i>
                                @else
                                    <i class="fas fa-eye-slash"></i>
                                @endif
                                &nbsp;&nbsp;
                                @if(!empty($category->picture)) <img src="{{ $category->picture }}" class="cat-pic">@endif {{ $category->title_ru }}
                                <div class="pull-right action-buttons">
                                    <a href="/equipment/{{$category->slug}}" id="" class="btn btn-xs btn-info" title="Посмотреть на сайте" target="_blank"><i class="fas fa-share"></i></a>
                                    <a href="{!! route('admin.categories.edit',$category->id) !!}" id="" class="btn btn-xs btn-warning" title="Редактировать"><i class="fas fa-pen"></i></a>
                                    <a href="" id="" class="btn btn-xs btn-danger btn-cat-del" data-id="{{$category->id}}" data-title="{{$category->title_ru}}" title="Удалить"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>

                            @if(count($category->children))
                                <ol class="dd-list">
                                    @foreach($category->children as $child)
                                        <li class="dd-item" id="cat-{{ $child->id }}" data-id="{{ $child->id }}">
                                            <div class="dd-handle">
                                                @if($category->active)
                                                    <i class="fas fa-eye"></i>
                                                @else
                                                    <i class="fas fa-eye-slash"></i>
                                                @endif
                                                    &nbsp;&nbsp;
                                                    @if(!empty($child->picture)) <img src="{{ $child->picture }}" class="cat-pic">@endif {{ $child->title_ru }}
                                                    <div class="pull-right action-buttons">
                                                        <a href="/equipment/{{$child->slug}}" id="" class="btn btn-xs btn-info" title="Посмотреть на сайте" target="_blank"><i class="fas fa-share"></i></a>
                                                        <a href="{!! route('admin.categories.edit',$child->id) !!}" id="" class="btn btn-xs btn-warning" title="Редактировать"><i class="fas fa-pen"></i></a>
                                                        <a href="" id="" class="btn btn-xs btn-danger btn-cat-del" data-id="{{$child->id}}" data-title="{{$child->title_ru}}" title="Удалить"><i class="fas fa-trash"></i></a>
                                                    </div>

                                            </div>
                                            <ol>
                                                @foreach($child->children as $ch)
                                                    <li class="dd-item" id="cat-{{ $ch->id }}" data-id="{{ $ch->id }}">
                                                        <div class="dd-handle">
                                                            @if($category->active)
                                                                <i class="fas fa-eye"></i>
                                                            @else
                                                                <i class="fas fa-eye-slash"></i>
                                                            @endif
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                            {{ $ch->title_ru }}
                                                                <div class="pull-right action-buttons">
                                                                    <a href="/equipment/{{$ch->slug}}" id="" class="btn btn-xs btn-info" title="Посмотреть на сайте" target="_blank"><i class="fas fa-share"></i></a>
                                                                    <a href="{!! route('admin.categories.edit',$ch->id) !!}" id="" class="btn btn-xs btn-warning" title="Редактировать"><i class="fas fa-pen"></i></a>
                                                                    <a href="" id="" class="btn btn-xs btn-danger btn-cat-del" data-id="{{$ch->id}}" data-title="{{$ch->title_ru}}" title="Удалить"><i class="fas fa-trash"></i></a>

                                                                </div>

                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ol>
                                        </li>
                                    @endforeach
                                </ol>
                            @endif
                        </li>
                    @endforeach
                @endif
            </ol>
        </div>
    </div>



    @if(!empty($all_categories) && count($all_categories) > 0)
    <div class="card">
        <div class="card-header">
            Проблемные категории
        </div>

        <div class="dd">
            <ol class="dd-list">

                @if(isset($all_categories) && count($all_categories) > 0)
                    @foreach($all_categories as $key => $category)
                        <li class="dd-item" id="cat-{{ $category->id }}" data-id="{!! $category->id !!}">
                            <div class="dd-handle">
                                {{ $category->title_ru }}
                                <div class="pull-right action-buttons">
                                    <a href="{!! route('admin.categories.edit',$category->id) !!}" id="" class="btn btn-xs btn-warning" title="Редактировать"><i class="fas fa-pen"></i></a>
                                    <a href="" id="" class="btn btn-xs btn-danger btn-cat-del" data-id="{{$category->id}}" data-title="{{$category->title_ru}}" title="Удалить"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                @endif
            </ol>
        </div>
    </div>
    @endif

    @section('scripts')
        @parent


        <div class="alert alert-warning sync-alert" role="alert">
            Выполняется процесс синхронизации цен и остатков<br />
            для категории &laquo;<span id="span-title"></span>&raquo;.<br />
            Пожалуйста, не закрывайте данное окно до его окончания!<br />
            <img src="/img/loading4.gif" style="max-width:40px; max-height:40px;" />
        </div>

        <div class="alert alert-info sync-alert-done" role="alert">
            Процесс синхронизации для<br />
            категории &laquo;<span id="span-title-done"></span>&raquo; завершен!<br />
            Обновлены цены для <span id="span-sync-price">0</span> товаров.<br />
            Обновлены остатки для <span id="span-sync-stock">0</span> товаров.<br />
            <a class="btn btn-primary btn-alert-close" href="" role="button">Закрыть</a>
        </div>


        <div class="alert alert-warning sync-alert-prods" role="alert">
            Выполняется процесс синхронизации номенклатуры.<br />
            Пожалуйста, не закрывайте данное окно до его окончания!<br />
            <img src="/img/loading4.gif" style="max-width:40px; max-height:40px;" />
        </div>

        <div class="alert alert-info sync-alert-prods-done" role="alert">
            Процесс синхронизации номенклатуры завершен!<br />
            Создано <span id="span-sync-new-prods">0</span> новых товаров.<br />
            <a class="btn btn-primary btn-alert-close" href="" role="button">Закрыть</a>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="categoryDelModal" tabindex="-1" role="dialog" aria-labelledby="categoryDelModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="categoryDelModalLabel">Удалить категорию</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="del-cat-id" value="0">
                        <p>Действительно удалить категорию &laquo;<span id="del-cat-title"></span>&raquo;?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <button type="button" class="btn btn-danger btn-category-delete">Удалить</button>
                    </div>
                </div>
            </div>
        </div>





        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#btn-get-products').click(function(e) {
                e.preventDefault();
                $('.sync-alert-prods').show();
                $.ajax({
                    type:'POST',
                    url:'/admin/make-prods-sync',
                    data:{},
                    success:function(data) {
                        $('.sync-alert-prods').hide();
                        $('#span-sync-new-prods').html(data.new);
                        $('.sync-alert-prods-done').show();
                    }
                });
            });

            $('.btn-cat-sync-1c').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');
                $('#span-title').html(title);
                $('.sync-alert').show();

                $.ajax({
                    type:'POST',
                    url:'/admin/make-cat-sync',
                    data:{id:id,title:title},
                    success:function(data) {
                        $('#span-title-done').html(data.title);
                        $('.sync-alert').hide();
                        if(data.price != ''){
                            $('#span-sync-price').html(data.price);
                        }
                        if(data.stock != ''){
                            $('#span-sync-stock').html(data.stock);
                        }
                        $('.sync-alert-done').show();

                    }
                });
            });

            $('.btn-alert-close').click(function(e) {
                e.preventDefault();
                $('.sync-alert').hide();
                $('.sync-alert-done').hide();
                $('.sync-alert-prods-done').hide();
                $('.sync-alert-prods').hide();
            });


            $('.btn-active-toggle').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var active = $(this).data('active');
                $.ajax({
                    type:'POST',
                    url:'/admin/categories-update-active',
                    data:{id:id,active:active},
                    success:function(data) {
                        if(data.id > 0){
                            if(data.active == 1){
                                $('#active-y-'+data.id).show();
                                $('#active-n-'+data.id).hide();
                            } else {
                                $('#active-y-'+data.id).hide();
                                $('#active-n-'+data.id).show();
                            }
                        }
                    }
                });
            });

            $('.btn-cat-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');
                $('#del-cat-id').val(id);
                $('#del-cat-title').html(title);
                $('#categoryDelModal').modal('show');
            });

            $('.btn-category-delete').click(function(e) {
                e.preventDefault();
                var id = $('#del-cat-id').val();
                $.ajax({
                    type:'POST',
                    url:'/admin/categories-delete',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){

                            $('#cat-' + data.id).remove();
                            $('#del-cat-id').val('');
                            $('#del-cat-title').html('');
                            $('#categoryDelModal').modal('hide');
                        }
                    }
                });
            });







        </script>

        <!-- page specific plugin scripts -->
        <script src="{{ asset('adminscripts/jquery.nestable.min.js') }}"></script>

        <!-- inline scripts related to this page -->
        <script type="text/javascript">
            jQuery(function($){
                var nestable,
                    serialized,
                    settings = {maxDepth:3},
                    saveOrder = $('#saveOrder'),
                    edit = $('.edit');

                nestable = $('.dd').nestable(settings);

                saveOrder.on('click', function(e) {
                    e.preventDefault();
                    serialized = nestable.nestable('serialize');

                    $.ajax({
                        method:'POST',
                        url : "/admin/categories-save-sort",
                        data: { _token: "{!! csrf_token() !!}", serialized: serialized }

                    }).done(function (data) {
                        alert("Сохранено!");
                    })
                })

                $('.dd-handle a').on('mousedown', function(e){
                    e.stopPropagation();
                });

                $('[data-rel="tooltip"]').tooltip();

            });
        </script>
    @endsection
@endsection
