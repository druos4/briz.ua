@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <script src="/ckeditor4/ckeditor.js"></script>
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
    </style>
@endsection
@section('content')
<a class="btn btn-default" href="{{ route('admin.categories.index') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a>
<div class="card">
    <div class="card-header bg-info">
        Новая категория
    </div>

    <div class="card-body">
        <form action="{{ route("admin.categories.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-7">
                    <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                        <label for="title_ru">Название категории RU*</label>
                        <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru', isset($category) ? $category->title_ru : '') }}">
                        @if($errors->has('title_ru'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title_ru') }}
                            </em>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                        <label for="title_ua">Название категории UA*</label>
                        <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', isset($category) ? $category->title_ua : '') }}">
                        @if($errors->has('title_ua'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title_ua') }}
                            </em>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                        <label for="title_en">Название категории EN*</label>
                        <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', isset($category) ? $category->title_en : '') }}">
                        @if($errors->has('title_en'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title_en') }}
                            </em>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                        <label for="slug">URL категории*</label>
                        <a href="" class="btn-make-slug">Сгенерировать URL<i class="fas fa-arrow-down"></i></a>
                        <input type="text" id="slug" name="slug" class="form-control" required value="{{ old('slug', isset($category) ? $category->slug : '') }}">
                        @if($errors->has('slug'))
                            <em class="invalid-feedback">
                                {{ $errors->first('slug') }}
                            </em>
                        @endif
                    </div>


                    <div class="form-group">
                        <label for="meta_title_ru">Meta title RU</label>
                        <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru', isset($category) ? $category->meta_title_ru : '') }}">
                        <label for="meta_title_ua">Meta title UA</label>
                        <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua', isset($category) ? $category->meta_title_ua : '') }}">
                        <label for="meta_title_en">Meta title EN</label>
                        <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en', isset($category) ? $category->meta_title_en : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description_ru">Meta description RU</label>
                        <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru', isset($category) ? $category->meta_description_ru : '') }}">
                        <label for="meta_description_ua">Meta description UA</label>
                        <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua', isset($category) ? $category->meta_description_ua : '') }}">
                        <label for="meta_description_en">Meta description EN</label>
                        <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en', isset($category) ? $category->meta_description_en : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords_ru">Meta keywords RU</label>
                        <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru', isset($category) ? $category->meta_keywords_ru : '') }}">
                        <label for="meta_keywords_ua">Meta keywords UA</label>
                        <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua', isset($category) ? $category->meta_keywords_ua : '') }}">
                        <label for="meta_keywords_en">Meta keywords EN</label>
                        <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en', isset($category) ? $category->meta_keywords_en : '') }}">
                    </div>

                </div>
                <div class="col-md-5">

                    <input type="hidden" name="parent_id" value="0">
                    {{--
                    <div class="form-group">
                        <label for="parent_id">Родительская категория</label>
                        <select name="parent_id" id="parent_id" class="form-control">
                            <option value="0">Корневая категория</option>
                            @if(count($categories) > 0)
                                @foreach($categories as $cat)
                                    <option value="{{ $cat->id }}">{{ $cat->title_ru }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    --}}
                    <div class="form-group">
                        <div class="toggle-label">Активность</div>
                        <div class="toggle-btn">
                            <input type="checkbox" class="cb-value" name="active" value="1" />
                            <span class="round-btn"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="picture">Картинка категории</label>
                        <input type="file" class="form-control-file" name="picture" id="picture">
                    </div>


                </div>
            </div>

            <div>
                <input class="btn btn-success" type="submit" value="Сохранить">
            </div>
        </form>
    </div>
</div>



@endsection
@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


    </script>
    <script>
    /*
        CKEDITOR.replace( 'seo_text', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        */
    </script>
@endsection
