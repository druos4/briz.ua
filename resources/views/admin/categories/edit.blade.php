@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <script src="/ckeditor4/ckeditor.js"></script>
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
    </style>
@endsection
@section('content')
<a class="btn btn-default" href="{{ route('admin.categories.index') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a>
<div class="card">
    <div class="card-header bg-info">
        Редактирование категории
    </div>

    <div class="card-body">
        <form action="{{ route('admin.categories.update',$category->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')




            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основные параметры</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Свойства</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="uslugy-tab" data-toggle="tab" href="#uslugy" role="tab" aria-controls="uslugy" aria-selected="false">Услуги</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Название категории RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru', isset($category) ? $category->title_ru : '') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ua">Название категории UA*</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', isset($category) ? $category->title_ua : '') }}">
                                @if($errors->has('title_ua'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_en">Название категории EN*</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', isset($category) ? $category->title_en : '') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                <label for="slug">URL категории*</label>
                                <a href="" class="btn-make-slug">Сгенерировать URL<i class="fas fa-arrow-down"></i></a>
                                <input type="text" id="slug" name="slug" class="form-control" required value="{{ old('slug', isset($category) ? $category->slug : '') }}">
                                @if($errors->has('slug'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('slug') }}
                                    </em>
                                @endif
                            </div>

                        </div>
                        <div class="col-md-5">
                            <input type="hidden" name="parent_id" value="0">
                            {{--
                            <div class="form-group">
                                <label for="parent_id">Родительская категория</label>
                                <select name="parent_id" id="parent_id" class="form-control">
                                    <option value="0">Корневая категория</option>
                                    @if(isset($categories) && count($categories) > 0)
                                        @foreach($categories as $key => $cat)
                                            <option value="{{$cat->id}}" @if($category->parent_id == $cat->id) selected @endif>{{ $cat->title_ru }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            --}}
                            <div class="form-group">
                                <div class="toggle-label">Активность</div>
                                <div class="toggle-btn @if($category->active == 1) active @endif">
                                    <input type="checkbox" class="cb-value" name="active" value="1" @if($category->active == 1) checked @endif />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="picture">Картинка категории</label>
                                <input type="file" class="form-control-file" name="picture" id="picture">
                                @if(isset($category->picture) && $category->picture != '')
                                    <img src="{{ $category->picture }}" style="max-width:200px; max-height:120px;" />
                                    <input type="checkbox" name="del_picture" value="1"> удалить
                                @endif
                            </div>

                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                    <h3>Свойства категории</h3>
                    <div class="row">
                        <div class="col-md-7">
                            <input type="hidden" name="props_sort" id="props_sort" value="{{ implode(',',$exist_props) }}">
                            <table class="table table-striped" style="width:100%;">
                                <thead>
                                    <th>Используемые свойства</th>
                                    {{--<th width="100px" align="center">В фильтре</th>
                                    <th width="100px" align="center">В товаре</th>--}}
                                    <th width="40px"></th>
                                </thead>
                                <tbody id="nestable">
                                    @if(isset($category->properties) && count($category->properties) > 0)
                                        @foreach($category->properties as $prop)
                                            <tr id="exist-prop-{{$prop->id}}">
                                                <td>{{ $prop->title_ru }}</td>
                                                {{--<td>
                                                    <input type="checkbox" name="use_in_filter[]" class="form-control" value="{{$prop->id}}" @if(isset($prop->pivot->use_in_filter) && $prop->pivot->use_in_filter == 1) checked @endif>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="show_in_product[]" class="form-control" value="{{$prop->id}}" @if(isset($prop->pivot->show_in_product) && $prop->pivot->show_in_product == 1) checked @endif>
                                                </td>--}}
                                                <td>
                                                    <a class="btn btn-info btn-prop-del-from-cat" href="" data-id="{{$prop->id}}" role="button" title="Удалить свойство из категории">
                                                        <i class="fas fa-arrow-right"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>



                        </div>
                        <div class="col-md-5">
                            <p><b>Доступные свойства</b></p>
                            @if(isset($properties) && count($properties) > 0)
                                <ul class="list-group">
                                @foreach($properties as $key => $prop)

                                    <li class="list-group-item @if(in_array($prop->id, $hasProps)) prop-hidden @endif" id="available-prop-{{$prop->id}}">
                                        <a class="btn btn-sm btn-info btn-prop-add-to-cat" href="" data-id="{{$prop->id}}" role="button" title="Добавить свойство для категории">
                                            <i class="fas fa-arrow-left"></i>
                                        </a> {{ $prop->title_ru }}
                                    </li>

                                @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                    {{--
                    <div class="row">
                        <div class="col-md-7">
                            <table class="table">
                                <tr><td>
                                        Наследовать настройки свойств всем вложенным категориям
                                    </td>
                                    <td>
                                        <input type="checkbox" name="use_parent_props" class="form-control" value="1">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    --}}
                </div>
                <div class="tab-pane fade" id="metas" role="tabpanel" aria-labelledby="metas-tab">

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="meta_title_ru">Meta title RU</label>
                                <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru', isset($category) ? $category->meta_title_ru : '') }}">
                                <label for="meta_title_ua">Meta title UA</label>
                                <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua', isset($category) ? $category->meta_title_ua : '') }}">
                                <label for="meta_title_en">Meta title EN</label>
                                <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en', isset($category) ? $category->meta_title_en : '') }}">
                            </div>
<hr />
                            <div class="form-group">
                                <label for="meta_description_ru">Meta description RU</label>
                                <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru', isset($category) ? $category->meta_description_ru : '') }}">
                                <label for="meta_description_ua">Meta description UA</label>
                                <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua', isset($category) ? $category->meta_description_ua : '') }}">
                                <label for="meta_description_en">Meta description EN</label>
                                <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en', isset($category) ? $category->meta_description_en : '') }}">
                            </div>
                            <hr />
                            <div class="form-group">
                                <label for="meta_keywords_ru">Meta keywords RU</label>
                                <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru', isset($category) ? $category->meta_keywords_ru : '') }}">
                                <label for="meta_keywords_ua">Meta keywords UA</label>
                                <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua', isset($category) ? $category->meta_keywords_ua : '') }}">
                                <label for="meta_keywords_en">Meta keywords EN</label>
                                <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en', isset($category) ? $category->meta_keywords_en : '') }}">
                            </div>

                        </div>
                    </div>

                </div>

                <div class="tab-pane fade" id="uslugy" role="tabpanel" aria-labelledby="uslugy-tab">

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label for="uslugy_title_ru">Заголовок для услуг RU</label>
                                <input type="text" id="uslugy_title_ru" name="uslugy_title_ru" class="form-control" value="{{ old('uslugy_title_ru', isset($category) ? $category->uslugy_title_ru : '') }}">
                                <label for="uslugy_title_ua">Заголовок для услуг UA</label>
                                <input type="text" id="uslugy_title_ua" name="uslugy_title_ua" class="form-control" value="{{ old('uslugy_title_ua', isset($category) ? $category->uslugy_title_ua : '') }}">
                                <label for="uslugy_title_en">Заголовок для услуг EN</label>
                                <input type="text" id="uslugy_title_en" name="uslugy_title_en" class="form-control" value="{{ old('uslugy_title_en', isset($category) ? $category->uslugy_title_en : '') }}">
                            </div>
<hr />
                            <div class="form-group">
                                <label for="uslugy_description_ru">Описание для услуг RU</label>
                                <textarea class="form-control" name="uslugy_description_ru" rows="3">{{ old('uslugy_description_ru', isset($category) ? $category->uslugy_description_ru : '') }}</textarea>
                                <label for="uslugy_description_ua">Описание для услуг UA</label>
                                <textarea class="form-control" name="uslugy_description_ua" rows="3">{{ old('uslugy_description_ua', isset($category) ? $category->uslugy_description_ua : '') }}</textarea>
                                <label for="uslugy_description_en">Описание для услуг EN</label>
                                <textarea class="form-control" name="uslugy_description_en" rows="3">{{ old('uslugy_description_en', isset($category) ? $category->uslugy_description_en : '') }}</textarea>
                            </div>

                        </div>
                    </div>

                </div>
            </div>









            <div>
                <input class="btn btn-success" type="submit" value="Сохранить">
            </div>
        </form>
    </div>
</div>



@endsection
@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#nestable").sortable({
            update: function(event, ui) {
                var changedList = this.id;
                var order = $(this).sortable('toArray');
                var positions = order.join(',');

                /*console.log({
                    id: changedList,
                    positions: positions
                });*/

                $('#props_sort').val(positions);
            }
        })
            .disableSelection();

        $('.btn-prop-add-to-cat').click(function(e) {
            e.preventDefault();
            var property_id = $(this).data('id');
            var category_id = '{{$category->id}}';
            var ids = $('#props_sort').val();
            $.ajax({
                type:'POST',
                url:'/admin/categories-add-prop',
                data:{property_id:property_id,category_id:category_id,ids:ids},
                success:function(data) {
                    $('#available-prop-' + data.id).hide();
                    $('#nestable').append(data.html);
                    $('#props_sort').val(data.ids);
                }
            });

        });
/*
        $('.btn-prop-del-from-cat').click(function(e) {
            e.preventDefault();
            var property_id = $(this).data('id');
            var category_id = '{{$category->id}}';

            $.ajax({
                type:'POST',
                url:'/admin/categories-del-prop',
                data:{property_id:property_id,category_id:category_id},
                success:function(data) {


                    if(data.id > 0){
                        $('#exist-prop-' + data.id).remove();
                        $('#available-prop-' + data.id).show();


                        var order = $("#nestable").sortable('toArray');
                        var positions = order.join(',');
                        $('#props_sort').val(positions);

                    }
                }
            });

        });
*/

        $( document ).on( "click", ".btn-prop-del-from-cat", function(e) {
            e.preventDefault();
            var property_id = $(this).data('id');
            var category_id = '{{$category->id}}';
            var ids = $('#props_sort').val();
            $.ajax({
                type:'POST',
                url:'/admin/categories-del-prop',
                data:{property_id:property_id,category_id:category_id,ids:ids},
                success:function(data) {
                    if(data.id > 0){
                        $('#exist-prop-' + data.id).remove();
                        $('#available-prop-' + data.id).show();
                        var order = $("#nestable").sortable('toArray');
                        var positions = order.join(',');
                        $('#props_sort').val(positions);
                        $('#props_sort').val(data.ids);
                    }
                }
            });

        });


    </script>
    <script>
        /*
        CKEDITOR.replace( 'seo_text', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });*/
    </script>
@endsection
