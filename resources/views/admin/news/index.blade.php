@extends('layouts.admin')
@section('title')
    Новости
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .select2{
            /*min-width:150px!important;*/
            width: 200px!important;
        }
    </style>
@endsection
@section('content')
    @can('news_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.news.create") }}">
                    <i class="fas fa-plus"></i> Создать новость
                </a>
            </div>
        </div>
    @endcan

    <div class="card">
        <div class="card-header">
            Фильтр
        </div>

        <div class="card-body">

            <form id="form-filter" action="{{ route("admin.news.index") }}" method="GET" enctype="multipart/form-data">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label>ID</label>
                        <input type="number" min="1" class="form-control w-80" name="id" @if(!empty($filter['id'])) value="{{ $filter['id'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>Название</label>
                        <input type="text" class="form-control w-160" name="title" @if(!empty($filter['title'])) value="{{ $filter['title'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>SLUG</label>
                        <input type="text" class="form-control w-160" name="slug" @if(!empty($filter['slug'])) value="{{ $filter['slug'] }}" @endif>
                    </div>

                    <div class="col-auto">
                        <label>Активность</label>
                        <select class="form-control" name="active">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['active']) && $filter['active'] == 1) selected @endif>Да</option>
                            <option value="0" @if(isset($filter['active']) && $filter['active'] == 0) selected @endif>Нет</option>
                        </select>
                    </div>

                    <div class="col-auto">
                        <label>Закреплено</label>
                        <select class="form-control" name="top">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['top']) && $filter['top'] == 1) selected @endif>Да</option>
                            <option value="0" @if(isset($filter['top']) && $filter['top'] == 0) selected @endif>Нет</option>
                        </select>
                    </div>
{{--
                    <div class="col-auto">
                        <label>Теги</label><br />
                        <select name="tag[]" id="tag" class="form-control select2" multiple="multiple">
                            @if(!empty($tags))
                                @foreach($tags as $tag)
                                    <option value="{{ $tag->id }}" @if(!empty($filter['tag']) && in_array($tag->id,$filter['tag'])) selected @endif>
                                        {{ $tag->title_ua }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
--}}
                    <div class="col-auto">
                        <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/news?reset=y" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <div class="card">
        <div class="card-header">
            Новости
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Картинка</th>
                            <th style="min-width: 150px">
                                Названия
                                {{--@if(!empty($sort) && $sort['field'] == 'title_ru' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'title_ru' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="title_ru" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="title_ru" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="title_ru" data-direction="desc"></i>--}}
                            </th>
                            <th>SLUG</th>
                            <th style="min-width: 120px">Акт.
    {{--@if(!empty($sort) && $sort['field'] == 'active' && $sort['direction'] == 'desc')
        <?php
        $s = 'sort-hide';
        $s_asc = 'sort-hide';
        $s_desc = 'sort-show';
        ?>
    @elseif(!empty($sort) && $sort['field'] == 'active' && $sort['direction'] == 'asc')
        <?php
        $s = 'sort-hide';
        $s_asc = 'sort-show';
        $s_desc = 'sort-hide';
        ?>
    @else
        <?php
        $s = 'sort-show';
        $s_asc = 'sort-hide';
        $s_desc = 'sort-hide';
        ?>
    @endif
    <i class="fas i-sort {{ $s }} fa-sort" data-field="active" data-direction="asc"></i>
    <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="active" data-direction="asc"></i>
    <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="active" data-direction="desc"></i>--}}
</th>
{{--<th>Топ</th>--}}

<th width="130px">Публикация
    {{--@if(!empty($sort) && $sort['field'] == 'show_from' && $sort['direction'] == 'desc')
        <?php
        $s = 'sort-hide';
        $s_asc = 'sort-hide';
        $s_desc = 'sort-show';
        ?>
    @elseif(!empty($sort) && $sort['field'] == 'show_from' && $sort['direction'] == 'asc')
        <?php
        $s = 'sort-hide';
        $s_asc = 'sort-show';
        $s_desc = 'sort-hide';
        ?>
    @else
        <?php
        $s = 'sort-show';
        $s_asc = 'sort-hide';
        $s_desc = 'sort-hide';
        ?>
    @endif
    <i class="fas i-sort {{ $s }} fa-sort" data-field="show_from" data-direction="asc"></i>
    <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="show_from" data-direction="asc"></i>
    <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="show_from" data-direction="desc"></i>--}}
</th>
<th>Обновлено</th>
<th style="min-width: 60px">ID
    {{-- @if(!empty($sort) && $sort['field'] == 'id' && $sort['direction'] == 'desc')
         <?php
         $s = 'sort-hide';
         $s_asc = 'sort-hide';
         $s_desc = 'sort-show';
         ?>
     @elseif(!empty($sort) && $sort['field'] == 'id' && $sort['direction'] == 'asc')
         <?php
         $s = 'sort-hide';
         $s_asc = 'sort-show';
         $s_desc = 'sort-hide';
         ?>
     @else
         <?php
         $s = 'sort-show';
         $s_asc = 'sort-hide';
         $s_desc = 'sort-hide';
         ?>
     @endif
     <i class="fas i-sort {{ $s }} fa-sort" data-field="id" data-direction="asc"></i>
     <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="id" data-direction="asc"></i>
     <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="id" data-direction="desc"></i>--}}
 </th>
 </tr>
 </thead>
 <tbody>
 @foreach($news as $key => $item)
 <tr id="item-{{ $item->id }}">
     <td>
         <div class="btn-group">
             <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <i class="fas fa-bars"></i>
             </button>
             <div class="dropdown-menu">
                 <a class="dropdown-item" href="/news/{{$item->slug}}" target="_blank" title="Просмотреть на сайте"><i class="fas fa-eye"></i> Просмотр на сайте</a>

                 @can('news_edit')
                     <a class="dropdown-item" href="/admin/news/{{$item->id}}/edit" role="button" title="Редактировать"><i class="fas fa-pencil-alt"></i> Редактировать</a>
                 @endcan

                 <div class="dropdown-divider"></div>
                 @can('news_delete')
                     <a class="dropdown-item btn-item-del" href=""
                        data-id="{{$item->id}}" data-title="{{ $item->title_ru }} [{{ $item->id }}]" data-url="/admin/news/{{ $item->id }}"
                        role="button" title="Удалить"><i class="fas fa-trash"></i> Удалить</a>
                 @endcan
             </div>
         </div>
     </td>
     <td>
         @if(!empty($item->picture)) <img src="{{ $item->picture }}" /> @endif
         @if(!empty($item->thumbnail)) <br /><img src="{{ $item->thumbnail }}" style="margin-top: 4px;" /> @endif
     </td>
     <td>
         {{ $item->title_ru ?? '' }}
         <br />{{ $item->title_ua ?? '' }}
         <br />{{ $item->title_en ?? '' }}
     </td>
     <td>
         {{ $item->slug }}
     </td>
     <td>
         @if($item->active == 1)
             <span class="badge badge-success">Да</span>
         @else
             <span class="badge badge-danger">Нет</span>
         @endif
     </td>
     {{--<td>
         @if($item->on_top == 1)
             <span class="badge badge-success">Да</span>
         @else
             <span class="badge badge-danger">Нет</span>
         @endif
     </td>--}}

     <td>
         {{ $item->show_from }}
     </td>
     <td>
         {{ $item->updated_at }}<br />
         {{ $item->updater->surname }} {{ $item->updater->name }} [{{ $item->updater->id }}]
     </td>
     <td>
         {{ $item->id }}
     </td>
 </tr>
 @endforeach
 </tbody>
 </table>
 </div>
 <div>
 {{ $news->appends(request()->except('page'))->links() }}
 </div>
 </div>
 </div>


 @section('scripts')
 <script>
 $('.i-sort').click(function (e) {
 e.preventDefault();
 var field = $(this).data('field');
 var direction = $(this).data('direction');
 var form = $('#form-filter').serialize();
 form = form + '&sort=' + field + '&direction=' + direction;
 window.location.href = "/admin/news?" + form;
 });
 </script>
 @endsection
 @endsection
