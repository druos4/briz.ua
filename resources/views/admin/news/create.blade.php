@extends('layouts.admin')
@section('title')
    Создание новости
@endsection
@section('styles')
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <link href="{{ asset('cropper/cropper.css') }}" rel="stylesheet" />
    <style>
        .publish_date{
            width:160px;
            display: inline-block;
        }
        .publish_time{
            width:60px;
            display: inline-block;
        }
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
        .invalid-feedback{
            display: block;
        }
    </style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        Создание новости
    </div>

    <div class="card-body">
        <form action="{{ route("admin.news.store") }}" method="POST" id="entity-form" enctype="multipart/form-data">
            @csrf
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основное</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="anons-tab" data-toggle="tab" href="#anons" role="tab" aria-controls="anons" aria-selected="true">Анонс</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="true">Подробно</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="recomend-tab" data-toggle="tab" href="#recomend" role="tab" aria-controls="recomend" aria-selected="false">Рекомендуемые новости</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок UA*</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua') }}">
                                @if($errors->has('title_ua'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок EN*</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="slug">SLUG*</label>
                                <a href="" class="btn-make-slug">Сгенерировать SLUG<i class="fas fa-arrow-down"></i></a>
                                <input type="text" id="slug" name="slug" class="form-control" required value="{{ old('slug') }}">
                                @if($errors->has('slug'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('slug') }}
                                    </em>
                                @endif
                            </div>
{{--
                            <div class="form-group">
                                <label for="tags">Теги</label>

                                <select name="tags[]" id="tags" class="form-control select2" multiple="multiple">
                                    @if(!empty($tags))
                                        @foreach($tags as $tag)
                                            <option value="{{ $tag->id }}">
                                                {{ $tag->title_ua }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            --}}
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="toggle-label">Активность</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="active" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="toggle-label">Опубликовано с (по умолчания берется дата создания новости)</div>
                                <div>
                                    <input type="date" class="form-control publish_date" name="publish_date" value="">
                                    <input type="text" class="form-control publish_time" name="publish_time" placeholder="__:__" value="">
                                </div>
                            </div>
{{--
                            <div class="form-group">
                                <div class="toggle-label">Закрепить</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="on_top" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>
--}}
                            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                <label for="image">Картинка (1905x516px)</label><br />
                                <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                                    <input type="file" class="" name="image" accept="image/*">
                                </label>
                                @if($errors->has('image'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('image') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="toggle-label">Можно подключить</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="can_connect" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="anons" role="tabpanel" aria-labelledby="anons-tab">
                    <label for="anons_ru">Анонс RU</label>
                    <textarea name="anons_ru" id="anons_ru" class="form-control" rows="12"></textarea>
                    <hr />
                    <label for="anons_ua">Анонс UA</label>
                    <textarea name="anons_ua" id="anons_ua" class="form-control" rows="12"></textarea>
                    <hr />
                    <label for="anons_ua">Анонс EN</label>
                    <textarea name="anons_en" id="anons_en" class="form-control" rows="12"></textarea>
                </div>

                <div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                    <label for="detail_ru">Подробный текст RU</label>
                    <textarea name="detail_ru" id="detail_ru" class="form-control" rows="12"></textarea>
                    <hr />
                    <label for="detail_ua">Подробный текст UA</label>
                    <textarea name="detail_ua" id="detail_ua" class="form-control" rows="12"></textarea>
                    <hr />
                    <label for="detail_ua">Подробный текст EN</label>
                    <textarea name="detail_en" id="detail_en" class="form-control" rows="12"></textarea>
                </div>

                <div class="tab-pane fade" id="metas" role="tabpanel" aria-labelledby="metas-tab">
                    <div class="form-group">
                        <label for="meta_title_ru">Meta title RU</label>
                        <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title UA</label>
                        <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title EN</label>
                        <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_description_ru">Meta description RU</label>
                        <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description UA</label>
                        <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description EN</label>
                        <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_keywords_ru">Meta keywords RU</label>
                        <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords UA</label>
                        <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords EN</label>
                        <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en') }}">
                    </div>

                </div>

                <div class="tab-pane fade" id="recomend" role="tabpanel" aria-labelledby="recomend-tab">

                    <div class="form-group">
                        <label for="tags">Рекомендуемые новости</label>
                        <?php $oldActions = old('recomends'); ?>
                        <select name="recomends[]" id="recomends" class="form-control select2" multiple="multiple">
                            @if(!empty($others))
                                @foreach($others as $other)
                                    <option value="{{ $other->id }}" @if(!empty($oldActions) && in_array($other->id,$oldActions)) selected @endif>
                                        {{ $other->title_ru }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />




                </div>

            </div>
            <div>
                <button class="btn btn-success btn-save" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/news" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>


<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <div class="alert-inner"></div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

@endsection

@section('scripts')
    @include('admin.modals.image-cropper')

    <script>
        $('.btn-save').click(function (e) {
            e.preventDefault();
            $('.alert-inner').html('');
            var title = $('#title_ru').val();
            var titleUa = $('#title_ua').val();
            var slug = $('#slug').val();
            var errors = '';
            if(title == ''){
                errors = 'Заполните поле *Заголовок RU*<br />';
            }
            if(slug == ''){
                errors = errors + 'Заполните поле *SLUG*<br />';
            }
            if(title.length > 255){
                errors = '*Заголовок RU* должен быть не длинее 255 символов<br />';
            }
            if(titleUa.length > 255){
                errors = '*Заголовок UA* должен быть не длинее 255 символов<br />';
            }
            if(slug.length > 255){
                errors = '*Заголовок RU* должен быть не длинее 255 символов<br />';
            }

            if(errors != ''){
                $('.alert-inner').html(errors);
                $('.alert-danger').show();
            } else {

                $.ajax({
                    type:'POST',
                    url:'/admin/news/slug',
                    data:{slug:slug},
                    success:function(data) {

                        if(data.slug == false){
                            $('.alert-inner').html('Поле SLUG должно быть уникальным!');
                            $('.alert-danger').show();
                        } else {
                            $('#entity-form').submit();
                        }

                    }
                });
            }
        });

        CKEDITOR.replace( 'anons_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'anons_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'anons_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });

        CKEDITOR.replace( 'detail_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'detail_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'detail_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
    {{--<script src="{{ asset('adminscripts/cropper.js') }}"></script>--}}
    <script src="{{ asset('adminscripts/jquery.maskeditinput.min.js') }}"></script>
    <script>
        $('.publish_time').mask("99:99");
    </script>
@endsection
