@extends('layouts.admin')
@section('title')
    Редактирование жанра каналов
@endsection
@section('styles')
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <style>
        #price_old{
            display: inline-block;
            width:120px;
        }
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
    </style>
@endsection

@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-secondary" href="{{ route("admin.genres.index") }}">
                <i class="fas fa-chevron-left"></i> Назад
            </a>
        </div>
    </div>

<div class="card">

    <div class="card-header">
        Редактирование жанра каналов
    </div>

    <div class="card-body">
        <form action="/admin/genres/{{ $genre->id }}/update" id="tariff-form" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                        <label for="title_ru">Заголовок RU*</label>
                        <input type="text" id="title_ru" name="title_ru" class="form-control" required value="{{ old('title_ru', ($genre->title_ru) ? $genre->title_ru : '') }}">
                        @if($errors->has('title_ru'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title_ru') }}
                            </em>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                        <label for="title_ua">Заголовок UA*</label>
                        <input type="text" id="title_ua" name="title_ua" class="form-control" value="{{ old('title_ua', ($genre->title_ua) ? $genre->title_ua : '') }}">
                        @if($errors->has('title_ua'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title_ua') }}
                            </em>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                        <label for="title_en">Заголовок EN*</label>
                        <input type="text" id="title_en" name="title_en" class="form-control" value="{{ old('title_en', ($genre->title_en) ? $genre->title_en : '') }}">
                        @if($errors->has('title_en'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title_en') }}
                            </em>
                        @endif
                    </div>

                </div>
                <div class="col-md-4">

                    <div class="form-group">
                        <label for="sort">Сортировка</label>
                        <input type="number" min="1" max="99999" id="sort" name="sort" class="form-control" value="{{ old('sort', ($genre->sort) ? $genre->sort : 10) }}">
                    </div>

                    <div class="form-group">
                        <label for="sort">Код: {{ $genre->code }}</label>
                    </div>

                </div>
            </div>

            <div>
                <button class="btn btn-success btn-save" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/genres" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script>
    </script>
@endsection
