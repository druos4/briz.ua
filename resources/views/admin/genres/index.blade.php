@extends('layouts.admin')
@section('title')
    Жанры каналов
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .cross-line{
            text-decoration: line-through;
        }
    </style>
@endsection
@section('content')

    <div class="card">
        <div class="card-header">
            Жанры каналов
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Названия</th>
                            <th>Код</th>
                            <th>Сортировка</th>
                            <th>Обновлено</th>
                            <th style="min-width: 60px">ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($genres))
                            @foreach($genres as $key => $item)
                                <tr id="item-{{ $item->id }}">
                                    <td>
                                        @can('tariffs_edit')
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-bars"></i>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="/admin/genres/{{$item->id}}/edit" role="button" title="Редактировать"><i class="fas fa-pencil-alt"></i> Редактировать</a>
                                                </div>
                                            </div>
                                        @endcan
                                    </td>
                                    <td>
                                        @if($item->title_ru != '') {{ $item->title_ru }}<br /> @endif
                                        @if($item->title_ua != '') {{ $item->title_ua }}<br /> @endif
                                        @if($item->title_en != '') {{ $item->title_en }} @endif
                                    </td>
                                    <td>
                                        {{ $item->code }}
                                    </td>
                                    <td>
                                        {{ $item->sort }}
                                    </td>
                                    <td>
                                        {{ $item->updated_at }}
                                    </td>
                                    <td>
                                        @can('tariffs_edit')
                                            <a href="/admin/genres/{{$item->id}}/edit" title="Редактировать">
                                        @endcan
                                            {{ $item->id }}
                                        @can('tariffs_edit')
                                           </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div>
                @if(!empty($items))
                    {{ $items->appends(request()->except('page'))->links() }}
                @endif
            </div>
        </div>
    </div>


    @section('scripts')
        <script>
            $('.i-sort').click(function (e) {
                e.preventDefault();
                var field = $(this).data('field');
                var direction = $(this).data('direction');
                var form = $('#form-filter').serialize();
                form = form + '&sort=' + field + '&direction=' + direction;
                window.location.href = "/admin/tariffs?" + form;
            });
        </script>
    @endsection
@endsection
