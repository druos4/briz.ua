@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .new-item-block{
            display:none;
        }

    </style>
@endsection
@section('content')
    <a class="btn btn-default" href="{{ route('admin.menu') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a>&nbsp;&nbsp;
    <a href="#" id="saveOrder" class="btn btn-primary"><i class="fas fa-save"></i> Сохранить порядок элементов</a>&nbsp;&nbsp;
    {{--
    <a href="/admin/menu/show" class="btn btn-info" target="_blank">Предпросмотр <i class="fas fa-caret-right"></i></a>
    <br />
    <br />
    Последняя генерация меню: {{ date('H:i:s d.m.Y',strtotime($menu->updated_at)) }}&nbsp;<a href="/admin/menu/{{ $menu->id }}/generate" class="btn btn-success"><i class="fas fa-save"></i> Сгенерировать меню</a>
--}}
    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            <span style="float:left;">Редактирвоание меню &laquo;{{ $menu->title }}&raquo;</span>
            <span style="float:right;">
                <a class="btn btn-success btn-create-new" href="" role="button"><i class="fas fa-plus"></i> Новый элемент</a>
            </span>
        </div>
        <div class="card-body">

            <div class="dd" id="nestable">
                <ol class="dd-list parent-list">


                    @if(isset($items) && count($items) > 0)
                        @foreach($items as $key => $item)
                            <li class="dd-item" id="item-{{ $item->id }}" data-id="{!! $item->id !!}">

                                <div class="dd-handle">
                                    @if($item->bold == 1) <span style="color:#000;"><i><?=$item->title_ru?></i></span> @else <?=$item->title_ru?> @endif
                                    <div class="pull-right action-buttons">
                                        @if($item->item_type == 'product') (товар)
                                        @elseif($item->item_type == 'category') (категория)
                                        @elseif($item->item_type == 'category-childs') (категория с вложениями)
                                        @elseif($item->item_type == 'divider') (разделитель)
                                        @elseif($item->item_type == 'simple')(обычный)
                                        @endif
                                        <a href="" class="btn btn-xs btn-warning btn-item-edit" data-id="{{$item->id}}" title="Редактировать"><i class="fas fa-pen"></i></a>
                                        <a href="" class="btn btn-xs btn-danger btn-item-menu-del" data-id="{{$item->id}}" data-title="{{$item->title_ru}}" title="Удалить"><i class="fas fa-trash"></i></a>
                                    </div>
                                </div>

                                @if(count($item->children))
                                    <ol class="dd-list">
                                        @foreach($item->children as $child)
                                            <li class="dd-item" id="item-{{ $child->id }}" data-id="{{ $child->id }}">
                                                <div class="dd-handle">
                                                    @if($child->bold == 1) <span style="color:#000;"><i><?=$child->title_ru?></i></span> @else <?=$child->title_ru?> @endif
                                                    <div class="pull-right action-buttons">
                                                        @if($child->item_type == 'product') (товар)
                                                        @elseif($child->item_type == 'category') (категория)
                                                        @elseif($child->item_type == 'category-childs') (категория с вложениями)
                                                        @elseif($child->item_type == 'divider') (разделитель)
                                                        @elseif($child->item_type == 'simple')(обычный)
                                                        @endif
                                                        <a href="" class="btn btn-xs btn-warning btn-item-edit" data-id="{{$child->id}}" title="Редактировать"><i class="fas fa-pen"></i></a>
                                                        <a href="" class="btn btn-xs btn-danger btn-item-menu-del" data-id="{{$child->id}}" data-title="{{$child->title_ru}}" title="Удалить"><i class="fas fa-trash"></i></a>
                                                    </div>
                                                </div>
                                                <ol>
                                                    @foreach($child->children as $ch)
                                                        <li class="dd-item" id="item-{{ $ch->id }}" data-id="{{ $ch->id }}">
                                                            <div class="dd-handle">
                                                                @if($ch->bold == 1) <span style="color:#000;"><i><?=$ch->title_ru?></i></span> @else <?=$ch->title_ru?> @endif
                                                                <div class="pull-right action-buttons">
                                                                    @if($ch->item_type == 'product') (товар)
                                                                    @elseif($ch->item_type == 'category') (категория)
                                                                    @elseif($ch->item_type == 'category-childs') (категория с вложениями)
                                                                    @elseif($ch->item_type == 'divider') (разделитель)
                                                                    @elseif($ch->item_type == 'simple')(обычный)
                                                                    @endif
                                                                    <a href="" class="btn btn-xs btn-warning btn-item-edit" data-id="{{$ch->id}}" title="Редактировать"><i class="fas fa-pen"></i></a>
                                                                    <a href="" class="btn btn-xs btn-danger btn-item-menu-del" data-id="{{$ch->id}}" data-title="{{$ch->title_ru}}" title="Удалить"><i class="fas fa-trash"></i></a>
                                                                </div>
                                                            </div>
                                                            <ol>
                                                                @foreach($ch->children as $ch2)
                                                                    <li class="dd-item" id="item-{{ $ch2->id }}" data-id="{{ $ch2->id }}">
                                                                        <div class="dd-handle">
                                                                            @if($ch2->bold == 1) <span style="color:#000;"><i><?=$ch2->title_ru?></i></span> @else <?=$ch2->title_ru?> @endif
                                                                            <div class="pull-right action-buttons">
                                                                                @if($ch2->item_type == 'product') (товар)
                                                                                @elseif($ch2->item_type == 'category') (категория)
                                                                                @elseif($ch2->item_type == 'category-childs') (категория с вложениями)
                                                                                @elseif($ch2->item_type == 'divider') (разделитель)
                                                                                @elseif($ch2->item_type == 'simple')(обычный)
                                                                                @endif
                                                                                <a href="" class="btn btn-xs btn-warning btn-item-edit" data-id="{{$ch2->id}}" title="Редактировать"><i class="fas fa-pen"></i></a>
                                                                                <a href="" class="btn btn-xs btn-danger btn-item-menu-del" data-id="{{$ch2->id}}" data-title="{{$ch2->title_ru}}" title="Удалить"><i class="fas fa-trash"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            </ol>
                                                        </li>
                                                    @endforeach
                                                </ol>
                                            </li>
                                        @endforeach
                                    </ol>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ol>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <!-- Modal -->
    <div class="modal fade" id="menuItemDelModal" tabindex="-1" role="dialog" aria-labelledby="menuItemDelModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="menuItemDelModalLabel">Удалить элемент</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="del-item-id" value="0">
                    <p>Действительно удалить элемент &laquo;<span id="del-item-title"></span>&raquo;?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                    <button type="button" class="btn btn-danger btn-item-menu-delete">Удалить</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="menuNewItem" tabindex="-1" role="dialog" aria-labelledby="menuNewItem" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="menuNewItemLabel">Новый элемент меню</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="new-item-form">
                        <input type="hidden" name="item_id" id="item_id" value="0">
                        <input type="hidden" name="parent_id" id="parent_id" value="0">
                        <input type="hidden" name="sort" id="sort" value="100">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Тип элемента</label>
                            <select class="form-control" name="item_type" id="item_type">
                                <option value="0"> - выберите тип элемента - </option>
                                <option value="simple">Обычная ссылка</option>
                                {{--<option value="product">Ссылка на товар</option>
                                <option value="category">Ссылка на категорию</option>
                                <option value="category-childs">Категория с вложениями</option>--}}
                                {{--<option value="divider">Разделительный блок</option>--}}
                            </select>
                        </div>

                        <div class="form-group new-item-block item-product">
                            <label for="exampleInputEmail1">Выберите категорию</label>
                            <select name="category-for-product" id="category-for-product" class="form-control">
                                <option value="0"> - укажите основную категорию - </option>
                                @if(isset($categories) && count($categories) > 0)
                                    <?php $a0 = 0; ?>
                                    @foreach($categories as $key => $category)
                                        <?php $a0++; ?>
                                        <option value="{{ $category->id }}">{{$a0}}. {{ $category->title }}</option>
                                        @if(count($category->children))
                                            <?php $a1 = 0; ?>
                                            @foreach($category->children as $child)
                                                <?php $a1++; ?>
                                                <option value="{{ $child->id }}">&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title }}</option>
                                                <?php $a2 = 0; ?>
                                                @foreach($child->children as $ch)
                                                    <?php $a2++; ?>
                                                    <option value="{{ $ch->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title }}</option>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                            <br />
                            <label for="exampleInputEmail1">Выберите товар</label>
                            <select name="product_id" id="product_id" class="form-control">

                            </select>
                        </div>


                        <div class="form-group new-item-block item-category">
                            <label for="exampleInputEmail1">Выберите категорию</label>
                            <select name="category-single" id="category-single" class="form-control">
                                <option value="0"> - укажите основную категорию - </option>
                                @if(isset($categories) && count($categories) > 0)
                                    <?php $a0 = 0; ?>
                                    @foreach($categories as $key => $category)
                                        <?php $a0++; ?>
                                        <option value="{{ $category->id }}">{{$a0}}. {{ $category->title }}</option>
                                        @if(count($category->children))
                                            <?php $a1 = 0; ?>
                                            @foreach($category->children as $child)
                                                <?php $a1++; ?>
                                                <option value="{{ $child->id }}">&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title }}</option>
                                                <?php $a2 = 0; ?>
                                                @foreach($child->children as $ch)
                                                    <?php $a2++; ?>
                                                    <option value="{{ $ch->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title }}</option>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group new-item-block item-category-childs">
                            <label for="exampleInputEmail1">Выберите категорию</label>
                            <select name="category-with-childs" id="category-with-childs" class="form-control">
                                <option value="0"> - укажите основную категорию - </option>
                                @if(isset($categories) && count($categories) > 0)
                                    <?php $a0 = 0; ?>
                                    @foreach($categories as $key => $category)
                                        <?php $a0++; ?>
                                        <option value="{{ $category->id }}">{{$a0}}. {{ $category->title }}</option>
                                        @if(count($category->children))
                                            <?php $a1 = 0; ?>
                                            @foreach($category->children as $child)
                                                <?php $a1++; ?>
                                                <option value="{{ $child->id }}">&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title }}</option>
                                                <?php $a2 = 0; ?>
                                                @foreach($child->children as $ch)
                                                    <?php $a2++; ?>
                                                    <option value="{{ $ch->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title }}</option>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>


                        <div class="form-group new-item-block item-simple">
                            <label for="exampleInputEmail1">Заголовок RU</label>
                            <input type="text" name="title_ru" id="title_ru" class="form-control" value="">
                            <label for="exampleInputEmail1">Заголовок UA</label>
                            <input type="text" name="title_ua" id="title_ua" class="form-control" value="">
                            <label for="exampleInputEmail1">Заголовок EN</label>
                            <input type="text" name="title_en" id="title_en" class="form-control" value="">
                        </div>
                        <div class="form-group new-item-block item-simple">
                            <label for="exampleInputEmail1">URL</label>
                            <input type="text" name="url" id="url" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Открывать в новой вкладке</label>
                            <select name="blank" id="blank" class="form-control">
                                <option value="0">нет</option>
                                <option value="1">да</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Жирный</label>
                            <select name="bold" id="bold" class="form-control">
                                <option value="0">нет</option>
                                <option value="1">да</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                    <button type="button" class="btn btn-success btn-item-add">Сохранить</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#item_type').change(function() {

            var id = $('#item_type').val();
            $('.new-item-block').hide();
            $('.item-' + id).show();
        });

        $('#category-for-product').on('change', function() {
            var catid = $(this).val();
            $('#product_id').html('');
            $.ajax({
                type:'POST',
                url:'/admin/products/getByCatId',
                data:{catid:catid},
                success:function(data) {
                    if(data.html != ''){
                        $('#product_id').html(data.html);
                    }
                }
            });
        });


            $('.btn-item-edit').click(function(e) {
                e.preventDefault();
                formReset();
                var id = '{{$menu->id}}';
                var item_id = $(this).data('id');
                $.ajax({
                    type:'POST',
                    url:'/admin/menu/' + id + '/get-item',
                    data:{item_id:item_id},
                    success:function(data) {
                        if(data.id > 0){
                            $('.item-' + data.item.item_type).show();
                            $('#item_id').val(data.id);
                            $('#item_type').val(data.item.item_type);
                            $('#parent_id').val(data.item.parent_id);
                            $('#sort').val(data.item.sort);

                            if(data.item.item_type == 'simple'){

                                $('#title_ru').val(data.item.title_ru);
                                $('#title_ua').val(data.item.title_ua);
                                $('#title_en').val(data.item.title_en);
                                $('#url').val(data.item.url);
                                $('#bold').val(data.item.bold);
                                $('#blank').val(data.item.blank);

                            } else if(data.item.item_type == 'product'){

                                $('#category-for-product').val(data.prod.category_id);
                                if(data.html != ''){
                                    $('#product_id').html(data.html);
                                }
                                $('#product_id').val(data.prod.id);

                            } else if(data.item.item_type == 'category'){

                                $('#category-single').val(data.item.options);

                            } else if(data.item.item_type == 'category-childs'){

                                $('#category-with-childs').val(data.item.options);

                            } else if(data.item.item_type == 'divider'){

                            }

                            $('#menuNewItem').modal('show');

                        }
                    }
                });
            });


        $('.btn-item-add').click(function(e) {
            e.preventDefault();
            var form = $('#new-item-form').serialize();
            var id = '{{$menu->id}}';
            $.ajax({
                type:'POST',
                url:'/admin/menu/' + id + '/add-item',
                data:form,
                success:function(data) {

                    if(data.id > 0 && data.existed == 0){
                        $('#nestable .parent-list').append(data.html);
                    } else if(data.id > 0 && data.existed == 1){
                        location.reload();
                    }
                    $('#menuNewItem').modal('toggle');

                    formReset();
                }
            });
        });


        $('.btn-item-menu-del').click(function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var title = $(this).data('title');
            $('#del-item-id').val(id);
            $('#del-item-title').html(title);
            $('#menuItemDelModal').modal('show');
        });

        function formReset(){
            $('.new-item-block').hide();
            $('#item_id').val(0);
            $('#parent_id').val(0);
            $('#item_type').val(0);
            $('#category-for-product').val(0);
            $('#product_id').val(0);
            $('#category-single').val(0);
            $('#category-with-childs').val(0);
            $('#title_ru').val('');
            $('#title_ua').val('');
            $('#title_en').val('');
            $('#url').val('');
            $('#bold').val(0);
            $('#blank').val(0);
        }

        $('.btn-item-menu-delete').click(function(e) {
            e.preventDefault();
            var item_id = $('#del-item-id').val();
            $.ajax({
                type:'POST',
                url:'/admin/menu/{{$menu->id}}/del-item',
                data:{item_id:item_id},
                success:function(data) {
                    if(data.id > 0){
                        $('#item-' + data.id).remove();
                        $('#del-item-id').val('');
                        $('#del-item-title').html('');
                        $('#menuItemDelModal').modal('hide');
                    }
                }
            });
        });

        $('.btn-create-new').click(function(e) {
            e.preventDefault();
            $('#menuNewItem').modal('show');
        });

        $('.cb-value').click(function() {
            var mainParent = $(this).parent('.toggle-btn');
            if($(mainParent).find('input.cb-value').is(':checked')) {
                $(mainParent).addClass('active');
                $(mainParent).find('input.cb-value').attr('checked', true);
            } else {
                $(mainParent).removeClass('active');
                $(mainParent).find('input.cb-value').attr('checked', false);
            }

        });


    </script>

    <!-- page specific plugin scripts -->
    <script src="{{ asset('adminscripts/jquery.nestable.min.js') }}"></script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
        jQuery(function($){
            var nestable,
                serialized,
                settings = {maxDepth:4},
                saveOrder = $('#saveOrder'),
                edit = $('.edit');

            nestable = $('.dd').nestable(settings);

            saveOrder.on('click', function(e) {
                e.preventDefault();
                serialized = nestable.nestable('serialize');

                $.ajax({
                    method:'POST',
                    url : "/admin/menu/{{$menu->id}}/items-save-sort",
                    data: { _token: "{!! csrf_token() !!}", serialized: serialized }

                }).done(function (data) {
                    alert("Сохранено!");
                })
            });

            $('.dd-handle a').on('mousedown', function(e){
                e.stopPropagation();
            });

            $('[data-rel="tooltip"]').tooltip();

        });
    </script>

@endsection
