@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')
    <a class="btn btn-default" href="/admin/menu/{{ $menu->id }}/edit" role="button"><i class="fas fa-angle-left"></i> Назад</a>
    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Предпросмотр меню &laquo;{{ $menu->title }}&raquo;
        </div>
        <div class="card-body">
            <ol class="dd-list">


                @if(isset($items) && count($items) > 0)
                    @foreach($items as $key => $item)
                        <li class="dd-item" id="item-{{ $item->id }}" data-id="{!! $item->id !!}">
                            <div class="dd-handle">
                                {{ $item->title }}
                            </div>

                            @if(count($item->children))
                                <ol class="dd-list">
                                    @foreach($item->children as $child)
                                        <li class="dd-item" id="item-{{ $child->id }}" data-id="{{ $child->id }}">
                                            <div class="dd-handle">
                                                {{ $child->title }}
                                            </div>
                                            <ol>
                                                @foreach($child->children as $ch)
                                                    <li class="dd-item" id="item-{{ $ch->id }}" data-id="{{ $ch->id }}">
                                                        <div class="dd-handle">
                                                            {{ $ch->title }}
                                                        </div>
                                                        <ol>
                                                            @foreach($ch->children as $ch2)
                                                                <li class="dd-item" id="item-{{ $ch2->id }}" data-id="{{ $ch2->id }}">
                                                                    <div class="dd-handle">
                                                                       {{ $ch2->title }}
                                                                    </div>
                                                                </li>
                                                            @endforeach
                                                        </ol>
                                                    </li>
                                                @endforeach
                                            </ol>
                                        </li>
                                    @endforeach
                                </ol>
                            @endif
                        </li>
                    @endforeach
                @endif



            </ol>
        </div>
    </div>
@endsection
@section('scripts')
    @parent

@endsection