@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')
    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Список блоков меню
        </div>


        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <th>Меню</th>
                    <th>Активность</th>
                    <th>Код</th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach($menus as $key => $menu)
                        @if($menu->code != 'footer_menu')
                        <tr>
                            <td>
                                {{ $menu->title }}
                            </td>
                            <td>
                                @if($menu->active == 1)
                                    <span class="badge badge-success">Да</span>
                                @else
                                    <span class="badge badge-warning">Нет</span>
                                @endif
                            </td>
                            <td>
                                {{ $menu->code }}
                            </td>
                            <td>
                                <a class="btn btn-info" href="/admin/menu/{{$menu->id}}/edit" role="button" title="Редактировать меню"><i class="fas fa-arrow-right"></i></a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('scripts')
    @parent

@endsection