@extends('layouts.admin')
@section('styles')
    <link rel="stylesheet" href="{{ asset('vendor/file-manager/css/file-manager.css') }}">
@endsection
@section('content')


<div class="card">
    <div class="card-header">
        <h1>Файловый менеджер</h1>
    </div>
    <div class="card-body">
        <div style="height: 600px;">
            <div id="fm"></div>
        </div>
    </div>
</div>






@endsection
@section('scripts')
@parent
<script src="{{ asset('vendor/file-manager/js/file-manager.js') }}"></script>
@endsection
