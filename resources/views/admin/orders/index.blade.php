@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')


    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            <span style="font-size:16px;">Список заказов</span>
            <a class="btn btn-warning" href="/admin/orders" role="button" style="float:right;" title="Сбросить фильтр"><i class="fas fa-remove"></i> Сбросить фильтр</a>
        </div>


        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="100px">№ заказа</th>
                        <th colspan="2">Создан</th>
                        <th>Статус</th>
                        <th>Клиент</th>
                        <th>Оплата</th>
                        <th>Доставка</th>
                        <th>Сумма</th>
                        <th>Менеджер</th>
                        <th></th>
                    </tr>
                    <tr>
                        <form action="/admin/orders" method="get">
                            @csrf
                        <th><input type="text" class="form-control" name="id" id="id"
                                   @if(isset($filter['id'])) value="{{$filter['id']}}" @endif></th>
                        <th>
                            <input type="date" class="form-control" name="from" id="from" style="width:150px;" @if(isset($filter['from'])) value="{{$filter['from']}}" @endif>
                        </th>
                        <th>
                            <input type="date" class="form-control" name="to" id="to" style="width:150px;" @if(isset($filter['to'])) value="{{$filter['to']}}" @endif>
                        </th>
                        <th>
                            <select name="status" id="status" class="form-control">
                                <option value="0">Все</option>
                                @foreach($statuses as $k => $v)
                                    <option value="{{ $v->code }}" @if(isset($filter['status']) && $filter['status'] == $v->code) selected @endif>{{ $v->title }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <input type="text" class="form-control" name="client" id="client" @if(isset($filter['client'])) value="{{$filter['client']}}" @endif>
                        </th>
                        <th>
                            <select name="payment" id="payment" class="form-control">
                                <option value="0">Все</option>
                                @foreach($payments as $k => $v)
                                    <option value="{{ $v->id }}" @if(isset($filter['payment']) && $filter['payment'] == $v->id) selected @endif>{{ $v->title }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <select name="shipment" id="shipment" class="form-control">
                                <option value="0">Все</option>
                                @foreach($shipments as $k => $v)
                                    <option value="{{ $v->id }}" @if(isset($filter['shipment']) && $filter['shipment'] == $v->id) selected @endif>{{ $v->title }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th></th>
                        <th>
                            <select name="manager" id="manager" class="form-control">
                                <option value="0">Все</option>
                                @foreach($managers as $k => $v)
                                    <option value="{{ $k }}">
                                        {{ $v }}
                                    </option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <button type="submit" class="btn btn-success"  title="Фильтровать"><i class="fas fa-filter"></i></button>
                        </th>
                        </form>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($orders) && count($orders) > 0)
                        @foreach($orders as $key => $order)
                            <tr id="row-{{ $order->id }}">
                                <td>
                                    <a href="/admin/orders/{{$order->id}}">{{ $order->id }}</a>
                                </td>
                                <td colspan="2">
                                    {{ date('H:i:s d.m.Y',strtotime($order->created_at)) }}
                                </td>
                                <td>
                                    @if(isset($order->status->title)) {{ $order->status->title }} @endif
                                     ({{ $order->status_id }})
                                </td>
                                <td>
                                    {{ $order->first_name }} {{ $order->last_name }} {{ $order->email }}
                                </td>
                                <td>
                                    @if(isset($order->payment->title)) {{ $order->payment->title }} @endif
                                </td>
                                <td>
                                    @if(isset($order->shipment->title)) {{ $order->shipment->title }} @endif
                                </td>
                                <td>
                                    {{ $order->total }}грн
                                </td>
                                <td>
                                    @if(isset($order->manager_id) && $order->manager_id > 0)
                                        {{ $order->manager->surname }} {{ $order->manager->name }}
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-info" href="/admin/orders/{{$order->id}}" role="button" title="Подробнее"><i class="fas fa-chevron-right"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            <div>
                {{ $orders->appends(request()->except('page'))->links() }}.
            </div>

        </div>
    </div>
    @section('scripts')
        @parent



        <!-- Modal -->
        <div class="modal fade" id="productDelModal" tabindex="-1" role="dialog" aria-labelledby="categoryDelModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="categoryDelModalLabel">Удалить тип оплаты</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="del-prod-id" value="0">
                        <p>Действительно удалить тип оплаты &laquo;<span id="del-prod-title"></span>&raquo;?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <button type="button" class="btn btn-danger btn-prod-delete">Удалить</button>
                    </div>
                </div>
            </div>
        </div>





        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

         /*   $('.btn-active-toggle').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var active = $(this).data('active');
                $.ajax({
                    type:'POST',
                    url:'/admin/categories-update-active',
                    data:{id:id,active:active},
                    success:function(data) {
                        if(data.id > 0){
                            if(data.active == 1){
                                $('#active-y-'+data.id).show();
                                $('#active-n-'+data.id).hide();
                            } else {
                                $('#active-y-'+data.id).hide();
                                $('#active-n-'+data.id).show();
                            }
                        }
                    }
                });
            });*/

            $('.btn-prod-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');
                $('#del-prod-id').val(id);
                $('#del-prod-title').html(title);
                $('#productDelModal').modal('show');
            });

            $('.btn-prod-delete').click(function(e) {
                e.preventDefault();
                var id = $('#del-prod-id').val();
                $.ajax({
                    type:'POST',
                    url:'/admin/payment-methods-delete',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){

                            $('#row-' + data.id).remove();
                            $('#del-prod-id').val('');
                            $('#del-prod-title').html('');
                            $('#productDelModal').modal('hide');

                        }
                    }
                });
            });







        </script>


    @endsection
@endsection
