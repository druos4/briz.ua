@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')


    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            <span style="font-size:16px;">Статусы заказов</span>
        </div>

        <div class="card-body">
            <form action="/admin/order-status/save" method="POST" enctype="multipart/form-data">
                @csrf
                <table class="table table-striped">
                    <thead>
                        <th>Статус</th>
                        <th>Активность</th>
                        <th>Переход в</th>
                    </thead>
                    <tbody>
                        @foreach($statuses as $key => $status)
                            <tr id="row-{{$status->id}}">
                                <td>
                                    {{ $status->title_ru }}
                                </td>
                                <td>
                                    <div class="toggle-btn @if($status->active == 1) active @endif">
                                        <input type="checkbox" class="cb-value" name="active[{{$status->id}}]" value="1" @if($status->active == 1) checked @endif />
                                        <span class="round-btn"></span>
                                    </div>
                                </td>
                                <td>
                                    <select name="goto[{{$status->id}}][]" class="form-control select2" multiple="multiple">
                                        @foreach($statuses as $k => $v)
                                            @if($v->id != $status->id)
                                                <option value="{{ $v->id }}" @if(in_array($v->id,explode(',',$status->goto))) selected @endif>{{ $v->title_ru }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <input class="btn btn-success" type="submit" value="Сохранить">
            </form>
        </div>
    </div>
    @section('scripts')
        @parent



        <!-- Modal -->
        <div class="modal fade" id="productDelModal" tabindex="-1" role="dialog" aria-labelledby="categoryDelModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="categoryDelModalLabel">Удалить тип оплаты</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="del-prod-id" value="0">
                        <p>Действительно удалить тип оплаты &laquo;<span id="del-prod-title"></span>&raquo;?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <button type="button" class="btn btn-danger btn-prod-delete">Удалить</button>
                    </div>
                </div>
            </div>
        </div>





        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('.cb-value').click(function() {
                var mainParent = $(this).parent('.toggle-btn');
                if($(mainParent).find('input.cb-value').is(':checked')) {
                    $(mainParent).addClass('active');
                    $(mainParent).find('input.cb-value').attr('checked', true);
                } else {
                    $(mainParent).removeClass('active');
                    $(mainParent).find('input.cb-value').attr('checked', false);
                }

            });



        </script>


    @endsection
@endsection
