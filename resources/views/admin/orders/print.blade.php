    <div class="card">
        <div class="card-header">
            <span style="float:left;"><b>Заказ № {{ $order->id }}</b></span>
            @if($order->manager_id > 0) <span style="float:right;">Менеджер: {{$order->manager->surname}} {{$order->manager->name}}</span> @endif
        </div>


        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <h3>Доставка и оплата</h3>
                    <table class="table">
                        <tr><td><b>Статус заказа:</b></td><td>
                                <form action="/admin/orders/{{$order->id}}/save-status" method="POST" enctype="multipart/form-data" style="display: inline-block;">
                                    @csrf
                                    <select name="status" id="status" style="display:inline-block; width:200px;" class="form-control">
                                        @foreach($statuses as $k => $v)

                                            @if(in_array($v->id,explode(',',$order->status->goto)) || $v->id == $order->status->id)
                                                <option value="{{ $v->code }}" @if($v->code == $order->status_id) selected @endif>{{ $v->title }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <input class="btn btn-success" type="submit" value="Сохранить">
                                </form>
                            </td></tr>
                        <tr><td><b>Тип доставки:</b></td><td>{{ $order->shipment->title }}</td></tr>
                        <tr><td><b>Город доставки:</b></td><td>{{ $order->delivery_city }}</td></tr>
                        <tr><td><b>Адрес доставки:</b></td><td>{{ $order->delivery_address }}</td></tr>
                        <tr><td><b>Тип оплаты:</b></td><td>{{ $order->payment->title }}</td></tr>
                        <tr><td><b>Промокод:</b></td><td>{{ $order->promocode }}</td></tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <h3>Данные клиента</h3>
                    <table class="table">
                        <tr><td><b>Фамилия:</b></td><td>{{ $order->first_name }}</td></tr>
                        <tr><td><b>Имя:</b></td><td>{{ $order->last_name }}</td></tr>
                        <tr><td><b>Отчество:</b></td><td>{{ $order->middle_name }}</td></tr>
                        <tr><td><b>E-mail:</b></td><td>{{ $order->email }}</td></tr>
                        <tr><td><b>Телефон:</b></td><td>{{ $order->phone }}</td></tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

        <div class="card">
            <div class="card-header">
                <b>Состав заказа</b>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <th>Фото</th>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>Количество</th>
                        <th>Сумма</th>
                    </thead>
                    <tbody>
                        @foreach($cart['products'] as $key => $row)
                            <tr>
                            @if(isset($row['product']['id']))

                                <td>
                                    <img src="{{ $row['product']['picture'] }}" alt="{{ $row['product']['title'] }}" class="img-fluid prod-photo" />
                                </td>
                                <td>
                                    {{ $row['product']['title'] }}
                                </td>
                                <td>
                                    {{ $row['price'] }}грн
                                </td>
                                <td>
                                    {{$row['qnt']}}шт.
                                </td>
                                <td>
                                    {{ $row['total'] }}грн
                                </td>

                            @else

                                <td colspan="2">
                                    @foreach($row['product'] as $prod)
                                        <div style="display: inline-block">
                                            @if($prod['picture'] != '')
                                                <img src="{{ $prod['picture'] }}" alt="{{ $prod['title' . getLocaleDBSuf()] }}" class="img-fluid prod-photo" />
                                            @endif
                                            <br />
                                                {{ $prod['title' . getLocaleDBSuf()] }}
                                        </div>
                                    @endforeach
                                </td>
                                <td>
                                    {{ $row['price'] }}грн
                                </td>
                                <td>
                                    {{$row['qnt']}}шт.
                                </td>
                                <td>
                                    {{ $row['total'] }}грн
                                </td>

                            @endif
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4"><b>Общая сумма</b></td>
                            <td><b>{{ $order->total }}грн</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    