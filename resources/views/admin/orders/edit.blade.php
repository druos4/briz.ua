@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .prod-photo{
            max-height:120px;
            max-width:120px;
        }
    </style>
@endsection
@section('content')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-default" href="/admin/orders">
                    <i class="fas fa-chevron-left"></i> Список заказов
                </a>
            </div>
        </div>
    <form action="/admin/orders/{{$order->id}}/save" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="card">
        <div class="card-header">
            <span style="float:left;"><b>Редактирование заказа № {{ $order->id }}</b></span>
            @if($order->manager_id > 0) <span style="float:right;">Менеджер: {{$order->manager->surname}} {{$order->manager->name}}</span> @endif
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <h3>Доставка и оплата</h3>
                    <table class="table">
                        <tr><td><b>Статус заказа:</b></td><td>
                                <select name="status_id" id="status_id" class="form-control">
                                    @foreach($statuses as $k => $v)
                                        @if(in_array($v->id,explode(',',$order->status->goto)) || $v->id == $order->status->id)
                                            <option value="{{ $v->code }}" @if($v->code == $order->status_id) selected @endif>{{ $v->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td></tr>
                        <tr><td><b>Тип доставки:</b></td><td>
                                <select name="shipment_id" id="shipment_id" class="form-control">
                                    @foreach($shipments as $k => $v)
                                        <option value="{{ $v->id }}" @if($v->id == $order->shipment_id) selected @endif>{{ $v->title }}</option>
                                    @endforeach
                                </select>
                            </td></tr>
                        <tr><td><b>Город доставки:</b></td><td>
                                <input type="text" name="delivery_city" id="delivery_city" class="form-control" value="{{ $order->delivery_city }}">

                            </td></tr>
                        <tr><td><b>Адрес доставки:</b></td><td>
                                <textarea name="delivery_address" id="delivery_address" class="form-control">{{ $order->delivery_address }}</textarea>
                            </td></tr>


                        <tr><td><b>Тип оплаты:</b></td><td>
                                <select name="payment_id" id="payment_id" class="form-control">
                                    @foreach($payments as $k => $v)
                                        <option value="{{ $v->id }}" @if($v->id == $order->payment_id) selected @endif>{{ $v->title }}</option>
                                    @endforeach
                                </select>

                            </td></tr>
                        <tr><td><b>Промокод:</b></td><td>
                                <input type="text" name="promocode" id="promocode" class="form-control" value="{{ $order->promocode }}">
                            </td></tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <h3>Данные клиента</h3>
                    <table class="table">
                        <tr><td><b>Фамилия:</b></td><td><input type="text" name="first_name" class="form-control" style="display: inline-block; width:300px;" value="{{ $order->first_name }}"></td></tr>
                        <tr><td><b>Имя:</b></td><td><input type="text" name="last_name" class="form-control" style="display: inline-block; width:300px;" value="{{ $order->last_name }}"></td></tr>
                        <tr><td><b>Отчество:</b></td><td><input type="text" name="middle_name" class="form-control" style="display: inline-block; width:300px;" value="{{ $order->middle_name }}"></td></tr>
                        <tr><td><b>E-mail:</b></td><td><input type="text" name="email" class="form-control" style="display: inline-block; width:300px;" value="{{ $order->email }}"></td></tr>
                        <tr><td><b>Телефон:</b></td><td><input type="text" name="phone" class="form-control" style="display: inline-block; width:300px;" value="{{ $order->phone }}"></td></tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

        <div class="card">
            <div class="card-header">
                <b>Состав заказа</b>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <th>Фото</th>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>Количество</th>
                        <th>Сумма</th>
                        <th></th>
                    </thead>
                    <tbody id="order-products">
                        @foreach($cart['products'] as $key => $row)
                            <tr>
                                @if(isset($row['product']['id']))

                                    <td>
                                        <img src="{{ $row['product']['picture'] }}" alt="{{ $row['product']['title'] }}" class="img-fluid prod-photo" />
                                    </td>
                                    <td>
                                        {{ $row['product']['title'] }}
                                    </td>
                                    <td>
                                        {{ $row['price'] }}грн
                                    </td>
                                    <td>
                                        <input type="number" style="width:100px; display: inline-block;" id="qnt-{{$row['id']}}" data-id="{{$row['id']}}"
                                               value="{{$row['qnt']}}" class="form-control cart-qnt">шт.
                                    </td>
                                    <td>
                                        <span id="cart-total-{{$row['id']}}">{{ $row['total'] }}</span>грн
                                    </td>
                                    <td>
                                        <a class="btn btn-danger btn-prod-del" data-id="{{$row['id']}}" data-title="{{$row['product']['title']}}" href="" role="button" title="Удалить товар"><i class="fas fa-trash"></i></a>
                                    </td>
                                @else

                                    <td colspan="2">
                                        @foreach($row['product'] as $prod)
                                            <div style="display: inline-block">
                                                @if($prod['picture'] != '')
                                                    <img src="{{ $prod['picture'] }}" alt="{{ $prod['title' . getLocaleDBSuf()] }}" class="img-fluid prod-photo" />
                                                @endif
                                                <br />
                                                {{ $prod['title' . getLocaleDBSuf()] }}
                                            </div>
                                        @endforeach
                                    </td>
                                    <td>
                                        {{ $row['price'] }}грн
                                    </td>
                                    <td>
                                        <input type="number" style="width:100px; display: inline-block;" id="qnt-{{$row['id']}}" data-id="{{$row['id']}}"
                                               value="{{$row['qnt']}}" class="form-control cart-qnt">шт.
                                    </td>
                                    <td>
                                        <span id="cart-total-{{$row['id']}}">{{ $row['total'] }}</span>грн
                                    </td>
                                    <td>
                                        <a class="btn btn-danger btn-prod-del" data-id="{{$row['id']}}" data-title="Комплект" href="" role="button" title="Удалить товар"><i class="fas fa-trash"></i></a>
                                    </td>
                                @endif
                            </tr>
                        @endforeach

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">
                                <b>Общая сумма</b>
                            </td>
                            <td>
                                <span id="order-total" style="font-weight: bold;">{{ $order->total }}грн<span/>
                            </td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
                <br />
                <h3>Добавить товар</h3>
                <table>
                    <tr>
                        <td>
                            Категория&nbsp;
                            <select name="category_id" id="category_id" class="form-control" style="display: inline-block; width:300px;">
                                <option value="0"> - укажите основную категорию - </option>
                                @if(isset($categories) && count($categories) > 0)
                                    <?php $a0 = 0; ?>
                                    @foreach($categories as $key => $category)
                                        <?php $a0++; ?>
                                        <option value="{{ $category->id }}">{{$a0}}. {{ $category->title }}</option>
                                        @if(count($category->children))
                                            <?php $a1 = 0; ?>
                                            @foreach($category->children as $child)
                                                <?php $a1++; ?>
                                                <option value="{{ $child->id }}">&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title }}</option>
                                                <?php $a2 = 0; ?>
                                                @foreach($child->children as $ch)
                                                    <?php $a2++; ?>
                                                    <option value="{{ $ch->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title }}</option>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </td>
                        <td>&nbsp;<i class="fas fa-chevron-right"></i>&nbsp;</td>
                        <td>
                            Товар&nbsp;
                            <select name="product_id" id="product_id" class="form-control" style="display: inline-block; width:300px;">

                            </select>
                        </td>
                        <td rowspan="2">
                            <a class="btn btn-info btn-add-product" href=""><i class="fas fa-plus"></i> Добавить</a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            Или артикул товара&nbsp;
                            <input type="text" id="article" style="display: inline-block; width:300px;" name="article" class="form-control" value="">
                        </td>
                    </tr>
                </table>



            </div>
        </div>

        <input class="btn btn-success" type="submit" value="Сохранить">&nbsp;&nbsp;
        <a class="btn btn-default" href="/admin/orders/{{$order->id}}">Отменить</a>

    </form>

    <br />
        <br />
        <br />
        <br />
        <br />
    @section('scripts')
        @parent



        <!-- Modal -->
        <div class="modal fade" id="productDelModal" tabindex="-1" role="dialog" aria-labelledby="categoryDelModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="categoryDelModalLabel">Удалить товар</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="del-prod-id" value="0">
                        <p>Действительно удалить товар &laquo;<span id="del-prod-title"></span>&raquo;?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <button type="button" class="btn btn-danger btn-prod-delete">Удалить</button>
                    </div>
                </div>
            </div>
        </div>





        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.btn-prod-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');
                $('#del-prod-id').val(id);
                $('#del-prod-title').html(title);
                $('#productDelModal').modal('show');
            });

            $('.btn-prod-delete').click(function(e) {
                e.preventDefault();
                var id = '{{$order->id}}';
                var sess = '{{$order->session_id}}';
                var cartid = $('#del-prod-id').val();
                $.ajax({
                    type:'POST',
                    url:'/admin/order-delete-product',
                    data:{id:id,sess:sess,cartid:cartid},
                    success:function(data) {
                        if(data.rowid > 0){
                            $('#order-total').html(data.carttotal + 'грн');
                            $('#cart-' + data.rowid).remove();
                            $('#del-prod-id').val('');
                            $('#del-prod-title').html('');
                            $('#productDelModal').modal('hide');
                        }
                    }
                });
            });

            $('.btn-add-product').click(function(e) {
                e.preventDefault();
                var id = '{{$order->id}}';
                var sess = '{{$order->session_id}}';
                var user = '{{$order->user_id}}';
                var product_id = $('#product_id').val();
                var article = $('#article').val();
                $('#category_id').val('0');
                $('#product_id').html('');

                $.ajax({
                    type:'POST',
                    url:'/admin/order-add-product',
                    data:{id:id,sess:sess,user:user,product_id:product_id,article:article},
                    success:function(data) {

                        $('#order-products').append(data.html);
                        $('#order-total').html(data.carttotal + 'грн');

                        $('#order-prod-del-' + data.rowid).click(function(e) {
                            e.preventDefault();
                            var id = data.rowid;
                            var title = data.title;
                            $('#del-prod-id').val(id);
                            $('#del-prod-title').html(title);
                            $('#productDelModal').modal('show');
                        });

                        $("#qnt-" + data.rowid).change(function(e) {
                            e.preventDefault();
                            var id = '{{$order->id}}';
                            var sess = '{{$order->session_id}}';
                            var user = '{{$order->user_id}}';
                            var val = $(this).val();
                            var cartid = $(this).data('id');
                            if(val < 1){
                                val = 1;
                                $(this).val(val);
                            }
                            $.ajax({
                                type:'POST',
                                url:'/admin/order-update-product',
                                data:{id:id,sess:sess,user:user,val:val,cartid:cartid},
                                success:function(data) {

                                    $('#cart-total-' + data.rowid).html(data.rowtotal);
                                    $('#order-total').html(data.carttotal + 'грн');

                                }
                            });
                        });
                    }
                });
            });

            $(".cart-qnt").change(function(e) {
                e.preventDefault();
                var id = '{{$order->id}}';
                var sess = '{{$order->session_id}}';
                var user = '{{$order->user_id}}';
                var val = $(this).val();
                var cartid = $(this).data('id');
                if(val < 1){
                    val = 1;
                    $(this).val(val);
                }
                $.ajax({
                    type:'POST',
                    url:'/admin/order-update-product',
                    data:{id:id,sess:sess,user:user,val:val,cartid:cartid},
                    success:function(data) {

                        $('#cart-total-' + data.rowid).html(data.rowtotal);
                        $('#order-total').html(data.carttotal + 'грн');

                    }
                });
            });


            $('#category_id').on('change', function() {
                var catid = $(this).val();
                $('#product_id').html('');
                $.ajax({
                    type:'POST',
                    url:'/admin/products/getByCatId',
                    data:{catid:catid},
                    success:function(data) {
                        if(data.html != ''){
                            $('#product_id').html(data.html);
                        }
                    }
                });
            });








        </script>


    @endsection
@endsection