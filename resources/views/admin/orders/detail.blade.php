@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .prod-photo{
            max-height:120px;
            max-width:120px;
        }
    </style>
@endsection
@section('content')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-default" href="/admin/orders">
                    <i class="fas fa-chevron-left"></i> Назад
                </a>
                <a class="btn btn-warning" href="/admin/orders/{{ $order->id }}/edit" role="button"><i class="fas fa-pen"></i> Редактировать заказ</a>
                &nbsp;&nbsp;<a class="btn btn-info" href="/admin/orders/{{ $order->id }}/print" role="button"><i class="fas fa-print"></i> Печать заказа</a>
            </div>
        </div>

    <div class="card">
        <div class="card-header">
            <span style="float:left;"><b>Заказ № {{ $order->id }}</b></span>
            @if($order->manager_id > 0) <span style="float:right;">Менеджер: {{$order->manager->surname}} {{$order->manager->name}}</span> @endif
        </div>


        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <h3>Доставка и оплата</h3>
                    <table class="table">
                        <tr><td><b>Статус заказа:</b></td><td>
                                @if(isset($order->status->goto))
                                <form action="/admin/orders/{{$order->id}}/save-status" method="POST" enctype="multipart/form-data" style="display: inline-block;">
                                    @csrf
                                    <select name="status" id="status" style="display:inline-block; width:200px;" class="form-control">
                                        @foreach($statuses as $k => $v)

                                            @if(in_array($v->id,explode(',',$order->status->goto)) || $v->id == $order->status->id)
                                                <option value="{{ $v->code }}" @if($v->code == $order->status_id) selected @endif>{{ $v->title }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <input class="btn btn-success" type="submit" value="Сохранить">
                                </form>
                                    @endif
                            </td></tr>
                        <tr><td><b>Тип доставки:</b></td><td>{{ $order->shipment->title }}</td></tr>
                        <tr><td><b>Город доставки:</b></td><td>{{ $order->delivery_city }}</td></tr>
                        <tr><td><b>Адрес доставки:</b></td><td>{{ $order->delivery_address }}</td></tr>
                        <tr><td><b>Тип оплаты:</b></td><td>{{ $order->payment->title }}</td></tr>
                        <tr><td><b>Промокод:</b></td><td>{{ $order->promocode }}</td></tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <h3>Данные клиента</h3>
                    <table class="table">
                        <tr><td><b>Фамилия:</b></td><td>{{ $order->first_name }}</td></tr>
                        <tr><td><b>Имя:</b></td><td>{{ $order->last_name }}</td></tr>
                        <tr><td><b>Отчество:</b></td><td>{{ $order->middle_name }}</td></tr>
                        <tr><td><b>E-mail:</b></td><td>{{ $order->email }}</td></tr>
                        <tr><td><b>Телефон:</b></td><td>{{ $order->phone }}</td></tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

        <div class="card">
            <div class="card-header">
                <b>Состав заказа</b>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <th>Фото</th>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>Количество</th>
                        <th>Сумма</th>
                    </thead>
                    <tbody>
                        @foreach($cart['products'] as $key => $row)
                            <tr>
                            @if(isset($row['product']['id']))

                                <td>
                                    <img src="{{ $row['product']['picture'] }}" alt="{{ $row['product']['title'] }}" class="img-fluid prod-photo" />
                                </td>
                                <td>
                                    {{ $row['product']['title'] }}
                                </td>
                                <td>
                                    {{ $row['price'] }}грн
                                </td>
                                <td>
                                    {{$row['qnt']}}шт.
                                </td>
                                <td>
                                    {{ $row['total'] }}грн
                                </td>

                            @else

                                <td colspan="2">
                                    @foreach($row['product'] as $prod)
                                        <div style="display: inline-block">
                                            @if($prod['picture'] != '')
                                                <img src="{{ $prod['picture'] }}" alt="{{ $prod['title' . getLocaleDBSuf()] }}" class="img-fluid prod-photo" />
                                            @endif
                                            <br />
                                                {{ $prod['title' . getLocaleDBSuf()] }}
                                        </div>
                                    @endforeach
                                </td>
                                <td>
                                    {{ $row['price'] }}грн
                                </td>
                                <td>
                                    {{$row['qnt']}}шт.
                                </td>
                                <td>
                                    {{ $row['total'] }}грн
                                </td>

                            @endif
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="4"><b>Общая сумма</b></td>
                            <td><b>{{ $order->total }}грн</b></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>



    @section('scripts')
        @parent



        <!-- Modal -->
        <div class="modal fade" id="productDelModal" tabindex="-1" role="dialog" aria-labelledby="categoryDelModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="categoryDelModalLabel">Удалить тип оплаты</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="del-prod-id" value="0">
                        <p>Действительно удалить тип оплаты &laquo;<span id="del-prod-title"></span>&raquo;?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <button type="button" class="btn btn-danger btn-prod-delete">Удалить</button>
                    </div>
                </div>
            </div>
        </div>





        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

         /*   $('.btn-active-toggle').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var active = $(this).data('active');
                $.ajax({
                    type:'POST',
                    url:'/admin/categories-update-active',
                    data:{id:id,active:active},
                    success:function(data) {
                        if(data.id > 0){
                            if(data.active == 1){
                                $('#active-y-'+data.id).show();
                                $('#active-n-'+data.id).hide();
                            } else {
                                $('#active-y-'+data.id).hide();
                                $('#active-n-'+data.id).show();
                            }
                        }
                    }
                });
            });*/

            $('.btn-prod-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');
                $('#del-prod-id').val(id);
                $('#del-prod-title').html(title);
                $('#productDelModal').modal('show');
            });

            $('.btn-prod-delete').click(function(e) {
                e.preventDefault();
                var id = $('#del-prod-id').val();
                $.ajax({
                    type:'POST',
                    url:'/admin/payment-methods-delete',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){

                            $('#row-' + data.id).remove();
                            $('#del-prod-id').val('');
                            $('#del-prod-title').html('');
                            $('#productDelModal').modal('hide');

                        }
                    }
                });
            });







        </script>


    @endsection
@endsection
