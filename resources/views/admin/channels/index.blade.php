@extends('layouts.admin')
@section('title')
    Каналы
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .cross-line{
            text-decoration: line-through;
        }
        .genres{
            display: inline-block;
        }
    </style>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">

            <a class="btn btn-info" href="/admin" role="button"><i class="fas fa-sync"></i> Синхронизировать</a>

        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Фильтр
        </div>

        <div class="card-body">

            <form id="form-filter" action="/admin/channels" method="GET" enctype="multipart/form-data">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label>ID</label>
                        <input type="number" min="1" class="form-control w-80" name="id" @if(!empty($filter['id'])) value="{{ $filter['id'] }}" @endif>
                    </div>

                    <div class="col-auto">
                        <label>Название</label>
                        <input type="text" class="form-control w-160" name="title" @if(!empty($filter['title'])) value="{{ $filter['title'] }}" @endif>
                    </div>

                    @if(!empty($tariffs))
                        <div class="col-auto">
                            <label>Пакет</label>
                            <select class="form-control" name="tariff">
                                <option value="">Все</option>
                                @foreach($tariffs as $key => $tariff)
                                    <option value="{{ $tariff->id }}" @if(isset($filter['tariff']) && $filter['tariff'] == $tariff->id) selected @endif>{{ $tariff->title_ru }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    @if(!empty($genres))
                        <div class="col-auto">
                            <label>Жанр</label>
                            <select class="form-control" name="genre">
                                <option value="">Все</option>
                                @foreach($genres as $key => $genre)
                                    <option value="{{ $genre->id }}" @if(isset($filter['genre']) && $filter['genre'] == $genre->id) selected @endif>{{ $genre->title_ru }} ({{ $genre->code }})</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="col-auto">
                        <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/channels?reset=y" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Каналы
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>

                            <th style="min-width: 150px">
                                Название
                                @if(!empty($sort) && $sort['field'] == 'title' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'title' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="title" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="title" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="title" data-direction="desc"></i>
                            </th>
                            <th>Лого</th>
                            <th>Сталкер Лого</th>
                            <th>Тарифы</th>
                            <th>Жанр</th>


                            <th style="min-width: 60px">ID
                                @if(!empty($sort) && $sort['field'] == 'id' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'id' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="id" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="id" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="id" data-direction="desc"></i>
                            </th>


                        </tr>
                    </thead>
                    <tbody>

                        @if(!empty($items))
                            @foreach($items as $key => $item)
                                <tr id="item-{{ $item->id }}">
                                    <td>
                                        {{ $item->title }} @if($item->is_analog == 1) <span class="badge badge-primary">аналоговый</span> @endif
                                    </td>
                                    <td>
                                        @if(!empty($item->logo))
                                            <img src="{{ $item->logo }}" style="max-width: 100px; max-height: 60px;" />
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ $item->billing_logo }}" target="_blank">
                                            <img src="{{ $item->billing_logo }}" style="max-width: 100px; max-height: 60px;" />
                                        </a>
                                    </td>
                                    <td>
                                        @if(!empty($item->tariffs))
                                            @foreach($item->tariffs as $tariff)
                                                <span class="badge badge-info">{{ $tariff->title_ru }}</span><br />
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        <div id="genres-{{ $item->id }}" class="genres">
                                        @if(!empty($item->genres))
                                            <?php $arr = []; ?>
                                            @foreach($item->genres as $genre)
                                                @if(!in_array($genre->id,$arr))
                                                    <?php $arr[] = $genre->id; ?>
                                                <span class="badge badge-info">{{ $genre->title_ru }} ({{ $genre->code }})</span>
                                                @endif
                                            @endforeach
                                        @endif
                                        </div>
                                        {{--<a class="btn btn-warning btn-genre-edit btn-sm" href="#" role="button" title="Редактировать жанр" data-id="{{ $item->id }}" data-title="{{ $item->title }}"><i class="fas fa-pen"></i></a>--}}


                                    </td>
                                    <td>
                                        {{ $item->id }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif

                    </tbody>
                </table>
            </div>
            <div>
                @if(!empty($items))
                    {{ $items->appends(request()->except('page'))->links() }}
                @endif
            </div>
        </div>
    </div>


    @section('scripts')

        <div class="modal fade" id="genreModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Редактировать жанр</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="genre-form">
                            <input type="hidden" name="channel_id" id="channel_id">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Укажите жанр для канала &laquo;<span id="channel-title"></span>&raquo;</label>
                                <select class="form-control" id="genre_id" name="genre_id">

                                </select>
                            </div>

                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                            <button type="button" class="btn btn-success btn-genre-save">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>




        <script>
            $('.i-sort').click(function (e) {
                e.preventDefault();
                var field = $(this).data('field');
                var direction = $(this).data('direction');
                var form = $('#form-filter').serialize();
                form = form + '&sort=' + field + '&direction=' + direction;
                window.location.href = "/admin/channels?" + form;
            });

            $('.btn-genre-edit').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');


                $.ajax({
                    type:'POST',
                    url:'/admin/channels/get-genres',
                    data:{},
                    success:function(data) {
                        $('#genre_id').html(data.html);
                    }
                });

                $('#channel_id').val(id);
                $('#channel-title').html(title);

                $('#genreModal').modal('show');
            });

            $('.btn-genre-save').click(function (e) {
                e.preventDefault();
                var form = $('#genre-form').serialize();
                $.ajax({
                    type:'POST',
                    url:'/admin/channels/update',
                    data:form,
                    success:function(data) {

                        $('#genres-' + data.id).html('');
                        if(data.update == true){
                            $('#genres-' + data.id).html('<span class="badge badge-info">' + data.title + '</span>');
                        }

                        $('#genreModal').modal('hide');

                    }
                });
            });

        </script>
    @endsection
@endsection
