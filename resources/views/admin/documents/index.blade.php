@extends('layouts.admin')
@section('title')
    Документы
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .table-responsive{
            padding-bottom: 50px;
        }
    </style>
@endsection
@section('content')
    @can('tag_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.documents.create") }}">
                    <i class="fas fa-plus"></i> Создать Документ
                </a>
            </div>
        </div>
    @endcan


    <div class="card">
        <div class="card-header">
            Документы
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="min-width: 60px"></th>
                            <th style="min-width: 150px">Название RU</th>
                            <th style="min-width: 150px">Название UA</th>
                            <th style="min-width: 150px">Название EN</th>
                            <th>Активность</th>
                            <th>Сортировка</th>
                            <th>Ссылка</th>
                            <th style="min-width: 60px">ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($documents as $key => $item)
                            <tr id="item-{{ $item->id }}">
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            @can('tag_edit')
                                                <a class="dropdown-item" href="/admin/documents/{{$item->id}}/edit"><i class="fas fa-pencil-alt"></i> Редактировать</a>
                                            @endcan
                                            @can('tag_delete')
                                                <a class="dropdown-item btn-item-del" href="" target="_blank" data-id="{{$item->id}}" data-title="{{ $item->title_ru }} [{{ $item->id }}]" data-url="/admin/documents/{{ $item->id }}"><i class="fas fa-trash"></i> Удалить</a>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="/admin/documents/{{$item->id}}/edit" title="Редактировать">{{ $item->title_ru ?? '' }}</a>
                                </td>
                                <td>
                                    <a href="/admin/documents/{{$item->id}}/edit" title="Редактировать">{{ $item->title_ua ?? '' }}</a>
                                </td>
                                <td>
                                    <a href="/admin/documents/{{$item->id}}/edit" title="Редактировать">{{ $item->title_en ?? '' }}</a>
                                </td>
                                <td>
                                    @if($item->active == true)
                                        <span class="badge badge-success">Да</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td>
                                    {{ $item->sort }}
                                </td>
                                <td>
                                    @if($item->link != '')
                                        <a class="btn btn-secondary btn-sm" href="{{ $item->link }}" role="button" target="_blank">Открыть <i class="fas fa-share"></i></a>
                                    @endif
                                </td>
                                <td>
                                    <a href="/admin/documents/{{$item->id}}/edit" title="Редактировать">{{ $item->id }}</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div>
                {{ $documents->appends(request()->except('page'))->links() }}
            </div>
        </div>
    </div>


    @section('scripts')
        <script>

        </script>
    @endsection
@endsection
