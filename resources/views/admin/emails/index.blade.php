@extends('layouts.admin')
@section('title')
    Emails
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .select2{
            /*min-width:150px!important;*/
            width: 200px!important;
        }
    </style>
@endsection
@section('content')

    <div class="card">
        <div class="card-header">
            Фильтр
        </div>

        <div class="card-body">

            <form id="form-filter" action="{{ route("admin.emails.index") }}" method="GET" enctype="multipart/form-data">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label>Emails</label>
                        <input type="text" class="form-control w-160" name="email" @if(!empty($filter['email'])) value="{{ $filter['email'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>Дата добавления</label>
                        с <input type="date" class="form-control" id="date-from" name="date-from" @if(!empty($filter['date-from'])) value="{{ $filter['date-from'] }}" @endif> по (включительно) <input type="date" id="date-to" class="form-control" name="date-to" @if(!empty($filter['date-to'])) value="{{ $filter['date-to'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <button id="submit-filter-btn" type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/emails" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <div class="card">
        <div class="card-header">
            Emails
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Сохранено</th>
                            <th style="min-width: 60px">ID</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($emails as $key => $item)
                        <tr id="item-{{ $item->id }}">
                            <td>
                                {{ $item->email }}
                            </td>
                            <td>
                                {{ $item->created_at }}
                            </td>
                            <td>
                                {{ $item->id }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div>
            @if(!empty($emails))
                {{ $emails->appends(request()->except('page'))->links() }}
            @endif
            </div>
        </div>
    </div>

@endsection
