@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
    </style>
@endsection
@section('content')
    <a class="btn btn-default" href="{{ route('admin.payments.index') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a>
    <div class="card">
        <div class="card-header bg-info">
            Редактирование способа оплаты
        </div>

        <div class="card-body">
            <form action="{{ route('admin.payments.update',$payment->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                            <label for="title_ru">Название RU*</label>
                            <input type="text" id="title_ru" name="title_ru" class="form-control" required value="{{ old('title_ru', isset($payment) ? $payment->title_ru : '') }}">
                            @if($errors->has('title_ru'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('title_ru') }}
                                </em>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                            <label for="title_en">Название UA*</label>
                            <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', isset($payment) ? $payment->title_ua : '') }}">
                            @if($errors->has('title_ua'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('title_ua') }}
                                </em>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                            <label for="title_en">Название EN*</label>
                            <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', isset($payment) ? $payment->title_en : '') }}">
                            @if($errors->has('title_en'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('title_en') }}
                                </em>
                            @endif
                        </div>


                        <h3>Описание RU</h3>
                        <textarea name="description_ru" id="description_ru" class="form-control" rows="3"><?=$payment->description_ru?></textarea>
                        <br /><br />
                        <h3>Описание UA</h3>
                        <textarea name="description_ua" id="description_ua" class="form-control" rows="3"><?=$payment->description_ua?></textarea>
                        <br /><br />
                        <h3>Описание EN</h3>
                        <textarea name="description_en" id="description_en" class="form-control" rows="3"><?=$payment->description_en?></textarea>
                        <br /><br />


                    </div>
                    <div class="col-md-4">

                        <div class="form-group">
                            <div class="toggle-label">Активность</div>
                            <div class="toggle-btn @if($payment->active) active @endif">
                                <input type="checkbox" class="cb-value" name="active" value="1" @if($payment->active) checked @endif />
                                <span class="round-btn"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="toggle-label">Есть комиссия</div>
                            <div class="toggle-btn @if($payment->commission) active @endif">
                                <input type="checkbox" class="cb-value" name="commission" value="1" @if($payment->commission) checked @endif />
                                <span class="round-btn"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="picture">Сортировка</label>
                            <input type="number" class="form-control" name="sort" min="1" max="999999" value="{{ old('sort', $payment->sort) }}">
                        </div>

                        <div class="form-group">
                            <label for="picture">Иконка</label>
                            <input type="file" class="form-control-file" name="image" id="image">
                            @if($payment->icon != '')
                                <br /><img class="" id="preview-picture" src="{{$payment->icon}}" alt="preview-picture" style="max-width:120px; max-height:120px;"><br />
                                <input type="checkbox" name="del_pic" value="1"> удалить
                            @endif

                        </div>
                        {{--<div class="form-group">
                            <label for="picture">Активная иконка</label>
                            <input type="file" class="form-control-file" name="image_active" id="image_active">
                            @if($payment->icon_active != '')
                                <br /><img class="" id="preview-picture_active" src="{{$payment->icon_active}}" alt="preview-picture_active" style="max-width:120px; max-height:120px;"><br />
                                <input type="checkbox" name="del_pic_active" value="1"> удалить
                            @endif

                        </div>
                        --}}
                    </div>
                </div>

                <div>
                    <input class="btn btn-success" type="submit" value="Сохранить">
                    <a class="btn btn-default" href="{{ route('admin.payments.index') }}" role="button">Отменить</a>
                </div>
            </form>
        </div>
    </div>












@endsection
@section('scripts')



    <script>
        CKEDITOR.replace( 'description_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'description_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'description_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>

    <script>
        $('.cb-value').click(function() {
            var mainParent = $(this).parent('.toggle-btn');
            if($(mainParent).find('input.cb-value').is(':checked')) {
                $(mainParent).addClass('active');
                $(mainParent).find('input.cb-value').attr('checked', true);
            } else {
                $(mainParent).removeClass('active');
                $(mainParent).find('input.cb-value').attr('checked', false);
            }

        });
    </script>


@endsection
