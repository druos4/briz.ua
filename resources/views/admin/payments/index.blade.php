@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')
    @can('category_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.payments.create") }}">
                    <i class="fas fa-plus"></i> Создать метод оплаты
                </a>
            </div>
        </div>
    @endcan
    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Методы оплаты
        </div>

        <div class="card-body">
            <table class="table table-striped">
                <thead>
                <th></th>
                <th>Иконка</th>
                <th>Названия</th>
                <th>Активность</th>
                <th>Комиссия</th>
                <th>Сортировка</th>
                <th>ID</th>
                </thead>
                <tbody>
                @if(!empty($payments))
                    @foreach($payments as $key => $item)
                        <tr id="item-{{ $item->id }}">
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="/admin/payments/{{$item->id}}/edit">Редактировать</a>
                                        <div class="dropdown-divider"></div>
                                        @can('payments_delete')
                                            <a class="dropdown-item btn-item-del" href=""
                                               data-id="{{$item->id}}" data-title="{{ $item->title_ru }} [{{ $item->id }}]" data-url="/admin/payments/{{ $item->id }}"
                                               role="button" title="Удалить"><i class="fas fa-trash"></i> Удалить</a>
                                        @endcan
                                    </div>
                                </div>
                            </td>
                            <td>
                                @if($item->icon != '')
                                    <img src="{{$item->icon}}" style="max-width:60px; max-height:60px;" />
                                @endif
                                   {{--
                                    @if($item->icon_active != '')
                                        <img src="{{$item->icon_active}}" style="max-width:60px; max-height:60px;" />
                                    @endif
                                    --}}
                            </td>
                            <td>
                                {{ $item->title_ru }}<br />
                                {{ $item->title_ua }}<br />
                                {{ $item->title_en }}
                            </td>
                            <td>
                                @if($item->active == 1)
                                    <span class="badge badge-success">Да</span>
                                @else
                                    <span class="badge badge-danger">Нет</span>
                                @endif
                            </td>
                            <td>
                                @if($item->commission == 1)
                                    <span class="badge badge-danger">Есть</span>
                                @else
                                    <span class="badge badge-secondary">Нет</span>
                                @endif
                            </td>
                            <td>
                                {{ $item->sort }}
                            </td>
                            <td>
                                {{ $item->id }}
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>

        </div>
    </div>
@section('scripts')
    @parent


    <script>



    </script>


@endsection
@endsection
