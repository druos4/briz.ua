@extends('layouts.admin')
@section('title')
    Создание прайса
@endsection
@section('styles')
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
    </style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        Создание прайса
    </div>

    <div class="card-body">
        <form action="{{ route("admin.prices.store") }}" id="form-create" method="POST" enctype="multipart/form-data">
            @csrf


                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ua">Заголовок UA*</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua') }}">
                                @if($errors->has('title_ua'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок EN*</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="form-group">
                                <div class="toggle-label">Активность</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="active" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="slug">Сортировка</label>
                                <input type="number" id="sort" name="sort" class="form-control" min="1" max="999999" value="{{ old('sort') }}">
                            </div>

                            <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                                <label for="image">Документ</label><br />
                                <label class="label" data-toggle="tooltip" title="Загрузить документ">
                                    <input type="file" class="" name="file">
                                </label>
                                @if($errors->has('file'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('file') }}
                                    </em>
                                @endif
                            </div>

                        </div>
                    </div>





            <div>
                <button class="btn btn-success btn-create-submit" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/prices" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>

@endsection

@section('scripts')
@endsection
