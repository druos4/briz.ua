@extends('layouts.admin')
@section('title')
    Создание баннера
@endsection
@section('styles')
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <link href="{{ asset('cropper/cropper.css') }}" rel="stylesheet" />
    <style>
        .hidden{
            display: none;
        }
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
        .color-btn{
            width: 36px;
            height: 36px;
            display: inline-block;
            border-radius: 50%;
            cursor:pointer;
        }
        .color-282828{
            background: #282828;
            border: 1px solid #000000;
        }
        .color-ffffff{
            background: #ffffff;
            border: 1px solid #000000;
        }
        .color-00a3a4{
            background: #00A3A4;
            border: 1px solid #000000;
        }
        .color-fac23d{
            background: #FAC23D;
            border: 1px solid #000000;
        }
    </style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        Создание баннера
    </div>

    <div class="card-body">
        <form action="{{ route("admin.sliders.store") }}" id="form-create" method="POST" enctype="multipart/form-data">
            @csrf
                    <div class="row">
                        <div class="col-md-8">

                                    <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                        <label for="title_ru">Заголовок RU</label>
                                        <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" value="{{ old('title_ru') }}">
                                        @if($errors->has('title_ru'))
                                            <em class="invalid-feedback">
                                                {{ $errors->first('title_ru') }}
                                            </em>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                        <label for="title_ru">Заголовок UA</label>
                                        <input type="text" id="title_ua" name="title_ua" class="form-control" value="{{ old('title_ua') }}">
                                    </div>
                                    <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                        <label for="title_en">Заголовок EN</label>
                                        <input type="text" id="title_en" name="title_en" class="form-control" value="{{ old('title_en') }}">
                                    </div>


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Сортировка</label>
                                        <input type="number" class="form-control" name="sort" min="1" max="99999" value="{{ old('sort',10) }}" style="width: 120px;" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                        <label for="title_ru">Цвет заголовка</label>
                                        <input type="color" id="title_color" name="title_color" class="form-control" value="{{ old('title_color') }}">
                                    </div>
                                    <div class="color-btns">
                                        <div class="color-btn color-282828" data-target="title_color" data-color="#282828"></div>
                                        <div class="color-btn color-ffffff" data-target="title_color" data-color="#ffffff"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group {{ $errors->has('anons_color') ? 'has-error' : '' }}">
                                        <label for="anons_color">Цвет анонса</label>
                                        <input type="color" id="anons_color" name="anons_color" class="form-control" value="{{ old('anons_color') }}">
                                    </div>
                                    <div class="color-btns">
                                        <div class="color-btn color-282828" data-target="anons_color" data-color="#282828"></div>
                                        <div class="color-btn color-ffffff" data-target="anons_color" data-color="#ffffff"></div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="toggle-label">Активность</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="active" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            {{--
                            <div class="form-group">
                                <div class="toggle-label">Показывать на основной части</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="show_on_main" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                                <br />
                                <div class="toggle-label">Показывать на оборудовании</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="show_on_shop" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>
                            --}}
                            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                <label for="image_ru">Картинка* (1905х520px)</label><br />
                                <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                                    <input type="file" class="" name="image" accept="image/*">
                                </label>
                                @if($errors->has('image'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('image') }}
                                    </em>
                                @endif
                            </div>


                            <div class="btn-more">

                                <div class="form-group" style="margin-bottom: 0;">
                                    <div class="toggle-label">Кнопка подробнее</div>
                                    <div class="toggle-btn">
                                        <input type="checkbox" class="cb-value" id="btn-more-ch" name="btn-more-ch" value="1" />
                                        <span class="round-btn"></span>
                                    </div>
                                </div>


                                <div class="btn-more-options hidden">
                                    <div class="form-group">
                                        <label for="slug">URL</label>
                                        <input type="text" id="url" name="url" class="form-control" value="{{ old('url') }}">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('btn-title-color') ? 'has-error' : '' }}">
                                                <label for="btn_title_color">Цвет заголовка кнопки</label>
                                                <input type="color" id="btn_title_color" name="btn_title_color" class="form-control" value="{{ old('btn_title_color') }}">
                                            </div>
                                            <div class="color-btns">
                                                <div class="color-btn color-282828" data-target="btn_title_color" data-color="#282828"></div>
                                                <div class="color-btn color-ffffff" data-target="btn_title_color" data-color="#ffffff"></div>
                                                <div class="color-btn color-00a3a4" data-target="btn_title_color" data-color="#00a3a4"></div>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('btn-bg-color') ? 'has-error' : '' }}">
                                                <label for="btn_bg_color">Цвет фона кнопки</label>
                                                <input type="color" id="btn_bg_color" name="btn_bg_color" class="form-control" value="{{ old('btn_bg_color','#00A3A4') }}">
                                            </div>
                                            <div class="color-btns">
                                                <div class="color-btn color-282828" data-target="btn_bg_color" data-color="#282828"></div>
                                                <div class="color-btn color-ffffff" data-target="btn_bg_color" data-color="#ffffff"></div>
                                                <div class="color-btn color-00a3a4" data-target="btn_bg_color" data-color="#00a3a4"></div>
                                                <div class="color-btn color-fac23d" data-target="btn_bg_color" data-color="#fac23d"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                  <div class="row">
                    <div class="col-4">
                    <label for="anons_ru">Анонс RU</label>
                    <textarea name="anons_ru" id="anons_ru" class="form-control" rows="12"></textarea>
                    </div>
                    <div class="col-4">
                    <label for="anons_ua">Анонс UA</label>
                    <textarea name="anons_ua" id="anons_ua" class="form-control" rows="12"></textarea>
                    </div>
                    <div class="col-4">
                        <label for="anons_ua">Анонс EN</label>
                        <textarea name="anons_en" id="anons_en" class="form-control" rows="12"></textarea>
                    </div>
                </div>
            <div class="row">
                <div class="col-12">
                    <button class="btn btn-success btn-create-submit" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                    <a href="/admin/sliders" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
                </div>
            </div>

        </form>
    </div>
</div>

@endsection

@section('scripts')


    <script>
        CKEDITOR.replace( 'anons_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });

        CKEDITOR.replace( 'anons_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });

        CKEDITOR.replace( 'anons_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });



        $('#btn-more-ch').change(function (e) {
            if ( $(this).is(':checked') ) {

                $('.btn-more-options').removeClass('hidden');
            } else {
                $('.btn-more-options').addClass('hidden');

            }
        });

        $('.color-btn').click(function (e){
            e.preventDefault();
            var target = $(this).data('target');
            var color = $(this).data('color');
            $('#' + target).val(color);
        });
    </script>
@endsection
