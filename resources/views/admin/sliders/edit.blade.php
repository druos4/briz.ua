@extends('layouts.admin')
@section('title')
    Редактирование баннера
@endsection
@section('styles')
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <link href="{{ asset('cropper/cropper.css') }}" rel="stylesheet" />
    <style>

        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
        .hidden{
            display: none;
        }


        .color-btn{
            width: 36px;
            height: 36px;
            display: inline-block;
            border-radius: 50%;
            cursor:pointer;
        }
        .color-282828{
            background: #282828;
            border: 1px solid #000000;
        }
        .color-ffffff{
            background: #ffffff;
            border: 1px solid #000000;
        }
        .color-00a3a4{
            background: #00A3A4;
            border: 1px solid #000000;
        }
          .color-fac23d{
            background: #FAC23D;
            border: 1px solid #000000;
        }
    </style>
@endsection

@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-secondary" href="{{ route("admin.sliders.index") }}">
                <i class="fas fa-chevron-left"></i> Назад
            </a>
        </div>
    </div>

<div class="card">

    <div class="card-header">
        Редактирование баннера
    </div>

    <div class="card-body">
        <form action="{{ route("admin.sliders.update", [$slider->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')


            <div class="row">
                <div class="col-md-8">

                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" value="{{ old('title_ru', ($slider->title_ru) ? $slider->title_ru : '') }}">
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок UA</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" value="{{ old('title_ua', ($slider->title_ua) ? $slider->title_ua : '') }}">
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_en">Заголовок EN</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" value="{{ old('title_en', ($slider->title_en) ? $slider->title_en : '') }}">
                            </div>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Сортировка</label>
                                <input type="number" class="form-control" name="sort" min="1" max="99999" value="{{ old('sort', ($slider->sort) ? $slider->sort : '') }}" style="width: 120px;" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_color">Цвет заголовка</label>
                                <input type="color" id="title_color" name="title_color" class="form-control" value="{{ old('title_color', ($slider->title_color) ? $slider->title_color : '#282828') }}">
                            </div>
                            <div class="color-btns">
                                <div class="color-btn color-282828" data-target="title_color" data-color="#282828"></div>
                                <div class="color-btn color-ffffff" data-target="title_color" data-color="#ffffff"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="anons_color">Цвет анонса</label>
                                <input type="color" id="anons_color" name="anons_color" class="form-control" value="{{ old('anons_color', ($slider->anons_color) ? $slider->anons_color : '#282828') }}">
                            </div>
                            <div class="color-btns">
                                <div class="color-btn color-282828" data-target="anons_color" data-color="#282828"></div>
                                <div class="color-btn color-ffffff" data-target="anons_color" data-color="#ffffff"></div>
                            </div>
                        </div>
                    </div>




                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="toggle-label">Активность</div>
                        <div class="toggle-btn @if($slider->active == 1) active @endif">
                            <input type="checkbox" class="cb-value" name="active" value="1" @if($slider->active == 1) checked @endif />
                            <span class="round-btn"></span>
                        </div>
                    </div>
{{--
                    <div class="form-group">
                        <div class="toggle-label">Показывать на основной части</div>
                        <div class="toggle-btn @if($slider->show_on_main == 1) active @endif">
                            <input type="checkbox" class="cb-value" name="show_on_main" value="1" @if($slider->show_on_main == 1) checked @endif />
                            <span class="round-btn"></span>
                        </div>
                        <br />
                        <div class="toggle-label">Показывать на оборудовании</div>
                        <div class="toggle-btn @if($slider->show_on_shop == 1) active @endif">
                            <input type="checkbox" class="cb-value" name="show_on_shop" value="1" @if($slider->show_on_shop == 1) checked @endif />
                            <span class="round-btn"></span>
                        </div>
                    </div>
--}}
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label for="picture">Картинка (1905x520px)</label><br />
                        <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                            <input type="file" class="" name="image" accept="image/*">
                        </label>
                        @if($errors->has('image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('image') }}
                            </em>
                        @endif
                        @if(isset($slider->picture_phone) && $slider->picture_phone != '')
                            <br /><img class="" id="preview-picture_ru" src="{{$slider->picture_phone}}" alt="preview-picture" style="max-width:120px; max-height:120px;"><br />
                            <input type="checkbox" name="del-pic" value="1"> удалить
                        @endif
                    </div>


                    <div class="btn-more">

                        <div class="form-group" style="margin-bottom: 0;">
                            <div class="toggle-label">Кнопка подробнее</div>
                            <div class="toggle-btn @if(!empty($slider->url)) active @endif">
                                <input type="checkbox" class="cb-value" id="btn-more-ch" name="btn-more-ch" value="1" @if(!empty($slider->url)) checked @endif />
                                <span class="round-btn"></span>
                            </div>
                        </div>


                        <div class="btn-more-options @if(empty($slider->url)) hidden @endif">
                            <div class="form-group">
                                <label for="slug">URL</label>
                                <input type="text" id="url" name="url" class="form-control" value="{{ old('url', ($slider->url) ? $slider->url : '') }}">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('btn_title_color') ? 'has-error' : '' }}">
                                        <label for="btn_title_color">Цвет заголовка кнопки</label>
                                        <input type="color" id="btn_title_color" name="btn_title_color" class="form-control" value="{{ old('btn_title_color', ($slider->btn_title_color) ? $slider->btn_title_color : '#282828') }}">
                                    </div>
                                    <div class="color-btns">
                                        <div class="color-btn color-282828" data-target="btn_title_color" data-color="#282828"></div>
                                        <div class="color-btn color-ffffff" data-target="btn_title_color" data-color="#ffffff"></div>
                                        <div class="color-btn color-00a3a4" data-target="btn_title_color" data-color="#00a3a4"></div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('btn_bg_color') ? 'has-error' : '' }}">
                                        <label for="btn_bg_color">Цвет фона кнопки</label>
                                        <input type="color" id="btn_bg_color" name="btn_bg_color" class="form-control" value="{{ old('btn_bg_color', ($slider->btn_bg_color) ? $slider->btn_bg_color : '#00A3A4') }}">
                                    </div>

                                    <div class="color-btns">
                                        <div class="color-btn color-282828" data-target="btn_bg_color" data-color="#282828"></div>
                                        <div class="color-btn color-ffffff" data-target="btn_bg_color" data-color="#ffffff"></div>
                                        <div class="color-btn color-00a3a4" data-target="btn_bg_color" data-color="#00a3a4"></div>
                                        <div class="color-btn color-fac23d" data-target="btn_bg_color" data-color="#fac23d"></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    <label for="anons_ru">Анонс RU</label>
                    <textarea name="anons_ru" id="anons_ru" class="form-control" rows="12"><?=$slider->anons_ru?></textarea>
                </div>
                <div class="col-4">
                    <label for="anons_ua">Анонс UA</label>
                    <textarea name="anons_ua" id="anons_ua" class="form-control" rows="12"><?=$slider->anons_ua?></textarea>
                </div>
                <div class="col-4">
                    <label for="anons_ua">Анонс EN</label>
                    <textarea name="anons_en" id="anons_en" class="form-control" rows="12"><?=$slider->anons_en?></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                    <a href="/admin/sliders" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')


    <script>

        CKEDITOR.replace( 'anons_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'anons_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'anons_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });

        $('#btn-more-ch').change(function (e) {
            if ( $(this).is(':checked') ) {

                $('.btn-more-options').removeClass('hidden');
            } else {
                $('.btn-more-options').addClass('hidden');

            }
        });


        $('.color-btn').click(function (e){
            e.preventDefault();
            var target = $(this).data('target');
            var color = $(this).data('color');
            $('#' + target).val(color);
        });

    </script>
@endsection
