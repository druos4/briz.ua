@extends('layouts.admin')
@section('title')
    Баннера
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .table-responsive{
            padding-bottom: 50px;
        }
    </style>
@endsection
@section('content')
    @can('slider_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.sliders.create") }}">
                    <i class="fas fa-plus"></i> Создать баннер
                </a>
            </div>
        </div>
    @endcan

    <div class="card">
        <div class="card-header">
            Фильтр
        </div>

        <div class="card-body">

            <form action="{{ route("admin.sliders.index") }}" method="GET" enctype="multipart/form-data" class="form-inline">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label>ID</label>
                        <input type="number" min="1" class="form-control w-80" name="id" @if(!empty($filter['id'])) value="{{ $filter['id'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>Название</label>
                        <input type="text" class="form-control w-160" name="title" @if(!empty($filter['title'])) value="{{ $filter['title'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>Активность</label>
                        <select class="form-control" name="active">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['active']) && $filter['active'] == 1) selected @endif>Да</option>
                            <option value="0" @if(isset($filter['active']) && $filter['active'] == 0) selected @endif>Нет</option>
                        </select>
                    </div>
                    <div class="col-auto">
                        <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/sliders?reset=y" role="button"><i class="fas fa-remove"></i> Сбросить</a>

                    </div>
                </div>
            </form>
        </div>
    </div>



    <div class="card">
        <div class="card-header">
            Баннера!
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Картинка</th>
                            <th>Названия</th>
                            <th>URL</th>
                            <th>Активность</th>
                            <th>Сортировка</th>
                            <th>Главная</th>
                            <th>Оборуд.</th>
                            <th>Обновлено</th>
                            <th>ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sliders as $key => $item)
                            <tr id="item-{{ $item->id }}">
                                <td>
                                    <div class="dropdown">


                                        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="/slider-test/{{$item->id}}" target="_blank" title="Просмотреть на сайте"><i class="fas fa-eye"></i> Просмотр на сайте</a>

                                            @can('news_edit')
                                                <a class="dropdown-item" href="/admin/sliders/{{$item->id}}/edit"><i class="fas fa-pencil-alt"></i> Редактировать</a>
                                            @endcan
                                            <a class="dropdown-item" href="/admin/sliders/{{ $item->id }}/history"><i class="fas fa-history"></i> История изменений</a>
                                            @can('news_delete')
                                                <a class="dropdown-item btn-item-del" href="" target="_blank" data-id="{{$item->id}}" data-title="{{ $item->title_ru }} [{{ $item->id }}]" data-url="/admin/sliders/{{ $item->id }}"><i class="fas fa-trash"></i> Удалить</a>
                                            @endcan
                                        </div>

                                    </div>
                                </td>
                                <td>
                                    @if(!empty($item->picture_phone))
                                        @can('news_edit')
                                            <a href="/admin/sliders/{{$item->id}}/edit"><img src="{{ $item->picture_phone }}" /></a>
                                        @else
                                            <img src="{{ $item->picture_phone }}" />
                                        @endif
                                    @endif
                                </td>
                                <td>
                                    @can('news_edit')
                                        <a href="/admin/sliders/{{$item->id}}/edit">
                                            {{ $item->title_ru ?? '' }}
                                            <br />{{ $item->title_ua ?? '' }}
                                            <br />{{ $item->title_en ?? '' }}
                                        </a>
                                    @else
                                        {{ $item->title_ru ?? '' }}
                                        <br />{{ $item->title_ua ?? '' }}
                                        <br />{{ $item->title_en ?? '' }}
                                    @endif
                                </td>
                                <td>
                                    {{ $item->url }}
                                </td>
                                <td>
                                    @if($item->active == 1)
                                        <span class="badge badge-success">Да</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td>
                                    {{ $item->sort }}
                                </td>
                                <td>
                                    @if($item->show_on_main == 1)
                                        <span class="badge badge-success">Да</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td>
                                    @if($item->show_on_shop == 1)
                                        <span class="badge badge-success">Да</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td>
                                    <?=str_replace(' ','<br />',$item->updated_at)?><br />
                                    {{ $item->updater->surname }} {{ $item->updater->name }} [{{ $item->updater->id }}]
                                </td>
                                <td>
                                    @can('news_edit')
                                        <a href="/admin/sliders/{{$item->id}}/edit">
                                            {{ $item->id }}
                                        </a>
                                    @else
                                        {{ $item->id }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div>
                {{ $sliders->appends(request()->except('page'))->links() }}
            </div>
        </div>
    </div>


    @section('scripts')

    @endsection
@endsection
