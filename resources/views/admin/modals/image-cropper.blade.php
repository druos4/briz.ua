<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Подготовка изображения</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-9">
                        <div class="img-container">
                            <img id="image" src="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="docs-data">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">X</span>
                                </div>
                                <input type="text" id="dataX" placeholder="x" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text">px</span>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Y</span>
                                </div>
                                <input type="text" id="dataY" placeholder="y" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text">px</span>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Ширина</span>
                                </div>
                                <input type="text" id="dataWidth" placeholder="ширина" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text">px</span>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Высота</span>
                                </div>
                                <input type="text" id="dataHeight" placeholder="высота" class="form-control">
                                <div class="input-group-append">
                                    <span class="input-group-text">px</span>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary" id="crop" style="width:100%; margin:20px 0px 20px 0px;">Применить</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width:100%;">Отменить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
