<div class="modal fade" id="itemDelModal" tabindex="-1" role="dialog" aria-labelledby="categoryDelModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="itemDelModalTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="del-item-url" value="">
                <input type="hidden" id="del-item-id" value="0">
                <p>Действительно удалить &laquo;<span id="del-item-title"></span>&raquo;?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                <button type="button" class="btn btn-danger btn-item-delete">Удалить</button>
            </div>
        </div>
    </div>
</div>
