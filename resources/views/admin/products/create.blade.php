@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <script src="/ckeditor4/ckeditor.js"></script>
    <style>

        .chosen-container{
            width:100%!important;
        }

        .level-0{
            padding-left:10px
        }
        .level-1{
            padding-left:40px;
        }
        .level-2{
            padding-left:70px;
        }
        .level-0:hover,.level-1:hover,.level-2:hover{
            background: #e9e9e9;
        }
        .level-active{
            color: #155724;
            background-color: #c3e6cb;
        }
        .level-parent{
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
        }

        .more_cats{
            height: 20px!important;
            width: 20px!important;
            float: right!important;
        }
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }

        .img-container img {
            max-width: 100%;
        }
    </style>
@endsection
@section('content')
<a class="btn btn-default" href="{{ route('admin.products.index') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a>
<div class="card">
    <div class="card-header bg-info">
        Новый товар
    </div>

    <div class="card-body">
        <form action="{{ route("admin.products.store") }}" id="form-create" method="POST" enctype="multipart/form-data">
            @csrf

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основные параметры</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="anons-tab" data-toggle="tab" href="#anons" role="tab" aria-controls="detail" aria-selected="false">Анонс</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">Подробно</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Свойства</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Название товара RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru', isset($product) ? $product->title_ru : '') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ua">Название товара UA*</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', isset($product) ? $product->title_ua : '') }}">
                                @if($errors->has('title_ua'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_en">Название товара EN*</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', isset($product) ? $product->title_en : '') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group has-error">
                                <label for="slug">SLUG*</label>
                                <a href="" class="btn-make-slug">Сгенерировать SLUG<i class="fas fa-arrow-down"></i></a>
                                <input type="text" id="slug" name="slug" class="form-control" required value="{{ old('slug', isset($product) ? $product->slug : '') }}">
                                @if($errors->has('slug'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('slug') }}
                                    </em>
                                @endif
                            </div>
{{--
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="title">Артикул</label>
                                        <input type="text" id="article" name="article" class="form-control" value="{{ old('article', isset($product) ? $product->article : '') }}">
                                    </div>
                                </div>

                            </div>
--}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Название товара в Биллинге (используется для синхронизации со складом)</label>
                                        <input type="text" id="billing_name" name="billing_name" class="form-control" value="{{ old('billing_name', isset($product) ? $product->billing_name : '') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Краткое описание RU для плитки товаров</label>
                                        <input type="text" id="section_description_ru" name="section_description_ru" class="form-control" value="{{ old('section_description_ru', isset($product) ? $product->section_description_ru : '') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Краткое описание UA для плитки товаров</label>
                                        <input type="text" id="section_description_ua" name="section_description_ua" class="form-control" value="{{ old('section_description_ua', isset($product) ? $product->section_description_ua : '') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Краткое описание EN для плитки товаров</label>
                                        <input type="text" id="section_description_en" name="section_description_en" class="form-control" value="{{ old('section_description_en', isset($product) ? $product->section_description_en : '') }}">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="form-group">
                                <label for="parent_id">Родительская категория</label>
                                <select name="category_id" id="category_id" class="form-control">
                                    <option value="0"> - укажите основную категорию - </option>
                                    @if(isset($categories) && count($categories) > 0)
                                        <?php $a0 = 0; ?>
                                        @foreach($categories as $key => $category)
                                            <?php $a0++; ?>
                                            <option value="{{ $category->id }}">{{$a0}}. {{ $category->title_ru }}</option>
                                            @if(count($category->children))
                                                <?php $a1 = 0; ?>
                                                @foreach($category->children as $child)
                                                    <?php $a1++; ?>
                                                    <option value="{{ $child->id }}">&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title_ru }}</option>
                                                    <?php $a2 = 0; ?>
                                                    @foreach($child->children as $ch)
                                                        <?php $a2++; ?>
                                                        <option value="{{ $ch->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title_ru }}</option>
                                                    @endforeach
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <div class="toggle-label">Активность</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="active" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label for="sort">Сортировка</label>
                                <input type="number" id="sort" name="sort" class="form-control" value="{{ old('sort', isset($product) ? $product->sort : '100') }}">
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Цена</span>
                                </div>
                                <input type="number" min="0" id="price" name="price" class="form-control" value="{{ old('price', isset($product) ? $product->price : '') }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">грн</span>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Старая цена</span>
                                </div>
                                <input type="number" min="0" id="price_old" name="price_old" class="form-control" value="{{ old('price_old', isset($product) ? $product->price_old : '') }}">
                                <div class="input-group-append">
                                    <span class="input-group-text">грн</span>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Количество</span>
                                </div>
                                <input type="number" min="0" max="999999" id="quantity" name="quantity" class="form-control" value="{{ old('quantity', isset($product) ? $product->quantity : '') }}">
                            </div>
                            <br />
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label for="sort">Гарантия</label>
                                <select name="warranty" class="form-control">
                                    <option value=""> - не указана - </option>
                                    @if(!empty($warranties))
                                        @foreach($warranties as $key => $warranty)
                                            <option value="{{ $key }}">{{ $warranty }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <br />
                            <div class="form-group">
                                <div class="toggle-label">Новинка</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="new" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="toggle-label">Акция</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="promo" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="toggle-label">ТОП</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="hit" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group has-error">
                                <label for="image_ru">Картинка товара*</label><br />
                                <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                                    <input type="file" class="" name="image" accept="image/*">
                                </label>
                                @if($errors->has('image'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('image') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group has-error">
                                <label for="image_recomend">Картинка рекомендуемого товара</label><br />
                                <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                                    <input type="file" class="" name="image_recomend" accept="image/*">
                                </label>
                                @if($errors->has('image_recomend'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('image_recomend') }}
                                    </em>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="anons" role="tabpanel" aria-labelledby="anons-tab">
                    <h3>Краткое описание товара RU</h3>
                    <textarea name="anons_ru" id="anons_ru" class="form-control" rows="20">{{ old('anons_ru', isset($product) ? $product->anons_ru : '') }}</textarea>
                    <br /><br />
                    <h3>Краткое описание товара UA</h3>
                    <textarea name="anons_ua" id="anons_ua" class="form-control" rows="20">{{ old('anons_ua', isset($product) ? $product->anons_ua : '') }}</textarea>
                    <br /><br />
                    <h3>Краткое описание товара EN</h3>
                    <textarea name="anons_en" id="anons_en" class="form-control" rows="20">{{ old('anons_en', isset($product) ? $product->anons_en : '') }}</textarea>
                    <br /><br />
                </div>

                <div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                    <h3>Описание товара RU</h3>
                    <textarea name="description_ru" id="description_ru" class="form-control" rows="20"></textarea>
                    <br /><br />
                    <h3>Описание товара UA</h3>
                    <textarea name="description_ua" id="description_ua" class="form-control" rows="20"></textarea>
                    <br /><br />
                    <h3>Описание товара EN</h3>
                    <textarea name="description_en" id="description_en" class="form-control" rows="20"></textarea>
                    <br /><br />

                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="alert alert-warning" role="alert">
                        Свойства доступны после сохранения товара!
                    </div>
                </div>
                <div class="tab-pane fade" id="metas" role="tabpanel" aria-labelledby="metas-tab">
                    <div class="form-group">
                        <label for="meta_title_ru">Meta title RU</label>
                        <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru', isset($product) ? $product->meta_title_ru : '') }}">
                        <label for="meta_title_ua">Meta title UA</label>
                        <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua', isset($product) ? $product->meta_title_ua : '') }}">
                        <label for="meta_title_en">Meta title EN</label>
                        <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en', isset($product) ? $product->meta_title_en : '') }}">
                    </div>

                    <hr />
                    <div class="form-group">
                        <label for="meta_description_ru">Meta description RU</label>
                        <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru', isset($product) ? $product->meta_description_ru : '') }}">
                        <label for="meta_description_ua">Meta description UA</label>
                        <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua', isset($product) ? $product->meta_description_ua : '') }}">
                        <label for="meta_description_en">Meta description EN</label>
                        <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en', isset($product) ? $product->meta_description_en : '') }}">
                    </div>

                    <hr />
                    <div class="form-group">
                        <label for="meta_keywords_ru">Meta keywords RU</label>
                        <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru', isset($product) ? $product->meta_keywords_ru : '') }}">
                        <label for="meta_keywords_ua">Meta keywords UA</label>
                        <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua', isset($product) ? $product->meta_keywords_ua : '') }}">
                        <label for="meta_keywords_en">Meta keywords EN</label>
                        <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en', isset($product) ? $product->meta_keywords_en : '') }}">
                    </div>

                </div>
            </div>

            <div>
                <input class="btn btn-success btn-create-submit" type="submit" value="Сохранить">
            </div>
        </form>
    </div>
</div>



@endsection
@section('scripts')

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Подготовка изображения</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="img-container">
                                <img id="image" src="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="docs-data">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">X</span>
                                    </div>
                                    <input type="text" id="dataX" placeholder="x" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">px</span>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Y</span>
                                    </div>
                                    <input type="text" id="dataY" placeholder="y" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">px</span>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Ширина</span>
                                    </div>
                                    <input type="text" id="dataWidth" placeholder="ширина" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">px</span>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Высота</span>
                                    </div>
                                    <input type="text" id="dataHeight" placeholder="высота" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">px</span>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary" id="crop" style="width:100%; margin:20px 0px 20px 0px;">Применить</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width:100%;">Отменить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script>
        CKEDITOR.replace( 'description_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'description_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'description_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });

        CKEDITOR.replace( 'anons_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'anons_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'anons_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>

@endsection
