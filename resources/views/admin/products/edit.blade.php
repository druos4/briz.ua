@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <link href="{{ asset('choosen/chosen.css') }}" rel="stylesheet" />
    <link href="{{ asset('cropper/cropper.css') }}" rel="stylesheet" />
    <script src="/ckeditor4/ckeditor.js"></script>
    <style>

        .chosen-container{
            width:100%!important;
        }

        .level-0{
            padding-left:10px
        }
        .level-1{
            padding-left:40px;
        }
        .level-2{
            padding-left:70px;
        }
        .level-0:hover,.level-1:hover,.level-2:hover{
            background: #e9e9e9;
        }
        .level-active{
            color: #155724;
            background-color: #c3e6cb;
        }
        .level-parent{
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
        }

        .more_cats{
            height: 20px!important;
            width: 20px!important;
            float: right!important;
        }


        .img-container img {
            max-width: 100%;
        }
        .bar {
            height: 18px;
            background: green;
        }



        #photo-gallery .gal-img, #photo-gallery-360 .gal-img{
            position: relative;
            float:left;
            width:120px;
            height:120px;
            margin: 0px 10px 10px 0px;
        }
        #photo-gallery .gal-img img, #photo-gallery-360 .gal-img img{
            max-width:120px;
            max-height:120px;
        }

        #photo-gallery .gal-img a, #photo-gallery-360 .gal-img a{
            position: absolute;
            top: 0;
            right: 0;

        }
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }


        .buywith-pic{
            max-width: 50px;
            max-height: 50px;
            margin: 0px 10px;
        }


    </style>


@endsection
@section('content')
    <a class="btn btn-default" href="{{ route('admin.products.index') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a>
    <div class="card">
        <div class="card-header bg-info">
            Редактирование товара {{$product->title_ru}}
        </div>

        <div class="card-body">
            <form action="{{ route('admin.products.update',$product->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основные параметры</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="anons-tab" data-toggle="tab" href="#anons" role="tab" aria-controls="anons" aria-selected="false">Анонс</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">Подробно</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="property-tab" data-toggle="tab" href="#property" role="tab" aria-controls="property" aria-selected="false">Свойства</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="gallery-tab" data-toggle="tab" href="#gallery" role="tab" aria-controls="gallery" aria-selected="false">Галерея</a>
                    </li>
{{--
                    <li class="nav-item">
                        <a class="nav-link" id="cats-tab" data-toggle="tab" href="#cats" role="tab" aria-controls="cats" aria-selected="false">Доп. категории</a>
                    </li>
--}}

                    <li class="nav-item">
                        <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="buywith-tab" data-toggle="tab" href="#buywith" role="tab" aria-controls="buywith" aria-selected="false">Похожие товары</a>
                    </li>

                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label for="title_ru">Название товара RU*</label>
                                    <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru', isset($product) ? $product->title_ru : '') }}">
                                    @if($errors->has('title_ru'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('title_ru') }}
                                        </em>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label for="title_ua">Название товара UA*</label>
                                    <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', isset($product) ? $product->title_ua : '') }}">
                                </div>
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label for="title_en">Название товара EN*</label>
                                    <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', isset($product) ? $product->title_en : '') }}">
                                </div>


                                <div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
                                    <label for="slug">SLUG*</label>
                                    <a href="" class="btn-make-slug">Сгенерировать SLUG<i class="fas fa-arrow-down"></i></a>
                                    <input type="text" id="slug" name="slug" class="form-control" required value="{{ old('slug', isset($product) ? $product->slug : '') }}">
                                    @if($errors->has('slug'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('slug') }}
                                        </em>
                                    @endif
                                </div>
{{--
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="title">Артикул</label>
                                            <input type="text" id="article" name="article" class="form-control" value="{{ old('article', isset($product) ? $product->article : '') }}">
                                        </div>
                                    </div>
                                </div>
--}}

                                <div class="row" style="margin-top: 20px;">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="title">Название товара в Биллинге (используется для синхронизации со складом)</label>
                                            <input type="text" id="billing_name" name="billing_name" class="form-control" value="{{ old('billing_name', isset($product) ? $product->billing_name : '') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 20px;">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="title">Краткое описание RU для плитки товаров</label>
                                            <input type="text" id="section_description_ru" name="section_description_ru" class="form-control" value="{{ old('section_description_ru', isset($product) ? $product->section_description_ru : '') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Краткое описание UA для плитки товаров</label>
                                            <input type="text" id="section_description_ua" name="section_description_ua" class="form-control" value="{{ old('section_description_ua', isset($product) ? $product->section_description_ua : '') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Краткое описание EN для плитки товаров</label>
                                            <input type="text" id="section_description_en" name="section_description_en" class="form-control" value="{{ old('section_description_en', isset($product) ? $product->section_description_en : '') }}">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4">

                                <div class="form-group">
                                    <label for="parent_id">Родительская категория</label>
                                    <select name="category_id" id="category_id" class="form-control">
                                        <option value="0"> - укажите основную категорию - </option>
                                        @if(isset($categories) && count($categories) > 0)
                                            <?php $a0 = 0; ?>
                                            @foreach($categories as $key => $category)
                                                <?php $a0++; ?>
                                                <option value="{{ $category->id }}" @if($product->category_id == $category->id) selected @endif>{{$a0}}. {{ $category->title_ru }}</option>
                                                @if(count($category->children))
                                                    <?php $a1 = 0; ?>
                                                    @foreach($category->children as $child)
                                                        <?php $a1++; ?>
                                                        <option value="{{ $child->id }}" @if($product->category_id == $child->id) selected @endif>&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title_ru }}</option>
                                                        <?php $a2 = 0; ?>
                                                        @foreach($child->children as $ch)
                                                            <?php $a2++; ?>
                                                            <option value="{{ $ch->id }}" @if($product->category_id == $ch->id) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title_ru }}</option>
                                                        @endforeach
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="toggle-label">Активность</div>
                                    <div class="toggle-btn @if($product->active == 1) active @endif">
                                        <input type="checkbox" class="cb-value" name="active" value="1" @if($product->active == 1) checked @endif />
                                        <span class="round-btn"></span>
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label for="sort">Сортировка</label>
                                    <input type="number" id="sort" name="sort" class="form-control" value="{{ old('sort', isset($product) ? $product->sort : '100') }}">
                                </div>

                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Цена</span>
                                    </div>
                                    <input type="number" min="0" id="price" name="price" class="form-control" value="{{ old('price', isset($product) ? $product->price : '') }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">грн</span>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Старая цена</span>
                                    </div>
                                    <input type="number" min="0" id="price_old" name="price_old" class="form-control" value="{{ old('price_old', isset($product) ? $product->price_old : '') }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text">грн</span>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Количество</span>
                                    </div>
                                    <input type="number" min="0" max="999999" id="quantity" name="quantity" class="form-control" value="{{ old('quantity', isset($product) ? $product->quantity : '') }}">
                                </div>
                                <br />
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label for="sort">Гарантия</label>
                                    <select name="warranty" class="form-control">
                                        <option value=""> - не указана - </option>
                                        @if(!empty($warranties))
                                            @foreach($warranties as $key => $warranty)
                                                <option value="{{ $key }}" @if($product->warranty == $key) selected @endif>{{ $warranty }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <br />
                                <div class="form-group">
                                    <div class="toggle-label">Новинка</div>
                                    <div class="toggle-btn @if($product->new == 1) active @endif">
                                        <input type="checkbox" class="cb-value" name="new" value="1" @if($product->new == 1) checked @endif />
                                        <span class="round-btn"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="toggle-label">Акция</div>
                                    <div class="toggle-btn @if($product->promo == 1) active @endif">
                                        <input type="checkbox" class="cb-value" name="promo" value="1" @if($product->promo == 1) checked @endif />
                                        <span class="round-btn"></span>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="toggle-label">ТОП</div>
                                    <div class="toggle-btn @if($product->hit == 1) active @endif">
                                        <input type="checkbox" class="cb-value" name="hit" value="1" @if($product->hit == 1) checked @endif />
                                        <span class="round-btn"></span>
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                    <label for="picture">Картинка товара</label><br />
                                    <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                                        <input type="file" class="" name="image" accept="image/*">
                                    </label>
                                    @if($errors->has('image'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('image') }}
                                        </em>
                                    @endif

                                    @if(isset($product->picture) && $product->picture != '')
                                        <br /><img class="" id="preview-picture" src="{{$product->picture}}" alt="preview-picture" style="max-width:120px; max-height:120px;"><br />
                                        <input type="checkbox" name="del-pic" value="1"> удалить
                                    @endif
                                </div>



                                <div class="form-group has-error">
                                    <label for="image_recomend">Картинка рекомендуемого товара</label><br />
                                    <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                                        <input type="file" class="" name="image_recomend" accept="image/*">
                                    </label>
                                    @if($errors->has('image_recomend'))
                                        <em class="invalid-feedback">
                                            {{ $errors->first('image_recomend') }}
                                        </em>
                                    @endif
                                    @if(isset($product->picture_recomend_phone) && $product->picture_recomend_phone != '')
                                        <br /><img class="" id="preview-picture1" src="{{$product->picture_recomend_phone}}" alt="preview-picture" style="max-width:120px; max-height:120px;"><br />
                                        <input type="checkbox" name="del-pic-rec" value="1"> удалить
                                    @endif
                                </div>


                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="anons" role="tabpanel" aria-labelledby="anons-tab">
                        <h3>Краткое описание товара RU</h3>
                        <textarea name="anons_ru" id="anons_ru" class="form-control" rows="20">{{ old('anons_ru', isset($product) ? $product->anons_ru : '') }}</textarea>
                        <br /><br />
                        <h3>Краткое описание товара UA</h3>
                        <textarea name="anons_ua" id="anons_ua" class="form-control" rows="20">{{ old('anons_ua', isset($product) ? $product->anons_ua : '') }}</textarea>
                        <br /><br />
                        <h3>Краткое описание товара EN</h3>
                        <textarea name="anons_en" id="anons_en" class="form-control" rows="20">{{ old('anons_en', isset($product) ? $product->anons_en : '') }}</textarea>
                        <br /><br />
                    </div>


                    <div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">


                        <h3>Описание товара RU</h3>
                        <textarea name="description_ru" id="description_ru" class="form-control" rows="50">{{ old('description_ru', isset($product) ? $product->description_ru : '') }}</textarea>
                        <br /><br />
                        <h3>Описание товара UA</h3>
                        <textarea name="description_ua" id="description_ua" class="form-control" rows="50">{{ old('description_ua', isset($product) ? $product->description_ua : '') }}</textarea>
                        <br /><br />
                        <h3>Описание товара EN</h3>
                        <textarea name="description_en" id="description_en" class="form-control" rows="50">{{ old('description_en', isset($product) ? $product->description_en : '') }}</textarea>
                        <br /><br />

                    </div>
                    <div class="tab-pane fade" id="property" role="tabpanel" aria-labelledby="property-tab">

                        @if(isset($properties) && count($properties) > 0)
                            @foreach($properties as $key => $prop)

                                <div class="form-group">
                                    <label for="picture">{{ $prop->title_ru }}</label>

                                    <select name="property-{{$prop->id}}[]" data-placeholder="Выберите свойство" class="chosen-select" @if(isset($prop->multiple) && $prop->multiple == 1) multiple tabindex="4" @endif>
                                        <option value="0"></option>
                                        @if(isset($prop->values) && count($prop->values) > 0)
                                            @foreach($prop->values as $k => $val)
                                                <option value="{{ $val->id }}" @if(in_array($val->id,$propertyValues)) selected @endif>{{ $val->value_ru }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                            @endforeach
                        @endif
                    </div>

                    <div class="tab-pane fade" id="gallery" role="tabpanel" aria-labelledby="gallery-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Фотографии</h3>
                                <div id="progress">
                                    <div class="bar" style="width: 0%;"></div>
                                </div>
                                <input id="fileupload" type="file" name="files[]" data-method="post" data-url="/admin/product-gallery-upload/{{$product->id}}" multiple>
                                <hr />
                                <div id="photo-gallery">
                                    <?php $pids = []; ?>
                                    @if(isset($gallery) && count($gallery) > 0)
                                        @foreach($gallery as $item)
                                            <?php $pids[] = $item->image_id; ?>
                                            <div class="gal-img" id="{{$item->image_id}}">
                                                <img src="{{$item->image->thumbnail}}" />
                                                <a class="btn btn-danger img-del-btn btn-xs" data-id="{{$item->image_id}}" id="gal-del-{{$item->image_id}}" href="" role="button" title="Удалить"><i class="fas fa-remove"></i></a>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <input type="hidden" name="gallery-photos" id="gallery-photos" value="{{implode(',',$pids)}}">
                            </div>
                        </div>
                    </div>




                    <div class="tab-pane fade" id="cats" role="tabpanel" aria-labelledby="cats-tab">
                        <p>Вы можете указать дополнительные категории для товара</p>

                        <ul class="list-group">
                            @if(isset($categories) && count($categories) > 0)
                                <?php $a0 = 0; ?>
                                @foreach($categories as $key => $category)
                                    <?php $a0++; ?>
                                    <li class="list-group-item level-0 @if($product->category_id == $category->id) level-parent @elseif(in_array($category->id,$moreCategories)) level-active @endif" id="cat-{{ $category->id }}">
                                        {{$a0}}. {{ $category->title_ru }}
                                        @if($product->category_id == $category->id)
                                            <span style="float:right;"><i class="fas fa-check"></i>основная</span>
                                        @else
                                            <input type="checkbox" class="form-control more_cats" name="categories[]" id="ch-{{ $category->id }}" data-id="{{ $category->id }}"
                                                   value="{{ $category->id }}" @if(in_array($category->id,$moreCategories)) checked @endif>
                                        @endif
                                    </li>
                                    @if(count($category->children))
                                        <?php $a1 = 0; ?>
                                        @foreach($category->children as $child)
                                            <?php $a1++; ?>
                                            <li class="list-group-item level-1 @if($product->category_id == $child->id) level-parent @elseif(in_array($child->id,$moreCategories)) level-active @endif" id="cat-{{ $child->id }}">
                                                {{$a0}}.{{$a1}}. {{ $child->title_ru }}
                                                @if($product->category_id == $child->id)
                                                    <span style="float:right;"><i class="fas fa-check"></i>основная</span>
                                                @else
                                                    <input type="checkbox" class="form-control more_cats" name="categories[]" id="ch-{{ $child->id }}" data-id="{{ $child->id }}"
                                                           value="{{ $child->id }}" @if(in_array($child->id,$moreCategories)) checked @endif>
                                                @endif
                                            </li>
                                            <?php $a2 = 0; ?>
                                            @foreach($child->children as $ch)
                                                <?php $a2++; ?>
                                                <li class="list-group-item level-2 @if($product->category_id == $ch->id) level-parent @elseif(in_array($ch->id,$moreCategories)) level-active @endif" id="cat-{{ $ch->id }}">
                                                    {{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title_ru }}
                                                    @if($product->category_id == $ch->id)
                                                        <span style="float:right;"><i class="fas fa-check"></i>основная</span>
                                                    @else
                                                        <input type="checkbox" class="form-control more_cats" name="categories[]" id="ch-{{ $ch->id }}" data-id="{{ $ch->id }}"
                                                               value="{{ $ch->id }}" @if(in_array($ch->id,$moreCategories)) checked @endif>
                                                    @endif
                                                </li>
                                            @endforeach
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <div class="tab-pane fade" id="metas" role="tabpanel" aria-labelledby="metas-tab">

                        <div class="form-group">
                            <label for="meta_title_ru">Meta title RU</label>
                            <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru', isset($product) ? $product->meta_title_ru : '') }}">
                            <label for="meta_title_ua">Meta title UA</label>
                            <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua', isset($product) ? $product->meta_title_ua : '') }}">
                            <label for="meta_title_en">Meta title EN</label>
                            <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en', isset($product) ? $product->meta_title_en : '') }}">
                        </div>


                        <hr />
                        <div class="form-group">
                            <label for="meta_description_ru">Meta description RU</label>
                            <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru', isset($product) ? $product->meta_description_ru : '') }}">
                            <label for="meta_description_ua">Meta description UA</label>
                            <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua', isset($product) ? $product->meta_description_ua : '') }}">
                            <label for="meta_description_en">Meta description EN</label>
                            <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en', isset($product) ? $product->meta_description_en : '') }}">
                        </div>


                        <hr />
                        <div class="form-group">
                            <label for="meta_keywords_ru">Meta keywords RU</label>
                            <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru', isset($product) ? $product->meta_keywords_ru : '') }}">
                            <label for="meta_keywords_ua">Meta keywords UA</label>
                            <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua', isset($product) ? $product->meta_keywords_ua : '') }}">
                            <label for="meta_keywords_en">Meta keywords EN</label>
                            <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en', isset($product) ? $product->meta_keywords_en : '') }}">
                        </div>


                    </div>

                    <div class="tab-pane fade" id="buywith" role="tabpanel" aria-labelledby="buywith-tab">

                        <div class="row">
                            <div class="col-md-6">
                                <p>Выбранные товары (максимум 3)</p>

                                <ul class="list-group" id="selected_products">
                                    @if(!empty($buywith))
                                        @foreach($buywith as $prod)
                                            @if(!empty($prod->target))
                                                @if(!empty($prod->target->quantity))
                                                    <li class="list-group-item" id="buywith-{{ $prod->target->id }}">
                                                        <a class="btn btn-danger btn-del-buywith btn-sm" href="#" data-id="{{ $prod->target->id }}" role="button"><i class="fas fa-arrow-right"></i></a>
                                                        <img src="{{ $prod->target->thumbnail }}" class="buywith-pic">
                                                        {{ $prod->target->title_ru }} ({{ $prod->target->price }}грн)
                                                    </li>
                                                @else
                                                    <li class="list-group-item list-group-item-warning" id="buywith-{{ $prod->target->id }}">
                                                        <a class="btn btn-danger btn-del-buywith btn-sm" href="#" data-id="{{ $prod->target->id }}" role="button"><i class="fas fa-arrow-right"></i></a>
                                                        <img src="{{ $prod->target->thumbnail }}" class="buywith-pic">
                                                        {{ $prod->target->title_ru }} ({{ $prod->target->price }}грн) <span class="badge badge-danger">Закончился!</span>
                                                    </li>
                                                @endif
                                            @else
                                                <li class="list-group-item list-group-item-warning" id="buywith-{{ $prod->buywith_id }}">
                                                    <a class="btn btn-danger btn-del-buywith btn-sm" href="#" data-id="{{ $prod->buywith_id }}" role="button"><i class="fas fa-arrow-right"></i></a>
                                                    <span class="badge badge-danger">Товар не найден!</span>
                                                </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>

                            </div>
                            <div class="col-md-6">
                                <p>Доступные товары</p>

                                <div class="form-group">
                                    <label for="parent_id">Категория</label>
                                    <select name="category" id="category" class="form-control">
                                        <option value="0"> - выберите категорию - </option>
                                        @if(isset($categories) && count($categories) > 0)
                                            <?php $a0 = 0; ?>
                                            @foreach($categories as $key => $category)
                                                <?php $a0++; ?>
                                                <option value="{{ $category->id }}">{{$a0}}. {{ $category->title_ru }}</option>
                                                @if(count($category->children))
                                                    <?php $a1 = 0; ?>
                                                    @foreach($category->children as $child)
                                                        <?php $a1++; ?>
                                                        <option value="{{ $child->id }}">&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title_ru }}</option>
                                                        <?php $a2 = 0; ?>
                                                        @foreach($child->children as $ch)
                                                            <?php $a2++; ?>
                                                            <option value="{{ $ch->id }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title_ru }}</option>
                                                        @endforeach
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <ul class="list-group" id="available_products">
                                </ul>


                            </div>
                        </div>

                    </div>
                </div>



                <div>
                    <input class="btn btn-success" type="submit" value="Сохранить">
                    <a class="btn btn-default" href="/admin/products/" role="button">К списку товаров</a>
                </div>
            </form>
        </div>
    </div>










@endsection
@section('scripts')

    <div class="modal fade" id="buyWithLimitModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Лимит похожих товаров</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning" role="alert">
                        Превышен лимит похожих товаров! Максимум 3 товара.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Подготовка изображения</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="img-container">
                                <img id="image" src="">
                            </div>
                        </div>
                        <div class="col-md-3">

                            <div class="docs-data">

                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">X</span>
                                    </div>
                                    <input type="text" id="dataX" placeholder="x" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">px</span>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Y</span>
                                    </div>
                                    <input type="text" id="dataY" placeholder="y" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">px</span>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Ширина</span>
                                    </div>
                                    <input type="text" id="dataWidth" placeholder="ширина" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">px</span>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Высота</span>
                                    </div>
                                    <input type="text" id="dataHeight" placeholder="высота" class="form-control">
                                    <div class="input-group-append">
                                        <span class="input-group-text">px</span>
                                    </div>
                                </div>
                            </div>

                            <button type="button" class="btn btn-primary" id="crop" style="width:100%; margin:20px 0px 20px 0px;">Применить</button>

                            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width:100%;">Отменить</button>



                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <script src="{{ asset('choosen/chosen.jquery.js') }}"></script>
    <script src="{{ asset('uploader/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('uploader/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('uploader/jquery.fileupload.js') }}"></script>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#category').change(function (e) {
            var id = $(this).val();
            var ownid = '{{ $product->id }}';
            $('#available_products').html('');
            $.ajax({
                type:'POST',
                url:'/admin/products/get-buy-with',
                data:{id:id,ownid:ownid},
                success:function(data) {

                    if(data.success == true){
                        $('#available_products').html(data.html);
                    }

                }
            });
        });


        $(document).on("click", ".btn-add-buywith", function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var ownid = '{{ $product->id }}';
            $.ajax({
                type:'POST',
                url:'/admin/products/add-to-buy-with',
                data:{id:id,ownid:ownid},
                success:function(data) {
                    if(data.success == true){
                        if(data.limit == true){
                            $('#buyWithLimitModal').modal('show');
                        } else {
                            $('#selected_products').append(data.html);
                        }
                    }
                }
            });
        });

        $(document).on("click", ".btn-del-buywith", function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var ownid = '{{ $product->id }}';
            $.ajax({
                type:'POST',
                url:'/admin/products/del-from-buy-with',
                data:{id:id,ownid:ownid},
                success:function(data) {
                    if(data.success == true){
                        $('#buywith-' + data.id).remove();
                    }
                }
            });
        });


        $('.youtube-add-btn').click(function(e) {
            e.preventDefault();
            var code = $('#youtubecode').val();
            $.ajax({
                type:'POST',
                url:'/admin/product-video/{{$product->id}}/add',
                data:{code:code},
                success:function(data) {
                    if(data.html != ''){
                        $('#prod-videos').append(data.html);


                        $('#vdel-' + data.id).click(function(e) {
                            e.preventDefault();
                            var id = data.id;
                            $.ajax({
                                type:'POST',
                                url:'/admin/product-video/{{$product->id}}/delete',
                                data:{id:id},
                                success:function(data) {
                                    if(data.id > 0){
                                        $('#vrow-' + data.id).remove();
                                    }
                                }
                            });
                        });

                    }
                }
            });
        });



        $('.img-del-btn').click(function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var ids = $('#gallery-photos').val();
            $.ajax({
                type:'POST',
                url:'/admin/product-gallery/{{$product->id}}/delete/' + id,
                data:{ids:ids},
                success:function(data) {
                    $('#' + data.id).remove();
                    $('#gallery-photos').val(data.ids);
                }
            });
        });

        function initDocBtns(){
            $('.doc-del-btn').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    type:'POST',
                    url:'/admin/product-documents-delete',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){
                            $('#doc-item-' + data.id).remove();
                        }
                    }
                });
            });

        }


        initDocBtns();


        $(function () {
            $('#fileupload').fileupload({
                dataType: 'json',
                done: function (e, data) {
                    if(data.result.id > 0){
                        $(data.result.pic).appendTo('#photo-gallery');

                        var ids = $('#gallery-photos').val();
                        if(ids == ''){
                            ids = data.result.id;
                        } else {
                            ids = ids + ',' + data.result.id;
                        }
                        $('#gallery-photos').val(ids);

                        $('#gal-del-' + data.result.id).click(function(e) {
                            e.preventDefault();
                            var id = data.result.id;
                            var ids = $('#gallery-photos').val();
                            $.ajax({
                                type:'POST',
                                url:'/admin/product-gallery/{{$product->id}}/delete/' + id,
                                data:{ids:ids},
                                success:function(data) {
                                    $('#' + data.id).remove();
                                    $('#gallery-photos').val(data.ids);
                                }
                            });
                        });
                    }
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .bar').css(
                        'width',
                        progress + '%'
                    );
                }
            });

            $('#fileupload360').fileupload({
                dataType: 'json',
                done: function (e, data) {
                    if(data.result.id > 0){
                        $(data.result.pic).appendTo('#photo-gallery-360');

                        /*$('#gal-del-' + data.result.id).click(function(e) {
                            e.preventDefault();
                            var id = data.result.id;
                            var ids = $('#gallery-photos').val();
                            $.ajax({
                                type:'POST',
                                url:'/admin/product-gallery/{{$product->id}}/delete/' + id,
                                data:{ids:ids},
                                success:function(data) {
                                    $('#' + data.id).remove();
                                    $('#gallery-photos').val(data.ids);
                                }
                            });
                        });*/
                    }
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress360 .bar').css(
                        'width',
                        progress + '%'
                    );
                }
            });
        });

        $(".chosen-select").chosen();


        window.addEventListener('DOMContentLoaded', function () {
            var avatar = document.getElementById('preview-picture');
            var prevpic = document.getElementById('prev-pic');
            var image = document.getElementById('image');
            var input = document.getElementById('input');
            var dataX = document.getElementById('dataX');
            var dataY = document.getElementById('dataY');
            var dataHeight = document.getElementById('dataHeight');
            var dataWidth = document.getElementById('dataWidth');
          //  var cropBoxData = document.querySelector('#cropBoxData');
           // var $progress = $('.progress');
           // var $progressBar = $('.progress-bar');
            var $alert = $('.alert');
            var $modal = $('#modal');

            var cropper;

            $('[data-toggle="tooltip"]').tooltip();

            input.addEventListener('change', function (e) {
                var files = e.target.files;
                var done = function (url) {
                    input.value = '';
                    image.src = url;
                    $alert.hide();
                    $modal.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function (e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            $modal.on('shown.bs.modal', function () {
                cropper = new Cropper(image, {
                    aspectRatio: 0,
                    viewMode: 0,
                    crop: function (event) {
                        var data = event.detail;
                        dataX.value = Math.round(data.x);
                        dataY.value = Math.round(data.y);
                        dataHeight.value = Math.round(data.height);
                        dataWidth.value = Math.round(data.width);
                    },


                });
            }).on('hidden.bs.modal', function () {
                cropper.destroy();
                cropper = null;
            });

            document.getElementById('crop').addEventListener('click', function () {
                var initialAvatarURL;
                var canvas;

                $modal.modal('hide');

                if (cropper) {
                    canvas = cropper.getCroppedCanvas({
                        //width: 160,
                        //height: 160,
                        width: dataWidth.value,
                        height: dataHeight.value,
                    });
                    initialAvatarURL = avatar.src;
                    avatar.src = canvas.toDataURL();
                    avatar.style.display = 'block';
                    prevpic.value = canvas.toDataURL();
/*
                    $progress.show();
                    $alert.removeClass('alert-success alert-warning');
                    canvas.toBlob(function (blob) {
                        var formData = new FormData();

                        formData.append('avatar', blob, 'avatar.jpg');
                        //$.ajax('https://jsonplaceholder.typicode.com/posts', {
                        $.ajax('/admin/product-picture-upload', {
                            method: 'POST',
                            data: formData,
                            processData: false,
                            contentType: false,

                            xhr: function () {
                                var xhr = new XMLHttpRequest();

                                xhr.upload.onprogress = function (e) {
                                    var percent = '0';
                                    var percentage = '0%';

                                    if (e.lengthComputable) {
                                        percent = Math.round((e.loaded / e.total) * 100);
                                        percentage = percent + '%';
                                        $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                                    }
                                };

                                return xhr;
                            },

                            success: function () {
                                $alert.show().addClass('alert-success').text('Upload success');
                            },

                            error: function () {
                                avatar.src = initialAvatarURL;
                                $alert.show().addClass('alert-warning').text('Upload error');
                            },

                            complete: function () {
                                $progress.hide();
                            },
                        });
                    });
                    */
                }
            });
        });


        $('.more_cats').click(function() {
            var id = $(this).data('id');
            var product_id = '{{$product->id}}';
            if($("#ch-" + id).prop("checked") == false) {
                var state = 0;
            } else {
                var state = 1;
            }

            $.ajax({
                type:'POST',
                url:'/admin/product-more-categories-set',
                data:{id:id,product_id:product_id,state:state},
                success:function(data) {

                    if(data.state == 1){
                        $('#cat-' + data.id).addClass('level-active');
                    } else {
                        $('#cat-' + data.id).removeClass('level-active');
                    }
                }
            });

        });


    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script>
        $("#photo-gallery").sortable({
            update: function(event, ui) {
                var changedList = this.id;
                var order = $(this).sortable('toArray');
                var positions = order.join(',');

                console.log({
                    id: changedList,
                    positions: positions
                });

                $('#gallery-photos').val(positions);
                }
            })
        .disableSelection();
    </script>
    <script>
        CKEDITOR.replace( 'description_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'description_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'description_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });

        CKEDITOR.replace( 'anons_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'anons_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'anons_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>


@endsection
