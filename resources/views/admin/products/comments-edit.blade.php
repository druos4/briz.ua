@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <span style="float:left;"><a class="btn btn-default" href="{{ route('admin.products.comments') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a></span>
            <span style="float:right;"><a class="btn btn-danger btn-del-comment" href="" role="button"><i class="fas fa-remove"></i> Удалить</a></span>
        </div>
    </div>

    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Редактирование отзыва
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                    @if(isset($comment->product->picture) && $comment->product->picture != '')
                        <a href="{{\App\Product::getProductUrl($comment->product->id)}}" title="Открыть страницу товара" target="_blank"><img src="{{ $comment->product->picture }}" style="max-width:150px; max-height:100px;" /></a>
                    @endif
                    <br />
                        <a href="{{\App\Product::getProductUrl($comment->product->id)}}" title="Открыть страницу товара" target="_blank">{{ $comment->product->title }}</a>
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-6">
                            <i class="fas fa-user"></i> {{ $comment->user_name }} {{ $comment->user_email }} @if($comment->user_id > 0) [{{ $comment->user_id }}] @endif
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                            <i class="fas fa-clock">{{ date('H:i:s d.m.Y',strtotime($comment->created_at)) }}</i>
                        </div>

                        <div class="col-md-6">
                            <div class="alert alert-success" role="alert">{{ $comment->pluses }}</div>
                        </div>
                        <div class="col-md-6">
                            <div class="alert alert-danger" role="alert">{{ $comment->minuses }}</div>
                        </div>
                        <div class="col-md-12">
                            <div class="alert alert-info" role="alert">{{ $comment->comment }}</div>
                        </div>

                    </div>
                </div>
                <div class="col-md-2">
                    <form action="/admin/products-comments/{{ $comment->id }}/edit" method="POST" enctype="multipart/form-data">
                        @csrf
                        Статус:
                        <select name="status" class="form-control">
                            <option value="0">Не обработанный</option>
                            <option value="1" @if($comment->moderated_at != null) selected @endif>Опубликованный</option>
                        </select>
                        <br />
                        <input class="btn btn-success" type="submit" value="Сохранить">
                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    @if(!empty($comment->media['pictures']))
                        @foreach($comment->media['pictures'] as $pic)
                            <a href="{{ $pic->sourse }}" target="_blank"><img src="{{ $pic->thumbnail }}" style="max-height:80px; margin:10px;" /></a>
                        @endforeach
                    @endif
                </div>

                <div class="col-md-4">
                    @if(!empty($comment->media['videos']))
                        @foreach($comment->media['videos'] as $video)
                            <?php
                            $code = false;
                            $arr = explode('/',$video->sourse);
                            $arrStr = explode('?',$arr[count($arr) - 1]);
                            $params = explode('&',$arrStr[1]);
                            if(!empty($params)){
                                foreach($params as $k => $p){
                                    $s = explode('=',$p);
                                    if(!empty($s[0]) && !empty($s[1]) && $s[0] == 'v'){
                                        $code = $s[1];
                                    }
                                }
                            }
                            ?>
                            @if($code !== false)
                                <iframe width="200" height="100" src="https://www.youtube.com/embed/{{ $code }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            @endif
                        @endforeach
                    @endif

                </div>
            </div>

        </div>
    </div>
    @section('scripts')
        @parent




        <div class="modal fade" id="productDelModal" tabindex="-1" role="dialog" aria-labelledby="categoryDelModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="categoryDelModalLabel">Удалить отзыв</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="del-prod-id" value="0">
                        <p>Действительно удалить отзыв?</p>
                    </div>
                    <div class="modal-footer">
                        <form action="/admin/products-comments/{{ $comment->id }}/delete" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{ $comment->id }}">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                            <input class="btn btn-danger" type="submit" value="Удалить">
                        </form>
                    </div>
                </div>
            </div>
        </div>





        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.btn-del-comment').click(function(e) {
                e.preventDefault();
                $('#productDelModal').modal('show');
            });

            $('.btn-prod-delete').click(function(e) {
                e.preventDefault();
                var id = $('#del-prod-id').val();
                $.ajax({
                    type:'POST',
                    url:'/admin/products-delete',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){

                            $('#row-' + data.id).remove();
                            $('#del-prod-id').val('');
                            $('#del-prod-title').html('');
                            $('#productDelModal').modal('hide');

                        }
                    }
                });
            });







        </script>


    @endsection
@endsection
