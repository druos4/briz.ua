@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')
    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Заканчивающиеся товары
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <th width="80">Фото</th>
                    <th>Название</th>
                    <th>Код 1С</th>
                    <th>Цена</th>
                    <th width="90">Кол-во</th>
                    <th>Категория</th>
                    <th></th>
                </thead>
                <tbody>
                    @if(isset($products) && count($products) > 0)
                        @foreach($products as $key => $product)
                            <tr id="row-{{$product->id}}">
                                <td width="80">
                                    @if($product->picture != '')
                                        <a href="/admin/products/{{$product->id}}/edit" title="Редактировать"><img src="{{ $product->picture }}" style="max-width:80px; max-height:60px;" /></a>
                                    @endif
                                </td>
                                <td><a href="/admin/products/{{$product->id}}/edit" title="Редактировать">{{ $product->title }}</a></td>
                                <td><a href="/admin/products/{{$product->id}}/edit" title="Редактировать">{{ $product->xml_id }}</a></td>
                                <td>{{ $product->price }}грн</td>
                                <td width="90">{{ $product->quantity }}шт.</td>
                                <td>
                                    <a href="/admin/categories/{{$product->category_id}}/edit" target="_blank">
                                        <span class="badge badge-info">
                                            {{ $product->category->title }}
                                        </span>
                                    </a>
                                </td>
                                <td>
                                    <a class="btn btn-danger btn-del" title="Удалить" href="" role="button" data-id="{{ $product->id }}"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>


                <form action="/admin/products-finishing-add" method="POST">
                    {!! csrf_field() !!}
                <h3>Отметить товар как заканчивающийся</h3>
                    <div class="row">
                        <div class="col-md-6">
                            Категория<br />
                            <select name="category" id="category" class="form-control">
                                <option value="0"> - выберите категорию - </option>
                                @if(isset($categories) && count($categories) > 0)
                                    <?php $a0 = 0; ?>
                                    @foreach($categories as $key => $category)
                                        <?php $a0++; ?>
                                        <option value="{{ $category->id }}" @if(isset($filter['category']) && $filter['category'] == $category->id) selected @endif>{{$a0}}. {{ $category->title }}</option>
                                        @if(count($category->children))
                                            <?php $a1 = 0; ?>
                                            @foreach($category->children as $child)
                                                <?php $a1++; ?>
                                                <option value="{{ $child->id }}" @if(isset($filter['category']) && $filter['category'] == $child->id) selected @endif>&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title }}</option>
                                                <?php $a2 = 0; ?>
                                                @foreach($child->children as $ch)
                                                    <?php $a2++; ?>
                                                    <option value="{{ $ch->id }}" @if(isset($filter['category']) && $filter['category'] == $ch->id) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title }}</option>
                                                @endforeach
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                            Товар<br />
                            <select name="product" id="product" class="form-control">
                            </select>
                        </div>
                        <div class="col-md-6">
                            Код товара в 1С
                            <input type="text" name="xml_id" class="form-control" value="">
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">Добавить</button>
                        </div>
                    </div>
                </form>
        </div>
    </div>
    @section('scripts')
        @parent








        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.btn-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                $.ajax({
                    type:'POST',
                    url:'/admin/products-finishing-del',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){
                            $('#row-' + data.id).remove();
                        }
                    }
                });
            });


            $('#category').change(function(e) {
                e.preventDefault();
                var catid = $(this).val();
                $('#product').html('');
                $.ajax({
                    type:'POST',
                    url:'/admin/get-products-list',
                    data:{catid:catid},
                    success:function(data) {
                        if(data.html != ''){
                            $('#product').html(data.html);
                        }
                    }
                });
            });

            $('.btn-prod-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');
                $('#del-prod-id').val(id);
                $('#del-prod-title').html(title);
                $('#productDelModal').modal('show');
            });

            $('.btn-prod-delete').click(function(e) {
                e.preventDefault();
                var id = $('#del-prod-id').val();
                $.ajax({
                    type:'POST',
                    url:'/admin/products-delete',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){

                            $('#row-' + data.id).remove();
                            $('#del-prod-id').val('');
                            $('#del-prod-title').html('');
                            $('#productDelModal').modal('hide');

                        }
                    }
                });
            });







        </script>


    @endsection
@endsection
