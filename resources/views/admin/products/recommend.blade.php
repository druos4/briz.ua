@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }

        .list-group-item .form-group{
            margin: 0;
        }
    </style>
@endsection
@section('content')

    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Рекомендуемые товары для страниц
        </div>


        <div class="card-body">
            <p>Выберите категории, из которых будет случайным образом выбираться рекомендуемый товар для страниц.</p>
            <form action="/admin/recommend-products" method="POST" id="entity-form" enctype="multipart/form-data">
            @csrf
                <ul class="list-group">
                    @if(!empty($categories))
                        @foreach($categories as $category)
                            <li class="list-group-item">
                                <div class="form-group">
                                    <div class="toggle-label">{{ $category->title_ru }}</div>
                                    <div class="toggle-btn @if($category->recommend_products == true) active @endif">
                                        <input type="checkbox" class="cb-value" name="categories[]" value="{{ $category->id }}" @if($category->recommend_products == true) checked @endif />
                                        <span class="round-btn"></span>
                                    </div>
                                </div>

                            </li>
                        @endforeach
                    @endif
                </ul>

                <button class="btn btn-success btn-save" type="submit"><i class="fas fa-check"></i> Сохранить</button>
            </form>
        </div>
    </div>
    @section('scripts')
        @parent
    @endsection
@endsection
