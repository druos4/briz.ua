@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
    </style>
@endsection
@section('content')
    @can('category_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.products.create") }}">
                    <i class="fas fa-plus"></i> Новый товар
                </a>

                <a class="btn btn-info" href="/admin/products/sync">
                    <i class="fas fa-refresh"></i> Синхронизировать цены и остаки
                </a>
            </div>
        </div>
    @endcan
    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Список товаров
        </div>


        <div class="card-body">
            <h3>Фильтр</h3>
            <div class="filter-container">
                <form id="form-filter" action="/admin/products" method="GET" class="form-inline">
                    {!! csrf_field() !!}

                    <div class="filter-item">
                        Категория<br />
                        <select name="category" class="form-control" style="width:300px;">
                            <option value="0">Все</option>
                            @if(isset($categories) && count($categories) > 0)
                                <?php $a0 = 0; ?>
                                @foreach($categories as $key => $category)
                                    <?php $a0++; ?>
                                    <option value="{{ $category->id }}" @if(isset($filter['category']) && $filter['category'] == $category->id) selected @endif>{{$a0}}. {{ $category->title_ru }}</option>
                                    @if(count($category->children))
                                        <?php $a1 = 0; ?>
                                        @foreach($category->children as $child)
                                            <?php $a1++; ?>
                                            <option value="{{ $child->id }}" @if(isset($filter['category']) && $filter['category'] == $child->id) selected @endif>&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title_ru }}</option>
                                            <?php $a2 = 0; ?>
                                            @foreach($child->children as $ch)
                                                <?php $a2++; ?>
                                                <option value="{{ $ch->id }}" @if(isset($filter['category']) && $filter['category'] == $ch->id) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title_ru }}</option>
                                            @endforeach
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="filter-item">
                        Активность<br />
                        <select name="active" class="form-control" style="width:100px;">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['active']) && $filter['active'] == 1) selected @endif>Да</option>
                            <option value="0" @if(isset($filter['active']) && $filter['active'] == 0) selected @endif>Нет</option>
                        </select>
                    </div>

                    <div class="filter-item">
                        Наличие<br />
                        <select name="stock" class="form-control" style="width:100px;">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['stock']) && $filter['stock'] == 1) selected @endif>Да</option>
                            <option value="0" @if(isset($filter['stock']) && $filter['stock'] == 0) selected @endif>Нет</option>
                        </select>
                    </div>
                    <div class="filter-item">
                        Новинка<br />
                        <select name="new" class="form-control" style="width:100px;">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['new']) && $filter['new'] == 1) selected @endif>Да</option>
                            <option value="0" @if(isset($filter['new']) && $filter['new'] == 0) selected @endif>Нет</option>
                        </select>
                    </div>
{{--


                    <div class="filter-item">
                        Хит<br />
                        <select name="hit" class="form-control" style="width:100px;">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['hit']) && $filter['hit'] == 1) selected @endif>Да</option>
                            <option value="0" @if(isset($filter['hit']) && $filter['hit'] == 0) selected @endif>Нет</option>
                        </select>
                    </div>
--}}
                    <div class="filter-item">
                        ID<br />
                        @if(isset($filter['id']) && $filter['id'] != '')
                            <input type="number" min="1" name="id" class="form-control" value="{{$filter['id']}}">
                        @else
                            <input type="number" min="1" name="id" class="form-control" value="">
                        @endif
                    </div>

                    <div class="filter-item">
                        Поиск<br />
                        @if(isset($filter['search']) && $filter['search'] != '')
                            <input type="text" name="search" class="form-control" value="{{$filter['search']}}">
                        @else
                            <input type="text" name="search" class="form-control" value="">
                        @endif
                    </div>
                    <div class="filter-item"><br />
                        <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/products?reset=y" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                    </div>
                </form>
            </div>



            <table class="table table-striped">
                <thead>
                    <th width="60"></th>
                    <th width="80">Фото</th>
                    <th>Название
                        @if(!empty($sort) && $sort['field'] == 'title_ru' && $sort['direction'] == 'desc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-show';
                            ?>
                        @elseif(!empty($sort) && $sort['field'] == 'title_ru' && $sort['direction'] == 'asc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-show';
                            $s_desc = 'sort-hide';
                            ?>
                        @else
                            <?php
                            $s = 'sort-show';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-hide';
                            ?>
                        @endif
                        <i class="fas i-sort {{ $s }} fa-sort" data-field="title_ru" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="title_ru" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="title_ru" data-direction="desc"></i>
                    </th>
                    <th width="70">Акт.
                        @if(!empty($sort) && $sort['field'] == 'active' && $sort['direction'] == 'desc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-show';
                            ?>
                        @elseif(!empty($sort) && $sort['field'] == 'active' && $sort['direction'] == 'asc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-show';
                            $s_desc = 'sort-hide';
                            ?>
                        @else
                            <?php
                            $s = 'sort-show';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-hide';
                            ?>
                        @endif
                        <i class="fas i-sort {{ $s }} fa-sort" data-field="active" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="active" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="active" data-direction="desc"></i>
                    </th>
                    <th width="100">Сорт.
                        @if(!empty($sort) && $sort['field'] == 'sort' && $sort['direction'] == 'desc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-show';
                            ?>
                        @elseif(!empty($sort) && $sort['field'] == 'sort' && $sort['direction'] == 'asc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-show';
                            $s_desc = 'sort-hide';
                            ?>
                        @else
                            <?php
                            $s = 'sort-show';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-hide';
                            ?>
                        @endif
                        <i class="fas i-sort {{ $s }} fa-sort" data-field="sort" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="sort" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="sort" data-direction="desc"></i>
                    </th>
                    <th>Цена
                        @if(!empty($sort) && $sort['field'] == 'price' && $sort['direction'] == 'desc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-show';
                            ?>
                        @elseif(!empty($sort) && $sort['field'] == 'price' && $sort['direction'] == 'asc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-show';
                            $s_desc = 'sort-hide';
                            ?>
                        @else
                            <?php
                            $s = 'sort-show';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-hide';
                            ?>
                        @endif
                        <i class="fas i-sort {{ $s }} fa-sort" data-field="price" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="price" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="price" data-direction="desc"></i>
                    </th>
                    <th width="90">Кол-во
                        @if(!empty($sort) && $sort['field'] == 'quantity' && $sort['direction'] == 'desc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-show';
                            ?>
                        @elseif(!empty($sort) && $sort['field'] == 'quantity' && $sort['direction'] == 'asc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-show';
                            $s_desc = 'sort-hide';
                            ?>
                        @else
                            <?php
                            $s = 'sort-show';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-hide';
                            ?>
                        @endif
                        <i class="fas i-sort {{ $s }} fa-sort" data-field="quantity" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="quantity" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="quantity" data-direction="desc"></i>
                    </th>
                    <th width="70">New
                        @if(!empty($sort) && $sort['field'] == 'new' && $sort['direction'] == 'desc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-show';
                            ?>
                        @elseif(!empty($sort) && $sort['field'] == 'new' && $sort['direction'] == 'asc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-show';
                            $s_desc = 'sort-hide';
                            ?>
                        @else
                            <?php
                            $s = 'sort-show';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-hide';
                            ?>
                        @endif
                        <i class="fas i-sort {{ $s }} fa-sort" data-field="new" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="new" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="new" data-direction="desc"></i>
                    </th>
                    <th width="70">ТОП
                        @if(!empty($sort) && $sort['field'] == 'hit' && $sort['direction'] == 'desc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-show';
                            ?>
                        @elseif(!empty($sort) && $sort['field'] == 'hit' && $sort['direction'] == 'asc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-show';
                            $s_desc = 'sort-hide';
                            ?>
                        @else
                            <?php
                            $s = 'sort-show';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-hide';
                            ?>
                        @endif
                        <i class="fas i-sort {{ $s }} fa-sort" data-field="hit" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="hit" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="hit" data-direction="desc"></i>
                    </th>
                    <th>Категория</th>
                    <th>Buy<br />with</th>
                    <th>Обновлено</th>
                    <th style="min-width: 60px">ID
                        @if(!empty($sort) && $sort['field'] == 'id' && $sort['direction'] == 'desc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-show';
                            ?>
                        @elseif(!empty($sort) && $sort['field'] == 'id' && $sort['direction'] == 'asc')
                            <?php
                            $s = 'sort-hide';
                            $s_asc = 'sort-show';
                            $s_desc = 'sort-hide';
                            ?>
                        @else
                            <?php
                            $s = 'sort-show';
                            $s_asc = 'sort-hide';
                            $s_desc = 'sort-hide';
                            ?>
                        @endif
                        <i class="fas i-sort {{ $s }} fa-sort" data-field="id" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="id" data-direction="asc"></i>
                        <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="id" data-direction="desc"></i></th>
                </thead>
                <tbody>
                    @if(isset($products) && count($products) > 0)
                        @foreach($products as $key => $product)
                            <tr id="row-{{$product->id}}">
                                <td width="60">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="/equipment/product/{{$product->slug}}" target="_blank">Посмотреть на сайте</a>
                                            <a class="dropdown-item" href="/admin/products/{{$product->id}}/edit">Редактировать</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item btn-prod-del" data-id="{{$product->id}}" data-title="{{$product->title_ru}}" href="">Удалить</a>
                                        </div>
                                    </div>
                                </td>
                                <td width="80">
                                    @if($product->picture != '')
                                        <a href="/admin/products/{{$product->id}}/edit" title="Редактировать"><img src="{{ $product->picture }}" style="max-width:80px; max-height:60px;" /></a>
                                    @endif
                                </td>
                                <td><a href="/admin/products/{{$product->id}}/edit" title="Редактировать">{{ $product->title_ru }}</a></td>
                                <td width="30">
                                    @if($product->active == 1)
                                        <span class="badge badge-success">Да</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td width="70">{{ $product->sort }}</td>
                                <td>{{ $product->price }}грн</td>
                                <td width="90">{{ $product->quantity }}шт.</td>
                                <td width="30">
                                    @if($product->new == 1)
                                        <span class="badge badge-success">Да</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td width="30">
                                    @if($product->hit == 1)
                                        <span class="badge badge-success">Да</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($product->category))
                                        <a href="/admin/categories/{{ $product->category_id }}/edit" target="_blank">
                                        <span class="badge badge-info">{{ $product->category->title_ru }}</span>
                                        </a>
                                    @else
                                        <span class="badge badge-danger">Без категории</span>
                                    @endif
                                </td>
                                <td>
                                    @if($product->buywith > 0)
                                        <span class="badge badge-info">{{ $product->buywith }}</span>
                                    @else
                                        <span class="badge badge-secondary">0</span>
                                    @endif
                                </td>
                                <td><?=str_replace(' ','<br />',$product->updated_at)?></td>
                                <td>{{ $product->id }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>


            <div>
                {{ $products->appends(request()->except('page'))->links() }}.
            </div>

        </div>
    </div>
    @section('scripts')
        @parent



        <!-- Modal -->
        <div class="modal fade" id="productDelModal" tabindex="-1" role="dialog" aria-labelledby="categoryDelModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="categoryDelModalLabel">Удалить товар</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="del-prod-id" value="0">
                        <p>Действительно удалить товар &laquo;<span id="del-prod-title"></span>&raquo;?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <button type="button" class="btn btn-danger btn-prod-delete">Удалить</button>
                    </div>
                </div>
            </div>
        </div>





        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

         /*   $('.btn-active-toggle').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var active = $(this).data('active');
                $.ajax({
                    type:'POST',
                    url:'/admin/categories-update-active',
                    data:{id:id,active:active},
                    success:function(data) {
                        if(data.id > 0){
                            if(data.active == 1){
                                $('#active-y-'+data.id).show();
                                $('#active-n-'+data.id).hide();
                            } else {
                                $('#active-y-'+data.id).hide();
                                $('#active-n-'+data.id).show();
                            }
                        }
                    }
                });
            });*/

            $('.btn-prod-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');
                $('#del-prod-id').val(id);
                $('#del-prod-title').html(title);
                $('#productDelModal').modal('show');
            });

            $('.btn-prod-delete').click(function(e) {
                e.preventDefault();
                var id = $('#del-prod-id').val();
                $.ajax({
                    type:'POST',
                    url:'/admin/products-delete',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){

                            $('#row-' + data.id).remove();
                            $('#del-prod-id').val('');
                            $('#del-prod-title').html('');
                            $('#productDelModal').modal('hide');

                        }
                    }
                });
            });



            $('.i-sort').click(function (e) {
                e.preventDefault();
                var field = $(this).data('field');
                var direction = $(this).data('direction');
                var form = $('#form-filter').serialize();
                form = form + '&field=' + field + '&direction=' + direction;
                window.location.href = "/admin/products?" + form;
            });



        </script>


    @endsection
@endsection
