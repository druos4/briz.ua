@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')
    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Отзывы к товарам
            @if(commentsOn())
                <span class="badge badge-info" style="float:right;">Отзывы на сайте включены</span>
            @else
                <span class="badge badge-warning" style="float:right;">Отзывы на сайте выключены</span>
            @endif
        </div>


        <div class="card-body">
            <form action="/admin/products-comments" method="GET" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-4">
                        Категория: <select name="category" id="category" class="form-control" style="display: inline-block; width:150px;">
                            <option value="0">Все</option>
                            @if(isset($categories) && count($categories) > 0)
                                <?php $a0 = 0; ?>
                                @foreach($categories as $key => $category)
                                    <?php $a0++; ?>
                                    <option value="{{ $category->id }}" @if(isset($filter['product']) && isset($filter['category']) && $filter['category'] == $category->id) selected @endif>{{$a0}}. {{ $category->title }}</option>
                                    @if(count($category->children))
                                        <?php $a1 = 0; ?>
                                        @foreach($category->children as $child)
                                            <?php $a1++; ?>
                                            <option value="{{ $child->id }}" @if(isset($filter['product']) && isset($filter['category']) && $filter['category'] == $child->id) selected @endif>&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title }}</option>
                                            <?php $a2 = 0; ?>
                                            @foreach($child->children as $ch)
                                                <?php $a2++; ?>
                                                <option value="{{ $ch->id }}" @if(isset($filter['product']) && isset($filter['category']) && $filter['category'] == $ch->id) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title }}</option>
                                            @endforeach
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </select>
                        Товар: <select name="product" id="product" class="form-control" style="display: inline-block; width:150px;">
                            @if(isset($filter['product']) && count($products) > 0)
                                <option value="0">Все</option>
                                @foreach($products as $p)
                                    <option value="{{ $p->id }}" @if($p->id == $filter['product']) selected @endif>{{ $p->title }} @if($p->article != '') ({{ $p->article }}) @endif</option>
                                @endforeach
                            @else
                                <option value="0">Все</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-md-3">
                        Показывать: <select name="status" class="form-control" style="display: inline-block; width:150px;">
                            <option value="0">Все</option>
                            <option value="published" @if(isset($filter['status']) && $filter['status'] == 'published') selected @endif>Опубликованные</option>
                            <option value="moderate" @if(isset($filter['status']) && $filter['status'] == 'moderate') selected @endif>Ждущие проверку</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        Пользователь: <input type="text" class="form-control" name="author" style="display: inline-block; width:150px;"
                        @if(isset($filter['author']) && $filter['author'] != '') value="{{ $filter['author'] }}" @endif>
                    </div>
                    <div class="col-md-2">
                        <input class="btn btn-success" type="submit" value="Фильтровать">
                        <a class="btn btn-default" href="/admin/products-comments" role="button">Сбросить</a>
                    </div>
                </div>
            </form>
            <table class="table table-striped">
                <thead>
                    <th>Товар</th>
                    <th>Пользователь</th>
                    <th>Статус</th>
                    {{--<th>Медиа</th>--}}
                    <th>Создан</th>
                    <th></th>
                </thead>
                <tbody>
                    @if(isset($comments) && count($comments) > 0)
                        @foreach($comments as $key => $comment)
                            <tr id="row-{{$comment->id}}" @if($comment->moderated_at == null) class="table-warning" @endif>
                                <td>
                                    @if(isset($comment->product->picture) && $comment->product->picture != '')
                                        <img src="{{ $comment->product->picture }}" style="max-width:80px; max-height:60px;" />
                                    @endif
                                    {{ $comment->product->title }}
                                </td>
                                <td>
                                    {{ $comment->user_name }}<br />
                                    {{ $comment->user_email }} @if($comment->user_id > 0) [{{$comment->user_id}}] @endif
                                </td>
                                <td>
                                    @if($comment->moderated_at > '0000-00-00 00:00:00')
                                        <span class="badge badge-success">Опубликован</span>
                                    @else
                                        <span class="badge badge-secondary">На проверке</span>
                                    @endif
                                </td>
                                {{--<td>
                                    <i class="fas fa-images" title="Есть фотографии"></i>
                                    <i class="fas fa-play-circle" title="Есть видео"></i>
                                </td>--}}
                                <td>
                                    {{ date('H:i:s d.m.Y',strtotime($comment->created_at)) }}
                                </td>
                                <td>
                                    <a class="btn btn-primary" href="/admin/products-comments/{{$comment->id}}/edit" role="button" title="Редактировать"><i class="fas fa-arrow-right"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>


            <div>
                {{ $comments->appends(request()->except('page'))->links() }}.
            </div>

        </div>
    </div>
    @section('scripts')
        @parent



        <!-- Modal -->
        <div class="modal fade" id="productDelModal" tabindex="-1" role="dialog" aria-labelledby="categoryDelModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="categoryDelModalLabel">Удалить товар</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="del-prod-id" value="0">
                        <p>Действительно удалить товар &laquo;<span id="del-prod-title"></span>&raquo;?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <button type="button" class="btn btn-danger btn-prod-delete">Удалить</button>
                    </div>
                </div>
            </div>
        </div>





        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

         /*   $('.btn-active-toggle').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var active = $(this).data('active');
                $.ajax({
                    type:'POST',
                    url:'/admin/categories-update-active',
                    data:{id:id,active:active},
                    success:function(data) {
                        if(data.id > 0){
                            if(data.active == 1){
                                $('#active-y-'+data.id).show();
                                $('#active-n-'+data.id).hide();
                            } else {
                                $('#active-y-'+data.id).hide();
                                $('#active-n-'+data.id).show();
                            }
                        }
                    }
                });
            });*/

            $('.btn-prod-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');
                $('#del-prod-id').val(id);
                $('#del-prod-title').html(title);
                $('#productDelModal').modal('show');
            });

            $('.btn-prod-delete').click(function(e) {
                e.preventDefault();
                var id = $('#del-prod-id').val();
                $.ajax({
                    type:'POST',
                    url:'/admin/products-delete',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){

                            $('#row-' + data.id).remove();
                            $('#del-prod-id').val('');
                            $('#del-prod-title').html('');
                            $('#productDelModal').modal('hide');

                        }
                    }
                });
            });



            $('#category').on('change', function() {
                var catid = $(this).val();
                $('#product').html('');
                $.ajax({
                    type:'POST',
                    url:'/admin/products/getByCatId',
                    data:{catid:catid},
                    success:function(data) {
                        if(data.html != ''){
                            $('#product').html(data.html);
                        }
                    }
                });
            });



        </script>


    @endsection
@endsection
