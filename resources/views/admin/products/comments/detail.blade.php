@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
    </style>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <a href="/admin/products-comments" class="btn btn-secondary"><i class="fas fa-chevron-left"></i> Назад</a>
        </div>
    </div>

    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Модерация отзыва к товару &laquo;{{ $comment->product->title_ru }}&raquo;
            <a style="float: right;" class="btn btn-sm btn-danger btn-delete" href="#" role="button"><i class="fas fa-trash"></i></a>
        </div>

        <div class="card-body">
            <form action="/admin/products-comments/{{ $comment->id }}/edit" method="POST" id="entity-form" enctype="multipart/form-data">
                @csrf
            <div class="row">
                <div class="col-md-8">
                    <?php $dt = strtotime($comment->created_at); ?>

                        <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                            <label for="title_ru">Автор</label>
                            <input type="text" id="name" name="name" class="form-control" required value="{{ old('name', $comment->name) }}">
                        </div>


                        <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                            <label for="title_ru">Оценка</label>
                            <input type="number" min="1" max="5" id="rate" name="rate" class="form-control" value="{{ old('rate', $comment->rate) }}">
                        </div>

                        <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                            <label for="title_ru">Отзыв</label>
                            <textarea class="form-control" name="comment">{{ old('comment', $comment->comment) }}</textarea>
                        </div>



                </div>
                <div class="col-md-4">
                    <p><b>Дата:</b> {{ date('H:i:s d.m.Y',$dt) }}</p>

                    @if(isset($comment->parent->id))
                        <div style="margin-top:30px;">
                            <p>Является ответом на <a href="/admin/products-comments/{{ $comment->parent->id }}/edit" target="_blank">отзыв <i class="fas fa-chevron-right"></i></a></p>

                        </div>
                    @endif

                    <div class="form-group">
                            <div class="toggle-label">Модерация</div>
                            <div class="toggle-btn @if($comment->moderated_at != '') active @endif">
                                <input type="checkbox" class="cb-value" name="active" value="1" @if($comment->moderated_at != '') checked @endif />
                                <span class="round-btn"></span>
                            </div>
                        </div>

                </div>
            </div>

                <button class="btn btn-success btn-save" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/products-comments" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </form>
        </div>
    </div>


    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Подтвердите удаление данного отзыва</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                    <a href="/admin/products-comments/{{ $comment->id }}/delete" class="btn btn-danger">Удалить</a>
                </div>
            </div>
        </div>
    </div>

    @section('scripts')
        @parent
        <script>
            $('.btn-delete').click(function (e) {
                e.preventDefault();
                $('#deleteModal').modal('show');
            });
        </script>
    @endsection
@endsection
