@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
    </style>
@endsection
@section('content')

    <div class="card">
        <div class="card-header">
            Список e-mail для уведомлений о новом отзыве
            <a class="btn btn-success btn-sm btn-add-email" style="float: right;" href="#" role="button"><i class="fas fa-plus"></i></a>
        </div>


        <div class="card-body">
            @if(!empty($emails))
                @foreach($emails as $email)
                    <button type="button" class="btn btn-secondary">{{ $email->email }} <a class="btn btn-danger btn-sm" href="/admin/products-comments/emails/{{ $email->id }}/delete" role="button" title="Удалить"><i class="fas fa-remove"></i></a></button>
                @endforeach
            @endif
        </div>
    </div>

    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Список комментариев к товарам
        </div>


        <div class="card-body">
            <h3>Фильтр</h3>
            <div class="filter-container">
                <form id="form-filter" action="/admin/products-comments" method="GET" class="form-inline">
                    {!! csrf_field() !!}


                    <div class="filter-item">
                        Товар<br />
                        <select name="product_id" class="form-control" style="width:300px;">
                            <option value="0">Все</option>
                            @if(isset($products) && count($products) > 0)
                                @foreach($products as $key => $product)
                                    <option value="{{ $product->product_id }}" @if(isset($filter['product_id']) && $filter['product_id'] == $product->product_id) selected @endif>{{ $product->title_ru }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="filter-item">
                        Модерация<br />
                        <select name="moderated" class="form-control" style="width:100px;">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['active']) && $filter['active'] == 1) selected @endif>Да</option>
                            <option value="0" @if(isset($filter['active']) && $filter['active'] == 0) selected @endif>Нет</option>
                        </select>
                    </div>


                    <div class="filter-item">
                        ID<br />
                        @if(isset($filter['id']) && $filter['id'] != '')
                            <input type="text" name="id" class="form-control" value="{{$filter['id']}}">
                        @else
                            <input type="text" name="id" class="form-control" value="">
                        @endif
                    </div>

                    <div class="filter-item">
                        Поиск<br />
                        @if(isset($filter['search']) && $filter['search'] != '')
                            <input type="text" name="search" class="form-control" value="{{$filter['search']}}">
                        @else
                            <input type="text" name="search" class="form-control" value="">
                        @endif
                    </div>
                    <div class="filter-item"><br />
                        <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/products-comments?reset=y" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                    </div>
                </form>
            </div>



            <table class="table table-striped">
                <thead>
                    <th>Товар</th>
                    <th>Имя пользователя</th>
                    <th></th>
                    <th>Модерация</th>
                    <th>Дата отзыва</th>
                    <th>ID</th>
                    <th></th>
                </thead>
                <tbody>
                    @if(!empty($comments))
                        @foreach($comments as $comment)
                            <tr>
                                <td>{{ $comment->product->title_ru }}</td>
                                <td>{{ $comment->name }}</td>
                                <td>
                                    <?php $str = substr($comment->comment,0, 20); ?>
                                    {{ $str }}...
                                </td>
                                <td>
                                    @if($comment->moderated_at != '')
                                        <?php $dt = strtotime($comment->moderated_at); ?>
                                        <span class="badge badge-success">{{ date('H:i:s d.m.Y',$dt) }}</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td>
                                    <?php $dt = strtotime($comment->created_at); ?>
                                    {{ date('H:i:s d.m.Y',$dt) }}
                                </td>
                                <td>{{ $comment->id }}</td>
                                <td>
                                    <a class="btn btn-info" href="/admin/products-comments/{{ $comment->id }}/edit" role="button"><i class="fas fa-chevron-right"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>


            <div>
                {{ $comments->appends(request()->except('page'))->links() }}.
            </div>

        </div>
    </div>


    <div class="modal fade" id="addEmail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Добавить e-mail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-filter" action="/admin/products-comments/emails/add" method="POST">
                        {!! csrf_field() !!}
                        <div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-mail</label><br />
                                    <input type="email" class="form-control" name="email">
                                </div>

                        </div>

                        <div>

                                <button type="submit" class="btn btn-success">Сохранить</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @section('scripts')
        @parent
        <script>
            $('.btn-add-email').click(function (e) {
                e.preventDefault();
                $('#addEmail').modal('show');
            });
        </script>
    @endsection
@endsection
