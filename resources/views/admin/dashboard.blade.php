@extends('layouts.admin')
@section('content')


<div class="card">
    <div class="card-header">
        <h1>Админ-панель</h1>
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-primary" href="/admin/clear-cache" role="button"><i class="fas fa-redo"></i> Сбросить кеш</a>
            </div>
        </div>


        @can('content_access')
        <div class="row" style="margin-top:20px;">
            <div class="col-md-12">
                <a class="btn btn-info" href="/admin/maps"  role="button"><i class="fas fa-sync"></i> Обновить Карту покрытия</a>
                <a class="btn btn-info" href="/admin/tariffs/sync" role="button"><i class="fas fa-sync"></i> Синхронизировать тарифы</a>

                @if(!empty($jobs) && in_array('SyncChannelsJob',$jobs))
                    <a class="btn btn-warning btn-no-link" href="" role="button"><i class="fas fa-exclamation"></i> Синхронизировать каналы</a>
                @else
                    <a class="btn btn-info" href="/admin/channels/sync" role="button"><i class="fas fa-sync"></i> Синхронизировать каналы</a>
                @endif


                <a class="btn btn-info" href="/admin/products/sync"  role="button"><i class="fas fa-sync"></i> Синхронизировать товары</a>
                <a class="btn btn-info" href="/admin/sitemap"  role="button"><i class="fas fa-sync"></i> Обновить Sitemap</a>

                @if(!empty($jobs) && in_array('MakeImagesSitemap',$jobs))
                    <a class="btn btn-warning btn-no-link" href="" role="button"><i class="fas fa-exclamation"></i> Обновить Sitemap изображений</a>
                @else
                    <a class="btn btn-info" href="/admin/imgsitemap" role="button"><i class="fas fa-sync"></i> Обновить Sitemap изображений</a>
                @endif
            </div>
        </div>

            @if(!empty($jobs))
                <div class="row" style="margin-top:20px;">
                    <div class="col-md-12">
                        <h2>Активные задачи:</h2>
                        <ul class="list-group">
                            @foreach($jobs as $job)
                                <li class="list-group-item">{{ $job }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        @endcan
    </div>
</div>






@endsection
@section('scripts')
@parent
    <script>
        $('.btn-no-link').click(function (e){
            e.preventDefault();
            alert('Задание еще в работе. Обновите страницы позже.');
        });
    </script>
@endsection
