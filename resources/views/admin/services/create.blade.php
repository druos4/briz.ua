@extends('layouts.admin')
@section('title')
    Создание услуги
@endsection
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .prod-pic{
            display: inline-block;
            width:80px;
            height:80px;
        }
        .prod-pic img{
            display: inline-block;
            max-width: 80px;
            max-height: 80px;
        }
        .prod-title{
            margin-left:20px;
            display: inline-block;
            font-size: 14px;
            line-height: 14px;
        }
        .prod-price{
            display: inline-block;
            margin-left:20px;
            font-size: 14px;
            line-height: 14px;

        }
        .btn-remove-product{
            float: right;
            z-index: 9;
        }
    </style>
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
    </style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        Создание услуги
    </div>

    <div class="card-body">
        <form action="{{ route("admin.services.store") }}" id="form-create" method="POST" enctype="multipart/form-data">
            @csrf
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основное</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="products-tab" data-toggle="tab" href="#products" role="tab" aria-controls="products" aria-selected="false">Товары</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control" required value="{{ old('title_ru') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок UA</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" value="{{ old('title_ua') }}">
                                @if($errors->has('title_ua'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок EN</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" value="{{ old('title_en') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="code">Код</label>
                                <input type="text" id="code" name="code" class="form-control" required value="{{ old('code') }}">
                            </div>

                        </div>
                        <div class="col-md-4">
{{--
                            <div class="form-group">
                                <label for="picture_ru">Баннер RU</label>
                                <input type="file" class="form-control-file" name="picture_ru" id="picture_ru">
                            </div>
                            <div class="form-group">
                                <label for="picture_ua">Баннер UA</label>
                                <input type="file" class="form-control-file" name="picture_ua" id="picture_ua">
                            </div>
                            <div class="form-group">
                                <label for="picture_en">Баннер EN</label>
                                <input type="file" class="form-control-file" name="picture_en" id="picture_en">
                            </div>
--}}
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="products" role="tabpanel" aria-labelledby="products-tab">
                    <input type="hidden" id="products_in" name="products_in" value="">
                    <a href="" class="btn btn-info btn-add-prod"><i class="fas fa-plus"></i> Добавить товар</a>

                    <div class="dd" id="nestable">
                        <ol class="dd-list">

                        </ol>
                    </div>

                    <br /><br />
                </div>

                <div class="tab-pane fade" id="metas" role="tabpanel" aria-labelledby="metas-tab">
                    <div class="form-group">
                        <label for="meta_title_ru">Meta title RU</label>
                        <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title UA</label>
                        <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title EN</label>
                        <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_description_ru">Meta description RU</label>
                        <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description UA</label>
                        <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description EN</label>
                        <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_keywords_ru">Meta keywords RU</label>
                        <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords UA</label>
                        <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords EN</label>
                        <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en') }}">
                    </div>

                </div>

            </div>
            <div>
                <button class="btn btn-success btn-create-submit" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/services" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>

@endsection

@section('scripts')

    <div class="modal fade" id="addProductModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Добавить товар</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="meta_title_ru">Категория</label>
                        <select class="form-control" name="category" id="category">
                            <option value="0"> - выберите категорию - </option>
                            @if(!empty($categories))
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->title_ru }}</option>
                                    @if(!empty($category->children))
                                        @foreach($category->children as $child)
                                            <option value="{{ $child->id }}">&nbsp;&nbsp;&nbsp;{{ $child->title_ru }}</option>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="meta_title_ru">Товар</label>
                        <select class="form-control" name="product" id="product">

                        </select>
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary btn-add-selected-product">Добавить</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('.btn-add-prod').click(function (e) {
            e.preventDefault();
            $('#product').html('');
            $('#category').prop('selectedIndex',0);
            $('#addProductModal').modal('show');
        });
        $('#category').change(function (e) {
            var id = $(this).val();
            $.ajax({
                type:'POST',
                url:'/admin/services/get-prods',
                data:{id:id},
                success:function(data) {
                    $('#product').html('');
                    if(data.options != ''){
                        $('#product').html(data.options);
                    }
                }
            });
        });
        $('.btn-add-selected-product').click(function (e) {
            e.preventDefault();
            var prodId = $('#product').val();
            var ids = $('#products_in').val();
            $.ajax({
                type:'POST',
                url:'/admin/services/add-prod',
                data:{prodId:prodId, ids:ids},
                success:function(data) {
                    $('#products_in').val(data.ids);
                    if(data.html != ''){
                        $('#nestable .dd-list').append(data.html);

                    }
                    $('#addProductModal').modal('hide');
                }
            });
        });

        $('#nestable').on("click", '.btn-remove-product', function(e) {
            e.preventDefault();
            var prodId = $(this).data('id');
            var ids = $('#products_in').val();
            $.ajax({
                type:'POST',
                url:'/admin/services/remove-prod',
                data:{prodId:prodId, ids:ids},
                success:function(data) {
                    $('#products_in').val(data.ids);
                    $('#prod-' + data.id).remove();
                }
            });
        });
    </script>

    <script src="{{ asset('adminscripts/jquery.nestable.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(function($){
            var nestable,
                serialized,
                settings = {maxDepth:1},
                saveOrder = $('#saveOrder'),

                edit = $('.edit');

            nestable = $('.dd').nestable(settings);

            saveOrder.on('click', function(e) {
                e.preventDefault();
                serialized = nestable.nestable('serialize');

                $.ajax({
                    method:'POST',
                    url : "/admin/services/products-save-sort",
                    data: { _token: "{!! csrf_token() !!}", serialized: serialized }

                }).done(function (data) {

                    $('#products_in').val(data.ids);
                    alert("Сохранено!");

                })
            })

            $('#nestable').on("mousedown", '.dd-handle a', function(e){
                e.stopPropagation();
            });

            $('.dd').on('change', function() {
                var serialized = nestable.nestable('serialize');
                $.ajax({
                    method:'POST',
                    url : "/admin/services/products-save-sort",
                    data: { _token: "{!! csrf_token() !!}", serialized: serialized }

                }).done(function (data) {

                    $('#products_in').val(data.ids);

                })
            });

            $('[data-rel="tooltip"]').tooltip();

        });



    </script>

@endsection
