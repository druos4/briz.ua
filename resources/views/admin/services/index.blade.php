@extends('layouts.admin')
@section('title')
    Сервисы
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
    </style>
@endsection
@section('content')
    {{--
    @can('news_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.services.create") }}">
                    <i class="fas fa-plus"></i> Создать Услуги
                </a>
            </div>
        </div>
    @endcan
--}}
    <div class="card">
        <div class="card-header">
            Услуги
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>

                            <th style="min-width: 150px">
                                Название RU
                            </th>
                            <th style="min-width: 150px">
                                Название UA
                            </th>
                            <th style="min-width: 150px">
                                Название EN
                            </th>
                            <th>Товаров</th>
                            <th>Код</th>
                            <th>Обновлено</th>

                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($pages))
                        @foreach($pages as $key => $item)
                            <tr id="item-{{ $item->id }}">
                                <td>
                                    @can('services_edit')
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-bars"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="/admin/services/{{$item->id}}/edit" role="button" title="Редактировать"><i class="fas fa-pencil-alt"></i> Редактировать</a>

                                            </div>
                                        </div>
                                    @endcan
                                </td>

                                <td>
                                    {{ $item->title_ru ?? '' }}
                                </td>
                                <td>
                                    {{ $item->title_ua ?? '' }}
                                </td>
                                <td>
                                    {{ $item->title_en ?? '' }}
                                </td>
                                <td>
                                    {{ count($item->products) }}
                                </td>
                                <td>
                                    {{ $item->code }}
                                </td>
                                <td>
                                    {{ $item->updated_at }}
                                </td>

                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>

        </div>
    </div>


    @section('scripts')
        <script>

        </script>
    @endsection
@endsection
