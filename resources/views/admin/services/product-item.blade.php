<li class="dd-item" id="prod-{{ $product->id }}" data-id="{!! $product->id !!}">
    <div class="dd-handle">
            <div class="prod-pic">
                <img src="{{ $product->picture }}" alt="{{ $product->title_ru }}" />
            </div>
            <div class="prod-title">
                {{ $product->title_ru }}
            </div>
            <div class="prod-price">
                {{ $product->price }}грн
            </div>
    </div>
    <a class="btn btn-danger btn-remove-product" id="del-{{ $product->id }}" href="" role="button" data-id="{{ $product->id }}" title="Удалить"><i class="fas fa-trash"></i></a>
</li>
