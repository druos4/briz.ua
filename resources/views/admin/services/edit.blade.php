@extends('layouts.admin')
@section('title')
    Редактирование услуги
@endsection
@section('styles')
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <style>
        .prod-pic{
            display: inline-block;
            width:80px;
            height:80px;
        }
        .prod-pic img{
            display: inline-block;
            max-width: 80px;
            max-height: 80px;
        }
        .prod-title{
            margin-left:20px;
            display: inline-block;
            font-size: 14px;
            line-height: 14px;
        }
        .prod-price{
            display: inline-block;
            margin-left:20px;
            font-size: 14px;
            line-height: 14px;

        }
        .btn-remove-product{
            z-index: 9;
            position: absolute;
            top: 5px;
            right: 5px;
            z-index: 9;
        }

        .bt-1{
            border-top: 1px solid #e1e1e1;
            margin-top:10px;
            padding-top:10px;
            padding-bottom:10px;
        }

        .insert-btn{
            display: inline-block;
        }
        .insert-select{
            display: inline-block;
            width:180px;
        }
        .insert-names{
            margin-top: 10px;
        }
    </style>
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
    </style>
@endsection

@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-secondary" href="{{ route("admin.services.index") }}">
                <i class="fas fa-chevron-left"></i> Назад
            </a>
        </div>
    </div>

<div class="card">

    <div class="card-header">
        Редактирование услуги
    </div>

    <div class="card-body">
        <form action="{{ route("admin.services.update", [$service->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основное</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="products-tab" data-toggle="tab" href="#products" role="tab" aria-controls="products" aria-selected="false">Товары</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control" required value="{{ old('title_ru', ($service->title_ru) ? $service->title_ru : '') }}">
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок UA</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" value="{{ old('title_ua', ($service->title_ua) ? $service->title_ua : '') }}">
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок EN</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" value="{{ old('title_en', ($service->title_en) ? $service->title_en : '') }}">
                            </div>
                            <input type="hidden" id="code" name="code" class="form-control" required value="{{ old('code', ($service->code) ? $service->code : '') }}">
                            {{--
                            <div class="form-group">
                                <label for="code">Код</label>
                                <input type="text" id="code" name="code" class="form-control" required value="{{ old('code', ($service->code) ? $service->code : '') }}">
                            </div>
--}}
                        </div>
                        <div class="col-md-4">
{{--
                            <div class="form-group">
                                <label for="picture_ru">Баннер RU</label>
                                <input type="file" class="form-control-file" name="picture_ru" id="picture_ru">
                                @if($service->banner_ru != '')
                                    <img class="" src="{{$service->banner_ru}}" alt="preview-picture" style="max-width:120px; max-height:120px;"><br />
                                    <input type="checkbox" name="del-pic_ru" value="1"> удалить
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="picture_ua">Баннер UA</label>
                                <input type="file" class="form-control-file" name="picture_ua" id="picture_ua">
                                @if($service->banner_ua != '')
                                    <img class="" src="{{$service->banner_ua}}" alt="preview-picture" style="max-width:120px; max-height:120px;"><br />
                                    <input type="checkbox" name="del-pic_ua" value="1"> удалить
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="picture_en">Баннер EN</label>
                                <input type="file" class="form-control-file" name="picture_en" id="picture_en">
                                @if($service->banner_en != '')
                                    <img class="" src="{{$service->banner_en}}" alt="preview-picture" style="max-width:120px; max-height:120px;"><br />
                                    <input type="checkbox" name="del-pic_en" value="1"> удалить
                                @endif
                            </div>
--}}
                        </div>
                    </div>


                    <div class="row bt-1">
                        <div class="col-md-8">
                            <label for="answer_ru">Описание Union-пакета RU</label>
                            <textarea name="union_description_ru" id="answer_ru" class="form-control" rows="12"><?=old('union_description_ru', isset($service) ? $service->union_description_ru : '')?></textarea>

                        </div>
                        <div class="col-md-4">
                            <div>Вставить в текст</div>
                            <div class="insert-names">
                                <div>Название или цену:</div>
                                <a class="btn btn-primary btn-insert-tariff insert-btn" data-id="ru" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_id_ru" id="service_id_ru">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <select class="form-control insert-select" name="service_field_ru" id="service_field_ru">
                                    <option value="0"> - выберите поле - </option>
                                    <option value="title">Название тарифа</option>
                                    <option value="price">Цена тарифа</option>
                                </select>
                            </div>


                            <div class="insert-names">
                                <div>Вставить разницу стоимости между:</div>
                                <a class="btn btn-primary btn-insert-diff insert-btn" data-id="ru" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_price1_ru" id="service_price1_ru">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                плюс
                                <select class="form-control insert-select" name="service_price2_ru" id="service_price2_ru">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <br />
                                минус
                                <select class="form-control insert-select" name="service_price0_ru" id="service_price0_ru">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>


                        </div>
                    </div>

                    <div class="row bt-1">
                        <div class="col-md-8">
                            <label for="answer_ru">Описание Union-пакета UA</label>
                            <textarea name="union_description_ua" id="answer_ua" class="form-control" rows="12"><?=old('union_description_ua', isset($service) ? $service->union_description_ua : '')?></textarea>

                        </div>
                        <div class="col-md-4">
                            <div>Вставить в текст</div>
                            <div class="insert-names">
                                <div>Название или цену:</div>
                                <a class="btn btn-primary btn-insert-tariff insert-btn" data-id="ua" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_id_ua" id="service_id_ua">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <select class="form-control insert-select" name="service_field_ua" id="service_field_ua">
                                    <option value="0"> - выберите поле - </option>
                                    <option value="title">Название тарифа</option>
                                    <option value="price">Цена тарифа</option>
                                </select>
                            </div>


                            <div class="insert-names">
                                <div>Вставить разницу стоимости между:</div>
                                <a class="btn btn-primary btn-insert-diff insert-btn" data-id="ua" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_price1_ua" id="service_price1_ua">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                плюс
                                <select class="form-control insert-select" name="service_price2_ua" id="service_price2_ua">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <br />
                                минус
                                <select class="form-control insert-select" name="service_price0_ua" id="service_price0_ua">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>


                        </div>
                    </div>

                    <div class="row bt-1">
                        <div class="col-md-8">
                            <label for="answer_ru">Описание Union-пакета EN</label>
                            <textarea name="union_description_en" id="answer_en" class="form-control" rows="12"><?=old('union_description_en', isset($service) ? $service->union_description_en : '')?></textarea>

                        </div>
                        <div class="col-md-4">
                            <div>Вставить в текст</div>
                            <div class="insert-names">
                                <div>Название или цену:</div>
                                <a class="btn btn-primary btn-insert-tariff insert-btn" data-id="en" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_id_en" id="service_id_en">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <select class="form-control insert-select" name="service_field_en" id="service_field_en">
                                    <option value="0"> - выберите поле - </option>
                                    <option value="title">Название тарифа</option>
                                    <option value="price">Цена тарифа</option>
                                </select>
                            </div>


                            <div class="insert-names">
                                <div>Вставить разницу стоимости между:</div>
                                <a class="btn btn-primary btn-insert-diff insert-btn" data-id="en" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_price1_en" id="service_price1_en">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                плюс
                                <select class="form-control insert-select" name="service_price2_en" id="service_price2_en">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <br />
                                минус
                                <select class="form-control insert-select" name="service_price0_en" id="service_price0_en">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>


                        </div>
                    </div>

                </div>

                <div class="tab-pane fade" id="products" role="tabpanel" aria-labelledby="products-tab">
                    <?php $pIds = []; ?>
                    @if(!empty($service->products))
                        @foreach($service->products as $product)
                            <?php $pIds[] = $product->id; ?>
                        @endforeach
                    @endif
                    <input type="hidden" id="products_in" name="products_in" value="{{ implode('|',$pIds) }}">
                    <a href="" class="btn btn-info btn-add-prod"><i class="fas fa-plus"></i> Добавить товар</a>

                    <div class="dd" id="nestable">
                        <ol class="dd-list">

                            @if(!empty($service->products))
                                @foreach($service->products as $product)
                                    <?=view('admin.services.product-item',compact('product'))?>
                                @endforeach
                            @endif
                        </ol>
                    </div>

                    <br /><br />

                </div>

                <div class="tab-pane fade" id="metas" role="tabpanel" aria-labelledby="metas-tab">
                    <div class="form-group">
                        <label for="meta_title_ru">Meta title RU</label>
                        <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru', ($service->meta_title_ru) ? $service->meta_title_ru : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title UA</label>
                        <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua', ($service->meta_title_ua) ? $service->meta_title_ua : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title EN</label>
                        <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en', ($service->meta_title_en) ? $service->meta_title_en : '') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_description_ru">Meta description RU</label>
                        <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru', ($service->meta_description_ru) ? $service->meta_description_ru : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description UA</label>
                        <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua', ($service->meta_description_ua) ? $service->meta_description_ua : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description EN</label>
                        <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en', ($service->meta_description_en) ? $service->meta_description_en : '') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_keywords_ru">Meta keywords RU</label>
                        <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru', ($service->meta_keywords_ru) ? $service->meta_keywords_ru : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords UA</label>
                        <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua', ($service->meta_keywords_ua) ? $service->meta_keywords_ua : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords EN</label>
                        <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en', ($service->meta_keywords_en) ? $service->meta_keywords_en : '') }}">
                    </div>

                </div>

            </div>
            <div>
                <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/services" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>

@endsection

@section('scripts')
    <div class="modal fade" id="addProductModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Добавить товар</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="meta_title_ru">Категория</label>
                        <select class="form-control" name="category" id="category">
                            <option value="0"> - выберите категорию - </option>
                            @if(!empty($categories))
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->title_ru }}</option>
                                    @if(!empty($category->children))
                                        @foreach($category->children as $child)
                                            <option value="{{ $child->id }}">&nbsp;&nbsp;&nbsp;{{ $child->title_ru }}</option>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="meta_title_ru">Товар</label>
                        <select class="form-control" name="product" id="product">

                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary btn-add-selected-product">Добавить</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.btn-add-prod').click(function (e) {
            e.preventDefault();
            $('#product').html('');
            $('#category').prop('selectedIndex',0);
            $('#addProductModal').modal('show');
        });
        $('#category').change(function (e) {
            var id = $(this).val();
            $.ajax({
                type:'POST',
                url:'/admin/services/get-prods',
                data:{id:id},
                success:function(data) {
                    $('#product').html('');
                    if(data.options != ''){
                        $('#product').html(data.options);
                    }
                }
            });
        });
        $('.btn-add-selected-product').click(function (e) {
            e.preventDefault();
            var prodId = $('#product').val();
            var ids = $('#products_in').val();
            $.ajax({
                type:'POST',
                url:'/admin/services/add-prod',
                data:{prodId:prodId, ids:ids},
                success:function(data) {
                    if(data.success == true){
                        $('#products_in').val(data.ids);
                        if(data.html != ''){
                            $('#nestable .dd-list').append(data.html);

                        }
                    }
                    $('#addProductModal').modal('hide');
                }
            });
        });

        $('#nestable').on("click", '.btn-remove-product', function(e) {
            e.preventDefault();
            e.stopPropagation();
            var prodId = $(this).data('id');
            var ids = $('#products_in').val();
            $.ajax({
                type:'POST',
                url:'/admin/services/remove-prod',
                data:{prodId:prodId, ids:ids},
                success:function(data) {
                    $('#products_in').val(data.ids);
                    $('#prod-' + data.id).remove();
                }
            });
        });
    </script>

    <script src="{{ asset('adminscripts/jquery.nestable.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(function($){
            var nestable,
                serialized,
                settings = {maxDepth:1},
                saveOrder = $('#saveOrder'),

                edit = $('.edit');

            nestable = $('.dd').nestable(settings);

            saveOrder.on('click', function(e) {
                e.preventDefault();
                serialized = nestable.nestable('serialize');

                $.ajax({
                    method:'POST',
                    url : "/admin/services/products-save-sort",
                    data: { _token: "{!! csrf_token() !!}", serialized: serialized }

                }).done(function (data) {

                    $('#products_in').val(data.ids);
                    alert("Сохранено!");

                })
            })

            $('#nestable').on("mousedown", '.dd-handle a', function(e){
                e.stopPropagation();
            });

            $('.dd').on('change', function() {
                var serialized = nestable.nestable('serialize');
                $.ajax({
                    method:'POST',
                    url : "/admin/services/products-save-sort",
                    data: { _token: "{!! csrf_token() !!}", serialized: serialized }

                }).done(function (data) {

                    $('#products_in').val(data.ids);

                })
            });

            $('[data-rel="tooltip"]').tooltip();

        });



    </script>
    <script>
        CKEDITOR.replace( 'answer_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'answer_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'answer_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });


        $('.btn-insert-tariff').click(function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var target = 'answer_' + id;
            var tarif = $('#service_id_' + id).val();
            var field = $('#service_field_' + id).val();
            var name = $("#service_id_" + id + " option:selected").text();
            if(tarif != 0 && field != 0){

                var data = '{service:' + tarif + ':' + field + ':' + name + '}';
                CKEDITOR.instances[target].insertHtml(data);
            }
        });

        $('.btn-insert-diff').click(function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var target = 'answer_' + id;
            var tarif1 = $('#service_price1_' + id).val();
            var tarif2 = $('#service_price2_' + id).val();
            var tarif0 = $('#service_price0_' + id).val();
            var name1 = $("#service_price1_" + id + " option:selected").text();
            var name2 = $("#service_price2_" + id + " option:selected").text();
            var name0 = $("#service_price0_" + id + " option:selected").text();
            if(tarif1 != 0 && tarif2 != 0 && tarif0 != 0){

                var data = '{diff:' + tarif1 + ':' + tarif2 + ':' + tarif0 + ':цена ' + name1 + ' плюс цена ' + name2 + ' минус цена ' + name0 + '}';
                CKEDITOR.instances[target].insertHtml(data);
            }

        });

    </script>
@endsection
