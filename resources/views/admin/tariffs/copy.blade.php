@extends('layouts.admin')
@section('title')
    Копирование тарифа
@endsection
@section('styles')
    <style>
    </style>
@endsection

@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-secondary" href="/admin/tariffs/{{ $tariff->id }}/edit">
                <i class="fas fa-chevron-left"></i> Назад
            </a>
        </div>
    </div>

<div class="card">

    <div class="card-header">
        Копирование тарифа
    </div>

    <div class="card-body">
        <form action="/admin/tariffs/{{ $tariff->id }}/copy" id="tariff-form" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="alert alert-info" role="alert">
                Выполняя копирование будет перенесено описание из выбранной карточки тарифа в целевую.
            </div>

            <div class="row">
                <div class="col-6">
                    <h3>Копируем из</h3>
                    @if(!empty($others))
                        @foreach($others as $item)
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="parent" id="exampleRadios1" value="{{ $item->id }}">
                                <label class="form-check-label" for="exampleRadios1">
                                    {{ $item->title_ru }} [{{ $item->id }}]. Billing ID: {{ $item->billing_id }}. Цена: {{ $item->price }}грн
                                </label>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="col-6">
                    <h3>Копируем в</h3>
                    {{ $tariff->title_ru }} [{{ $tariff->id }}]<br />
                    Billing ID: {{ $tariff->billing_id }}<br />
                    Цена: {{ $tariff->price }}грн<br />
                </div>
            </div>

            <div>
                <button class="btn btn-success btn-save" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/tariffs/{{ $tariff->id }}/edit" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>



@endsection

@section('scripts')
@endsection
