@extends('layouts.admin')
@section('title')
    Редактирование тарифа
@endsection
@section('styles')
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <style>
        #price_old{
            display: inline-block;
            width:120px;
        }
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
        .fr{
            float:right;
        }
        .films-block{
            display: none;
        }
        .films-block.active{
            display: block;
        }

    </style>
@endsection

@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-secondary" href="{{ route("admin.tariffs.index") }}">
                <i class="fas fa-chevron-left"></i> Назад
            </a>
        </div>
    </div>

<div class="card">

    <div class="card-header">
        Редактирование тарифа

        <a class="btn btn-primary fr" href="/admin/tariffs/{{ $tariff->id }}/copy" role="button"><i class="fas fa-copy"></i>Копировать из карточки</a>
    </div>

    <div class="card-body">
        <form action="/admin/tariffs/{{ $tariff->id }}/update" id="tariff-form" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                        <label for="title_ru">Заголовок RU*</label>
                        <input type="text" id="title_ru" name="title_ru" class="form-control" required value="{{ old('title_ru', ($tariff->title_ru) ? $tariff->title_ru : '') }}">
                        @if($errors->has('title_ru'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title_ru') }}
                            </em>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                        <label for="title_ua">Заголовок UA*</label>
                        <input type="text" id="title_ua" name="title_ua" class="form-control" value="{{ old('title_ua', ($tariff->title_ua) ? $tariff->title_ua : '') }}">
                        @if($errors->has('title_ua'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title_ua') }}
                            </em>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                        <label for="title_en">Заголовок EN*</label>
                        <input type="text" id="title_en" name="title_en" class="form-control" value="{{ old('title_en', ($tariff->title_en) ? $tariff->title_en : '') }}">
                        @if($errors->has('title_en'))
                            <em class="invalid-feedback">
                                {{ $errors->first('title_en') }}
                            </em>
                        @endif
                    </div>



                    <label for="description_ru">Описание RU</label>
                    @if($tariff->description_old_ru != '')
                        <div class="alert alert-info" role="alert">
                            <?=$tariff->description_old_ru?>
                        </div>
                    @endif
                    <textarea name="description_ru" id="description_ru" class="form-control" rows="12"><?=$tariff->description_ru?></textarea>
                    <hr />
                    <label for="description_ua">Описание UA</label>
                    @if($tariff->description_old_ru != '')
                        <div class="alert alert-info" role="alert">
                            <?=$tariff->description_old_ua?>
                        </div>
                    @endif
                    <textarea name="description_ua" id="description_ua" class="form-control" rows="12"><?=$tariff->description_ua?></textarea>
                    <hr />
                    <label for="description_en">Описание EN</label>
                    @if($tariff->description_old_ru != '')
                        <div class="alert alert-info" role="alert">
                            <?=$tariff->description_old_en?>
                        </div>
                    @endif
                    <textarea name="description_en" id="description_en" class="form-control" rows="12"><?=$tariff->description_en?></textarea>

                </div>
                <div class="col-md-4">

                    <div class="form-group">
                        <div class="toggle-label">Активность</div>
                        <div class="toggle-btn @if($tariff->active == 1) active @endif">
                            <input type="checkbox" class="cb-value" name="active" value="1" @if($tariff->active == 1) checked @endif />
                            <span class="round-btn"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Показывать от</label>
                        <input type="datetime-local" id="show_from" name="show_from" @if(!empty($tariff->show_from)) value="{{ str_replace(' ','T',$tariff->show_from) }}" @endif>
                    </div>
                    <div class="form-group">
                        <label>Показывать до</label>
                        <input type="datetime-local" id="show_to" name="show_to" @if(!empty($tariff->show_to)) value="{{ str_replace(' ','T',$tariff->show_to) }}" @endif>
                    </div>

                    <div class="form-group">
                        <label for="image">Картинка</label><br />
                        <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                            <input type="file" class="" name="image" accept="image/*">
                        </label>
                        @if($errors->has('image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('image') }}
                            </em>
                        @endif
                        @if(!empty($tariff->picture))
                            <br /><img class="" id="preview-picture_ru" src="{{ $tariff->picture }}" alt="preview-picture" style="max-width:120px; max-height:120px;"><br />
                            <input type="checkbox" name="del-pic" value="1"> удалить
                        @endif
                    </div>

                    @if(!empty($index_groups))
                        <div class="form-group">
                            <label for="union_group">Группа для основной страницы</label>
                            <select class="form-control" name="index_group">
                                <option value="0"> - нет - </option>
                                @foreach($index_groups as $key => $value)
                                    <option value="{{ $key }}" @if($tariff->index_group == $key) selected @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="sort">Сортировка</label>
                                <input type="number" min="1" max="99999" id="sort" name="sort" class="form-control" value="{{ old('sort', ($tariff->sort) ? $tariff->sort : 10) }}">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="toggle-label">Показывать первым</div>
                                <div class="toggle-btn @if($tariff->first_to_show == 1) active @endif">
                                    <input type="checkbox" class="cb-value" name="first_to_show" value="1" @if($tariff->first_to_show == 1) checked @endif />
                                    <span class="round-btn"></span>
                                </div>
                            </div>
                        </div>
                    </div>


{{--
                    <div class="form-group">
                        <label for="power">"Мощность" (от 0% до 100%)</label>
                        <input type="number" min="0" max="100" id="power" name="power" class="form-control" value="{{ old('power', ($tariff->power) ? $tariff->power : '') }}">
                    </div>
--}}

                    @if($tariff->tariff_type == 'union')
                    <div class="form-group">
                        <label for="union_group">Группа Union</label>
                        <select class="form-control" name="union_group">
                            <option value="0"> - нет - </option>
                            <option value="tv" @if($tariff->union_group == 'tv') selected @endif>TV</option>
                            <option value="tvmax" @if($tariff->union_group == 'tvmax') selected @endif>TVmax</option>
                            <option value="iptv" @if($tariff->union_group == 'iptv') selected @endif>IPTV</option>
                            <option value="briztv" @if($tariff->union_group == 'briztv') selected @endif>Интернет + Briz TV</option>
                        </select>
                    </div>
                    @endif
                    @if($tariff->user_type == 2)
                    <div class="form-group">
                        <label for="union_group">Группа для бизнесса</label>
                        <select class="form-control" name="business_group">
                            <option value="0"> - нет - </option>
                            @if(!empty($business_groups))
                                @foreach($business_groups as $key => $value)
                                    <option value="{{ $key }}" @if($tariff->business_group == $key) selected @endif>{{ $value }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="sort">Тип: {{ $tariff->tariff_type }}</label>
                    </div>

                    @if(!empty($tariff->service_type) && !empty($serviceTypes[$tariff->service_type]))
                    <div class="form-group">
                        <label for="sort">ServiceType: <span class="badge badge-secondary">{{ $serviceTypes[$tariff->service_type] }} [{{ $tariff->service_type }}]</span></label>
                    </div>
                    @endif

                    <div class="form-group">
                        <label for="sort">Цена: {{ $tariff->price }}грн</label>
                    </div>

                    <div class="form-group {{ $errors->has('price_old') ? 'has-error' : '' }}">
                        <label for="price_old">Старая цена:</label>
                        <input type="number" min="0" max="999999" id="price_old" name="price_old" class="form-control" value="{{ old('price_old', ($tariff->price_old) ? $tariff->price_old : '') }}">
                        @if($errors->has('price_old'))
                            <em class="invalid-feedback">
                                {{ $errors->first('price_old') }}
                            </em>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="sort">Billing ID: {{ $tariff->billing_id }}</label>
                    </div>

                    <div class="form-group">
                        <label for="sort">Тип клиента:@if($tariff->user_type == 1) Физ.Лицо @elseif($tariff->user_type == 2) Юр.Лицо @endif</label>
                    </div>

                    <div class="form-group">
                        <label for="sort">Период:@if($tariff->monthly == 1) Месячный @elseif($tariff->daily == 1) Дневной @endif</label>
                    </div>

                    <div class="form-group">
                        <div class="toggle-label">Скидка по предоплате</div>
                        <div class="toggle-btn @if($tariff->prepay_discount == 1) active @endif">
                            <input type="checkbox" class="cb-value" name="prepay_discount" value="1" @if($tariff->prepay_discount == 1) checked @endif />
                            <span class="round-btn"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="toggle-label">7 дней</div>
                        <div class="toggle-btn @if($tariff->can_seven_days == 1) active @endif">
                            <input type="checkbox" class="cb-value" name="can_seven_days" value="1" @if($tariff->can_seven_days == 1) checked @endif />
                            <span class="round-btn"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="toggle-label">Оплата бонусами</div>
                        <div class="toggle-btn @if($tariff->bonus_payment == 1) active @endif">
                            <input type="checkbox" class="cb-value" name="bonus_payment" value="1" @if($tariff->bonus_payment == 1) checked @endif />
                            <span class="round-btn"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="toggle-label">Начисление бонусов</div>
                        <div class="toggle-btn @if($tariff->get_bonus == 1) active @endif">
                            <input type="checkbox" class="cb-value" name="get_bonus" value="1" @if($tariff->get_bonus == 1) checked @endif />
                            <span class="round-btn"></span>
                        </div>
                    </div>

                    @if($tariff->tariff_type == 'union')
                        <div class="form-group">
                            <label for="union_group">Список каналов аналогичен с</label>
                            <select class="form-control" name="channels_from">
                                <option value="0"> - не указано - </option>
                                @if(!empty($tvs))
                                    @foreach($tvs as $tv)
                                        <option value="{{ $tv->id }}" @if($tariff->channels_from == $tv->id) selected @endif>{{ $tv->title_ru }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    @endif

                    <div class="form-group">
                        <div class="toggle-label">+ Фильмы</div>
                        <div class="toggle-btn @if($tariff->has_films == 1) active @endif">
                            <input type="checkbox" class="cb-value" id="has_films" name="has_films" value="1" @if($tariff->has_films == 1) checked @endif />
                            <span class="round-btn"></span>
                        </div>
                    </div>
                    <div class="films-block @if($tariff->has_films == 1) active @endif">

                            <div class="form-group">
                                <label for="sort">Цена +фильмы</label>
                                <input type="number" min="1" max="99999" id="films_price" name="films_price" class="form-control" value="{{ old('films_price', ($tariff->films_price) ? $tariff->films_price : '') }}">
                            </div>
                            <div class="form-group">
                                <label for="sort">Заголовок Фильмы RU</label>
                                <input type="text" name="films_title_ru" class="form-control" value="{{ old('films_title_ru', ($tariff->films_title_ru) ? $tariff->films_title_ru : '') }}">
                            </div>
                            <div class="form-group">
                                <label for="sort">Заголовок Фильмы UA</label>
                                <input type="text" name="films_title_ua" class="form-control" value="{{ old('films_title_ua', ($tariff->films_title_ua) ? $tariff->films_title_ua : '') }}">
                            </div>
                            <div class="form-group">
                                <label for="sort">Заголовок Фильмы EN</label>
                                <input type="text" name="films_title_en" class="form-control" value="{{ old('films_title_en', ($tariff->films_title_en) ? $tariff->films_title_en : '') }}">
                            </div>

                    </div>
                </div>
            </div>

            <div>
                <button class="btn btn-success btn-save" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/tariffs" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>



    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <div class="alert-inner"></div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

@endsection

@section('scripts')
    <script>
        CKEDITOR.replace( 'description_ru' );
        CKEDITOR.replace( 'description_ua' );
        CKEDITOR.replace( 'description_en' );

        $('.btn-save').click(function (e) {
            e.preventDefault();
            $('.alert-inner').html('');
            $('.alert-danger').hide();
            var titleRu = $('#title_ru').val();
            var titleUa = $('#title_ua').val();
            var titleEn = $('#title_en').val();

            var price = '{{ $tariff->price }}';
            var priceOld = $('#price_old').val();

            var errors = '';
            if(titleRu == ''){errors = errors + 'Заполните поле *Заголовок RU*<br />';}
            if(titleRu.length > 255){errors = errors + '*Заголовок RU* должен быть не длинее 255 символов<br />';}
            if(titleUa == ''){errors = errors + 'Заполните поле *Заголовок UA*<br />';}
            if(titleUa.length > 255){errors = errors + '*Заголовок UA* должен быть не длинее 255 символов<br />';}
            if(titleEn == ''){errors = errors + 'Заполните поле *Заголовок EN*<br />';}
            if(titleEn.length > 255){errors = errors + '*Заголовок EN* должен быть не длинее 255 символов<br />';}

            if(priceOld != ''){
                if(parseFloat(priceOld) <= parseFloat(price)){
                    errors = errors + 'Старая цена должна быть выше текущей цены<br />';
                }
            }

            if(errors != ''){
                $('.alert-inner').html(errors);
                $('.alert-danger').show();
            } else {
                $('#tariff-form').submit();
            }

        });


        $('#has_films').change(function (e){
            if($('#has_films').filter(':checked').length){
                $('.films-block').addClass('active');
            } else {
                $('.films-block').removeClass('active');
            }
        });

    </script>
@endsection
