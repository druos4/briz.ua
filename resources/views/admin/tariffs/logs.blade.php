@extends('layouts.admin')
@section('title')
    Логи изменения тарифов
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
    </style>
@endsection
@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-secondary" href="{{ route("admin.tariffs.index") }}">
                <i class="fas fa-chevron-left"></i> Список тарифов
            </a>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Логи изменения тарифов
        </div>

        <div class="card-body">
            @if(!empty($logs))
                <table class="table table-striped">
                    <thead>
                        <th>Тариф</th>
                        <th>Дата</th>
                        <th>Автор</th>
                        <th>Изменения</th>
                    </thead>
                    <tbody>
                        @foreach($logs as $log)
                            <tr>
                                <td>
                                    {{ $log->tariff->title_ru }}
                                    <br />ID: {{ $log->tariff->id }}
                                </td>
                                <td>
                                    <i class="fas fa-clock"></i> {{ $log->created_at }}
                                </td>
                                <td>
                                    <i class="fas fa-user"></i> {{ $log->author->surname }} {{ $log->author->name }} [{{ $log->author->id }}]
                                </td>
                                <td>
                                    <?=$log->log?>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>

        {{ $logs->appends(request()->except('page'))->links() }}
    </div>


    @section('scripts')

    @endsection
@endsection
