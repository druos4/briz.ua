@extends('layouts.admin')
@section('title')
    Тарифы
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .cross-line{
            text-decoration: line-through;
        }
    </style>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">

            <a class="btn btn-info" href="/admin/tariffs/sync" role="button"><i class="fas fa-sync"></i> Синхронизировать</a>
            <a class="btn btn-secondary" href="/admin/tariffs/logs" role="button"><i class="fas fa-list"></i> Логи изменений</a>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Фильтр
        </div>

        <div class="card-body">

            <form id="form-filter" action="/admin/tariffs" method="GET" enctype="multipart/form-data">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label>ID</label>
                        <input type="number" min="1" class="form-control w-80" name="id" @if(!empty($filter['id'])) value="{{ $filter['id'] }}" @endif>
                    </div>

                    <div class="col-auto">
                        <label>Тип</label>
                        <select class="form-control" name="tariff_type">
                            <option value="">Все</option>
                            <option value="internet" @if(isset($filter['tariff_type']) && $filter['tariff_type'] == 'internet') selected @endif>Internet</option>
                            <option value="ctv" @if(isset($filter['tariff_type']) && $filter['tariff_type'] == 'ctv') selected @endif>Ctv</option>
                            <option value="iptv" @if(isset($filter['tariff_type']) && $filter['tariff_type'] == 'iptv') selected @endif>Iptv</option>
                            <option value="union" @if(isset($filter['tariff_type']) && $filter['tariff_type'] == 'union') selected @endif>Union</option>
                        </select>
                    </div>

                    <div class="col-auto">
                        <label>Название</label>
                        <input type="text" class="form-control w-160" name="title" @if(!empty($filter['title'])) value="{{ $filter['title'] }}" @endif>
                    </div>

                    <div class="col-auto">
                        <label>Активность</label>
                        <select class="form-control" name="active">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['active']) && $filter['active'] == 1) selected @endif>Да</option>
                            <option value="0" @if(isset($filter['active']) && $filter['active'] == 0) selected @endif>Нет</option>
                        </select>
                    </div>


                    <div class="col-auto">
                        <label>Пероид</label>
                        <select class="form-control" name="period">
                            <option value="">Все</option>
                            <option value="month" @if(isset($filter['period']) && $filter['period'] == 'month') selected @endif>Месячный</option>
                            <option value="day" @if(isset($filter['period']) && $filter['period'] == 'day') selected @endif>Дневной</option>
                        </select>
                    </div>

                    <div class="col-auto">
                        <label>Клиент</label>
                        <select class="form-control" name="user_type">
                            <option value="">Все</option>
                            <option value="1" @if(isset($filter['user_type']) && $filter['user_type'] == 1) selected @endif>Физ.Лицо</option>
                            <option value="2" @if(isset($filter['user_type']) && $filter['user_type'] == 2) selected @endif>Юр.Лицо</option>
                        </select>
                    </div>

                    @if(!empty($index_groups))
                    <div class="col-auto">
                        <label>Группа главной стр.</label>
                        <select class="form-control" name="index_group">
                            <option value="">Все</option>
                            @foreach($index_groups as $key => $value)
                                <option value="{{ $key }}" @if(isset($filter['index_group']) && $filter['index_group'] == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif

                    @if(!empty($business_group))
                    <div class="col-auto">
                        <label>Группа для бизнесса</label>
                        <select class="form-control" name="business_group">
                            <option value="">Все</option>
                            @foreach($business_group as $key => $value)
                                <option value="{{ $key }}" @if(isset($filter['business_group']) && $filter['business_group'] == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif

                    <div class="col-auto">
                        <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/tariffs?reset=y" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <div class="card">
        <div class="card-header">
            Тарифы
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th style="min-width: 120px">Тип
                                @if(!empty($sort) && $sort['field'] == 'tariff_type' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'tariff_type' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="tariff_type" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="tariff_type" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="tariff_type" data-direction="desc"></i>
                            </th>
                            <th style="min-width: 120px">ServiceType</th>
                            <th style="min-width: 150px">
                                Названия
                                @if(!empty($sort) && $sort['field'] == 'title_ru' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'title_ru' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="title_ru" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="title_ru" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="title_ru" data-direction="desc"></i>
                            </th>
                            <th>Картинка</th>
                            <th style="min-width: 120px">Цена
                                @if(!empty($sort) && $sort['field'] == 'price' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'price' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="price" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="price" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="price" data-direction="desc"></i>
                            </th>

                            <th style="min-width: 120px">Сорт.
                                @if(!empty($sort) && $sort['field'] == 'sort' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'sort' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="sort" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="sort" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="sort" data-direction="desc"></i>
                            </th>

                            <th style="min-width: 120px">Первый
                                @if(!empty($sort) && $sort['field'] == 'first_to_show' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'first_to_show' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="first_to_show" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="first_to_show" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="first_to_show" data-direction="desc"></i>
                            </th>

                            <th style="min-width: 120px">Акт.
                                @if(!empty($sort) && $sort['field'] == 'active' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'active' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="active" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="active" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="active" data-direction="desc"></i>
                            </th>
                            <th>Bil.ID</th>
                            <th>Обновлено</th>

                            <th style="min-width: 60px">ID
                                @if(!empty($sort) && $sort['field'] == 'id' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'id' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="id" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="id" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="id" data-direction="desc"></i>
                            </th>


                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($items))
                            @foreach($items as $key => $item)
                                <tr id="item-{{ $item->id }}">
                                    <td>
                                        @can('tariffs_edit')
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-bars"></i>
                                                </button>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" href="/admin/tariffs/{{$item->id}}/edit" role="button" title="Редактировать"><i class="fas fa-pencil-alt"></i> Редактировать</a>
                                                </div>
                                            </div>
                                        @endcan
                                    </td>
                                    <td>
                                        {{ $item->tariff_type }}
                                    </td>
                                    <td>
                                        @if(!empty($item->service_type))
                                            {{ $item->service_type }}
                                        @endif
                                        @if(!empty($item->service_type) && !empty($serviceTypes[$item->service_type]))
                                            <br /><span class="badge badge-secondary">{{ $serviceTypes[$item->service_type] }}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @can('tariffs_edit')
                                            <a href="/admin/tariffs/{{$item->id}}/edit" title="Редактировать">{{ $item->title_ru }}</a>
                                            @if($item->title_ua != '')
                                                <br /><a href="/admin/tariffs/{{$item->id}}/edit" title="Редактировать">{{ $item->title_ua }}</a>
                                            @endif
                                            @if($item->title_en != '')
                                                <br /><a href="/admin/tariffs/{{$item->id}}/edit" title="Редактировать">{{ $item->title_en }}</a>
                                            @endif
                                        @endcan
                                    </td>
                                    <td>
                                        @if(!empty($item->picture))
                                            <img src="{{ $item->picture }}" style="max-width: 100px; max-height: 60px;" />
                                        @endif
                                    </td>
                                    <td>
                                        {{ $item->price }} грн.
                                        @if(!empty($item->price_old))
                                            <br /><span class="cross-line">{{ $item->price_old }} грн.</span>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $item->sort }}
                                    </td>
                                    <td>
                                        @if($item->first_to_show == true)
                                            <span class="badge badge-success">Да</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->active == 1)
                                            <span class="badge badge-success">Да</span>
                                        @else
                                            <span class="badge badge-danger">Нет</span>
                                        @endif

                                        @if(!empty($item->show_from))
                                            <br /><span class="badge badge-warning">Показ от {{ $item->show_from }}</span>
                                        @endif
                                        @if(!empty($item->show_to))
                                                <br /><span class="badge badge-warning">Показ до {{ $item->show_to }}</span>
                                        @endif

                                    </td>
                                    <td>
                                        {{ $item->billing_id }}
                                    </td>
                                    <td>
                                        {{ $item->updated_at }}
                                    </td>
                                    <td>
                                        @can('tariffs_edit')
                                            <a href="/admin/tariffs/{{$item->id}}/edit" title="Редактировать">
                                        @endcan
                                            {{ $item->id }}
                                        @can('tariffs_edit')
                                           </a>
                                        @endcan
                                    </td>


                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <div>
                @if(!empty($items))
                    {{ $items->appends(request()->except('page'))->links() }}
                @endif
            </div>
        </div>
    </div>


    @section('scripts')
        <script>
            $('.i-sort').click(function (e) {
                e.preventDefault();
                var field = $(this).data('field');
                var direction = $(this).data('direction');
                var form = $('#form-filter').serialize();
                form = form + '&sort=' + field + '&direction=' + direction;
                window.location.href = "/admin/tariffs?" + form;
            });
        </script>
    @endsection
@endsection
