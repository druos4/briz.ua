@extends('layouts.admin')
@section('title')
    Добавить дом на карту
@endsection
@section('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>


    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet.markercluster@1.3.0/dist/MarkerCluster.Default.css" />

    <script src="https://unpkg.com/leaflet.markercluster@1.3.0/dist/leaflet.markercluster.js"></script>

    <script src="https://unpkg.com/esri-leaflet@2.5.3/dist/esri-leaflet.js"
            integrity="sha512-K0Vddb4QdnVOAuPJBHkgrua+/A9Moyv8AQEWi0xndQ+fqbRfAFd47z4A9u1AW/spLO0gEaiE1z98PK1gl5mC5Q=="
            crossorigin=""></script>

    <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
          integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
          crossorigin="">
    <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
            integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
            crossorigin=""></script>

    <style>
        .alert-danger, .alert-success{
            display: none;
        }

        .btn-house{
            margin:2px 2px;
        }
    </style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        Добавить дом на карту
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-md-8">

                <div id="mapid" style="width: 100%; height: 600px;"></div>

            </div>
            <div class="col-md-4">
                <form action="" method="POST" enctype="multipart/form-data">
                @csrf
                    <p>Укажите на карте нужное здание и заполните необходимые поля.</p>
                    <input type="hidden" name="addr_id" id="addr_id" value="0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Широта (LAT)*</label>
                                <input type="text" id="lat" name="lat" class="form-control" required value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Долгота (LNG)*</label>
                                <input type="text" id="lon" name="lon" class="form-control" required value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                        <label for="title_ru">Улица*</label>
                        <input type="text" id="addr_name" name="addr_name" class="form-control" required value="">
                    </div>
                    <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                        <label for="title_ru">Номер дома* (* - для всей улицы)</label>
                        <input type="text" id="addr_num" name="addr_num" class="form-control" required value="">
                    </div>

                    <div class="form-group">
                        <div class="toggle-label">Наличие интернета</div>
                        <div class="toggle-btn">
                            <input type="checkbox" class="cb-value" name="has_inet" value="1" />
                            <span class="round-btn"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="toggle-label">Наличие КТВ</div>
                        <div class="toggle-btn">
                            <input type="checkbox" class="cb-value" name="has_ctv" value="1" />
                            <span class="round-btn"></span>
                        </div>
                    </div>
                    <button class="btn btn-success btn-add-marker" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                    <div class="alert alert-danger" role="alert">
                    </div>
                    <div class="alert alert-success" role="alert">
                        Точка добавлена!
                    </div>
                </form>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <b>Адреса без маркеров:</b>
                @if(!empty($houses))
                    @foreach($houses as $house)
                        <button type="button" class="btn btn-secondary btn-house" data-id="{{ $house->id }}"
                                data-addr="{{ $house->Name }}"
                                data-num="{{ $house->Dom }}" id="house-{{ $house->id }}">
                            {{ $house->Name }}, {{ $house->Dom }} <i class="fas fa-pencil-alt"></i>
                        </button>
                    @endforeach
                @else
                    Нет не отмеченных адресов.
                @endif
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="markerUpdModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Редактировать точку</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="marker-upd-form" action="" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" id="marker_id" value="0">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Широта (LAT)*</label>
                                <input type="text" id="marker_lat" name="lat" class="form-control" required value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Долгота (LNG)*</label>
                                <input type="text" id="marker_lon" name="lon" class="form-control" required value="">
                            </div>
                        </div>
                    </div>

                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Улица*</label>
                                <input type="text" id="marker_addr_name" name="addr_name" class="form-control" required value="">
                            </div>
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Номер дома*</label>
                                <input type="text" id="marker_addr_num" name="addr_num" class="form-control" required value="">
                            </div>

                    <div class="form-group">
                        <div class="toggle-label">Наличие интернета</div>
                        <div class="toggle-btn">
                            <input type="checkbox" class="cb-value" name="has_inet" value="1" />
                            <span class="round-btn"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="toggle-label">Наличие КТВ</div>
                        <div class="toggle-btn">
                            <input type="checkbox" class="cb-value" name="has_ctv" value="1" />
                            <span class="round-btn"></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                <button type="button" class="btn btn-success">Сохранить</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var mymap = L.map('mapid').setView([46.48282147637722, 30.735668696567473], 11);
/*
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1
        }).addTo(mymap);
*/

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1
        }).addTo(mymap);

        var geocodeService = L.esri.Geocoding.geocodeService();


        var markerName = '';


        var markers = L.markerClusterGroup();



/*
        $.ajax({
            type:'POST',
            url:'/map/get-own-markers',
            data:{},
            success:function(data) {
                $.each(data.streets, function( index, value ) {
                    if(value.address_num == '*'){
                        markerName = value.address_name;
                    } else {
                        markerName = value.address_name + ' ' +value.address_num
                    }
                    markerName = markerName + '<br />' +
                        '<a href="" class="marker-upd" data-id="' + value.id + '"><i class="fas fa-pencil-alt"></i> Редактировать</a><br />' +
                        '<a href="" class="marker-del"><i class="fas fa-remove"></i> Удалить</a>';
                    //L.marker([parseFloat(value.lat), parseFloat(value.lon)]).addTo(mymap).bindPopup(markerName);

                    var marker = L.marker([parseFloat(value.lat), parseFloat(value.lon)]);
                    markers.addLayer(marker);

                });

                mymap.addLayer(markers);

            }
        });*/

        $.ajax({
            type:'POST',
            url:'/map/load-markers',
            data:{},
            success:function(data) {


                $.each(data.markers, function( index, value ) {
                    if(value.lat != '' && value.lon != ''){
                        var title = 'lat=' + parseFloat(value.lat) + ' | lon=' + parseFloat(value.lon) + ' | ' + value.street + ', ' + value.dom;
                        var marker = L.marker([parseFloat(value.lat), parseFloat(value.lon)]).bindPopup(title);
                        markers.addLayer(marker);
                    }


                });

                mymap.addLayer(markers);

            }
        });


        var popup = L.popup();
        function onMapClick(e) {
            geocodeService.reverse().latlng(e.latlng).run(function (error, result) {
                if (error) {
                    return;
                }
                var num = result.address.AddNum;
                var addr = result.address.Address;
                $.ajax({
                    type:'POST',
                    url:'/map/find-by-address',
                    data:{num:num, address: addr},
                    success:function(data) {



                    }
                });

                //L.marker(result.latlng).addTo(map).bindPopup(result.address.Match_addr).openPopup();
            });

            /*
            popup
            .setLatLng(e.latlng)
            .setContent("Вы указали на карте " + e.latlng.toString())
            .openOn(mymap);

            $('#lat').val(e.latlng.lat);
            $('#lon').val(e.latlng.lng);

             */
        }

        mymap.on('click', onMapClick);



        $('.btn-add-marker').click(function (e) {
            e.preventDefault();
            var check = true;
            var formLat = $('#lat').val();
            var formLon = $('#lon').val();
            var formAddrName = $('#addr_name').val();
            var formAddrNum = $('#addr_num').val();

            var alertMessage = $('.alert-danger');
            var successMessage = $('.alert-success');


            alertMessage.hide();
            alertMessage.html('');

            if(formLat == ''){
                check == false;
                alertMessage.show();
                alertMessage.html('Укажите широту точки');
            }
            if(formLon == ''){
                check == false;
                alertMessage.show();
                alertMessage.html('Укажите долготу точки');
            }
            if(formAddrName == ''){
                check == false;
                alertMessage.show();
                alertMessage.html('Укажите адресс точки');
            }
            if(formAddrNum == ''){
                check == false;
                alertMessage.show();
                alertMessage.html('Укажите номер дома');
            }


            if(check == true){
                L.marker([parseFloat(formLat), parseFloat(formLon)]).addTo(mymap).bindPopup(formAddress);

                $('#lat').val('');
                $('#lon').val('');
                $('#addr_name').val('');
                $('#addr_num').val('');

                mymap.closePopup();

                successMessage.show();
                setTimeout(function(){
                    successMessage.hide();
                },1500);
            }


        });


        $('.btn-house').click(function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var addr = $(this).data('addr');
            var num = $(this).data('num');
            $('.btn-house').removeClass('btn-primary');
            $('.btn-house').addClass('btn-secondary');

            $('#address').val(addr);
            $('#addr_name').val(addr);
            $('#addr_num').val(num);

            $('#addr_id').val(id);
            $(this).removeClass('btn-secondary');
            $(this).addClass('btn-primary');
        });


        $( document ).on( "click", ".marker-upd", function(e) {
            e.preventDefault();
            e.stopPropagation();
            var id = $(this).data('id');
            $.ajax({
                type:'POST',
                url:'/admin/maps/get-by-id',
                data:{id:id},
                success:function(data) {
                    console.log(data);

                    $('#marker_id').val(data.marker.id);
                    $('#marker_lat').val(data.marker.lat);
                    $('#marker_lon').val(data.marker.lon);
                    $('#marker_addr_name').val(data.marker.address_name);
                    $('#marker_addr_num').val(data.marker.address_num);



                    $('#markerUpdModal').modal('show');
                }
            });


        });
    </script>
@endsection
