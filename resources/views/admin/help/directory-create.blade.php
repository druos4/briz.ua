@extends('layouts.admin')
@section('title')
    Создание элемента
@endsection
@section('styles')
    {{--<script src="/ckeditor4/ckeditor.js"></script>--}}
    <script src="/ckeditorfull/ckeditor.js"></script>
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
    </style>
@endsection

@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            @if($parent_id > 0)
                <a class="btn btn-secondary" href="/admin/help/{{ $parent_id }}/children">
            @else
                <a class="btn btn-secondary" href="{{ route("admin.help.index") }}">
            @endif
                <i class="fas fa-chevron-left"></i> Назад
            </a>
        </div>
    </div>

<div class="card">
    <div class="card-header">
        Создание элемента
    </div>

    <div class="card-body">
        <form action="/admin/help/store" method="POST" enctype="multipart/form-data">
            @csrf

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основные параметры</a>
                </li>
                @if($parent_id > 0)
                <li class="nav-item">
                    <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="false">Подробно</a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Название элемента RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru', isset($product) ? $product->title_ru : '') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ua">Название элемента UA</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" value="{{ old('title_ua', isset($product) ? $product->title_ua : '') }}">
                                @if($errors->has('title_ua'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_en">Название элемента EN</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" value="{{ old('title_en', isset($product) ? $product->title_en : '') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group has-error">
                                <label for="slug">SLUG*</label>
                                <a href="" class="btn-make-slug">Сгенерировать SLUG<i class="fas fa-arrow-down"></i></a>
                                <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug', isset($product) ? $product->slug : '') }}">
                                @if($errors->has('slug'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('slug') }}
                                    </em>
                                @endif
                            </div>

                            <div class="row" style="margin-top: 20px;">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title">Краткое описание RU</label>
                                        <input type="text" id="anons_ru" name="anons_ru" class="form-control" value="{{ old('anons_ru', isset($product) ? $product->section_description_ru : '') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Краткое описание UA</label>
                                        <input type="text" id="anons_ua" name="anons_ua" class="form-control" value="{{ old('anons_ua', isset($product) ? $product->section_description_ua : '') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Краткое описание EN</label>
                                        <input type="text" id="anons_en" name="anons_en" class="form-control" value="{{ old('anons_en', isset($product) ? $product->section_description_en : '') }}">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="form-group">
                                <label for="parent_id">Родительский элемент</label>

                                <select name="parent_id" id="parent_id" class="form-control">
                                    <option value="0"> - верхний уровень - </option>
                                    @if(isset($levels) && count($levels) > 0)
                                        <?php $a0 = 0; ?>
                                        @foreach($levels as $key => $level)
                                            <?php $a0++; ?>
                                            <option value="{{ $level->id }}" @if(!empty($parent_id) && $parent_id == $level->id) selected @endif>{{$a0}}. {{ $level->title_ru }}</option>
                                            @if(count($level->children))
                                                <?php $a1 = 0; ?>
                                                @foreach($level->children as $child)
                                                    <?php $a1++; ?>
                                                    <option value="{{ $child->id }}" @if(!empty($parent_id) && $parent_id == $child->id) selected @endif>&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}. {{ $child->title_ru }}</option>
                                                    @if(count($child->children))
                                                        <?php $a2 = 0; ?>
                                                        @foreach($child->children as $ch)
                                                            <?php $a2++; ?>
                                                            <option value="{{ $ch->id }}" @if(!empty($parent_id) && $parent_id == $ch->id) selected @endif>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$a0}}.{{$a1}}.{{$a2}}. {{ $ch->title_ru }}</option>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                </select>

                            </div>

                            <div class="form-group">
                                <div class="toggle-label">Активность</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="active" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                <label for="sort">Сортировка</label>
                                <input type="number" id="sort" name="sort" class="form-control" min="1" max="99999" value="{{ old('sort', '100') }}">
                            </div>

                            <div class="form-group has-error">
                                <label for="image_ru">Картинка</label><br />
                                <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                                    <input type="file" class="" name="image" accept="image/*">
                                </label>
                                @if($errors->has('image'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('image') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="parent_id">Режим показа вложенных элементов</label>
                                <select name="articles_show_type" id="articles_show_type" class="form-control">
                                    <option value="lenta">Лента</option>
                                    <option value="plitka">Плитка</option>
                                </select>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                    <h3>Описание RU</h3>
                    <textarea name="detail_ru" id="detail_ru" class="form-control" rows="20"></textarea>
                    <br /><br />
                    <h3>Описание UA</h3>
                    <textarea name="detail_ua" id="detail_ua" class="form-control" rows="20"></textarea>
                    <br /><br />
                    <h3>Описание EN</h3>
                    <textarea name="detail_en" id="detail_en" class="form-control" rows="20"></textarea>
                    <br /><br />

                </div>

                <div class="tab-pane fade" id="metas" role="tabpanel" aria-labelledby="metas-tab">
                    <div class="form-group">
                        <label for="meta_title_ru">Meta title RU</label>
                        <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru', isset($product) ? $product->meta_title_ru : '') }}">
                        <label for="meta_title_ua">Meta title UA</label>
                        <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua', isset($product) ? $product->meta_title_ua : '') }}">
                        <label for="meta_title_en">Meta title EN</label>
                        <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en', isset($product) ? $product->meta_title_en : '') }}">
                    </div>

                    <hr />
                    <div class="form-group">
                        <label for="meta_description_ru">Meta description RU</label>
                        <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru', isset($product) ? $product->meta_description_ru : '') }}">
                        <label for="meta_description_ua">Meta description UA</label>
                        <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua', isset($product) ? $product->meta_description_ua : '') }}">
                        <label for="meta_description_en">Meta description EN</label>
                        <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en', isset($product) ? $product->meta_description_en : '') }}">
                    </div>

                    <hr />
                    <div class="form-group">
                        <label for="meta_keywords_ru">Meta keywords RU</label>
                        <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru', isset($product) ? $product->meta_keywords_ru : '') }}">
                        <label for="meta_keywords_ua">Meta keywords UA</label>
                        <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua', isset($product) ? $product->meta_keywords_ua : '') }}">
                        <label for="meta_keywords_en">Meta keywords EN</label>
                        <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en', isset($product) ? $product->meta_keywords_en : '') }}">
                    </div>

                </div>
            </div>

            <div>
                <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Сохранить</button>


                @if($parent_id > 0)
                    <a class="btn btn-secondary" href="/admin/help/{{ $parent_id }}/children">
                        @else
                            <a class="btn btn-secondary" href="{{ route("admin.help.index") }}">
                                @endif
                                <i class="fas fa-remove"></i> Отменить
                            </a>
            </div>

        </form>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        CKEDITOR.replace( 'detail_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'detail_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'detail_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
@endsection
