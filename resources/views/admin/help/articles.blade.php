@extends('layouts.admin')
@section('title')
    Статьи помощи
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .table-responsive{
            margin-bottom: 50px;
        }
    </style>
@endsection
@section('content')
    @can('help_access')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                @if($parent->parent_id > 0)
                    <a class="btn btn-secondary" href="/admin/help/{{ $parent->parent_id }}/children">
                @else
                    <a class="btn btn-secondary" href="/admin/help">
                @endif
                    <i class="fas fa-chevron-left"></i> Вернуться к разделам
                </a>
                <a class="btn btn-success" href="/admin/help/{{ $parent->id }}/articles/create">
                    <i class="fas fa-plus"></i> Создать статью
                </a>
            </div>
        </div>
    @endcan



    <div class="card">
        <div class="card-header">
            Статьи помощи "{{ $parent->title_ru }}"
        </div>

        <div class="card-body">
            <div class="table-responsive">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="min-width: 60px"></th>

                        <th style="min-width: 150px">
                            Названия
                        </th>
                        <th style="min-width: 150px">
                            Иконка
                        </th>
                        <th style="min-width: 60px">
                            Акт.
                        </th>
                        <th style="min-width: 60px">
                            Сорт.
                        </th>
                        <th>Обновлено</th>
                        <th style="min-width: 60px">
                            ID
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(!empty($articles))
                    @foreach($articles as $item)
                        <tr id="item-{{ $item->id }}">
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="/help/{{ $parent->slug }}/{{ $item->slug }}"><i class="fas fa-eye"></i> Открыть на сайте</a>


                                        <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="/admin/help/{{ $parent->id }}/articles/{{ $item->id }}/edit"><i class="fas fa-pen"></i> Редактировать статью</a>


                                            <a class="dropdown-item btn-item-del" href=""
                                               data-id="{{$item->id}}" data-title="{{ $item->title_ru }} [{{ $item->id }}]" data-url="/admin/help/{{ $parent->id }}/article/{{ $item->id }}"
                                               role="button" title="Удалить"><i class="fas fa-trash"></i> Удалить статью</a>

                                    </div>
                                </div>
                            </td>
                            <td>
                                {{ $item->title_ru }}
                                @if($item->title_ua != '') <br />{{ $item->title_ua }} @endif
                                @if($item->title_en != '') <br />{{ $item->title_en }} @endif
                            </td>
                            <td>
                                @if($item->icon != '')
                                    <img src="{{ $item->icon }}" style="max-height:80px; max-width:100px;" />
                                @endif
                            </td>
                            <td>
                                @if($item->active == 1)
                                    <span class="badge badge-success">Да</span>
                                @else
                                    <span class="badge badge-danger">Нет</span>
                                @endif
                            </td>
                            <td>
                                {{ $item->sort }}
                            </td>
                            <td>
                                <?=drawDateTime($item->updated_at)?>
                            </td>
                            <td>
                                {{ $item->id }}
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>


            </div>

        </div>
    </div>


    @section('scripts')
        <script>

        </script>
    @endsection
@endsection
