@extends('layouts.admin')
@section('title')
    Редактирование группы &laquo;{{ $faq_group->title_ru }}&raquo;
@endsection
@section('styles')

@endsection

@section('content')

<div class="card">
    <div class="card-header">
        Редактирование группы &laquo;{{ $faq_group->title_ru }}&raquo;
    </div>

    <div class="card-body">
        <form action="/admin/faqs/{{ $faq_group->id }}/update" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">

                    <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                        <label for="title_ru">Заголовок RU*</label>
                        <input type="text" id="title_ru" name="title_ru" class="form-control" required value="{{ old('title_ru', isset($faq_group) ? $faq_group->title_ru : '') }}">
                    </div>

                    <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                        <label for="title_ua">Заголовок UA</label>
                        <input type="text" id="title_ua" name="title_ua" class="form-control" value="{{ old('title_ua', isset($faq_group) ? $faq_group->title_ua : '') }}">
                    </div>

                    <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                        <label for="title_en">Заголовок EN</label>
                        <input type="text" id="title_en" name="title_en" class="form-control" value="{{ old('title_en', isset($faq_group) ? $faq_group->title_en : '') }}">
                    </div>

                </div>
                <div class="col-md-4">

                    <div class="form-group">
                        <div class="toggle-label">Активность</div>
                        <div class="toggle-btn @if($faq_group->active == 1) active @endif">
                            <input type="checkbox" class="cb-value" name="active" value="1" @if($faq_group->active == 1) checked @endif />
                            <span class="round-btn"></span>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('sort') ? 'has-error' : '' }}">
                        <label for="sort">Сортировка</label>
                        <input type="number" min="1" max="99999" id="sort" name="sort" class="form-control" value="{{ old('sort', isset($faq_group) ? $faq_group->sort : 10) }}">
                    </div>

                    <div class="form-group">
                        <label for="image">Картинка</label><br />
                        <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                            <input type="file" class="" name="image" accept="image/*">
                        </label>
                        @if($errors->has('image'))
                            <em class="invalid-feedback">
                                {{ $errors->first('image') }}
                            </em>
                        @endif
                        @if(!empty($faq_group->icon))
                            <br /><img class="" id="preview-picture_ru" src="{{ $faq_group->icon }}" alt="preview-picture" style="max-width:120px; max-height:120px;"><br />
                            <input type="checkbox" name="del-pic" value="1"> удалить
                        @endif
                    </div>

            </div>

            <div>
                <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/faqs" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>

@endsection

@section('scripts')
@endsection
