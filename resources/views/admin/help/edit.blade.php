@extends('layouts.admin')
@section('title')
    Редактирование новости
@endsection
@section('styles')
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <link href="{{ asset('cropper/cropper.css') }}" rel="stylesheet" />
    <style>
        .publish_date{
            width:160px;
            display: inline-block;
        }
        .publish_time{
            width:60px;
            display: inline-block;
        }
    </style>
@endsection

@section('content')

    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-secondary" href="{{ route("admin.news.index") }}">
                <i class="fas fa-chevron-left"></i> Назад
            </a>
        </div>
    </div>

<div class="card">

    <div class="card-header">
        Редактирование новости
    </div>

    <div class="card-body">
        <form action="{{ route("admin.news.update", [$news->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')


            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основное</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="anons-tab" data-toggle="tab" href="#anons" role="tab" aria-controls="anons" aria-selected="true">Анонс</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="true">Подробно</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru', ($news->title_ru) ? $news->title_ru : '') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок UA</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" value="{{ old('title_ua', ($news->title_ua) ? $news->title_ua : '') }}">
                                @if($errors->has('title_ua'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок EN</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" value="{{ old('title_en', ($news->title_en) ? $news->title_en : '') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="slug">SLUG*</label>
                                <a href="" class="btn-make-slug">Сгенерировать SLUG<i class="fas fa-arrow-down"></i></a>
                                <input type="text" id="slug" name="slug" class="form-control" required value="{{ old('slug', ($news->slug) ? $news->slug : '') }}">
                            </div>

                            <div class="form-group">
                                <label for="tags">Теги</label>

                                <select name="tags[]" id="tags" class="form-control select2" multiple="multiple">
                                    @if(!empty($tags))
                                        @foreach($tags as $tag)
                                            <option value="{{ $tag->id }}" @if(!empty($tags_in) && in_array($tag->id,$tags_in)) selected @endif>
                                                {{ $tag->title_ua }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="toggle-label">Активность</div>
                                <div class="toggle-btn @if($news->active == 1) active @endif">
                                    <input type="checkbox" class="cb-value" name="active" value="1" @if($news->active == 1) checked @endif />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="toggle-label">Опубликовано с</div>
                                <div>
                                <input type="date" class="form-control publish_date" name="publish_date" value="">
                                <input type="text" class="form-control publish_time" name="publish_time" placeholder="__:__" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="toggle-label">Закрепить</div>
                                <div class="toggle-btn @if($news->on_top == 1) active @endif">
                                    <input type="checkbox" class="cb-value" name="on_top" value="1" @if($news->on_top == 1) checked @endif />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="picture">Картинка анонса</label><br />
                                <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                                    <input type="file" class="" id="input" name="image" accept="image/*">
                                </label>
                                <input type="hidden" id="prev-pic" name="prev-pic" value="">

                                @if(isset($news->picture) && $news->picture != '')
                                    <br /><img class="" id="preview-picture" src="{{$news->picture}}" alt="preview-picture" style="max-width:120px; max-height:120px;"><br />
                                    <input type="checkbox" name="del-pic" value="1"> удалить
                                @else
                                    <img class="" id="preview-picture" src="" alt="preview-picture" style="display:none; max-width:120px; max-height:120px;"><br />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="anons" role="tabpanel" aria-labelledby="anons-tab">
                    <label for="anons_ru">Анонс RU</label>
                    <textarea name="anons_ru" id="anons_ru" class="form-control" rows="12"><?=$news->anons_ru?></textarea>
                    <hr />
                    <label for="anons_ua">Анонс UA</label>
                    <textarea name="anons_ua" id="anons_ua" class="form-control" rows="12"><?=$news->anons_ua?></textarea>
                    <hr />
                    <label for="anons_en">Анонс EN</label>
                    <textarea name="anons_en" id="anons_en" class="form-control" rows="12"><?=$news->anons_en?></textarea>
                </div>

                <div class="tab-pane fade" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                    <label for="detail_ru">Подробный текст RU</label>
                    <textarea name="detail_ru" id="detail_ru" class="form-control" rows="12"><?=$news->detail_ru?></textarea>
                    <hr />
                    <label for="detail_ua">Подробный текст UA</label>
                    <textarea name="detail_ua" id="detail_ua" class="form-control" rows="12"><?=$news->detail_ua?></textarea>
                    <hr />
                    <label for="detail_en">Подробный текст EN</label>
                    <textarea name="detail_en" id="detail_en" class="form-control" rows="12"><?=$news->detail_en?></textarea>
                </div>

                <div class="tab-pane fade" id="metas" role="tabpanel" aria-labelledby="metas-tab">
                    <div class="form-group">
                        <label for="meta_title_ru">Meta title RU</label>
                        <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru', ($news->meta_title_ru) ? $news->meta_title_ru : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title UA</label>
                        <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua', ($news->meta_title_ua) ? $news->meta_title_ua : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title EN</label>
                        <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en', ($news->meta_title_en) ? $news->meta_title_en : '') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_description_ru">Meta description RU</label>
                        <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru', ($news->meta_description_ru) ? $news->meta_description_ru : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description UA</label>
                        <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua', ($news->meta_description_ua) ? $news->meta_description_ua : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description EN</label>
                        <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en', ($news->meta_description_en) ? $news->meta_description_en : '') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_keywords_ru">Meta keywords RU</label>
                        <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru', ($news->meta_keywords_ru) ? $news->meta_keywords_ru : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords UA</label>
                        <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua', ($news->meta_keywords_ua) ? $news->meta_keywords_ua : '') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords EN</label>
                        <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en', ($news->meta_keywords_en) ? $news->meta_keywords_en : '') }}">
                    </div>
                </div>

            </div>
            <div>
                <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/news" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>

@endsection

@section('scripts')
    @include('admin.modals.image-cropper')

    <script>
        CKEDITOR.replace( 'anons_ru' );
        CKEDITOR.replace( 'anons_ua' );
        CKEDITOR.replace( 'anons_en' );
        CKEDITOR.replace( 'detail_ru' );
        CKEDITOR.replace( 'detail_ua' );
        CKEDITOR.replace( 'detail_en' );
    </script>
    {{--<script src="{{ asset('adminscripts/cropper.js') }}"></script>--}}
    <script src="{{ asset('adminscripts/jquery.maskeditinput.min.js') }}"></script>
    <script>
        $('.publish_time').mask("99:99");
    </script>
@endsection
