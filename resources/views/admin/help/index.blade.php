@extends('layouts.admin')
@section('title')
    Помощь
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .table-responsive{
            margin-bottom: 50px;
        }
    </style>
@endsection
@section('content')
    @can('help_access')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="/admin/help/create?parent_id={{ (!empty($parent->id)) ? $parent->id : 0 }}">
                    <i class="fas fa-plus"></i> Создать элемент
                </a>
            </div>
        </div>
    @endcan



    <div class="card">
        <div class="card-header">
            Помощь
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="min-width: 60px"></th>

                        <th style="min-width: 150px">
                            Названия
                        </th>
                        <th style="min-width: 150px">
                            Иконка
                        </th>
                        <th style="min-width: 60px">
                            Акт.
                        </th>
                        <th style="min-width: 60px">
                            Вложенные<br />элементы
                        </th>
                        <th style="min-width: 60px">
                            Показ<br />вложенных
                        </th>
                        <th style="min-width: 60px">
                            Сорт.
                        </th>
                        <th>Обновлено</th>
                        <th style="min-width: 60px">
                            ID
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($parent))
                        <tr class="table-primary">
                            <td>
                                @if($parent->parent_id > 0)
                                    <a href="/admin/help/{{ $parent->parent_id }}/children">
                                @else
                                        <a href="/admin/help">
                                @endif
                                            <i class="fas fa-chevron-left"></i> <i class="fas fa-folder-open"></i>
                                </a>
                            </td>
                            <td>
                                {{ $parent->title_ru }}
                                @if($parent->title_ua != '') <br />{{ $parent->title_ua }} @endif
                                @if($parent->title_en != '') <br />{{ $parent->title_en }} @endif
                            </td>
                            <td>
                                @if($parent->icon != '')
                                    <img src="{{ $parent->icon }}" style="max-height:80px; max-width:100px;" />
                                @endif
                            </td>
                            <td colspan="2">
                                @if($parent->active == 1)
                                    <span class="badge badge-success">Да</span>
                                @else
                                    <span class="badge badge-danger">Нет</span>
                                @endif
                            </td>
                            <td>
                                @if($parent->articles_show_type == 'plitka')
                                    <i class="fas fa-grip-horizontal"></i>
                                @else
                                    <i class="fas fa-bars"></i>
                                @endif
                            </td>
                            <td colspan="3">
                            </td>
                        </tr>
                    @endif
                    @if(!empty($items))
                    @foreach($items as $item)
                        <tr id="item-{{ $item->id }}">
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="/help/{{ $item->slug }}" target="_blank"><i class="fas fa-eye"></i> Открыть на сайте</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="/admin/help/{{ $item->id }}/children"><i class="fas fa-cubes"></i> Вложенные элементы</a>
                                        <div class="dropdown-divider"></div>

                                            <a class="dropdown-item" href="/admin/help/{{ $item->id }}/edit"><i class="fas fa-pen"></i> Редактировать элемент</a>

                                        @if($item->children_count > 0)
                                            <a class="dropdown-item btn-item-del-no" href=""
                                               data-id="{{$item->id}}" data-title="{{ $item->title_ru }}"
                                               role="button" title="Удалить"><i class="fas fa-trash"></i> Удалить элемент</a>
                                        @else
                                            <a class="dropdown-item btn-item-del" href=""
                                               data-id="{{$item->id}}" data-title="{{ $item->title_ru }} [{{ $item->id }}]" data-url="/admin/help/{{ $item->id }}"
                                               role="button" title="Удалить"><i class="fas fa-trash"></i> Удалить элемент</a>
                                        @endif


                                    </div>
                                </div>
                            </td>
                            <td>
                                {{ $item->title_ru }}
                                @if($item->title_ua != '') <br />{{ $item->title_ua }} @endif
                                @if($item->title_en != '') <br />{{ $item->title_en }} @endif
                            </td>
                            <td>
                                @if($item->icon != '')
                                    <img src="{{ $item->icon }}" style="max-height:80px; max-width:100px;" />
                                @endif
                            </td>
                            <td>
                                @if($item->active == 1)
                                    <span class="badge badge-success">Да</span>
                                @else
                                    <span class="badge badge-danger">Нет</span>
                                @endif
                            </td>
                            <td>
                                <a href="/admin/help/{{ $item->id }}/children" title="Перейти к элементам">
                                    @if($item->children_count > 0)
                                        <span class="badge badge-warning">{{ $item->children_count }}</span>
                                    @else
                                        <span class="badge badge-secondary">{{ $item->children_count }}</span>
                                    @endif
                                </a>
                            </td>
                            <td>
                                @if($item->articles_show_type == 'plitka')
                                    <i class="fas fa-grip-horizontal"></i>
                                @else
                                    <i class="fas fa-bars"></i>
                                @endif
                            </td>
                            <td>
                                {{ $item->sort }}
                            </td>
                            <td>
                                <?=drawDateTime($item->updated_at)?>
                            </td>
                            <td>
                                {{ $item->id }}
                            </td>
                        </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>

            </div>

        </div>
    </div>



    <div class="modal fade" id="noDeleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Удалить элемент</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning" role="alert">
                        Элемент нельзя удалить, так как содержит вложенные элементы.<br />
                        Удалите предварительно все вложенные.
                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('scripts')
        <script>
            $('.btn-item-del-no').click(function (e) {
                e.preventDefault();
                $('#noDeleteModal').modal('show');
            });
        </script>
    @endsection
@endsection
