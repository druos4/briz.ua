@extends('layouts.admin')
@section('title')
    F.A.Q. {{ $faq_group->title_ru }}
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .table-responsive{
            margin-bottom: 50px;
        }
    </style>
@endsection
@section('content')

        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-default" href="/admin/faqs">
                    <i class="fas fa-chevron-left"></i> Назад
                </a>
                @can('help_access')
                <a class="btn btn-success" href="/admin/faqs/{{ $faq_group->id }}/elements/create">
                    <i class="fas fa-plus"></i> Создать вопрос
                </a>
                @endcan
            </div>
        </div>




    <div class="card">
        <div class="card-header">
            F.A.Q. &laquo;{{ $faq_group->title_ru }}&raquo;
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="min-width: 60px"></th>

                            <th style="min-width: 150px">
                                Названия
                            </th>
                            <th style="min-width: 60px">
                                Активность
                            </th>
                            <th style="min-width: 60px">
                                Сортировка
                            </th>
                            <th>Обновлено</th>
                            <th style="min-width: 60px">
                                ID
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($faqs))
                        @foreach($faqs as $item)
                            <tr id="item-{{ $item->id }}">
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            @can('help_access')
                                                <a class="dropdown-item" href="/admin/faqs/{{ $faq_group->id }}/elements/{{ $item->id }}/edit"><i class="fas fa-pen"></i> Редактировать вопрос</a>
                                            @endcan
                                            @can('help_access')
                                                <a class="dropdown-item btn-item-del" href=""
                                                   data-id="{{$item->id}}" data-title="{{ $item->title_ru }} [{{ $item->id }}]" data-url="/admin/faqs/{{ $faq_group->id }}/elements/{{ $item->id }}"
                                                   role="button" title="Удалить"><i class="fas fa-trash"></i> Удалить вопрос</a>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{ $item->title_ru }}
                                    @if($item->title_ua != '') <br />{{ $item->title_ua }} @endif
                                    @if($item->title_en != '') <br />{{ $item->title_en }} @endif
                                </td>
                                <td>
                                    @if($item->active == 1)
                                        <span class="badge badge-success">Да</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td>
                                    {{ $item->sort }}
                                </td>
                                <td>
                                    {{ $item->updated_at }}
                                </td>
                                <td>
                                    {{ $item->id }}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

        </div>
    </div>


    @section('scripts')
        <script>

        </script>
    @endsection
@endsection
