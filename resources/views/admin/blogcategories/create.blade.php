@extends('layouts.admin')
@section('title')
    Создание категории блога
@endsection
@section('styles')
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
        .invalid-feedback{
            display: block;
        }
    </style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        Создание категории блога
    </div>

    <div class="card-body">
        <form action="{{ route("admin.blogcategories.store") }}" method="POST" id="entity-form" enctype="multipart/form-data">
            @csrf
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основное</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок UA*</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua') }}">
                                @if($errors->has('title_ua'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок EN*</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="slug">SLUG*</label>
                                <a href="" class="btn-make-slug">Сгенерировать SLUG<i class="fas fa-arrow-down"></i></a>
                                <input type="text" id="slug" name="slug" class="form-control" required value="{{ old('slug') }}">
                                @if($errors->has('slug'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('slug') }}
                                    </em>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="toggle-label">Активность</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="is_active" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="metas" role="tabpanel" aria-labelledby="metas-tab">
                    <div class="form-group">
                        <label for="meta_title_ru">Meta title RU</label>
                        <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title UA</label>
                        <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title EN</label>
                        <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_description_ru">Meta description RU</label>
                        <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description UA</label>
                        <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description EN</label>
                        <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_keywords_ru">Meta keywords RU</label>
                        <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords UA</label>
                        <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords EN</label>
                        <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en') }}">
                    </div>

                </div>

            </div>
            <div>
                <button class="btn btn-success btn-save" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/blogcategories" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>


<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <div class="alert-inner"></div>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

@endsection

@section('scripts')

    <script>
        $('.btn-save').click(function (e) {
            e.preventDefault();
            $('.alert-inner').html('');
            var title = $('#title_ru').val();
            var titleUa = $('#title_ua').val();
            var slug = $('#slug').val();
            var errors = '';
            if(title == ''){
                errors = 'Заполните поле *Заголовок RU*<br />';
            }
            if(slug == ''){
                errors = errors + 'Заполните поле *SLUG*<br />';
            }
            if(title.length > 255){
                errors = '*Заголовок RU* должен быть не длинее 255 символов<br />';
            }
            if(titleUa.length > 255){
                errors = '*Заголовок UA* должен быть не длинее 255 символов<br />';
            }
            if(slug.length > 255){
                errors = '*Заголовок RU* должен быть не длинее 255 символов<br />';
            }

            if(errors != ''){
                $('.alert-inner').html(errors);
                $('.alert-danger').show();
            } else {

                $.ajax({
                    type:'POST',
                    url:'/admin/blogcategories/slug',
                    data:{slug:slug,entity:'category'},
                    success:function(data) {

                        if(data.success == false){
                            $('.alert-inner').html('Поле SLUG должно быть уникальным!');
                            $('.alert-danger').show();
                        } else {

                            $('#entity-form').submit();

                        }

                    }
                });
            }
        });
    </script>
@endsection
