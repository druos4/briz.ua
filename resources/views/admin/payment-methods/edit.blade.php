@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
    <link href="{{ asset('choosen/chosen.css') }}" rel="stylesheet" />
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.3/cropper.css'>
    <style>

        .chosen-container{
            width:100%!important;
        }

        img {
            max-width: 100%; /* This rule is very important, please do not ignore this! */
        }

        #canvas {
            height: 600px;
            width: 600px;
            background-color: #ffffff;
            cursor: default;
            border: 1px solid black;
        }

        .level-0{
            padding-left:10px
        }
        .level-1{
            padding-left:40px;
        }
        .level-2{
            padding-left:70px;
        }
        .level-0:hover,.level-1:hover,.level-2:hover{
            background: #e9e9e9;
        }
        .level-active{
            color: #155724;
            background-color: #c3e6cb;
        }
        .level-parent{
            color: #fff;
            background-color: #007bff;
            border-color: #007bff;
        }

        .more_cats{
            height: 20px!important;
            width: 20px!important;
            float: right!important;
        }
    </style>


@endsection
@section('content')
    <a class="btn btn-default" href="{{ route('admin.payment-methods.index') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a>
    <div class="card">
        <div class="card-header bg-info">
            Редактирование типа оплаты
        </div>

        <div class="card-body">
            <form action="{{ route('admin.payment-methods.update',$paymentMethod->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="title_ru">Название RU*</label>
                            <input type="text" id="title_ru" name="title_ru" class="form-control" required value="{{ old('title_ru', isset($paymentMethod) ? $paymentMethod->title_ru : '') }}">
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="title_ua">Название UA</label>
                            <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', isset($paymentMethod) ? $paymentMethod->title_ua : '') }}">
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label for="title_en">Название EN</label>
                            <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', isset($paymentMethod) ? $paymentMethod->title_en : '') }}">
                        </div>

                        <h3>Описание RU</h3>
                        <textarea name="description_ru" id="description_ru" class="form-control" rows="3">{{ old('description_ru', isset($paymentMethod) ? $paymentMethod->description_ru : '') }}</textarea>
                        <br /><br />
                        <h3>Описание UA</h3>
                        <textarea name="description_ua" id="description_ua" class="form-control" rows="3">{{ old('description_ua', isset($paymentMethod) ? $paymentMethod->description_ua : '') }}</textarea>
                        <br /><br />
                        <h3>Описание EN</h3>
                        <textarea name="description_en" id="description_en" class="form-control" rows="3">{{ old('description_en', isset($paymentMethod) ? $paymentMethod->description_en : '') }}</textarea>
                        <br /><br />

                    </div>
                    <div class="col-md-4">

                        <div class="form-group">
                            <div class="toggle-label">Активность</div>
                            <div class="toggle-btn @if($paymentMethod->active == 1) active @endif">
                                <input type="checkbox" class="cb-value" name="active" value="1" @if($paymentMethod->active == 1) checked @endif />
                                <span class="round-btn"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="picture">Картинка</label>
                            <input type="file" class="form-control-file" name="image" id="image">
                            @if(isset($paymentMethod->picture) && $paymentMethod->picture != '')
                                <img src="{{$paymentMethod->picture}}" style="max-width:200px; max-height:100px;" />
                                <input type="checkbox" name="del_pic" value="1"> удалить
                            @endif
                        </div>
                    </div>
                </div>

                <div>
                    <input class="btn btn-success" type="submit" value="Сохранить">
                    <a class="btn btn-default" href="/admin/payment-methods/" role="button">Отменить</a>
                </div>
            </form>
        </div>
    </div>












@endsection
@section('scripts')




    <script src="{{ asset('choosen/chosen.jquery.js') }}"></script>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(".chosen-select").chosen();

        $('.cb-value').click(function() {
            var mainParent = $(this).parent('.toggle-btn');
            if($(mainParent).find('input.cb-value').is(':checked')) {
                $(mainParent).addClass('active');
                $(mainParent).find('input.cb-value').attr('checked', true);
            } else {
                $(mainParent).removeClass('active');
                $(mainParent).find('input.cb-value').attr('checked', false);
            }

        });

        $('.btn-make-slug').click(function(e) {
            e.preventDefault();
            var title = $('#title_ru').val();
            $.ajax({
                type:'POST',
                url:'/make-slug',
                data:{title:title},
                success:function(data) {
                    $('#slug').val(data.slug);
                }
            });
        });
    </script>

@endsection
