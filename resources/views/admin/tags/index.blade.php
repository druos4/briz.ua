@extends('layouts.admin')
@section('title')
    Теги
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .table-responsive{
            padding-bottom: 50px;
        }
    </style>
@endsection
@section('content')
    @can('tag_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.tags.create") }}">
                    <i class="fas fa-plus"></i> Создать тег
                </a>
            </div>
        </div>
    @endcan

    <div class="card">
        <div class="card-header">
            Фильтр
        </div>

        <div class="card-body">

            <form id="form-filter" action="{{ route("admin.tags.index") }}" method="GET" enctype="multipart/form-data" class="form-inline">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label>ID</label>
                        <input type="text" class="form-control w-80" name="id" @if(!empty($filter['id'])) value="{{ $filter['id'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>Название</label>
                        <input type="text" class="form-control w-160" name="title" @if(!empty($filter['title'])) value="{{ $filter['title'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>Код</label>
                        <input type="text" class="form-control w-160" name="code" @if(!empty($filter['code'])) value="{{ $filter['code'] }}" @endif>
                    </div>

                    <div class="col-auto">
                        <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/tags?reset=y" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                    </div>
                </div>
            </form>
        </div>
    </div>



    <div class="card">
        <div class="card-header">
            Теги
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="min-width: 60px"></th>
                            <th style="min-width: 150px">Название RU
                                @if(!empty($sort) && $sort['field'] == 'title_ru' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'title_ru' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="title_ru" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="title_ru" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="title_ru" data-direction="desc"></i></th>
                            <th style="min-width: 150px">Название UA
                                @if(!empty($sort) && $sort['field'] == 'title_ua' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'title_ua' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="title_ua" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="title_ua" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="title_ua" data-direction="desc"></i></th>
                            <th style="min-width: 150px">Название EN
                                @if(!empty($sort) && $sort['field'] == 'title_en' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'title_en' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="title_en" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="title_en" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="title_en" data-direction="desc"></i></th>
                            <th>Код</th>
                            <th>Сортировка
                                @if(!empty($sort) && $sort['field'] == 'sort' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'sort' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="sort" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="sort" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="sort" data-direction="desc"></i></th></th>
                            <th>Обновлено</th>
                            <th style="min-width: 60px">ID
                                @if(!empty($sort) && $sort['field'] == 'id' && $sort['direction'] == 'desc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-show';
                                    ?>
                                @elseif(!empty($sort) && $sort['field'] == 'id' && $sort['direction'] == 'asc')
                                    <?php
                                    $s = 'sort-hide';
                                    $s_asc = 'sort-show';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @else
                                    <?php
                                    $s = 'sort-show';
                                    $s_asc = 'sort-hide';
                                    $s_desc = 'sort-hide';
                                    ?>
                                @endif
                                <i class="fas i-sort {{ $s }} fa-sort" data-field="id" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_desc }} fa-sort-amount-down" data-field="id" data-direction="asc"></i>
                                <i class="fas i-sort {{ $s_asc }} fa-sort-amount-up" data-field="id" data-direction="desc"></i>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tags as $key => $item)
                            <tr id="item-{{ $item->id }}">
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            @can('tag_edit')
                                                <a class="dropdown-item" href="/admin/tags/{{$item->id}}/edit"><i class="fas fa-pencil-alt"></i> Редактировать</a>
                                            @endcan
                                            <a class="dropdown-item" href="/admin/tags/{{ $item->id }}/history"><i class="fas fa-history"></i> История изменений</a>
                                            @can('tag_delete')
                                                <a class="dropdown-item btn-item-del" href="" target="_blank" data-id="{{$item->id}}" data-title="{{ $item->title_ru }} [{{ $item->id }}]" data-url="/admin/tags/{{ $item->id }}"><i class="fas fa-trash"></i> Удалить</a>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="/admin/tags/{{$item->id}}/edit" title="Редактировать">{{ $item->title_ru ?? '' }}</a>
                                </td>
                                <td>
                                    <a href="/admin/tags/{{$item->id}}/edit" title="Редактировать">{{ $item->title_ua ?? '' }}</a>
                                </td>
                                <td>
                                    <a href="/admin/tags/{{$item->id}}/edit" title="Редактировать">{{ $item->title_en ?? '' }}</a>
                                </td>
                                <td>
                                    <a href="/admin/tags/{{$item->id}}/edit" title="Редактировать">{{ $item->code }}</a>
                                </td>
                                <td>
                                    {{ $item->sort }}
                                </td>
                                <td>
                                    {{ $item->updated_at }}<br />
                                    {{ $item->updater->surname }} {{ $item->updater->name }} [{{ $item->updater->id }}]
                                </td>
                                <td>
                                    <a href="/admin/tags/{{$item->id}}/edit" title="Редактировать">{{ $item->id }}</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div>
                {{ $tags->appends(request()->except('page'))->links() }}
            </div>
        </div>
    </div>


    @section('scripts')
        <script>
            $('.i-sort').click(function (e) {
                e.preventDefault();
                var field = $(this).data('field');
                var direction = $(this).data('direction');
                var form = $('#form-filter').serialize();
                form = form + '&field=' + field + '&direction=' + direction;
                window.location.href = "/admin/tags?" + form;
            });
        </script>
    @endsection
@endsection
