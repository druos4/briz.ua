@extends('layouts.admin')
@section('title')
    Создание тега
@endsection
@section('styles')
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        Создание тега
    </div>

    <div class="card-body">
        <form action="{{ route("admin.tags.store") }}" id="form-create" method="POST" enctype="multipart/form-data">
            @csrf


                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control title_for_slug" required value="{{ old('title_ru') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок UA</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок EN</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>


                            <div class="form-group">
                                <label for="slug">Код*</label>
                                <a href="" class="btn-make-slug">Сгенерировать Код<i class="fas fa-arrow-down"></i></a>
                                <input type="text" id="slug" name="code" class="form-control" value="{{ old('code') }}">
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="slug">Сортировка</label>
                                <input type="number" id="sort" name="sort" class="form-control" min="1" max="999999" value="{{ old('sort') }}">
                            </div>

                        </div>
                    </div>





            <div>
                <button class="btn btn-success btn-create-submit" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/tags" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>

@endsection

@section('scripts')
@endsection
