@extends('layouts.admin')
@section('title')
    Комментарии в блоге
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .select2{
            /*min-width:150px!important;*/
            width: 200px!important;
        }
    </style>
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            Фильтр
        </div>

        <div class="card-body">

            <form id="form-filter" action="{{ route("admin.blog.comments.index") }}" method="GET" enctype="multipart/form-data">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label>ID</label>
                        <input type="number" min="1" class="form-control w-80" name="id" @if(!empty($filter['id'])) value="{{ $filter['id'] }}" @endif>
                    </div>
                    <div class="col-auto">
                        <label>Автор</label>
                        <input type="text" class="form-control w-160" name="author" @if(!empty($filter['author'])) value="{{ $filter['author'] }}" @endif>
                    </div>

                    <div class="col-auto">
                        <label>Модерация</label>
                        <select class="form-control" name="moderated">
                            <option value="">Все</option>
                            <option value="y" @if(isset($filter['moderated']) && $filter['moderated'] == 'y') selected @endif>Да</option>
                            <option value="n" @if(isset($filter['moderated']) && $filter['moderated'] == 'n') selected @endif>Нет</option>
                        </select>
                    </div>

                    <div class="col-auto">
                        <label>Статья</label>
                        <select class="form-control" name="blog">
                            <option value="">Все</option>
                            @if(!empty($blogs))
                                @foreach($blogs as $blog)
                                    <option value="{{ $blog->id }}" @if(!empty($filter['blog']) && $filter['blog'] == $blog->id) selected @endif>{{ $blog->title_ru }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="col-auto">
                        <button type="submit" class="btn btn-info"><i class="fas fa-filter"></i> Фильтр</button>
                        <a class="btn btn-default" href="/admin/blogs/comments" role="button"><i class="fas fa-remove"></i> Сбросить</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Комментарии в блоге
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Статья</th>
                            <th>Автор</th>
                            <th>Комментарий</th>
                            <th>Дата</th>
                            <th>Статус</th>
                            <th style="min-width: 60px">ID</th>
                            <th></th>
                        </tr>
                    </thead>
                <tbody>
                @if(!empty($comments))
                    @foreach($comments as $key => $item)
                        <tr id="item-{{ $item->id }}">
                            <td>
                                @if(!empty($item->blog))
                                    <a href="/blog/{{ $item->blog->slug }}" target="_blank">{{ $item->blog->title_ru }}</a>
                                @endif
                            </td>
                            <td>{{ $item->author }}</td>
                            <td>{{ $item->comment }}</td>
                            <td><?=drawDateTime($item->created_at)?></td>
                            <td>
                                <a class="btn btn-sm btn-success btn-moder-y" title="Заблокировать комментарий" @if(empty($item->moderated_at)) style="display:none;" @endif href="" data-id="{{ $item->id }}"><i class="fas fa-check"></i> Одобрен</a>
                                <a class="btn btn-sm btn-danger btn-moder-n" title="Одобрить комментарий" @if(!empty($item->moderated_at)) style="display:none;" @endif href="" data-id="{{ $item->id }}"><i class="fas fa-remove"></i> Блок</a>
                            </td>
                            <td>
                                {{ $item->id }}
                            </td>
                            <td>
                                <a class="btn btn-sm btn-warning" href="/admin/blogs/comments/{{ $item->id }}" role="button" title="Редактировать"><i class="fas fa-pen"></i></a>
                                <a class="btn btn-sm btn-danger btn-item-del" href=""
                                   data-id="{{$item->id}}" data-title="{{ $item->comment }} [{{ $item->id }}]" data-url="/admin/blogs/comments/{{ $item->id }}"
                                   role="button" title="Удалить"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    <div>

    @if(!empty($comments))
        {{ $comments->appends(request()->except('page'))->links() }}
    @endif
@endsection
@section('scripts')
    <script>
        $('.btn-moder-y').click(function (e){
            e.preventDefault();
            var id = $(this).data('id');
            var moder = 'n';
            $.ajax({
                type:'POST',
                url:'/admin/blogs/comments/' + id + '/moderate',
                data:{moder:moder},
                success:function(data) {
                    if(data.moder == 'n'){
                        $('#item-' + data.id + ' .btn-moder-y').hide();
                        $('#item-' + data.id + ' .btn-moder-n').show();
                    } else {
                        $('#item-' + data.id + ' .btn-moder-n').hide();
                        $('#item-' + data.id + ' .btn-moder-y').show();
                    }
                }
            });
        });
        $('.btn-moder-n').click(function (e){
            e.preventDefault();
            var id = $(this).data('id');
            var moder = 'y';
            $.ajax({
                type:'POST',
                url:'/admin/blogs/comments/' + id + '/moderate',
                data:{moder:moder},
                success:function(data) {
                    if(data.moder == 'n'){
                        $('#item-' + data.id + ' .btn-moder-y').hide();
                        $('#item-' + data.id + ' .btn-moder-n').show();
                    } else {
                        $('#item-' + data.id + ' .btn-moder-n').hide();
                        $('#item-' + data.id + ' .btn-moder-y').show();
                    }
                }
            });
        });
    </script>
@endsection
