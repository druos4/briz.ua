@extends('layouts.admin')
@section('title')
    Редактирование комментария
@endsection
@section('styles')
@endsection

@section('content')

    <div class="card">
        <div class="card-header">
            Редактирование комментария
        </div>

        <div class="card-body">
            <form action="/admin/blogs/comments/{{ $comment->id }}" method="POST" id="entity-form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-8">
                        <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                            <label for="title_ru">Автор</label>
                            <input type="text" id="author" name="author" class="form-control" required value="{{ old('author', ($comment->author) ? $comment->author : '') }}">
                            @if($errors->has('author'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('author') }}
                                </em>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                            <label for="comment">Комментарий</label>
                            <textarea name="comment" id="comment" class="form-control" rows="3"><?=old('comment', ($comment->comment) ? $comment->comment : '')?></textarea>
                        </div>
                    </div>
                    <div class="col-4">
                        <div>Комментарий к статье: <a href="/blog/{{ $comment->blog->slug }}" target="_blank">{{ $comment->blog->title_ru }}</a></div>

                        <div class="form-group">
                            <div class="toggle-label">Активность</div>
                            <div class="toggle-btn @if(!empty($comment->moderated_at)) active @endif">
                                <input type="checkbox" class="cb-value" name="is_active" value="1" @if(!empty($comment->moderated_at)) checked @endif />
                                <span class="round-btn"></span>
                            </div>
                        </div>
                    </div>
                </div>


                <div>
                    <button class="btn btn-success btn-save" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                    <a href="/admin/blogs/comments" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
