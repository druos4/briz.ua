@extends('layouts.admin')
@section('title')
    Создание группы
@endsection
@section('styles')
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
        .invalid-feedback{
            display: block;
        }
    </style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        Создание группы
    </div>

    <div class="card-body">
        <form action="/admin/faqs/store" method="POST" enctype="multipart/form-data">
            @csrf
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основное</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="metas" aria-selected="false">Описание</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                    <div class="row">
                        <div class="col-md-8">

                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control" required value="{{ old('title_ru') }}">
                                @if($errors->has('title_ru'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ru') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ua">Заголовок UA*</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua') }}">
                                @if($errors->has('title_ua'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_ua') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_en">Заголовок EN*</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en') }}">
                                @if($errors->has('title_en'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('title_en') }}
                                    </em>
                                @endif
                            </div>

                        </div>
                        <div class="col-md-4">

                            <div class="form-group">
                                <div class="toggle-label">Активность</div>
                                <div class="toggle-btn">
                                    <input type="checkbox" class="cb-value" name="active" value="1" />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('sort') ? 'has-error' : '' }}">
                                <label for="sort">Сортировка</label>
                                <input type="number" min="1" max="99999" id="sort" name="sort" class="form-control" value="{{ old('sort', 10) }}">
                                @if($errors->has('sort'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('sort') }}
                                    </em>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="image">Картинка</label><br />
                                <label class="label" data-toggle="tooltip" title="Загрузить картинку">
                                    <input type="file" class="" name="image" accept="image/*">
                                </label>
                                @if($errors->has('image'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('image') }}
                                    </em>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>

                <div class="tab-pane fade show" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                    <label for="description_ru">Подробный текст RU</label>
                    <textarea name="description_ru" id="description_ru" class="form-control" rows="12"></textarea>
                    <hr />
                    <label for="description_ua">Подробный текст UA</label>
                    <textarea name="description_ua" id="description_ua" class="form-control" rows="12"></textarea>
                    <hr />
                    <label for="description_en">Подробный текст EN</label>
                    <textarea name="description_en" id="description_en" class="form-control" rows="12"></textarea>
                </div>

                <div class="tab-pane fade show" id="metas" role="tabpanel" aria-labelledby="metas-tab">

                    <div class="form-group">
                        <label for="meta_title_ru">Meta title RU</label>
                        <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ old('meta_title_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title UA</label>
                        <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ old('meta_title_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title EN</label>
                        <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ old('meta_title_en') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_description_ru">Meta description RU</label>
                        <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ old('meta_description_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description UA</label>
                        <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ old('meta_description_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description EN</label>
                        <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ old('meta_description_en') }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_keywords_ru">Meta keywords RU</label>
                        <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ old('meta_keywords_ru') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords UA</label>
                        <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ old('meta_keywords_ua') }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords EN</label>
                        <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ old('meta_keywords_en') }}">
                    </div>

                </div>


            </div>









            <div>
                <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                <a href="/admin/faqs" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
            </div>

        </form>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        CKEDITOR.replace( 'description_ru' );
        CKEDITOR.replace( 'description_ua' );
        CKEDITOR.replace( 'description_en' );
    </script>
@endsection
