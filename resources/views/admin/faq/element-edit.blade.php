@extends('layouts.admin')
@section('title')
    Редактирование вопроса
@endsection
@section('styles')
    <script src="{{ asset('ckeditor4/ckeditor.js') }}"></script>
    <style>
        .bt-1{
            border-top: 1px solid #e1e1e1;
            margin-top:10px;
            padding-top:10px;
            padding-bottom:10px;
        }

        .insert-btn{
            display: inline-block;
        }
        .insert-select{
            display: inline-block;
            width:180px;
        }
        .insert-names{
            margin-top: 10px;
        }
    </style>
    <style>
        .alert-danger{
            display: none;
            position: fixed;
            width: 400px;
            top: 40%;
            left: 50%;
            margin-left: -200px;
        }
    </style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        Редактирование вопроса для &laquo;{{ $faq_group->title_ru }}&raquo;
    </div>

    <div class="card-body">
        <form action="/admin/faqs/{{ $faq_group->id }}/elements/{{ $faq->id }}/update" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Основное</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="metas-tab" data-toggle="tab" href="#metas" role="tab" aria-controls="metas" aria-selected="false">Meta-теги</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ $errors->has('title_ru') ? 'has-error' : '' }}">
                                <label for="title_ru">Заголовок RU*</label>
                                <input type="text" id="title_ru" name="title_ru" class="form-control" required value="{{ old('title_ru', isset($faq) ? $faq->title_ru : '') }}">
                            </div>

                            <div class="form-group {{ $errors->has('title_ua') ? 'has-error' : '' }}">
                                <label for="title_ua">Заголовок UA*</label>
                                <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', isset($faq) ? $faq->title_ua : '') }}">
                            </div>

                            <div class="form-group {{ $errors->has('title_en') ? 'has-error' : '' }}">
                                <label for="title_en">Заголовок EN*</label>
                                <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', isset($faq) ? $faq->title_en : '') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="toggle-label">Активность</div>
                                <div class="toggle-btn @if($faq->active == 1) active @endif">
                                    <input type="checkbox" class="cb-value" name="active" value="1" @if($faq->active == 1) checked @endif />
                                    <span class="round-btn"></span>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('sort') ? 'has-error' : '' }}">
                                <label for="sort">Сортировка</label>
                                <input type="number" min="1" max="99999" id="sort" name="sort" class="form-control" value="{{ old('sort', isset($faq) ? $faq->sort : 10) }}">
                            </div>
                        </div>
                    </div>

                    <div class="row bt-1">
                        <div class="col-md-8">
                            <label for="answer_ru">Ответ RU</label>
                            <textarea name="answer_ru" id="answer_ru" class="form-control" rows="12"><?=old('answer_ru', isset($faq) ? $faq->answer_ru : '')?></textarea>

                        </div>
                        <div class="col-md-4">
                            <div>Вставить в текст</div>
                            <div class="insert-names">
                                <div>Название или цену:</div>
                                <a class="btn btn-primary btn-insert-tariff insert-btn" data-id="ru" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_id_ru" id="service_id_ru">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <select class="form-control insert-select" name="service_field_ru" id="service_field_ru">
                                    <option value="0"> - выберите поле - </option>
                                    <option value="title">Название тарифа</option>
                                    <option value="price">Цена тарифа</option>
                                </select>
                            </div>


                            <div class="insert-names">
                                <div>Вставить разницу стоимости между:</div>
                                <a class="btn btn-primary btn-insert-diff insert-btn" data-id="ru" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_price1_ru" id="service_price1_ru">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                плюс
                                <select class="form-control insert-select" name="service_price2_ru" id="service_price2_ru">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <br />
                                минус
                                <select class="form-control insert-select" name="service_price0_ru" id="service_price0_ru">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>


                        </div>
                    </div>

                    <div class="row bt-1">
                        <div class="col-md-8">
                            <label for="answer_ru">Ответ UA</label>
                            <textarea name="answer_ua" id="answer_ua" class="form-control" rows="12"><?=old('answer_ua', isset($faq) ? $faq->answer_ua : '')?></textarea>

                        </div>
                        <div class="col-md-4">
                            <div>Вставить в текст</div>
                            <div class="insert-names">
                                <div>Название или цену:</div>
                                <a class="btn btn-primary btn-insert-tariff insert-btn" data-id="ua" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_id_ua" id="service_id_ua">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <select class="form-control insert-select" name="service_field_ua" id="service_field_ua">
                                    <option value="0"> - выберите поле - </option>
                                    <option value="title">Название тарифа</option>
                                    <option value="price">Цена тарифа</option>
                                </select>
                            </div>


                            <div class="insert-names">
                                <div>Вставить разницу стоимости между:</div>
                                <a class="btn btn-primary btn-insert-diff insert-btn" data-id="ua" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_price1_ua" id="service_price1_ua">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                плюс
                                <select class="form-control insert-select" name="service_price2_ua" id="service_price2_ua">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <br />
                                минус
                                <select class="form-control insert-select" name="service_price0_ua" id="service_price0_ua">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>


                        </div>
                    </div>

                    <div class="row bt-1">
                        <div class="col-md-8">
                            <label for="answer_ru">Ответ EN</label>
                            <textarea name="answer_en" id="answer_en" class="form-control" rows="12"><?=old('answer_en', isset($faq) ? $faq->answer_en : '')?></textarea>

                        </div>
                        <div class="col-md-4">
                            <div>Вставить в текст</div>
                            <div class="insert-names">
                                <div>Название или цену:</div>
                                <a class="btn btn-primary btn-insert-tariff insert-btn" data-id="en" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_id_en" id="service_id_en">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <select class="form-control insert-select" name="service_field_en" id="service_field_en">
                                    <option value="0"> - выберите поле - </option>
                                    <option value="title">Название тарифа</option>
                                    <option value="price">Цена тарифа</option>
                                </select>
                            </div>


                            <div class="insert-names">
                                <div>Вставить разницу стоимости между:</div>
                                <a class="btn btn-primary btn-insert-diff insert-btn" data-id="en" href="" role="button"><i class="fas fa-chevron-left"></i></a>
                                <select class="form-control insert-select" name="service_price1_en" id="service_price1_en">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                плюс
                                <select class="form-control insert-select" name="service_price2_en" id="service_price2_en">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <br />
                                минус
                                <select class="form-control insert-select" name="service_price0_en" id="service_price0_en">
                                    <option value="0"> - выберите тариф - </option>
                                    @if(!empty($tariffs))
                                        @foreach($tariffs as $tariff)
                                            <option value="{{ $tariff->id }}">{{ $tariff->tariff_type }} - {{ str_replace(':',' ',$tariff->title_ru) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>


                        </div>
                    </div>

                    <div class="row bt-1">
                        <div class="col-md-12">
                            <label for="answer_ru">Поисковые фразы (вставьте все нужные фразы для поиска через запятую)</label>
                            <textarea name="search" id="search" class="form-control" rows="2"><?=old('search', isset($faq) ? $faq->search : '')?></textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="metas" role="tabpanel" aria-labelledby="metas-tab">

                    <div class="form-group">
                        <label for="meta_title_ru">Meta title RU</label>
                        <input type="text" id="meta_title_ru" name="meta_title_ru" class="form-control" value="{{ $faq->meta_title_ru }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title UA</label>
                        <input type="text" id="meta_title_ua" name="meta_title_ua" class="form-control" value="{{ $faq->meta_title_ua }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_title">Meta title EN</label>
                        <input type="text" id="meta_title_en" name="meta_title_en" class="form-control" value="{{ $faq->meta_title_en }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_description_ru">Meta description RU</label>
                        <input type="text" id="meta_description_ru" name="meta_description_ru" class="form-control" value="{{ $faq->meta_description_ru }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description UA</label>
                        <input type="text" id="meta_description_ua" name="meta_description_ua" class="form-control" value="{{ $faq->meta_description_ua }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_description">Meta description EN</label>
                        <input type="text" id="meta_description_en" name="meta_description_en" class="form-control" value="{{ $faq->meta_description_en }}">
                    </div>

                    <hr />

                    <div class="form-group">
                        <label for="meta_keywords_ru">Meta keywords RU</label>
                        <input type="text" id="meta_keywords_ru" name="meta_keywords_ru" class="form-control" value="{{ $faq->meta_keywords_ru }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords UA</label>
                        <input type="text" id="meta_keywords_ua" name="meta_keywords_ua" class="form-control" value="{{ $faq->meta_keywords_ua }}">
                    </div>
                    <div class="form-group">
                        <label for="meta_keywords">Meta keywords EN</label>
                        <input type="text" id="meta_keywords_en" name="meta_keywords_en" class="form-control" value="{{ $faq->meta_keywords_en }}">
                    </div>

                </div>

            </div>


            <div class="row bt-1">
                <div class="col-md-12">
                    <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Сохранить</button>
                    <a href="/admin/faqs/{{ $faq_group->id }}/elements" class="btn btn-secondary"><i class="fas fa-remove"></i> Отменить</a>
                </div>
            </div>




        </form>

    </div>
</div>

@endsection

@section('scripts')
    <script>
        CKEDITOR.replace( 'answer_ru', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'answer_ua', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
        CKEDITOR.replace( 'answer_en', {
            filebrowserUploadUrl: "{{route('upload', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });


        $('.btn-insert-tariff').click(function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var target = 'answer_' + id;
            var tarif = $('#service_id_' + id).val();
            var field = $('#service_field_' + id).val();
            var name = $("#service_id_" + id + " option:selected").text();
            if(tarif != 0 && field != 0){

                var data = '{service:' + tarif + ':' + field + ':' + name + '}';
                CKEDITOR.instances[target].insertHtml(data);
            }
        });

        $('.btn-insert-diff').click(function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var target = 'answer_' + id;
            var tarif1 = $('#service_price1_' + id).val();
            var tarif2 = $('#service_price2_' + id).val();
            var tarif0 = $('#service_price0_' + id).val();
            var name1 = $("#service_price1_" + id + " option:selected").text();
            var name2 = $("#service_price2_" + id + " option:selected").text();
            var name0 = $("#service_price0_" + id + " option:selected").text();
            if(tarif1 != 0 && tarif2 != 0 && tarif0 != 0){

                var data = '{diff:' + tarif1 + ':' + tarif2 + ':' + tarif0 + ':цена ' + name1 + ' плюс цена ' + name2 + ' минус цена ' + name0 + '}';
                CKEDITOR.instances[target].insertHtml(data);
            }

        });

    </script>
@endsection
