@extends('layouts.admin')
@section('title')
    F.A.Q.
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .table-responsive{
            margin-bottom: 50px;
        }
    </style>
@endsection
@section('content')
    @can('faq_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="/admin/faqs/create">
                    <i class="fas fa-plus"></i> Создать группу
                </a>
            </div>
        </div>
    @endcan



    <div class="card">
        <div class="card-header">
            F.A.Q. группы
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="min-width: 60px"></th>

                            <th style="min-width: 150px">
                                Названия
                            </th>
                            <th style="min-width: 150px">
                                Иконка
                            </th>
                            <th style="min-width: 60px">
                                Активность
                            </th>
                            <th style="min-width: 60px">
                                Элементов
                            </th>
                            <th style="min-width: 60px">
                                Сортировка
                            </th>
                            <th>Обновлено</th>
                            <th style="min-width: 60px">
                                ID
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($faq_groups))
                        @foreach($faq_groups as $item)
                            <tr id="item-{{ $item->id }}">
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="/admin/faqs/{{ $item->id }}/elements"><i class="fas fa-cubes"></i> Перейти к элементам</a>
                                            <div class="dropdown-divider"></div>
                                            @can('faq_edit')
                                                <a class="dropdown-item" href="/admin/faqs/{{ $item->id }}/edit"><i class="fas fa-pen"></i> Редактировать группу</a>
                                            @endcan
                                            @can('faq_delete')
                                                <a class="dropdown-item btn-item-del" href=""
                                                   data-id="{{$item->id}}" data-title="{{ $item->title_ru }} [{{ $item->id }}]" data-url="/admin/faqs/{{ $item->id }}"
                                                   role="button" title="Удалить"><i class="fas fa-trash"></i> Удалить группу</a>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{ $item->title_ru }}
                                    @if($item->title_ua != '') <br />{{ $item->title_ua }} @endif
                                    @if($item->title_en != '') <br />{{ $item->title_en }} @endif
                                </td>
                                <td>
                                    @if($item->icon != '')
                                        <img src="{{ $item->icon }}" style="max-height:80px; max-width:100px;" />
                                    @endif
                                </td>
                                <td>
                                    @if($item->active == 1)
                                        <span class="badge badge-success">Да</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="/admin/faqs/{{ $item->id }}/elements" title="Перейти к элементам">
                                        <span class="badge badge-warning">{{ count($item->children) }}</span>
                                    </a>
                                </td>
                                <td>
                                    {{ $item->sort }}
                                </td>
                                <td>
                                    {{ $item->updated_at }}
                                </td>
                                <td>
                                    {{ $item->id }}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>

        </div>
    </div>


    @section('scripts')
        <script>

        </script>
    @endsection
@endsection
