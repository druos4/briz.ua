@extends('layouts.admin')
@section('title')
    Не найденные URL
@endsection
@section('styles')
    <style>
        .form-inline label{
            display: inline-block;
        }
        .w-80{
            width:80px!important;
        }
        .w-160{
            width:160px!important;
        }
        .table img{
            max-width:100px;
            max-height: 80px;
        }
        .i-sort{
            cursor:pointer;
        }
        .sort-hide{
            display: none;
        }
        .sort-show{
            display: inline-block;
        }
        .select2{
            /*min-width:150px!important;*/
            width: 200px!important;
        }
    </style>
@endsection
@section('content')

    <div class="card">
        <div class="card-header">
            Не найденные URL
        </div>

        <div class="card-body">
            <div class="table-responsive">
                @if(!empty($urls))
                    <ul class="list-group">
                        @foreach($urls as $url)
                            <li class="list-group-item">{{ $url->url }}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
            <div>
            @if(!empty($urls))
                {{ $urls->appends(request()->except('page'))->links() }}
            @endif
            </div>
        </div>
    </div>

@endsection
