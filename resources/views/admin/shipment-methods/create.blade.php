@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')
<a class="btn btn-default" href="{{ route('admin.shipment-methods.index') }}" role="button"><i class="fas fa-angle-left"></i> Назад</a>
<div class="card">
    <div class="card-header bg-info">
        Создание доставки
    </div>

    <div class="card-body">
        <form action="{{ route("admin.shipment-methods.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="title">Название RU*</label>
                        <input type="text" id="title_ru" name="title_ru" class="form-control" required value="{{ old('title_ru', isset($product) ? $product->title_ru : '') }}">
                    </div>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="title">Название UA</label>
                        <input type="text" id="title_ua" name="title_ua" class="form-control" required value="{{ old('title_ua', isset($product) ? $product->title_ua : '') }}">
                    </div>
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="title">Название EN</label>
                        <input type="text" id="title_en" name="title_en" class="form-control" required value="{{ old('title_en', isset($product) ? $product->title_en : '') }}">
                    </div>

                    <h3>Описание RU</h3>
                    <textarea name="description_ru" id="description_ru" class="form-control" rows="3"></textarea>
                    <br /><br />
                    <h3>Описание UA</h3>
                    <textarea name="description_ua" id="description_ua" class="form-control" rows="3"></textarea>
                    <br /><br />
                    <h3>Описание EN</h3>
                    <textarea name="description_en" id="description_en" class="form-control" rows="3"></textarea>
                    <br /><br />

                </div>
                <div class="col-md-4">

                    <div class="form-group">
                        <div class="toggle-label">Активность</div>
                        <div class="toggle-btn">
                            <input type="checkbox" class="cb-value" name="active" value="1" />
                            <span class="round-btn"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="picture">Картинка</label>
                        <input type="file" class="form-control-file" name="image" id="image">
                    </div>
                </div>
            </div>

            <div>
                <input class="btn btn-success" type="submit" value="Сохранить">
            </div>
        </form>
    </div>
</div>



@endsection
@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('.cb-value').click(function() {
            var mainParent = $(this).parent('.toggle-btn');
            if($(mainParent).find('input.cb-value').is(':checked')) {
                $(mainParent).addClass('active');
                $(mainParent).find('input.cb-value').attr('checked', true);
            } else {
                $(mainParent).removeClass('active');
                $(mainParent).find('input.cb-value').attr('checked', false);
            }

        });

        $('.btn-make-slug').click(function(e) {
            e.preventDefault();
            var title = $('#title_ru').val();
            $.ajax({
                type:'POST',
                url:'/make-slug',
                data:{title:title},
                success:function(data) {
                    $('#slug').val(data.slug);
                }
            });
        });
    </script>

@endsection
