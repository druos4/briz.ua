@extends('layouts.admin')
@section('styles')
    <link href="{{ asset('css/admin/category.css') }}" rel="stylesheet" />
@endsection
@section('content')
    @can('category_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.shipment-methods.create") }}">
                    <i class="fas fa-plus"></i> Создать тип доставки
                </a>
            </div>
        </div>
    @endcan
    <div class="card" style="padding-bottom:100px;">
        <div class="card-header">
            Список типов доставок
        </div>


        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <th></th>
                    <th>Иконка</th>
                    <th>Название RU</th>
                    <th>Название UA</th>
                    <th>Название EN</th>
                    <th>Активность</th>
                    <th>Сортировка</th>
                </thead>
                <tbody>
                    @if(isset($shipments) && count($shipments) > 0)
                        @foreach($shipments as $key => $item)
                            <tr id="row-{{$item->id}}">
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="/admin/shipment-methods/{{$item->id}}/edit">Редактировать</a>
                                            @if($item->type != 'preinstalled')
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item btn-prod-del" data-id="{{$item->id}}" data-title="{{$item->title}}" href="">Удалить</a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @if(isset($item->picture) && $item->picture != '')
                                        <a href="/admin/shipment-methods/{{$item->id}}/edit" title="Редактировать"><img src="{{$item->picture}}" style="max-width:60px; max-height:60px;" /></a>
                                    @endif
                                </td>
                                <td><a href="/admin/shipment-methods/{{$item->id}}/edit" title="Редактировать">{{ $item->title_ru }}</a></td>
                                <td><a href="/admin/shipment-methods/{{$item->id}}/edit" title="Редактировать">{{ $item->title_ua }}</a></td>
                                <td><a href="/admin/shipment-methods/{{$item->id}}/edit" title="Редактировать">{{ $item->title_en }}</a></td>
                                <td>
                                    @if($item->active == 1)
                                        <span class="badge badge-success">Да</span>
                                    @else
                                        <span class="badge badge-danger">Нет</span>
                                    @endif
                                </td>
                                <td>
                                    {{ $item->sort }}
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>

        </div>
    </div>
    @section('scripts')
        @parent



        <!-- Modal -->
        <div class="modal fade" id="productDelModal" tabindex="-1" role="dialog" aria-labelledby="categoryDelModal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="categoryDelModalLabel">Удалить тип оплаты</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="del-prod-id" value="0">
                        <p>Действительно удалить тип оплаты &laquo;<span id="del-prod-title"></span>&raquo;?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <button type="button" class="btn btn-danger btn-prod-delete">Удалить</button>
                    </div>
                </div>
            </div>
        </div>





        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

         /*   $('.btn-active-toggle').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var active = $(this).data('active');
                $.ajax({
                    type:'POST',
                    url:'/admin/categories-update-active',
                    data:{id:id,active:active},
                    success:function(data) {
                        if(data.id > 0){
                            if(data.active == 1){
                                $('#active-y-'+data.id).show();
                                $('#active-n-'+data.id).hide();
                            } else {
                                $('#active-y-'+data.id).hide();
                                $('#active-n-'+data.id).show();
                            }
                        }
                    }
                });
            });*/

            $('.btn-prod-del').click(function(e) {
                e.preventDefault();
                var id = $(this).data('id');
                var title = $(this).data('title');
                $('#del-prod-id').val(id);
                $('#del-prod-title').html(title);
                $('#productDelModal').modal('show');
            });

            $('.btn-prod-delete').click(function(e) {
                e.preventDefault();
                var id = $('#del-prod-id').val();
                $.ajax({
                    type:'POST',
                    url:'/admin/shipment-methods-delete',
                    data:{id:id},
                    success:function(data) {
                        if(data.id > 0){

                            $('#row-' + data.id).remove();
                            $('#del-prod-id').val('');
                            $('#del-prod-title').html('');
                            $('#productDelModal').modal('hide');

                        }
                    }
                });
            });







        </script>


    @endsection
@endsection
