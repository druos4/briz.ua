<?php

return [

    'title' => 'Update your browser',
    'description' => 'Your browser is not supported. To open our site and use all its features,
    try to enter through any of the listed browsers.',

];
