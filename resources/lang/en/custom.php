<?php

return [

    'test' => 'some text here',
    'title' => 'Інтернет та Телебачення зі знижкою 50% | БРІЗ в Одесі та області',
    'description' => 'Стабільний домашній інтернет в Одесі. Швидке підключення від 1 до 3 днів. Цілодобова підтримка. Ціни від 120 грн/міс',
    'keywords' => '',

    'category' => [
        'sort' => [
            'rating' => 'By rating',
            'cheap' => 'From cheap to expensive',
            'expensive' => 'From expensive to cheap',
        ],
    ],
    'remont' => [
        'title' => 'Dear subscribers!',
    ],
    'equipment' => [
        'allproducts' => 'All products',
        'title' => 'Equipment',
        'castota-raboty-wi-fi' => 'Support 5 GHz',
        'skorost-lan-portov' => 'Gigabit',
        'new' => 'New models',
        'discount' => 'Discounts',
        'top-products' => 'TOP equipment',
        'promo' => 'Promotion',
    ],
    'channels' => [
        'all' => 'All',
        'analog' => 'Analog',
        'digital' => 'Digital',
    ],
    'warranty' => [
        '14d' => '14 days',
        '6m' => '6 months',
        '12m' => '12 months',
        '24m' => '24 months',
        '36m' => '36 months',
        '48m' => '48 months',
    ],
    'metas' => [
        'help' => [
            'title' => 'Technical Support for Internet Provider BRIZ in Odessa',
            'title-child' => 'Technical support on Briz.ua',
            'description' => 'Technical support for Internet provider Briz provides complete instructions for setting up network equipment in an affordable and convenient format.',
            'description-child' => 'Providing full and deployed network equipment settings in the "Help" section on the website of the Internet provider Briz in Odessa.',
        ],
        'shop' => [
            'title' => 'Buy network equipment by BRIZ in Odessa',
            'description' => 'at a low price can be purchased at Briz Internet provider in Odessa.',
        ],
        'news' => [
            'title' => 'Internet provider news in BRIZ in Odessa',
            'description' => 'Only fresh news for subscribers of Internet provider Briz in Odessa (Ukraine).',
        ],
        'actions' => [
            'title' => 'BRIZ Internet provider shares in Odessa',
            'description' => 'Shares for clients of the Internet provider Briz in Odessa.',
        ],
        'blog' => [
            'title' => 'BRIZ Internet provider blog in Odessa ',
            'description' => 'Interesting and useful articles in the world of Internet technologies and network equipment on the blog Internet provider Briz in Odessa.',
        ],
        'blogCategory' => [
            'title' => 'ISP BRIZ Blog in Odessa',
            'description' => 'on the blog of the site www.briz.ua. Helpful articles on choosing and configuring network equipment',
        ],
    ],
    'helpBriz' => 'Internet provider Briz in Odesa',
    'emails' => [
        'validation' => [
            'required' => 'Email field required',
            'unique' => 'Email is already registered',
            'email' => 'Enter valid email',
            'less-1-minute' => 'Next attempt after',
            'sec' => 'seconds',
        ],

    ],
    'blog' => [
        'sort' => [
            'new' => 'New',
            'popular' => 'Popular',
        ],
        'pagination' => [
            'back' => 'Back',
            'forward' => 'Forward',
        ],
        'all-articles' => 'All articles',
        'comment' => [
            'no-author' => 'Enter author',
            'no-comment' => 'Enter comment',
            'no-id' => 'No blog id',
            'no-parent' => 'No parent',
        ],
    ],
];
