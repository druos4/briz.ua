<?php

return [

    'main' => 'Main',
    'news' => 'News',
    'actions' => 'Special offers',
    'faq' => 'FAQ',
    'search' => 'Search',
    'equipment' => 'Equipment',
    'help' => 'Help',
    'blog' => 'Blog',
];
