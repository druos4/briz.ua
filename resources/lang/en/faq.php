<?php

return [

    'meta-title' => 'FAQ on the website of the Internet provider Briz in Odessa',
    'meta-description-1' => 'Answers to the most frequent subscribers questions in the section',
    'meta-description-2' => 'on the website of the BRIZ Internet provider in Odessa.',
    'meta-keywords' => '',
    'child-meta-title' => 'FAQ on the site briz.ua',
    'child-meta-description' => 'Here you will find answers to the most frequent issues of subscribers of the Internet provider Briz in Odessa.',
];
