<?php

return [
   'errors' => [
       'invalid-phone' => 'Incorrect format',
       'invalid-captcha' => 'Incorrect captcha',
       'invalid-service' => 'Incorrect service code',
       'invalid-ghost' => 'Incorrect data.',
       'invalid-times' => 'You can retry send after',
       'invalid-product' => 'Incorrect product code',
   ],
];
