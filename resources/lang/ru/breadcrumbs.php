<?php

return [

    'main' => 'Главная',
    'news' => 'Новости',
    'actions' => 'Акции',
    'faq' => 'FAQ',
    'search' => 'Поиск',
    'equipment' => 'Оборудование',
    'help' => 'Помощь',
    'blog' => 'Блог',
];
