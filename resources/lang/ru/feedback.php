<?php

return [
   'errors' => [
       'invalid-phone' => 'Неверный формат',
       'invalid-captcha' => 'Неверный проверочный код',
       'invalid-service' => 'Неверный код сервиса',
       'invalid-ghost' => 'Отправленные некорректные данные.',
       'invalid-times' => 'Повторная отправка будет доступна через',
       'invalid-product' => 'Неверный код товара',
   ],
];
