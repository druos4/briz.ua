<?php

return [
   'errors' => [
       'invalid-phone' => 'Невірний формат',
       'invalid-captcha' => 'Невірний перевірочний код',
       'invalid-service' => 'Невірний код сервісу',
       'invalid-ghost' => 'Відправлено невірні дані.',
       'invalid-times' => 'Повторна відправка буде доступна через',
       'invalid-product' => 'Невірний код товару',
   ],
];
