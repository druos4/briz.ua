<?php

return [

    'main' => 'Головна',
    'news' => 'Новини',
    'actions' => 'Акції',
    'faq' => 'FAQ',
    'search' => 'Пошук',
    'equipment' => 'Обладнання',
    'help' => 'Допомога',
    'blog' => 'Блог',
];
