export const TYPE_PRODUCT_SERVICE = "service";
export const TYPE_PRODUCT_PRODUCT = "product";
export const TYPE_PRODUCT_PRODUCT_BY_ADDRESS = "address";
export const TYPE_PRODUCT_MOBILE_CALLME = "callme";
export const TYPE_PRODUCT_CONNECT_ACTION_SERVICE = "connect";
export const TYPE_QUESTION_CALLME = "feedback";
export const TYPE_CABLE_RENT = "cable-rent";
