import { getCurrentLang } from "../utils";

export const PRODUCT_SERVICE = {
    type: "service",
    url: `/${getCurrentLang()}/order-service`,
};

export const PRODUCT_PRODUCT = {
    type: "product",
    url: `/${getCurrentLang()}/order-product`,
};

export const PRODUCT_PRODUCT_BY_ADDRESS = {
    type: "address",
    url: `/${getCurrentLang()}/map-connect`,
};

export const PRODUCT_MOBILE_CALLME = {
    type: "callme",
    url: `/${getCurrentLang()}/call-me`,
};

export const PRODUCT_CONNECT_ACTION_SERVICE = {
    type: "connect",
    url: `/${getCurrentLang()}/connect-action`,
};

export const QUESTION_CALLME = {
    type: "feedback",
    url: `/${getCurrentLang()}/feedback`,
};

export const CABLE_RENT = {
    type: "cable-rent",
    url: `/${getCurrentLang()}/cable-rent`,
};

export const COMMENT_BLOG = {
    type: "comment",
    url: `/${getCurrentLang()}/blog/comment`,
};
