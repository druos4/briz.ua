/* eslint-disable no-unused-vars */
/* eslint-disable eqeqeq */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-undef */
/* eslint-disable no-return-assign */

// Transforms key/value pairs to FormData() object
export function toFormData(values = {}, method = "POST") {
    const formData = new FormData();
    for (const field of Object.keys(values)) {
        formData.append(field, values[field]);
    }

    // NOTE: When working with Laravel PUT/PATCH requests and FormData
    // you SHOULD send POST request and fake the PUT request like this.
    // More info: http://stackoverflow.com/q/50691938
    if (method.toUpperCase() === "PUT") {
        formData.append("_method", "PUT");
    }

    return formData;
}

export const getWorkTime = () => {
    const nowOnServer = new Date().toLocaleString("en-US", {
        timeZone: "Europe/Kiev",
    });
    const hour = new Date(nowOnServer).getHours();
    if (hour >= 9 && hour <= 21) {
        return true;
    }
    return false;
};

export const defineModalType = (error, typeRequest, requestStatusMessage) => {
    const workTime = getWorkTime();
    if (typeRequest === "writeComment") {
        if (!!requestStatusMessage.errors) {
            return "toManyComments";
        } else {
            return "reqSentComment";
        }
    }

    if (
        (typeRequest === "callMeHeader" || typeRequest === "callMe") &&
        !workTime
    ) {
        return "callMeHeader";
    }
    if (error && error["seconds-left"]) {
        return "reqAccept";
    }
    return "reqSent";
};

export const keepTypeRequest = (typeRequest) => {
    // return a(typeRequest);
    return (typeModal = typeRequest);
};

// export const typeError = (response) => {
//     const currentLang = localStorage.getItem("i18nextLng");

//     if (response.data.hasOwnProperty("errors")) {
//         if (!response.data.errors.hasOwnProperty("seconds-left")) {
//             // getStatus(prevState => ({
//             //   ...prevState,
//             //   errorPhone: res.data.errors.message.filter(error => error.indexOf('Неверный формат') !== -1 ? error : '')
//             // }));
//             return response.data.errors.message.length;
//         } else {
//             return {
//                 showModal: true,
//                 modalContent: {
//                     title:
//                         (currentLang === "ru" && "Вы уже оставили заявку") ||
//                         (currentLang === "ua" && "Ви вже залишили заявку") ||
//                         (currentLang === "en" &&
//                             "You have already left a request"),
//                     content:
//                         (currentLang === "ru" &&
//                             "Данные обрабатываются и в ближайшее время с Вами свяжется наш оператор. Пожалуйста ожидайте.") ||
//                         (currentLang === "ua" &&
//                             "Дані обробляються і найближчим часом з Вами зв'яжеться наш оператор. Будь ласка чекайте.") ||
//                         (currentLang === "en" &&
//                             "The data is processed and our operator will contact you shortly. Please wait."),
//                     buttonContent:
//                         (currentLang === "ru" && "Закрыть") ||
//                         (currentLang === "ua" && "Закрити") ||
//                         (currentLang === "en" && "Close"),
//                 },
//                 callmeResponse: {
//                     title:
//                         (currentLang === "ru" && "Запрос отправлен") ||
//                         (currentLang === "ua" && "Запит відправлено") ||
//                         (currentLang === "en" && "Request has been sent"),
//                     content:
//                         (currentLang === "ru" &&
//                             "Возможность запросить повторный звонок будет доступна через 5 минут") ||
//                         (currentLang === "ua" &&
//                             "Можливість запросити повторний дзвінок буде доступна через 5 хвилин") ||
//                         (currentLang === "en" &&
//                             "The ability to request a second call will be available through 5 minutes"),
//                     buttonContent:
//                         (currentLang === "ru" && "Закрыть") ||
//                         (currentLang === "ua" && "Закрити") ||
//                         (currentLang === "en" && "Close"),
//                 },
//                 errorPhone: response.data.errors,
//             };
//             // showModal(true);
//             // setModalContent()
//             // getStatus(prevState => ({
//             //   ...prevState,
//             //   errorPhone: res.data.errors
//             // }))
//         }
//     }

//     if (response.data.hasOwnProperty("success") && response.data.success) {
//         // getFieldsValue({ phone: '', username: '' });
//         ReactGA.ga("send", "event", localStorage.getItem("seoId"), "send");

//         if (typeModal === "callme" && getWorkTime()) {
//             return {
//                 showModal: true,
//                 modalContent: {
//                     title:
//                         (currentLang === "ru" && "Спасибо!") ||
//                         (currentLang === "ua" && "Спасибі!") ||
//                         (currentLang === "en" && "Thank you!"),
//                     content:
//                         (currentLang === "ru" &&
//                             "Заявка принята. Наш оператор свяжется с вами.") ||
//                         (currentLang === "ua" &&
//                             "Заявку прийнято. Наш оператор зв'яжеться з вами.") ||
//                         (currentLang === "en" &&
//                             "Our operator will contact you."),
//                     aditionalDescription:
//                         (currentLang === "ru" &&
//                             'В соответствии с требованиями действующего законодательства Украины услуга "Перезвоните мне" работает с') ||
//                         (currentLang === "ua" &&
//                             'Відповідно до вимог чинного законодавства України послуга "Передзвоніть мені" працює з') ||
//                         (currentLang === "en" &&
//                             'In accordance with the requirements of the current legislation of Ukraine, the "Call me back" service works from'),
//                     buttonContent:
//                         (currentLang === "ru" && "Закрыть") ||
//                         (currentLang === "ua" && "Закрити") ||
//                         (currentLang === "en" && "Close"),
//                 },
//                 errorPhone: "",
//             };
//         } else {
//             return {
//                 showModal: true,
//                 modalContent: {
//                     title:
//                         (currentLang === "ru" && "Спасибо!") ||
//                         (currentLang === "ua" && "Спасибі!") ||
//                         (currentLang === "en" && "Thank you!"),
//                     content:
//                         (currentLang === "ru" &&
//                             "Заявка принята. В ближайшее время с Вами свяжется наш оператор.") ||
//                         (currentLang === "ua" &&
//                             "Заявку прийнято. Найближчим часом з Вами зв'яжеться наш оператор.") ||
//                         (currentLang === "en" &&
//                             "The application has been accepted. Our operator will contact you shortly."),
//                     buttonContent:
//                         (currentLang === "ru" && "Закрыть") ||
//                         (currentLang === "ua" && "Закрити") ||
//                         (currentLang === "en" && "Close"),
//                 },
//                 errorPhone: "",
//             };
//         }
//     }
// };

export const getSlugFromUrl = (url) => {
    const arrayUrl = url ? url.split("/") : ["all"];
    const slug = arrayUrl[arrayUrl.length - 1];

    return slug;
};

export const hexToHSLHoverBtn = (H) => {
    // Конвертировать hex формат в RGB
    let r = 0;
    let g = 0;
    let b = 0;
    if (H.length == 4) {
        r = `0x${H[1]}${H[1]}`;
        g = `0x${H[2]}${H[2]}`;
        b = `0x${H[3]}${H[3]}`;
    } else if (H.length == 7) {
        r = `0x${H[1]}${H[2]}`;
        g = `0x${H[3]}${H[4]}`;
        b = `0x${H[5]}${H[6]}`;
    }
    // Потом конвертировать RGB в HSL
    r /= 255;
    g /= 255;
    b /= 255;
    const cmin = Math.min(r, g, b);
    const cmax = Math.max(r, g, b);
    const delta = cmax - cmin;
    let h = 0;
    let s = 0;
    let l = 0;

    if (delta == 0) h = 0;
    else if (cmax == r) h = ((g - b) / delta) % 6;
    else if (cmax == g) h = (b - r) / delta + 2;
    else h = (r - g) / delta + 4;

    h = Math.round(h * 60);

    if (h < 0) h += 360;

    l = (cmax + cmin) / 2;
    s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);

    return `hsl(${h},${s}%,${l - 5}%)`;
};

export const linksForApps = (language) => {
    let setLinks = {};
    switch (language) {
        case "ua":
            setLinks = {
                apple: "https://apps.apple.com/ru/app/%D0%BC%D0%BE%D0%B9-briz/id1506187441",
                google: "https://play.google.com/store/apps/details?id=ua.briz.briz&hl=uk",
            };
            break;
        case "en":
            setLinks = {
                apple: "https://apps.apple.com/us/app/%D0%BC%D0%BE%D0%B9-briz/id1506187441",
                google: "https://play.google.com/store/apps/details?id=ua.briz.briz&hl=en",
            };
            break;
        case "ru":
            setLinks = {
                apple: "https://apps.apple.com/ru/app/%D0%BC%D0%BE%D0%B9-briz/id1506187441",
                google: "https://play.google.com/store/apps/details?id=ua.briz.briz&hl=ru",
            };
            break;
        default:
            setLinks = {
                apple: "https://apps.apple.com/ru/app/%D0%BC%D0%BE%D0%B9-briz/id1506187441",
                google: "https://play.google.com/store/apps/details?id=ua.briz.briz&hl=uk",
            };
    }
    return setLinks;
};

export const currentLang = () => {
    if (typeof window !== "undefined") {
        return app.getLang() === "ua" ? "" : `/${app.getLang()}`;
    }
};

export const getCurrentLang = () => {
    if (typeof window !== "undefined") {
        return document.querySelector("html").lang;

    }
};

export const getLocaleHref = (locale) => {
    return (locale === "ua") ? "" : (`/${locale}` || "");
};

export const getLocale = (locale) => {
    return locale || 'ua'
}


export const userAgent = () => {
    if (typeof window !== "undefined") {
        return window.navigator.userAgent.toLowerCase();
    }
};

export const isIos = () => {
    return /iphone|ipod|ipad/.test(userAgent());
};

export const isSafari = () => {
    return /^((?!chrome|android).)*safari/i.test(userAgent());
};

export const isMozilla = () => {
    return /firefox/.test(userAgent());
};

export const isStandalone = () => typeof window !== "undefined" && window.navigator.standalone;

export const getCookie = (name) => {
    const matches = document.cookie.match(
        new RegExp(
            `(?:^|; )${name.replace(
                /([\.$?*|{}\(\)\[\]\\\/\+^])/g,
                "\\$1"
            )}=([^;]*)`
        )
    );
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

export const categoryTitleCompare = (a, b) => {
    if (a.title.length > b.title.length) {
        return -1;
    }
    if (a.title.length < b.title.length) {
        return 1;
    }
    return 0;
};
