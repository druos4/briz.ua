/* eslint-disable no-console */
import axios from "axios";
import fetchApi from "../api-service";
import {
    GET_MENU,
    LOADED_MENU,
    API_CANDIDACY_BUY_PRODUCT,
    SET_STICKY_HEADER,
    SET_CURRENT_PAGE,
    SET_PART_LINK_FAQ,
    SET_MODAL_ACCEPT,
    CLEAR_MODAL_ACCEPT,
    CLEAR_API_CANDIDACY_BUY_PRODUCT,
    SET_SHOP_PRODUCT,
    SET_LIKE_BLOG_ARTICLE,
} from "./types";

export const getMenu = (token, lang) => async (dispatch) => {
    try {
        await axios
            .get(`/${lang}/get-header-menu`)
            .then((response) => {
                dispatch({
                    type: GET_MENU,
                    payload: response.data.menu,
                });
            });
    } catch (err) {
        console.log(err.message);
    }
};

export const menuIsLoading = (flag) => {
    return {
        type: LOADED_MENU,
        payload: flag,
    };
};

export const submitModalForm = ({ body, url, keyFormRedux, typeProduct }) => {
    return (dispatch) =>
        dispatch(
            fetchApi(
                url,
                "POST",
                API_CANDIDACY_BUY_PRODUCT,
                body,
                keyFormRedux,
                typeProduct
            )
        );
};

export const setModalAccept = (value) => (dispatch) => {
    dispatch({
        type: SET_MODAL_ACCEPT,
        payload: value,
    });
};
export const clearModalAccept = () => (dispatch) => {
    dispatch({
        type: CLEAR_MODAL_ACCEPT,
        payload: "",
    });
};

export const clearApiCandidacy = () => (dispatch) => {
    dispatch({
        type: CLEAR_API_CANDIDACY_BUY_PRODUCT,
        payload: "",
    });
};

export const keepTrackStickyHeader = (flag) => (dispatch) => {
    dispatch({
        type: SET_STICKY_HEADER,
        payload: flag,
    });
};

export const keepTrackCurrentPage = (page) => (dispatch) => {
    dispatch({
        type: SET_CURRENT_PAGE,
        payload: page,
    });
};

export const savePartLink = (partLink) => (dispatch) => {
    dispatch({
        type: SET_PART_LINK_FAQ,
        payload: partLink,
    });
};

export const setShopProduct = (product) => (dispatch) => {
    dispatch({
        type: SET_SHOP_PRODUCT,
        payload: product
    });
};

export const setLikeBlogArticle = (like) => (dispatch) => {
    dispatch({
        type: SET_LIKE_BLOG_ARTICLE,
        payload: like
    });
};