export const GET_MENU = "GET_MENU";
export const LOADED_MENU = "LOADED_MENU";
export const SET_FIELDS_VALUE = "SET_FIELDS_VALUE";

// Значения полей перед отправкой заявки на покупку
export const SET_CUSTOMER_PHONE = "SET_CUSTOMER_PHONE";
export const SET_CUSTOMER_USERNAME = "SET_CUSTOMER_USERNAME";

// Отправка заявки на покупку
export const API_CANDIDACY_BUY_PRODUCT = "API_CANDIDACY_BUY_PRODUCT";
export const PENDING_API_CANDIDACY_BUY_PRODUCT =
    "PENDING_API_CANDIDACY_BUY_PRODUCT";
export const SUCCESS_API_CANDIDACY_BUY_PRODUCT =
    "SUCCESS_API_CANDIDACY_BUY_PRODUCT";
export const FAILURE_API_CANDIDACY_BUY_PRODUCT =
    "FAILURE_API_CANDIDACY_BUY_PRODUCT";
export const CLEAR_API_CANDIDACY_BUY_PRODUCT =
    "CLEAR_API_CANDIDACY_BUY_PRODUCT";

// Модальное окно
export const SET_MODAL = "SET_MODAL";
export const CLEAR_MODAL_CONTENT = "CLEAR_MODAL_CONTENT";
export const SET_MODAL_ACCEPT = "SET_MODAL_ACCEPT";
export const CLEAR_MODAL_ACCEPT = "CLEAR_MODAL_ACCEPT";

// Состояние sticky header
export const SET_STICKY_HEADER = "SET_STICKY_HEADER";

// Сохранение текущей страницы
export const SET_CURRENT_PAGE = "SET_CURRENT_PAGE";

// Сохранение части ссылки для страницы FaqItem
export const SET_PART_LINK_FAQ = "SET_PART_LINK_FAQ";

// Магазин
export const SET_TAB_AND_FILTERS_SHOP = "SET_TAB_AND_FILTERS_SHOP";
export const SET_TAB_SHOP = "SET_TAB_SHOP";
export const SET_TAB_SHOP_FILTER_URL = "SET_TAB_SHOP_FILTER_URL";
export const SET_TAB_AND_SORT_SHOP = "SET_TAB_AND_SORT_SHOP";
// Товар
export const SET_SHOP_PRODUCT = "SET_SHOP_PRODUCT";

// Лайки
export const SET_LIKE_BLOG_ARTICLE = "SET_LIKE_BLOG_ARTICLE";
