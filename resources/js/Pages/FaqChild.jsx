import React, { useRef } from "react";
import PropTypes from "prop-types";
import { InertiaHead } from "@inertiajs/inertia-react";
import { useHandleTables } from "@/hooks/useHandleTables";

const FaqChild = (props) => {
    const wrapperElement = useRef(null);
    useHandleTables(wrapperElement);

    return (
        <>
            <InertiaHead>
                <meta name="keywords" content={props.content?.meta_keywords} />
                <meta name="og:title" content={props.content?.meta_title} />
                <meta property="og:url" content={props.content?.url} />
            </InertiaHead>
            <div className="faq-child">
                <h1 className="faq-child__answer-title">{props.content?.title}</h1>
                <div
                    dangerouslySetInnerHTML={{ __html: props.content?.detail }}
                    className="faq-child__answer-content"
                    ref={wrapperElement}
                />
            </div>
        </>
    )
};

FaqChild.propTypes = {
    content: PropTypes.shape({
        id: PropTypes.number,
        detail: PropTypes.string,
        lastmod: PropTypes.string,
        slug: PropTypes.string,
        meta_description: PropTypes.string,
        meta_keywords: PropTypes.string,
        meta_title: PropTypes.string,
        title: PropTypes.string,
        url: PropTypes.string,
    }),
};

FaqChild.defaultProps = {
    content: {}
};

export default FaqChild;
