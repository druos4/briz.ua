import React, { useState, useEffect, useRef } from "react";
import Layout from '../Shared/Layout';
import { InertiaLink } from "@inertiajs/inertia-react";
import { Inertia } from "@inertiajs/inertia";
import { connect } from "react-redux";
import { keepTrackCurrentPage } from "@/actions/actions";
import { useTranslation } from "react-i18next";
import imgEmail from '../../images/blog/img-email.png';
import BlogSocialsPanel from '../Shared/BlogParts/BlogSocialsPanel';
import BlogArticlesList from '../Shared/BlogParts/BlogArticlesList';
import BlogPagination from '../Shared/BlogParts/BlogPagination';
import BlogSubscribeEmail from '../Shared/BlogParts/BlogSubscribeEmail';
import BlogMobileDropdown from '../Shared/BlogParts/BlogMobileDropdown';
import ToUp from "@/Shared/ToUp";
import { getLocale, getLocaleHref } from "@/utils";

const BlogItems = (props) => {
    const { t } = useTranslation();

    const { blogs, categories, meta, pagination, sort, keepTrackCurrentPage, locale } = props;
    const valueTitle = categories.filter(item => item.selected)[0].title;
    const [sortedBlogsFirst, setSortedBlogsFirst] = useState([]);
    const [sortedBlogsSecond, setSortedBlogsSecond] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState(
        categories.find((item) => item.selected)
    );
    const categoryRef = useRef(null)
    // const [valueTitle, setValueTitle] = useState(categories.filter(item => item.selected)[0].title);
    const currentLocaleHref = getLocaleHref(locale)
    const currentLocale = getLocale(locale)

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        if (blogs && blogs.length) {
            if (blogs.length >= 6) {
                setSortedBlogsFirst(blogs.slice(0, 6));
                setSortedBlogsSecond(blogs.slice(6));
            } else {
                setSortedBlogsFirst(blogs.slice(0));
            }
        }

        categoryRef.current.addEventListener("wheel", (event) => {
            event.preventDefault();
            categoryRef.current.scrollTo({
                left: (categoryRef.current.scrollLeft + (event.deltaY / 2))
            });
        });
    }, []);

    const callbackSelectedCategory = (selected) => {
        setSelectedCategory(selected);
    };

    return (
        <>
            <div className="wrapper blog">
                <ToUp scrollStepInPx="50" delayInMs="5"/>
                <h1 className="page-title blog__page-title">{valueTitle}</h1>
                <span className="channels__dropdown shop__dropdown">
                        <BlogMobileDropdown
                            defaultText={selectedCategory}
                            optionsList={categories}
                            getSelectedChannel={callbackSelectedCategory}
                        />
                    </span>
                <div className="blog__categories">
                    <ul className="blog__list-categories" ref={categoryRef}>
                        {
                            categories && Boolean(categories.length) && categories.map((category, index) => {
                                return (
                                    <li
                                        className="blog__item-category"
                                        key={index}
                                    >
                                        <a
                                            href={category.url}
                                            className={category.selected ? "blog__link-category blog__link-category--selected" : "blog__link-category"}
                                        >
                                            {category.title}
                                        </a>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>

                <div className="blog__sort">
                    <ul className="blog__list-sort">
                        {
                            sort && Boolean(sort.length) && sort.map((sortItem, index) => {
                                return (
                                    <li className="blog__item-sort" key={index}>
                                        <a
                                            href={sortItem.url}
                                            className={sortItem.selected ? "blog__link-sort blog__link-sort--selected" : "blog__link-sort"}
                                        >
                                            {sortItem.title}
                                        </a>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>

                <div className="blog__articles">
                    {Boolean(sortedBlogsFirst.length) && <BlogArticlesList data={sortedBlogsFirst} />}
                </div>
            </div>
            <div className="blog__socials">
                <div className="wrapper blog__socials-wrapper">
                    <BlogSocialsPanel page={"BlogItems"} currentLocale={currentLocale}/>
                </div>
            </div>

            <div className="wrapper blog__articles-bottom">
                {
                    Boolean(sortedBlogsSecond.length) && <BlogArticlesList data={sortedBlogsSecond} />
                }
                
                {
                    Boolean(pagination) && Boolean(Object.keys(pagination).length) && <div className="blog__pagination"><BlogPagination pagination={pagination} /></div>
                }
            </div>

            <div className="blog__email-subscribe subscribe-email">
                <div className="wrapper blog__email-subscribe-wrapper">
                    <BlogSubscribeEmail
                        imgSource={imgEmail}
                        title={valueTitle}
                        currentLocaleHref={currentLocaleHref}
                    />
                </div>
            </div>
        </>
    )
};

BlogItems.layout = (page) => <Layout children={page} title={'Blog'} />

export default connect(null, { keepTrackCurrentPage })(BlogItems);