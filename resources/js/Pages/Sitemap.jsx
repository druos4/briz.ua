import React from 'react';
import Layout from '../Shared/Layout';
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { InertiaLink } from "@inertiajs/inertia-react";

const Sitemap = ({ sitemap }) => {
    const { t } = useTranslation();
    let depth;
    const childrenArr = ({ title, url, children = [] }, i) => {
        if (!children.length) {
            depth = 0;
        } else {
            ++depth;
        }
        
        return (
            <div className={`sitemap__group`} key={i}>
                <a href={url} className={`sitemap__link`}>{title}</a>
                {!!children.length && <div className={`link-children--${depth}`}>{children.map(childrenArr)}</div>}
            </div>
        );
    }

    return (
        <div className="wrapper sitemap">
            <div className="sitemap__content">
                <h1 className="page-title sitemap__page-title">{t('sitemap')}</h1>
                <section className="sitemap__recursive-list">
                {
                    sitemap.map(childrenArr)
                }
                </section>
            </div>
        </div>
    )
};

Sitemap.layout = (page) => <Layout title="Usloviya">{page}</Layout>;

Sitemap.propTypes = {
    sitemap: PropTypes.arrayOf(PropTypes.object),
};

Sitemap.defaultProps = {
    sitemap: []
};

export default Sitemap;