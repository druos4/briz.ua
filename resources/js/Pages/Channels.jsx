/* eslint-disable no-shadow */
import React, { useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Inertia } from "@inertiajs/inertia";
import Layout from "../Shared/Layout";
import { keepTrackCurrentPage } from "../actions/actions";
import ChannelsMobile from "../Shared/ChannelsOrientation/ChannelsMobile";
import ChannelsDesktop from "../Shared/ChannelsOrientation/ChannelsDesktop";
import { useMediaQuery } from "../Shared/MediaQuery/useMediaQuery";

const Channels = ({ tariff, channels, count, meta, keepTrackCurrentPage }) => {
    const isWidthDesktop = useMediaQuery("(min-width: 1280px)");

    const titles = Object.keys(channels)
        .map((channel, index) => {
            const itemChannel = channels[channel];

            return {
                genre_title: itemChannel.genre_title,
                id: index,
                channels: itemChannel.channels,
                url: itemChannel.url,
            };
        })
        .filter((item) => (item?.genre_title ? item : null));

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        const title = document.querySelector("title");
        if (title && meta && !!Object.keys(meta).length) {
            title.textContent = meta["meta-title"];
        }
    }, []);

    return (
        <>
            <div className="channels wrapper">
                <div className="channels__container">
                    {isWidthDesktop ? (
                        <ChannelsDesktop
                            channels={titles}
                            tariff={tariff}
                            count={count}
                        />
                    ) : (
                        <ChannelsMobile
                            channels={titles}
                            tariff={tariff}
                            count={count}
                        />
                    )}
                </div>
            </div>
        </>
    );
};

Channels.layout = (page) => <Layout title="Channels">{page}</Layout>;

Channels.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    tariff: PropTypes.shape({
        id: PropTypes.number,
        title: PropTypes.string,
        price: PropTypes.number,
    }),
    channels: PropTypes.arrayOf(PropTypes.object),
    count: PropTypes.number,
    meta: PropTypes.shape({
        "meta-title": PropTypes.string,
    }),
};
Channels.defaultProps = {
    keepTrackCurrentPage: () => {},
    tariff: {},
    channels: [],
    count: 0,
    meta: {},
};

export default connect(null, { keepTrackCurrentPage })(Channels);
