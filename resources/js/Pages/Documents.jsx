/* eslint-disable no-shadow */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/forbid-prop-types */
import React, {useEffect} from "react";
import PropTypes from "prop-types";
import {useTranslation} from "react-i18next";
import {connect} from "react-redux";
import {Inertia} from "@inertiajs/inertia";
import {keepTrackCurrentPage, setModalAccept} from "@/actions/actions";
import Layout from "../Shared/Layout";
import {useIsSsr} from "@/hooks/useIsSsr";
import {isIos} from "@/utils";
import {InertiaHead} from "@inertiajs/inertia-react";

const Documents = (props) => {
    const {
        meta,
        keepTrackCurrentPage,
        setModalAccept,
        prices,
        documents,
        docactions,
        sertificats,
        qualities,
    } = props;

    const {t} = useTranslation();
    const isSsr = useIsSsr();
    const ios = !isSsr && isIos();

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        if (meta && !!Object.keys(meta).length) {
            const title = document.querySelector("title");
            title.textContent = meta["meta-title"];
        }
    }, []);

    const handleAppareModalPopup = (link) => {
        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalImg",
            currentImgLink: link,
        });
    };

    const keyHandleAppareModalPopup = (e, link) => {
        if (e.keyCode === 13) {
            setModalAccept({
                showModalAccept: true,
                typeModalAccept: "modalImg",
                currentImgLink: link,
            });
        }
    };

    return (
        <>
            {/*<InertiaHead>*/}
            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta-description") && (*/}
            {/*        <meta*/}
            {/*            name="description"*/}
            {/*            content={meta["meta-description"]}*/}
            {/*        />*/}
            {/*    )}*/}

            {/*    {meta.hasOwnProperty("meta-keywords") && (*/}
            {/*        <meta*/}
            {/*            name="keywords"*/}
            {/*            content={meta["meta-keywords"]}*/}
            {/*        />*/}
            {/*    )}*/}
            {/*    {meta.hasOwnProperty("meta-title") && (*/}
            {/*        <meta name="og:title" content={meta["meta-title"]}/>*/}
            {/*    )}*/}
            {/*    <meta property="og:url" content={!isSsr ? location.pathname : ""}/>*/}
            {/*</InertiaHead>*/}

            <div className="documents wrapper">
                <h1
                    className={
                        ios
                            ? "page-title documents__page-title documents__page-title--ios"
                            : "page-title documents__page-title"
                    }
                >
                    {t("documentsTitle")}
                </h1>
                <section className="documents__section">
                    <div className="documents__prices block-1">
                        <div className="documents__category-title">
                            {t("prices")}
                        </div>
                        <ul className="documents__list">
                            {prices.map((price, index) => {
                                return (
                                    <li className="documents__item" key={index}>
                                        <a
                                            href={price.link}
                                            className="documents__item-link main-link"
                                        >
                                            {price.title}{" "}
                                            <span className="documents__item-link-pdf">
                                                (pdf)
                                            </span>
                                        </a>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                    <div className="documents__contracts block-2">
                        <div className="documents__category-title">
                            {t("contracts")}
                        </div>
                        <ul className="documents__list">
                            {documents.map((document, index) => {
                                return (
                                    <li className="documents__item" key={index}>
                                        <a
                                            href={document.link}
                                            className="documents__item-link main-link"
                                        >
                                            {document.title}{" "}
                                            <span className="documents__item-link-pdf">
                                                (pdf)
                                            </span>
                                        </a>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                    <div className="documents__promotions">
                        <div className="documents__category-title">
                            {t("detailConditionsPromo")}
                        </div>
                        <ul className="documents__list-promotions">
                            {docactions.map((docaction, index) => {
                                return (
                                    <li
                                        className="documents__item-promotions"
                                        key={index}
                                    >
                                        <a
                                            href={docaction.link}
                                            className="documents__item-link main-link"
                                        >
                                            {docaction.title}{" "}
                                            <span className="documents__item-link-pdf">
                                                (pdf)
                                            </span>
                                        </a>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                    <div className="documents__awards block-3">
                        <div className="documents__category-title">
                            {t("awards")}
                        </div>
                        <ul className="documents__list-awards">
                            {sertificats.map((award, index) => {
                                return (
                                    <li
                                        className="documents__item-award"
                                        key={index}
                                    >
                                        <img
                                            src={award.link}
                                            className="documents__img-award"
                                            alt={`${award.title} - інтернет-провайдер Briz в Одесі`}
                                            title={award.title}
                                        />
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                    <div className="documents__report-services block-4">
                        <div className="documents__category-title">
                            {t("reportQuality")}
                        </div>
                        <ul className="documents__list-quality">
                            {qualities.map((quality, index) => {
                                return (
                                    <li
                                        className="documents__item-quality"
                                        key={index}
                                    >
                                        <div
                                            role="button"
                                            tabIndex="0"
                                            onClick={() =>
                                                handleAppareModalPopup(
                                                    quality.link
                                                )
                                            }
                                            onKeyDown={(e) => {
                                                keyHandleAppareModalPopup(
                                                    e,
                                                    quality.link
                                                );
                                            }}
                                        >
                                            <img
                                                src={quality.link}
                                                alt={`${quality.title} - інтернет-провайдер Briz в Одесі`}
                                                title={quality.title}
                                                className="documents__img-sertificate"
                                            />
                                        </div>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                    <div className="documents__permissive block-5">
                        <div className="documents__category-title">
                            {t("permittingDocuments")}
                        </div>
                        <div className="documents__permissive-licenses">
                            <div className="documents__permissive-license-item">
                                {/* <div className="documents__permissive-title">{t('licenseNKRSI')}</div> */}
                                {/* <table className="documents__table">
                                <thead>
                                    <tr>
                                        <th align="left" className="documents__table-th">{t('licenseNumber')}</th>
                                        <th align="left" className="documents__table-th documents__table-th--padding-left">{t('expiryDate')}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="documents__table-td">№ 000610</td>
                                        <td className="documents__table-td documents__table-td--padding-left">28.09.2016-26.09.2021</td>
                                    </tr>
                                    <tr>
                                        <td className="documents__table-td">№ 000937</td>
                                        <td className="documents__table-td documents__table-td--padding-left">09.07.2018-08.07.2023</td>
                                    </tr>
                                    <tr>
                                        <td className="documents__table-td">№ 000737</td>
                                        <td className="documents__table-td documents__table-td--padding-left">05.04.2017-04.04.2022</td>
                                    </tr>
                                </tbody>
                            </table> */}
                                <p className="documents__permissive-text">
                                    {t("inRegisterOperators")}
                                </p>
                            </div>
                            <div className="documents__permissive-license-item">
                                <div className="documents__permissive-title">
                                    {t("licenseNationalCouncil")}
                                </div>
                                <table className="documents__table-second">
                                    <thead>
                                    <tr>
                                        <th
                                            align="left"
                                            className="documents__table-th"
                                        >
                                            {t("licenseNumber")}
                                        </th>
                                        <th
                                            align="left"
                                            className="documents__table-th documents__table-th--padding-left"
                                        >
                                            {t("expiryDate")}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td className="documents__table-td documents__table-td--padding-right">
                                            {t("series")} НР № 00755-П
                                        </td>
                                        <td className="documents__table-td documents__table-td--width-second">
                                            22.11.2018-22.11.2028
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="documents__table-td documents__table-td--width">
                                            {t("series")} НР № 00176-П
                                        </td>
                                        <td className="documents__table-td documents__table-td--width-second">
                                            11.03.2013-11.03.2023
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="documents__table-td documents__table-td--width">
                                            {t("series")} НР № 00172-П
                                        </td>
                                        <td className="documents__table-td documents__table-td--width-second">
                                            11.03.2013-11.03.2023
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="documents__table-td documents__table-td--width">
                                            {t("series")} НР № 00166-П
                                        </td>
                                        <td className="documents__table-td documents__table-td--width-second">
                                            08.07.2013-08.07.2023
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="documents__table-td documents__table-td--width">
                                            {t("series")} НР № 00174-П
                                        </td>
                                        <td className="documents__table-td documents__table-td--width-second">
                                            08.07.2013-08.07.2023
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="documents__table-td documents__table-td--padding-right">
                                            {t("series")} НР № 00175-П
                                        </td>
                                        <td className="documents__table-td documents__table-td--width-second">
                                            08.07.2013-08.07.2023
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </>
    );
};

Documents.layout = (page) => <Layout title="Documents">{page}</Layout>;

Documents.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    meta: PropTypes.any,
    setModalAccept: PropTypes.func,
    prices: PropTypes.arrayOf(PropTypes.object),
    documents: PropTypes.arrayOf(PropTypes.object),
    docactions: PropTypes.arrayOf(PropTypes.object),
    sertificats: PropTypes.arrayOf(PropTypes.object),
    qualities: PropTypes.arrayOf(PropTypes.object),
};
Documents.defaultProps = {
    keepTrackCurrentPage: () => {
    },
    meta: {},
    setModalAccept: () => {
    },
    prices: [],
    documents: [],
    docactions: [],
    sertificats: [],
    qualities: [],
};

export default connect(null, {keepTrackCurrentPage, setModalAccept})(
    Documents
);
