import React from "react";
import PropTypes from "prop-types";
import Layout from "../Shared/Layout";
import BannerSlider from "../Shared/BannerSlider";
import { isIos, isSafari } from '../utils';
import { useIsSsr } from '../hooks/useIsSsr';

const SliderTest = ({ sliders }) => {
    const isSsr = useIsSsr();
    const safari = !isSsr && isSafari();
    const ios = !isSsr && isIos();

    return (
        <>
            {sliders?.length ? (
                <div
                    className={
                        ios || safari
                            ? "home__slider home__slider--ios"
                            : "home__slider"
                    }
                    style={
                        ios || safari ? { marginTop: "0" } : { marginTop: "" }
                    }
                >
                    <BannerSlider sliders={sliders} />
                </div>
            ) : null}
        </>
    );
};

SliderTest.layout = (page) => <Layout title="Slider test">{page}</Layout>;

SliderTest.propTypes = {
    sliders: PropTypes.arrayOf(PropTypes.object),
};
SliderTest.defaultProps = {
    sliders: [],
};
export default SliderTest;
