import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import img500Error from "../../images/500/500.svg";
import WithLangProvider from "../Shared/hoc/WithLangProvider";

const Error500 = () => {
    const [lang, setLang] = useState("ua");

    const langWithoutUa = lang === "ua" ? "" : lang;

    const { t, i18n } = useTranslation();
    useEffect(() => {
        const langFromStorage = localStorage.getItem("i18nextLng");
        i18n.changeLanguage(langFromStorage);
        setLang(langFromStorage);
    }, []);

    return (
        <div className="wrapper code-500">
            <div className="code-500__wrapper">
                <div className="code-500__banner">
                    <img
                        className="code-500__banner-img"
                        src={img500Error}
                        alt="Server"
                    />
                </div>
                <h1 className="code-500__title">{t("page500.serverError")}</h1>
                <div className="code-500__subtitle">{t("page500.tryLater")}</div>
                <button className="code-500__button" type="button">
                    <a
                        className="code-500__button-link"
                        href={`/${langWithoutUa}`}
                    >
                        {t("page500.backToMaine")}
                    </a>
                </button>
            </div>
        </div>
    );
};

const ErrPage500 = WithLangProvider(Error500);

export default ErrPage500;
