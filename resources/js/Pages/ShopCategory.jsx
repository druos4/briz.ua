/* eslint-disable no-shadow */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Inertia } from "@inertiajs/inertia";
import { setModalAccept, keepTrackCurrentPage, setShopProduct } from "@/actions/actions";
import Layout from "../Shared/Layout";
import Tabset from "../Shared/Tabset/Tabset";
import ShopCategoryItem from "../Shared/ShopCategoryItem";
import { useMediaQuery } from "@/Shared/MediaQuery/useMediaQuery";
import ShopCategoryItemMobile from "../Shared/ShopCategoryItemMobile";
import {InertiaHead} from '@inertiajs/inertia-react';
import {useIsSsr} from '@/hooks/useIsSsr';

const ShopCategory = (props) => {
    const {
        categories,
        setModalAccept,
        keepTrackCurrentPage,
        meta,
        buttons,
        products,
        setShopProduct,
        isShowModalAccept,
        shopProduct
    } = props;
    const isSsr = useIsSsr();
    // Установка дефолтного таба
    const [currentTab, setTab] = useState(
        categories.find((item) => item.selected).slug
    );
    const [actualTitle, setTitle] = useState("");

    const isWidthDesktop = useMediaQuery("(min-width: 1280px)");

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        const title = document.querySelector("title");
        title.textContent = meta.title;
    }, []);

    const callbackActualTitle = (title) => {
        setTitle(title);
    };

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta-description") &&*/}
            {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

            {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
            {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <div className="shop wrapper">
                <h1 className="page-title shop__page-title">{actualTitle}</h1>
                {isWidthDesktop ? (
                    // <Tabset currentTab={currentTab} setTab={setTab}>
                    <Tabset
                        setTab={() => {}}
                        currentTab={
                            categories.find((item) => item.selected).slug
                        }
                        isHasPicture
                    >
                        {categories.map((category) => {
                            return (
                                <ShopCategoryItem
                                    picture={category.picture}
                                    key={category.slug}
                                    products={products}
                                    buttons={buttons}
                                    url={category.url}
                                    title={category.title}
                                    id={category.slug}
                                    setModalAccept={setModalAccept}
                                    propSlug={category.slug}
                                    callbackActualTitle={callbackActualTitle}
                                    setShopProduct={setShopProduct}
                                    isShowModalAccept={isShowModalAccept}
                                />
                            );
                        })}
                    </Tabset>
                ) : (
                    <ShopCategoryItemMobile
                        callbackActualTitle={callbackActualTitle}
                        categories={categories}
                        setModalAccept={setModalAccept}
                        buttons={buttons}
                        products={products}
                        setShopProduct={setShopProduct}
                        isShowModalAccept={isShowModalAccept}
                        isHasMenuPictures
                    />
                )}
            </div>
        </>
    );
};

ShopCategory.layout = (page) => <Layout title="Shop Category">{page}</Layout>;

ShopCategory.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    setModalAccept: PropTypes.func,
    setShopProduct: PropTypes.func,
    categories: PropTypes.arrayOf(PropTypes.object),
    meta: PropTypes.shape({
        "meta-title": PropTypes.string,
        title: PropTypes.string,
    }),
    buttons: PropTypes.shape({
        filter: PropTypes.arrayOf(PropTypes.object),
        sort: PropTypes.shape({
            title: PropTypes.string,
            url: PropTypes.string,
        }),
    }),
    products: PropTypes.arrayOf(PropTypes.object),
    isShowModalAccept: PropTypes.bool,
};
ShopCategory.defaultProps = {
    keepTrackCurrentPage: () => {},
    setModalAccept: () => {},
    setShopProduct: () => {},
    categories: [],
    meta: {},
    buttons: {},
    products: [],
};

const mapStateToProps = state => {
    return {
        isShowModalAccept: state.modalAccept.showModalAccept
    };
};

export default connect(mapStateToProps, { setModalAccept, keepTrackCurrentPage, setShopProduct })(
    ShopCategory
);
