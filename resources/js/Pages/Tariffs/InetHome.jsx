/* eslint-disable no-shadow */
/* eslint-disable react/no-danger */
/* eslint-disable camelcase */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { InertiaLink } from "@inertiajs/inertia-react";
import { useTranslation } from "react-i18next";
import { Inertia } from "@inertiajs/inertia";
import { advantagesProviderData } from "@/Shared/AdvantagesProvider/advantagesProviderData";
import AdvantagesProvider from "../../Shared/AdvantagesProvider/AdvantagesProvider";
import Layout from "../../Shared/Layout";
import Tabset from "../../Shared/Tabset/Tabset";
import StepsConnectInternet from "../../Shared/StepsConnectInternet/StepsConnectInternet";
import TariffsList from "../../Shared/TariffsList/TariffsList";
import { setModalAccept, keepTrackCurrentPage } from "../../actions/actions";
import { PRODUCT_PRODUCT } from "../../constants/apiParams";
import ActionsButton from "../../Shared/ui/buttons/ActionsButton";
import { linksForApps, isIos, isSafari, getLocale, getLocaleHref } from "../../utils";
import { useIsSsr } from "../../hooks/useIsSsr";
import {InertiaHead} from '@inertiajs/inertia-react';

const InetHome = (props) => {
    const {
        tariffs,
        daily,
        page: { products, category_title, category_description, meta_title, title },
        setModalAccept,
        keepTrackCurrentPage,
        locale
    } = props


    const currentLocaleHref = getLocaleHref(locale)
    const currentLocale = getLocale(locale)
    const isSsr = useIsSsr();
    const { t } = useTranslation();
    const safari = !isSsr && isSafari();
    const ios = !isSsr && isIos();
    // Установка дефолтного таба
    const [currentTab, setTab] = useState("tariffs");
    // Определить целевые тарифы, приходящие в пропсах в один массив
    const [allTariffs, setAllTariffs] = useState([]);
    const [langAppLinks, setLangAppLinks] = useState({ apple: "", google: "" });

    const brizProviderComfortData = advantagesProviderData.brizProviderComfort;

    useEffect(() => {
        !isSsr && setLangAppLinks(linksForApps(currentLocaleHref));
    }, []);

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        setAllTariffs({
            tariffs,
            daily,
        });

        const title = document.querySelector("title");
        title.textContent = meta_title;
    }, []);

    const handleAppareModal = (productTitle, id) => {
        // Открытие модального окна с передачей в него данных

        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle,
            productId: id,
            titleModal: t("purchaseRequest"),
            titleButton: t("sendRequest"),
            apiUrl: PRODUCT_PRODUCT.url,
            typeProduct: PRODUCT_PRODUCT.type,
            titleProduct: "",
            seoId: PRODUCT_PRODUCT.type,
            body: { [PRODUCT_PRODUCT.type]: id, ghost: "" },
            keyFormRedux: "default",
        });
    };

    return (
        <>

            {/*<InertiaHead>*/}

            {/*    {props.page.hasOwnProperty("title") && <title>{title}</title>}*/}

            {/*    {props.page.hasOwnProperty("meta_description") &&*/}
            {/*    <meta name="description" content={props.page["meta_description"]}/>}*/}

            {/*    {props.page.hasOwnProperty("meta_keywords") &&*/}
            {/*    <meta name="keywords" content={props.page["meta_keywords"]}/>}*/}
            {/*    {props.page.hasOwnProperty("meta_title") && <meta name="og:title" content={props.page["meta_title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <div className="tv tariff-page wrapper">
                <h1
                    className={
                        ios || safari
                            ? "page-title tariff-page__page-title tariff-page__page-title--ios"
                            : "page-title tariff-page__page-title"
                    }
                >
                    {t("internet")}
                </h1>
                <div className="page-subtitle-h3 tariff-page__page-subtitle">
                    {t("chooseYourTariff")}
                </div>
                <Tabset currentTab={currentTab} setTab={setTab}>
                    {Object.keys(allTariffs).map((tariff) => {
                        const itemTariff = allTariffs[tariff];
                        const tariffTitle =
                            (tariff === "tariffs" && t("monthlyInterner")) ||
                            (tariff === "daily" && t("dailyInet"));
                        return (
                            <TariffsList
                                id={tariff}
                                tariffs={itemTariff}
                                title={tariffTitle}
                                key={tariff}
                                affiliation="Інтернет"
                            />
                        );
                    })}
                </Tabset>
                {/* RBUC-803 убрать поле о тарифе статический IP */}
                {/* <div
                    className={
                        ios || isSafari
                            ? "tariff-page__static-ip-address tariff-page__static-ip-address--ios"
                            : "tariff-page__static-ip-address"
                    }
                >
                    <span
                        className={
                            ios || isSafari
                                ? "tariff-page__ip-miniature tariff-page__ip-miniature--ios"
                                : "tariff-page__ip-miniature"
                        }
                    />
                    <p className="tariff-page__ip-description-content">
                        <span className="tariff-page__ip-description">
                            {t("staticIPaddress")}
                        </span>
                        <span className="tariff-page__ip-description">
                            * {t("permanentIPaddressMonth")}
                        </span>
                    </p>
                </div> */}

                <section className="section">
                    <StepsConnectInternet
                        title="connectingInternetEasy"
                        typeСonnection="homeInet"
                    />
                </section>
                <AdvantagesProvider
                    advantagesData={brizProviderComfortData}
                    currentLocaleHref={currentLocaleHref}
                    currentLocale={currentLocale}
                />
            </div>
            <section className="section tariff-page__section section__consoles">
                <div className="page-subtitle home__page-subtitle wrapper page-subtitle__padding">
                    {category_title}
                </div>
                <p className="tariff-page__opportunity-txt wrapper consoles__top-description-padding">
                    {category_description}
                </p>
                <ul className="tariff-page__smart-console consoles">
                    {!!products &&
                        !!products.length &&
                        Object.keys(products).map((product, index) => {
                            const productItem = products[product];
                            const splitTitle = productItem.title.split(" ");
                            const lastWord = splitTitle[splitTitle.length - 1];
                            const newTitle = splitTitle
                                .filter((item) => item !== lastWord)
                                .join(" ");

                            return (
                                <li
                                    className="consoles__item consoles__item-padding"
                                    key={productItem.id}
                                >
                                    <div
                                        className={
                                            (index + 1) % 2 === 0
                                                ? "consoles__item-block wrapper consoles__item-padding consoles__item-block--even"
                                                : "consoles__item-block consoles__item-padding wrapper"
                                        }
                                    >
                                        <div className="consoles__item-details">
                                            <div className="consoles__item-title">
                                                {/* <a href={productItem.url} className="consoles__item-link">
                                                    <span>{newTitle}&#32;</span>
                                                    <span className="consoles__item-link-last-word"> {lastWord}</span>
                                                </a> */}
                                                <a
                                                    href={`${currentLocaleHref}/equipment/product/${
                                                        productItem.slug
                                                    }`}
                                                    className="consoles__item-link"
                                                >
                                                    <span>{newTitle}&#32;</span>
                                                    <span className="consoles__item-link-last-word">
                                                        {" "}
                                                        {lastWord}
                                                    </span>
                                                </a>
                                                {/* <span className="consoles__item-icon" /> */}
                                            </div>
                                            <p
                                                className="consoles__item-description"
                                                dangerouslySetInnerHTML={{
                                                    __html: productItem.anons,
                                                }}
                                            />
                                            <div className="consoles__item-order">
                                                <span className="consoles__item-price">
                                                    {productItem.price}{" "}
                                                    {t("UAH")}
                                                </span>
                                                <ActionsButton
                                                    handler={() =>
                                                        handleAppareModal(
                                                            productItem.title,
                                                            productItem.id
                                                        )
                                                    }
                                                    titleButton={t("toOrder")}
                                                />
                                            </div>
                                        </div>
                                        {/* <span className="consoles__item-picture consoles__item-picture--mecool" /> */}
                                        <img
                                            src={productItem.picture}
                                            alt={`${productItem.title} - інтернет-провайдер Briz в Одесі`}
                                            title={productItem.title}
                                            className="consoles__img"
                                        />
                                    </div>
                                </li>
                            );
                        })}
                </ul>
            </section>
        </>
    );
};

InetHome.layout = (page) => <Layout title="Inet Home">{page}</Layout>;

InetHome.propTypes = {
    setModalAccept: PropTypes.func,
    keepTrackCurrentPage: PropTypes.func,
    page: PropTypes.shape({
        meta_title: PropTypes.string,
        products: PropTypes.arrayOf(PropTypes.object),
        category_title: PropTypes.string,
        category_description: PropTypes.string,
    }),
    tariffs: PropTypes.arrayOf(PropTypes.object),
    daily: PropTypes.arrayOf(PropTypes.object),
    locale: PropTypes.string
};
InetHome.defaultProps = {
    setModalAccept: () => {},
    keepTrackCurrentPage: () => {},
    page: {},
    tariffs: [],
    daily: [],
    locale: "ua"
};

export default connect(null, { setModalAccept, keepTrackCurrentPage })(
    InetHome
);
