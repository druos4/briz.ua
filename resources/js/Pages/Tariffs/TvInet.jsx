/* eslint-disable no-shadow */
/* eslint-disable camelcase */
/* eslint-disable react/no-danger */
import React, {useState, useEffect} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {useTranslation} from "react-i18next";
import {Inertia} from "@inertiajs/inertia";
import {InertiaLink} from "@inertiajs/inertia-react";
import Layout from "../../Shared/Layout";
import Tabset from "../../Shared/Tabset/Tabset";
import TariffsList from "../../Shared/TariffsList/TariffsList";
import StepsConnectInternet from "../../Shared/StepsConnectInternet/StepsConnectInternet";
import {setModalAccept, keepTrackCurrentPage} from "../../actions/actions";
import {PRODUCT_PRODUCT} from "../../constants/apiParams";
import ActionsButton from "../../Shared/ui/buttons/ActionsButton";
import { getLocaleHref, isIos } from "../../utils";
import {useIsSsr} from "../../hooks/useIsSsr";
import {InertiaHead} from '@inertiajs/inertia-react';

const TvInet = (props) => {

    const {
        iptv,
        tv,
        tvmax,
        briztv,
        page: {
            products,
            category_title,
            category_description,
            union_description,
            meta_title,
            meta_keywords,
            meta_description,
            title
        },
        setModalAccept,
        keepTrackCurrentPage,
        locale
    } = props

    const isSsr = useIsSsr();
    const {t} = useTranslation();
    const currentLocaleHref = getLocaleHref(locale)

    const ios = !isSsr && isIos();

    // Установка дефолтного таба
    const [currentTab, setTab] = useState("tvmax");
    // Определить целевые тарифы, приходящие в пропсах в один массив
    const [allTariffs, setAllTariffs] = useState([]);
    // Текущий массив с тарифами
    const [lowestPriceTariff, setLowestPriceTariff] = useState(0);

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        setAllTariffs({
            tvmax,
            iptv,
            tv,
            briztv
        });

        setLowestPriceTariff(
            !!tvmax && !!tvmax.length
                ? tvmax.reduce((prev, cur) =>
                    prev.price < cur.price ? prev : cur
                )
                : null
        );

        const title = document.querySelector("title");
        title.textContent = meta_title;
    }, []);

    useEffect(() => {
        if (!!allTariffs[currentTab] && !!allTariffs[currentTab].length) {
            setLowestPriceTariff(
                allTariffs[currentTab].reduce((prev, cur) =>
                    prev.price < cur.price ? prev : cur
                )
            );
        }
    }, [currentTab]);

    const handleAppareModal = (productTitle, id) => {
        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle,
            productId: id,
            titleModal: t("purchaseRequest"),
            titleButton: t("sendRequest"),
            apiUrl: PRODUCT_PRODUCT.url,
            typeProduct: PRODUCT_PRODUCT.type,
            titleProduct: "",
            seoId: PRODUCT_PRODUCT.type,
            body: {[PRODUCT_PRODUCT.type]: id, ghost: ""},
            keyFormRedux: "default",
        });
    };

    return (
        <>

            {/*<InertiaHead>*/}

            {/*    {props.page.hasOwnProperty("title") && <title>{title}</title>}*/}

            {/*    {props.page.hasOwnProperty("meta_description") &&*/}
            {/*    <meta name="description" content={props.page["meta_description"]}/>}*/}

            {/*    {props.page.hasOwnProperty("meta_keywords") &&*/}
            {/*    <meta name="keywords" content={props.page["meta_keywords"]}/>}*/}
            {/*    {props.page.hasOwnProperty("meta_title") && <meta name="og:title" content={props.page["meta_title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <div className="tv-inet tariff-page wrapper">
                <h1
                    className={
                        ios
                            ? "page-title tariff-page__page-title tariff-page__page-title--ios"
                            : "page-title tariff-page__page-title"
                    }
                >
                    {t("internetPlusTv")}
                </h1>
                <div className="page-subtitle-h3 tariff-page__page-subtitle">
                    {t("from")} {lowestPriceTariff.price} {t("hryvnia")}
                </div>
                <Tabset currentTab={currentTab} setTab={setTab}>
                    {Object.keys(allTariffs).map((tariff) => {
                        const isBriztv = !!allTariffs.briztv?.length
                        const itemTariff = allTariffs[tariff];
                        const tariffTitle =
                            (tariff === "iptv" && t("internetIPTV")) ||
                            (tariff === "tvmax" && t("internetTVmax")) ||
                            (tariff === "tv" && t("internetPlusTv")) ||
                            (isBriztv && tariff === "briztv" && t("internetPlusBrizTv"));

                        return (
                            <TariffsList
                                id={tariff}
                                tariffs={itemTariff}
                                title={tariffTitle}
                                key={tariff}
                                affiliation="Тариф iнтернет"
                            />
                        );
                    })}
                </Tabset>
                {/* RBUC-803 убрать поле о тарифе статический IP */}
                {/* <div className="tariff-page__static-ip-address">
                    <span className="tariff-page__ip-miniature" />
                    <p className="tariff-page__ip-description-content">
                        <span className="tariff-page__ip-description">
                            {t("staticIPaddress")}
                        </span>
                        <span className="tariff-page__ip-description">
                            {t("permanentIPaddressMonth")}{" "}
                            {t("youReconnectIPchange")}
                        </span>
                    </p>
                </div> */}
                <section className="section">
                    <StepsConnectInternet
                        title={"howConnectInternetTV"}
                        typeСonnection={"tvInet"}
                    />
                </section>
                {union_description ? (
                    <section className="section">
                        <div className="page-subtitle home__page-subtitle">
                            {t("combinedTariffsProfitable")}
                        </div>
                        <p
                            className="tariff-page__union"
                            dangerouslySetInnerHTML={{
                                __html: union_description,
                            }}
                        />

                        <span className="tariff-page__union-bg"/>
                    </section>
                ) : null}
            </div>
            <section className="section section__consoles">
                <div className="page-subtitle home__page-subtitle wrapper page-subtitle__padding">
                    {category_title}
                </div>
                <p className="tariff-page__opportunity-txt wrapper consoles__top-description-padding">
                    {category_description}
                </p>
                <ul className="tariff-page__smart-console consoles">
                    {!!products &&
                    !!products.length &&
                    Object.keys(products).map((product, index) => {
                        const productItem = products[product];
                        const splitTitle = productItem.title.split(" ");
                        const lastWord = splitTitle[splitTitle.length - 1];
                        const newTitle = splitTitle
                            .filter((item) => item !== lastWord)
                            .join(" ");

                        return (
                            <li
                                className="consoles__item"
                                key={productItem.id}
                            >
                                <div
                                    className={
                                        (index + 1) % 2 === 0
                                            ? "consoles__item-block  consoles__item-padding wrapper consoles__item-block--even"
                                            : "consoles__item-block wrapper  consoles__item-padding"
                                    }
                                >
                                    <div className="consoles__item-details">
                                        <div className="consoles__item-title">
                                            {/* <a href={productItem.url} className="consoles__item-link">
                                                    <span>{newTitle}&#32;</span>
                                                    <span className="consoles__item-link-last-word"> {lastWord}</span>
                                                </a> */}
                                            <a
                                                href={`${currentLocaleHref}/equipment/product/${productItem.slug}`}
                                                className="consoles__item-link"
                                            >
                                                <span>{newTitle}&#32;</span>
                                                <span className="consoles__item-link-last-word">
                                                        {" "}
                                                    {lastWord}
                                                    </span>
                                            </a>
                                            {/* <span className="consoles__item-icon" /> */}
                                        </div>

                                        <p
                                            className="consoles__item-description"
                                            dangerouslySetInnerHTML={{
                                                __html: productItem.anons,
                                            }}
                                        />
                                        <div className="consoles__item-order">
                                                <span className="consoles__item-price">
                                                    {productItem.price}{" "}
                                                    {t("UAH")}
                                                </span>
                                            <ActionsButton
                                                handler={() =>
                                                    handleAppareModal(
                                                        productItem.title,
                                                        productItem.id
                                                    )
                                                }
                                                titleButton={t("toOrder")}
                                            />
                                        </div>
                                    </div>
                                    {/* <span className="consoles__item-picture consoles__item-picture--mecool" /> */}
                                    <img
                                        src={productItem.picture}
                                        alt={`${productItem.title} - інтернет-провайдер Briz в Одесі`}
                                        title={productItem.title}
                                        className="consoles__img"
                                    />
                                </div>
                            </li>
                        );
                    })}
                </ul>
            </section>
        </>
    );
};

TvInet.layout = (page) => <Layout title="TV+INET">{page}</Layout>;

TvInet.propTypes = {
    setModalAccept: PropTypes.func,
    keepTrackCurrentPage: PropTypes.func,
    page: PropTypes.shape({
        meta_title: PropTypes.string,
        products: PropTypes.arrayOf(PropTypes.object),
        category_title: PropTypes.string,
        category_description: PropTypes.string,
        union_description: PropTypes.node,
    }),
    iptv: PropTypes.arrayOf(PropTypes.object),
    tv: PropTypes.arrayOf(PropTypes.object),
    tvmax: PropTypes.arrayOf(PropTypes.object),
    locale: PropTypes.string
};
TvInet.defaultProps = {
    setModalAccept: () => {
    },
    keepTrackCurrentPage: () => {
    },
    page: {},
    iptv: [],
    tv: [],
    tvmax: [],
    locale: "ua"
};

export default connect(null, {setModalAccept, keepTrackCurrentPage})(TvInet);
