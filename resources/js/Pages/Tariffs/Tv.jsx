/* eslint-disable camelcase */
/* eslint-disable no-shadow */
/* eslint-disable react/no-danger */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import Layout from "../../Shared/Layout";
import TariffsList from "../../Shared/TariffsList/TariffsList";
import StepsConnectInternet from "../../Shared/StepsConnectInternet/StepsConnectInternet";
import { setModalAccept, keepTrackCurrentPage } from "../../actions/actions";
import { PRODUCT_PRODUCT } from "../../constants/apiParams";
import ActionsButton from "../../Shared/ui/buttons/ActionsButton";
import { advantagesProviderData } from "../../Shared/AdvantagesProvider/advantagesProviderData";
import AdvantagesProvider from "../../Shared/AdvantagesProvider/AdvantagesProvider";
import { getLocaleHref, isIos, isSafari, getLocale } from "../../utils";
import { useIsSsr } from "../../hooks/useIsSsr";
import {InertiaHead} from '@inertiajs/inertia-react';

const Tv = (props) => {
    const {
        tariffs,
        page: {
            products,
            category_title,
            category_description,
            union_description,
            meta_title,
            title
        },
        setModalAccept,
        keepTrackCurrentPage,
        locale
    } = props

    const isSsr = useIsSsr();
    const { t } = useTranslation();
    const currentLocaleHref = getLocaleHref(locale)
    const currentLocale = getLocale(locale)

    const safari = !isSsr && isSafari();
    const ios = !isSsr && isIos();

    const brizProviderComfortData = advantagesProviderData.advantagesIptv;

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        const title = document.querySelector("title");
        title.textContent = meta_title;
    }, []);

    const handleAppareModal = (productTitle, id) => {
        // Открытие модального окна с передачей в него данных

        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle,
            productId: id,
            titleModal: t("purchaseRequest"),
            titleButton: t("sendRequest"),
            apiUrl: PRODUCT_PRODUCT.url,
            typeProduct: PRODUCT_PRODUCT.type,
            titleProduct: "",
            seoId: PRODUCT_PRODUCT.type,
            body: { [PRODUCT_PRODUCT.type]: id, ghost: "" },
            keyFormRedux: "default",
        });
    };

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {props.page.hasOwnProperty("title") && <title>{title}</title>}*/}

            {/*    {props.page.hasOwnProperty("meta_description") &&*/}
            {/*    <meta name="description" content={props.page["meta_description"]}/>}*/}

            {/*    {props.page.hasOwnProperty("meta_keywords") &&*/}
            {/*    <meta name="keywords" content={props.page["meta_keywords"]}/>}*/}
            {/*    {props.page.hasOwnProperty("meta_title") && <meta name="og:title" content={props.page["meta_title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <div className="tv tariff-page wrapper">
                <h1
                    className={
                        ios
                            ? "page-title tariff-page__page-title tariff-page__page-title--ios"
                            : "page-title tariff-page__page-title"
                    }
                >
                    {t("television")}
                </h1>
                <div className="page-subtitle-h3 tariff-page__page-subtitle">
                    {t("chooseYourTariff")}
                </div>
                <TariffsList tariffs={tariffs} affiliation="Тариф телебачення" />
                <section className="section">
                    <StepsConnectInternet
                        title="howConnectIPTV"
                        typeСonnection="tvIptv"
                    />
                </section>
                <AdvantagesProvider
                    advantagesData={brizProviderComfortData}
                    currentLocaleHref={currentLocaleHref}
                    currentLocale={currentLocale}
                />
                <section className="section">
                    <div className="page-subtitle home__page-subtitle">
                        {t("WhatWatchIPTV")}
                    </div>
                    <div
                        className={
                            ios || safari
                                ? "tariff-page__what-watch tariff-page__what-watch--ios"
                                : "tariff-page__what-watch"
                        }
                    >
                        <span
                            className={
                                ios || safari
                                    ? "tariff-page__img-tv tariff-page__img-tv--ios"
                                    : "tariff-page__img-tv"
                            }
                        />
                        <ul
                            className={
                                ios || safari
                                    ? "tariff-page__list-watch tariff-page__list-watch--ios"
                                    : "tariff-page__list-watch"
                            }
                        >
                            <li
                                className={
                                    ios || safari
                                        ? "tariff-page__item-watch tariff-page__item-watch--ios"
                                        : "tariff-page__item-watch"
                                }
                            >
                                <span
                                    className={
                                        ios || safari
                                            ? "tariff-page__icon-watch tariff-page__icon-watch--ios tariff-page__icon-watch--portal"
                                            : "tariff-page__icon-watch tariff-page__icon-watch--portal"
                                    }
                                />
                                <span className="tariff-page__text-watch">
                                    {t("webPortal")}{" "}
                                    <a
                                        href="http://tv.briz.ua/"
                                        target="_blank"
                                        className="tariff-page__link-watch main-link"
                                    >
                                        tv.briz.ua
                                    </a>
                                </span>
                            </li>
                            <li
                                className={
                                    ios || safari
                                        ? "tariff-page__item-watch tariff-page__item-watch--ios"
                                        : "tariff-page__item-watch"
                                }
                            >
                                <span
                                    className={
                                        ios || safari
                                            ? "tariff-page__icon-watch tariff-page__icon-watch--ios tariff-page__icon-watch--tv-samsung"
                                            : "tariff-page__icon-watch tariff-page__icon-watch--tv-samsung"
                                    }
                                />
                                <span className="tariff-page__text-watch">
                                    {t("SamsungLGTVs")}
                                </span>
                            </li>
                            <li
                                className={
                                    ios || safari
                                        ? "tariff-page__item-watch tariff-page__item-watch--ios"
                                        : "tariff-page__item-watch"
                                }
                            >
                                <span
                                    className={
                                        ios || safari
                                            ? "tariff-page__icon-watch tariff-page__icon-watch--ios tariff-page__icon-watch--tv-android"
                                            : "tariff-page__icon-watch tariff-page__icon-watch--tv-android"
                                    }
                                />
                                <span className="tariff-page__text-watch">
                                    {t("TVsBoxesAndroidTV")}
                                </span>
                            </li>
                            <li
                                className={
                                    ios || safari
                                        ? "tariff-page__item-watch tariff-page__item-watch--ios"
                                        : "tariff-page__item-watch"
                                }
                            >
                                <span
                                    className={
                                        ios || safari
                                            ? "tariff-page__icon-watch tariff-page__icon-watch--ios tariff-page__icon-watch--os-android"
                                            : "tariff-page__icon-watch tariff-page__icon-watch--os-android"
                                    }
                                />
                                <span className="tariff-page__text-watch">
                                    {t("AndroidSmartphonesTablets")}
                                </span>
                            </li>
                        </ul>
                    </div>
                </section>
                <section className="section">
                    <StepsConnectInternet
                        title="howConnectKTV"
                        typeСonnection="tvKTV"
                    />
                </section>
                {union_description ? (
                    <section className="section">
                        <div className="page-subtitle home__page-subtitle">
                            {t("combinedTariffsProfitable")}
                        </div>
                        <p
                            className="tariff-page__union"
                            dangerouslySetInnerHTML={{
                                __html: union_description,
                            }}
                        />

                        <span className="tariff-page__union-bg" />
                    </section>
                ) : null}
            </div>
            <section className="section section__consoles">
                <div className="page-subtitle home__page-subtitle wrapper page-subtitle__padding">
                    {category_title}
                </div>
                <p className="tariff-page__opportunity-txt wrapper consoles__top-description-padding">
                    {category_description}
                </p>
                <ul className="tariff-page__smart-console consoles">
                    {!!products &&
                        !!products.length &&
                        Object.keys(products).map((product, index) => {
                            const productItem = products[product];
                            const splitTitle = productItem.title.split(" ");
                            const lastWord = splitTitle[splitTitle.length - 1];
                            const newTitle = splitTitle
                                .filter((item) => item !== lastWord)
                                .join(" ");

                            return (
                                <li
                                    className="consoles__item consoles__item-padding"
                                    key={productItem.id}
                                >
                                    <div
                                        className={
                                            (index + 1) % 2 === 0
                                                ? "consoles__item-block wrapper consoles__item-padding consoles__item-block--even"
                                                : "consoles__item-block consoles__item-padding wrapper"
                                        }
                                    >
                                        <div className="consoles__item-details">
                                            <div className="consoles__item-title">
                                                {/* <a href={productItem.url} className="consoles__item-link">
                                                    <span>{newTitle}&#32;</span>
                                                    <span className="consoles__item-link-last-word"> {lastWord}</span>
                                                </a> */}
                                                <a
                                                    href={`${currentLocaleHref}/equipment/product/${
                                                        productItem.slug
                                                    }`}
                                                    className="consoles__item-link"
                                                >
                                                    <span>{newTitle}&#32;</span>
                                                    <span className="consoles__item-link-last-word">
                                                        {" "}
                                                        {lastWord}
                                                    </span>
                                                </a>
                                                {/* <span className="consoles__item-icon" /> */}
                                            </div>
                                            <p
                                                className="consoles__item-description"
                                                dangerouslySetInnerHTML={{
                                                    __html: productItem.anons,
                                                }}
                                            />
                                            <div className="consoles__item-order">
                                                <span className="consoles__item-price">
                                                    {productItem.price}{" "}
                                                    {t("UAH")}
                                                </span>
                                                <ActionsButton
                                                    handler={() =>
                                                        handleAppareModal(
                                                            productItem.title,
                                                            productItem.id
                                                        )
                                                    }
                                                    titleButton={t("toOrder")}
                                                />
                                            </div>
                                        </div>

                                        <img
                                            src={productItem.picture}
                                            alt={`${productItem.title} - інтернет-провайдер Briz в Одесі`}
                                            title={productItem.title}
                                            className="consoles__img"
                                        />
                                    </div>
                                </li>
                            );
                        })}
                </ul>
            </section>
        </>
    );
};

Tv.layout = (page) => <Layout title="TV">{page}</Layout>;

Tv.propTypes = {
    setModalAccept: PropTypes.func,
    keepTrackCurrentPage: PropTypes.func,
    page: PropTypes.shape({
        meta_title: PropTypes.string,
        products: PropTypes.arrayOf(PropTypes.object),
        category_title: PropTypes.string,
        category_description: PropTypes.string,
        union_description: PropTypes.string,
    }),
    tariffs: PropTypes.arrayOf(PropTypes.object),
    locale: PropTypes.string
};

Tv.defaultProps = {
    setModalAccept: () => {},
    keepTrackCurrentPage: () => {},
    page: {},
    tariffs: [],
    locale: "ua"
};

export default connect(null, { setModalAccept, keepTrackCurrentPage })(Tv);
