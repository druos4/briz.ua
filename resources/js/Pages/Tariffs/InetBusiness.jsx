//* eslint-disable camelcase */
/* eslint-disable no-shadow */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { Inertia } from "@inertiajs/inertia";
import Layout from "../../Shared/Layout";
import TariffsList from "../../Shared/TariffsList/TariffsList";
import StepsConnectInternet from "../../Shared/StepsConnectInternet/StepsConnectInternet";
import { setModalAccept, keepTrackCurrentPage } from "@/actions/actions";
import cableImg from "../../../images/inet-business/cable.png";
import { CABLE_RENT } from "@/constants/apiParams";
import ActionsButton from "../../Shared/ui/buttons/ActionsButton";
import { getLocale, getLocaleHref, isIos } from "@/utils";
import { useIsSsr } from "@/hooks/useIsSsr";
import { advantagesProviderData } from "@/Shared/AdvantagesProvider/advantagesProviderData";
import AdvantagesProvider from "../../Shared/AdvantagesProvider/AdvantagesProvider";
import {InertiaHead} from '@inertiajs/inertia-react';

const InetBusiness = (props) => {
    const {
        iptv,
        pppoe,
        vlan,
        page: { meta_title, title },
        setModalAccept,
        keepTrackCurrentPage,
        locale
    } = props

    const isSsr = useIsSsr();
    const { t } = useTranslation();
    const ios = !isSsr && isIos();
    const currentLocaleHref = getLocaleHref(locale)
    const currentLocale = getLocale(locale)

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        const title = document.querySelector("title");
        title.textContent = meta_title;
    }, []);

    const brizProviderComfortData =
        advantagesProviderData.reliableAndProfitablePartner;

    const handleAppareModal = () => {
        // Открытие модального окна с передачей в него данных

        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle: "",
            productId: "",
            titleModal: t("rentApplication"),
            titleButton: t("sendRequest"),
            apiUrl: CABLE_RENT.url,
            typeProduct: CABLE_RENT.type,
            titleProduct: "",
            seoId: CABLE_RENT.type,
            body: { ghost: "" },
            keyFormRedux: "default",
        });
    };

    return (
       <>
           {/*<InertiaHead>*/}

           {/*    {props.page.hasOwnProperty("title") && <title>{title}</title>}*/}

           {/*    {props.page.hasOwnProperty("meta_description") &&*/}
           {/*    <meta name="description" content={props.page["meta_description"]}/>}*/}

           {/*    {props.page.hasOwnProperty("meta_keywords") &&*/}
           {/*    <meta name="keywords" content={props.page["meta_keywords"]}/>}*/}
           {/*    {props.page.hasOwnProperty("meta_title") && <meta name="og:title" content={props.page["meta_title"]}/>}*/}
           {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

           {/*</InertiaHead>*/}

           <div className="inet-business">
               <div className="tariff-page wrapper">
                   <h1
                       className={
                           ios
                               ? "page-title tariff-page__page-title tariff-page__page-title--ios"
                               : "page-title tariff-page__page-title"
                       }
                   >
                       {t("foBusiness")}
                   </h1>
                   <div className="page-subtitle-h3 tariff-page__page-subtitle">
                       {t("usingVLANtechnology")}
                   </div>
                   <TariffsList tariffs={vlan} affiliation="Тариф для бiзнесу" />
                   <section className="section tariff-page__business-section">
                       <div className="page-subtitle-h3 tariff-page__page-subtitle">
                           {t("internetConnection")}
                       </div>
                       <TariffsList tariffs={pppoe} affiliation="Тариф для бiзнесу" />
                   </section>
                   <section className="section tariff-page__business-section">
                       <div className="page-subtitle-h3 tariff-page__page-subtitle">
                           {t("televisionIPTV")}
                       </div>
                       <TariffsList tariffs={iptv} affiliation="Тариф для бiзнесу" />
                   </section>
                   <section className="section">
                       <StepsConnectInternet
                           title="howConnectServiceBusiness"
                           typeСonnection="business"
                       />
                   </section>
                   <AdvantagesProvider
                       advantagesData={brizProviderComfortData}
                       currentLocaleHref={currentLocaleHref}
                       currentLocale={currentLocale}
                   />
               </div>
               <section className="inet-business__rent-section">
                   <div className="wrapper">
                       <div className="inet-business__rent-relative">
                           <div className="inet-business__rent">
                               <div className="page-subtitle inet-business__page-subtitle">
                                   {t("opticalFiberRental")}
                               </div>
                               <div className="page-subtitle-h3 inet-business__page-subtitle-tariff">
                                   {t("combineEverythingIntoNetwork")}
                               </div>
                               <div className="inet-business__rent-description">
                                   <p className="inet-business__rent-text">
                                       {t("secureCommunicationLine")}
                                   </p>
                                   <p className="inet-business__rent-text">
                                       {t("longerRentalPeriodCheaper")}
                                   </p>
                               </div>
                               <div className="inet-business__margin-btn">
                                   <ActionsButton
                                       handler={() => handleAppareModal()}
                                       titleButton={t("contactUs")}
                                   />
                               </div>
                           </div>
                           <img
                               src={cableImg}
                               alt="Оренда оптичного волокна - інтернет-провайдер Briz в Одесі"
                               title="Оренда оптичного волокна"
                               className="inet-business__rent-img"
                           />
                       </div>
                   </div>
                   {/* <div className="inet-business__rent-relative">
                    <span className="inet-business__rent-img" />
                </div> */}
               </section>
           </div>
       </>
    );
};

InetBusiness.layout = (page) => <Layout title="Inet Business">{page}</Layout>;

InetBusiness.propTypes = {
    setModalAccept: PropTypes.func,
    keepTrackCurrentPage: PropTypes.func,
    page: PropTypes.shape({ meta_title: PropTypes.string }),
    iptv: PropTypes.arrayOf(PropTypes.object),
    pppoe: PropTypes.arrayOf(PropTypes.object),
    vlan: PropTypes.arrayOf(PropTypes.object),
    locale: PropTypes.string
};
InetBusiness.defaultProps = {
    setModalAccept: () => {},
    keepTrackCurrentPage: () => {},
    page: {},
    iptv: [],
    pppoe: [],
    vlan: [],
    locale: "ua"
};

export default connect(null, { setModalAccept, keepTrackCurrentPage })(
    InetBusiness
);
