/* eslint-disable dot-notation */
/* eslint-disable no-shadow */
/* eslint-disable react/no-danger */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Inertia} from "@inertiajs/inertia";
import Lightbox from "react-image-lightbox";
import {InertiaHead} from "@inertiajs/inertia-react";
import { keepTrackCurrentPage } from "@/actions/actions";
import Layout from "../Shared/Layout";
import TopPanelLinks from "../Shared/HelpWidgets/TopPanelLinks";
import HelpCategoryLinks from "../Shared/HelpWidgets/HelpCategoryLinks";
import HelpCategoryPlitka from "../Shared/HelpWidgets/HelpCategoryPlitka";
import ContactsServiceSupport from "../Shared/HelpWidgets/ContactsServiceSupport";
import ToUp from "../Shared/ToUp";
import {useIsSsr} from '@/hooks/useIsSsr';

const HelpDirectory = (props) => {
    const [isOpenImageModal, setIsOpenImageModal] = useState(false)
    const [selectedImage, setSelectedImage] = useState('')
    const isSsr = useIsSsr();
    const {theme, meta, menu, keepTrackCurrentPage} = props


    const html = (typeof window !== 'undefined') && document.getElementsByTagName("html")[0];

    const handleModalImg = (event) => {
        const element = event.target;
        if (element.tagName === "IMG") {
            setSelectedImage(element.src);
            setIsOpenImageModal(true);
        }
    };

    useEffect(() => {
        if (html.nodeName === 'HTML') {
            html.style.overflow = `${isOpenImageModal ? "hidden" : "initial"}`;
            html.style.position = `${isOpenImageModal ? "hidden" : "initial"}`;
            html.style["touch-action"] = `${isOpenImageModal ? "none" : "initial"}`;
        }
    }, [isOpenImageModal]);

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        if (meta && !!Object.keys(meta).length) {
            const pageTitle = document.querySelector("title");
            // тут мета приходит с meta_title _
            if (meta["meta_title"] !== null) {
                pageTitle.textContent = meta["meta_title"];
            } else {
                pageTitle.textContent = meta.title;
            }
        }
        //
        // if (meta) {
        //     const metaDescription = document.querySelector(
        //         'meta[name="description"]'
        //     );
        //     const metaKeywords = document.querySelector(
        //         'meta[name="keywords"]'
        //     );
        //     metaDescription.content = meta["meta_description"];
        //     metaKeywords.content = meta["meta_keywords"];
        // }
    }, []);

    return (
        <>
            {/*<InertiaHead>*/}
            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta_description") &&*/}
            {/*    <meta name="description" content={meta["meta_description"]}/>}*/}

            {/*    {meta.hasOwnProperty("meta_keywords") && <meta name="keywords" content={meta["meta_keywords"]}/>}*/}
            {/*    {meta.hasOwnProperty("meta_title") && <meta name="og:title" content={meta["meta_title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}
            {/*</InertiaHead>*/}

            <div className="wrapper help-directory">
                <ToUp scrollStepInPx="50" delayInMs="5"/>
                {isOpenImageModal && (
                    <Lightbox
                        mainSrc={selectedImage}
                        onCloseRequest={() => setIsOpenImageModal(false)}
                        wrapperClassName={"image-modal"}
                        reactModalStyle={{ overlay: { zIndex: 2000 } }}
                    />
                )}
                <TopPanelLinks menu={menu}/>
                <section className="section help-directory__section">
                    <h1 className="page-title help-directory__page-title">
                        {theme.title}
                    </h1>

                    {!theme.detail_empty && (
                        <div
                            className="help-articles__content"
                            dangerouslySetInnerHTML={{__html: theme.detail}}
                            onClick={handleModalImg}
                        />
                    )}

                    <div className="help-directory__categories ">
                        {theme.children_show === "lenta" && (
                            <HelpCategoryLinks theme={theme}/>
                        )}
                        {theme.children_show === "plitka" && (
                            <HelpCategoryPlitka theme={theme}/>
                        )}
                    </div>
                    <div className="help-directory__contacts-service-support">
                        <ContactsServiceSupport/>
                    </div>
                </section>
            </div>
        </>
    );
};

HelpDirectory.layout = (page) => <Layout title="Help directory">{page}</Layout>;

HelpDirectory.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    theme: PropTypes.shape({
        children_show: PropTypes.string,
        detail: PropTypes.string,
        detail_empty: PropTypes.bool,
        title: PropTypes.string,
    }),
    meta: PropTypes.shape({
        meta_title: PropTypes.string,
        title: PropTypes.string,
    }),
    menu: PropTypes.arrayOf(PropTypes.object),
};
HelpDirectory.defaultProps = {
    keepTrackCurrentPage: () => {
    },
    theme: {},
    meta: {},
    menu: [],
};

export default connect(null, {keepTrackCurrentPage})(HelpDirectory);
