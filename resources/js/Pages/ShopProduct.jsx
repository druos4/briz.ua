/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-shadow */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/no-danger */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import { keepTrackCurrentPage, setModalAccept, setShopProduct } from "../actions/actions";
import Layout from "../Shared/Layout";
import { PRODUCT_PRODUCT } from "../constants/apiParams";
import { useMediaQuery } from "../Shared/MediaQuery/useMediaQuery";
import ShopProductButton from "../Shared/ui/buttons/ShopProductButton";
import ShopProductButtonCard from "../Shared/ui/buttons/ShopProductButtonCard";
import { useIsSsr } from "../hooks/useIsSsr";
import { InertiaHead } from '@inertiajs/inertia-react';
import { getLocaleHref } from "@/utils";

const deliverySetupHref = '/faq/texniceskie-voprosy/dostavka-i-nalastuvannya-obladnannya'

const ShopProduct = (props) => {
    const {
        product,
        properties,
        buywith,
        keepTrackCurrentPage,
        setModalAccept,
        setShopProduct,
        isShowModalAccept,
        locale
    } = props;

    const isSsr = useIsSsr();
    const { t } = useTranslation();
    const isWidthMobile = useMediaQuery("(max-width: 1025px)");
    const isWidthDesktop = useMediaQuery("(min-width: 1025px)");
    const currentLocaleHref = getLocaleHref(locale)

    const lang = !isSsr && app.getLang();

    const [miniatureLink, setMiniatureLink] = useState(
        product.gallery[0] && product.gallery[0].original
    );
    const [cutPropertiesArr, setCutPropertiesArr] = useState([]);
    const [isOpen, setOpen] = useState(false);
    const [showGuarantee, setShowGuarantee] = useState(false);

    const getGuarantyLink = (currLang) => {
        switch (currLang) {
            case "en":
                return "/en/faq/texniceskie-voprosy?garantiyi-na-obladnannya-vid-briz";
            case "ru":
                return "/ru/faq/texniceskie-voprosy?garantiyi-na-obladnannya-vid-briz";
            default:
                return "/faq/texniceskie-voprosy?garantiyi-na-obladnannya-vid-briz";
        }
    };

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);
        const separatedProperties = [];

        const title = document.querySelector("title");
        title.textContent = product.title;

        properties.forEach((property) => {
            return property.values.sort((a, b) => {
                return b.value.length - a.value.length;
            });
        });

        if (isWidthMobile && !isWidthDesktop) {
            // let propertiesLength = properties.length;
            // let firstPartProperties = properties.slice(0, propertiesLength / 2);
            let firstPartProperties = properties.slice(0, 10);

            setCutPropertiesArr(firstPartProperties);
        } else {
            if (properties.length > 1) {

                const size = properties.length / 2;
                for (let i = 0; i < Math.ceil(properties.length / size); i += 1) {
                    separatedProperties[i] = properties.slice((i * size), (i * size) + size);
                }

                setCutPropertiesArr(separatedProperties);
            } else {
                separatedProperties.push(properties);

                setCutPropertiesArr(separatedProperties);
            }
        }
    }, [isWidthMobile,isWidthDesktop]);

    const handleMouseOverMiniature = (miniature) => {
        setMiniatureLink(miniature);
    };

    const handleAppareModal = (e, productTitle, id, product) => {
        e.preventDefault();
        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle,
            productId: id,
            titleModal: t("purchaseRequest"),
            titleButton: t("sendRequest"),
            apiUrl: PRODUCT_PRODUCT.url,
            typeProduct: PRODUCT_PRODUCT.type,
            titleProduct: "",
            seoId: PRODUCT_PRODUCT.type,
            body: { [PRODUCT_PRODUCT.type]: id, ghost: "" },
            keyFormRedux: "default",
        });

        setShopProduct(product);
    };

    useEffect(() => {
        if (!isShowModalAccept) {
            setShopProduct({});
        }
    }, [isShowModalAccept]);

    useEffect(() => {
        // let propertiesLength = properties.length;
        // let firstPartProperties = properties.slice(0, propertiesLength / 2);
        let firstPartProperties = properties.slice(0, 10);

        if (isWidthMobile && !isWidthDesktop) {
            if (isOpen) {
                setCutPropertiesArr(properties);
            } else {
                setCutPropertiesArr(firstPartProperties);
            }
        }
    }, [isOpen]);

    const handleOpenRestChars = () => {
        setOpen(!isOpen);
    };

    const renderSimilarProducts = () => {
        return (
            <div className="product-cart__similar">
                <div className="page-subtitle product-cart__page-subtitle-similar">
                    {t("similarProducts")}
                </div>
                <ul className="shop__list-catalog product-cart__list-catalog">
                    {buywith.map((product) => {
                        return product.can_buy ? (
                            <li
                                className="shop__item-catalog"
                                key={product.slug}
                            >
                                {/* <a href={product.url}> */}
                                <a
                                    href={product.url}
                                    className="shop__item-link"
                                >
                                    <div
                                        className="shop__item-wrapper"
                                    >
                                        <img
                                            src={product.picture}
                                            alt={`${product.title} - інтернет-провайдер Briz в Одесі`}
                                            title={product.title}
                                            className="shop__item-catalog-pic"
                                        />
                                        <div className="shop__item-catalog-info">
                                            <div
                                                className="shop__item-chips-wrapper"
                                            >
                                                {
                                                    product.top ?
                                                        (
                                                            <div
                                                                className="shop__item-catalog-top  shop__item-chips"
                                                            >
                                                            <span className="shop__item-catalog-label shop__item-catalog-label--top">
                                                                {t("top")}
                                                            </span>
                                                            </div>
                                                        )
                                                        :
                                                        null
                                                }
                                                {
                                                    product.has_discount ?
                                                        (
                                                            <div className="shop__item-catalog-discount shop__item-chips">
                                                            <span className="shop__item-catalog-label">
                                                                {t("discount")}
                                                            </span>
                                                            </div>
                                                        )
                                                        :
                                                        null
                                                }
                                                {
                                                    product.promo ?
                                                        (
                                                            <div
                                                                className="shop__item-catalog-promo  shop__item-chips"
                                                            >
                                                            <span className="shop__item-catalog-label">
                                                                {t("labelPromo")}
                                                            </span>
                                                            </div>
                                                        )
                                                        :
                                                        null
                                                }
                                                {
                                                    product.new ?
                                                        (
                                                            <div
                                                                className="shop__item-catalog-new  shop__item-chips"
                                                            >
                                                            <span className="shop__item-catalog-label">
                                                                {t("new")}
                                                            </span>
                                                            </div>
                                                        )
                                                        :
                                                        null
                                                }
                                            </div>
                                            <p
                                                className="shop__item-catalog-title"
                                            >
                                                {product.title}
                                            </p>
                                            <span className="shop__item-catalog-can-buy shop__item-catalog-can-buy-icon">
                                                {t("freeDeliverySetupSecond")}
                                            </span>
                                            <p
                                                className="shop__item-catalog-short-note"
                                            >
                                                {product.section_description}
                                            </p>
                                            <div className="shop__item-price-block">
                                                <span
                                                    className="shop__item-true-price"
                                                >
                                                    <span>
                                                        {product.price}
                                                    </span>{" "}
                                                    <span
                                                        className={
                                                            lang === "en"
                                                                ? "shop__currency"
                                                                : ""
                                                        }
                                                    >
                                                        {t("UAH")}
                                                    </span>
                                                </span>
                                                {product.price_old ? (
                                                    <span className="shop__item-old-price">
                                                        {product.price_old}{" "}
                                                        <span
                                                            className={
                                                                lang === "en"
                                                                    ? "shop__currency"
                                                                    : ""
                                                            }
                                                        >
                                                            {t("UAH")}
                                                        </span>
                                                    </span>
                                                ) : null}
                                            </div>
                                            <ShopProductButton
                                                handler={(e) =>
                                                    handleAppareModal(
                                                        e,
                                                        product.title,
                                                        product.id,
                                                        product
                                                    )
                                                }
                                                titleButton={t("toOrder")}
                                            />
                                        </div>
                                    </div>
                                </a>
                            </li>
                        ) : null;
                    })}
                </ul>
            </div>
        );
    };

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {product.hasOwnProperty("title") && <title>{product.title}</title>}*/}

            {/*    {product.hasOwnProperty("meta_description") &&*/}
            {/*    <meta name="description" content={product["meta_description"]}/>}*/}

            {/*    {product.hasOwnProperty("meta_keywords") && <meta name="keywords" content={product["meta_keywords"]}/>}*/}
            {/*    {product.hasOwnProperty("meta_title") && <meta name="og:title" content={product["meta_title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <div className="product-cart wrapper">
                <section className="section product-cart__section">
                    <div
                        className="product-cart__general product-cart__general--top"
                        itemScope
                        itemType="http://schema.org/Product"
                    >
                        <h1
                            className="page-title product-cart__product-title"
                            itemProp="name"
                        >
                            {product.title}
                        </h1>
                        <div className="product-cart__general-flex-row">
                            <div className="product-cart__preview-block">
                                <div className="product-cart__preview">
                                    <div
                                        className="shop__item-chips-wrapper shop__item-chips-wrapper--product-cart"
                                    >
                                        {
                                            product.top ?
                                                (
                                                    <div
                                                        className="shop__item-catalog-top  shop__item-chips"
                                                    >
                                                        <span className="shop__item-catalog-label shop__item-catalog-label--top">
                                                            {t("top")}
                                                        </span>
                                                    </div>
                                                )
                                                :
                                                null
                                        }
                                        {
                                            product.has_discount ?
                                                (
                                                    <div className="shop__item-catalog-discount shop__item-chips">
                                                        <span className="shop__item-catalog-label">
                                                            {t("discount")}
                                                        </span>
                                                    </div>
                                                )
                                                :
                                                null
                                        }
                                        {
                                            product.promo ?
                                                (
                                                    <div
                                                        className="shop__item-catalog-promo  shop__item-chips"
                                                    >
                                                        <span className="shop__item-catalog-label">
                                                            {t("labelPromo")}
                                                        </span>
                                                    </div>
                                                )
                                                :
                                                null
                                        }
                                        {
                                            product.new ?
                                                (
                                                    <div
                                                        className="shop__item-catalog-new  shop__item-chips"
                                                    >
                                                        <span className="shop__item-catalog-label">
                                                            {t("new")}
                                                        </span>
                                                    </div>
                                                )
                                                :
                                                null
                                        }
                                    </div>
                                    <img
                                        src={miniatureLink}
                                        className="product-cart__preview-img"
                                        itemProp="image"
                                        alt={`${product.title} - інтернет-провайдер Briz в Одесі`}
                                        title={product.title}
                                    />
                                    {product.meta_description && (
                                        <span itemProp="description" hidden>
                                            {product.meta_description}
                                        </span>
                                    )}
                                </div>
                                <ul
                                    className={
                                        product.gallery.length < 4
                                            ? "product-cart__miniatures"
                                            : "product-cart__miniatures product-cart__miniatures--space-between"
                                    }
                                >
                                    {product.gallery.map((miniature, index) => {
                                        return (
                                            <li
                                                className={
                                                    product.gallery.length < 4
                                                        ? "product-cart__miniature-item product-cart__miniature-item--margin-left"
                                                        : "product-cart__miniature-item"
                                                }
                                                key={index}
                                                onMouseOver={() =>
                                                    handleMouseOverMiniature(
                                                        miniature.original
                                                    )
                                                }
                                            >
                                                <img
                                                    alt={`${product.title}-${index + 1} - інтернет-провайдер Briz в Одесі`}
                                                    title={product.title + '-' + (index + 1)}
                                                    src={miniature.thumbnail}
                                                    className="product-cart__miniature-img"
                                                />
                                            </li>
                                        );
                                    })}
                                </ul>
                                <div className="product-cart__detail-tablet">
                                    <div className="shop__item-price-block product-cart__item-price-block">
                                        <span
                                            className="shop__item-true-price"
                                            itemProp="offers"
                                            itemScope
                                            itemType="http://schema.org/Offer"
                                        >
                                            <meta
                                                itemProp="priceCurrency"
                                                content="UAH"
                                            />
                                            <span itemProp="price">
                                                {product.price}
                                            </span>{" "}
                                            <span
                                                className={
                                                    lang === "en"
                                                        ? "shop__currency"
                                                        : ""
                                                }
                                            >
                                                {t("UAH")}
                                            </span>
                                            <span
                                                itemProp="availability"
                                                href="http://schema.org/InStock"
                                                hidden
                                            >
                                                InStock
                                            </span>
                                        </span>
                                        {product.price_old ? (
                                            <span className="shop__item-old-price">
                                                {product.price_old}{" "}
                                                <span
                                                    className={
                                                        lang === "en"
                                                            ? "shop__currency-old-price"
                                                            : ""
                                                    }
                                                >
                                                    {t("UAH")}
                                                </span>
                                            </span>
                                        ) : null}
                                    </div>
                                    <ShopProductButtonCard
                                        disabled={product.quantity === 0}
                                        handler={(e) =>
                                            handleAppareModal(
                                                e,
                                                product.title,
                                                product.id,
                                                product
                                            )
                                        }
                                        titleButton={t("toOrder")}
                                    />
                                    {product.quantity === 0
                                        ? (
                                            <p className="product-cart__delivery-settings-text product-cart__delivery-settings-text--disabled">
                                                {t("notAreAvailable")}
                                            </p>)
                                        : (
                                            <a href={`${currentLocaleHref}${deliverySetupHref}`} className="product-cart__delivery-settings-text product-cart__delivery-settings-text-icon">
                                                {t("freeDeliverySetupSecond")}
                                            </a>)}
                                </div>
                            </div>
                            <div className="product-cart__detail">
                                <div className="shop__item-price-block product-cart__item-price-block product-cart__item-price-block--no-tablet">
                                    <span
                                        className="shop__item-true-price"
                                        itemProp="offers"
                                        itemScope
                                        itemType="http://schema.org/Offer"
                                    >
                                        <meta
                                            itemProp="priceCurrency"
                                            content="UAH"
                                        />
                                        <span itemProp="price">
                                            {product.price}
                                        </span>{" "}
                                        <span
                                            className={
                                                lang === "en"
                                                    ? "shop__currency"
                                                    : ""
                                            }
                                        >
                                            {t("UAH")}
                                        </span>
                                        <span
                                            itemProp="availability"
                                            href="http://schema.org/InStock"
                                            hidden
                                        >
                                            InStock
                                        </span>
                                    </span>
                                    {product.price_old ? (
                                        <span className="shop__item-old-price">
                                            {product.price_old}{" "}
                                            <span
                                                className={
                                                    lang === "en"
                                                        ? "shop__currency-old-price"
                                                        : ""
                                                }
                                            >
                                                {t("UAH")}
                                            </span>
                                        </span>
                                    ) : null}
                                </div>
                                <ShopProductButtonCard
                                    disabled={product.quantity === 0}
                                    handler={(e) =>
                                        handleAppareModal(
                                            e,
                                            product.title,
                                            product.id,
                                            product
                                        )
                                    }
                                    titleButton={t("toOrder")}
                                    customClass="product-cart__order-btn--no-tablet"
                                />
                                {product.quantity === 0
                                    ? (
                                        <p className="product-cart__delivery-settings-text product-cart__delivery-settings-text--disabled product-cart__delivery-settings-text--no-tablet">
                                            {t("notAreAvailable")}
                                        </p>)
                                    : (
                                        <a href={`${currentLocaleHref}${deliverySetupHref}`} className="product-cart__delivery-settings-text product-cart__delivery-settings-text-icon product-cart__delivery-settings-text--no-tablet">
                                            {t("freeDeliverySetupSecond")}
                                        </a>)}
                                {product?.warranty ? (
                                    <>
                                        <div
                                            role="button"
                                            tabIndex="0"
                                            onClick={() =>
                                                setShowGuarantee(!showGuarantee)
                                            }
                                            onKeyDown={(e) => {
                                                if (e.keyCode === 13) {
                                                    setShowGuarantee(
                                                        !showGuarantee
                                                    );
                                                }
                                            }}
                                            className="product-cart__guarantee-block product-cart__guarantee-block--no-tablet"
                                        >
                                            <div className="product-cart__guarantee-block-title-wrap">
                                                <p className="product-cart__guarantee-block-title">
                                                    {`${t("guarantee")} ${product?.warranty
                                                        }!`}
                                                </p>
                                                <span
                                                    className={
                                                        showGuarantee
                                                            ? "accordion__toggle-image"
                                                            : "accordion__toggle-image accordion__toggle-image--active product-cart__toggle-image--active--no-tablet"
                                                    }
                                                >
                                                    <svg
                                                        width="19"
                                                        height="11"
                                                        viewBox="0 0 19 11"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path
                                                            d="M16.8847 10.6283C17.3686 11.1239 18.1532 11.1239 18.6371 10.6283C19.121 10.1326 19.121 9.32895 18.6371 8.83329L10.3762 0.371749C9.89229 -0.123917 9.10771 -0.123917 8.6238 0.371749L0.362933 8.83329C-0.120978 9.32895 -0.120978 10.1326 0.362933 10.6283C0.846843 11.1239 1.63142 11.1239 2.11533 10.6283L9.5 3.06419L16.8847 10.6283Z"
                                                            fill="#00A3A4"
                                                        />
                                                    </svg>
                                                </span>
                                            </div>
                                        </div>
                                        <div
                                            className={`accordion__collapse ${!showGuarantee
                                                ? "accordion__collapse--active"
                                                : ""
                                                }`}
                                        >
                                            <p className="product-cart__guarantee-block-description">
                                                {t("guaranteeInfo")}
                                            </p>
                                            <p className="product-cart__guarantee-block-link">
                                                {t("guaranteeDetails")}{" "}
                                                <a href={getGuarantyLink(lang)}>
                                                    {t("guaranteeLink")}
                                                </a>
                                            </p>
                                        </div>
                                    </>
                                ) : null}

                                <div className="product-cart__description-block">
                                    <p className="product-cart__description-title">
                                        {t("description")}
                                    </p>
                                    <div
                                        className="product-cart__description-text"
                                        dangerouslySetInnerHTML={{
                                            __html: product.description,
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    {properties.length ? (
                        <div className="product-cart__general">
                            <div className="page-subtitle product-cart__page-subtitle">
                                {t("characteristics")}
                            </div>
                            <div className="product-cart__wrapper-list-chars">
                                {cutPropertiesArr.length && cutPropertiesArr.map((items, idx) => {
                                    return (
                                        <ul className="product-cart__char-list-item" key={idx}>
                                            {
                                                items.length && items.map((property, index) => {
                                                    return (
                                                        <li
                                                            className="product-cart__char-item"
                                                            key={index}
                                                        >
                                                            <p className="product-cart__char-title">
                                                                <span className="product-cart__char-title-text">
                                                                    {property.title}
                                                                </span>
                                                            </p>
                                                            {/* <span className="product-cart__dashed" /> */}
                                                            <ul className="product-cart__char-list-values">
                                                                {property.values.map(
                                                                    (item, index) => {
                                                                        return (
                                                                            <li
                                                                                key={index}
                                                                                className="product-cart__char-item-value"
                                                                            >
                                                                                <p className="product-cart__char-value">
                                                                                    {item.value}
                                                                                </p>
                                                                            </li>
                                                                        );
                                                                    }
                                                                )}
                                                            </ul>
                                                        </li>
                                                    );
                                                })
                                            }
                                        </ul>
                                    );
                                })}
                            </div>
                            <ul className="product-cart__char-list">
                                {cutPropertiesArr.length && cutPropertiesArr.map((property, index) => {
                                    return (
                                        <li
                                            className="product-cart__char-item"
                                            key={index}
                                        >
                                            <p className="product-cart__char-title">
                                                <span className="product-cart__char-title-text">
                                                    {property.title}
                                                </span>
                                            </p>
                                            {/* <span className="product-cart__dashed" /> */}
                                            <ul className="product-cart__char-list-values">
                                                {property.values.length && property.values.map(
                                                    (item, index) => {
                                                        return (
                                                            <li
                                                                key={index}
                                                                className="product-cart__char-item-value"
                                                            >
                                                                <p className="product-cart__char-value">
                                                                    {item.value}
                                                                </p>
                                                            </li>
                                                        );
                                                    }
                                                )}
                                            </ul>
                                        </li>
                                    );
                                })}
                            </ul>
                            <div
                                tabIndex="0"
                                role="button"
                                className="product-cart__open-rest-chars"
                                onClick={() => handleOpenRestChars()}
                                onKeyDown={(e) => {
                                    if (e.keyCode === 13) {
                                        handleOpenRestChars();
                                    }
                                }}
                            >
                                {properties.length > 10 ? (
                                    isOpen ? (
                                        <>
                                            <span className="product-cart__text">
                                                {t("hide")}
                                            </span>{" "}
                                            <span className="product-cart__icon-arrow-low" />
                                        </>
                                    ) : (
                                        <>
                                            <span className="product-cart__text">
                                                {t("showAll")}
                                            </span>{" "}
                                            <span className="product-cart__icon-arrow-high" />
                                        </>
                                    )
                                ) : null}
                            </div>
                        </div>
                    ) : null}
                    {!!buywith.length && renderSimilarProducts()}
                </section>
            </div>
        </>
    );
};

ShopProduct.layout = (page) => <Layout title="Shop Product">{page}</Layout>;

ShopProduct.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    setModalAccept: PropTypes.func,
    setShopProduct: PropTypes.func,
    product: PropTypes.shape({
        gallery: PropTypes.arrayOf(PropTypes.object),
        title: PropTypes.string,
        description: PropTypes.string,
        warranty: PropTypes.string,
        price_old: PropTypes.number,
        price: PropTypes.number,
        meta_description: PropTypes.string,
        id: PropTypes.number,
        has_discount: PropTypes.bool,
        meta_title: PropTypes.string,
        promo: PropTypes.bool,
        new: PropTypes.bool,
        can_buy: PropTypes.bool,
        slug: PropTypes.string,
        url: PropTypes.string,
        section_description: PropTypes.string,
        picture: PropTypes.string,
    }),
    properties: PropTypes.arrayOf(PropTypes.object),
    buywith: PropTypes.arrayOf(PropTypes.object),
};
ShopProduct.defaultProps = {
    keepTrackCurrentPage: () => { },
    setModalAccept: () => { },
    setShopProduct: () => { },
    product: {},
    properties: [],
    buywith: [],
};

const mapStateToProps = (state) => {
    return {
        isShowModalAccept: state.modalAccept.showModalAccept
    }
}

export default connect(mapStateToProps, { keepTrackCurrentPage, setModalAccept, setShopProduct })(
    ShopProduct
);
