/* eslint-disable no-shadow */
/* eslint-disable react/no-array-index-key */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { connect } from "react-redux";
import { Inertia } from "@inertiajs/inertia";
import Layout from "../Shared/Layout";
import Accordion from "../Shared/Accordion/Accordion";
import { keepTrackCurrentPage } from "@/actions/actions";
import { useIsSsr } from '@/hooks/useIsSsr';
import { isIos } from '@/utils';
import {InertiaHead} from '@inertiajs/inertia-react';

const Payment = (props) => {
    const { keepTrackCurrentPage, meta, payments } = props

    const isSsr = useIsSsr();
    const { t } = useTranslation();

    const ios = !isSsr && isIos();

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        if (meta && !!Object.keys(meta).length) {
            const title = document.querySelector("title");
            title.textContent = meta["meta-title"];
        }
    }, []);

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta-description") &&*/}
            {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

            {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
            {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <div className="payment wrapper">
                <h1
                    className={
                        ios
                            ? "page-title payment__page-title payment__page-title--ios"
                            : "page-title payment__page-title"
                    }
                >
                    {t("paymentMethods")}
                </h1>
                <section className="payment__tabset">
                    {payments.map((payment, index) => {
                        return (
                            <Accordion
                                key={index}
                                title={payment.title}
                                payment={payment}
                            />
                        );
                    })}
                </section>
            </div>
            <div className="payment__decotaion-content">
                <span className="payment__decoration-img payment__decoration-img--order1" />
                <span className="payment__decoration-img payment__decoration-img--order2" />
                <span className="payment__decoration-img payment__decoration-img--order3" />
                <span className="payment__decoration-img payment__decoration-img--order4" />
            </div>
        </>
    );
};

Payment.layout = (page) => <Layout title="payment">{page}</Layout>;

Payment.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    meta: PropTypes.shape({
        "meta-title": PropTypes.string,
        title: PropTypes.string,
    }),
    payments: PropTypes.arrayOf(PropTypes.object),
};
Payment.defaultProps = {
    keepTrackCurrentPage: () => {},
    meta: {},
    payments: [],
};

export default connect(null, { keepTrackCurrentPage })(Payment);
