/* eslint-disable no-shadow */
/* eslint-disable no-console */
/* eslint-disable no-return-assign */
import React, {useState, useEffect, useRef} from "react";
import PropTypes from "prop-types";
import {InertiaLink} from "@inertiajs/inertia-react";
import {connect} from "react-redux";
import {useTranslation} from "react-i18next";
import axios from "axios";
import {Inertia} from "@inertiajs/inertia";
import Layout from "../Shared/Layout";
import BannerSlider from "../Shared/BannerSlider";
import OfferTariffs from "../Shared/OfferTariffs/OfferTariffs";
import OurApps from "../Shared/OurApps";
import {menuIsLoading, keepTrackCurrentPage} from "../actions/actions";
import AdvantagesProvider from "../Shared/AdvantagesProvider/AdvantagesProvider";
import {advantagesProviderData} from "../Shared/AdvantagesProvider/advantagesProviderData";
import { isIos, isSafari, getLocaleHref, getLocale } from "../utils";
import {useIsSsr} from '../hooks/useIsSsr';
import {InertiaHead} from '@inertiajs/inertia-react';

const Home = (props) => {

    const {news, sliders, tarifs, meta, keepTrackCurrentPage, locale} = props

    const isSsr = useIsSsr();
    const {t} = useTranslation();

    const ios = !isSsr && isIos();
    const safari = !isSsr && isSafari();
    const currentLocaleHref = getLocaleHref(locale)
    const currentLocale = getLocale(locale)

    const [fixesMessage, setFixesMessage] = useState(false);
    const cardsListElement = useRef();
    const brizProviderComfortData = advantagesProviderData.brizProviderComfort;

    useEffect(() => {
        let cleanupFunction = false;

        keepTrackCurrentPage(Inertia.page.component);

        if (meta && !!Object.keys(meta).length) {
            const title = document.querySelector("title");
            title.textContent = meta["meta-title"];
        }

        (async () => {
            try {
                await axios.get("/check-for-remont").then((res) => {
                    if (!cleanupFunction) {
                        setFixesMessage(res.data.remont);
                    }
                });
            } catch (e) {
                console.log(e.message);
            }
        })();

        return () => (cleanupFunction = true);
    }, []);

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta-description") &&*/}
            {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

            {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
            {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}
            <div className="home">
                {fixesMessage ? (
                    <div
                        className={`home__fixes-message-container ${
                            ios ? "home__fixes-message-container--ios" : ""
                        }`}
                    >
                        <div className="home__fixes-message">
                            {window.screen.availWidth > 1025
                                ? t("technicalWorkProgress")
                                : t("tech")}
                        </div>
                        <a
                            className="main-link home__fixes-message-link"
                            href={`${currentLocaleHref}/news/remont`}
                        >
                            {t("moreDetails")}
                            <span className="home__fixes-message-link-arrow"/>
                        </a>
                    </div>
                ) : null}

                {sliders?.length ? (
                    <div
                        className={
                            ios || safari
                                ? "home__slider home__slider--ios"
                                : "home__slider"
                        }
                        style={
                            (fixesMessage && ios) || (safari && !ios)
                                ? {marginTop: "0"}
                                : {marginTop: ""}
                        }
                    >
                        <BannerSlider sliders={sliders} currentLocale={currentLocale}/>
                    </div>
                ) : null}

                <div className="wrapper">
                    <section className="home__section-top home__section-top-tariffs">
                        <h1 className="page-title home__page-title">
                            {t("internetProviderBrizInOdessa")}
                        </h1>
                        <div className="page-subtitle home__page-subtitle">
                            {t("recommendedRates")}
                        </div>
                        <div
                            className={
                                ios || safari
                                    ? "offer-tariffs__cards-list offer-tariffs__cards-list--ios"
                                    : "offer-tariffs__cards-list"
                            }
                            ref={cardsListElement}
                        >
                            {Object.keys(tarifs).map((tariff) => {
                                const itemTariff = tarifs[tariff];
                                // itemTariff.sort((x, y) => !!x.first_to_show ? -1 : !!y.first_to_show ? 1 : 0);

                                const tariffTitle =
                                    (tariff === "homeInet" && t("internet")) ||
                                    (tariff === "union" && t("internetPlusTv")) ||
                                    (tariff === "tv" && t("television")) ||
                                    (tariff === "business" && t("forBusiness"));
                                const tariffUrl =
                                    (tariff === "homeInet" &&
                                        `${currentLocaleHref}/domashniy-internet`) ||
                                    (tariff === "union" &&
                                        `${currentLocaleHref}/televidenie-i-internet`) ||
                                    (tariff === "tv" &&
                                        `${currentLocaleHref}/televidenie`) ||
                                    (tariff === "business" &&
                                        `${currentLocaleHref}/internet-dlya-biznesa`);

                                if (itemTariff?.length) {
                                    return (
                                        <OfferTariffs
                                            key={tariff}
                                            tariff={itemTariff}
                                            linkText={tariffTitle}
                                            linkUrl={tariffUrl}
                                            tariffKey={tariff}
                                            cardsListElement={cardsListElement}
                                        />
                                    );
                                }
                                return null;
                            })}
                        </div>
                    </section>
                    <AdvantagesProvider
                        advantagesData={brizProviderComfortData}
                        currentLocaleHref={currentLocaleHref}
                        currentLocale={currentLocale}
                    />
                </div>
                {/* <LazyLoad height={300}> */}
                {/* <section className="section home__section-map">
                    <CoverageMap title={t("findOutIfThereIsBreeze")} />
                </section> */}

                <div className="wrapper">
                    <section className="section home__section-cards">
                        <div className="page-subtitle home__page-subtitle">
                            {t("newsAndPromotions")}
                        </div>
                        <ul
                            className="actions__cards-list actions__cards-list-recomended"
                            style={
                                ios || safari
                                    ? {
                                        columnGap: "initial",
                                        rowGap: "initial",
                                    }
                                    : {
                                        columnGap: "30px",
                                        rowGap: "50px",
                                    }
                            }
                        >
                            {news.map((item) => {
                                return (
                                    <li
                                        className={
                                            ios || safari
                                                ? "actions__card-item actions__card-item--ios actions__card-item-recomended"
                                                : "actions__card-item actions__card-item-recomended"
                                        }
                                        key={item.id}
                                    >
                                        <a
                                            className="actions__card-link"
                                            href={item.url}
                                        >
                                            <img
                                                src={item.thumbnail}
                                                alt={`${item.title} - інтернет-провайдер Briz в Одесі`}
                                                title={item.title}
                                                className="actions__card-img actions__card-img-recomended"
                                            />

                                            {/* <span className="actions__card-img" style={{backgroundImage: `url(http://localhost:3000${action.picture})`}} /> */}
                                            <div className="actions__card-bottom-content">
                                                <div className="actions__card-title">
                                                    {item.title}
                                                </div>
                                                <span className="actions__card-date">
                                                    {item.published}
                                                </span>
                                            </div>
                                        </a>
                                    </li>
                                );
                            })}
                        </ul>
                    </section>

                    <section
                        className={
                            ios || safari
                                ? "section home__section-our-apps home__section-our-apps--ios"
                                : "section home__section-our-apps"
                        }
                    >
                        <div className="page-subtitle home__page-subtitle home__page-subtitle--modif">
                            {t("ourApps")}
                        </div>
                        <OurApps currentLocaleHref={currentLocaleHref} currentLocale={currentLocale}/>
                    </section>
                </div>
                {/* </LazyLoad> */}
            </div>
        </>
    );
};

Home.layout = (page) => <Layout title="home">{page}</Layout>;

Home.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    meta: PropTypes.shape({
        "meta-title": PropTypes.string,
        title: PropTypes.string,
    }),
    news: PropTypes.arrayOf(PropTypes.object),
    sliders: PropTypes.arrayOf(PropTypes.object),
    tarifs: PropTypes.shape({
        business: PropTypes.arrayOf(PropTypes.object),
        homeInet: PropTypes.arrayOf(PropTypes.object),
        tv: PropTypes.arrayOf(PropTypes.object),
        union: PropTypes.arrayOf(PropTypes.object),
    }),
    locale: PropTypes.string
};
Home.defaultProps = {
    keepTrackCurrentPage: () => {
    },
    meta: {},
    news: [],
    sliders: [],
    tarifs: {},
    locale: "ua"
};

export default connect(null, {menuIsLoading, keepTrackCurrentPage})(Home);
