/* eslint-disable no-shadow */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import CoverageMapContainer from '../Shared/CoverageMap/CoverageMapContainer';
import { connect } from "react-redux";
import { Inertia } from "@inertiajs/inertia";
import Layout from "../Shared/Layout";
import { keepTrackCurrentPage } from "../actions/actions";
import { useIsSsr } from '../hooks/useIsSsr';
import { isIos } from '../utils';
import {InertiaHead} from '@inertiajs/inertia-react';

const Map = (props) => {
    const { meta, keepTrackCurrentPage, locale } = props

    const isSsr = useIsSsr();
    const ios = !isSsr && isIos();

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        if (meta && !!Object.keys(meta).length) {
            const title = document.querySelector("title");
            title.textContent = meta["meta-title"];
        }
    }, []);

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta-description") &&*/}
            {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

            {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
            {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <section className={ios ? "coverage-map-page coverage-map-page--ios" : "coverage-map-page"}>
                <CoverageMapContainer locale={locale}/>
            </section>
        </>
    )
}

Map.layout = (page) => <Layout title="Map">{page}</Layout>;

Map.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    meta: PropTypes.shape({
        "meta-title": PropTypes.string,
        title: PropTypes.string,
    }),
    locale: PropTypes.string
};
Map.defaultProps = {
    keepTrackCurrentPage: () => {},
    meta: {},
    locale: "ua"
};

export default connect(null, { keepTrackCurrentPage })(Map);
