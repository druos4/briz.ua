import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Inertia } from "@inertiajs/inertia";
import Layout from "../Shared/Layout";
import { keepTrackCurrentPage } from "../actions/actions";
import { useIsSsr } from '../hooks/useIsSsr';
import { isIos } from '../utils';

const Politica = (props) => {
    const isSsr = useIsSsr();
    const ios = !isSsr && isIos();

    useEffect(() => {
        props.keepTrackCurrentPage(Inertia.page.component);
    }, []);

    return (
        <div className="politica wrapper">
            <h1
                className={
                    ios
                        ? "page-title politica__page-title politica__page-title--ios"
                        : "page-title politica__page-title"
                }
            >
                Политика конфиденциальности приложений Briz
            </h1>
            <section className="politica__connection-section">
                <div className="politica__description">
                    <p>
                        Мы стремимся обеспечить конфиденциальность пользователей
                        и безопасность наших сервисов Briz SMART TV, BrizTV и
                        Мой Briz.
                    </p>
                    <p>
                        Данные приложения работают только в сети телекомпании
                        БРИЗ.
                    </p>
                    <p>
                        Мы используем логин и пароль от учетной записи абонента.
                    </p>
                    <p>
                        Это позволяет идентифицировать пользователя услуги IPTV
                        и предоставить ему доступ
                    </p>
                    <p>к просмотру IPTV каналов в мобильном приложении.</p>
                    <p>
                        Наши приложения, ни при каких обстоятельствах, не
                        публикуют идентификационные данные пользователя.
                    </p>
                </div>
            </section>
        </div>
    );
};

Politica.layout = (page) => <Layout title="Privacy Policy">{page}</Layout>;

Politica.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
};
Politica.defaultProps = {
    keepTrackCurrentPage: () => {},
};

export default connect(null, { keepTrackCurrentPage })(Politica);
