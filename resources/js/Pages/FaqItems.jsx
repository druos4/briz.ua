/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-danger */
/* eslint-disable no-return-assign */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { Inertia } from "@inertiajs/inertia";
import {InertiaHead, InertiaLink} from "@inertiajs/inertia-react";
import Autocomplete, {
    createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Scrollspy from "react-scrollspy";
import { keepTrackCurrentPage, savePartLink } from "../actions/actions";
import Layout from "../Shared/Layout";
import Tabset from "../Shared/Tabset/Tabset";
import MobileDropdownFaq from "../Shared/MobileWidgets/MobileDropdownFaq";
import { useIsSsr } from '../hooks/useIsSsr';
import { isIos } from '../utils';

const windowAvailWidth = typeof window !== 'undefined' && window.screen.availWidth;

const filter = createFilterOptions();

const CssTextField = withStyles({

    // input2: {
    //   height: 10
    // },

    root: {
        "& input": {
            height: (windowAvailWidth < 768 && 7) || (windowAvailWidth >= 768 && 15),
        },

        '&:hover input::placeholder': {
            color: "#424242",
            opacity: 1,
            transition: ".2s ease-in",
        },

        '& .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"] .MuiAutocomplete-input':
        {
            paddingLeft: "35px",
        },

        "& label.Mui-focused": {
            color: "#00a3a4",
            fontSize: "16px",
        },

        "& .MuiInputLabel-outlined": {
            transform: windowAvailWidth < 768 ? "translate(14px, 14px) scale(1)" : "translate(14px, 18px) scale(1)",
        },

        "& .MuiInputLabel-outlined.MuiInputLabel-shrink": {
            transform: "translate(14px, -7px) scale(0.75)",
        },

        "&:hover .MuiInputLabel-formControl": {
            color: "#424242",
            transition: ".2s ease-in",
        },

        "& .MuiOutlinedInput-root": {
            "& fieldset": {
                borderColor: "#B8B8B8",
                borderRadius: "10px",
            },
            "&:hover fieldset": {
                borderColor: "#424242",
                color: "#424242",
                transition: ".2s ease-in",
            },
            "&.Mui-focused fieldset": {
                borderColor: "#00a3a4",
                borderRadius: "10px",
            },
        },
    },
})(TextField);

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    margin: {
        margin: 0
    },
}));
const FaqItems = ({ questions, stickyHeader, keepTrackCurrentPage, items, partLinkFaq, savePartLink, meta }) => {
    const { t } = useTranslation();
    const isSsr = useIsSsr();
    const classes = useStyles();
    const containerRef = React.useRef();

    const ls = typeof localStorage !== 'undefined' ? localStorage.getItem('i18nextLng') : 'ua';
    const lang = !isSsr ? app.getLang() : ls;

    // const windowAvailWidthInner = !isSsr && window.screen.availWidth;

    const [valueQuestion, setValueQuestion] = useState(null);
    const [currentTab, setTab] = useState('');
    //mobile
    const [defaultSelectedQuestion, setDefaultQuestion] = useState({});
    const [activeItemLink, setActiveItemLink] = useState("");
    const [selectedQuestion, setSelectedQuestion] = useState({});

    const ios = !isSsr && isIos();

    const uniqueQuestions = questions.filter(question => items.items.every(item => item.slug !== question.slug));

    // mobile, решение до реализации ssr
    // if (
    //     !isSsr &&
    //     window.screen.availWidth < 1025 && !!partLinkFaq.length
    //     ) {
    //     // setTimeout(() => window.location.href = Inertia.page.url + "#" + partLinkFaq, 1000)
    //     // window.location.href = Inertia.page.url + "#" + partLinkFaq;
    // }
    // else if (
    //         !isSsr &&
    //         window.screen.availWidth < 1025 &&
    //         !partLinkFaq.length &&
    //         !isSsr &&
    //         !!window.location.search.length
    //     ) {

    //     const parsedUri = decodeURI(window.location.search);
    //     const tabItemFromURI = parsedUri.replace('?', '');
    //     window.location.href = items.url + "#" + tabItemFromURI;
    // }

    useEffect(() => {
        // // mobile
        if (
            window.screen.availWidth < 1025 && !!partLinkFaq.length
        ) {
            // setTimeout(() => window.location.href = Inertia.page.url + "#" + partLinkFaq, 1000)
            // window.location.href = Inertia.page.url + "#" + partLinkFaq;
        }
        else if (
            window.screen.availWidth < 1025 &&
            !partLinkFaq.length &&
            !!window.location.search.length
        ) {

            const parsedUri = decodeURI(window.location.search);
            const tabItemFromURI = parsedUri.replace('?', '');
            window.location.href = items.url + "#" + tabItemFromURI;
        }
    });

    // const handleVisitFaq = () => {
    //     Inertia.visit(lang !== 'ua' ? `/${lang}/faq` : "/faq", { component: Faq });
    // };

    useEffect(() => {
        let cleanupFunction = false;

        keepTrackCurrentPage(Inertia.page.component);

        const title = document.querySelector("title");
        title.textContent = 'FAQ | ' + items.title;

        // mobile
        if (window.screen.availWidth < 1025 && !!partLinkFaq.length) {
            items.items.map(item => item.slug === partLinkFaq ? setDefaultQuestion(item) : null);
        } else if (window.screen.availWidth < 1025 && !!window.history.state && window.history.state.url.length && window.history.state.url.includes('#') && !partLinkFaq.length) {

            const decodedUrlMobile = decodeURI(window.history.state.url);
            const strNeedReplace = items.url + '#';
            const parsedDecodedUrlMobile = decodedUrlMobile.replace(strNeedReplace, '');

            items.items.map(item => item.slug === parsedDecodedUrlMobile ? setDefaultQuestion(item) : null);
            window.location.href = items.url + "#" + parsedDecodedUrlMobile;
        } else {
            setDefaultQuestion(items.items[0]);
        }
        // desctop
        if (window.screen.availWidth > 1024 && !!partLinkFaq.length) {
            items.items.map(item => {
                const { origin } = window.location;
                item.slug === partLinkFaq ? setTab(item.slug) : null;

                const currentLang = lang !== 'ua' ? `/${lang}/` : "/";
                Inertia.visit(`${origin}${currentLang}faq/${items.slug}?${partLinkFaq}`)

            });
        } else if (window.screen.availWidth > 1024 && !!window.location.search.length) {

            // items.items.map(item => decodeURI(window.location.search).includes(item.title) && setTab(item.title) )
            const parsedUri = window.location.search;
            const tabItemFromURI = parsedUri.replace('?', '');

            setTab(tabItemFromURI);

        } else if (window.history.state && window.history.state.url.length > items.url.length && !currentTab.length) {
            const decodedUrl = window.history.state.url;
            const str = items.url + '#';
            const parsedDecodedUrl = decodedUrl.replace(str, '');

            setTab(parsedDecodedUrl);
        } else if (window.screen.availWidth > 1024 && !currentTab.length) {
            setTab(items.items[0].slug)
        }

        // if (window.screen.availWidth > 1024) {
        //     window.addEventListener('popstate', handleVisitFaq);
        // }

        return () => {
            setValueQuestion(null);
            setTab('');
            setDefaultQuestion({});
            setActiveItemLink("");
            setSelectedQuestion({});
            if (window.screen.availWidth > 1025) {
                savePartLink('');
            }

            // window.removeEventListener('popstate', handleVisitFaq);

            cleanupFunction = true;
        };

    }, []);

    useEffect(() => {
        if (activeItemLink.length) {
            setTimeout(() => setActiveItemLink(""), 500);
        }
    }, [activeItemLink]);

    const handleScroll = (e) => {
        if (e && e.id) {
            setActiveItemLink(e.id);
        }

        if (e && e.id === partLinkFaq) {
            setTimeout(() => {
                savePartLink('');
            }, 1000);
        } else {

        }

        document.documentElement.style.scrollBehavior = "smooth";
    };

    const handleChangeQuestion = (e) => {
        if (!uniqueQuestions.length) {
            setValueQuestion({ val: e.target.value });
        }
    };

    const getSelectedQuestion = (selected) => {
        setSelectedQuestion(selected);
    };

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta-description") &&*/}
            {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

            {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
            {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}
            {/*</InertiaHead>*/}

            <div className="faq-item">
                <div className="wrapper">
                    <section className="faq-item__section-top home__section-top-tariffs">
                        <div className="faq-item__top-container">
                            <div className="faq-item__top-back-search">
                                <div className="faq-item__back">
                                    <span className="faq-item__arrow-icon" />
                                    <a href={lang !== 'ua' ? `/${lang}/faq` : "/faq"} className="faq-item__back-link">{t('toBack')}</a>
                                </div>
                                <div className="faq-item__search-autocomplete-container faq__search-autocomplete-container">
                                    <Autocomplete
                                        id="questions"
                                        freeSolo
                                        options={uniqueQuestions.map(question => question)}
                                        getOptionLabel={(option) => {
                                            if (typeof option === 'string') {
                                                return option;
                                            }

                                            if (option.inputValue) {
                                                return option.inputValue;
                                            }

                                            return option.title ? option.title : '';
                                        }}

                                        className="faq__autocomplete-search-question"
                                        renderInput={(params) => <CssTextField
                                            required={true}
                                            {...params}
                                            onChange={handleChangeQuestion}
                                            className={classes.margin}
                                            placeholder={t('cannotFindAnswer')}
                                            variant="outlined"
                                            name="question"
                                            type="text"
                                            inputProps={{ ...params.inputProps, maxLength: 99 }}
                                            onFocus={(e) => e.target.placeholder = ''}
                                            onBlur={(e) => e.target.placeholder = t('cannotFindAnswer')}
                                        />
                                        }

                                        value={valueQuestion}
                                        onChange={(event, newValue) => {
                                            if (!!newValue && typeof newValue === 'object') {
                                                const url = `${newValue.url}`;
                                                if (window.location.pathname !== url) {
                                                    savePartLink(newValue.slug);
                                                    Inertia.visit(url);
                                                }

                                                setValueQuestion(newValue);
                                            }
                                        }}
                                        filterOptions={(options, params) => {
                                            return filter(options, params);
                                        }}
                                        selectOnFocus
                                        clearOnBlur
                                        handleHomeEndKeys
                                    />
                                </div>
                            </div>
                            <span className="faq-item__dropdown channels__dropdown">
                                <MobileDropdownFaq
                                    defaultText={defaultSelectedQuestion}
                                    optionsList={items.items}
                                    getSelectedChannel={getSelectedQuestion}
                                />
                            </span>
                            <h1 className="page-title faq-item__page-title">{items.title}</h1>
                        </div>

                        <div className="faq-item__tabset-container question-item">
                            <Tabset currentTab={currentTab} setTab={setTab}>
                                {/* {
                                    items.items.map((question) => {
                                        return (
                                            <QuestionItem
                                                key={question.id}
                                                title={question.title}
                                                answer={question.answer}
                                                id={question.slug}
                                            />
                                        )
                                    })
                                } */}
                            </Tabset>
                        </div>

                        <div className="faq-item__item-questions-container">
                            {
                                !isSsr && window.screen.availWidth < 1025 && !!items.items.length ?
                                    <div className="channels__flex faq-item__flex">
                                        <div className="channels__left-list-titles faq-item__left-list-titles">
                                            <Scrollspy
                                                items={items.items.map(item => item.slug)}
                                                offset={-200}
                                                currentClassName="is-active"
                                                onUpdate={(e) => handleScroll(e)}
                                                className={

                                                    !!stickyHeader.stickyHeader ? "channels__left-list faq-item__left-list--top-container-point" : "channels__left-list"
                                                }
                                            />
                                        </div>

                                        <ul
                                            className={ios ? "faq-item__right-list-categories faq-item__right-list-categories--ios" : "faq-item__right-list-categories"}
                                            ref={containerRef}
                                        >
                                            {
                                                items.items.map((channel) => {
                                                    return (

                                                        <li
                                                            key={channel.slug}
                                                            className="faq-item__right-item-category"
                                                        >
                                                            <span
                                                                id={channel.slug}
                                                                style={{ visibility: "hidden" }}
                                                                className="faq-item__tech-id-block"
                                                            >{channel.id}</span>

                                                            <div
                                                                className={
                                                                    activeItemLink === channel.slug ?
                                                                        "faq-item__right-category-title faq-item__right-category-title--active"
                                                                        :
                                                                        "faq-item__right-category-title"
                                                                }
                                                            >{channel.title}</div>
                                                            <div dangerouslySetInnerHTML={{ __html: channel.answer }} className="faq-item__right-inner-text" />
                                                        </li>
                                                    )
                                                })
                                            }
                                        </ul>
                                    </div>
                                    :
                                    null
                            }
                        </div>
                    </section>
                </div>
            </div>
        </>
    )
};

const mapStateToProps = state => {
    return {
        stickyHeader: state.stickyHeader,
        partLinkFaq: state.partLinkFaq.partLink,
    };
};

FaqItems.layout = (page) => <Layout title="Faq item">{page}</Layout>;

FaqItems.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    savePartLink: PropTypes.func,
    setModalAccept: PropTypes.func,
    questions: PropTypes.arrayOf(PropTypes.object),
    items: PropTypes.shape({
        items: PropTypes.arrayOf(PropTypes.object),
        url: PropTypes.string,
        title: PropTypes.string,
        slug: PropTypes.string,
    }),
    partLinkFaq: PropTypes.string,
    stickyHeader: PropTypes.shape({
        stickyHeader: PropTypes.bool,
    }),
};
FaqItems.defaultProps = {
    keepTrackCurrentPage: () => {},
    savePartLink: () => {},
    setModalAccept: () => {},
    questions: [],
    items: {},
    partLinkFaq: "",
    stickyHeader: {},
};

export default connect(mapStateToProps, { keepTrackCurrentPage, savePartLink })(
    FaqItems
);

