/* eslint-disable camelcase */
/* eslint-disable no-shadow */
/* eslint-disable react/no-danger */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { InertiaLink } from "@inertiajs/inertia-react";
import axios from "axios";
import { useTranslation } from "react-i18next";
import { Inertia } from "@inertiajs/inertia";
import Layout from "../Shared/Layout";
import { setModalAccept, keepTrackCurrentPage } from "../actions/actions";
import { PRODUCT_PRODUCT } from "../constants/apiParams";
import { useMediaQuery } from "../Shared/MediaQuery/useMediaQuery";
import ActionsButton from "../Shared/ui/buttons/ActionsButton";
import { useIsSsr } from "../hooks/useIsSsr";
import { getLocale, getLocaleHref, isIos, isSafari } from "../utils";
import { InertiaHead } from '@inertiajs/inertia-react';

const Actions = (props) => {
    const {
        actions,
        has_more,
        product,
        setModalAccept,
        meta,
        keepTrackCurrentPage,
        locale
    } = props
    const isSsr = useIsSsr();
    const { t } = useTranslation();
    const safari = !isSsr && isSafari();
    const ios = !isSsr && isIos();
    const currentLocale = getLocale(locale)
    const currentLocaleHref = getLocaleHref(locale)

    const [retrivedActions, setActions] = useState({
        retrivedActions: actions,
    });
    const [currentPage, setCurrentPage] = useState(1);
    const [hasMore, setMoreLoad] = useState(has_more);

    const isWidthMobile = useMediaQuery("(min-width: 360px)");
    const isWidthTablet = useMediaQuery("(min-width: 768px)");
    const isWidthDesktop = useMediaQuery("(min-width: 1025px)");

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        if (meta && !!Object.keys(meta).length) {
            const title = document.querySelector("title");
            title.textContent = meta["meta-title"];
        }
    }, []);

    const handleAppareModal = (productTitle, id) => {
        // Открытие модального окна с передачей в него данных

        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle,
            productId: id,
            titleModal: t("purchaseRequest"),
            titleButton: t("sendRequest"),
            apiUrl: PRODUCT_PRODUCT.url,
            typeProduct: PRODUCT_PRODUCT.type,
            titleProduct: "",
            seoId: PRODUCT_PRODUCT.type,
            body: { [PRODUCT_PRODUCT.type]: id, ghost: "" },
            keyFormRedux: "default",
        });
    };

    const handleLoadMore = async () => {
        await axios
            .get(`${currentLocaleHref}/actions/get-more-item?page=${currentPage + 1}`)
            .then((response) => {
                if (typeof response.data === 'object' && Object.keys(response.data).length) {
                    setActions((prevState) => ({
                        retrivedActions: [
                            ...prevState.retrivedActions,
                            ...response.data.actions,
                        ],
                    }));
                    setMoreLoad(response.data.has_more);
                    setCurrentPage(currentPage + 1);
                }
            });
    };

    const keyhandleLoadMore = (e) => {
        if (e.keyCode === 13) {
            handleLoadMore();
        }
    };

    const firstAction = actions[0];
    // Преобразованный заголовок для отображения иконки стрелки при переносе на другую строку
    const splitTitle = product && product.title.split(" ");
    const lastWord = splitTitle && splitTitle[splitTitle.length - 1];
    const newTitle =
        splitTitle && splitTitle.filter((item) => item !== lastWord).join(" ");

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta-description") &&*/}
            {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

            {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
            {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <div className="actions wrapper">
                <h1
                    className={
                        ios
                            ? "page-title actions__page-title actions__page-title--ios"
                            : "page-title actions__page-title"
                    }
                >
                    {t("promotions")}
                </h1>
                <section className="actions__all-actions">
                    <a
                        className="actions__preview-card-link"
                        href={firstAction.url}
                    >
                        <div className="actions__preview-action actions__preview-action-top50">
                            <img
                                className="actions__preview-img"
                                src={
                                    (isWidthDesktop &&
                                        firstAction.picture_phone) ||
                                    (isWidthTablet &&
                                        firstAction.picture_tablet) ||
                                    (isWidthMobile &&
                                        firstAction.picture_phone) ||
                                    firstAction.picture_phone
                                }
                                alt={`${firstAction.title} - інтернет-провайдер Briz в Одесі`}
                                title={firstAction.title}
                            />
                            <div className="actions__preview-description">
                                <div className="actions__card-title actions__card-title--first">
                                    {firstAction.title}
                                </div>
                                <span className="actions__card-date">
                                    {firstAction.published}
                                </span>
                                <p
                                    className="actions__product-anounce"
                                    dangerouslySetInnerHTML={{
                                        __html: firstAction.anons
                                            ? firstAction.anons
                                            : "",
                                    }}
                                />
                            </div>
                        </div>
                    </a>
                    <ul
                        className={
                            ios
                                ? "actions__cards-list actions__cards-list--ios actions__cards-list--no-scroll actions__cards-list-recomended"
                                : "actions__cards-list actions__cards-list--no-scroll actions__cards-list-recomended"
                        }
                    >
                        {retrivedActions.retrivedActions
                            .filter((item, index) => index > 0)
                            .map((action) => {
                                return (
                                    <li
                                        className="actions__card-item actions__card-item-recomended"
                                        key={action.id}
                                    >
                                        <a
                                            className="actions__card-link"
                                            href={action.url}
                                        >
                                            <img
                                                src={action.thumbnail}
                                                alt={`${action.title} - інтернет-провайдер Briz в Одесі`}
                                                title={action.title}
                                                className="actions__card-img actions__card-img-recomended"
                                            />

                                            {/* <span className="actions__card-img" style={{backgroundImage: `url(http://localhost:3000${action.picture})`}} /> */}
                                            <div className="actions__card-bottom-content">
                                                <div className="actions__card-title">
                                                    {action.title}
                                                </div>
                                                <span className="actions__card-date">
                                                    {action.published}
                                                </span>
                                            </div>
                                        </a>
                                    </li>
                                );
                            })}
                    </ul>
                    {hasMore ? (
                        <span
                            role="button"
                            tabIndex="0"
                            className="actions__cards-show"
                            onClick={handleLoadMore}
                            onKeyDown={(e) => keyhandleLoadMore(e)}
                        >
                            {t("showMore")}
                            <span className="actions__cards-arrow-icon" />
                        </span>
                    ) : null}
                </section>
            </div>

            {product && (
                <section className="section actions__section">
                    <div className="page-subtitle actions__page-subtitle wrapper">
                        {product.category_title}
                    </div>
                    <p className="tariff-page__opportunity-txt actions__opportunity-txt wrapper">
                        {product.category_text}
                    </p>
                    <ul className="actions__smart-console consoles">
                        <li className="consoles__item actions__console-item">
                            <div
                                className={
                                    ios || safari
                                        ? "consoles__item-block consoles__item-block--ios wrapper consoles__item-block--even"
                                        : "consoles__item-block wrapper consoles__item-block--even"
                                }
                            >
                                <div className="consoles__item-details">
                                    <div className="consoles__item-title">
                                        {/* <a href={product.url} className="consoles__item-link">
                                            <span>{newTitle}&#32;</span>
                                            <span className="consoles__item-link-last-word">{lastWord}</span>
                                        </a> */}
                                        <a
                                            href={`${currentLocaleHref}/equipment/product/${product.slug}`}
                                            className="consoles__item-link"
                                        >
                                            <span>{newTitle}&#32;</span>
                                            <span className="consoles__item-link-last-word">
                                                {" "}
                                                {lastWord}
                                            </span>
                                        </a>
                                        {/* <span className="consoles__item-icon" /> */}
                                    </div>
                                    <p
                                        className="consoles__item-description"
                                        dangerouslySetInnerHTML={{
                                            __html: product.anons
                                                ? product.anons
                                                : "",
                                        }}
                                    />
                                    <div className="consoles__item-order">
                                        <span className="consoles__item-price">
                                            {product.price}{" "}
                                            <span
                                                className={
                                                    currentLocale === "en"
                                                        ? "consoles__currency"
                                                        : ""
                                                }
                                            >
                                                {t("UAH")}
                                            </span>
                                        </span>
                                        <ActionsButton
                                            handler={() =>
                                                handleAppareModal(
                                                    product.title,
                                                    product.id
                                                )
                                            }
                                            titleButton={t("toOrder")}
                                        />
                                    </div>
                                </div>
                                {/* <span className="consoles__item-picture consoles__item-picture--mecool" /> */}
                                <img
                                    src={product.picture}
                                    alt={`${product.title} - інтернет-провайдер Briz в Одесі`}
                                    title={product.title}
                                    className="consoles__img"
                                />
                            </div>
                        </li>
                    </ul>
                </section>
            )}
        </>
    );
};

Actions.layout = (page) => <Layout title="Actions">{page}</Layout>;

Actions.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    setModalAccept: PropTypes.func,
    actions: PropTypes.arrayOf(PropTypes.object),
    has_more: PropTypes.bool,
    product: PropTypes.shape({
        title: PropTypes.string,
        id: PropTypes.number,
        picture: PropTypes.string,
        anons: PropTypes.string,
        category_title: PropTypes.string,
        category_text: PropTypes.string,
        price: PropTypes.number,
        slug: PropTypes.string,
    }),
    meta: PropTypes.shape({
        title: PropTypes.string,
        "meta-title": PropTypes.string,
        "meta-keywords": PropTypes.string,
        "meta-description": PropTypes.string,
    }),
    locale: PropTypes.string
};
Actions.defaultProps = {
    keepTrackCurrentPage: () => {
    },
    setModalAccept: () => {
    },
    actions: [],
    has_more: false,
    product: {},
    meta: {},
    locale: "ua"
};

export default connect(null, { setModalAccept, keepTrackCurrentPage })(Actions);
