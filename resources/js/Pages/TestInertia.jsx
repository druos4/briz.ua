import React from 'react';
import Layout from '../Shared/Layout';
import axios from 'axios';
import { currentLang } from "../utils";

const TestInertia = () => {
    
    const handleClick = async () => {
        try {
            await axios
                .post(`${currentLang()}/test`)
                .then((res) => {
                    console.log(res, 'DONE');
                })
                .catch((e) => console.log(e.message));
        } catch (e) {
            console.log(e.message);
        }
    }

    return (
        <div className="test-inertia" onClick={() => handleClick()}>TEST INERTIA</div>
    )
};

TestInertia.layout = (page) => <Layout children={page} title={'test inertia'} />

export default TestInertia;