/* eslint-disable no-shadow */
import React, {useEffect} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Inertia} from "@inertiajs/inertia";
import {useTranslation} from "react-i18next";
import Layout from "../../Shared/Layout";
import HelpCart from "../../Shared/HelpCart";
import {keepTrackCurrentPage, setModalAccept} from "@/actions/actions";
import {QUESTION_CALLME} from "@/constants/apiParams";
import ContactsServiceSupport from "../../Shared/HelpWidgets/ContactsServiceSupport";
import ToUp from "../../Shared/ToUp";
import {InertiaHead} from '@inertiajs/inertia-react';
import {useIsSsr} from '@/hooks/useIsSsr';

const Help = (props) => {
    const isSsr = useIsSsr();

    const {meta, themes, keepTrackCurrentPage, setModalAccept} = props

    const {t} = useTranslation();

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);
        if (meta && !!Object.keys(meta).length) {
            const pageTitle = document.querySelector("title");
            // тут мета приходит с meta-title -

            pageTitle.textContent = meta["meta-title"];
        }
        //
        // if (meta) {
        //     const metaDescription = document.querySelector(
        //         'meta[name="description"]'
        //     );
        //     const metaKeywords = document.querySelector(
        //         'meta[name="keywords"]'
        //     );
        //
        //     // metaDescription.content = meta["meta_description"];
        //     // metaKeywords.content = meta["meta_keywords"];
        //
        //     metaDescription.content = "  metaDescription.content";
        //
        //     metaKeywords.content = " metaKeywords.content";
        // }
    }, []);

    const handleAppareModal = () => {
        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle: "",
            productId: "",
            titleModal: t("callMeBack"),
            titleButton: t("send"),
            apiUrl: QUESTION_CALLME.url,
            typeProduct: QUESTION_CALLME.type,
            titleProduct: "",
            body: {ghost: ""},
            keyFormRedux: "default",
        });
    };

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta-description") &&*/}
            {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

            {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
            {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <ToUp scrollStepInPx="50" delayInMs="5"/>
            <span className="help-feedback-modal"/>
            <div className="help-page">
                <div className="wrapper">
                    <section className="section help-page-section">
                        <h1 className="page-title help-page__title">
                            {t("help")}
                        </h1>
                        <ul className="help-faq__list">
                            {themes.map((element) => {
                                return (
                                    <li
                                        key={element.id}
                                        className="help-faq__item"
                                    >
                                        <HelpCart
                                            icon={element.icon}
                                            title={element.title}
                                            anons={element.anons}
                                            url={element.url}
                                            uniqId={element.id}
                                        />
                                    </li>
                                );
                            })}
                        </ul>
                    </section>
                </div>

                <section className="help-request">
                    <div className="wrapper">
                        <div className="help-request__container">
                            <div className="page-title help-request__title">
                                {t("helpRequestTitle")}
                            </div>
                            <p className="help-request__text">
                                {t("helpRequestText")}
                            </p>
                            <button
                                type="button"
                                onClick={(e) => handleAppareModal(e)}
                                className="help-request__btn custom-button"
                            >
                                {t("helpRequestButton")}
                            </button>
                        </div>
                    </div>
                </section>
                <div className="wrapper">
                    <div className="help-request__support">
                        <ContactsServiceSupport/>
                    </div>
                </div>
            </div>
        </>
    );
};

Help.layout = (page) => <Layout title="Help">{page}</Layout>;

Help.propTypes = {
    meta: PropTypes.shape({
        title: PropTypes.string,
        "meta-title": PropTypes.string,
    }),
    themes: PropTypes.arrayOf(PropTypes.object),
    keepTrackCurrentPage: PropTypes.func,
    setModalAccept: PropTypes.func,
};
Help.defaultProps = {
    meta: {},
    themes: [],
    keepTrackCurrentPage: () => {
    },
    setModalAccept: () => {
    },
};

export default connect(null, {
    keepTrackCurrentPage,
    setModalAccept,
})(Help);
