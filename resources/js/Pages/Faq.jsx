/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-shadow */
/* eslint-disable no-unused-expressions */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import Autocomplete, {
    createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import {
    keepTrackCurrentPage,
    savePartLink,
    setModalAccept,
} from "@/actions/actions";
import { QUESTION_CALLME } from "@/constants/apiParams";
import Layout from "../Shared/Layout";
import {InertiaHead} from '@inertiajs/inertia-react';
import {useIsSsr} from '@/hooks/useIsSsr';

const filter = createFilterOptions();

const windowAvailWidth = typeof window !== 'undefined' && window.screen.availWidth;

const CssTextField = withStyles({
    // input2: {
    //   height: 10
    // },

    root: {
        "& input": {
            height:
                (windowAvailWidth < 768 && 7) || (windowAvailWidth >= 768 && 15),
        },

        "&:hover input::placeholder": {
            color: "#424242",
            opacity: 1,
            transition: ".2s ease-in",
        },

        '& .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"] .MuiAutocomplete-input':
            {
                paddingLeft: "35px",
            },

        "& label.Mui-focused": {
            color: "#00a3a4",
            fontSize: "16px",
        },

        "& .MuiInputLabel-outlined": {
            transform:
                windowAvailWidth < 768 ?
                    "translate(14px, 14px) scale(1)"
                    :
                    "translate(14px, 18px) scale(1)",
        },

        "& .MuiInputLabel-outlined.MuiInputLabel-shrink": {
            transform: "translate(14px, -7px) scale(0.75)",
        },

        "&:hover .MuiInputLabel-formControl": {
            color: "#424242",
            transition: ".2s ease-in",
        },

        "& .MuiOutlinedInput-root": {
            "& fieldset": {
                borderColor: "#B8B8B8",
                borderRadius: "10px",
                // borderRadius: window.screen.availWidth < 1025 ? "10px" : "10px 0 0 10px",
                // borderRight: window.screen.availWidth < 1025 ? "1px solid #B8B8B8" : "none",
            },
            "&:hover fieldset": {
                borderColor: "#424242",
                color: "#424242",
                transition: ".2s ease-in",
            },
            "&.Mui-focused fieldset": {
                borderColor: "#00a3a4",
                borderRadius: "10px",
                // borderRadius: window.screen.availWidth < 1025 ? "10px" : "10px 0 0 10px",
                // borderRight: window.screen.availWidth < 1025 ? "2px solid #00a3a4" : "none",
            },
        },
    },
})(TextField);

const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexWrap: "wrap",
    },
    margin: {
        margin: 0,
    },
}));

const Faq = (props) => {
    const {
        meta,
        questionsFullList,
        items,
        setModalAccept,
        keepTrackCurrentPage,
        savePartLink,
    } = props
    const isSsr = useIsSsr();
    const { t } = useTranslation();
    const classes = useStyles();
    const [valueQuestion, setValueQuestion] = useState(null);

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        if (meta && !!Object.keys(meta).length) {
            const title = document.querySelector("title");
            title.textContent = meta["meta-title"];
        }

        return () => {
            window.screen.availWidth > 1025 ? savePartLink("") : null;
        };
    }, []);

    const handleChangeQuestion = (e) => {
        if (!questionsFullList.length) {
            setValueQuestion({ val: e.target.value });
        }
    };

    const handleAppareModal = () => {
        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle: "",
            productId: "",
            titleModal: t("callMeBack"),
            titleButton: t("send"),
            apiUrl: QUESTION_CALLME.url,
            typeProduct: QUESTION_CALLME.type,
            titleProduct: "",
            body: { ghost: "" },
            keyFormRedux: "default",
        });
    };

    const handleFocus = (e) => {
        e.target.placeholder = "";
    };

    const handleBlur = (e) => {
        e.target.placeholder = t("faqSearchLabel");
    };

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta-description") &&*/}
            {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

            {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
            {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <span className="faq-feedback-modal" />
            <div className="faq">
                <div className="wrapper">
                    <section className="faq__section-top home__section-top-tariffs">
                        <h1 className="page-title faq__page-title">
                            {t("frequentlyAskedQuestions")}
                        </h1>
                        <div className="faq__search-question-container">
                            <div className="faq__search-autocomplete-container">
                                <Autocomplete
                                    id="question"
                                    freeSolo
                                    options={questionsFullList.map(
                                        (question) => question
                                    )}
                                    getOptionLabel={(option) => {
                                        if (typeof option.title === "string") {
                                            return option.title;
                                        }

                                        if (option.inputValue) {
                                            return option.inputValue;
                                        }

                                        if (option?.title) {
                                            return option.title;
                                        }

                                        return "";
                                    }}
                                    className="faq__autocomplete-search-question"
                                    renderInput={(params) => (
                                        <CssTextField
                                            required
                                            {...params}
                                            onChange={(e) =>
                                                handleChangeQuestion(e)
                                            }
                                            className={classes.margin}
                                            placeholder={t("faqSearchLabel")}
                                            variant="outlined"
                                            name="question"
                                            type="text"
                                            inputProps={{
                                                ...params.inputProps,
                                                maxLength: 99,
                                            }}
                                            onFocus={(e) => handleFocus(e)}
                                            onBlur={(e) => handleBlur(e)}
                                        />
                                    )}
                                    value={valueQuestion}
                                    onChange={(event, newValue) => {
                                        if (typeof newValue === "string") {
                                            setValueQuestion({
                                                title: newValue,
                                            });
                                        } else if (
                                            newValue &&
                                            newValue.inputValue
                                        ) {
                                            setValueQuestion({
                                                title: newValue.inputValue,
                                            });
                                        } else {
                                            if (newValue) {
                                                setValueQuestion(newValue);
                                                const url = `${newValue.url}`;
                                                savePartLink(
                                                    `${newValue.slug}`
                                                );

                                                Inertia.visit(url);
                                            }

                                            setValueQuestion(newValue);
                                        }
                                    }}
                                    filterOptions={(options, params) => {
                                        return filter(options, params);
                                    }}
                                    selectOnFocus
                                    clearOnBlur
                                    handleHomeEndKeys
                                />
                            </div>
                        </div>
                        <ul className="faq__tiles-list">
                            {!!items.length &&
                                items.map((tile) => {
                                    return (
                                        <li
                                            className="faq__tile-item"
                                            key={tile.id}
                                        >
                                            <a
                                                className="faq__tile-link"
                                                href={tile.url}
                                            >
                                                <img
                                                    className="faq__tile-pic"
                                                    src={tile.icon}
                                                    alt={`${tile.title !== null ? tile.title : ''} - інтернет-провайдер Briz в Одесі`}
                                                    title={tile.title}
                                                />
                                                <span className="faq__tile-name">
                                                    {tile.title}
                                                </span>
                                            </a>
                                        </li>
                                    );
                                })}
                        </ul>
                    </section>
                </div>
                <div className="faq__no-match-question">
                    <div className="faq__no-match-question-container">
                        <div className="faq__no-match-question-title">
                            {t("cannotFindQuestion")}
                        </div>
                        <div className="faq__no-match-question-description">
                            <p>{t("cannotFindQuestionDescription")}</p>
                            <p>{t("cannotFindQuestionDescription2")}</p>
                        </div>
                        <button
                            type="button"
                            className="faq__no-match-question-btn custom-button"
                            onClick={() => handleAppareModal()}
                        >
                            {t("send")}
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};

Faq.layout = (page) => <Layout title="Faq">{page}</Layout>;

Faq.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    savePartLink: PropTypes.func,
    setModalAccept: PropTypes.func,
    meta: PropTypes.shape({
        "meta-title": PropTypes.string,
    }),
    questionsFullList: PropTypes.arrayOf(PropTypes.object),
    items: PropTypes.arrayOf(PropTypes.object),
};
Faq.defaultProps = {
    keepTrackCurrentPage: () => {},
    savePartLink: () => {},
    setModalAccept: () => {},
    meta: {},
    questionsFullList: [],
    items: [],
};
export default connect(null, {
    setModalAccept,
    keepTrackCurrentPage,
    savePartLink,
})(Faq);
