/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable no-shadow */
/* eslint-disable react/no-danger */
import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { InertiaLink } from "@inertiajs/inertia-react";
import { useTranslation } from "react-i18next";
import { connect } from "react-redux";
import { Inertia } from "@inertiajs/inertia";
import Layout from "../Shared/Layout";
import { useMediaQuery } from "@/Shared/MediaQuery/useMediaQuery";
import { setModalAccept, keepTrackCurrentPage } from "@/actions/actions";
import { PRODUCT_CONNECT_ACTION_SERVICE } from "@/constants/apiParams";
import ActionDetailsButton from "../Shared/ui/buttons/ActionDetailsButton";
import { useIsSsr } from "../hooks/useIsSsr";
import { useHandleTables } from "../hooks/useHandleTables";
import {InertiaHead} from '@inertiajs/inertia-react';
import { isIos } from "../utils";

const NewDetail = ({ news, setModalAccept, keepTrackCurrentPage }) => {
    const isSsr = useIsSsr();
    const { t } = useTranslation();

    const wrapperElement = useRef(null);
    useHandleTables(wrapperElement);

    const ios = !isSsr && isIos();

    const isWidthMobile = useMediaQuery("(min-width: 360px)");
    const isWidthTablet = useMediaQuery("(min-width: 768px)");
    const isWidthDesktop = useMediaQuery("(min-width: 1025px)");

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);

        const title = document.querySelector("title");
        title.textContent = news.title;
    }, []);

    const handleAppareModal = () => {
        // Открытие модального окна с передачей в него данных

        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle: news.title,
            productId: news.id,
            titleModal: t("connectionRequest"),
            titleButton: t("send"),
            apiUrl: PRODUCT_CONNECT_ACTION_SERVICE.url,
            typeProduct: PRODUCT_CONNECT_ACTION_SERVICE.type,
            titleProduct: "",
            seoId: PRODUCT_CONNECT_ACTION_SERVICE.type,
            body: { id: news.id, ghost: "" },
            keyFormRedux: "default",
        });
    };

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {news.hasOwnProperty("title") && <title>{news.title}</title>}*/}

            {/*    {news.hasOwnProperty("meta_description") &&*/}
            {/*    <meta name="description" content={news["meta_description"]}/>}*/}

            {/*    {news.hasOwnProperty("meta_keywords") && <meta name="keywords" content={news["meta_keywords"]}/>}*/}
            {/*    {news.hasOwnProperty("meta_title") && <meta name="og:title" content={news["meta_title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}
            {/*</InertiaHead>*/}

            <div className="actions actions__detail wrapper">
                <div className="actions__card">
                    <div className="actions__card-block">
                        <div className="actions__card-top">
                            <img
                                className="actions__card-img"
                                src={news.picture_tablet}
                                src={
                                    (isWidthDesktop && news.picture) ||
                                    (isWidthTablet && news.picture_tablet) ||
                                    (isWidthMobile && news.picture_phone) ||
                                    ""
                                }
                                alt={`${news.title} - інтернет-провайдер Briz в Одесі`}
                                title={news.title}
                            />
                        </div>
                        <div className="actions__card-bottom">
                            {/* <div className="actions__card-title">{action.title}</div> */}
                            {/* <p className="actions__card-description">{action.meta_description}</p> */}
                            <h1 className="actions__card-title actions__card-title--detail">
                                {news.title}
                            </h1>
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: news.detail,
                                }}
                                className="actions__card-detail"
                                ref={wrapperElement}
                            />

                            {news?.can_connect ? (
                                <ActionDetailsButton
                                    handler={() => handleAppareModal()}
                                    titleButton={t("toPlugBtn")}
                                />
                            ) : null}
                        </div>
                    </div>
                </div>
                {news?.recomends.length ? (
                    <section className="section actions__detail-section">
                        <div className="actions__product-wrapper">
                            <div className="page-subtitle wrapper">
                                {t("recommended")}
                            </div>
                            <ul
                                className="actions__cards-list actions__cards-list-detail actions__cards-list-recomended"
                                style={
                                    ios
                                        ? {
                                              columnGap: "initial",
                                              rowGap: "initial",
                                          }
                                        : { columnGap: "30px", rowGap: "50px" }
                                }
                            >
                                {news?.recomends.map((item) => {
                                    return (
                                        <li
                                            className={
                                                ios
                                                    ? "actions__card-item actions__card-item--ios actions__card-item-recomended"
                                                    : "actions__card-item actions__card-item-recomended"
                                            }
                                            key={item.id}
                                        >
                                            <a
                                                className="actions__card-link"
                                                href={item.url}
                                            >
                                                <div className="actions__card-img-container">
                                                    <img
                                                        src={item.thumbnail}
                                                        alt={`${item.title} - інтернет-провайдер Briz в Одесі`}
                                                        title={item.title}
                                                        className="actions__card-img actions__card-img-recomended"
                                                    />
                                                </div>

                                                <div className="actions__card-bottom-content">
                                                    <div className="actions__card-title">
                                                        {item.title}
                                                    </div>
                                                    <span className="actions__card-date">
                                                        {item.published}
                                                    </span>
                                                </div>
                                            </a>
                                        </li>
                                    );
                                })}
                            </ul>
                        </div>
                    </section>
                ) : null}
            </div>
        </>
    );
};

NewDetail.layout = (page) => <Layout title="Action item">{page}</Layout>;

NewDetail.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    setModalAccept: PropTypes.func,
    news: PropTypes.shape({
        id: PropTypes.number,
        recomends: PropTypes.arrayOf(PropTypes.object),
        can_connect: PropTypes.number,
        detail: PropTypes.string,
        published: PropTypes.string,
        title: PropTypes.string,
        picture: PropTypes.string,
        picture_tablet: PropTypes.string,
        picture_phone: PropTypes.string,
    }),
};
NewDetail.defaultProps = {
    keepTrackCurrentPage: () => {},
    setModalAccept: () => {},
    news: {},
};

export default connect(null, { setModalAccept, keepTrackCurrentPage })(
    NewDetail
);
