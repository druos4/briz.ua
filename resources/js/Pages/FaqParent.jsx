import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { Inertia } from "@inertiajs/inertia";
import { InertiaLink, InertiaHead } from "@inertiajs/inertia-react";
import Autocomplete, {
    createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import {
    keepTrackCurrentPage
} from "@/actions/actions";
import Layout from "../Shared/Layout";
import { useIsSsr } from '@/hooks/useIsSsr';
import FaqChild from './FaqChild';
import MobileDropdownFaq from "../Shared/MobileWidgets/MobileDropdownFaq";
import { motion, AnimateSharedLayout } from "framer-motion";

const windowAvailWidth = typeof window !== 'undefined' && window.screen.availWidth;

const filter = createFilterOptions();

const CssTextField = withStyles({
    root: {
        '& input': {
            height: (windowAvailWidth < 768 && 7) || (windowAvailWidth >= 768 && 15),
        },

        "&:hover input::placeholder": {
            color: "#424242",
            opacity: 1,
            transition: ".2s ease-in",
        },

        '& .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"] .MuiAutocomplete-input': {
            paddingLeft: "35px"
        },

        '& label.Mui-focused': {
            color: '#00a3a4',
            fontSize: "16px",
        },

        '& .MuiInputLabel-outlined': {
            transform: windowAvailWidth < 768 ? 'translate(14px, 14px) scale(1)' : 'translate(14px, 18px) scale(1)'
        },

        '& .MuiInputLabel-outlined.MuiInputLabel-shrink': {
            transform: 'translate(14px, -7px) scale(0.75)'
        },

        '&:hover .MuiInputLabel-formControl': {
            color: '#424242',
            transition: '.2s ease-in'
        },

        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: '#B8B8B8',
                borderRadius: "10px"
            },
            '&:hover fieldset': {
                borderColor: '#424242',
                color: '#424242',
                transition: '.2s ease-in'
            },
            '&.Mui-focused fieldset': {
                borderColor: '#00a3a4',
                borderRadius: "10px"
            },
        },
    },
})(TextField);

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    margin: {
        margin: 0
    },
}));

const FaqParent = ({ answer, item, keepTrackCurrentPage, questions, questionsFullList }) => {
    const { t } = useTranslation();
    const isSsr = useIsSsr();
    const classes = useStyles();
    const ls = typeof localStorage !== 'undefined' ? localStorage.getItem('i18nextLng') : 'ua';
    const lang = !isSsr ? app.getLang() : ls;
    const uniqueQuestions = questionsFullList.filter(question => questions.every(item => item.slug !== question.slug));

    const [valueQuestion, setValueQuestion] = useState(null);

    const handleChangeQuestion = (e) => {
        if (!uniqueQuestions.length) {
            setValueQuestion({ val: e.target.value });
        }
    };

    useEffect(() => {
        setValueQuestion(null);
    });

    useEffect(() => {
        keepTrackCurrentPage(Inertia.page.component);
    }, []);

    const handleClickLink = (e, question) => {
        Inertia.get(question.url, {}, { preserveState: true });
    }

    return (
        <>
            {
                answer === undefined ?
                    <InertiaHead>
                        <meta name="keywords" content={item.meta_keywords} />
                        <meta name="og:title" content={item.meta_title} />
                        <meta property="og:url" content={item.url} />
                    </InertiaHead>
                    :
                    null
            }
            <div className="faq-item">
                <div className="wrapper">
                    <section className="faq-item__section-top home__section-top-tariffs">
                        <div className="faq-item__top-container">
                            <div className="faq-item__top-back-search">
                                <div className="faq-item__back">
                                    <span className="faq-item__arrow-icon" />
                                    <a href={lang !== 'ua' ? `/${lang}/faq` : "/faq"} className="faq-item__back-link">{t('toBack')}</a>
                                </div>
                                <div className="faq-item__search-autocomplete-container faq__search-autocomplete-container">
                                    <Autocomplete
                                        id="questions"
                                        freeSolo
                                        options={uniqueQuestions.map(question => question)}
                                        getOptionLabel={(option) => {
                                            if (typeof option === 'string') {
                                                return option;
                                            }

                                            if (option.inputValue) {
                                                return option.inputValue;
                                            }

                                            return option.title ? option.title : '';
                                        }}

                                        className="faq__autocomplete-search-question"
                                        renderInput={(params) => <CssTextField
                                            required={true}
                                            {...params}
                                            onChange={handleChangeQuestion}
                                            className={classes.margin}
                                            placeholder={t('cannotFindAnswer')}
                                            variant="outlined"
                                            name="question"
                                            type="text"
                                            inputProps={{ ...params.inputProps, maxLength: 99 }}
                                            onFocus={(e) => e.target.placeholder = ''}
                                            onBlur={(e) => e.target.placeholder = t('cannotFindAnswer')}
                                        />
                                        }

                                        value={valueQuestion}
                                        onChange={(event, newValue) => {
                                            if (!!newValue && typeof newValue === 'object') {
                                                const url = `${newValue.url}`;
                                                if (window.location.pathname !== url) {
                                                    Inertia.get(url, {}, { preserveState: true });
                                                }

                                                setValueQuestion(newValue);
                                            }
                                        }}
                                        filterOptions={(options, params) => {
                                            return filter(options, params);
                                        }}
                                        selectOnFocus
                                        clearOnBlur
                                        handleHomeEndKeys
                                    />
                                </div>
                            </div>
                            <span className="faq-item__dropdown channels__dropdown">
                                <MobileDropdownFaq
                                    defaultText={valueQuestion}
                                    optionsList={questions}
                                    answer={answer}
                                />
                            </span>
                        </div>
                        <div className="faq-item__tabset-container question-item">
                            <div className="faq-parent">
                                <div className="faq-parent__question-list-container">
                                    <AnimateSharedLayout transition={{ duration: 0.5 }}>
                                        <ul className="faq-parent__question-list">
                                            {
                                                questions.map((question) => {
                                                    return (
                                                        <motion.li key={question.id} className="faq-parent__question-item" animate>
                                                            <span
                                                                className={question.current ? "faq-parent__question-link faq-parent__question-link--active" : "faq-parent__question-link"}
                                                                onClick={(e) => handleClickLink(e, question)}
                                                            >
                                                                {
                                                                question.current
                                                                &&
                                                                <motion.div
                                                                    className="sideline"
                                                                    layoutId="sideline"
                                                                    style={{
                                                                        backgroundColor: "#00A3A4",
                                                                    }}
                                                                    animate
                                                                />
                                                            }
                                                                {question.title}
                                                            </span>
                                                        
                                                        </motion.li>
                                                    )
                                                })
                                            }
                                        </ul>
                                    </AnimateSharedLayout>
                                </div>

                                <div className="faq-parent__answer-content">
                                    {
                                        !answer && <div className="faq-parent__default-description">
                                            <h1 className="faq-child__answer-title">{item?.title}</h1>
                                            <div
                                                dangerouslySetInnerHTML={{ __html: item?.detail }}
                                                className="faq-child__answer-content"
                                            />
                                        </div>
                                    }
                                    {
                                        !!answer && <FaqChild content={answer} />
                                    }
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </>
    )
};

FaqParent.layout = page => <Layout children={page} title={'Faq Parent'} />

FaqParent.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    questions: PropTypes.arrayOf(PropTypes.object),
    answer: PropTypes.object,
    item: PropTypes.object,
    questionsFullList: PropTypes.arrayOf(PropTypes.object)
};
FaqParent.defaultProps = {
    keepTrackCurrentPage: () => { },
    questions: [],
    answer: undefined,
    item: {},
    questionsFullList: []
};
export default connect(null, {
    keepTrackCurrentPage
})(FaqParent);
