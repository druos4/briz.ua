/* eslint-disable react/forbid-prop-types */
import React, {useEffect} from "react";
import PropTypes from "prop-types";
import {useTranslation} from "react-i18next";
import {connect} from "react-redux";
import {Inertia} from "@inertiajs/inertia";
import {keepTrackCurrentPage} from "@/actions/actions";
import Layout from "../Shared/Layout";
import ModalForm from "../Shared/Forms/ModalForm";
import {QUESTION_CALLME} from "@/constants/apiParams";
import {isIos, isSafari} from "@/utils";
import {useIsSsr} from "@/hooks/useIsSsr";
import {InertiaHead} from '@inertiajs/inertia-react';

const Contacts = (props) => {

    const {meta} = props

    const isSsr = useIsSsr();
    const {t} = useTranslation();
    const safari = !isSsr && isSafari();
    const ios = !isSsr && isIos();

    useEffect(() => {
        props.keepTrackCurrentPage(Inertia.page.component);

        if (
            Object.prototype.hasOwnProperty.call(props, "meta") &&
            !!Object.keys(props.meta).length
        ) {
            const title = document.querySelector("title");
            title.textContent = props.meta["meta-title"];
        }
    }, []);

    return (
        <>
            {/*<InertiaHead>*/}

            {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

            {/*    {meta.hasOwnProperty("meta-description") &&*/}
            {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

            {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
            {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
            {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

            {/*</InertiaHead>*/}

            <div className="contacts wrapper">
                <h1
                    className={
                        ios || safari
                            ? "page-title contacts__page-title contacts__page-title--ios"
                            : "page-title contacts__page-title"
                    }
                >
                    {t("contacts")}
                </h1>
                <div className="page-subtitle contacts__page-subtitle">
                    {t("support")}
                </div>
                <section className="contacts__connection-section">
                    <div
                        className="contacts__lists"
                        itemScope
                        itemType="http://schema.org/Organization"
                    >
                        <span itemProp="name" hidden>
                            Briz
                        </span>
                        <ul
                            className={
                                ios || safari
                                    ? "contacts__list contacts__list--ios"
                                    : "contacts__list"
                            }
                        >
                            <li className="contacts__list-item">
                                <div className="contacts__small-title">
                                    {t("forHome")}
                                </div>
                                <ul className="contacts__list-inner">
                                    <li className="contacts__list-inner-item">
                                        <a
                                            className="contacts__contact-link contacts__contact-icon--it"
                                            itemProp="telephone"
                                            href="tel:0487972525"
                                        >
                                            048 797 25 25
                                        </a>
                                    </li>
                                    <li className="contacts__list-inner-item">
                                        <a
                                            className="contacts__contact-link contacts__contact-icon--it"
                                            href="tel:0487291122"
                                            itemProp="telephone"
                                        >
                                            048 729 11 22
                                        </a>
                                    </li>
                                    <li className="contacts__list-inner-item">
                                        <a
                                            className="contacts__contact-link contacts__contact-link--lifecell"
                                            href="tel:0937972525"
                                            itemProp="telephone"
                                        >
                                            093 797 25 25
                                        </a>
                                    </li>
                                    <li className="contacts__list-inner-item">
                                        <a
                                            className="contacts__contact-link contacts__contact-link--kyivstar"
                                            href="tel:0687972525"
                                            itemProp="telephone"
                                        >
                                            068 797 25 25
                                        </a>
                                    </li>
                                    <li className="contacts__list-inner-item">
                                        <a
                                            className="contacts__contact-link contacts__contact-link--circle"
                                            href="tel:0957972525"
                                            itemProp="telephone"
                                        >
                                            095 797 25 25
                                        </a>
                                    </li>
                                    <li className="contacts__list-inner-item">
                                        <a
                                            className="contacts__contact-link contacts__contact-link--mailto"
                                            href="mailto:supp@briz.ua"
                                            itemProp="email"
                                        >
                                            supp@briz.ua
                                        </a>
                                    </li>
                                </ul>
                                <p className="contacts__item-note">
                                    {t("sevenDaysWeek")}
                                </p>
                            </li>
                            <li
                                className={
                                    ios || safari
                                        ? "contacts__list-item contacts__list-item--ios"
                                        : "contacts__list-item"
                                }
                            >
                                <div className="contacts__small-title">
                                    {t("forBusiness")}
                                </div>
                                <ul className="contacts__list-inner">
                                    <li className="contacts__list-inner-item">
                                        <a
                                            className="contacts__contact-link contacts__contact-icon--it"
                                            href="tel:0487291077"
                                            itemProp="telephone"
                                        >
                                            048 729 10 77
                                        </a>
                                    </li>
                                    <li className="contacts__list-inner-item">
                                        <a
                                            className="contacts__contact-link contacts__contact-icon--it"
                                            href="tel:0487972929"
                                            itemProp="telephone"
                                        >
                                            048 797 29 29
                                        </a>
                                    </li>
                                    <li className="contacts__list-inner-item">
                                        <a
                                            className="contacts__contact-link contacts__contact-icon--it"
                                            href="tel:0487972323"
                                            itemProp="telephone"
                                        >
                                            048 797 23 23
                                        </a>
                                    </li>
                                    {/* <li className="contacts__list-inner-item">
                                    <a className="contacts__contact-link contacts__contact-icon--it" href="tel:0487972323">048 797 28 28 <span>({t('fax')})</span></a>
                                </li> */}
                                    <li className="contacts__list-inner-item">
                                        <a
                                            className="contacts__contact-link contacts__contact-link--mailto"
                                            href="mailto:vipclient@briz.ua"
                                            itemProp="email"
                                        >
                                            vipclient@briz.ua
                                        </a>
                                    </li>
                                </ul>
                                <p className="contacts__item-note">
                                    {t("monFri")} 9:00-18:00
                                </p>
                            </li>
                        </ul>

                        <ul className="contacts__list contacts__list--second-section">
                            <li className="contacts__list-item contacts__list-item--margin-toright">
                                <div className="page-subtitle contacts__small-title">
                                    <span className="page-subtitle__defis"/>
                                    <span className="page-subtitle__text">
                                        {t("office")}
                                    </span>
                                </div>
                                <address
                                    className="contacts__address"
                                    itemProp="address"
                                    itemScope
                                    itemType="http://schema.org/PostalAddress"
                                >
                                    <p className="contacts__address-schedule">
                                        {t("MonSat")} <span>9:00 - 19:00</span>
                                    </p>
                                    <p>{t("streetAddressOffice")}</p>
                                    <p itemProp="streetAddress" hidden>
                                        вул. Генерала Бочарова, 2
                                    </p>
                                    <p>
                                        <span>{t("cityAddreddOffice")}</span>
                                        <span itemProp="addressLocality" hidden>
                                            Одеса
                                        </span>
                                        , <span>{t("postIndex")}</span>
                                        <span itemProp="postalCode" hidden>
                                            65035
                                        </span>
                                    </p>
                                    <p>{t("countryOffice")}</p>
                                    <p itemProp="addressLocality" hidden>
                                        Україна
                                    </p>
                                </address>
                            </li>
                        </ul>
                    </div>
                    <div className="contacts__connection-form">
                        <ModalForm
                            titleModal={t("haveQuestion")}
                            type="contacts"
                            titleButton={t("sendRequest")}
                            apiUrl={QUESTION_CALLME.url}
                            typeProduct={QUESTION_CALLME.type}
                            body={{ghost: ""}}
                            keyFormRedux="contacts"
                        />
                    </div>
                </section>
            </div>
        </>
    );
};

Contacts.layout = (page) => <Layout title="contacts">{page}</Layout>;

Contacts.propTypes = {
    keepTrackCurrentPage: PropTypes.func,
    meta: PropTypes.any,
};
Contacts.defaultProps = {
    keepTrackCurrentPage: () => {
    },
    meta: {},
};

export default connect(null, {keepTrackCurrentPage})(Contacts);
