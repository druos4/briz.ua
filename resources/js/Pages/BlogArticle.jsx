/* eslint-disable no-shadow */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/forbid-prop-types */
import React, { useState, useEffect } from "react";
import Layout from "../Shared/Layout";
import axios from 'axios';
import { keepTrackCurrentPage, setLikeBlogArticle } from "@/actions/actions";
import { InertiaLink } from "@inertiajs/inertia-react";
import { Inertia } from "@inertiajs/inertia";
import { connect } from "react-redux";
import bgItemPath from "../../images/blog/blog_banner.png";
import imgEmailArticle from '../../images/blog/img-email-article.png';
import { motion, AnimateSharedLayout } from "framer-motion";
import BlogSidebar from '../Shared/BlogParts/BlogSidebar';
import BlogSubscribeEmail from '../Shared/BlogParts/BlogSubscribeEmail';
import BlogSendArticleModal from '../Shared/BlogParts/BlogSendArticleModal';
import BlogArticleComments from '../Shared/BlogParts/BlogArticleComments';
import { useTranslation } from "react-i18next";
import { categoryTitleCompare, getLocale, getLocaleHref, isIos } from "../utils";
import BlogGmailModal from '../Shared/BlogParts/BlogGmailModal';
import ToUp from "@/Shared/ToUp";
import { Link as ScrollLink} from "react-scroll";

const variants = {
    open: {
        transition: { staggerChildren: 0.07, delayChildren: 0.2 }
    },
    closed: {
        transition: { staggerChildren: 0.05, staggerDirection: -1 }
    }
};

const BlogArticle = (props) => {
    const { t } = useTranslation();
    const { blog, categories, recomends, keepTrackCurrentPage, setLikeBlogArticle, likeValue, locale, isShowingHeader } = props;
    const [isOpen, setIsOpen] = useState(false);
    const [isRunSocialsModal, setIsRunSocialsModal] = useState(false);
    const [isRunGmailModal, setIsRunGmailModal] = useState(false);
    const [likeIsSelected, setLikeIsSelected] = useState(false);

    const currentLocaleHref = getLocaleHref(locale)
    const currentLocale = getLocale(locale)

    useEffect(() => {
        setLikeBlogArticle(blog.likes_count);
        setLikeIsSelected(blog.like_is_setted);
        keepTrackCurrentPage(Inertia.page.component);
    }, []);

    const handleAppareModal = () => {
        setIsRunSocialsModal(true);
    };

    const callBackCloseModal = (flag) => {
        setIsRunSocialsModal(flag);
    };

    const callBackGmailModal = (flag) => {
        setIsRunGmailModal(flag);
    }

    const callBackCloseGmailModal = (flag) => {
        setIsRunGmailModal(flag);
    };

    const handleLike = async () => {
        try {
            await axios
                .post(`${currentLocaleHref}/blog/like`, { blog: blog.id })
                .then((res) => {
                    if (res.data.success) {
                        setLikeIsSelected(res.data.success);
                        setLikeBlogArticle(likeValue + 1);
                    }
                })
                .catch((e) => console.log(e.message));
        } catch (e) {
            console.log(e.message);
        }
    };

    return (
        <>
            {isRunSocialsModal && <BlogSendArticleModal callBackCloseModal={callBackCloseModal} callBackGmailModal={callBackGmailModal} />}
            {isRunGmailModal && <BlogGmailModal callBackCloseGmailModal={callBackCloseGmailModal} />}
            <div className="wrapper blog-cart">
                <ToUp scrollStepInPx="50" delayInMs="5"/>
                <div className="blog-cart__container">
                    <div className="blog-cart__basic-wrapper">
                        <section className="blog-cart__basic-block">
                            <h1 className="page-title blog-cart__page-title">
                                {blog.title.replaceAll('-', '\u2011')}
                            </h1>
                            <span className="blog-cart__read-time">
                                {t('toRead')} {blog.read_time} {t('min')}
                            </span>
                            <div className="blog-cart__img-block">
                                <img
                                    src={blog.picture}
                                    alt={`${blog.title} - інтернет-провайдер Briz в Одесі`}
                                    title={blog.title}
                                    className="blog-cart__img"
                                />
                                <div className="blog-cart__share-bg-icon" onClick={() => handleAppareModal()}>
                                    <span className="blog-cart__share-icon" />
                                </div>
                            </div>
                            <div className="blog-cart__content-wrapper">
                                <div className="blog-cart__info-container">
                                    <span className="blog-cart__info-date">{blog.published_at}</span>
                                    <div className="blog-cart__info-block">
                                        <span
                                            className="blog-cart__info-text blog-cart__info-text-like"
                                            onClick={() => handleLike()}
                                        >
                                            <span
                                                className={
                                                    likeIsSelected ?
                                                        "blog-cart__info-icon blog-cart__info-like blog-cart__info-like--isLike"
                                                        :
                                                        "blog-cart__info-icon blog-cart__info-like"
                                                }
                                            /> {likeValue}
                                        </span>
                                        <span className="blog-cart__info-text">
                                            <span className="blog-cart__info-icon blog-cart__info-comments" /> {blog.comments_count}
                                        </span>
                                        <span className="blog-cart__info-text">
                                            <span className="blog-cart__info-icon blog-cart__info-views" /> {blog.views_count}
                                        </span>
                                    </div>
                                </div>
                                <div className="blog-cart__thematic-links">
                                    {
                                        Boolean(blog.categories) && Boolean(blog.categories.length) && blog.categories
                                            .sort(categoryTitleCompare)
                                            .map((category, index) => {
                                            return (
                                                <a href={category.url} className="blog-cart__thematic-link" key={index}>{category.title}</a>
                                            )
                                        })
                                    }
                                </div>
                                {
                                    Boolean(blog.titles.length)
                                    &&
                                    <div className="blog-cart__content-article">
                                        <div className={
                                            isOpen ?
                                                "blog-cart__content-article-control blog-cart__content-article-control--is-open"
                                                :
                                                "blog-cart__content-article-control"
                                            }
                                        >
                                            <span className="blog-cart__content-article-text">{t('content')}</span>
                                            <span className="blog-cart__content-article-toggle" onClick={() => setIsOpen(!isOpen)}>{isOpen ? "Закрыть" : "Открыть"}</span>
                                        </div>


                                        <AnimateSharedLayout>
                                            <motion.ol
                                                // layout
                                                className={
                                                    isOpen ?
                                                        "blog-cart__content-article-list blog-cart__content-article-list--is-open"
                                                        :
                                                        "blog-cart__content-article-list"
                                                }
                                            >
                                                {
                                                    isOpen && blog.titles.map((item, index) => {
                                                        return (
                                                            <motion.li
                                                                className="blog-cart__content-article-item"
                                                                key={index}
                                                                layout
                                                                initial={{ opacity: 0 }}
                                                                animate={{ opacity: 1 }}
                                                            >
                                                                <ScrollLink
                                                                    to={item.ankor}
                                                                    smooth={true}
                                                                    offset={-70}
                                                                    className="blog-cart__content-article-link"
                                                                >
                                                                    {item.title}
                                                                </ScrollLink>
                                                            </motion.li>
                                                        )
                                                    })
                                                }
                                            </motion.ol>
                                        </AnimateSharedLayout>
                                    </div>
                                }
                                <div className="blog-cart__incoming-content" dangerouslySetInnerHTML={{ __html: blog.detail }} />
                            </div>
                        </section>
                        <section className="blog-cart__basic-block">
                            <div className="blog-cart__wrapper-subscribe-email">
                                <BlogSubscribeEmail
                                    imgSource={imgEmailArticle}
                                    title={blog.title}
                                    currentLocaleHref={currentLocaleHref}
                                />
                            </div>
                        </section>
                        <section className="blog-cart__comments">
                            <BlogArticleComments 
                                blogId={blog.id}
                                comments_count={blog.comments_count}
                            />
                        </section>
                    </div>
                    <BlogSidebar recomended={recomends} currentLocale={currentLocale} isShowingHeader={isShowingHeader}/>
                </div>
            </div>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        likeValue: state.blogStuff.like,
        isShowingHeader: state.stickyHeader.stickyHeader
    }
};

BlogArticle.layout = (page) => <Layout title="BlogArticle">{page}</Layout>;

export default connect(mapStateToProps, { keepTrackCurrentPage, setLikeBlogArticle })(BlogArticle);
