import React, { useEffect, useState } from "react";
import { Provider } from "react-redux";
import store from "../store";
import Header from "../Shared/Ott/Header";
import Aside from "../Shared/Ott/Aside";
import Subscribing from "../Shared/Ott/Subscribing";
import TopBanner from "../Shared/Ott/TopBanner";
import InfoBlock from "../Shared/Ott/InfoBlock";
import Tabs from "../Shared/Ott/Tabs";
import Footer from "../Shared/Ott/Footer";
import Modal from "../Shared/ModalWindow";
import WithLangProvider from "../Shared/hoc/WithLangProvider";
import {
    televisonSources,
    filmsSources,
    serialsSources,
    cartoonsSources,
    cartoons,
    logoStudios,
    plugBrizTVSources,
    watchBrizTVSources,
    tabsSources,
    television,
    films,
    serials,
    tabs,
    connectBrizTv,
    watchBrizTV,
    footerContent,
} from "@/Shared/Ott/ottData";
import {InertiaHead} from '@inertiajs/inertia-react';
import { useIsSsr } from '../hooks/useIsSsr';

const html = (typeof window !== 'undefined') && document.getElementsByTagName("html")[0];

const Ott = (props) => {
    const {meta} = props;
    const isSsr = useIsSsr();
    const [isShow, setIsShow] = useState(false);
    const handleOpenSideBar = () => {
        setIsShow((prev) => !prev);
    };

    useEffect(() => {
        document.documentElement.style.scrollBehavior = "smooth";
        return () => {
            document.documentElement.style.scrollBehavior = "auto";
        };
    }, []);

    useEffect(() => {
        if (html.nodeName === 'HTML') {
            html.style.overflow = `${isShow ? "hidden" : "initial"}`;
            html.style.position = `${isShow ? "hidden" : "initial"}`;
            html.style["touch-action"] = `${isShow ? "none" : "initial"}`;
        }
    }, [isShow]);

    return (
       <>
           {/*<InertiaHead>*/}

           {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

           {/*    {meta.hasOwnProperty("meta-description") &&*/}
           {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

           {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
           {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
           {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

           {/*</InertiaHead>*/}

           <Provider store={store}>
               <div className="ott-main-bg">
                   <Header show={handleOpenSideBar} isShow={isShow} />
                   <Aside show={setIsShow} isShow={isShow} />
                   <main className="ott-main-container">
                       <TopBanner id="aboutService" />
                       <InfoBlock
                           sources={televisonSources}
                           content={television}
                           imgSide="left"
                           blockBg
                           id="ottTv"
                           pt="padding-top"
                           className="ott-info__tv"
                       />
                       <Subscribing
                           content={watchBrizTV}
                           sources={watchBrizTVSources}
                           imgSide="right"
                       />
                       <InfoBlock
                           sources={filmsSources}
                           content={films}
                           imgSide="left"
                           id="ottFilms"
                           decorationClass="ott-info__decor-right ott-info__decor-right-icon"
                           pt="padding-top"
                       />
                       <InfoBlock
                           sources={serialsSources}
                           content={serials}
                           imgSide="right"
                           id="ottSerials"
                           decorationClass="ott-info__decor-left"
                       />
                       <InfoBlock
                           sources={cartoonsSources}
                           content={cartoons}
                           imgSide="left"
                           logo={logoStudios}
                           id="ottCartoons"
                           decorationClass="ott-info__decor-right"
                       />
                       <Tabs
                           id="ottDevices"
                           content={tabs}
                           sources={tabsSources}
                       />
                       <Subscribing
                           content={connectBrizTv}
                           sources={plugBrizTVSources}
                           imgSide="left"
                       />
                   </main>
                   <Footer content={footerContent} />
                   <div id="modal-root" />
                   <Modal />
               </div>
           </Provider>
       </>
    );
};

const OttWithLangProvider = WithLangProvider(Ott);

export default OttWithLangProvider;
