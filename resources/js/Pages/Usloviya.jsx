import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import Layout from "../Shared/Layout";
import {InertiaHead} from '@inertiajs/inertia-react';
import { useIsSsr } from '../hooks/useIsSsr';

const Usloviya = (props) => {
    const { meta: { detail, title } } = props
    const meta = props.meta
    const isSsr = useIsSsr();
    const { t } = useTranslation();

    return (
      <>
          {/*<InertiaHead>*/}

          {/*    {meta.hasOwnProperty("title") && <title>{meta.title}</title>}*/}

          {/*    {meta.hasOwnProperty("meta-description") &&*/}
          {/*    <meta name="description" content={meta["meta-description"]}/>}*/}

          {/*    {meta.hasOwnProperty("meta-keywords") && <meta name="keywords" content={meta["meta-keywords"]}/>}*/}
          {/*    {meta.hasOwnProperty("meta-title") && <meta name="og:title" content={meta["meta-title"]}/>}*/}
          {/*    <meta property="og:url"content={ !isSsr ? location.pathname : ""}/>*/}

          {/*</InertiaHead>*/}

          <div className="usloviya wrapper">
              <section className="section section__usloviya">
                  <h1 className="page-title usloviya__page-title">
                      {title || t("usloviya")}
                  </h1>
                  <div className="usloviya-section">
                      <div className="usloviya__description">
                          <div
                              // eslint-disable-next-line react/no-danger
                              dangerouslySetInnerHTML={{
                                  __html: detail || "",
                              }}
                          />
                      </div>
                  </div>
              </section>
          </div>
      </>
    );
};

Usloviya.layout = (page) => <Layout title="Usloviya">{page}</Layout>;
Usloviya.propTypes = {
    meta: PropTypes.shape({
        "meta-title": PropTypes.string,
        title: PropTypes.string,
        detail: PropTypes.string,
    }),
};
Usloviya.defaultProps = {
    meta: {},
};

export default Usloviya;
