import React from "react";
import { useTranslation } from "react-i18next";

const OttNav = () => {
    const { t } = useTranslation();
    return (
        <nav className="ott-nav">
            <ul className="ott-nav__list">
                <li className="ott-nav__item">
                    <a href="#aboutService">{t("ott.aboutServ")}</a>
                </li>
                <li className="ott-nav__item">
                    <a href="#ottTv">{t("ott.tv")}</a>
                </li>
                <li className="ott-nav__item">
                    <a href="#ottFilms">{t("ott.films")}</a>
                </li>
                <li className="ott-nav__item">
                    <a href="#ottSerials">{t("ott.serials")}</a>
                </li>
                <li className="ott-nav__item">
                    <a href="#ottCartoons">{t("ott.cartoons")}</a>
                </li>
                <li className="ott-nav__item">
                    <a href="#ottDevices">{t("ott.gadgets")}</a>
                </li>
            </ul>
        </nav>
    );
};

export default OttNav;
