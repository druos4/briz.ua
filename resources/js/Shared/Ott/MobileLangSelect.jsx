import React, { useState, useEffect } from "react";
import ButtonLangSidebar from "../ui/buttons/ButtonLangSidebar";
import PropTypes from "prop-types";

const MobileLangSelect = ({isRefreshPage}) => {
    const [currentLang, setCurrentLang] = useState(null);

    useEffect(() => {
        setCurrentLang(document.getElementsByTagName("html")[0].lang);
    }, []);

    const handleOptionClick = (e, lang) => {
        if (isRefreshPage) {
            window.location.href = `/lang/${lang}`;
        }
        localStorage.setItem("i18nextLng", lang);
    };
    return (
        <div className="ott-mobile-select">
            <ButtonLangSidebar
                handler={(e) => handleOptionClick(e, "ua")}
                customClass={`ott-mobile-select__button ${
                    currentLang === "ua"
                        ? "ott-mobile-select__button-active"
                        : ""
                }`}
                titleButton="UA"
            />

            <ButtonLangSidebar
                handler={(e) => handleOptionClick(e, "ru")}
                customClass={`ott-mobile-select__button ${
                    currentLang === "ru"
                        ? "ott-mobile-select__button-active"
                        : ""
                }`}
                titleButton="RU"
            />

            <ButtonLangSidebar
                handler={(e) => handleOptionClick(e, "en")}
                customClass={`ott-mobile-select__button ${
                    currentLang === "en"
                        ? "ott-mobile-select__button-active"
                        : ""
                }`}
                titleButton="ENG"
            />
        </div>
    );
};

MobileLangSelect.propTypes = {
    isRefreshPage: PropTypes.bool
};
MobileLangSelect.defaultProps = {
    isRefreshPage:true
};

export default MobileLangSelect;
