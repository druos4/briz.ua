import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import Contacts from "./Contacts";
import ModalForm from "../Forms/ModalForm";
import { QUESTION_CALLME } from "../../constants/apiParams";

const Footer = ({ content }) => {
    const { t } = useTranslation();
    return (
        <footer className="ott-footer">
            <div className="ott-footer__contact-wrap">
                <div className="ott-info__title">{t(content.title)}</div>
                <Contacts inFooter />
            </div>
            <ModalForm
                titleModal={t("haveQuestion")}
                type="ottForm"
                titleButton={t("sendRequest")}
                apiUrl={QUESTION_CALLME.url}
                typeProduct={QUESTION_CALLME.type}
                body={{ ghost: "" }}
                keyFormRedux="default"
            />
        </footer>
    );
};

Footer.propTypes = {
    content: PropTypes.shape({
        title: PropTypes.string,
    }),
};
Footer.defaultProps = {
    content: {},
};
export default Footer;
