import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import { useIsSsr } from "../../hooks/useIsSsr";
import Picture from "./ui/picture";
import { useMediaQuery } from "../MediaQuery/useMediaQuery";

const Subscribing = ({ sources, content, imgSide }) => {
    const isSsr = useIsSsr();
    const { t } = useTranslation();
    // eslint-disable-next-line no-undef
    const lang = !isSsr && app.getLang();
    const isWidthDesktop = useMediaQuery("(min-width: 560px)");

    const [ref, inView] = useInView({
        triggerOnce: true,
        rootMargin: `${isWidthDesktop ? "-300" : "-100"}px`,
        initialInView: false,
    });
    const variantsLeft = {
        visible: { opacity: inView ? 1 : 0, x: inView ? 0 : 300 },
        hidden: { opacity: 0, x: 300 },
    };
    const variantsRight = {
        visible: { opacity: inView ? 1 : 0, x: inView ? 0 : -300 },
        hidden: { opacity: 0, x: -300 },
    };
    return (
        <section
            className={`ott-subscribing ${
                imgSide === "left" ? "ott-subscribing-left" : ""
            }`}
            ref={ref}
        >
            {imgSide === "left" && (
                <Picture isAnimated variant={variantsRight} sources={sources} />
            )}
            <motion.div
                className="ott-subscribing__content"
                initial="hidden"
                animate="visible"
                variants={imgSide === "left" ? variantsLeft : variantsRight}
                transition={{ duration: 1.5 }}
                key={content.descriptionText}
            >
                <h2 className="ott-info__title">
                    {t(content.titleConnectBrizTV)}
                </h2>
                <p className="ott-info__item-subtitle ott-subscribing__text">
                    {t(content.descriptionText)}
                </p>
                <p className="ott-info__item-subtitle ott-subscribing__text">
                    {t(content.descriptionSubText)}
                </p>
                <a
                    href={`https://stat.briz.ua/?lang=${lang}`}
                    rel="nofollow"
                    className="ott-subscribing__link"
                >
                    {t(content.plug)}
                </a>
                <p className="ott-info__item-subtitle ott-subscribing__subtext">
                    {t(content.descriptionAccess)}
                </p>
            </motion.div>
            {imgSide === "right" && (
                <Picture isAnimated variant={variantsLeft} sources={sources} />
            )}
        </section>
    );
};

Subscribing.propTypes = {
    sources: PropTypes.shape({
        "1x": PropTypes.string,
        "2x": PropTypes.string,
        alt: PropTypes.string,
    }),
    content: PropTypes.shape({
        titleConnectBrizTV: PropTypes.string,
        descriptionText: PropTypes.string,
        descriptionSubText: PropTypes.string,
        plug: PropTypes.string,
        descriptionAccess: PropTypes.string,
    }),
    imgSide: PropTypes.string,
};
Subscribing.defaultProps = {
    sources: {
        "1x": "",
        "2x": "",
        alt: "",
    },
    content: {
        titleConnectBrizTV: "",
        descriptionText: "",
        descriptionSubText: "",
        plug: "",
        descriptionAccess: "",
    },
    imgSide: "",
};

export default Subscribing;
