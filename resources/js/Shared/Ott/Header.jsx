import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import ottLogo from "../../../images/ott-logo.svg";
import ottLogo360 from "../../../images/ott-logo-360.svg";
import OttLangSelect from "./LangSelect";
import { useMediaQuery } from "../MediaQuery/useMediaQuery";
import OttNav from "./Nav";
import { useIsSsr } from "../../hooks/useIsSsr";

const Header = ({ show, isShow }) => {
    const isSsr = useIsSsr();
    const { t, i18n } = useTranslation();

    const [bgHeader, setBgHeader] = useState(false);

    const redirectLang = !isSsr && app.getLang();

    const language = [
        { value: "ru", label: "Ru" },
        { value: "ua", label: "Ua" },
        { value: "en", label: "En" },
    ];
    const defaultSelectLang = { value: "ua", label: "Ua" };
    const [selectedLanguage, setSelectedLanguage] = useState({});
    !isSsr &&
        localStorage.setItem("i18nextLng", localStorage.getItem("i18nextLng"));
    useEffect(() => {
        i18n.changeLanguage(localStorage.getItem("i18nextLng"));
    }, [selectedLanguage]);

    useEffect(() => {
        if (app.getLang() === "ua") {
            setSelectedLanguage({ value: app.getLang(), label: "Ua" });
            i18n.changeLanguage(app.getLang());
        }

        if (app.getLang() === "ru") {
            setSelectedLanguage({ value: app.getLang(), label: "Ru" });
            i18n.changeLanguage(app.getLang());
        }

        if (app.getLang() === "en") {
            setSelectedLanguage({ value: app.getLang(), label: "En" });
            i18n.changeLanguage(app.getLang());
        }
    }, []);
    useEffect(() => {
        window.addEventListener("scroll", () => {
            if (window.scrollY > 0) {
                setBgHeader(true);
            } else {
                setBgHeader(false);
            }
        });
    });
    const isWidthDesktop = useMediaQuery("(min-width: 1121px)");
    return (
        <div
            className={`ott-header-wrap ${
                bgHeader ? "ott-header-wrap-bg" : ""
            }`}
        >
            <header className="ott-header">
                <div className="ott-header__nav-wrap">
                    <a className="ott-header__logo" href="/">
                        <picture>
                            <source
                                srcSet={ottLogo360}
                                media="(max-width: 450px)"
                            />
                            <img
                                src={ottLogo}
                                alt="logo Briz - інтернет-провайдер Briz в Одесі"
                                title="logo Briz"
                            />
                        </picture>
                    </a>
                    {isWidthDesktop && <OttNav />}
                </div>
                <div className="ott-header__controls-wrap">
                    {isWidthDesktop && (
                        <OttLangSelect
                            defaultText={defaultSelectLang}
                            optionsList={language}
                            getSelectedLanguage={setSelectedLanguage}
                            customClass="ott-select-language"
                            desktop={isWidthDesktop}
                        />
                    )}
                    <a
                        href={`https://stat.briz.ua/?lang=${redirectLang}`}
                        rel="nofollow"
                        className="ott-header__plug-link"
                    >
                        {t("ott.plug")}
                    </a>
                </div>
                {!isWidthDesktop && (
                    <button
                        type="button"
                        aria-label="button show aside"
                        className={`ott-header__sidebar-button ${
                            isShow ? "ott-header__sidebar-button-active" : ""
                        }`}
                        onClick={() => show()}
                    />
                )}
            </header>
        </div>
    );
};

Header.propTypes = {
    show: PropTypes.func,
    isShow: PropTypes.bool,
};

Header.defaultProps = {
    show: () => {},
    isShow: false,
};

export default Header;
