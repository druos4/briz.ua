import teleWebp from "../../../images/ott/tele.webp";
import telePng from "../../../images/ott/tele.png";
import filmsPng from "../../../images/ott/films.png";
import filmsWebp from "../../../images/ott/films.webp";
import serialsPng from "../../../images/ott/serials.png";
import serialsWebp from "../../../images/ott/serials.webp";
import cartoonPng from "../../../images/ott/cartoon.png";
import cartoonWebp from "../../../images/ott/cartoon.webp";
import disney from "../../../images/ott/disney.svg";
import pixar from "../../../images/ott/pixar.svg";
import dw from "../../../images/ott/dw.svg";
import wb from "../../../images/ott/wb.svg";
import mill from "../../../images/ott/mill.svg";
import plugPng from "../../../images/ott/plug.png";
import plugWebp from "../../../images/ott/plug.webp";
import actionPng from "../../../images/ott/action.png";
import actionWebp from "../../../images/ott/action.webp";
import webPng from "../../../images/ott/web.png";
import webWebp from "../../../images/ott/web.webp";
import androidAppPng from "../../../images/ott/androidApp.png";
import androidAppWebp from "../../../images/ott/androidApp.webp";
import iosAppPng from "../../../images/ott/iosApp.png";
import iosAppWebp from "../../../images/ott/iosApp.webp";
import androidTvPng from "../../../images/ott/androidTv.png";
import androidTvWebp from "../../../images/ott/androidTv.webp";
import appleTvPng from "../../../images/ott/appleTv.png";
import appleTvWebp from "../../../images/ott/appleTv.webp";
import samsungTvPng from "../../../images/ott/samsungTv.png";
import samsungTvWebp from "../../../images/ott/samsungTv.webp";
import lgTvPng from "../../../images/ott/lgTv.png";
import lgTvWebp from "../../../images/ott/lgTv.webp";

export const televisonSources = {
    "png": telePng,
    "webp": teleWebp,
    alt: "films",
};
export const filmsSources = {
    "png": filmsPng,
    "webp": filmsWebp,
    alt: "tv channels",
};
export const serialsSources = {
    "png": serialsPng,
    "webp": serialsWebp,
    alt: "serials",
};
export const cartoonsSources = {
    "png": cartoonPng,
    "webp": cartoonWebp,
    alt: "picture cartoons",
};

export const logoStudios = [
    {
        img: disney,
        alt: "logo disney",
    },
    {
        img: wb,
        alt: "logo Warner Bros.",
    },
    {
        img: dw,
        alt: "logo dream works",
    },
    {
        img: mill,
        alt: "logo studio mill",
    },
    {
        img: pixar,
        alt: "logo pixar",
    },
];

export const plugBrizTVSources = {
    "png": plugPng,
    "webp": plugWebp,
    alt: "picture doc",
};

export const watchBrizTVSources = {
    "png": actionPng,
    "webp": actionWebp,
    alt: "action",
};

export const tabsSources = {
    site: { png: webPng, webp: webWebp,  alt: "website" },
    android: { png: androidAppPng, webp: androidAppWebp, alt: "android device" },
    ios: { png: iosAppPng, webp: iosAppWebp, alt: "ios device" },
    androidTv: { png: androidTvPng, webp: androidTvWebp, alt: "androidTV device" },
    appleTv: { png: appleTvPng, webp: appleTvWebp, alt: "appleTV device" },
    samsung: { png: samsungTvPng, webp: samsungTvWebp, alt: "samsungTV device" },
    lg: { png: lgTvPng, webp: lgTvWebp, alt: "LGTV device" },
};

export const television = {
    title: "ott.tv",
    infoList: [
        {
            title: "ott.channels",
            desc: "ott.descChannels",
        },
        {
            title: "ott.rew",
            desc: "ott.descRew",
        },
        {
            title: "ott.tvRecord",
            desc: "ott.descTvRecord",
        },
    ],
};

export const films = {
    title: "ott.films",
    infoList: [
        {
            title: "ott.filmsHd",
            desc: "ott.colectionFilms",
        },
        {
            title: "ott.premiers",
            desc: "ott.premiersHome",
        },
        {
            title: "ott.devices",
            desc: "ott.descDevices",
        },
    ],
};

export const serials = {
    title: "ott.serials",
    infoList: [
        {
            title: "ott.allSerials",
            desc: "ott.favoritsSerials",
        },
        {
            title: "ott.pause",
            desc: "ott.pauseSerial",
        },
        {
            title: "ott.filmStudios",
            desc: "ott.serialsCollections",
        },
    ],
};
export const cartoons = {
    title: "ott.cartoons",
    infoList: [
        {
            title: "ott.allCartoons",
            desc: "ott.bestCartoons",
        },
        {
            title: "ott.collectionCartoons",
            desc: "ott.addNewCartoons",
        },
        {
            title: "ott.animateStudios",
            desc: "ott.fromDisney",
        },
    ],
};
export const tabs = [
    { tab: "ott.tabSite", link: "site" },
    { tab: "ott.tabAndroid", link: "android" },
    { tab: "ott.tabIos", link: "ios" },
    { tab: "ott.tabAndroidTv", link: "androidTv" },
    { tab: "ott.tabAppleTv", link: "appleTv" },
    { tab: "ott.tabSamsung", link: "samsung" },
    { tab: "ott.tabLg", link: "lg" },
];

export const connectBrizTv = {
    titleConnectBrizTV: "ott.titleConnectBrizTV",
    descriptionText: "ott.bigChoiceContent",
    plug: "ott.plug",
    descriptionAccess: "ott.accessBrizUsers",
};

export const watchBrizTV = {
    titleConnectBrizTV: "ott.watchBrizTvTitle",
    descriptionText: "ott.registartion",
    descriptionSubText: "ott.action",
    plug: "ott.plug",
    descriptionAccess: "ott.accessBrizUsers",
};

export const footerContent = {
    title: "ott.titleContactFooter",
};
