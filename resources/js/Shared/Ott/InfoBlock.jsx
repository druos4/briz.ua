/* eslint-disable react/no-array-index-key */
import React from "react";
import PropTypes, { arrayOf } from "prop-types";
import { useTranslation } from "react-i18next";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";
import Picture from "./ui/picture";

const InfoBlock = ({
    content,
    imgSide,
    blockBg,
    sources,
    id,
    decorationClass,
    pt,
    className
}) => {
    const { t } = useTranslation();

    const [ref, inView] = useInView({
        triggerOnce: true,
        threshold: 0.4,
    });
    const variantsLeft = {
        visible: { opacity: inView ? 1 : 0, x: inView ? 0 : 300 },
        hidden: { opacity: 0, x: 300 },
    };
    const variantsRight = {
        visible: { opacity: inView ? 1 : 0, x: inView ? 0 : -300 },
        hidden: { opacity: 0, x: -300 },
    };

    const variantsLeftDecor = {
        visible: { opacity: 1, x: inView ? 0 : 600 },
        hidden: { opacity: 0, x: 600 },
    };
    const variantsRightDecor = {
        visible: { opacity: 1, x: inView ? 0 : -900 },
        hidden: { opacity: 0, x: -900 },
    };
    return (
        <section
            ref={ref}
            id={id}
            className={`ott-info ${className} ${pt && "ott-info__padding-top"} ${
                blockBg ? "ott-info-bg" : ""
            } ${imgSide === "right" ? "ott-info-img-right" : ""}`}
        >
            {decorationClass && (
                <motion.div
                    initial="hidden"
                    animate="visible"
                    variants={
                        imgSide === "left"
                            ? variantsLeftDecor
                            : variantsRightDecor
                    }
                    transition={{ duration: 1.5 }}
                    className={decorationClass}
                />
            )}
            <div className="ott-info__wrap">
                {imgSide === "left" && (
                    <Picture
                        sources={sources}
                        isAnimated
                        variant={variantsRight}
                    />
                )}
                <motion.div
                    className="ott-info__text"
                    initial="hidden"
                    animate="visible"
                    variants={imgSide === "left" ? variantsLeft : variantsRight}
                    transition={{ duration: 1.5 }}
                    key={id}
                >
                    <h2 className="ott-info__title">{t(content.title)}</h2>
                    <ul className="ott-info__list">
                        {content &&
                            content.infoList &&
                            content.infoList.map((info, index) => {
                                return (
                                    <li key={index} className="ott-info__item">
                                        <h3 className="ott-info__item-title">
                                            {t(info.title)}
                                        </h3>
                                        <p className="ott-info__item-subtitle">
                                            {t(info.desc)}
                                        </p>
                                    </li>
                                );
                            })}
                    </ul>
                </motion.div>
                {imgSide === "right" && (
                    <Picture
                        sources={sources}
                        isAnimated
                        variant={variantsLeft}
                    />
                )}
            </div>
        </section>
    );
};

InfoBlock.propTypes = {
    content: PropTypes.shape({
        title: PropTypes.string,
        infoList: arrayOf(PropTypes.object),
    }),
    decorationClass: PropTypes.string,
    imgSide: PropTypes.string,
    blockBg: PropTypes.bool,
    pt: PropTypes.string,
    sources: PropTypes.shape({
        "1x": PropTypes.string,
        "2x": PropTypes.string,
        alt: PropTypes.string,
    }),
    id: PropTypes.string,
    className: PropTypes.string
};
InfoBlock.defaultProps = {
    content: {
        title: "",
        infoList: [],
    },
    decorationClass: "",
    imgSide: "",
    blockBg: false,
    pt: "",
    sources: {
        "1x": "",
        "2x": "",
        alt: "",
    },
    id: "",
    className:""
};

export default InfoBlock;
