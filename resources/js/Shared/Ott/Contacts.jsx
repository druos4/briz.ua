import React from "react";
import PropTypes from "prop-types";

const Contacts = ({ inFooter }) => {
    return (
        <div className="ott-contact-list-wrap">
            <ul
                className={`${
                    inFooter ? "ott-contact-list-footer" : "ott-contact-list"
                }`}
            >
                <li className="ott-contact-list__item">
                    <a
                        href="tel:048 797 25 25"
                        className="ott-contact-list__link"
                    >
                        048 797 25 25
                    </a>
                </li>
                <li className="ott-contact-list__item">
                    <a
                        href="tel:048 729 11 22"
                        className="ott-contact-list__link"
                    >
                        048 729 11 22
                    </a>
                </li>
                <li className="ott-contact-list__item">
                    <a
                        href="tel:093 797 25 25"
                        className="ott-contact-list__link"
                    >
                        093 797 25 25
                    </a>
                </li>
            </ul>
            <ul
                className={`${
                    inFooter ? "ott-contact-list-footer" : "ott-contact-list"
                }`}
            >
                <li className="ott-contact-list__item">
                    <a
                        href="tel:068 797 25 25"
                        className="ott-contact-list__link"
                    >
                        068 797 25 25
                    </a>
                </li>
                <li className="ott-contact-list__item">
                    <a
                        href="tel:095 797 25 25"
                        className="ott-contact-list__link"
                    >
                        095 797 25 25
                    </a>
                </li>
                <li className="ott-contact-list__item">
                    <a
                        href="mailto:supp@briz.ua"
                        className="ott-contact-list__link"
                    >
                        supp@briz.ua
                    </a>
                </li>
            </ul>
        </div>
    );
};

Contacts.propTypes = {
    inFooter: PropTypes.bool,
};
Contacts.defaultProps = {
    inFooter: false,
};

export default Contacts;
