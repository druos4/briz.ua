/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useRef } from "react";
import PropTypes from "prop-types";
import OttNav from "./Nav";
import OttLangSelect from "./LangSelect";
import Contacts from "./Contacts";

export const OttAside = ({ isShow, show }) => {
    const elRef = useRef();
    return (
        <>
            {isShow && (
                <div
                    onClick={() => show(false)}
                    className="ott-sidebar__overlay"
                />
            )}
            <aside
                ref={elRef}
                className={`ott-sidebar ${
                    isShow ? "ott-sidebar__visible" : "ott-sidebar__hidden"
                } `}
            >
                <OttLangSelect />
                <OttNav />
                <Contacts />
            </aside>
        </>
    );
};

OttAside.propTypes = {
    isShow: PropTypes.bool,
    show: PropTypes.func,
};

OttAside.defaultProps = {
    isShow: false,
    show: () => {},
};
export default OttAside;
