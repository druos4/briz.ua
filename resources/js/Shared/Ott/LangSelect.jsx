import React from "react";
import PropTypes from "prop-types";
import DropdownSelect from "../DropdownSelect";
import MobileLangSelect from "./MobileLangSelect";

const OttLangSelect = ({
    optionsList,
    defaultText,
    getSelectedLanguage,
    desktop,
}) => {
    return desktop ? (
        <DropdownSelect
            defaultText={defaultText}
            optionsList={optionsList}
            getSelectedLanguage={(selected) => getSelectedLanguage(selected)}
            customClass="ott-select-language"
            isRefreshPage={false}
        />
    ) : (
        <MobileLangSelect isRefreshPage={false} />
    );
};

OttLangSelect.propTypes = {
    optionsList: PropTypes.arrayOf(PropTypes.object),
    defaultText: PropTypes.shape({
        value: PropTypes.string,
        label: PropTypes.string,
    }),
    getSelectedLanguage: PropTypes.func,
    desktop: PropTypes.bool,
};
OttLangSelect.defaultProps = {
    optionsList: [],
    defaultText: {},
    getSelectedLanguage: () => {},
    desktop: false,
};

export default OttLangSelect;
