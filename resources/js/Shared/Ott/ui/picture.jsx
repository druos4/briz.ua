import React from "react";
import PropTypes from "prop-types";
import { motion } from "framer-motion";

const Picture = React.forwardRef((props, ref) => {
    const { sources, alt, isAnimated, variant } = props;
    return isAnimated ? (
        <motion.picture
            initial="hidden"
            animate="visible"
            variants={variant}
            transition={{ duration: 1.5 }}
            key={sources["png"]}
            ref={ref}
        >
            <source srcSet={`${sources["webp"]}`} type='image/webp'/>
            <img
                className="ott-picture"
                src={sources["png"]}
                alt={`${alt} - інтернет-провайдер Briz в Одесі`}
                title={alt}
            />
        </motion.picture>
    ) : (
        <picture>
            <source srcSet={`${sources["webp"]}`} type='image/webp'/>
            <img
                className="ott-picture"
                src={sources["png"]}
                alt={`${alt} - інтернет-провайдер Briz в Одесі`}
                title={alt}
            />
        </picture>
    );
});

Picture.propTypes = {
    sources: PropTypes.shape({
        "1x": PropTypes.string,
        "2x": PropTypes.string,
        alt: PropTypes.string,
    }),
    alt: PropTypes.string,
    isAnimated: PropTypes.bool,
    variant: PropTypes.shape({}),
};
Picture.defaultProps = {
    sources: [],
    alt: "background picture",
    isAnimated: false,
    variant: {},
};

export default Picture;
