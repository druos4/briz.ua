/* eslint-disable react/no-array-index-key */
import React, { useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { motion } from "framer-motion";
import { useInView } from "react-intersection-observer";

const Tabs = ({ content, sources, id }) => {
    const [activeTab, setActiveTab] = useState(content[0].link);
    const handleSelectTab = (idTab) => {
        setActiveTab(idTab);
    };

    const [ref, inView] = useInView({
        triggerOnce: true,
        rootMargin: "-300px 0px",
    });
    const variant = {
        visible: { opacity: inView ? 1 : 0, y: inView ? 0 : -50 },
        hidden: { opacity: 0, y: -50 },
    };
    const { t } = useTranslation();
    return (
        <motion.section
            id={id}
            className="ott-tabs"
            ref={ref}
            initial="hidden"
            animate="visible"
            variants={variant}
            transition={{ duration: 1.5 }}
        >
            <div className="ott-top-banner__title">{t("ott.tabsTitle")}</div>
            <div className="ott-tabs__wrap">
                <ul className="ott-tabs__nav-list">
                    {content.map((item, index) => {
                        return (
                            <li key={index}>
                                <button
                                    type="button"
                                    id={item.link}
                                    className={`ott-tabs__button ${
                                        item.link === activeTab
                                            ? "ott-tabs__button-active"
                                            : ""
                                    }`}
                                    onClick={() => handleSelectTab(item.link)}
                                >
                                    {t(item.tab)}
                                </button>
                            </li>
                        );
                    })}
                </ul>
            </div>
            <div className="ott-tabs__content">
                <motion.picture
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    exit={{ opacity: 0 }}
                    transition={{ duration: 0.7 }}
                    key={sources[activeTab].png}
                >
                    <source srcSet={sources[activeTab].webp} type='image/webp'/>
                    <img
                      src={sources[activeTab].png}
                      alt={`${sources[activeTab].alt} - інтернет-провайдер Briz в Одесі`}
                      title={sources[activeTab].alt}
                    />
                </motion.picture>
            </div>
            <p className="ott-info__item-subtitle ott-tabs__description">
                {t("ott.tabsDesc")}
            </p>
        </motion.section>
    );
};

Tabs.propTypes = {
    id: PropTypes.string,
    content: PropTypes.arrayOf(
        PropTypes.shape({ link: PropTypes.string, tab: PropTypes.string })
    ),
    sources: PropTypes.shape({
        site: PropTypes.shape({
            alt: PropTypes.string,
            img: PropTypes.string,
        }),
        android: PropTypes.shape({
            alt: PropTypes.string,
            img: PropTypes.string,
        }),
        ios: PropTypes.shape({
            alt: PropTypes.string,
            img: PropTypes.string,
        }),
        androidTV: PropTypes.shape({
            alt: PropTypes.string,
            img: PropTypes.string,
        }),
        appleTV: PropTypes.shape({
            alt: PropTypes.string,
            img: PropTypes.string,
        }),
        samsung: PropTypes.shape({
            alt: PropTypes.string,
            img: PropTypes.string,
        }),
        lg: PropTypes.shape({
            alt: PropTypes.string,
            img: PropTypes.string,
        }),
    }),
};
Tabs.defaultProps = {
    id: "",
    content: [],
    sources: {},
};

export default Tabs;
