import React, { useRef, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import videoPoster from "../../../images/ott/videoPoster.png";
import videoDesktop from "../../../images/ott/desktop.mp4";
import videoMobile from "../../../images/ott/mobile.mp4";
import videoTablet from "../../../images/ott/tablet.mp4";
import { useMediaQuery } from "../MediaQuery/useMediaQuery";

const TopBanner = ({ id }) => {
    const video = useRef();
    const { t } = useTranslation();
    const [play, setPlay] = useState(true);

    const isWidthDesktop = useMediaQuery("(min-width: 1020px)");
    const isWidthMobile = useMediaQuery("(max-width: 560px)");
    useEffect(() => {
        video.current.load();
    }, [isWidthDesktop, isWidthMobile]);

    const handlePlayPause = () => {
        if (play) {
            video.current.pause();
            setPlay((prev) => !prev);
            return;
        }
        video.current.play();
        setPlay((prev) => !prev);
    };

    return (
        <section id={id} className="ott-top-banner">
            <div className="ott-top-banner__video-wrap">
                <button
                    aria-label="play/pause"
                    className={`ott-top-banner__button  ott-top-banner__button--icon-${
                        play ? "pause" : "play"
                    }`}
                    type="button"
                    onClick={() => handlePlayPause()}
                />
                <video
                    ref={video}
                    className="ott-top-banner__video"
                    autoPlay
                    loop
                    muted
                    poster={videoPoster}
                >
                    {isWidthDesktop && (
                        <source
                            label="desktop"
                            src={videoDesktop}
                            type="video/mp4"
                        />
                    )}

                    {!isWidthMobile && !isWidthDesktop && (
                        <source
                            label="tablet"
                            src={videoTablet}
                            type="video/mp4"
                        />
                    )}

                    {isWidthMobile && (
                        <source
                            label="mobile"
                            src={videoMobile}
                            type="video/mp4"
                        />
                    )}
                </video>
                <div className="ott-top-banner__filter" />
                <div className="ott-top-banner__wrap">
                    <h1 className="ott-top-banner__title">{t("ott.title")}</h1>
                    <p className="ott-top-banner__subtitle">
                        {t("ott.subtitle")}
                    </p>
                    <svg
                        className="ott-top-banner__arrow"
                        width="45"
                        height="45"
                        viewBox="0 0 13 7"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <path
                            fill="#8D8984"
                            d="M1.44733 0.236567C1.11623 -0.0788564 0.57942 -0.0788564 0.248323 0.236567C-0.0827732 0.551991 -0.0827733 1.06339 0.248323 1.37882L5.9005 6.76343C6.23159 7.07886 6.76841 7.07886 7.0995 6.76343L12.7517 1.37882C13.0828 1.06339 13.0828 0.551991 12.7517 0.236568C12.4206 -0.0788559 11.8838 -0.0788559 11.5527 0.236568L6.5 5.05006L1.44733 0.236567Z"
                        />
                    </svg>
                </div>
            </div>
        </section>
    );
};

TopBanner.propTypes = {
    id: PropTypes.string,
};
TopBanner.defaultProps = {
    id: "",
};

export default TopBanner;
