/* eslint-disable react/no-danger */
import React, { useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

const Accordion = ({ title, payment }) => {
    const [isOpen, setOpen] = useState(false);
    const { t } = useTranslation();

    return (
        <div className="accordion">
            <div className="accordion__wrapper-item">
                <div
                    tabIndex="0"
                    role="button"
                    className="accordion__toggle"
                    onClick={() => setOpen(!isOpen)}
                    onKeyDown={(e) => {
                        if (e.keyCode === 13) {
                            setOpen(!isOpen);
                        }
                    }}
                >
                    <img
                        src={payment.icon}
                        alt="icon"
                        className="accordion__toggle-img"
                    />
                    <div className="accordion__toggle-text">{title}</div>
                    <span className="accordion__toggle-image-box">
                        {Number(payment.commission) !== 0 ? (
                            <span className="accordion__commission-border">
                                <span className="accordion__commission">
                                    {t("commission")}
                                </span>
                            </span>
                        ) : null}
                        <span
                            className={
                                isOpen
                                    ? "accordion__toggle-image"
                                    : "accordion__toggle-image accordion__toggle-image--active"
                            }
                        >
                            <svg
                                width="19"
                                height="11"
                                viewBox="0 0 19 11"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M16.8847 10.6283C17.3686 11.1239 18.1532 11.1239 18.6371 10.6283C19.121 10.1326 19.121 9.32895 18.6371 8.83329L10.3762 0.371749C9.89229 -0.123917 9.10771 -0.123917 8.6238 0.371749L0.362933 8.83329C-0.120978 9.32895 -0.120978 10.1326 0.362933 10.6283C0.846843 11.1239 1.63142 11.1239 2.11533 10.6283L9.5 3.06419L16.8847 10.6283Z"
                                    fill="#00A3A4"
                                />
                            </svg>
                        </span>
                    </span>
                </div>
                <div
                    className={`accordion__collapse ${
                        !isOpen ? "accordion__collapse--active" : ""
                    }`}
                >
                    <div
                        className="accordion__content"
                        dangerouslySetInnerHTML={{
                            __html: payment.description,
                        }}
                    />
                </div>
            </div>
        </div>
    );
};

Accordion.propTypes = {
    title: PropTypes.string,
    payment: PropTypes.shape({
        icon: PropTypes.string,
        description: PropTypes.string,
        commission: PropTypes.number,
    }),
};
Accordion.defaultProps = {
    title: "",
    payment: {},
};

export default Accordion;
