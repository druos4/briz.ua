/* eslint-disable no-unused-vars */
/* eslint-disable no-return-assign */
/* eslint-disable react/no-array-index-key */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
// import { InertiaLink } from "@inertiajs/inertia-react";
import { PRODUCT_PRODUCT } from "@/constants/apiParams";
import ShopProductButton from "./ui/buttons/ShopProductButton";
import { useIsSsr } from "@/hooks/useIsSsr";

const ShopCategoryItem = ({
    products,
    buttons,
    title,
    callbackActualTitle,
    setModalAccept,
    setShopProduct,
    isShowModalAccept,
}) => {
    const isSsr = useIsSsr();
    const { t } = useTranslation();
    const lang = !isSsr && document.querySelector("html").lang;
    const handleAppareModal = (e, productTitle, id, product) => {
        e.preventDefault();
        // Открытие модального окна с передачей в него данных

        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle,
            productId: id,
            titleModal: t("purchaseRequest"),
            titleButton: t("sendRequest"),
            apiUrl: PRODUCT_PRODUCT.url,
            typeProduct: PRODUCT_PRODUCT.type,
            titleProduct: "",
            seoId: PRODUCT_PRODUCT.type,
            body: { [PRODUCT_PRODUCT.type]: id, ghost: "" },
            keyFormRedux: "default",
        });

        setShopProduct(product);
    };

    useEffect(() => {
        let cleanupFunction = false;
        // sortButton.sort((x, y) => x.active ? -1 : y.active ? 1 : 0);
        callbackActualTitle(title);

        return () => (cleanupFunction = true);
    }, []);

    useEffect(() => {
        if (!isShowModalAccept) {
            setShopProduct({});
        }
    }, [isShowModalAccept]);

    return (
        <>
            <div className="shop__category-item">
                <div className="shop__buttons-group-panel">
                    <ul className="shop__list-filters">
                        {buttons.filter.map((filter, index) => {
                            return (
                                <li
                                    className={
                                        filter.active
                                            ? "shop__item-filter shop__item-filter--active"
                                            : "shop__item-filter"
                                    }
                                    key={index}
                                >
                                    <a
                                        className={
                                            filter.active
                                                ? "shop__item-filter-text shop__item-filter-text--active"
                                                : "shop__item-filter-text"
                                        }
                                        href={filter.url}
                                    >
                                        {filter.title}

                                        {filter.active ? (
                                            <span className="shop__item-filter-checked-icon" />
                                        ) : (
                                            <span className="shop__item-filter-checked-icon-hidden" />
                                        )}
                                    </a>
                                </li>
                            );
                        })}
                    </ul>
                    <div>
                        <a className="shop__sort" href={buttons.sort.url}>
                            <span className="shop__sort-button">
                                {buttons.sort.title}
                            </span>
                            <span className="shop__sort-icon">
                                {!isSsr && location.search.includes("cheap") ? (
                                    <svg
                                        width="22"
                                        height="22"
                                        viewBox="0 0 22 22"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            fillRule="evenodd"
                                            clipRule="evenodd"
                                            d="M3.66686 6.0002C4.17285 6.0002 4.58351 6.41269 4.58351 6.91686V13.8743L5.76875 12.6826C6.12717 12.3251 6.70649 12.3251 7.06491 12.6826C7.42332 13.0401 7.42332 13.6268 7.06491 13.9843L4.31493 16.7343C3.95652 17.0917 3.37719 17.0917 3.01878 16.7343L0.26881 13.9843C-0.0896032 13.6268 -0.0896032 13.0401 0.26881 12.6826C0.627223 12.3251 1.20655 12.3251 1.56496 12.6826L2.7502 13.8743V6.91686C2.7502 6.41269 3.16086 6.0002 3.66686 6.0002ZM9.16667 6.91666C9.16667 6.4125 9.57733 6 10.0833 6H17.4166C17.9226 6 18.3332 6.4125 18.3332 6.91666C18.3332 7.42082 17.9226 7.83331 17.4166 7.83331H10.0833C9.57733 7.83331 9.16667 7.42082 9.16667 6.91666ZM9.16667 11.5C9.16667 10.9958 9.57733 10.5833 10.0833 10.5833H17.4166C17.9226 10.5833 18.3332 10.9958 18.3332 11.5C18.3332 12.0042 17.9226 12.4166 17.4166 12.4166H10.0833C9.57733 12.4166 9.16667 12.0042 9.16667 11.5ZM9.16667 16.0833C9.16667 15.5792 9.57733 15.1667 10.0833 15.1667H21.0832C21.5892 15.1667 21.9999 15.5792 21.9999 16.0833C21.9999 16.5875 21.5892 17 21.0832 17H10.0833C9.57733 17 9.16667 16.5875 9.16667 16.0833Z"
                                            fill="#424242"
                                        />
                                    </svg>
                                ) : (
                                    <svg
                                        width="22"
                                        height="22"
                                        viewBox="0 0 22 22"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            className="shop__sort-icon-inner-selector"
                                            fillRule="evenodd"
                                            clipRule="evenodd"
                                            d="M3.01878 7.26812C3.37719 6.91063 3.95652 6.91063 4.31493 7.26812L7.06491 10.0181C7.42332 10.3756 7.42332 10.9623 7.06491 11.3197C6.70649 11.6772 6.12717 11.6772 5.76875 11.3197L4.58351 10.1281V17.0855C4.58351 17.5897 4.17285 18.0022 3.66686 18.0022C3.16086 18.0022 2.7502 17.5897 2.7502 17.0855V10.1281L1.56496 11.3197C1.20655 11.6772 0.627223 11.6772 0.26881 11.3197C-0.0896032 10.9623 -0.0896032 10.3756 0.26881 10.0181L3.01878 7.26812ZM21.0832 8.83592H10.0833C9.57733 8.83592 9.16667 8.42342 9.16667 7.91926C9.16667 7.4151 9.57733 7.0026 10.0833 7.0026H21.0832C21.5892 7.0026 21.9999 7.4151 21.9999 7.91926C21.9999 8.42342 21.5892 8.83592 21.0832 8.83592ZM9.16667 12.5026C9.16667 11.9984 9.57733 11.5859 10.0833 11.5859H17.4166C17.9226 11.5859 18.3332 11.9984 18.3332 12.5026C18.3332 13.0068 17.9226 13.4193 17.4166 13.4193H10.0833C9.57733 13.4193 9.16667 13.0068 9.16667 12.5026ZM9.16667 17.0859C9.16667 16.5818 9.57733 16.1693 10.0833 16.1693H17.4166C17.9226 16.1693 18.3332 16.5818 18.3332 17.0859C18.3332 17.5901 17.9226 18.0026 17.4166 18.0026H10.0833C9.57733 18.0026 9.16667 17.5901 9.16667 17.0859Z"
                                            fill="#424242"
                                        />
                                    </svg>
                                )}
                            </span>
                        </a>
                    </div>
                </div>

                <ul
                    className={
                        products.length
                            ? "shop__list-catalog"
                            : "shop__list-catalog shop__list-catalog--no-products"
                    }
                >
                    {products.length ? (
                        products.map((product) => {
                            return product.can_buy ? (
                                <li
                                    className="shop__item-catalog"
                                    key={product.slug}
                                >
                                    <a
                                        href={product.url}
                                        className="shop__item-link"
                                    >
                                        <div
                                            className="shop__item-wrapper"
                                            itemScope
                                            itemType="http://schema.org/Product"
                                        >
                                            <img
                                                src={product.picture}
                                                alt={`${product.title} - інтернет-провайдер Briz в Одесі`}
                                                title={product.title}
                                                className="shop__item-catalog-pic"
                                                itemProp="image"
                                            />
                                            <div className="shop__item-catalog-info">
                                                <div className="shop__item-chips-wrapper">
                                                    {product.top ? (
                                                        <div className="shop__item-catalog-top  shop__item-chips">
                                                        <span className="shop__item-catalog-label shop__item-catalog-label--top">
                                                            {t("top")}
                                                        </span>
                                                    </div>
                                                ) : null}
                                                {product.has_discount ? (
                                                    <div className="shop__item-catalog-discount shop__item-chips">
                                                        <span className="shop__item-catalog-label">
                                                            {t("discount")}
                                                        </span>
                                                    </div>
                                                ) : null}
                                                {product.promo ? (
                                                    <div className="shop__item-catalog-promo  shop__item-chips">
                                                        <span className="shop__item-catalog-label">
                                                            {t("labelPromo")}
                                                        </span>
                                                    </div>
                                                ) : null}
                                                {product.new ? (
                                                    <div className="shop__item-catalog-new  shop__item-chips">
                                                        <span className="shop__item-catalog-label">
                                                            {t("new")}
                                                        </span>
                                                        </div>
                                                    ) : null}
                                                </div>
                                                <p
                                                    className="shop__item-catalog-title"
                                                    itemProp="name"
                                                >
                                                    {product.title}
                                                </p>
                                                <span className="shop__item-catalog-can-buy shop__item-catalog-can-buy-icon">
                                                    {t(
                                                        "freeDeliverySetupSecond"
                                                    )}
                                                </span>
                                                <div className="shop__item-price-block">
                                                    <span
                                                        className="shop__item-true-price"
                                                        itemProp="offers"
                                                        itemScope
                                                        itemType="http://schema.org/Offer"
                                                    >
                                                        <meta
                                                            itemProp="priceCurrency"
                                                            content="UAH"
                                                        />
                                                        <span itemProp="price">
                                                            {product.price}
                                                        </span>{" "}
                                                        <span
                                                            className={
                                                                lang === "en"
                                                                    ? "shop__currency"
                                                                    : ""
                                                            }
                                                        >
                                                            {t("UAH")}
                                                        </span>
                                                        <span
                                                            itemProp="availability"
                                                            href="http://schema.org/InStock"
                                                            hidden
                                                        >
                                                            InStock
                                                        </span>
                                                    </span>
                                                    {product.price_old ? (
                                                        <span className="shop__item-old-price">
                                                            {product.price_old}{" "}
                                                            <span
                                                                className={
                                                                    lang ===
                                                                    "en"
                                                                        ? "shop__currency-old-price"
                                                                        : ""
                                                                }
                                                            >
                                                                {t("UAH")}
                                                            </span>
                                                        </span>
                                                    ) : null}
                                                </div>
                                                <ShopProductButton
                                                    handler={(e) =>
                                                        handleAppareModal(
                                                            e,
                                                            product.title,
                                                            product.id,
                                                            product
                                                        )
                                                    }
                                                    titleButton={t("toOrder")}
                                                />
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            ) : null;
                        })
                    ) : (
                        <li className="shop__item-no-products">
                            <span className="shop__img-no-products" />
                            <span className="shop__text-no-products">
                                {t("noEquipmentsFound")}
                            </span>
                        </li>
                    )}
                </ul>
            </div>
        </>
    );
};

ShopCategoryItem.propTypes = {
    setModalAccept: PropTypes.func,
    products: PropTypes.arrayOf(PropTypes.object),
    buttons: PropTypes.shape({
        filter: PropTypes.arrayOf(PropTypes.object),
        sort: PropTypes.shape({
            title: PropTypes.string,
            url: PropTypes.string,
        }),
    }),
    title: PropTypes.string,
    callbackActualTitle: PropTypes.func,
    isShowModalAccept: PropTypes.bool,
};
ShopCategoryItem.defaultProps = {
    setModalAccept: () => {},
    products: [],
    buttons: {},
    title: "",
    callbackActualTitle: () => {},
};

export default ShopCategoryItem;
