/* eslint-disable react/no-array-index-key */
/* eslint-disable no-return-assign */
import React from "react";
import PropTypes from "prop-types";
import { InertiaLink } from "@inertiajs/inertia-react";
import { useIsSsr } from '../hooks/useIsSsr';
import { isIos } from '../utils';

const Breadcrumbs = ({ breadcrumbs }) => {
    const isSsr = useIsSsr();
    const breadcrumbsLength = breadcrumbs.length;
    
    const ios = !isSsr && isIos();

    return (
        <div className={ios ? "breadcrumbs breadcrumbs--ios" : "breadcrumbs"}>
            <ul
                className="breadcrumbs__list"
                itemScope
                itemType="http://schema.org/BreadcrumbList"
            >
                {breadcrumbs.map((item, index) => {
                    return (
                        <li
                            className="breadcrumbs__item"
                            itemProp="itemListElement"
                            itemScope
                            itemType="http://schema.org/ListItem"
                            key={index}
                        >
                            {breadcrumbsLength !== index + 1 ? (
                                <>
                                    <a
                                        href={item.url}
                                        className="breadcrumbs__link"
                                        itemProp="item"
                                        onTouchStart={(e) =>
                                            (e.target.style.color = "#00A3A4")
                                        }
                                        onTouchEnd={(e) =>
                                            (e.target.style.color = "#B8B8B8")
                                        }
                                    >
                                        <span itemProp="name">
                                            {item.title}
                                        </span>
                                    </a>
                                    <meta
                                        itemProp="position"
                                        content={index + 1}
                                    />
                                    <span className="breadcrumbs__separator">
                                        <svg
                                            width="7"
                                            height="13"
                                            viewBox="0 0 7 13"
                                            fill="none"
                                            xmlns="http://www.w3.org/2000/svg"
                                        >
                                            <path
                                                d="M1.38281 12.9766H0.140625L4.89062 0.625H6.125L1.38281 12.9766Z"
                                                fill="#B8B8B8"
                                            />
                                        </svg>
                                    </span>
                                </>
                            ) : (
                                <>
                                    <link
                                        itemProp="item"
                                        href={`${item.url}`}
                                    />
                                    <span
                                        itemProp="name"
                                        className="breadcrumbs__last-item"
                                    >
                                        {item.title}
                                    </span>
                                    <meta
                                        itemProp="position"
                                        content={index + 1}
                                    />
                                </>
                            )}
                        </li>
                    );
                })}
            </ul>
        </div>
    );
};

Breadcrumbs.propTypes = {
    breadcrumbs: PropTypes.arrayOf(PropTypes.object),
};
Breadcrumbs.defaultProps = {
    breadcrumbs: [],
};

export default Breadcrumbs;
