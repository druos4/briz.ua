/* eslint-disable react/no-danger */
import React from "react";
import PropTypes from "prop-types";

const QuestionItem = ({ title, answer }) => {
    return (
        <>
            <div className="question-item__title">{title}</div>
            <div
                className="question-item__answer"
                dangerouslySetInnerHTML={{ __html: answer }}
            />
        </>
    );
};

QuestionItem.propTypes = {
    title: PropTypes.string,
    answer: PropTypes.string,
};
QuestionItem.defaultProps = {
    title: "",
    answer: "",
};

export default QuestionItem;
