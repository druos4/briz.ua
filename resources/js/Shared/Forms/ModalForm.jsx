/* eslint-disable no-shadow */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import ReactGA from "react-ga";
import {
    submitModalForm,
    setModalAccept,
    clearApiCandidacy,
    clearModalAccept,
} from "../../actions/actions";
import {
    InputPhone,
    InputText,
    InputMultiText,
    InputPhoneHeader,
    InputPhoneOtt,
    InputNameOtt,
} from "./InputFields";
import ButtonSubmitForm from "../ui/buttons/ButtonSubmitForm";
import { defineModalType, getCookie } from "../../utils";

const Form = ({
    titleButton,
    seoId,
    productTitle,
    titleModal,
    titleProduct,
    apiUrl,
    submitModalForm,
    body,
    setModalAccept,
    clearApiCandidacy,
    clearModalAccept,
    type,
    keyFormRedux = "default",
    state,
    typeProduct,
    closeFormOnHeader = null,
    productInfo,
    productId,
    commentId,
}) => {
    const { t } = useTranslation();
    const initForm = {
        phone: "",
        name: "",
        comment: "",
        address: "",
    };
    const initFormErrors = {
        name:'',
        comment:''
    }
    const [formData, setFormData] = useState(initForm);
    const [errors, setErrors] = useState(null);
    const [errorsFromServer, SetErrorsFromServer] = useState(null);
    const { loading, error, isInited, errorSecondSend, errorMessage } = state[keyFormRedux];
    const [errorCommentForm, setErrorCommentForm] = useState(initFormErrors);

    const handleForm = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    useEffect(() => {
        if (!!formData.comment) {
            setErrorCommentForm({ ...errorCommentForm, comment: "" });
        }
    }, [formData.comment]);

    useEffect(()=> {
        if (!!formData.name) {
            setErrorCommentForm({ ...errorCommentForm, name: "" });
        }
    },[formData.name])

    useEffect(() => {
        if (error) {
            SetErrorsFromServer(t("serverNotRes"));
            setTimeout(() => clearApiCandidacy(), 4000);
            return;
        }
        SetErrorsFromServer(null);
    }, [error]);

    useEffect(() => {
        if (formData.phone.length === 17) {
            setErrors(null);
        }
    }, [formData.phone]);

    // Выводим тип модального окна, успешный ответ от сервера
    useEffect(() => {
        if (isInited) {
            const requestStatusMessage = errorMessage.data
            setModalAccept({
                showModalAccept: true,
                typeModalAccept: defineModalType(errorSecondSend, type, requestStatusMessage),
            });
            setFormData(initForm);
            closeFormOnHeader && closeFormOnHeader(false);
        }
    }, [isInited]);

    // Отправка данных на бек
    const handleSubmit = async (e) => {
        e.preventDefault();
        if (formData.phone.length < 17) {
            setErrors(t("wrongFormat"));
            return;
        }
        // check cookie
        if (getCookie(typeProduct)) {
            setModalAccept({
                showModalAccept: true,
                typeModalAccept: "reqAccept",
            });
            return;
        }

        seoId && localStorage.setItem("seoId", seoId);
        // Отправка в аналитику типа формы (заказ товара/заказ услуги/оренда кабеля/обратный звонок/поддержка)
        ReactGA.ga("send", "event", seoId, "send");

        // Отправка в аналитику наименование товара
        if (Boolean(Object.keys(productInfo).length)) {
            ReactGA.ga("send", "event", productInfo.getTitle(), "send");
        }

        try {
            submitModalForm({
                body: {
                    ...body,
                    phone: formData.phone.toString().replace(/\D+/g, ""),
                    name: formData.name,
                    comment: formData.comment,
                    address: formData.address || body.address || "",
                },
                url: apiUrl,
                keyFormRedux,
                typeProduct,
            });
        } catch (e) {
            console.log(e.message);
        }
    };

    const handleSubmitComment = (e) => {
        e.preventDefault();
        switch (true) {
            case formData.name.trim() === "" && formData.comment.trim() === "":
                setErrorCommentForm({
                    ...errorCommentForm,
                    comment: `${t("requiredField")}`,
                    name: `${t("requiredField")}`
                });
                break;
            case formData.name.trim() === "":
                setErrorCommentForm({
                    ...errorCommentForm,
                    name: `${t("requiredField")}`
                });
                break;
            case formData.comment.trim() === "":
                setErrorCommentForm({
                    ...errorCommentForm,
                    comment: `${t("requiredField")}`
                });
                break;
            default:
                try {
                    submitModalForm({
                        body: {
                            ...body,
                            author: formData.name,
                            comment: formData.comment,
                            id: productId,
                            parent: commentId
                        },
                        url: apiUrl,
                        keyFormRedux
                    });
                } catch (e) {
                    console.log(e.message);
                }
        }
    };

    const contentFromType = (typeForm) => {
        switch (typeForm) {
            case "callMe":
                return (
                    <div className="modal-popup__content">
                        <div className="offer-tariffs__modal-container">
                            <div className="offer-tariffs__modal-title">
                                {titleModal}
                            </div>
                            <a
                                href={`tel:${productTitle}`}
                                className="offer-tariffs__modal-name-tariff offer-tariffs__modal-name-tariff--link"
                            >
                                {productTitle}
                            </a>
                            <div className="modal-popup__or">{t("or")}</div>
                            <p className="modal-popup__recall-me">
                                {t("callMeBack")}
                            </p>
                            <p className="modal-popup__callme-schedule">
                                {t("supportSchedule")}
                            </p>
                            <form className="form___modal-form">
                                <InputPhone
                                    errors={errors}
                                    name="phone"
                                    value={formData.phone}
                                    labelText={`${t("phone")}*`}
                                    handleChange={handleForm}
                                />
                                {errorsFromServer ? (
                                    <small className="contacts__error-message-phone error-message">
                                        {errorsFromServer}
                                    </small>
                                ) : null}
                            </form>
                            <ButtonSubmitForm
                                titleButton={titleButton}
                                seoId={seoId}
                                isLoading={loading}
                                type={type}
                                submit={handleSubmit}
                                errors={errors}
                            />
                        </div>
                    </div>
                );
            case "contacts":
                return (
                    <div>
                        <div className="offer-tariffs__modal-container">
                            <div className="contacts__small-title">
                                {titleModal}
                            </div>
                            <form className="form___modal-form form___modal-form--contacts">
                                <InputPhone
                                    errors={errors}
                                    name="phone"
                                    value={formData.phone}
                                    labelText={`${t("phone")}*`}
                                    handleChange={handleForm}
                                />
                                <InputText
                                    labelText={t("streetHouse")}
                                    handleChange={handleForm}
                                    value={formData.address}
                                    name="address"
                                />

                                <InputMultiText
                                    labelText={t("message")}
                                    handleChange={handleForm}
                                    value={formData.comment}
                                    name="comment"
                                    rows={5}
                                />
                                {errorsFromServer ? (
                                    <small className="contacts__error-message-phone error-message">
                                        {errorsFromServer}
                                    </small>
                                ) : null}
                            </form>
                            <ButtonSubmitForm
                                titleButton={titleButton}
                                seoId={seoId}
                                isLoading={loading}
                                type={type}
                                submit={handleSubmit}
                                errors={errors}
                            />

                            <small className="contacts__small">{`${t(
                                "accordanceRequirementsLegislation"
                            )} ${t(
                                "accordanceRequirementsLegislationTime"
                            )}`}</small>
                        </div>
                    </div>
                );
            case "writeComment":
                return (
                    <div className="modal-popup__content modal-popup__content--write-comment">
                        <div className="modal-popup__write-comment-container">
                            <h3 className="offer-tariffs__modal-title">
                                {titleModal}
                            </h3>
                            <form className="form___modal-form">
                                <div className={Boolean(errorCommentForm.name.length)
                                    ? "form__field-block form__field-block--error"
                                    : null}>
                                    <InputText
                                        name="name"
                                        labelText={t("username")}
                                        handleChange={handleForm}
                                        value={formData.name}
                                        errors={errors}
                                    />
                                    {
                                        Boolean(errorCommentForm.name.length)
                                            ? <span className="subscribe-email__error-text">{errorCommentForm.name}</span>
                                            : null
                                    }
                                </div>
                                {errorsFromServer ? (
                                    <small className="contacts__error-message-phone error-message">
                                        {errorsFromServer}
                                    </small>
                                ) : null}
                                <div className={Boolean(errorCommentForm.comment.length)
                                    ? "form__field-block form__field-block--error"
                                    : null}>
                                    <InputMultiText
                                        labelText={t("comment")}
                                        handleChange={handleForm}
                                        value={formData.comment}
                                        name="comment"
                                        rows={9}
                                    />
                                    {
                                        Boolean(errorCommentForm.comment.length)
                                            ? <span className="subscribe-email__error-text">{errorCommentForm.comment}</span>
                                            : null
                                    }
                                </div>
                            </form>
                            <div className="modal-popup__button-group">
                                <button
                                    className="write-comment-close-btn"
                                    onClick={() => {
                                        clearModalAccept();
                                    }}
                                >{t('cancel')}</button>
                                <ButtonSubmitForm
                                    titleButton={titleButton}
                                    seoId={seoId}
                                    isLoading={loading}
                                    type={type}
                                    submit={handleSubmitComment}
                                    errors={errors}
                                    className='write-comment-submit-btn'
                                />
                            </div>
                        </div>
                    </div>

                )
            case "callMeHeader":
                return (
                    <>
                        <form className="form___modal-form">
                            <div className="support__callme-title">
                                {t("callMeBack")}
                            </div>
                            <p className="support__callme-schedule">
                                {t("supportSchedule")}
                            </p>
                            <InputPhoneHeader
                                errors={errors}
                                name="phone"
                                value={formData.phone}
                                labelText={`${t("phone")}*`}
                                handleChange={handleForm}
                                closeFormOnHeader={closeFormOnHeader}
                            />
                            {errorsFromServer ? (
                                <small className="contacts__error-message-phone error-message">
                                    {errorsFromServer}
                                </small>
                            ) : null}
                            <ButtonSubmitForm
                                type={type}
                                seoId={seoId}
                                submit={(e) => handleSubmit(e)}
                                titleButton={titleButton}
                            />
                        </form>
                    </>
                );
            case "ottForm":
                return (
                    <form
                        className="ott-form form___modal-form"
                        onSubmit={(e) => handleSubmit(e)}
                    >
                        <div className="ott-form__wrap">
                            <div className="ott-info__title ott-form__title">
                                {t("callMeBack")}
                            </div>
                            <InputPhoneOtt
                                errors={errors}
                                name="phone"
                                value={formData.phone}
                                labelText={`${t("ott.phoneNumber")}*`}
                                handleChange={handleForm}
                            />
                            <InputNameOtt
                                name="name"
                                labelText={t("ott.name")}
                                handleChange={handleForm}
                                value={formData.name}
                            />
                            <button
                                type="submit"
                                className="ott-form__submit"
                                disabled={loading || error}
                            >
                                {!loading ? (
                                    t("send")
                                ) : (
                                    <svg
                                        className="spinner"
                                        viewBox="0 0 50 50"
                                    >
                                        <circle
                                            className="path"
                                            cx="25"
                                            cy="25"
                                            r="20"
                                            fill="none"
                                            strokeWidth="5"
                                        />
                                    </svg>
                                )}
                            </button>
                        </div>
                        {errorsFromServer ? (
                            <small className="contacts__error-message-phone error-message">
                                {errorsFromServer}
                            </small>
                        ) : null}
                    </form>
                );
            default:
                return (
                    <div className="modal-popup__content">
                        <div className="offer-tariffs__modal-container">
                            <div className="offer-tariffs__modal-title">
                                {titleModal}
                            </div>
                            <span className="offer-tariffs__modal-name-tariff">
                                {titleProduct} {productTitle}
                            </span>
                            <form className="form___modal-form">
                                <InputPhone
                                    errors={errors}
                                    name="phone"
                                    value={formData.phone}
                                    labelText={`${t("phone")}*`}
                                    handleChange={handleForm}
                                />
                                <InputText
                                    name="name"
                                    labelText={t("howCanCallYou")}
                                    handleChange={handleForm}
                                    value={formData.name}
                                />
                                {errorsFromServer ? (
                                    <small className="contacts__error-message-phone error-message">
                                        {errorsFromServer}
                                    </small>
                                ) : null}
                            </form>
                            <ButtonSubmitForm
                                titleButton={titleButton}
                                seoId={seoId}
                                isLoading={loading}
                                type={type}
                                submit={handleSubmit}
                                errors={errors}
                            />
                        </div>
                    </div>
                );
        }
    };

    return <>{contentFromType(type)}</>;
};

const mapStateToProps = (state) => {
    return {
        state: state.candidacyBuyProduct,
        productInfo: state.shopProduct.product
    };
};

Form.propTypes = {
    titleButton: PropTypes.string,
    seoId: PropTypes.string,
    productTitle: PropTypes.string,
    titleModal: PropTypes.string,
    titleProduct: PropTypes.string,
    typeProduct: PropTypes.string,
    apiUrl: PropTypes.string,
    submitModalForm: PropTypes.func,
    body: PropTypes.shape({
        ghost: PropTypes.string,
        address: PropTypes.string,
    }),
    setModalAccept: PropTypes.func,
    clearApiCandidacy: PropTypes.func,
    clearModalAccept: PropTypes.func,
    type: PropTypes.string,
    keyFormRedux: PropTypes.string,
    state: PropTypes.shape({
        contacts: PropTypes.shape({
            loading: PropTypes.bool,
            error: PropTypes.bool,
            errorPhone: PropTypes.string,
            isInited: PropTypes.bool,
            errorMessage: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.object,
            ]),
            errorSecondSend: PropTypes.bool,
        }),
        comment: PropTypes.shape({

        }),
        default: PropTypes.shape({
            loading: PropTypes.bool,
            error: PropTypes.bool,
            errorPhone: PropTypes.string,
            isInited: PropTypes.bool,
            errorMessage: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.object,
            ]),
            errorSecondSend: PropTypes.bool,
        }),
        callMe: PropTypes.shape({
            loading: PropTypes.bool,
            error: PropTypes.bool,
            errorPhone: PropTypes.string,
            isInited: PropTypes.bool,
            errorMessage: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.object,
            ]),
            errorSecondSend: PropTypes.bool,
        }),
    }),
    closeFormOnHeader: PropTypes.func,
};
Form.defaultProps = {
    titleButton: "",
    seoId: "",
    productTitle: "",
    titleModal: "",
    titleProduct: "",
    typeProduct: "",
    apiUrl: "",
    submitModalForm: () => { },
    body: {},
    setModalAccept: () => { },
    clearApiCandidacy: () => { },
    clearModalAccept: () => { },
    type: "",
    keyFormRedux: "default",
    state: {},
    closeFormOnHeader: () => { },
};

export default connect(mapStateToProps, {
    submitModalForm,
    setModalAccept,
    clearApiCandidacy,
    clearModalAccept,
})(Form);
