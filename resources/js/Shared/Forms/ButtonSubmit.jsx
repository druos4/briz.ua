import React from "react";
import PropTypes from "prop-types";

const ButtonSubmit = ({
    titleButton,
    seoId,
    isLoading,
    type,
    submit,
    errors,
}) => {
    const classFromType = (typeButton) => {
        switch (typeButton) {
            case "contacts":
                return "contacts__custom-button";
            case "callMeHeader":
                return "support__callme-btn custom-button";
            default:
                return "modal-popup__button modal-popup__button--tariff-submit";
        }
    };
    return (
        <button
            id={seoId}
            className={
                isLoading
                    ? `custom-button custom-button--is-loading ${classFromType(
                          type
                      )}`
                    : `custom-button ${classFromType(type)}`
            }
            type="submit"
            onClick={(e) => submit(e)}
            disabled={isLoading || errors}
        >
            {isLoading ? (
                <svg className="spinner" viewBox="0 0 50 50">
                    <circle
                        className="path"
                        cx="25"
                        cy="25"
                        r="20"
                        fill="none"
                        strokeWidth="5"
                    />
                </svg>
            ) : (
                titleButton
            )}
        </button>
    );
};
ButtonSubmit.propTypes = {
    titleButton: PropTypes.string,
    seoId: PropTypes.string,
    isLoading: PropTypes.bool,
    type: PropTypes.string,
    submit: PropTypes.func,
    errors: PropTypes.string,
};
ButtonSubmit.defaultProps = {
    titleButton: "",
    seoId: "",
    isLoading: false,
    type: "",
    submit: () => {},
    errors: "",
};

export default ButtonSubmit;
