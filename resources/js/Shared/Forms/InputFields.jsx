import React from "react";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import InputMask from "react-input-mask";

const CssTextFieldHeader = withStyles({
    root: {
        "& input": {
            // height: 15,
        },

        "& label.Mui-focused": {
            color: "#00a3a4",
            fontSize: "14px",
        },

        "&:hover .MuiInputLabel-formControl": {
            color: "#424242",
            transition: ".2s ease-in",
        },

        "& .MuiOutlinedInput-root": {
            "& fieldset": {
                borderColor: "#B8B8B8",
                borderRadius: "10px",
            },
            "&:hover fieldset": {
                // borderColor: "#B8B8B8",
                borderColor: "#424242",
                transition: ".2s ease-in",
            },
            "&.Mui-focused fieldset": {
                borderColor: "#00a3a4",
                borderRadius: "10px",
            },
        },
    },
})(TextField);

const CssTextFieldOtt = withStyles({
    root: {
        "& input": {},
        "& label.Mui-focused": {
            color: "rgba(255, 255, 255, 0.8)",
        },
        "& .MuiInput-underline:after": {
            borderBottomColor: "rgba(255, 255, 255, 0.8)",
        },
        "& .MuiOutlinedInput-root": {
            "font-family": "Roboto, sans-serif",
            height: "50px",
            borderRadius: "3px",
            color: "rgba(255, 255, 255, 0.8)",
            "& fieldset": {
                borderColor: "rgba(255, 255, 255, 0.8)",
            },
            "&:hover fieldset": {
                borderColor: "rgba(255, 255, 255, 0.8)",
            },
            "&.Mui-focused fieldset": {
                borderColor: "rgba(255, 255, 255, 0.8)",
            },
        },
    },
})(TextField);

const CssTextField = withStyles({
    // input2: {
    //   height: 10
    // },

    root: {
        "& input": {
            height: 15,
        },
        "& label.Mui-focused": {
            color: "#00a3a4",
            fontSize: "16px",
        },

        "&:hover .MuiInputLabel-formControl": {
            color: "#424242",
            transition: ".2s ease-in",
        },

        "& .MuiOutlinedInput-root": {
            "& fieldset": {
                borderColor: "#B8B8B8",
                borderRadius: "10px",
            },
            "&:hover fieldset": {
                borderColor: "#424242",
                color: "#424242",
                transition: ".2s ease-in",
            },
            "&.Mui-focused fieldset": {
                borderColor: "#00a3a4",
                borderRadius: "10px",
            },
        },
    },
})(TextField);

const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexWrap: "wrap",
    },
    margin: {
        margin: 0,
    },
}));

export const InputPhone = ({
    errors,
    handleChange,
    value,
    labelText,
    name,
}) => {
    const classes = useStyles();

    return (
        <div
            className={
                errors
                    ? "contacts__field-block contacts__field-block--invalid-phone"
                    : "contacts__field-block"
            }
        >
            <InputMask
                onChange={(e) => handleChange(e)}
                name={name}
                mask="+38 099 99 99 999"
                maskChar={" ".trim()}
                value={value}
            >
                {() => (
                    <CssTextField
                        name={name}
                        autoComplete="off"
                        className={classes.margin}
                        label={labelText}
                        variant="outlined"
                    />
                )}
            </InputMask>
            {errors ? (
                <small className="contacts__error-message-phone error-message">
                    {errors}
                </small>
            ) : null}
        </div>
    );
};

export const InputText = ({ labelText, handleChange, value, name }) => {
    const classes = useStyles();
    return (
        <div className="contacts__field-block">
            <CssTextField
                className={classes.margin}
                label={labelText}
                autoComplete="off"
                variant="outlined"
                inputProps={{ maxLength: 255 }}
                onChange={(e) => handleChange(e)}
                value={value}
                name={name}
            />
        </div>
    );
};

export const InputMultiText = ({ labelText, handleChange, value, name, rows }) => {
    const classes = useStyles();
    return (
        <div className="contacts__field-block">
            <CssTextField
                className={classes.margin}
                label={labelText}
                variant="outlined"
                autoComplete="off"
                multiline
                rows={rows}
                inputProps={{ maxLength: 255 }}
                onChange={(e) => handleChange(e)}
                value={value}
                name={name}
            />
        </div>
    );
};

export const InputPhoneHeader = ({
    errors,
    handleChange,
    value,
    labelText,
    name,
    closeFormOnHeader,
}) => {
    const classes = useStyles();

    const handleFocus = (e) => {
        closeFormOnHeader(true);
    };

    const handleBlur = (e) => {
        closeFormOnHeader(false);
    };

    return (
        <div
            className={
                errors
                    ? "support__field-wrapper support__field-wrapper--invalid-phone"
                    : "support__field-wrapper"
            }
        >
            <InputMask
                onChange={(e) => handleChange(e)}
                name={name}
                mask="+38 099 99 99 999"
                maskChar={" ".trim()}
                value={value}
                onFocus={handleFocus}
                onBlur={handleBlur}
            >
                {() => (
                    <CssTextFieldHeader
                        name={name}
                        autoComplete="off"
                        className={classes.margin}
                        label={labelText}
                        variant="outlined"
                        margin="dense"
                    />
                )}
            </InputMask>
            {errors ? (
                <small className="contacts__error-message-phone error-message">
                    {errors}
                </small>
            ) : null}
        </div>
    );
};

export const InputPhoneOtt = ({
    errors,
    handleChange,
    value,
    labelText,
    name,
}) => {
    const classes = useStyles();
    return (
        <div
            className={
                errors
                    ? "contacts__field-block contacts__field-block--invalid-phone"
                    : "contacts__field-block"
            }
        >
            <InputMask
                onChange={(e) => handleChange(e)}
                name={name}
                mask="+38 099 99 99 999"
                maskChar={" ".trim()}
                value={value}
            >
                {() => (
                    <CssTextFieldOtt
                        name={name}
                        autoComplete="off"
                        className={classes.margin}
                        label={labelText}
                        variant="outlined"
                    />
                )}
            </InputMask>
            {errors ? (
                <small className="contacts__error-message-phone error-message">
                    {errors}
                </small>
            ) : null}
        </div>
    );
};

export const InputNameOtt = ({ labelText, handleChange, value, name }) => {
    const classes = useStyles();
    return (
        <div className="contacts__field-block">
            <CssTextFieldOtt
                className={classes.margin}
                label={labelText}
                variant="outlined"
                autoComplete="off"
                inputProps={{ maxLength: 255 }}
                onChange={(e) => handleChange(e)}
                value={value}
                name={name}
            />
        </div>
    );
};

InputText.propTypes = {
    labelText: PropTypes.string,
    handleChange: PropTypes.func,
    value: PropTypes.string,
    name: PropTypes.string,
};
InputText.defaultProps = {
    labelText: "",
    handleChange: () => {},
    value: "",
    name: "",
};

InputMultiText.propTypes = {
    labelText: PropTypes.string,
    handleChange: PropTypes.func,
    value: PropTypes.string,
    name: PropTypes.string,
};
InputMultiText.defaultProps = {
    labelText: "",
    handleChange: () => {},
    value: "",
    name: "",
};

InputNameOtt.propTypes = {
    labelText: PropTypes.string,
    handleChange: PropTypes.func,
    value: PropTypes.string,
    name: PropTypes.string,
};
InputNameOtt.defaultProps = {
    labelText: "",
    handleChange: () => {},
    value: "",
    name: "",
};
InputPhoneOtt.propTypes = {
    labelText: PropTypes.string,
    handleChange: PropTypes.func,
    value: PropTypes.string,
    name: PropTypes.string,
};
InputPhoneOtt.defaultProps = {
    labelText: "",
    handleChange: () => {},
    value: "",
    name: "",
};

InputPhoneOtt.propTypes = {
    labelText: PropTypes.string,
    handleChange: PropTypes.func,
    value: PropTypes.string,
    name: PropTypes.string,
    errors: PropTypes.string,
};
InputPhoneOtt.defaultProps = {
    labelText: "",
    handleChange: () => {},
    value: "",
    name: "",
    errors: "",
};

InputPhoneHeader.propTypes = {
    labelText: PropTypes.string,
    handleChange: PropTypes.func,
    value: PropTypes.string,
    name: PropTypes.string,
    errors: PropTypes.string,
    closeFormOnHeader: PropTypes.func,
};
InputPhoneHeader.defaultProps = {
    labelText: "",
    handleChange: () => {},
    value: "",
    name: "",
    errors: "",
    closeFormOnHeader: () => {},
};

InputPhone.propTypes = {
    labelText: PropTypes.string,
    handleChange: PropTypes.func,
    value: PropTypes.string,
    name: PropTypes.string,
    errors: PropTypes.string,
};
InputPhone.defaultProps = {
    labelText: "",
    handleChange: () => {},
    value: "",
    name: "",
    errors: "",
};
