/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

const SevenDays = ({ handleCloseSevenDays, currentLocale }) => {
    const { t } = useTranslation();
    const currentLang = currentLocale === 'ua' ? '' : `index-${currentLocale}.html`;

    return (
        <div className="seven-days__container">
            <a
                href={`https://stat.briz.ua/looppage/${currentLang}`}
                rel="nofollow"
                className="seven-days__text"
            >
                {t("sevenDaysService")}
            </a>
            <span
                className="seven-days__icon-close"
                onClick={() => handleCloseSevenDays(true)}
            />
        </div>
    );
}

SevenDays.propTypes = {
    handleCloseSevenDays: PropTypes.func,
    currentLocale: PropTypes.string
};
SevenDays.defaultProps = {
    handleCloseSevenDays: () => {},
    currentLocale: "ua"
};

export default SevenDays;
