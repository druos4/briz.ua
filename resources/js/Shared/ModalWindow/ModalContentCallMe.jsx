import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import ButtonCloseModal from "../ui/buttons/ButtonCloseModal";

const ModalContentCallMe = ({ handleCloseModal }) => {
    const { t } = useTranslation();
    return (
        <div className="modal-popup__content modal-popup__content--callme">
            <span className="modal-popup__callme-img-svg" />
            <div className="modal-popup__title">{t("ott.thankYou")}</div>
            <p className="modal-popup__text modal-popup__text-black">
                {t("ott.reqSent")}
            </p>
            <p className="modal-popup__aditional-description">
                {t("accordanceRequirementsLegislation")}{" "}
                <span className="modal-popup__aditional-description-schedule">
                    {t("accordanceRequirementsLegislationTime")}
                </span>
            </p>
            <ButtonCloseModal
                handleCloseModal={handleCloseModal}
                content={t("close")}
            />
        </div>
    );
};

ModalContentCallMe.propTypes = {
    handleCloseModal: PropTypes.func,
};

ModalContentCallMe.defaultProps = {
    handleCloseModal: () => {},
};
export default ModalContentCallMe;
