import React, { useEffect, useRef } from "react";
import ReactDOM from "react-dom";
import { motion } from "framer-motion";
import { useIsSsr } from '../../hooks/useIsSsr';

const ModalWindow = ({ showModalAccept, children, handleCloseModal }) => {
    const isSsr = useIsSsr();
    const modalContainer = !isSsr && document.getElementById("modal-root");
    const elRef = useRef(null);

    const handleClickOutside = (event) => {
        if (elRef && elRef.current === event.target) {
            handleCloseModal();
        }
    };

    const handleKeyDownEsc = (e) => {
        if (e.keyCode === 27) {
            handleCloseModal();
        }
    };

    useEffect(() => {
        document.addEventListener("click", handleClickOutside);
        document.addEventListener("keydown", handleKeyDownEsc);
        return () => {
            document.removeEventListener("click", handleClickOutside);
            document.removeEventListener("keydown", handleKeyDownEsc);
        };
    }, []);

    if (!modalContainer || !showModalAccept) {
        return null;
    }
    return ReactDOM.createPortal(
        <div className="modal-popup">
            <div className="modal-popup__overlay" ref={elRef}>
                <motion.div
                    className="modal-popup__window"
                    initial={{
                        // opacity: 0,
                        scale: 0.5,
                        y: "-50%",
                        x: "-50%",
                    }}
                    animate={{
                        opacity: 1,
                        scale: 1,
                        // making use of framer-motion spring animation
                        // with stiffness = 300
                        transition: {
                            type: "spring",
                            stiffness: 150,
                        },
                    }}
                    exit={{
                        // opacity: 0,
                        scale: 0.5,
                        y: "0",
                        x: "0",
                        transition: { duration: 0.6 },
                    }}
                >
                    <button
                        type="button"
                        aria-label="button close modal window"
                        className="modal-popup__window-close"
                        onClick={() => handleCloseModal({})}
                    />
                    {children}
                </motion.div>
            </div>
        </div>,
        modalContainer
    );
};

export default ModalWindow;
