import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import ButtonCloseModal from "../ui/buttons/ButtonCloseModal";

const ModalContentReqAccept = ({ handleCloseModal }) => {
    const { t } = useTranslation();
    return (
        <div className="modal-popup__content">
            <div className="modal-popup__title">{t("requestHasBeenSent")}</div>
            <p className="modal-popup__text ott-modal-popup__text">
                {t("abilityRequest")}{" "}
                <span className="modal-popup__text-bold">
                    {t("abilityRequestTime")}
                </span>
            </p>
            <ButtonCloseModal
                content={t("close")}
                handleCloseModal={handleCloseModal}
            />
        </div>
    );
};

ModalContentReqAccept.propTypes = {
    handleCloseModal: PropTypes.func,
};

ModalContentReqAccept.defaultProps = {
    handleCloseModal: () => {},
};

export default ModalContentReqAccept;
