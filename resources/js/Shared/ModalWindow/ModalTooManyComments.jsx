import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import ButtonCloseModal from "../ui/buttons/ButtonCloseModal";

const ModalTooManyComments = ({ handleCloseModal }) => {
    const { t } = useTranslation();
    return (
        <div className="modal-popup__content modal-popup__content--comment--to-many-comment">
            <div className="modal-popup__title--comment--to-many-comment">{t("alreadyRightComment")}</div>
            <p className="modal-popup__text modal-popup__text-comment--to-many-comment">
                {t("abilityToSendComment")}&nbsp;
                <strong>
                    {t("minute1")}
                </strong>
            </p>
            <span className="modal-popup__align-start">
                <ButtonCloseModal
                    handleCloseModal={handleCloseModal}
                    content={t("close")}
                />
            </span>
        </div>
    );
};

ModalTooManyComments.propTypes = {
    handleCloseModal: PropTypes.func
};

ModalTooManyComments.defaultProps = {
    handleCloseModal: () => {
    }
};
export default ModalTooManyComments;
