import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import ButtonCloseModal from "../ui/buttons/ButtonCloseModal";

const ModalResponseComment = ({ handleCloseModal }) => {
    const { t } = useTranslation();
    return (
        <div className="modal-popup__content modal-popup__content--comment">
            <span className="modal-popup__comment-img-svg" />
            <h2 className="modal-popup__title--comment">{t("done")}</h2>
            <p className="modal-popup__text modal-popup__text-comment">
                {t("modalDescriptionComment")}
            </p>
            {/* <p className="modal-popup__aditional-description">
                {t("accordanceRequirementsLegislation")}{" "}
                <span className="modal-popup__aditional-description-schedule">
                    {t("accordanceRequirementsLegislationTime")}
                </span>
            </p> */}
            <span className="modal-popup__align-center">
                <ButtonCloseModal
                    handleCloseModal={handleCloseModal}
                    content={t("close")}
                />
            </span>
        </div>
    );
};

ModalResponseComment.propTypes = {
    handleCloseModal: PropTypes.func,
};

ModalResponseComment.defaultProps = {
    handleCloseModal: () => {},
};
export default ModalResponseComment;
