import React from "react";
import PropTypes from "prop-types";

const Button = ({ content, handleCloseModal }) => {
    return (
        <button
            type="button"
            onClick={() => handleCloseModal(false, "")}
            className="custom-button modal-popup__button"
        >
            {content}
        </button>
    );
};
Button.propTypes = {
    content: PropTypes.string,
    handleCloseModal: PropTypes.func,
};

Button.defaultProps = {
    content: "",
    handleCloseModal: () => {},
};

export default Button;
