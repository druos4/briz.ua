/* eslint-disable no-shadow */
import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import ModalWindow from "./ModalWindow";
import { clearModalAccept, clearApiCandidacy } from "../../actions/actions";
import ModalResponseComment from './ModalResponseComment';
import ModalContentCallMe from "./ModalContentCallMe";
import ModalContentReqAccept from "./ModalContentReqAccept";
import ModalContentReqSent from "./ModalContentReqSent";
import ModalForm from "../Forms/ModalForm";
import ModalTooManyComments from "@/Shared/ModalWindow/ModalTooManyComments";

const Modal = ({
    clearApiCandidacy,
    clearModalAccept,
    showModalAccept,
    typeModalAccept,
    productTitle,
    productId,
    titleModal,
    titleButton,
    apiUrl,
    typeProduct,
    titleProduct,
    seoId,
    body,
    currentImgLink,
    keyFormRedux,
    commentId
}) => {
    const closeModal = () => {
        clearModalAccept();
        clearApiCandidacy();
    };
    const renderModalContent = (type) => {
        switch (type) {
            case "reqAccept":
                return <ModalContentReqAccept handleCloseModal={closeModal} />;
            case "reqSent":
                return <ModalContentReqSent handleCloseModal={closeModal} />;
            case "callMeHeader":
                return <ModalContentCallMe handleCloseModal={closeModal} />;
            case "reqSentComment":
                return <ModalResponseComment handleCloseModal={closeModal} />;
            case "toManyComments":
                return <ModalTooManyComments handleCloseModal={closeModal}/>
            case "modalForm":
                return (
                    <ModalForm
                        productTitle={productTitle}
                        productId={productId}
                        titleModal={titleModal}
                        titleButton={titleButton}
                        apiUrl={apiUrl}
                        typeProduct={typeProduct}
                        titleProduct={titleProduct}
                        seoId={seoId}
                        handleCloseModal={closeModal}
                        body={body}
                        keyFormRedux={keyFormRedux}
                    />
                );
            case "writeCommentBlog":
                return (
                    <ModalForm
                        titleModal={titleModal}
                        titleButton={titleButton}
                        apiUrl={apiUrl}
                        typeProduct={typeProduct}
                        titleProduct={titleProduct}
                        seoId={seoId}
                        handleCloseModal={closeModal}
                        body={body}
                        productId={productId}
                        commentId={commentId}
                        type="writeComment"
                        keyFormRedux={keyFormRedux}
                    />
                )
            case "modalFormCallMe":
                return (
                    <ModalForm
                        productTitle={productTitle}
                        productId={productId}
                        titleModal={titleModal}
                        titleButton={titleButton}
                        apiUrl={apiUrl}
                        typeProduct={typeProduct}
                        titleProduct={titleProduct}
                        seoId={seoId}
                        handleCloseModal={closeModal}
                        body={body}
                        type="callMe"
                        keyFormRedux={keyFormRedux}
                    />
                );
            case "modalImg":
                return (
                    <img
                        src={currentImgLink}
                        alt="doc"
                        className="documents__img-popup"
                    />
                );
            default:
                <h1>Content</h1>;
        }
        return "dddd";
    };
    return (
        <ModalWindow
            showModalAccept={showModalAccept}
            handleCloseModal={closeModal}
        >
            {renderModalContent(typeModalAccept)}
        </ModalWindow>
    );
};

const mapStateToProps = (state) => ({
    showModalAccept: state.modalAccept.showModalAccept,
    typeModalAccept: state.modalAccept.typeModalAccept,
    productTitle: state.modalAccept.productTitle,
    productId: state.modalAccept.productId,
    titleModal: state.modalAccept.titleModal,
    titleButton: state.modalAccept.titleButton,
    apiUrl: state.modalAccept.apiUrl,
    typeProduct: state.modalAccept.typeProduct,
    titleProduct: state.modalAccept.titleProduct,
    seoId: state.modalAccept.seoId,
    body: state.modalAccept.body,
    keyFormRedux: state.modalAccept.keyFormRedux,
    currentImgLink: state.modalAccept.currentImgLink,
    commentId: state.modalAccept.commentId,
});

Modal.propTypes = {
    keyFormRedux: PropTypes.string,
    currentImgLink: PropTypes.string,
    body: PropTypes.shape({
        ghost: PropTypes.string,
    }),
    seoId: PropTypes.string,
    titleProduct: PropTypes.string,
    typeProduct: PropTypes.string,
    apiUrl: PropTypes.string,
    titleButton: PropTypes.string,
    titleModal: PropTypes.string,
    productId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    commentId: PropTypes.number,
    productTitle: PropTypes.string,
    typeModalAccept: PropTypes.string,
    showModalAccept: PropTypes.bool,
    clearModalAccept: PropTypes.func,
    clearApiCandidacy: PropTypes.func,
};

Modal.defaultProps = {
    keyFormRedux: "",
    currentImgLink: "",
    body: {},
    seoId: "",
    titleProduct: "",
    typeProduct: "",
    apiUrl: "",
    titleButton: "",
    titleModal: "",
    productId: null,
    commentId: null,
    productTitle: "",
    typeModalAccept: "",
    showModalAccept: false,
    clearModalAccept: () => { },
    clearApiCandidacy: () => { },
};

export default connect(mapStateToProps, {
    clearModalAccept,
    clearApiCandidacy,
})(Modal);
