import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import ButtonCloseModal from "../ui/buttons/ButtonCloseModal";

const ModalContentReqSent = ({ handleCloseModal }) => {
    const { t } = useTranslation();
    return (
        <div className="modal-popup__content">
            <div className="modal-popup__title">{t("ott.thankYou")}</div>
            <p className="modal-popup__text ott-modal-popup__text">
                {t("ott.reqSent")}
            </p>
            <ButtonCloseModal
                handleCloseModal={handleCloseModal}
                content={t("close")}
            />
        </div>
    );
};
ModalContentReqSent.propTypes = {
    handleCloseModal: PropTypes.func,
};

ModalContentReqSent.defaultProps = {
    handleCloseModal: () => {},
};

export default ModalContentReqSent;
