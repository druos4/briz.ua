// import React from "react";
// import PropTypes from "prop-types";
// import { connect } from "react-redux";

// const Loader = ({ preload }) => {
//     if (!preload) {
//         document.body.style.overflow = "hidden";
//     } else {
//         document.body.style.overflow = "visible";
//     }
//     return (
//         <>
//             {!preload ? (
//                 <div className="preloader">
//                     <div className="wifi-loader">
//                         <svg className="circle-outer" viewBox="0 0 86 86">
//                             <circle className="back" cx="43" cy="43" r="40" />
//                             <circle className="front" cx="43" cy="43" r="40" />
//                             <circle className="new" cx="43" cy="43" r="40" />
//                         </svg>
//                         <svg className="circle-middle" viewBox="0 0 60 60">
//                             <circle className="back" cx="30" cy="30" r="27" />
//                             <circle className="front" cx="30" cy="30" r="27" />
//                         </svg>
//                         <svg className="circle-inner" viewBox="0 0 34 34">
//                             <circle className="back" cx="17" cy="17" r="14" />
//                             <circle className="front" cx="17" cy="17" r="14" />
//                         </svg>
//                         <svg className="circle" viewBox="0 0 34 34">
//                             <circle className="back" cx="17" cy="17" r="0.5" />
//                             <circle className="front" cx="17" cy="17" r="0.5" />
//                         </svg>
//                     </div>
//                 </div>
//             ) : null}
//         </>
//     );
// };

// const mapStateToProps = (state) => {
//     return {
//         preload: state.preload.loaded,
//     };
// };

// Loader.propTypes = {
//     preload: PropTypes.bool,
// };
// Loader.defaultProps = {
//     preload: false,
// };

// export default connect(mapStateToProps, null)(Loader);
