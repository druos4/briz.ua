/* eslint-disable no-nested-ternary */
/* eslint-disable react/no-danger */
/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import React, { useRef, useEffect, useState } from "react";
import PropTypes from "prop-types";
import SwiperCore, {
    Autoplay,
    Pagination,
    EffectFade,
    Navigation,
} from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { InertiaLink } from "@inertiajs/inertia-react";
import { useTranslation } from "react-i18next";
import SevenDays from "./SevenDays";
import { useMediaQuery } from "./MediaQuery/useMediaQuery";
import { hexToHSLHoverBtn, isIos } from "../utils";
import { useIsSsr } from "../hooks/useIsSsr";

const BannerSlider = ({ sliders, currentLocale }) => {
    SwiperCore.use([Autoplay, Pagination, Navigation]);
    SwiperCore.use(EffectFade);

    const isSsr = useIsSsr();
    const { t } = useTranslation();

    const swiperRef = useRef();
    const navContainer = useRef();
    const navContainerDesktop = useRef();

    const [isCloseSevenDays, setCloseSevenDays] = useState(true);
    const [touchSelectorNext, setTouchSelectorNext] = useState("");
    const [touchSelectorPrev, setTouchSelectorPrev] = useState("");

    const date = new Date();
    const currentDate = date.getDate();

    const isWidthMobile = useMediaQuery("(min-width: 360px)");
    const isWidthTablet = useMediaQuery("(min-width: 768px)");
    const isWidthDesktop = useMediaQuery("(min-width: 1025px)");

    const ios = !isSsr && isIos();

    useEffect(() => {
        const pagination = document.querySelector(".swiper-pagination");

        if (window.screen.availWidth > 1024) {
            pagination.classList.add("swiper-pagination-custom-desktop");
            const sliderWrapper = document.querySelector(".home__slider");
            sliderWrapper.insertBefore(pagination, null);
        }

        if (window.screen.availWidth < 1025 && !!navContainer.current) {
            navContainer.current.insertBefore(pagination, null);
        }

        if (currentDate < 8) {
            setCloseSevenDays(false);
        }
    }, []);

    const handleCloseSevenDays = (flag) => {
        setCloseSevenDays(flag);
    };

    const handleMouseOver = () => {
        swiperRef.current.swiper.autoplay.stop();
    };

    const handleMouseLeave = () => {
        swiperRef.current.swiper.autoplay.start();
    };

    const handleHover = (e, colorBtn) => {
        e.target.style.backgroundColor = hexToHSLHoverBtn(colorBtn);
    };

    const handleLeaveBtn = (e, colorBtn) => {
        e.target.style.backgroundColor = colorBtn;
    };

    return (
        <div
            onMouseOver={() => handleMouseOver()}
            onMouseLeave={() => handleMouseLeave()}
        >
            <Swiper
                // spaceBetween={50}
                ref={swiperRef}
                slidesPerView={1}
                // onSlideChange={(slide) => console.log(slide, 'SLIDE')}
                // onSwiper={(swiper) => console.log(swiper)}
                loop
                pagination={{
                    clickable: true,
                    type: "bullets",
                    bulletElement: "span",
                    // Пагинация в виде миниатюр на десктопной версии
                    // renderBullet: window.screen.availWidth > 1025 && function (index, className) {
                    //     return '\
                    //           <div class="box ' + className + '">\
                    //           <div class="bigNumber">' + `<img src=${sliders[index].picture_prev} width="100%" />` + '</div>\
                    //           </div>';

                    // },

                    // renderBullet: window.screen.availWidth > 1025 && function (index, className) {
                    //     return '\
                    //         <div class="box ' + className + '" >\
                    //         <div class="bigNumber">' + `
                    //             <svg width=${sqSize} height=${sqSize} viewBox="0 0 ${sqSize} ${sqSize}"  class="cirlce-svg-custom">
                    //                 <circle
                    //                     class="circle-background"
                    //                     cx=${sqSize / 2}
                    //                     cy=${sqSize / 2}
                    //                     r=${radius}
                    //                     strokeWidth=${strokeWidth}px
                    //                 />
                    //                 <circle
                    //                     class="circle-progress"
                    //                     cx=${sqSize / 2}
                    //                     cy=${sqSize / 2}
                    //                     r=${radius}
                    //                     strokeWidth=${strokeWidth}px
                    //                     style="stroke-dasharray: ${dashArray}; stroke-dashoffset: ${dashOffset}; transform: rotate(-90 ${sqSize / 2} ${sqSize / 2})"
                    //                 />
                    //     </svg> ` + '</div>\ </div>'
                    // },
                }}
                autoplay={{ delay: 5000, disableOnInteraction: true }}
                // effect={window.screen.availWidth < 1025 ? "" : "fade"}
                navigation={{
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                }}
            >
                {
                    <>
                        {
                            sliders.map(slider => {
                                return (
                                    <SwiperSlide key={slider.id}>
                                        <img
                                            src={
                                                // !!imageLoaded ? 
                                                // slider.pictureHd 
                                                // : 
                                                // imgPreload
                                                isWidthDesktop && slider.pictureHd || isWidthTablet && slider.pictureTablet || isWidthMobile && slider.picturePhone || ""

                                            }
                                            width={'100%'}
                                            height={'auto'}
                                            alt={`${slider.title} - інтернет-провайдер Briz в Одесі`}
                                            title={slider.title}
                                            // className={isSafari ? "slider-slick-content-image-is-safari" : ""}
                                        // onLoad={e => handleLoadImg(e)}
                                    />

                                    <div className="slider-slick-content-wrapper">
                                        <div className="slider-slick-content">
                                            <p
                                                className="slider-slick-title"
                                                style={{
                                                    color: slider.titleColor,
                                                }}
                                            >
                                                {slider.title}
                                            </p>
                                            <p
                                                style={{
                                                    color: slider.anonsColor,
                                                }}
                                                className={
                                                    ios
                                                        ? "slider-slick-description slider-slick-description--ios"
                                                        : "slider-slick-description"
                                                }
                                                dangerouslySetInnerHTML={{
                                                    __html: slider.anons,
                                                }}
                                            />
                                            {slider.show_btn ? (
                                                slider.url.substr(0, 4) ===
                                                "http" ? (
                                                    <a
                                                        className="slider-slick-link"
                                                        href={slider.url}
                                                        style={{
                                                            color: slider.btnTitleColor,
                                                            backgroundColor:
                                                                slider.btnBgColor,
                                                            transition:
                                                                "0.2s ease-in",
                                                        }}
                                                        onMouseOver={(e) =>
                                                            handleHover(
                                                                e,
                                                                slider.btnBgColor
                                                            )
                                                        }
                                                        onMouseLeave={(e) =>
                                                            handleLeaveBtn(
                                                                e,
                                                                slider.btnBgColor
                                                            )
                                                        }
                                                    >
                                                        {t("moreDetails")}
                                                    </a>
                                                ) : (
                                                    <a
                                                        className="slider-slick-link"
                                                        href={slider.url}
                                                        style={{
                                                            color: slider.btnTitleColor,
                                                            backgroundColor:
                                                                slider.btnBgColor,
                                                            transition:
                                                                "0.2s ease-in",
                                                        }}
                                                        onMouseOver={(e) =>
                                                            handleHover(
                                                                e,
                                                                slider.btnBgColor
                                                            )
                                                        }
                                                        onMouseLeave={(e) =>
                                                            handleLeaveBtn(
                                                                e,
                                                                slider.btnBgColor
                                                            )
                                                        }
                                                    >
                                                        {t("moreDetails")}
                                                    </a>
                                                )
                                            ) : null}
                                        </div>
                                    </div>
                                </SwiperSlide>
                            );
                        })}
                        <div
                            className="slider-slick-button-group"
                            ref={navContainer}
                        >
                            <span
                                className={`swiper-button-prev slider-slick-button-container slider-slick-button-container--right ${touchSelectorPrev}`}
                                onTouchStart={() =>
                                    setTouchSelectorPrev(
                                        "swiper-button-next--touch-start-prev"
                                    )
                                }
                                onTouchEnd={() => setTouchSelectorPrev("")}
                            >
                                <span className="slider-slick-arrow-right" />
                            </span>
                            <span
                                className={`swiper-button-next slider-slick-button-container slider-slick-button-container--left ${touchSelectorNext}`}
                                onTouchStart={() =>
                                    setTouchSelectorNext(
                                        "swiper-button-next--touch-start-next"
                                    )
                                }
                                onTouchEnd={() => setTouchSelectorNext("")}
                            >
                                <span className="slider-slick-arrow-left" />
                            </span>
                        </div>

                        <div
                            className="slider-slick-button-group-desctop"
                            ref={navContainerDesktop}
                        >
                            <span
                                className={`swiper-button-prev slider-slick-button-container-desctop slider-slick-button-container-desctop--right ${touchSelectorPrev}`}
                                onTouchStart={() =>
                                    setTouchSelectorPrev(
                                        "swiper-button-next--touch-start-prev"
                                    )
                                }
                                onTouchEnd={() => setTouchSelectorPrev("")}
                            >
                                <span className="slider-slick-arrow-right-desctop" />
                            </span>
                            <span
                                className={`swiper-button-next slider-slick-button-container-desctop slider-slick-button-container-desctop--left ${touchSelectorNext}`}
                                onTouchStart={() =>
                                    setTouchSelectorNext(
                                        "swiper-button-next--touch-start-next"
                                    )
                                }
                                onTouchEnd={() => setTouchSelectorNext("")}
                            >
                                <span className="slider-slick-arrow-left-desctop" />
                            </span>
                        </div>
                        {!isCloseSevenDays ? (
                            <div className="seven-days">
                                <SevenDays
                                    handleCloseSevenDays={handleCloseSevenDays}
                                    currentLocale={currentLocale}
                                />
                            </div>
                        ) : (
                            <div className="seven-days seven-days--close">
                                <SevenDays
                                    handleCloseSevenDays={handleCloseSevenDays}
                                    currentLocale={currentLocale}
                                />
                            </div>
                        )}
                    </>
                }
            </Swiper>
        </div>
    );
};

BannerSlider.propTypes = {
    sliders: PropTypes.arrayOf(PropTypes.object),
    currentLocale: PropTypes.string
};
BannerSlider.defaultProps = {
    sliders: [],
    currentLocale: "ua"
};

export default BannerSlider;