/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React from "react";
import { useTranslation } from "react-i18next";
import { InertiaLink } from "@inertiajs/inertia-react";
import myBriz from "../../images/home/my-briz.png";
import appBrizTv from "../../images/home/app-briztv.png";
import smartTv from "../../images/home/smart-tv.png";
import { useIsSsr } from '../hooks/useIsSsr';
import { isIos, isSafari } from '../utils';

const OurApps = ({currentLocale, currentLocaleHref}) => {
    const isSsr = useIsSsr();
    const { t } = useTranslation();

    const lang = currentLocale
    const langUS = lang === 'en' ? 'us' : lang;
    const langUK = lang === 'ua' ? 'uk' : lang;

    const ios = !isSsr && isIos();
    const safari = !isSsr && isSafari();

    return (
        <div className="our-apps">
            <ul className={ios || safari ? "our-apps__list our-apps__list--ios" : "our-apps__list"}>
                <li className={ios || safari ? "our-apps__item our-apps__item--ios" : "our-apps__item"}>
                    {/* <span className="our-apps__poster our-apps__poster--my-briz" /> */}
                    <img
                        src={myBriz}
                        alt="«Мій BRIZ» - особистий кабінет в смартфоні - інтернет-провайдер Briz в Одесі"
                        title="«Мій BRIZ» - особистий кабінет в смартфоні"
                        className="our-apps__poster"
                    />
                    <div className={ios || safari ? "our-apps__description-container our-apps__description-container--ios" : "our-apps__description-container"}>
                        <div className="our-apps__title">{t('myBrizApp')}</div>
                        <p className="our-apps__simple-txt">{t('convenientlyFundAccount')}</p>
                        <p className="our-apps__simple-txt our-apps__simple-txt--modif">{t('applicationFeatures')}</p>
                        <ul className="our-apps__inner-list">
                            <li className="our-apps__simple-txt">{t('checkingBalanceInApp')}</li>
                            <li className="our-apps__simple-txt">{t('paymentServicesWithoutCommissionApp')}</li>
                            <li className="our-apps__simple-txt">{t('paymentHistoryApp')}</li>
                            {/* <li className="our-apps__simple-txt">{t('connectionDisconnectionServicesApp')}</li> */}
                            <li className="our-apps__simple-txt">{t('communicationTechnicalSupportApp')}</li>
                            <li className="our-apps__simple-txt">{t('authorizationViaGoogAccountApp')}</li>
                            {/* <li className="our-apps__simple-txt">{t('connectionSettingsApp')}</li> */}
                        </ul>
                        <div className="our-apps__apps-miniatures">
                            <a
                                href={`https://play.google.com/store/apps/details?id=ua.briz.briz&hl=${langUK}`}
                                target="_blank"
                                className={`our-apps__apps-miniature our-apps__apps-miniature--google-play-${lang}`}
                            />
                            <a
                                href={`https://apps.apple.com/${langUS}/app/мой-briz/id1506187441`}
                                target="_blank"
                                className={`our-apps__apps-miniature our-apps__apps-miniature--app-store-${lang}`}
                            />
                        </div>
                    </div>
                </li>

                <li className={ios || safari ? "our-apps__item our-apps__item--ios" : "our-apps__item"}>
                    {/* <span className="our-apps__poster our-apps__poster--app-briztv" /> */}
                    <img
                        src={appBrizTv}
                        alt="app-briztv"
                        alt="ТВ на смартфоні і планшеті: додаток BrizTV - інтернет-провайдер Briz в Одесі"
                        title="ТВ на смартфоні і планшеті: додаток BrizTV"
                        className="our-apps__poster"
                    />
                    <div className={ios || safari ? "our-apps__description-container our-apps__description-container--ios" : "our-apps__description-container"}>
                        <div className="our-apps__title">{t('TVonSmartphoneTabletApp')}</div>
                        <p className="our-apps__simple-txt">
                            {t('favoriteFilmsProgramsNotOnlyTVApp')}
                        </p>
                        <p className="our-apps__simple-txt">
                            {t('clearMenuEasyNavigation')}
                        </p>

                        <div className="our-apps__apps-miniatures">
                            <a href={`https://play.google.com/store/apps/details?id=ua.briz.briztv&hl=${langUK}`}
                               target="_blank"
                               className={`our-apps__apps-miniature our-apps__apps-miniature--google-play-${lang}`}
                            />
                        </div>
                    </div>
                </li>

                <li className={ios || safari ? "our-apps__item our-apps__item--ios" : "our-apps__item"}>
                    {/* <span className="our-apps__poster our-apps__poster--smart-tv" /> */}
                    <img
                        src={smartTv}
                        alt="BRIZ SMART TV: цікаве та приємне дозвілля - інтернет-провайдер Briz в Одесі"
                        title="BRIZ SMART TV: цікаве та приємне дозвілля"
                        className="our-apps__poster"
                    />
                    <div
                        className={
                            ios || safari ?
                                "our-apps__description-container our-apps__description-container--ios"
                                :
                                "our-apps__description-container"
                            }
                        >
                        <div className="our-apps__title">
                            {t('interestingEnjoyableLeisure')}
                        </div>
                        <p className="our-apps__simple-txt">
                            {t('boxesSubscribersBreezeShoppingMall')}
                        </p>
                        <p className="our-apps__simple-txt">
                            {t('connectedOurNetworkApp')}
                        </p>
                        <p className="our-apps__simple-txt">
                            {t('NoConsoleNoproblemApp')}{' '}
                            <a href={`${currentLocaleHref}/equipment/iptv-pristavki`} className="our-apps__simple-link main-link">{t('onlineStoreApp')}{' '}</a>
                            {t('smartConsolesEveryTasteApp')}
                        </p>
                        <div className="our-apps__apps-miniatures">
                            <a
                                href={`https://play.google.com/store/apps/details?id=ua.briz.brizsmarttv&hl=${langUK}`}
                                target="_blank"
                                className={`our-apps__apps-miniature our-apps__apps-miniature--google-play-${lang}`}
                            />
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    )
}

export default OurApps;
