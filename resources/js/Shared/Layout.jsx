/* eslint-disable no-restricted-globals */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { I18nextProvider, initReactI18next } from "react-i18next";
import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { resources } from "@/i18n/i18n";
import {Provider} from "react-redux";
import Header from "./Header/Header";
import Footer from "./Footer";
import store from "../store";
import Wrapper from "./Wrapper";
import Breadcrumbs from "./Breadcrumbs";
import Modal from "./ModalWindow";
import {useIsSsr} from "../hooks/useIsSsr";

const Layout = ({children}) => {
    const isSsr = useIsSsr();
    const id = "page-wrap";
    const outer = "outer-container";
    const locale = children.props.locale || 'ua'

    useEffect(() => {
        if (
            /iPad|iPhone|iPod/.test(navigator.platform) ||
            (navigator.platform === "MacIntel" && navigator.maxTouchPoints > 1)
        ) {
            const css =
                "a:hover, button:hover {background-color: rgba(0, 0, 0, 0.03); border-radius: 10px; box-shadow: 0 0 8px 2px rgb(0 0 0 / 3%)} a, button, span {-webkit-tap-highlight-color: transparent;}";
            const head =
                document.head || document.getElementsByTagName("head")[0];
            const style = document.createElement("style");

            style.type = "text/css";
            if (style.styleSheet) {
                style.styleSheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }

            head.appendChild(style);
        }
    }, []);

    i18n.use(LanguageDetector)
        .use(initReactI18next).init({
        resources,
        lng: locale,
        ns: ["translations"],
        defaultNS: "translations",
        interpolation: {
            escapeValue: false,
            formatSeparator: ","
        },
        react: {
            wait: true
        }
    });

    return (
        <Provider store={store}>
            <I18nextProvider i18n={i18n}>
                <div className="layout" id={outer}>
                    {!isSsr && location.search !== "?only_content=1" ? (
                        <Header id={id} outer={outer} props={children.props}/>
                    ) : null}

                    {children.props.breadcrumbs &&
                    !!children.props.breadcrumbs.length ? (
                        <Breadcrumbs breadcrumbs={children.props.breadcrumbs}/>
                    ) : null}
                    <Wrapper>
                        <div className="main" id={id}>
                            {children}
                        </div>
                    </Wrapper>
                    {/* <a
                        target="_blanc"
                        className="givo-chat-container"
                        href={`https://onlinehelp.briz.ua/client.php?locale=${
                            !isSsr && document.querySelector("html").lang
                        }&style=briz`}
                    >
                        <span className="givo-chat" />
                    </a> */}
                    {!isSsr && location.search !== "?only_content=1" ? (
                        <Footer props={children.props}/>
                    ) : null}
                </div>
                <div id="modal-root"/>
                <Modal/>
            </I18nextProvider>
        </Provider>
    );
};

Layout.propTypes = {
    children: PropTypes.element,
};
Layout.defaultProps = {
    children: <></>,
};

export default Layout;
