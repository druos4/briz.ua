/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

const BlogMobileDropdown = ({ defaultText, optionsList }) => {
    const [defaultSelectTxt, setDefaultSelectTxt] = useState({});
    const [isOpen, setFlagOpen] = useState(false);

    useEffect(() => {
        setDefaultSelectTxt({ id: defaultText.slug, title: defaultText.title });
    }, []);

    const handleClickOutside = (e) => {
        if (
            !e.target.classList.contains("custom-select-option") &&
            !e.target.classList.contains("selected-text") &&
            !e.target.classList.contains("select-block") &&
            !e.target.classList.contains("select-icon") &&
            e.target.tagName !== "WTF"
        ) {
            setFlagOpen(false);
        }
    };

    useEffect(() => {
        setDefaultSelectTxt({ id: defaultText.slug, title: defaultText.title });
        document.addEventListener("mousedown", handleClickOutside);
        // setDefaultSelectTxt(defaultText);
        return () =>
            document.removeEventListener("mousedown", handleClickOutside);
    }, [defaultText]);

    const handleListDisplay = () => {
        setFlagOpen(!isOpen);
    };

    return (
        <div className="custom-select-container">
            <div
                className={isOpen ? "select-block active" : "select-block"}
                onClick={handleListDisplay}
            >
                <div
                    className={
                        isOpen ? "selected-text active" : "selected-text"
                    }
                >
                    {defaultSelectTxt.title}
                </div>
                <span
                    className={isOpen ? "select-icon active" : "select-icon"}
                />
            </div>

            {isOpen && (
                <ul className="select-options select-options-shop">
                    {!!optionsList.length &&
                        optionsList.map((option) => {
                            return (
                                <li
                                    className={
                                        option.selected
                                            ? "custom-select-option-li custom-select-option-li--active"
                                            : "custom-select-option-li"
                                    }
                                    key={option.id}
                                >
                                    <a
                                        className={
                                            option.slug === "top"
                                                ? "custom-select-option custom-select-option--top"
                                                : "custom-select-option"
                                        }
                                        href={option.url}
                                    >
                                        {option.title}
                                        {option.slug === "top" ? (
                                            <span className="custom-select-option--top-miniature" />
                                        ) : null}
                                    </a>
                                </li>
                            );
                        })}
                </ul>
            )}
        </div>
    );
};

BlogMobileDropdown.propTypes = {
    defaultText: PropTypes.shape({
        id: PropTypes.number,
        slug: PropTypes.string,
        title: PropTypes.string,
    }),
    optionsList: PropTypes.arrayOf(PropTypes.object),
};
BlogMobileDropdown.defaultProps = {
    defaultText: {},
    optionsList: [],
};

export default BlogMobileDropdown;
