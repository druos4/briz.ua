import React from 'react';
import { InertiaLink } from "@inertiajs/inertia-react";
import { useTranslation } from "react-i18next";

const BlogSocialsPanel = ({ page, currentLocale }) => {
    const { t } = useTranslation();

    return (
        <>
            <div className="blog__socials-title">{t('ourSocialNetworks')}</div>
            <ul className="blog__socials-list">
                <li className="blog__socials-item blog-cart__socials-item--pink">
                    <div className="blog-cart__socials-left">
                        <span className="blog-cart__socials-icon blog-cart__socials-icon--instagram" />
                        <div className="blog-cart__socials-title-subscribers">
                            <span className="blog-cart__socials-subscribers">
                                Instagram
                            </span>
                            <span className="blog-cart__socials-name">
                                briz_official
                            </span>
                        </div>
                    </div>
                    <a 
                        href="https://www.instagram.com/briz_official/" 
                        className="blog__socials-link-subscribe"
                        target="_blank"
                    >
                        {t('subscribe')}
                    </a>
                </li>
                <li className="blog__socials-item blog-cart__socials-item--lightblue-1">
                    <div className="blog-cart__socials-left">
                        <span className="blog-cart__socials-icon blog-cart__socials-icon--facebook" />
                        <div className="blog-cart__socials-title-subscribers">
                            <span className="blog-cart__socials-subscribers">
                                Facebook
                            </span>
                            <span className="blog-cart__socials-name">
                                brizodessa
                            </span>
                        </div>
                    </div>
                    <a 
                        href="https://www.facebook.com/brizodessa/" 
                        className="blog__socials-link-subscribe"
                        target="_blank"
                    >
                        {t('subscribe')}
                    </a>
                </li>
                <li className="blog__socials-item blog-cart__socials-item--lightblue-2">
                    <div className="blog-cart__socials-left">
                        <span className="blog-cart__socials-icon blog-cart__socials-icon--telegram" />
                        <div className="blog-cart__socials-title-subscribers">
                            <span className="blog-cart__socials-subscribers">
                                Telegram
                            </span>
                            <span className="blog-cart__socials-name">
                                brizinfo
                            </span>
                        </div>
                    </div>
                    <a 
                        href="https://t.me/brizinfo" 
                        className="blog__socials-link-subscribe"
                        target="_blank"
                    >
                        {t('subscribe')}
                    </a>
                </li>
                <li className="blog__socials-item blog-cart__socials-item--violet">
                    <div className="blog-cart__socials-left">
                        <span className="blog-cart__socials-icon blog-cart__socials-icon--viber" />
                        <div className="blog-cart__socials-title-subscribers">
                            <span className="blog-cart__socials-subscribers">
                                Viber
                            </span>
                            <span className="blog-cart__socials-name">
                                BRIZ INFO
                            </span>
                        </div>
                    </div>
                    <a
                        href={`https://invite.viber.com/?g2=AQBP6T8C0GnSf0zICnMwJglZHVgGiKRa7zlioveG8yysTGbXk39nRbU14kQCy1Wm&lang=${currentLocale}`}
                        className="blog__socials-link-subscribe"
                        target="_blank"
                    >
                        {t('subscribe')}
                    </a>
                </li>
            </ul>
        </>
    )
};

export default BlogSocialsPanel;