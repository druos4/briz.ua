import React from 'react';

// const pages = [
//     {
//         current: true,
//         title: 1,
//         url: "/ru/blog/ru-poleznye-sovety?page=1&sort=new",
//     },
//     {
//         current: false,
//         title: 2,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 3,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 4,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 5,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 6,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 7,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 8,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 9,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 10,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 11,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 12,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 13,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 14,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
//     {
//         current: false,
//         title: 15,
//         url: "/ru/blog/ru-poleznye-sovety?page=2&sort=new",
//     },
// ]

const BlogPagination = ({ pagination: { back, forward, pages } }) => {
    return (
        <>
            <div className="blog__pagination-to-back">
                {
                    back.disabled ?
                        <span
                            className="blog__pagination-link-to-back blog__pagination-link-to-back--disabled"
                        >
                            <span
                                className="blog__pagination-arrow-back blog__pagination-arrow-back--disabled"
                            />
                            {back.title}
                        </span>
                        :
                        <a
                            className="blog__pagination-link-to-back"
                            href={back.url}
                        >
                            <span
                                className="blog__pagination-arrow-back"
                            />
                            {back.title}
                        </a>
                }
            </div>
            <div className="blog__pagination-links">
                {
                    Boolean(pages) && Boolean(pages.length) && pages.map((page, index) => {
                        return (
                            page.current ?
                                <span
                                    key={index}
                                    className="blog__pagination-link blog__pagination-link--current"
                                >
                                    {page.title}
                                </span>
                                :
                                <a
                                    key={index}
                                    href={page.url}
                                    className="blog__pagination-link"
                                >
                                    {page.title}
                                </a>
                        )
                    })
                }
            </div>
            <div className="blog__pagination-to-forward">
                {
                    forward.disabled ?
                        <span
                            className="blog__pagination-link-to-forward blog__pagination-link-to-forward--disabled"
                        >
                            {forward.title}
                            <span
                                className="blog__pagination-arrow-forward blog__pagination-arrow-forward--disabled"
                            />
                        </span>
                        :
                        <a
                            className="blog__pagination-link-to-forward"
                            href={forward.url}
                        >
                            {forward.title}

                            <span
                                className="blog__pagination-arrow-forward"
                            />
                        </a>
                }

            </div>
        </>
    )
};

export default BlogPagination;