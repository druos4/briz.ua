import React, { useState } from 'react';
import axios from "axios";
import Autocomplete, {
    createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";
import { isIos } from '../../utils';
import { useIsSsr } from '../../hooks/useIsSsr';
import { motion } from "framer-motion";
import validator from 'validator';

const windowAvailWidth = typeof window !== 'undefined' && window.screen.availWidth;

const filter = createFilterOptions();

const CssTextField = withStyles({
    // input2: {
    //   height: 10
    // },

    root: {
        "& input": {
            height: 15,
        },

        '&:hover input::placeholder': {
            color: "#424242",
            opacity: 1,
            transition: ".2s ease-in",
        },

        '& .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"] .MuiAutocomplete-input':
        {
            paddingLeft: "35px",
        },

        "& label.Mui-focused": {
            color: "#00a3a4",
            fontSize: "16px",
        },

        "&:hover .MuiInputLabel-formControl": {
            color: "#424242",
            transition: ".2s ease-in",
        },

        "& .MuiOutlinedInput-root": {
            "& fieldset": {
                borderColor: "#B8B8B8",
                borderRadius: "10px",
            },
            "&:hover fieldset": {
                borderColor: "#424242",
                color: "#424242",
                transition: ".2s ease-in",
            },
            "&.Mui-focused fieldset": {
                borderColor: "#00a3a4",
                borderRadius: "10px",
            },
        },
    },
})(TextField);

const useStyles = makeStyles(() => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    margin: {
        margin: 0,
        width: "100%",
    },
}));

const BlogGmailModal = ({ callBackGmailModal, callBackCloseGmailModal }) => {
    const { t } = useTranslation();
    const isSsr = useIsSsr();
    const classes = useStyles();
    const [email, setEmail] = useState("");
    const [errorField, setErrorField] = useState('');

    const handleOutsideModal = () => {
        callBackCloseGmailModal(false);
    };

    const closeModal = () => {
        callBackCloseGmailModal(false);
    };

    const handleField = (e) => {
        setErrorField('');
        setEmail(e.target.value.toLowerCase());
    };

    const handleClickBtnEmail = (e) => {
        e.preventDefault();
        
        if (!validator.isEmail(email)) {
            setErrorField(t('formatError'));
        } else {
            // location.href=`${email}`
            console.log(email, 'EMAIL VALID')
            // try {
            //     await axios
            //         .post(`${currentLang()}/emails`, { email: email })
            //         .then((res) => {
            //             if (res.data.success) {
            //                 setSentEmailType('SENT_EMAIL');
            //             } else if (!res.data.success && res.data.hasOwnProperty('errors') && Array.isArray(res.data.errors.email)) {
            //                 setErrorField(res.data.errors.email[0]);
            //             } else {
            //                 setSentEmailType('REPEAT_SENT_EMAIL');
            //             }
            //         })
            //         .catch((e) => console.log(e.message));
            // } catch (e) {
            //     onsole.log(e.message);
            // }
        }
    };

    return (
        <div className="subscribe-email__modal-sent-email modal-popup">
            <div className="modal-popup__overlay" onClick={(e) => handleOutsideModal(e)}>
                <motion.div
                    className="modal-popup__window"
                    onClick={(e) => e.stopPropagation()}
                    initial={{
                        // opacity: 0,
                        scale: 0.5,
                        y: "-50%",
                        x: "-50%",
                    }}
                    animate={{
                        opacity: 1,
                        scale: 1,
                        // making use of framer-motion spring animation
                        // with stiffness = 300
                        transition: {
                            type: "spring",
                            stiffness: 150,
                        },
                    }}
                    exit={{
                        // opacity: 0,
                        scale: 0.5,
                        y: "0",
                        x: "0",
                        transition: { duration: 0.6 },
                    }}
                >
                    <button type="button" aria-label="button close modal window" className="modal-popup__window-close" onClick={() => closeModal()} />
                    <div className="modal-popup__content">
                        <div className="offer-tariffs__modal-title">{t('sendArticleToEmail')}</div>
                        <form onSubmit={(e) => handleClickBtnEmail(e)}>
                            <div className="subscribe-email__wrapper-fields">
                                <div className={Boolean(errorField.length) ? "subscribe-email__field-block subscribe-email__field-block--invalid-email" : "subscribe-email__field-block"}>
                                    <CssTextField
                                        className={classes.margin}
                                        label="E-mail"
                                        variant="outlined"
                                        inputProps={{ maxLength: 255 }}
                                        onChange={(e) => handleField(e)}
                                        value={email}
                                        name="email"
                                    />

                                    {
                                        Boolean(errorField.length) && <span className="subscribe-email__error-text">{errorField}</span>
                                    }
                                </div>
                                <button
                                    type="submit"
                                    className="custom-button subscribe-email__btn-subscribe"
                                >
                                    {t("send")}
                                </button>
                            </div>
                        </form>
                    </div>
                </motion.div>
            </div>
        </div>
    )
};

export default BlogGmailModal;