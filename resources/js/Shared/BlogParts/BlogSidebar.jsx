import React from 'react';
import { InertiaLink } from "@inertiajs/inertia-react";
import { useTranslation } from "react-i18next";

const BlogSidebar = ({ recomended, currentLocale, isShowingHeader }) => {
    const { t } = useTranslation();

    return (
        <aside className="blog-cart__aside">
            <div className="blog-cart__recomended">
                <div className="blog-cart__recomended-title">{t('recommendedArticles')}</div>
                <ul className="blog-cart__recomended-list">
                    {
                        Boolean(recomended) && Boolean(recomended.length) && recomended.map((articleItem, index) => {
                            return (
                                <li className="blog-cart__recomended-item" key={index}>
                                    <a className="blog-cart__recomended-link" href={articleItem.url}>
                                        {articleItem.title}
                                    </a>
                                    {
                                        Boolean(articleItem.read_time)
                                        &&
                                        <span className="blog-cart__recomended-time">
                                            {t('toRead')} {articleItem.read_time} {t('min')}
                                        </span>
                                    }

                                </li>
                            )
                        })
                    }
                </ul>
            </div>
            <div className={
                    isShowingHeader
                        ? "blog-cart__socials blog-cart__socials__sticky blog-cart__socials__sticky__with-header"
                        : "blog-cart__socials blog-cart__socials__sticky"
                }
            >
                <div className="blog-cart__socials-title">{t('ourSocialMedia')}</div>
                <ul className="blog-cart__socials-list">
                    <li className="blog-cart__socials-item blog-cart__socials-item--pink">
                        <div className="blog-cart__socials-left">
                            <span className="blog-cart__socials-icon blog-cart__socials-icon--instagram" />
                            <div className="blog-cart__socials-title-subscribers">
                                <span className="blog-cart__socials-subscribers">
                                    Instagram
                                </span>
                                <span className="blog-cart__socials-name">
                                    briz_official
                                </span>
                            </div>
                        </div>
                        <a 
                            href="https://www.instagram.com/briz_official/" 
                            className="blog-cart__socials-link-subscribe"
                            target="_blank"
                        >
                            {t('subscribe')}
                        </a>
                    </li>
                    <li className="blog-cart__socials-item blog-cart__socials-item--lightblue-1">
                        <div className="blog-cart__socials-left">
                            <span className="blog-cart__socials-icon blog-cart__socials-icon--facebook" />
                            <div className="blog-cart__socials-title-subscribers">
                                <span className="blog-cart__socials-subscribers">
                                    Facebook
                                </span>
                                <span className="blog-cart__socials-name">
                                    brizodessa
                                </span>
                            </div>
                        </div>
                        <a 
                            href="https://www.facebook.com/brizodessa/" 
                            className="blog-cart__socials-link-subscribe"
                            target="_blank"
                        >
                            {t('subscribe')}
                        </a>
                    </li>
                    <li className="blog-cart__socials-item blog-cart__socials-item--lightblue-2">
                        <div className="blog-cart__socials-left">
                            <span className="blog-cart__socials-icon blog-cart__socials-icon--telegram" />
                            <div className="blog-cart__socials-title-subscribers">
                                <span className="blog-cart__socials-subscribers">
                                    Telegram
                                </span>
                                <span className="blog-cart__socials-name">
                                    brizinfo
                                </span>
                            </div>
                        </div>
                        <a 
                            href="https://t.me/brizinfo" 
                            className="blog-cart__socials-link-subscribe"
                            target="_blank"
                        >
                            {t('subscribe')}
                        </a>
                    </li>
                    <li className="blog-cart__socials-item blog-cart__socials-item--violet">
                        <div className="blog-cart__socials-left">
                            <span className="blog-cart__socials-icon blog-cart__socials-icon--viber" />
                            <div className="blog-cart__socials-title-subscribers">
                                <span className="blog-cart__socials-subscribers">
                                    Viber
                                </span>
                                <span className="blog-cart__socials-name">
                                    BRIZ INFO
                                </span>
                            </div>
                        </div>
                        <a
                            href={`https://invite.viber.com/?g2=AQBP6T8C0GnSf0zICnMwJglZHVgGiKRa7zlioveG8yysTGbXk39nRbU14kQCy1Wm&lang=${currentLocale}`}
                            className="blog-cart__socials-link-subscribe"
                            target="_blank"
                        >
                            {t('subscribe')}
                        </a>
                    </li>
                </ul>
            </div>
        </aside>
    );
};

export default BlogSidebar;