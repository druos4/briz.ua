import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import axios from 'axios';
import { setModalAccept } from "@/actions/actions";
import imgNoComments from '../../../images/blog/no_comments.png';
import { useTranslation } from "react-i18next";
import { COMMENT_BLOG } from "../../constants/apiParams";

const BlogArticleComments = ({ setModalAccept, blogId, comments_count }) => {
    const { t } = useTranslation();

    const [comments, setComments] = useState([]);
    const [currentPage, setCurrentPage] = useState(0);
    const [hasMore, setHasMore] = useState(false);
    const [hasMoreCount, setHasMoreCount] = useState(0);

    useEffect(() => {
        let cleanupFunction = false;

        (async () => {
            try {
                await axios.get(`/blog/comments?id=${blogId}`).then((res) => {
                    if (!cleanupFunction) {
                        if (res.data.success && Object.keys(res.data.success).length) {
                            setComments(res.data.success.comments);
                            setCurrentPage(Number(res.data.success.currentPage));
                            setHasMore(res.data.success.hasMore);
                            setHasMoreCount(res.data.success.hasMoreCount);
                        }
                    }
                });
            } catch (e) {
                console.log(e.message);
            }
        })();

        return () => (cleanupFunction = true);
    }, []);

    const handleAppareModal = (e, titleModal, parentId) => {
        setModalAccept({
            showModalAccept: true,
            titleModal: titleModal,
            titleButton: t("send"),
            typeModalAccept: "writeCommentBlog",
            keyFormRedux: "comment",
            productId: blogId,
            commentId: parentId,
            apiUrl: COMMENT_BLOG.url,
        });
    };

    const handleLoadMoreComments = async () => {
        try {
            await axios.get(`/blog/comments?id=${blogId}&page=${currentPage + 1}`).then((res) => {
                if (Boolean(res.data) && res.data.success && Object.keys(res.data.success).length) {
                    setComments(prevState => [...prevState, ...res.data.success.comments]);
                    setCurrentPage(Number(res.data.success.currentPage));
                    setHasMore(res.data.success.hasMore);
                    setHasMoreCount(res.data.success.hasMoreCount);
                }
            });
        } catch (e) {
            console.log(e.message);
        }
    };

    return (
        <div className="blog-comments">
            <div className="blog-comments__wrapper">
                {
                    comments_count === 0 ?
                        <div className="blog-comments__no-comments">
                            <span className="blog-comments__count-text">
                                {t('comments')}{' '}
                                <span className="blog-comments__count-value">{comments_count}</span>
                            </span>
                            <p className="blog-comments__be-first-comment">{t('beFirstComment')}</p>
                            <img className="blog-comments__no-comments-img" src={imgNoComments} alt="" />
                            <button
                                type="button"
                                className="blog-comments__to-comment-btn"
                                onClick={(e) => handleAppareModal(e, t("writeComment"))}
                            >
                                {t('writeComment')}
                            </button>
                        </div>
                        :
                        <div className="blog-comments__have-comments">
                            <div className="blog-comments__top-line-block">
                                <span className="blog-comments__count-text blog-comments__count-text--have-comments">
                                    {t('comments')}{' '}
                                    <span className="blog-comments__count-value">{comments_count}</span>
                                </span>
                                <button
                                    type="button"
                                    className="blog-comments__to-comment-btn blog-comments__to-comment-btn--have-comments"
                                    onClick={(e) => handleAppareModal(e, t("writeComment"))}
                                >
                                    {t('writeComment')}
                                </button>
                            </div>
                            <div className="blog-comments__comments-block">
                                <ul className="blog-comments__comments-list">
                                    {
                                        comments.map((comment, index) => {

                                            return (
                                                <li className="blog-comments__comment-item" key={comment.id}>
                                                    <div className="blog-comments__comment-item-top">
                                                        <div className="blog-comments__userpick">{comment.picture}</div>
                                                        <div className="blog-comments__after-userpick">
                                                            <div className="blog-comments__author">{comment.author}</div>
                                                            <p className="blog-comments__comment-day">{comment.day}</p>
                                                        </div>
                                                    </div>
                                                    <p className="blog-comments__comment-text">{comment.comment}</p>
                                                    <button
                                                        className="blog-comments__repeat-btn"
                                                        onClick={(e) => handleAppareModal(e, t("replyComment"), comment.id)}
                                                    >
                                                        {t('toAnswer')}
                                                    </button>

                                                    {
                                                        Boolean(comment.children.length)
                                                        &&
                                                        <ul className="blog-comments__inner-list">
                                                            {
                                                                comment.children.map((innerComment, index) => {
                                                                    return (
                                                                        <li className="blog-comments__comment-item blog-comments__comment-item-inner" key={index}>
                                                                            <div className="blog-comments__comment-item-top">
                                                                                <div className="blog-comments__userpick">{innerComment.picture}</div>
                                                                                <div className="blog-comments__after-userpick">
                                                                                    <div className="blog-comments__author">{innerComment.author}</div>
                                                                                    <p className="blog-comments__comment-day">{innerComment.day}</p>
                                                                                </div>
                                                                            </div>
                                                                            <p className="blog-comments__comment-text">{innerComment.comment}</p>
                                                                        </li>
                                                                    )
                                                                })
                                                            }
                                                        </ul>
                                                    }
                                                </li>
                                            )
                                        })
                                    }
                                </ul>

                                {
                                    hasMore
                                    &&
                                    <span
                                        className="blog-comments__load-more-btn"
                                        onClick={() => handleLoadMoreComments()}
                                    >
                                        {t('loadMore')}
                                    </span>
                                }
                            </div>
                        </div>
                }
            </div>

        </div>
    )
};

export default connect(null, { setModalAccept })(BlogArticleComments);
