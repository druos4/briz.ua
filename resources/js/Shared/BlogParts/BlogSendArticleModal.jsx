import React from 'react';
import { motion } from "framer-motion";
import { useTranslation } from "react-i18next";

const BlogSendArticleModal = ({ callBackCloseModal, callBackGmailModal }) => {
    const { t } = useTranslation();

    const handleOutsideModal = () => {
        callBackCloseModal(false);
    };

    const closeModal = () => {
        callBackCloseModal(false);
    };
console.log(location, 'LOCATION')
    const handleRunPopupEmeil = () => {
        callBackGmailModal(true);
        callBackCloseModal(false);
    }

    return (
        <div className="subscribe-email__modal-sent-email modal-popup">
            <div className="modal-popup__overlay" onClick={(e) => handleOutsideModal(e)}>
                <motion.div
                    className="modal-popup__window"
                    onClick={(e) => e.stopPropagation()}
                    initial={{
                        // opacity: 0,
                        scale: 0.5,
                        y: "-50%",
                        x: "-50%",
                    }}
                    animate={{
                        opacity: 1,
                        scale: 1,
                        // making use of framer-motion spring animation
                        // with stiffness = 300
                        transition: {
                            type: "spring",
                            stiffness: 150,
                        },
                    }}
                    exit={{
                        // opacity: 0,
                        scale: 0.5,
                        y: "0",
                        x: "0",
                        transition: { duration: 0.6 },
                    }}
                >
                    <button type="button" aria-label="button close modal window" className="modal-popup__window-close" onClick={() => closeModal()} />
                    <div className="modal-popup__content modal-popup__content--comment">
                        <div className="modal-popup__title modal-popup__title--send-socials">{t('submitArticleTitle')}</div>
                        <p className="subscribe-email__modal-text">
                            {t('submitArticleText')}
                        </p>
                        <p className="subscribe-email__modal-bolt-text">
                            {t('submitArticleTo')}
                        </p>
                        <ul className="subscribe-email__list-socials">
                            <li className="subscribe-email__item-social">
                                <a href={`https://www.facebook.com/sharer/sharer.php?u=${location.href}`} className="subscribe-email__link-social" target="_blank">
                                    <span className="subscribe-email__icon-social subscribe-email__icon-social--facebook" />
                                    <span className="subscribe-email__text-social">Facebook</span>
                                </a>
                            </li>
                            <li className="subscribe-email__item-social">
                                <a href={`https://telegram.me/share/url?url=${location.href}`} className="subscribe-email__link-social" target="_blank">
                                    <span className="subscribe-email__icon-social subscribe-email__icon-social--telegram" />
                                    <span className="subscribe-email__text-social">Telegram</span>
                                </a>
                            </li>
                            <li className="subscribe-email__item-social">
                                <a
                                    href={
                                        `https://www.facebook.com/dialog/send?app_id=478698489460946&link=${location.href}&redirect_uri=${location.origin}`
                                    }
                                    className="subscribe-email__link-social"
                                    target="_blank"
                                >
                                    <span className="subscribe-email__icon-social subscribe-email__icon-social--messenger" />
                                    <span className="subscribe-email__text-social">Messenger</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </motion.div>
            </div>
        </div>
    )
};

export default BlogSendArticleModal;