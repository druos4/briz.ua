import React, { useEffect, useRef, } from "react";
import { useTranslation } from "react-i18next";
import { Inertia } from "@inertiajs/inertia";
import { InertiaLink } from "@inertiajs/inertia-react";
import { categoryTitleCompare } from "@/utils";

const BlogArticlesList = ({ data }) => {
    const { t } = useTranslation();
    
    const handleCustomLink = (e, url) => {
        e.preventDefault();
        // Inertia.get(url, {}, { preserveState: true });
        Inertia.visit(url);
    }

    return (
        <ul className="blog__list-articles">
            {
                data.map((article, index) => {
                    
                    return (
                        <li className="blog__item-article" key={index}>
                            <a href={article.url} className="blog__link-article">
                                <img src={article.picture} alt={`${article.title} - інтернет-провайдер Briz в Одесі`} title={article.title} className="blog__item-img" />
                                <div className="blog__content-article">
                                    <ul className="blog__theme-list-links">
                                        {
                                            article.categories && Boolean(article.categories.length) && article.categories
                                                .sort(categoryTitleCompare)
                                                .map((category, index) => {
                                                    return (
                                                        <li className={category.title.length >= 20
                                                            ? "blog__theme-item-link blog__theme-item-link--to-big"
                                                            : "blog__theme-item-link"}
                                                            key={index}
                                                            onClick={(e) => handleCustomLink(e, category.url)}
                                                        >
                                                            {category.title}
                                                        </li>
                                                    );
                                                })
                                        }
                                    </ul>
                                    <div className="blog__content-article-title">{article.title}</div>
                                    {
                                        Boolean(article.read_time) && <div className="blog__content-article-readtime">{t('toRead')} {article.read_time} {t('min')}</div>
                                    }
                                    <div className="blog__content-article-anons">{article.anons}</div>
                                    <div className="blog__article-info">
                                        <span className="blog__article-date">{article.published_at}</span>
                                        <div className="blog__article-info-block">
                                            <div className="blog__article-like-item">
                                                <span className="blog__article-like-icon" />
                                                <span className="blog__article-like-value">{article.likes_count}</span>
                                            </div>
                                            <div className="blog__article-view-item">
                                                <span className="blog__article-view-icon" />
                                                <span className="blog__article-view-value">{article.views_count}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    )
                })
            }
        </ul>
    )
};

export default BlogArticlesList;