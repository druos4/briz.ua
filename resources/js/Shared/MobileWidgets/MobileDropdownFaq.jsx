/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { InertiaLink } from "@inertiajs/inertia-react";
import { Inertia } from "@inertiajs/inertia";
import { useTranslation } from "react-i18next";

const MobileDropdownFaq = ({
    defaultText,
    optionsList,
    answer
}) => {
    const { t } = useTranslation();
    const [defaultSelectTxt, setDefaultSelectTxt] = useState({});
    const [isOpen, setFlagOpen] = useState(false);

    useEffect(() => {
        if (answer) {
            setDefaultSelectTxt({ id: answer.id, title: answer.title });
        } else {
            setDefaultSelectTxt({ id:null, title: t('chooseTheQuestion') })
        }
        // Inertia.get(optionsList[0].url, {}, { preserveState: true });
    }, [t('chooseTheQuestion')]);

    const handleClickOutside = (e) => {
        if (
            !e.target.classList.contains("custom-select-option") &&
            !e.target.classList.contains("selected-text") &&
            !e.target.classList.contains("select-block") &&
            !e.target.classList.contains("select-icon") &&
            e.target.tagName !== "WTF"
        ) {
            setFlagOpen(false);
        }
    };

    useEffect(() => {
        if (!!defaultText) {
            setDefaultSelectTxt({ id: defaultText.id, title: defaultText.title });
        }
        document.addEventListener("mousedown", handleClickOutside);
        
        return () =>
            document.removeEventListener("mousedown", handleClickOutside);
    }, [defaultText]);

    const handleListDisplay = () => {
        setFlagOpen(!isOpen);
    };

    const handleOptionClick = (e, selected) => {
        Inertia.get(selected.url, {}, { preserveState: true });
        setDefaultSelectTxt({id: selected.id, title: selected.title});
        setFlagOpen(false);
    };
    
    return (
        <div className="custom-select-container">
            <div
                className={isOpen ? "select-block active" : "select-block"}
                onClick={handleListDisplay}
            >
                <div
                    className={
                        defaultSelectTxt.title === t("chooseTheQuestion")
                            ? "default-text"
                            : (isOpen
                                ? "selected-text active"
                                : "selected-text")
                    }
                >
                    {defaultSelectTxt.title}
                </div>
                <span
                    className={isOpen ? "select-icon active" : "select-icon"}
                />
            </div>

            {isOpen && (
                <ul className="select-options">
                    {optionsList.map((option) => {
                        return (
                            <li
                                className="custom-select-option-li"
                                key={option.id}
                            >
                                <span
                                    className={
                                        defaultSelectTxt.id === option.id
                                            ? "custom-select-option custom-select-option--active"
                                            : "custom-select-option"
                                    }
                                    href={option.url}
                                    onClick={(e) =>
                                        handleOptionClick(e, option)
                                    }
                                >
                                    {option.title}
                                </span>
                            </li>
                        );
                    })}
                </ul>
            )}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        currentPage: state.currentPage.currentPage,
    };
};

MobileDropdownFaq.propTypes = {
    defaultText: PropTypes.shape({
        id: PropTypes.number,
        slug: PropTypes.string,
        title: PropTypes.string,
        url: PropTypes.string,
    }),
    optionsList: PropTypes.arrayOf(PropTypes.object),
    answer: PropTypes.object
};
MobileDropdownFaq.defaultProps = {
    defaultText: null,
    optionsList: [],
    answer: undefined,
};

export default connect(mapStateToProps, null)(MobileDropdownFaq);
