// import React from 'react';
// import { Inertia } from '@inertiajs/inertia';
// import { useTranslation } from "react-i18next";
// import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
// import TextField from '@material-ui/core/TextField';
// import {
//     withStyles,
//     makeStyles,
// } from '@material-ui/core/styles';

// const filter = createFilterOptions();

// const CssTextField = withStyles({

//     // input2: {
//     //   height: 10
//     // },

//     root: {
//         '& input': {
//             height: (window.screen.availWidth < 768 && 7) || (window.screen.availWidth >= 768 && 15),
//         },

//         '& .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"] .MuiAutocomplete-input': {
//             paddingLeft: "35px"
//         },

//         '& label.Mui-focused': {
//             color: '#00a3a4',
//             fontSize: "16px",
//         },

//         '& .MuiInputLabel-outlined': {
//             transform: window.screen.availWidth < 768 ? 'translate(14px, 14px) scale(1)' : 'translate(14px, 18px) scale(1)'
//         },

//         '& .MuiInputLabel-outlined.MuiInputLabel-shrink': {
//             transform: 'translate(14px, -7px) scale(0.75)'
//         },

//         '&:hover .MuiInputLabel-formControl': {
//             color: '#424242',
//             transition: '.2s ease-in'
//         },

//         '& .MuiOutlinedInput-root': {
//             '& fieldset': {
//                 borderColor: '#B8B8B8',
//                 borderRadius: "10px"
//             },
//             '&:hover fieldset': {
//                 borderColor: '#424242',
//                 color: '#424242',
//                 transition: '.2s ease-in'
//             },
//             '&.Mui-focused fieldset': {
//                 borderColor: '#00a3a4',
//                 borderRadius: "10px"
//             },
//         },
//     },
// })(TextField);

// const useStyles = makeStyles(() => ({
//     root: {
//         display: 'flex',
//         flexWrap: 'wrap',
//     },
//     margin: {
//         margin: 0
//     },
// }));

// const MobileFaqAutocomplete = ({uniqueQuestions, valueQuestion, callbackValueQuestion}) => {
//     const { t } = useTranslation();
//     const classes = useStyles();

//     const handleChangeQuestion = (e) => {
//         if (!uniqueQuestions.length) {
//             callbackValueQuestion({ val: e.target.value });
//         }
//     };

//     return (
//         <Autocomplete
//             id="questions"
//             freeSolo
//             options={uniqueQuestions.map(question => question)}
//             // onInputChange={
//             //     (e, value, reason) => clearHandlerBuilding(e, value, reason)
//             // }
//             getOptionLabel={(option) => {
//                 return option.title;
//             }}

//             className="faq__autocomplete-search-question"
//             renderInput={(params) => <CssTextField
//                 required={true}
//                 {...params}
//                 onChange={handleChangeQuestion}
//                 className={classes.margin}
//                 placeholder={t('cannotFindAnswer')}
//                 variant="outlined"
//                 name="question"
//                 type="text"
//                 inputProps={{ ...params.inputProps, maxLength: 99 }}
//                 onFocus={(e) => e.target.placeholder = ''}
//                 onBlur={(e) => e.target.placeholder = t('cannotFindAnswer')}
//             />
//             }
//             // renderOption={(option) => option.dom}
//             value={valueQuestion}
//             onChange={(event, newValue) => {
//                 if (!!newValue) {
//                     const url = `${newValue.url}`;
//                     if (window.location.pathname !== url) {
//                         // savePartLink(newValue.slug);
//                         Inertia.visit(url);
//                     }

//                     callbackValueQuestion(newValue);
//                 }
//             }}
//             filterOptions={(options, params) => {
//                 return filter(options, params);
//             }}
//             selectOnFocus
//             clearOnBlur
//             handleHomeEndKeys
//         />
//     )
// };

// export default MobileFaqAutocomplete;