/* eslint-disable no-nested-ternary */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable no-shadow */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { InertiaLink } from "@inertiajs/inertia-react";
import { getMenu } from "@/actions/actions";
import { useIsSsr } from "@/hooks/useIsSsr";
import { isIos, isSafari } from "@/utils";

const MobileSidebar = ({ getMenu, clickedMobileItem, isOpen, mainMenu }) => {
    const isSsr = useIsSsr();
    const { t } = useTranslation();

    const ios = !isSsr && isIos();
    const safari = !isSsr && isSafari();

    const lang = !isSsr && app.getLang();

    let actualLang = "";

    if (lang === "ua" || lang === "en") {
        actualLang = "ua";
    } else {
        actualLang = "ru";
    }

    const handleOptionClick = (lang) => {
        localStorage.setItem("i18nextLng", lang);
        window.location.href = `/lang/${lang}`;
    };

    const handleClickLink = () => {
        clickedMobileItem(false);
    };

    return (
        <div
            style={ios || safari ? { height: "100vh" } : { height: "100%" }}
            className={
                isOpen
                    ? "mobile-sidebar__container  mobile-sidebar__container--visible"
                    : "mobile-sidebar__container mobile-sidebar__container--hidden"
            }
        >
            <div className="mobile-sidebar__first-section">
                <ul className="mobile-sidebar__languages">
                    <li className="mobile-sidebar__language">
                        <button
                            type="button"
                            className={
                                !isSsr &&
                                localStorage.getItem("i18nextLng") === "ua"
                                    ? "mobile-sidebar__language-link mobile-sidebar__language-link--active"
                                    : "mobile-sidebar__language-link"
                            }
                            onClick={() => handleOptionClick("ua")}
                        >
                            UA
                        </button>
                    </li>
                    <li className="mobile-sidebar__language">
                        <button
                            type="button"
                            className={
                                !isSsr &&
                                localStorage.getItem("i18nextLng") === "ru"
                                    ? "mobile-sidebar__language-link mobile-sidebar__language-link--active"
                                    : "mobile-sidebar__language-link"
                            }
                            onClick={() => handleOptionClick("ru")}
                        >
                            RU
                        </button>
                    </li>
                    <li className="mobile-sidebar__language">
                        <button
                            type="button"
                            className={
                                !isSsr &&
                                localStorage.getItem("i18nextLng") === "en"
                                    ? "mobile-sidebar__language-link mobile-sidebar__language-link--active"
                                    : "mobile-sidebar__language-link"
                            }
                            onClick={() => handleOptionClick("en")}
                        >
                            ENG
                        </button>
                    </li>
                </ul>
                <div className="mobile-sidebar__account-link">
                    <a
                        href={`https://stat.briz.ua/?lang=${lang}`}
                        className="header__account-link"
                        target="_blank"
                        rel="nofollow"
                    >
                        {/* <img src={userIcon} alt="user-icon" className="header__user-icon" /> */}
                        <span className="header__user-icon" />
                        <span className="header__account-texyt">
                            {t("personalArea")}
                        </span>
                    </a>
                </div>
                <div className="mobile-sidebar__tv">
                    <a
                        href="http://tv.briz.ua/"
                        className="mobile-sidebar__tv-link"
                    >
                        <span className="mobile-sidebar__tv-icon" />
                        <span className="mobile-sidebar__iptv-briz">
                            IPTV Briz
                        </span>
                    </a>

                    {/* <a href="#" className="header__account-link">
                        <span className="header__user-icon" />
                        <span className="header__account-texyt">Личный кабинет</span>
                    </a> */}
                </div>
                {/* <div className="mobile-sidebar__givo-chat">
                    <a href={`https://onlinehelp.briz.ua/client.php?locale=${document.querySelector('html').lang}&style=briz`}
                       className="mobile-sidebar__givo-chat-link" rel="nofollow">
                        <span className="mobile-sidebar__givo-chat-icon"/>
                        <span className="mobile-sidebar__givo-chat-text">{t('onlineChat')}</span>
                    </a>
                </div> */}
            </div>
            <div className="mobile-sidebar__main-menu">
                {mainMenu.menu &&
                    mainMenu.menu.length &&
                    mainMenu.menu.map((itemMenu, index) => {
                        return itemMenu.chilren.length ? (
                            <div
                                key={index}
                                className="mobile-sidebar__has-inner-block"
                            >
                                <span className="mobile-sidebar__has-inner-block-title">
                                    {itemMenu.title}
                                </span>
                                {itemMenu.chilren.map((child, index) => {
                                    return child.blank ? (
                                        <a
                                            className="mobile-sidebar__has-inner-block-link"
                                            href={child.url}
                                        >
                                            {child.title}
                                        </a>
                                    ) : (
                                        <a
                                            key={index}
                                            href={child.url}
                                            className="mobile-sidebar__has-inner-block-link"
                                            onClick={handleClickLink}
                                        >
                                            {child.title}
                                        </a>
                                    );
                                })}
                            </div>
                        ) : (
                            <div
                                key={index}
                                className={`mobile-sidebar__no-inner-block mobile-sidebar__no-inner-block--${index}`}
                            >
                                {itemMenu.url.includes("shop.briz.ua") ? (
                                    <a
                                        className="mobile-sidebar__no-inner-block-link"
                                        href={itemMenu.url}
                                    >
                                        {itemMenu.title}
                                    </a>
                                ) : itemMenu.url.includes("help.briz.ua") ? (
                                    <a
                                        href={`${itemMenu.url}${actualLang}/`}
                                        className="mobile-sidebar__no-inner-block-link"
                                    >
                                        {itemMenu.title}
                                    </a>
                                ) : (
                                    <a
                                        className="mobile-sidebar__no-inner-block-link"
                                        href={itemMenu.url}
                                        onClick={handleClickLink}
                                    >
                                        {itemMenu.title}
                                    </a>
                                )}
                            </div>
                        );
                    })}
            </div>
            <div
                className="mobile-sidebar__contacts"
                itemScope
                itemType="http://schema.org/Organization"
            >
                <span itemProp="name" hidden>
                    Briz
                </span>
                <ul className="mobile-sidebar__contacts-list">
                    <li className="mobile-sidebar__contacts-item">
                        <a
                            className="mobile-sidebar__contacts-link"
                            onClick={handleClickLink}
                            href="tel:0487972525"
                            itemProp="telephone"
                        >
                            048 797 25 25
                        </a>
                    </li>
                    <li className="mobile-sidebar__contacts-item">
                        <a
                            className="mobile-sidebar__contacts-link"
                            onClick={handleClickLink}
                            href="tel:0487291122"
                            itemProp="telephone"
                        >
                            048 729 11 22
                        </a>
                    </li>
                    <li className="mobile-sidebar__contacts-item">
                        <a
                            className="mobile-sidebar__contacts-link"
                            onClick={handleClickLink}
                            href="tel:0937972525"
                            itemProp="telephone"
                        >
                            093 797 25 25
                        </a>
                    </li>
                    <li className="mobile-sidebar__contacts-item">
                        <a
                            className="mobile-sidebar__contacts-link"
                            onClick={handleClickLink}
                            href="tel:0687972525"
                            itemProp="telephone"
                        >
                            068 797 25 25
                        </a>
                    </li>
                    <li className="mobile-sidebar__contacts-item">
                        <a
                            className="mobile-sidebar__contacts-link"
                            onClick={handleClickLink}
                            href="tel:0957972525"
                            itemProp="telephone"
                        >
                            095 797 25 25
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        mainMenu: state.mainMenu,
    };
};
MobileSidebar.propTypes = {
    getMenu: PropTypes.func,
    clickedMobileItem: PropTypes.func,
    isOpen: PropTypes.bool,
    mainMenu: PropTypes.shape({
        menu: PropTypes.oneOfType([
            PropTypes.object,
            PropTypes.arrayOf(PropTypes.object),
        ]),
    }),
};
MobileSidebar.defaultProps = {
    getMenu: () => {},
    clickedMobileItem: () => {},
    isOpen: false,
    mainMenu: {},
};

export default connect(mapStateToProps, { getMenu })(MobileSidebar);
