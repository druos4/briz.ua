/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Inertia } from "@inertiajs/inertia";
import { getSlugFromUrl } from "../../utils";

const MobileDropdownChannels = ({
    defaultText,
    optionsList,
    getSelectedChannel,
}) => {
    // const lang = document.querySelector('html').lang;
    // const currentLang = lang !== 'ua' ? `/${lang}/` : "/";

    const [defaultSelectTxt, setDefaultSelectTxt] = useState({});
    const [isOpen, setFlagOpen] = useState(false);

    useEffect(() => {
        setDefaultSelectTxt({
            id: defaultText.id,
            genre_title: defaultText.genre_title,
        });
    }, []);

    const handleClickOutside = (e) => {
        if (
            !e.target.classList.contains("custom-select-option") &&
            !e.target.classList.contains("selected-text") &&
            !e.target.classList.contains("select-block") &&
            !e.target.classList.contains("select-icon") &&
            e.target.tagName !== "WTF"
        ) {
            setFlagOpen(false);
        }
    };

    useEffect(() => {
        setDefaultSelectTxt({
            id: defaultText.id,
            genre_title: defaultText.genre_title,
        });
        document.addEventListener("mousedown", handleClickOutside);
        // setDefaultSelectTxt(defaultText);
        return () =>
            document.removeEventListener("mousedown", handleClickOutside);
    }, [defaultText]);

    const handleListDisplay = () => {
        setFlagOpen(!isOpen);
    };

    const handleOptionClick = (selected) => {
        const { pathname, origin } = window.location;

        // window.location.href = '/lang/' + selected.value;
        document.documentElement.style.scrollBehavior = "smooth";
        setDefaultSelectTxt(selected);
        getSelectedChannel(selected);
        setFlagOpen(false);
        Inertia.replace(`${origin}${pathname}?${getSlugFromUrl(selected.url)}`);
    };

    return (
        <div className="custom-select-container">
            <div
                className={isOpen ? "select-block active" : "select-block"}
                onClick={handleListDisplay}
            >
                <div
                    className={
                        isOpen ? "selected-text active" : "selected-text"
                    }
                >
                    {defaultSelectTxt.genre_title}
                </div>
                <span
                    className={isOpen ? "select-icon active" : "select-icon"}
                />
            </div>

            {isOpen && (
                <ul className="select-options">
                    {optionsList.map((option, index) => {
                        return (
                            <li
                                // data-name={option.value}
                                className="custom-select-option-li"
                                key={index}
                            >
                                <span
                                    className={
                                        defaultSelectTxt.id === option.id
                                            ? "custom-select-option custom-select-option--active"
                                            : "custom-select-option"
                                    }
                                    // href={`/lang/${option.value}`}
                                    // href="#"
                                    // data-name={option.value}

                                    onClick={() => handleOptionClick(option)}
                                >
                                    {option.genre_title}
                                </span>
                            </li>
                        );
                    })}
                </ul>
            )}
        </div>
    );
};
MobileDropdownChannels.propTypes = {
    defaultText: PropTypes.shape({
        id: PropTypes.number,
        slug: PropTypes.string,
        title: PropTypes.string,
        genre_title: PropTypes.string,
    }),
    optionsList: PropTypes.arrayOf(PropTypes.object),
    getSelectedChannel: PropTypes.func,
};
MobileDropdownChannels.defaultProps = {
    defaultText: {},
    optionsList: [],
    getSelectedChannel: () => {},
};

export default MobileDropdownChannels;
