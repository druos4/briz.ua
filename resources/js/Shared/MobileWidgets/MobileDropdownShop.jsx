/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

const MobileDropdownShop = ({ defaultText, optionsList, isHasPicture }) => {
    const [defaultSelectTxt, setDefaultSelectTxt] = useState({});
    const [isOpen, setFlagOpen] = useState(false);

    useEffect(() => {
        setDefaultSelectTxt({ id: defaultText.slug, title: defaultText.title, picture: defaultText.picture });
    }, []);

    const handleClickOutside = (e) => {
        if (
            !e.target.classList.contains("custom-select-option") &&
            !e.target.classList.contains("selected-text") &&
            !e.target.classList.contains("select-block") &&
            !e.target.classList.contains("select-icon") &&
            e.target.tagName !== "WTF"
        ) {
            setFlagOpen(false);
        }
    };

    useEffect(() => {
        setDefaultSelectTxt({ id: defaultText.slug, title: defaultText.title, picture: defaultText.picture });
        document.addEventListener("mousedown", handleClickOutside);
        // setDefaultSelectTxt(defaultText);
        return () =>
            document.removeEventListener("mousedown", handleClickOutside);
    }, [defaultText]);

    const handleListDisplay = () => {
        setFlagOpen(!isOpen);
    };

    return (
        <div className="custom-select-container">
            <div
                className={isOpen ? "select-block active" : "select-block"}
                onClick={handleListDisplay}
            >
                {isHasPicture && <div className={
                    isOpen ? "selected-picture active" : "selected-picture"
                }>
                    {defaultSelectTxt.picture ? (
                        <img
                            className="tabset__tab-shop-title__picture"
                            src={defaultSelectTxt.picture}
                            alt={defaultSelectTxt.title}
                        />
                    ) : (
                        <div className="tabset__tab-shop-title__picture__empty" />
                    )}
                </div>}
                <div
                    className={
                        isOpen ? "selected-text active" : "selected-text"
                    }
                >
                    {defaultSelectTxt.title}
                </div>
                <span
                    className={isOpen ? "select-icon active" : "select-icon"}
                />
            </div>

            {isOpen && (
                <ul className="select-options select-options-shop">
                    {!!optionsList.length &&
                        optionsList.map((option) => {
                            return (
                                <li
                                    className={
                                        option.selected
                                            ? "custom-select-option-li custom-select-option-li--active"
                                            : "custom-select-option-li"
                                    }
                                    key={option.id}
                                >
                                    <a
                                        className={
                                            option.slug === "top"
                                                ? "custom-select-option custom-select-option--top"
                                                : "custom-select-option"
                                        }
                                        href={option.url}
                                    >
                                        {isHasPicture ? (
                                            <>
                                                {option.picture ? (
                                                    <img
                                                        className="tabset__tab-shop-title__picture"
                                                        src={option.picture}
                                                        alt={option.title}
                                                    />
                                                ) : (
                                                    <div className="tabset__tab-shop-title__picture__empty" />
                                                )}
                                                {option.title}
                                            </>
                                        ) : (
                                            option.title
                                        )}
                                        {option.slug === "top" && !isHasPicture ? (
                                            <span className="custom-select-option--top-miniature" />
                                        ) : null}
                                    </a>
                                </li>
                            );
                        })}
                </ul>
            )}
        </div>
    );
};

MobileDropdownShop.propTypes = {
    defaultText: PropTypes.shape({
        id: PropTypes.string,
        slug: PropTypes.string,
        title: PropTypes.string,
        picture: PropTypes.string
    }),
    optionsList: PropTypes.arrayOf(PropTypes.object),
    isHasPicture:PropTypes.bool
};
MobileDropdownShop.defaultProps = {
    defaultText: {},
    optionsList: [],
    isHasPicture:false
};

export default MobileDropdownShop;
