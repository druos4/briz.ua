/* eslint-disable no-shadow */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { setModalAccept } from "../../actions/actions";
import { PRODUCT_MOBILE_CALLME } from "../../constants/apiParams";

const SupportMobile = ({ setModalAccept, currentPage }) => {
    const { t } = useTranslation();
    const handleAppareModal = () => {
        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalFormCallMe",
            productTitle:
                currentPage === "Tariffs/InetBusiness"
                    ? "(048) 797 29 29"
                    : "(048) 797 25 25",
            productId: "",
            titleModal: t("supportHeader"),
            titleButton: t("callBack"),
            apiUrl: PRODUCT_MOBILE_CALLME.url,
            typeProduct: PRODUCT_MOBILE_CALLME.type,
            titleProduct: "",
            body: {},
            keyFormRedux: "callMe",
        });
    };

    return (
        <>
            <span
                className="header__mobile-support"
                onClick={() => handleAppareModal()}
            />
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        currentPage: state.currentPage.currentPage,
    };
};

SupportMobile.propTypes = {
    setModalAccept: PropTypes.func,
    currentPage: PropTypes.string,
};
SupportMobile.defaultProps = {
    setModalAccept: () => { },
    currentPage: "",
};

export default connect(mapStateToProps, { setModalAccept })(SupportMobile);
