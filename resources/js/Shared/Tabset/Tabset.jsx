import React from "react";
import PropTypes from "prop-types";
import TabData from "./TabData";
import TabNav from "./TabNav";

const Tabset = (props) => {
    const { children, currentTab, setTab, isHasPicture } = props;
    return (
        <div className="tabset">
            <TabNav
                clickHandler={setTab}
                selected={currentTab}
                navItems={children}
                isHasPicture={isHasPicture}
            />
            <TabData activeTab={currentTab}>{children}</TabData>
        </div>
    );
};

Tabset.propTypes = {
    children: PropTypes.arrayOf(PropTypes.object),
    currentTab: PropTypes.string,
    setTab: PropTypes.func,
    isHasPicture: PropTypes.bool
};
Tabset.defaultProps = {
    children: [],
    currentTab: "",
    setTab: () => {},
    isHasPicture:false
};

export default Tabset;
