/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from "react";
import PropTypes from "prop-types";
import { Inertia } from "@inertiajs/inertia";
import { connect } from "react-redux";
import { motion, AnimateSharedLayout } from "framer-motion";
import { getSlugFromUrl } from "../../utils";

const TabNav = ({ currentPage, selected, clickHandler, navItems, isHasPicture, isShowingHeader }) => {
    // const [selectedCard, setSelectedCard] = useState(0);

    const showMenuContent = (menuItem) => {
        // else if (props.currentPage === "ShopCategory" && props.slugForTab === props.selected) {
        //     return " tabset__selected tabset__tab-list-active";
        // }
        if (currentPage === "FaqItems" && menuItem === selected) {
            return " tabset__selected tabset__tab-list-active";
        }
        if (currentPage === "ShopCategory" && menuItem === selected) {
            return " tabset__shop-selected tabset__tab-list-shop-active";
        }
        if (selected === menuItem) {
            return " tabset__selected tabset__tab-list-active";
        }

        return "";
    };

    const openSubPage = (index, menuItem, propsItem) => {
        clickHandler(menuItem);

        // setSelectedCard(index);

        if (currentPage === "Channels") {
            const { pathname, origin } = window.location;

            Inertia.replace(
                `${origin}${pathname}?${getSlugFromUrl(propsItem.url)}`
            );
        }
    };

    const handleClickFormingURI = (e, id) => {
        const { pathname } = window.location;

        Inertia.replace(`${pathname}?${id}`);
    };

    // const handleScrollList = (e) => {
    //     console.log(e.target, "E T");
    // };

    const renderUnderlinePointPage = () => {
        switch (currentPage) {
            case "Tariffs/InetHome":
                return (
                    <motion.div
                        className="underline-tariffs"
                        layoutId="underline"
                        style={{ backgroundColor: "#00A3A4" }}
                    />
                );
            case "Tariffs/TvInet":
                return (
                    <motion.div
                        className="underline-tariffs"
                        layoutId="underline"
                        style={{ backgroundColor: "#00A3A4" }}
                    />
                );
            case "Channels":
                return (
                    <motion.div
                        className="sideline"
                        layoutId="sideline"
                        style={{
                            backgroundColor: "#00A3A4",
                        }}
                    />
                );
            case "FaqItems":
                return (
                    <motion.div
                        className="sideline"
                        layoutId="sideline"
                        style={{
                            backgroundColor: "#00A3A4",
                        }}
                    />
                );
            default:
                return "";
        }
    };

    return (
        <div className="tabset__tabs">
            {currentPage !== "ShopCategory" ? (
                <AnimateSharedLayout transition={{ duration: 0.5 }}>
                    <ol
                        className="tabset__tab-list"
                        // onMouseOver={e => document.body.style.overflow = 'hidden'}
                        // onMouseLeave={e => document.body.style.overflow = 'auto'}
                        // onScroll={(e) => handleScrollList(e)}
                    >
                        {navItems.map((item, index) => {
                            return (
                                <motion.li
                                    className={`tabset__tab-list-item ${showMenuContent(
                                        item.props.id
                                    )}`}
                                    onClick={() =>
                                        openSubPage(
                                            index,
                                            item.props.id,
                                            item.props
                                        )
                                    }
                                    key={item.props.id}
                                    animate
                                    // onScroll={e => handleScrollList(e)}
                                >
                                    {currentPage === "FaqItems" ? (
                                        <button
                                            type="button"
                                            className="tabset__tab-title tabset__tab-title--link"
                                            onClick={(e) =>
                                                handleClickFormingURI(
                                                    e,
                                                    item.props.id
                                                )
                                            }
                                        >
                                            {item.props.title}
                                        </button>
                                    ) : (
                                        <button
                                            className="tabset__tab-title"
                                            type="button"
                                        >
                                            {item.props.title}
                                        </button>
                                    )}
                                    {showMenuContent(item.props.id) &&
                                        renderUnderlinePointPage()}
                                </motion.li>
                            );
                        })}
                    </ol>
                </AnimateSharedLayout>
            ) : (
                <ol
                    className={`tabset__tab-list-shop ${isShowingHeader
                        ? "tabset__tab-list-shop__sticky tabset__tab-list-shop__sticky__with-header"
                        : "tabset__tab-list-shop__sticky"}`}
                    // onMouseOver={e => document.body.style.overflow = 'hidden'}
                    // onMouseLeave={e => document.body.style.overflow = 'auto'}
                >
                    {navItems.map((item) => {
                        return (
                            <li
                                className={`tabset__tab-list-shop-item ${showMenuContent(
                                    item.props.id
                                )}`}
                                onClick={() => openSubPage(item.props.id)}
                                key={item.props.id}
                                // onScroll={e => handleScrollList(e)}
                            >
                                <a
                                    className={
                                        item.props.id === "top"
                                            ? "tabset__tab-shop-title tabset__tab-shop-title--top"
                                            : "tabset__tab-shop-title"
                                    }
                                    href={item.props.url}
                                    onClick={() => openSubPage(item.props.id)}
                                    key={item.props.id}
                                >
                                    {isHasPicture ? (
                                        <>
                                            {item.props.picture ? (
                                                <img
                                                    className="tabset__tab-shop-title__picture"
                                                    src={item.props.picture}
                                                    alt={item.props.title}
                                                />
                                            ) : (
                                                <div className="tabset__tab-shop-title__picture" />
                                            )}
                                            <div className="tabset__tab-shop-title__title">{item.props.title}</div>
                                        </>
                                    ) : (
                                        item.props.title
                                    )}
                                </a>
                            </li>
                        );
                    })}
                </ol>
            )}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        currentPage: state.currentPage.currentPage,
        isShowingHeader: state.stickyHeader.stickyHeader
    };
};

TabNav.propTypes = {
    selected: PropTypes.string,
    clickHandler: PropTypes.func,
    currentPage: PropTypes.string,
    navItems: PropTypes.arrayOf(PropTypes.object),
    isHasPicture: PropTypes.bool,
    isShowingHeader: PropTypes.bool
};

TabNav.defaultProps = {
    selected: "",
    clickHandler: () => {},
    currentPage: "",
    navItems: [],
    isHasPicture: false,
    isShowingHeader:false
};

export default connect(mapStateToProps, null)(TabNav);
