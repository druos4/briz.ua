/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

const TabData = ({ children = [], activeTab, currentPage }) => {
    return (
        <div className="tabset__tab-content">
            {children.map((child) => {
                if (
                    currentPage === "FaqItems" &&
                    child.props.id === activeTab
                ) {
                    return child;
                }
                if (child.props.id === activeTab) {
                    return child;
                }
            })}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        currentPage: state.currentPage.currentPage,
    };
};

TabData.propTypes = {
    children: PropTypes.arrayOf(PropTypes.object),
    activeTab: PropTypes.string,
    currentPage: PropTypes.string,
};
TabData.defaultProps = {
    children: [],
    activeTab: "",
    currentPage: "",
};

export default connect(mapStateToProps, null)(TabData);
