import React from "react";
import { useHowConnectData } from "./useHowConnectData";
import { useHowConnectTitle } from "./useHowConnectTitle";
import StepsConnectInternetItem from "./StepsConnectInternetItem";

const StepsConnectInternet = ({ typeСonnection, title }) => {
    const howConnectCollection = useHowConnectData(typeСonnection);

    return (
        <>
            <div className="page-subtitle home__page-subtitle">
                {useHowConnectTitle(title)}
            </div>
            <ul className="tariff-page__how-connect how-connect">
                {howConnectCollection.map((item, index) => {
                    return <StepsConnectInternetItem item={item} key={index} />;
                })}
            </ul>
        </>
    );
};

export default StepsConnectInternet;
