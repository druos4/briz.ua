import { useTranslation } from "react-i18next";

export const useHowConnectData = (key) => {
    const { t } = useTranslation();

    // homeInet repeat tvInet

    const data = {
        business: [
            {
                step: `${t("step")}  1`,
                description: `<p class="how-connect__description">${t(
                    "clickUnderSelectedTariff"
                )} <span class="how-connect__description-medium500">${t(
                    "toPlug"
                )}</span> ${t("clickUnderSelectedTariff3")}</p>`,
                selector: "how-connect__miniature--tap",
            },
            {
                step: `${t("step")}  2`,
                description: `<p class="how-connect__description">${t(
                    "callYouBackNoLaterThan"
                )}</p>`,
                selector: "how-connect__miniature--speaker",
            },
            {
                step: `${t("step")}  3`,
                description: `<p class="how-connect__description">${t(
                    "prepareContractOneDay"
                )}</p>`,
                selector: "how-connect__miniature--contract",
            },
            {
                step: `${t("step")}  4`,
                description: `<p class="how-connect__description">${t(
                    "connectAfterConclusionContract"
                )}</p>`,
                selector: "how-connect__miniature--checked-finish",
            },
        ],
        homeInet: [
            {
                step: `${t("step")}  1`,
                description: `<p class="how-connect__description">${t(
                    "clickOn"
                )}<span class="how-connect__description-medium500">${t(
                    "toPlug"
                )}</span>${t("underSelectedTariffCallBack")}</p>`,
                selector: "how-connect__miniature--tap",
            },
            {
                step: `${t("step")}  2`,
                description: `<p class="how-connect__description">${t(
                    "supportOperatorCallYou"
                )}</p>`,
                selector: "how-connect__miniature--speaker",
            },
            {
                step: `${t("step")}  3`,
                description: `<p class="how-connect__description">${t(
                    "installerComeAppointedTime"
                )}</p>`,
                selector: "how-connect__miniature--fixer",
            },
            {
                step: `${t("step")}  4`,
                description: `<p class="how-connect__description"><span class="how-connect__description-first-str">${t(
                    "thatsAll"
                )}</span>${t("internetEvenFaster")}</p>`,
                selector: "how-connect__miniature--darts",
            },
        ],
        tvIptv: [
            {
                step: `${t("step")}  1`,
                description: `<p class="how-connect__description">${t(
                    "clickOn"
                )}<span class="how-connect__description-medium500">${t(
                    "toPlug"
                )}</span>${t("underSelectedTariffCallBack")}</p>`,
                selector: "how-connect__miniature--tap",
            },
            {
                step: `${t("step")}  2`,
                description: ` <p class="how-connect__description">${t(
                    "supportOperatorCallYou2"
                )}</p>`,
                selector: "how-connect__miniature--speaker",
            },
            {
                step: `${t("step")}  3`,
                description: `<p class="how-connect__description">${t(
                    "connectConfigureRemotelyNotMaster"
                )}</p>`,
                selector: "how-connect__miniature--no-fixer",
            },
            {
                step: `${t("step")}  4`,
                description: ` <p class="how-connect__description">${t(
                    "payTariffWatchIPTVonTV"
                )}</p>`,
                selector: "how-connect__miniature--watch-iptv",
            },
        ],
        tvKTV: [
            {
                step: `${t("step")}  1`,
                description: `<p class="how-connect__description">${t(
                    "clickUnderSelectedTariff"
                )}<span class="how-connect__description-connect">${t(
                    "clickUnderSelectedTariff2"
                )}</span>${t("clickUnderSelectedTariff3")}</p>`,
                selector: "how-connect__miniature--tap",
            },
            {
                step: `${t("step")}  2`,
                description: `<p class="how-connect__description">${t(
                    "supportOperatorCallYou"
                )}</p>`,
                selector: "how-connect__miniature--speaker",
            },
            {
                step: `${t("step")}  3`,
                description: `<p class="how-connect__description">${t(
                    "installerComeAppointedTime"
                )}</p>`,
                selector: "how-connect__miniature--fixer",
            },
            {
                step: `${t("step")}  4`,
                description: `<p class="how-connect__description">${t(
                    "payForTariffWatchFavoriteChannels"
                )}</p>`,
                selector: "how-connect__miniature--love-channels",
            },
        ],
        tvInet: [
            {
                step: `${t("step")}  1`,
                description: `<p class="how-connect__description">${t(
                    "clickOn"
                )}<span class="how-connect__description-medium500">${t(
                    "toPlug"
                )}</span>${t("underSelectedTariffCallBack")}</p>`,
                selector: "how-connect__miniature--tap",
            },
            {
                step: `${t("step")}  2`,
                description: `<p class="how-connect__description">${t(
                    "supportOperatorCallYou"
                )}</p>`,
                selector: "how-connect__miniature--speaker",
            },
            {
                step: `${t("step")}  3`,
                description: `<p class="how-connect__description">${t(
                    "installerComeAppointedTime"
                )}</p>`,
                selector: "how-connect__miniature--fixer",
            },
            {
                step: `${t("step")}  4`,
                description: `<p class="how-connect__description">${t(
                    "watchFavoriteChannelsFastInternet"
                )}</p>`,
                selector: "how-connect__miniature--rocket-into-tv",
            },
        ],
    };

    return data[key];
};
