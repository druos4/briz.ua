import React from "react";

const StepsConnectInternetItem = ({ item }) => {
    return (
        <li className="how-connect__item">
            <span className={`how-connect__miniature ${item.selector}`} />
            <p className="how-connect__title">{item.step}</p>
            <div dangerouslySetInnerHTML={{ __html: item.description }} />
        </li>
    );
};

export default StepsConnectInternetItem;
