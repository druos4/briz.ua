import { useTranslation } from "react-i18next";

export const useHowConnectTitle = (title) => {
    const { t } = useTranslation();
    return t(title);
};
