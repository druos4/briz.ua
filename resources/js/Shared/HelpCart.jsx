import React from "react";
import PropTypes from "prop-types";
import { InertiaLink } from "@inertiajs/inertia-react";

const HelpCart = ({ icon, title, anons, url, uniqId }) => {
    return (
        <>
            <a className="help-card" href={url}>
                <img className="help-card__img" src={icon} alt={uniqId} />
                <div className="help-card__content">
                    <div className="help-card__info">
                        <div className="help-card__title">{title}</div>
                        <span className="help-card__text">{anons}</span>
                    </div>
                    <div className="help-card__action">
                        <div className="help-arrow-widget help-category-links__arrow-direction help-arrow-button">
                            <span className="help-arrow-widget__icon" />
                        </div>
                    </div>
                </div>
            </a>
        </>
    );
};

HelpCart.propTypes = {
    icon: PropTypes.string,
    title: PropTypes.string,
    anons: PropTypes.string,
    url: PropTypes.string,
    uniqId: PropTypes.number,
};
HelpCart.defaultProps = {
    icon: "",
    title: "",
    anons: "",
    url: "",
    uniqId: null,
};

export default HelpCart;
