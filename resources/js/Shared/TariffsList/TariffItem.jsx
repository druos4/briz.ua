/* eslint-disable no-shadow */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/no-danger */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ReactCardFlip from "react-card-flip";
import { useTranslation } from "react-i18next";
import { setModalAccept } from "../../actions/actions";
import { PRODUCT_SERVICE } from "../../constants/apiParams";
import OfferTariffButton from "../ui/buttons/OfferTariffButton";
import { useIsSsr } from "../../hooks/useIsSsr";

const TariffItem = ({ tariff, tariffKey, index, setModalAccept, id, affiliation }) => {
    const isSsr = useIsSsr();
    const [isFlipped, setIsFlipped] = useState(false);
    const { t } = useTranslation();
    const lang = !isSsr && app.getLang();

    const handleClick = () => {
        // Переворот карточек при клике
        if (id === "daily") {
            setIsFlipped(false);
        } else {
            setIsFlipped(!isFlipped);
        }
    };

    const handleAppareModal = (id, title, seoId) => {
        // Открытие модального окна

        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle: title,
            productId: id,
            titleModal: t("connectionRequest"),
            titleButton: t("send"),
            apiUrl: PRODUCT_SERVICE.url,
            typeProduct: PRODUCT_SERVICE.type,
            titleProduct: t("rate"),
            seoId,
            body: { [PRODUCT_SERVICE.type]: id, ghost: "" },
            keyFormRedux: "default",
        });
    };

    return (
        <div className="tariffs-list__container-el">
            <div className="scale tariffs-list__scale">
                <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
                    <div
                        className="tariffs-list__cards-item"
                        onClick={handleClick}
                    >
                        <div className="tariffs-list__card tariffs-list__card--default">
                            <div
                                className={
                                    id === "daily"
                                        ? `tariffs-list__card-top-bg tariffs-list__card-top-bg--no-flip-icon tariffs-list__card-top-bg--${tariffKey}`
                                        : `tariffs-list__card-top-bg tariffs-list__card-top-bg--${tariffKey}`
                                }
                            >
                                <img
                                    src={tariff.picture}
                                    className="tariffs-list__card-img-bg"
                                    alt={`${affiliation} ${tariff.title} - інтернет-провайдер Briz в Одесі`}
                                    title={`${affiliation} ${tariff.title}`}
                                />
                                {tariff.price_old ? (
                                    <span className="offer-tariffs__card-top-discount">
                                        {t("discount")}
                                    </span>
                                ) : null}
                            </div>
                            <div className="offer-tariffs__card-content">
                                <div className="offer-tariffs__card-content-item">
                                    <div
                                        className="offer-tariffs__card-content-item-margin"
                                        onClick={(e) => e.stopPropagation()}
                                    >
                                        <span
                                            className={
                                                !tariff.title.includes(" ") &&
                                                tariff.title.length > 17
                                                    ? "offer-tariffs__card-content-speed offer-tariffs__card-content-speed--overstack-text"
                                                    : "offer-tariffs__card-content-speed"
                                            }
                                        >
                                            {tariff.title}
                                        </span>

                                        <span
                                            className={
                                                index === 1
                                                    ? "tariffs-list__progress-decoration range-wrapper"
                                                    : "tariffs-list__progress-decoration"
                                            }
                                            style={{
                                                background: tariff.background,
                                            }}
                                        />

                                        <p
                                            className="offer-tariffs__card-content-description"
                                            dangerouslySetInnerHTML={{
                                                __html: tariff.description,
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="offer-tariffs__card-content-item offer-tariffs__card-content-item--price">
                                    {
                                        tariff.price_old ?
                                            <span className="offer-tariffs__card-old-price" onClick={(e) => e.stopPropagation()}>
                                                {tariff.price_old}{" "}
                                                {id === 'daily' ?
                                                    <span className={lang === 'en' ? "offer-tariffs__currency-price-old" : ""}>{t('UAHdaily')}</span>
                                                    :
                                                    <span className={lang === 'en' ? "offer-tariffs__currency-price-old" : ""}>{t('UAHmonth')}</span>
                                                }

                                            </span>
                                            :
                                            null

                                    }
                                    <span
                                        className="offer-tariffs__card-content-price"
                                        onClick={(e) => e.stopPropagation()}>
                                        {tariff.price} {id === 'daily' ?
                                            <span className={lang === 'en' ? "offer-tariffs__currency" : ""}>{t('UAHdaily')}</span>
                                            :
                                            <span className={lang === 'en' ? "offer-tariffs__currency" : ""}>{t('UAHmonth')}</span>
                                        }
                                    </span>
                                    <span onClick={(e) => e.stopPropagation()}>
                                        <OfferTariffButton
                                            handler={() =>
                                                handleAppareModal(
                                                    tariff.id,
                                                    tariff.title,
                                                    tariff.seo_id
                                                )
                                            }
                                            titleButton={t("toPlugBtn")}
                                        />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        className="tariffs-list__cards-item"
                        onClick={handleClick}
                    >
                        <div className="tariffs-list__card tariffs-list__card--back">
                            {/* <div className="offer-tariffs__card-back"> */}
                            {!!tariff.can_prepay_discount &&
                            !!Object.keys(tariff.discounts).length ? (
                                <div className="offer-tariffs__back-block">
                                    <p className="offer-tariffs__back-title">
                                        {t("prepaidDiscounts")}
                                    </p>
                                    {Object.keys(tariff.discounts).map(
                                        (discountIndex) => {
                                            const discountItem =
                                                tariff.discounts[discountIndex];

                                            return (
                                                <div
                                                    className="offer-tariffs__back-list-discounts"
                                                    key={discountIndex}
                                                >
                                                    <span className="offer-tariffs__back-discounts-month">
                                                        {discountItem.month}{" "}
                                                        {t("monthShort")}
                                                    </span>
                                                    <span className="offer-tariffs__back-discounts-percent">
                                                        {discountItem.discount}
                                                    </span>
                                                    <span className="offer-tariffs__back-discounts-count">
                                                        <strong>
                                                            {
                                                                discountItem.amount
                                                            }
                                                        </strong>{" "}
                                                        <span
                                                            className={
                                                                lang === "en"
                                                                    ? "offer-tariffs__currency-back-card"
                                                                    : ""
                                                            }
                                                        >
                                                            {t("UAHmonth")}
                                                        </span>
                                                    </span>
                                                </div>
                                            );
                                        }
                                    )}
                                </div>
                            ) : null}
                            <div className="offer-tariffs__back-block">
                                {!!tariff.can_get_bonuses ||
                                !!tariff.can_seven_days ? (
                                    <p className="offer-tariffs__back-title">
                                        {t("afterConnecting")}
                                    </p>
                                ) : null}

                                <div className="offer-tariffs__back-bonuses">
                                    {tariff.can_get_bonuses ? (
                                        <p
                                            className="offer-tariffs__back-bonuses-txt"
                                            onClick={(e) => e.stopPropagation()}
                                        >
                                            <span className="offer-tariffs__back-bonuses-icon offer-tariffs__back-bonuses-icon--bonus" />{" "}
                                            {t("accrual")} {tariff.bonuses}{" "}
                                            {t("accrual25Bonuses")}
                                        </p>
                                    ) : null}

                                    {tariff.can_seven_days ? (
                                        <p
                                            className="offer-tariffs__back-bonuses-txt"
                                            onClick={(e) => e.stopPropagation()}
                                        >
                                            <span className="offer-tariffs__back-bonuses-icon offer-tariffs__back-bonuses-icon--7days" />{" "}
                                            {t("service7daysActive")}
                                        </p>
                                    ) : null}
                                </div>
                            </div>
                            <div className="offer-tariffs__back-btn-wrapper">
                                <span
                                    className="offer-tariffs__back-btn"
                                    onClick={handleClick}
                                >
                                    {t("backTo")}
                                </span>
                            </div>
                            {/* </div> */}
                            
                        </div>
                    </div>
                </ReactCardFlip>
            </div>
        </div>
    );
};

TariffItem.propTypes = {
    setModalAccept: PropTypes.func,
    id: PropTypes.string,
    index: PropTypes.number,
    tariffKey: PropTypes.string,
    tariff: PropTypes.shape({
        background: PropTypes.string,
        seo_id: PropTypes.string,
        description: PropTypes.string,
        title: PropTypes.string,
        picture: PropTypes.string,
        id: PropTypes.number,
        price_old: PropTypes.number,
        price: PropTypes.number,
        can_seven_days: PropTypes.number,
        discounts: PropTypes.arrayOf(
            PropTypes.shape({
                amount: PropTypes.number,
                month: PropTypes.number,
                discount: PropTypes.string,
            })
        ),
        can_get_bonuses: PropTypes.number,
        can_prepay_discount: PropTypes.number,
        bonuses: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
    }),
};
TariffItem.defaultProps = {
    setModalAccept: () => {},
    id: "",
    index: null,
    tariffKey: "",
    tariff: {},
};

export default connect(null, { setModalAccept })(TariffItem);
