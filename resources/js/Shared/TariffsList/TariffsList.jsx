import React from "react";
import PropTypes from "prop-types";
import TariffItem from "./TariffItem";

const TariffsList = ({ tariffs, id, affiliation }) => {
    return (
        <div className="tariffs-list">
            {
                Object.keys(tariffs).map((tariff, index) => {
                    const itemTariff = tariffs[tariff];

                    return (
                        <TariffItem
                            key={tariff}
                            tariff={itemTariff}
                            tariffKey={tariff}
                            index={index + 1}
                            id={id}
                            affiliation={affiliation}
                        />
                    )
                })
            }
        </div>
    )
};

TariffsList.propTypes = {
    tariffs: PropTypes.arrayOf(PropTypes.object),
    id: PropTypes.string,
};
TariffsList.defaultProps = {
    tariffs: [],
    id: "",
};

export default TariffsList;