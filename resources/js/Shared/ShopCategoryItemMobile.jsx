/* eslint-disable no-restricted-globals */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/no-array-index-key */
/* eslint-disable no-return-assign */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
// import { InertiaLink, Link } from "@inertiajs/inertia-react";
import MobileDropdownShop from "./MobileWidgets/MobileDropdownShop";
import { PRODUCT_PRODUCT } from "../constants/apiParams";
import ShopProductButton from "./ui/buttons/ShopProductButton";
import { useIsSsr } from "../hooks/useIsSsr";

const ShopCategoryItemMobile = ({
    categories,
    callbackActualTitle,
    setModalAccept,
    products,
    buttons,
    setShopProduct,
    isShowModalAccept,
    isHasMenuPictures
}) => {
    const isSsr = useIsSsr();
    const lang = !isSsr && document.querySelector("html").lang;
    const { t } = useTranslation();

    const [selectedCategory, setSelectedCategory] = useState(
        categories.find((item) => item.selected)
    );

    const callbackSelectedCategory = (selected) => {
        callbackActualTitle(selected.title);
        setSelectedCategory(selected);
    };

    const handleAppareModal = (e, productTitle, id, product) => {
        e.preventDefault();
        // Открытие модального окна с передачей в него данных
        // Открытие модального окна с передачей в него данных
        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle,
            productId: id,
            titleModal: t("purchaseRequest"),
            titleButton: t("sendRequest"),
            apiUrl: PRODUCT_PRODUCT.url,
            typeProduct: PRODUCT_PRODUCT.type,
            titleProduct: "",
            seoId: PRODUCT_PRODUCT.type,
            body: { [PRODUCT_PRODUCT.type]: id, ghost: "" },
            keyFormRedux: "default",
        });

        setShopProduct(product);
    };

    useEffect(() => {
        let cleanupFunction = false;
        callbackActualTitle(selectedCategory.title);

        return () => (cleanupFunction = true);
    }, []);

    useEffect(() => {
        if (!isShowModalAccept) {
            setShopProduct({});
        }
    }, [isShowModalAccept]);

    return (
        <>
            <div className="shop__nav-block-for-tablet">
                <span className="faq-item__dropdown channels__dropdown shop__dropdown">
                    <MobileDropdownShop
                        defaultText={selectedCategory}
                        optionsList={categories}
                        getSelectedChannel={callbackSelectedCategory}
                        isHasPicture={isHasMenuPictures}
                    />
                </span>
                <ul className="shop__list-filters">
                    {buttons.filter
                        .sort((a, b) => {
                            return a.title.length - b.title.length;
                        })
                        .map((filter, index) => {
                            return (
                                <li
                                    className={
                                        filter.active
                                            ? "shop__item-filter shop__item-filter--active"
                                            : "shop__item-filter"
                                    }
                                    key={index}
                                >
                                    <a
                                        className={
                                            filter.active
                                                ? "shop__item-filter-text shop__item-filter-text--active"
                                                : "shop__item-filter-text"
                                        }
                                        href={filter.url}
                                    >
                                        {filter.title}

                                    {filter.active ? (
                                        <span className="shop__item-filter-checked-icon" />
                                    ) : (
                                        <span className="shop__item-filter-checked-icon-hidden" />
                                    )}
                                </a>
                            </li>
                        );
                    })}
                </ul>
                <div className="shop__sort-block">
                    <a className="shop__sort" href={buttons.sort.url}>
                        <span className="shop__sort-button">
                            {buttons.sort.title}
                        </span>
                        <span
                            className={
                                !isSsr && location.search.includes("cheap")
                                    ? "shop__sort-icon"
                                    : "shop__sort-icon shop__sort-icon--expensive"
                            }
                        />
                    </a>
                </div>
            </div>
            <ul
                className={
                    products.length
                        ? "shop__list-catalog"
                        : "shop__list-catalog shop__list-catalog--no-products"
                }
            >
                {products.length ? (
                    products.map((product, index) => {
                        return product.can_buy ? (
                            <li
                                className="shop__item-catalog"
                                key={product.slug}
                            >
                                <a
                                    href={product.url}
                                    className="shop__item-link"
                                >
                                    <div
                                        className="shop__item-wrapper"
                                        itemScope
                                        itemType="http://schema.org/Product"
                                    >
                                        <img
                                            src={product.picture}
                                            alt={`${product.title} - інтернет-провайдер Briz в Одесі`}
                                            title={product.title}
                                            className="shop__item-catalog-pic"
                                            itemProp="image"
                                        />
                                        <div className="shop__item-catalog-info">
                                            <div className="shop__item-chips-wrapper">
                                                {product.top ? (
                                                    <div className="shop__item-catalog-top  shop__item-chips">
                                                    <span className="shop__item-catalog-label shop__item-catalog-label--top">
                                                        {t("top")}
                                                    </span>
                                                </div>
                                            ) : null}
                                            {product.has_discount ? (
                                                <div className="shop__item-catalog-discount shop__item-chips">
                                                    <span className="shop__item-catalog-label">
                                                        {t("discount")}
                                                    </span>
                                                </div>
                                            ) : null}
                                            {product.promo ? (
                                                <div className="shop__item-catalog-promo  shop__item-chips">
                                                    <span className="shop__item-catalog-label">
                                                        {t("labelPromo")}
                                                    </span>
                                                </div>
                                            ) : null}
                                            {product.new ? (
                                                <div className="shop__item-catalog-new  shop__item-chips">
                                                    <span className="shop__item-catalog-label">
                                                        {t("new")}
                                                    </span>
                                                    </div>
                                                ) : null}
                                            </div>
                                            <p
                                                className="shop__item-catalog-title"
                                                itemProp="name"
                                            >
                                                {product.title}
                                            </p>
                                            <span className="shop__item-catalog-can-buy shop__item-catalog-can-buy-icon">
                                                {t("freeDeliverySetupSecond")}
                                            </span>
                                            <div className="shop__item-price-block">
                                                <span
                                                    className="shop__item-true-price"
                                                    itemProp="offers"
                                                    itemScope
                                                    itemType="http://schema.org/Offer"
                                                >
                                                    <meta
                                                        itemProp="priceCurrency"
                                                        content="UAH"
                                                    />
                                                    <span itemProp="price">
                                                        {product.price}
                                                    </span>{" "}
                                                    <span
                                                        className={
                                                            lang === "en"
                                                                ? "shop__currency"
                                                                : ""
                                                        }
                                                    >
                                                        {t("UAH")}
                                                    </span>
                                                    <span
                                                        itemProp="availability"
                                                        href="http://schema.org/InStock"
                                                        hidden
                                                    >
                                                        InStock
                                                    </span>
                                                </span>
                                                {product.price_old ? (
                                                    <span className="shop__item-old-price">
                                                        {product.price_old}{" "}
                                                        <span
                                                            className={
                                                                lang === "en"
                                                                    ? "shop__currency-old-price"
                                                                    : ""
                                                            }
                                                        >
                                                            {t("UAH")}
                                                        </span>
                                                    </span>
                                                ) : null}
                                            </div>
                                            <ShopProductButton
                                                handler={(e) =>
                                                    handleAppareModal(
                                                        e,
                                                        product.title,
                                                        product.id,
                                                        product
                                                    )
                                                }
                                                titleButton={t("toOrder")}
                                            />
                                        </div>
                                    </div>
                                </a>
                            </li>
                        ) : null;
                    })
                ) : (
                    <li className="shop__item-no-products">
                        <span className="shop__img-no-products" />
                        <span className="shop__text-no-products">
                            {t("noEquipmentsFound")}
                        </span>
                    </li>
                )}
            </ul>
        </>
    );
};

ShopCategoryItemMobile.propTypes = {
    setModalAccept: PropTypes.func,
    setShopProduct: PropTypes.func,
    products: PropTypes.arrayOf(PropTypes.object),
    buttons: PropTypes.shape({
        filter: PropTypes.arrayOf(PropTypes.object),
        sort: PropTypes.shape({
            title: PropTypes.string,
            url: PropTypes.string,
        }),
    }),
    title: PropTypes.string,
    callbackActualTitle: PropTypes.func,
    categories: PropTypes.arrayOf(PropTypes.object),
    isShowModalAccept: PropTypes.bool,
    isHasMenuPictures: PropTypes.bool,
};
ShopCategoryItemMobile.defaultProps = {
    setModalAccept: () => {},
    setShopProduct: () => {},
    products: [],
    buttons: {},
    title: "",
    callbackActualTitle: () => {},
    categories: [],
    isHasMenuPictures: false
};

export default ShopCategoryItemMobile;
