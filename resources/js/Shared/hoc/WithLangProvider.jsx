import React from "react";
import { I18nextProvider, initReactI18next } from "react-i18next";
import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { resources } from "../../i18n/i18n";

const WithLangProvider = (BaseComponent) => (props) => {
    const locale = props.locale || "ua";

    i18n.use(LanguageDetector)
        .use(initReactI18next)
        .init({
            resources,
            lng: locale,
            defaultNS: "translations",
            interpolation: {
                escapeValue: false
            }
        });

    return (
        <I18nextProvider i18n={i18n}>
            <BaseComponent {...props} />
        </I18nextProvider>
    );
};
export default WithLangProvider;
