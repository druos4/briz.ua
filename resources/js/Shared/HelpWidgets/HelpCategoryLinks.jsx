import React from "react";
import PropTypes from "prop-types";
import { InertiaLink } from "@inertiajs/inertia-react";

const HelpCategoryLinks = ({ theme }) => {
    return (
        <div className="help-category-links">
            <ul className="help-category-links__list">
                {!!theme.children.length &&
                    theme.children.map((link) => {
                        return (
                            <li
                                className="help-category-links__item"
                                key={link.id}
                            >
                                <a
                                    href={link.url}
                                    className="help-category-links__link"
                                >
                                    <img
                                        src={link.icon}
                                        alt={link.id}
                                        className="help-category-links__img"
                                    />
                                    <div className="help-category-links__text-block">
                                        <p className="help-category-links__title">
                                            {link.title}
                                        </p>
                                        <p className="help-category-links__description">
                                            {link.anons}
                                        </p>
                                    </div>
                                    <div className="help-arrow-widget help-category-links__arrow-direction">
                                        <span className="help-arrow-widget__icon" />
                                    </div>
                                </a>
                            </li>
                        );
                    })}
            </ul>
        </div>
    );
};

HelpCategoryLinks.propTypes = {
    theme: PropTypes.shape({
        children: PropTypes.arrayOf(PropTypes.object),
    }),
};
HelpCategoryLinks.defaultProps = {
    theme: {},
};

export default HelpCategoryLinks;
