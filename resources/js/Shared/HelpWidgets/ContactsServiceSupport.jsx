import React from "react";
import { useTranslation } from "react-i18next";
import imgBottomHelper from "../../../images/help/contacts-service-support.svg";

const ContactsServiceSupport = () => {
    const { t } = useTranslation();

    return (
        <div className="contacts-service-support">
            <div className="contacts-service-support__opportunities">
                <div
                    className="contacts-service-support__phones-block"
                    itemScope
                    itemType="http://schema.org/Organization"
                >
                    <span itemProp="name" hidden>
                        Briz
                    </span>
                    <p className="contacts-service-support__opportunities-title">
                        {t("supportContacts")}
                    </p>
                    <ul className="contacts-service-support__phones-list">
                        <li className="contacts-service-support__phones-item">
                            <a
                                className="contacts-service-support__phones-link"
                                href="tel:0487972525"
                            >
                                <span className="contacts-service-support__phones-icon contacts-service-support__phones-icon--simple" />
                                <span
                                    className="contacts-service-support__phones-number"
                                    itemProp="telephone"
                                >
                                    048 797 25 25
                                </span>
                            </a>
                        </li>
                        <li className="contacts-service-support__phones-item">
                            <a
                                className="contacts-service-support__phones-link"
                                href="tel:0937972525"
                            >
                                <span className="contacts-service-support__phones-icon contacts-service-support__phones-icon--life" />
                                <span
                                    className="contacts-service-support__phones-number"
                                    itemProp="telephone"
                                >
                                    093 797 25 25
                                </span>
                            </a>
                        </li>
                        <li className="contacts-service-support__phones-item">
                            <a
                                className="contacts-service-support__phones-link"
                                href="tel:0957972525"
                            >
                                <span className="contacts-service-support__phones-icon contacts-service-support__phones-icon--vodafon" />
                                <span
                                    className="contacts-service-support__phones-number"
                                    itemProp="telephone"
                                >
                                    095 797 25 25
                                </span>
                            </a>
                        </li>
                        <li className="contacts-service-support__phones-item">
                            <a
                                className="contacts-service-support__phones-link"
                                href="tel:0487291122"
                            >
                                <span className="contacts-service-support__phones-icon contacts-service-support__phones-icon--simple" />
                                <span
                                    className="contacts-service-support__phones-number"
                                    itemProp="telephone"
                                >
                                    048 729 11 22
                                </span>
                            </a>
                        </li>
                        <li className="contacts-service-support__phones-item">
                            <a
                                className="contacts-service-support__phones-link"
                                href="tel:0687972525"
                            >
                                <span className="contacts-service-support__phones-icon contacts-service-support__phones-icon--kievstar" />
                                <span
                                    className="contacts-service-support__phones-number"
                                    itemProp="telephone"
                                >
                                    068 797 25 25
                                </span>
                            </a>
                        </li>
                        <li className="contacts-service-support__phones-item">
                            <a
                                className="contacts-service-support__phones-link"
                                href="mailto:supp@briz.ua"
                            >
                                <span className="contacts-service-support__phones-icon contacts-service-support__phones-icon--mail" />
                                <span
                                    className="contacts-service-support__phones-number"
                                    itemProp="email"
                                >
                                    supp@briz.ua
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="contacts-service-support__helper">
                    <p className="contacts-service-support__opportunities-title">
                        {t("helperBriz")}
                    </p>
                    <ul className="contacts-service-support__helper-list">
                        <li className="contacts-service-support__helper-item">
                            {t("beAbleViewInformation")}
                        </li>
                        <li className="contacts-service-support__helper-item">
                            {t("beAbleChangeMACadress")}
                        </li>
                    </ul>
                    <a
                        className="contacts-service-support__helper-link-telegram"
                        href="https://t.me/brizuabot"
                        target="_blank"
                    >
                        <span className="contacts-service-support__helper-icon-telegram" />
                        <span className="contacts-service-support__helper-text-telegram">
                            {t("joinUsTelegram")}
                        </span>
                    </a>
                </div>
            </div>
            <img
                alt="Контакти служби підтримки - інтернет-провайдер Briz в Одесі"
                title="Контакти служби підтримки"
                src={imgBottomHelper}
                className="contacts-service-support__img"
            />
        </div>
    );
};

export default ContactsServiceSupport;
