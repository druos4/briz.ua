import React from "react";
import PropTypes from "prop-types";
import { InertiaLink } from "@inertiajs/inertia-react";

const HelpCategoryPlitka = ({ theme }) => {
    return (
        <div className="help-category-links">
            <ul className="help-articles__palette-list">
                {!!theme.children.length &&
                    theme.children.map((link) => {
                        return (
                            <li
                                className="help-articles__palette-item"
                                key={link.id}
                            >
                                <a
                                    className="help-articles__palette-link"
                                    href={link.url}
                                >
                                    <img
                                        alt="icon"
                                        src={link.icon}
                                        className="help-articles__palette-pic"
                                    />
                                    <div className="help-articles__pallette-bottom">
                                        <span className="help-articles__pallette-title">
                                            {link.title}
                                        </span>
                                        <div className="help-arrow-widget help-category-links__arrow-direction">
                                            <span className="help-arrow-widget__icon" />
                                        </div>
                                    </div>
                                </a>
                            </li>
                        );
                    })}
            </ul>
        </div>
    );
};

HelpCategoryPlitka.propTypes = {
    theme: PropTypes.shape({
        children: PropTypes.arrayOf(PropTypes.object),
    }),
};
HelpCategoryPlitka.defaultProps = {
    theme: {},
};

export default HelpCategoryPlitka;
