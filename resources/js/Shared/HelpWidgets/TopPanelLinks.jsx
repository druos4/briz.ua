import React from "react";
import PropTypes from "prop-types";
import { InertiaLink } from "@inertiajs/inertia-react";

const TopPanelLinks = ({ menu }) => {
    return (
        <div className="top-panel-links">
            <ul className="top-panel-links__list">
                {!!menu.length &&
                    menu.map((link) => {
                        return (
                            <li className="top-panel-links__item" key={link.id}>
                                <a
                                    className="top-panel-links__link"
                                    href={link.url}
                                >
                                    <img
                                        src={link.icon}
                                        alt={link.id}
                                        className="top-panel-links__img"
                                    />
                                    <span className="top-panel-links__title">
                                        {link.title}
                                    </span>
                                </a>
                            </li>
                        );
                    })}
            </ul>
        </div>
    );
};

TopPanelLinks.propTypes = {
    menu: PropTypes.arrayOf(PropTypes.object),
};
TopPanelLinks.defaultProps = {
    menu: [],
};

export default TopPanelLinks;
