/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

const DropdownSelect = ({
    defaultText,
    optionsList,
    getSelectedLanguage,
    customClass = "",
    isRefreshPage
}) => {
    const [defaultSelectTxt, setDefaultSelectTxt] = useState({
        value: (typeof window !== 'undefined') &&  app.getLang(),
        label: (typeof window !== 'undefined') &&  app.getLang(),
    });

    // useEffect(() => {
    //     setDefaultSelectTxt({
    //         value: localStorage.hasOwnProperty('i18nextLng') ? localStorage.getItem('i18nextLng') : app.getLang(),
    //         label: localStorage.hasOwnProperty('i18nextLng') ? localStorage.getItem('i18nextLng') : app.getLang()
    //     })
    // }, [localStorage.getItem('i18nextLng')]);

    const [isOpen, setFlagOpen] = useState(false);

    const handleClickOutside = (e) => {
        if (
            !e.target.classList.contains("custom-select-option") &&
            !e.target.classList.contains("selected-text") &&
            !e.target.classList.contains("select-block") &&
            !e.target.classList.contains("select-icon") &&
            e.target.tagName !== "WTF"
        ) {
            setFlagOpen(false);
        }
    };

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () =>
            document.removeEventListener("mousedown", handleClickOutside);
    }, [defaultText]);

    const handleOptionClick = (selected) => {
        if (isRefreshPage) {
            window.location.href = `/lang/${selected.value}`;
        }
        localStorage.setItem("i18nextLng", selected.value)
        setDefaultSelectTxt(selected);
        getSelectedLanguage(selected);
        setFlagOpen(false);
    };

    return (
        <div
            className={`custom-select-container custom-select-container-language ${customClass}`}
        >
            <div className="select-block">
                <div
                    className={
                        isOpen ? "selected-text active" : "selected-text"
                    }
                    style={{ textTransform: "uppercase", userSelect: "none" }}
                >
                    {defaultSelectTxt.label}
                </div>
                <span
                    className={isOpen ? "select-icon active" : "select-icon"}
                />
            </div>

            <ul className="select-options">
                {optionsList.map((option) => {
                    return defaultSelectTxt.value !== option.value ? (
                        <li
                            className="custom-select-option-li"
                            key={option.value}
                        >
                            <a
                                className={`custom-select-option custom-select-option--${option.value}`}
                                onClick={() => handleOptionClick(option)}
                            >
                                {option.label}
                            </a>
                        </li>
                    ) : null;
                })}
            </ul>
        </div>
    );
};

DropdownSelect.propTypes = {
    optionsList: PropTypes.arrayOf(PropTypes.object),
    defaultText: PropTypes.shape({
        value: PropTypes.string,
        label: PropTypes.string,
    }),
    getSelectedLanguage: PropTypes.func,
    customClass: PropTypes.string,
    isRefreshPage: PropTypes.bool
};
DropdownSelect.defaultProps = {
    optionsList: [],
    defaultText: {},
    getSelectedLanguage: () => {},
    customClass: "",
    isRefreshPage:true
};

export default DropdownSelect;
