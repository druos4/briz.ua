/* eslint-disable no-restricted-globals */
/* eslint-disable react/no-array-index-key */
import React, { useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import Tabset from "../Tabset/Tabset";
import ChannelsCategory from "./ChannelsCategory";
import { getSlugFromUrl } from "../../utils";

const findPointId = (arr) => {
    const pointItem = arr.find(
        (item) => getSlugFromUrl(item.url) === location.search.substr(1)
    );

    if (pointItem) {
        return pointItem.id;
    }
    return 0;
};

const ChannelsDesktop = ({ channels, tariff, count }) => {
    const { t } = useTranslation();
    // const [currentTab, setTab] = useState(0);
    const [currentTab, setTab] = useState(findPointId(channels));

    return (
        <>
            <span className="channels__fixed-top">
                <h1 className="page-title tariff-page__page-title">
                    {t("channels")} {tariff.title}
                    <span className="channels__count">({count})</span>
                </h1>
            </span>
            <div className="channels__channels-category channels-category">
                <Tabset currentTab={currentTab} setTab={setTab}>
                    {channels.map((channel, index) => {
                        return (
                            <ChannelsCategory
                                key={index}
                                title={channel.genre_title}
                                channelsArr={channel.channels}
                                id={index}
                                url={channel.url}
                            />
                        );
                    })}
                </Tabset>
            </div>
        </>
    );
};

ChannelsDesktop.propTypes = {
    count: PropTypes.number,
    channels: PropTypes.arrayOf(PropTypes.object),
    tariff: PropTypes.shape({
        title: PropTypes.string,
    }),
};
ChannelsDesktop.defaultProps = {
    count: null,
    channels: [],
    tariff: {},
};

export default ChannelsDesktop;
