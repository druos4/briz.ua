/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-restricted-globals */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import MobileDropdownChannels from "../MobileWidgets/MobileDropdownChannels";
import { getSlugFromUrl } from "../../utils";
import { useIsSsr } from '../../hooks/useIsSsr';

const ChannelsMobile = ({ channels, tariff, count }) => {
    const isSsr = useIsSsr();
    const { t } = useTranslation();

    const [selectedCategory, setSelectedCategory] = useState(!isSsr && channels.find(item => getSlugFromUrl(item.url) === location.search.substr(1)) || channels[0]);
    const [isScrolling, setScrolling] = useState(false);

    const getSelectedChannel = (selected) => {
        setSelectedCategory(selected);
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScrollEvent);

        return () => {
            window.removeEventListener('scroll', handleScrollEvent);
        };
    }, []);

    const handleScrollEvent = () => {
        setScrolling(true);
    };

    return (
        <>
            <span className="channels__fixed-top">
                <h1 className="page-title tariff-page__page-title">
                    {t("channels")} {tariff.title}
                    <span className="channels__count">({count})</span>
                </h1>
                <span className="channels__dropdown">
                    <MobileDropdownChannels
                        defaultText={selectedCategory}
                        optionsList={channels}
                        getSelectedChannel={getSelectedChannel}
                    />
                </span>
            </span>
            <p className="channels__category-list-title">
                {selectedCategory.genre_title}{" "}
                <span className="channels__title-count-channels">
                    ({selectedCategory.channels && selectedCategory.channels.length})
                </span>
            </p>
            <ul className="channels__result-list">
                {selectedCategory && !!Object.keys(selectedCategory).length && selectedCategory.channels &&
                    selectedCategory.channels.map((category, index) => {
                        return (
                            <li key={index} className="channels__item">
                                <img
                                    src={category.icon}
                                    className="channels__icon-pic"
                                    alt={index}
                                />
                                <span className="channels__category-name">
                                    {category.title}
                                </span>
                                {category.is_analog ? (
                                    <div className="channels-category__analog-pic-container">
                                        <span
                                            className="channels-category__analog-pic"
                                            onClick={() => setScrolling(false)}
                                        />
                                        <span
                                            className={
                                                isScrolling
                                                    ? "channels-category__analog-pic-tooltip channels-category__analog-pic-tooltip--scrolling"
                                                    : "channels-category__analog-pic-tooltip"
                                            }
                                        >
                                            {t("textTooltipAnalogChannel")}
                                        </span>
                                    </div>
                                ) : null}
                            </li>
                        );
                    })}
            </ul>
        </>
    );
};

ChannelsMobile.propTypes = {
    count: PropTypes.number,
    channels: PropTypes.arrayOf(PropTypes.object),
    tariff: PropTypes.shape({
        title: PropTypes.string,
    }),
};
ChannelsMobile.defaultProps = {
    count: null,
    channels: [],
    tariff: {},
};

export default ChannelsMobile;
