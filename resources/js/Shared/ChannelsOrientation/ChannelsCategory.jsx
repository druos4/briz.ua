/* eslint-disable react/no-array-index-key */
import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import Tooltip from "@material-ui/core/Tooltip";
import Fade from "@material-ui/core/Fade";
import { withStyles } from "@material-ui/core/styles";

const CustomTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: "#424242",
        color: "#fff",
        fontSize: "14px",
        borderRadius: "3px",
        fontFamily: theme.typography.fontFamily,
        fontWeight: theme.typography.fontWeightRegular,
        top: "5px",
        transition: "all 0.1s ease-out",
    },
    arrow: {
        color: "#424242",
    },
}))(Tooltip);

const ChannelsCategory = ({ title, channelsArr }) => {
    const { t } = useTranslation();
    // const classes = useStyles();

    return (
        <>
            <p className="channels-category__title">
                {title}{" "}
                <span className="channels-category__title-count-channels">
                    ({channelsArr?.length})
                </span>
            </p>
            <ul
                className={
                    channelsArr?.length > 20
                        ? "channels-category__list"
                        : "channels-category__list channels-category__list--height-auto"
                }
            >
                {!!channelsArr?.length &&
                    channelsArr.map((channel, index) => {
                        return (
                            <li key={index} className="channels-category__item">
                                <img
                                    src={channel.icon}
                                    className="channels-category__icon"
                                    alt={index}
                                />
                                <span className="channels-category__name">
                                    {channel.title}
                                </span>
                                {channel.is_analog ? (
                                    <div className="channels-category__analog-pic-container">
                                        <CustomTooltip
                                            title={t(
                                                "textTooltipAnalogChannel"
                                            )}
                                            placement="top-end"
                                            arrow
                                            TransitionComponent={Fade}
                                            TransitionProps={{ timeout: 0 }}
                                        >
                                            <span className="channels-category__analog-pic" />
                                        </CustomTooltip>
                                    </div>
                                ) : null}
                            </li>
                        );
                    })}
            </ul>
        </>
    );
};
ChannelsCategory.propTypes = {
    title: PropTypes.string,
    channelsArr: PropTypes.arrayOf(PropTypes.object),
};
ChannelsCategory.defaultProps = {
    title: "",
    channelsArr: [],
};

export default ChannelsCategory;
