import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import CallMe from "./CallMe";

const Support = ({ currentPage }) => {
    const [flagFocus, setFlagFocus] = useState();
    const { t } = useTranslation();

    const getFocus = (flag) => {
        setFlagFocus(flag);
    };

    return (
        <div className="header__support support">
            <div
                className={
                    flagFocus
                        ? "support__content support__content--focus"
                        : "support__content"
                }
            >
                <div className="support__content-wrapper">
                    <div className="support__visible-content">
                        <div className="support__tel-container" itemScope itemType="http://schema.org/Organization">
                            <span itemProp="name" hidden>Briz</span>
                            <a
                                href={
                                    currentPage === "Tariffs/InetBusiness"
                                        ? "tel:0487972929"
                                        : "tel:0487972525"
                                }
                                className="support__tel"
                                itemProp="telephone"
                            >
                                {currentPage === "Tariffs/InetBusiness"
                                    ? "(048) 797 29 29"
                                    : "(048) 797 25 25"}
                            </a>
                            <span className="support__icon-arrow" />
                        </div>
                        <span className="support__notice">
                            {t("supportHeader")}
                        </span>
                    </div>
                </div>
                <div className="support__hidden-content">
                    <span className="support__or">{t("or")}</span>
                    <CallMe getFocus={getFocus} />
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        currentPage: state.currentPage.currentPage,
    };
};

Support.propTypes = {
    currentPage: PropTypes.string,
};
Support.defaultProps = {
    currentPage: "",
};

export default connect(mapStateToProps, null)(Support);
