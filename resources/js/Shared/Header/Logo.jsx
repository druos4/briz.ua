import React from "react";
import PropTypes from "prop-types";
import imgLogo from '../../../images/logo-header.svg';
import { InertiaLink } from "@inertiajs/inertia-react";
import { useIsSsr } from '@/hooks/useIsSsr';
// i18nextLng - LANGUAGE KEY LOCAL STORAGE

const Logo = ({ clickedMobileItem }) => {
    const isSsr = useIsSsr();
    let lang = !isSsr && app.getLang();

    if (lang === "ua") {
        lang = "";
    }

    const handleClickLogoLink = () => {
        clickedMobileItem(false);
    };

    return (
        <div className="header__logo logo">
            <a
                className="header__logo-link"
                href={`/${lang}`}
                onClick={() => handleClickLogoLink()}
            >
                <img
                    alt="logo company Briz"
                    className="header__logo-pic"
                    src={imgLogo}
                />
            </a>
        </div>
    );
};
Logo.propTypes = {
    clickedMobileItem: PropTypes.func,
};
Logo.defaultProps = {
    clickedMobileItem: () => {},
};

export default Logo;
