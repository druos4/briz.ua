/* eslint-disable no-nested-ternary */
import React from "react";
import PropTypes from "prop-types";
import { InertiaLink } from "@inertiajs/inertia-react";
import { connect } from "react-redux";
import { menuIsLoading } from "@/actions/actions";
import { useIsSsr } from "@/hooks/useIsSsr";

const MainMenuItem = ({ menuItem, children, menuIsLoading }) => {
    const isSsr = useIsSsr();
    const lang = !isSsr && app.getLang();
    let actualLang = "";

    if (lang === "ua" || lang === "en") {
        actualLang = "ua";
    } else {
        actualLang = "ru";
    }

    return (
        <li
            className={
                children
                    ? "header__menu-item header__menu-item--modif"
                    : "header__menu-item"
            }
        >
            {children ? (
                <span className="header__menu-item-link">{menuItem.title}</span>
            ) : menuItem?.url?.includes("shop.briz.ua") ? (
                <a
                    className={
                        menuItem.url
                            ? "header__menu-item-link active"
                            : "header__menu-item-link"
                    }
                    href={menuItem.url}
                >
                    {menuItem.title}
                </a>
            ) : menuItem.url.includes("help.briz.ua") ? (
                <a
                    className={
                        menuItem.url
                            ? "header__menu-item-link active"
                            : "header__menu-item-link"
                    }
                    href={`${menuItem.url}${actualLang}/`}
                >
                    {menuItem.title}
                </a>
            ) : (
                <a
                    className={
                        menuItem.url
                            ? "header__menu-item-link active"
                            : "header__menu-item-link"
                    }
                    href={menuItem.url}
                    // onClick={e => {
                    //     const http = new XMLHttpRequest();
                    //     http.open('HEAD', menuItem.url, false);
                    //     http.send();
                    //     if (http.status != 404);

                    //     else window.location.href = menuItem.url;
                    // }}
                >
                    {menuItem.title}
                </a>
            )}

            {children ? (
                <>
                    <span className="header__arrow-closed" />
                    <ul className="header__menu-submenu">
                        {children.map((submenuItem, index) => {
                            return (
                                <li
                                    className="header__menu-submenu-item"
                                    // eslint-disable-next-line react/no-array-index-key
                                    key={index}
                                >
                                    <a
                                        className={
                                            submenuItem.url
                                                ? "header__submenu-link"
                                                : ""
                                        }
                                        href={submenuItem.url}
                                    >
                                        {submenuItem.title}
                                    </a>
                                </li>
                            );
                        })}
                    </ul>
                </>
            ) : null}
        </li>
    );
};

MainMenuItem.propTypes = {
    menuItem: PropTypes.shape({
        title: PropTypes.string,
        url: PropTypes.string,
    }),
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.object),
        PropTypes.bool,
    ]),
};
MainMenuItem.defaultProps = {
    menuItem: {},
    children: [],
};

export default connect(null, { menuIsLoading })(MainMenuItem);
