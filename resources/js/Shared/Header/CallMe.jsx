import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import ModalForm from "../Forms/ModalForm";
import { PRODUCT_MOBILE_CALLME } from "../../constants/apiParams";

const CallMe = ({ getFocus }) => {
    const { t } = useTranslation();

    return (
        <div className="support__callme">
            <ModalForm
                titleModal={t("callMeBack")}
                type="callMeHeader"
                titleButton={t("callBack")}
                apiUrl={PRODUCT_MOBILE_CALLME.url}
                typeProduct={PRODUCT_MOBILE_CALLME.type}
                body={{ ghost: "" }}
                keyFormRedux="default"
                closeFormOnHeader={getFocus}
            />
        </div>
    );
};

CallMe.propTypes = {
    getFocus: PropTypes.func,
};
CallMe.defaultProps = {
    getFocus: () => {},
};

export default CallMe;
