/* eslint-disable no-shadow */
/* eslint-disable no-prototype-builtins */
/* eslint-disable react/no-children-prop */
/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/no-array-index-key */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import MainMenuItem from "./MainMenuItem";
import { getMenu } from "@/actions/actions";

const MainMenu = ({ mainMenu: { menu }, getMenu }) => {
    const lang =  (typeof window !== 'undefined') &&  app.getLang();

    useEffect(() => {
        const csrf = document.querySelector("[name=csrf-token]");
        getMenu(csrf, lang);
    }, [lang]);

    return (
        <nav className="header__navbar">
            <ul className="header__menu">
                {menu &&
                    !!menu.length &&
                    Object.keys(menu).map((menuItem, index) => {
                        const item = menu[menuItem];

                        return (
                            <MainMenuItem
                                key={index}
                                menuItem={item}
                                children={
                                    item.hasOwnProperty("chilren") &&
                                    !!item.chilren.length &&
                                    item.chilren
                                }
                            />
                        );
                    })}
            </ul>
        </nav>
    );
};

const mapStateToProps = (state) => {
    return {
        mainMenu: state.mainMenu,
    };
};
MainMenu.propTypes = {
    mainMenu: PropTypes.object,
    menu: PropTypes.arrayOf(PropTypes.object),
    getMenu: PropTypes.func,
};
MainMenu.defaultProps = {
    mainMenu: {},
    menu: [],
    getMenu: () => {},
};
export default connect(mapStateToProps, { getMenu })(MainMenu);
