/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable consistent-return */
import React, { useState, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import { keepTrackStickyHeader } from "@/actions/actions";
import MainMenu from "./MainMenu";
import MobileSidebar from "../MobileWidgets/MobileSidebar";
import Logo from "./Logo";
import Support from "./Support";
import SupportMobile from "../MobileWidgets/SupportMobile";
import DropdownSelect from "../DropdownSelect";
import { getLocale, isIos } from "@/utils";
import {Inertia} from "@inertiajs/inertia";

const defaultSelectLang = { value: "ua", label: "Ua" };

const Header = ({ props, keepTrackStickyHeader}) => {
    const ios = isIos();
    const lang = (typeof window !== 'undefined') && app.getLang();

    const headerRef = useRef(null);
    let lastScroll = 0;
    const language = [
        { value: "ru", label: "Ru" },
        { value: "ua", label: "Ua" },
        { value: "en", label: "En" },
    ];
    const [sticky, setSticky] = useState(true);
    const [selectedLanguage, setSelectedLanguage] = useState({});
    const { t, i18n } = useTranslation();
    const [openedSidebar, setOpenSidebar] = useState(false);

    const date = new Date();
    const currentDate = date.getDate();
    const currentLocale = getLocale(props.locale)

    const currentLangSevenDays = currentLocale === "ua" ? "" : `index-${currentLocale}.html`;

    const html = (typeof window !== 'undefined') && document.getElementsByTagName("html")[0];

    const clickedMobileItem = (flag) => {
        setOpenSidebar(flag);
    };

    const handleScroll = () => {
        if (
            window.screen.availWidth < 1025 &&
            window.pageYOffset > lastScroll
        ) {
            setSticky(true);
        }

        if (window.pageYOffset <= 0) {
            setSticky(null);
        }

        if (
            window.screen.availWidth > 1024 &&
            window.pageYOffset > lastScroll
        ) {
            setSticky(false);
        } else if (window.pageYOffset < lastScroll) {
            setSticky(true);
        }

        lastScroll = window.pageYOffset;
    };

    useEffect(() => {
        const validatedLang = validateStorageLang(localStorage.getItem("i18nextLng"));
        localStorage.setItem("i18nextLng", validatedLang);

        const header = headerRef.current.getBoundingClientRect();

        if (window.location.pathname === "/") {
            setSelectedLanguage({ value: lang, label: lang });
            i18n.changeLanguage("ua");
        }

        if (lang === "ua") {
            setSelectedLanguage({ value: lang, label: "Ua" });
            i18n.changeLanguage(lang);
        }

        if (lang === "ru") {
            setSelectedLanguage({ value: lang, label: "Ru" });
            i18n.changeLanguage(lang);
        }

        if (lang === "en") {
            setSelectedLanguage({ value: lang, label: "En" });
            i18n.changeLanguage(lang);
        }

        if (!ios) {
            const handleScrollEvent = () => {
                handleScroll(header.top, header.height);
            };

            window.addEventListener("scroll", handleScrollEvent);

            return () => {
                window.removeEventListener("scroll", handleScrollEvent);
            };
        }
    }, []);

    useEffect(() => {
        const handleResize = () => {
            if (html.clientWidth >= 1280) {
                setOpenSidebar(false);
            }
        };
        window.addEventListener("resize", handleResize);
        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, []);

    useEffect(() => {
        if (html.nodeName === 'HTML') {
            html.style.overflow = `${openedSidebar ? "hidden" : "initial"}`;
            html.style.position = `${openedSidebar ? "hidden" : "initial"}`;
            html.style["touch-action"] = `${openedSidebar ? "none" : "initial"}`;
        }
    }, [openedSidebar]);

    useEffect(() => {
        keepTrackStickyHeader(sticky);
    }, [sticky]);

    const getSelectedLanguage = (selected) => {
        setSelectedLanguage(selected);
    };

    const validateStorageLang = (lang) => {
        const availableLangs = language.map(el => el.value);
        const filteredLang = lang.replace(/[^a-zA-Z]/g, "").toLowerCase();
        return availableLangs.includes(filteredLang) ? filteredLang : "ua";
    };

    return (
        <>
            <header
                style={
                    ios
                        ? {
                              position: "fixed",
                              transform: "translate3d(0, 0, 0)",
                          }
                        : { position: "sticky" }
                }
                className={
                    sticky
                        ? "header header__scroll-up"
                        : "header header__scroll-down"
                }
                ref={headerRef}
            >
                <div className="header__wrapper">
                    <Logo clickedMobileItem={clickedMobileItem} />
                    <div className="header__mobile-items">
                        {currentDate < 8 ? (
                            <a
                                href={`https://stat.briz.ua/looppage/${currentLangSevenDays}`}
                                rel="nofollow"
                                className="header__seven-days-link"
                            />
                        ) : null}

                        <SupportMobile />
                        <span className="header__mobile-menu mobile-sidebar">
                            {openedSidebar ? (
                                <div
                                    className={
                                        ios
                                            ? "mobile-sidebar__overlay mobile-sidebar__overlay--ios"
                                            : "mobile-sidebar__overlay"
                                    }
                                    onClick={() => setOpenSidebar(false)}
                                />
                            ) : null}
                            <span
                                className={
                                    openedSidebar
                                        ? "mobile-sidebar__burger mobile-sidebar__burger--opened"
                                        : "mobile-sidebar__burger mobile-sidebar__burger--closed"
                                }
                                onClick={() => setOpenSidebar(!openedSidebar)}
                            />

                            <MobileSidebar
                                isOpen={openedSidebar}
                                clickedMobileItem={clickedMobileItem}
                            />
                        </span>
                    </div>
                    <MainMenu />
                    <Support />
                    <span className="header__remove-on-mobile">
                        <DropdownSelect
                            defaultText={defaultSelectLang}
                            optionsList={language}
                            getSelectedLanguage={getSelectedLanguage}
                        />
                    </span>
                    <a
                        href="http://tv.briz.ua/"
                        target="_blank"
                        className="header__tv-icon tv-icon"
                    />
                    <a
                        href={`https://stat.briz.ua/?lang=${currentLocale}`}
                        target="_blank"
                        className="header__account-link"
                        rel="nofollow"
                    >
                        <span className="header__user-icon" />
                        <span className="header__account-texyt">
                            {t("personalArea")}
                        </span>
                    </a>
                </div>
            </header>
        </>
    );
};

export default connect(null, { keepTrackStickyHeader })(Header);
