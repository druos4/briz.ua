/* eslint-disable jsx-a11y/anchor-has-content */
import React from "react";
import { InertiaLink } from "@inertiajs/inertia-react";
import { useTranslation } from "react-i18next";
import logoFooter from "../../images/footer/LogoFooter.svg";
import { getLocale, getLocaleHref, linksForApps } from "../utils";
import { isIos, isSafari } from '../utils';
import { useIsSsr } from '../hooks/useIsSsr';

const Footer = ({props}) => {

    const isSsr = useIsSsr();
    const { t } = useTranslation();

    const currentLocale = getLocale(props.locale)
    const currentLocaleHref = getLocaleHref(props.locale)

    let actualLang = "";

    if (currentLocale === "ua" || currentLocale === "en") {
        actualLang = "ua";
    } else {
        actualLang = "ru";
    }

    const ios = isSsr && isIos();
    const safari = isSsr && isSafari();

    return (
        <footer className="footer">
            <div className="footer__container">
                <div
                    className={ios || safari ? "footer__horizontal-section footer__horizontal-section--ios" : "footer__horizontal-section"}>
                    <span className={ios || safari ? "footer__logo footer__logo--ios" : "footer__logo"}>
                        <a href="/">
                            <img src={logoFooter} alt="logo company Briz"/>
                        </a>
                    </span>
                    <div
                        className={ios || safari ? "footer__section-cell footer__section-cell--ios" : "footer__section-cell"}>
                        <ul className="footer__section-list">
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/domashniy-internet`}
                                >
                                    {t('internet')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/televidenie`}
                                >
                                    {t('television')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/televidenie-i-internet`}
                                >
                                    {t('inetPlusTB')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/internet-dlya-biznesa`}
                                >
                                    {t('forBusiness')}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="footer__section-cell">
                        <ul className="footer__section-list">
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-uppercase"
                                    href={`${currentLocaleHref}/map`}
                                >
                                    {t('coverage')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-uppercase"
                                    href={`${currentLocaleHref}/equipment`}
                                >
                                    {t('shop')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-uppercase"
                                    href={`${currentLocaleHref}/payment`}
                                >
                                    {t('payment')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-uppercase"
                                    target="_blank"
                                    href={`https://stat.briz.ua/?lang=${currentLocale}`}
                                    rel="nofollow noreferrer"
                                >
                                    {t('personalArea')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-uppercase"
                                    href={`${currentLocaleHref}/contacts`}
                                >
                                    {t('contacts')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-uppercase"
                                    href={`${currentLocaleHref}/sitemap`}
                                >
                                    {t('sitemap')}
                                </a>
                            </li>

                        </ul>
                    </div>
                    <div className="footer__section-cell">
                        <ul className="footer__section-list">
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/actions`}
                                >
                                        {t('promotions')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/news`}
                                >
                                    {t('news')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/documents`}
                                >
                                    {t('documents')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/usloviya-i-pravila`}
                                >
                                    {t('usloviya')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/help`}
                                >
                                    {t('help')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/blog`}
                                >
                                    {t('blog')}
                                </a>
                            </li>
                            <li className="footer__section-list-item">
                                <a
                                    className="footer__link-lowercase"
                                    href={`${currentLocaleHref}/faq`}
                                >
                                    {t('FAQ')}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="footer__section-cell footer__section-cell--last">
                        <div
                            className={ios || safari ? "footer__last-cell footer__last-cell--ios" : "footer__last-cell"}>
                            <div
                                className={ios || safari ? "footer__section-cell-wrapper footer__section-cell-wrapper--ios" : "footer__section-cell-wrapper"}
                            >
                                <p className="footer__section-title">{t('personalAccountSmartphone')}</p>
                                <ul className="footer__section-list-last">
                                    <li className="footer__section-item-preview">
                                        <a
                                            aria-label="link app apple"
                                            href={linksForApps(currentLocale).apple}
                                            target="_blank"
                                            // rel="nofollow"
                                            className="footer__link-app footer__link-app--apple"
                                        />
                                    </li>
                                    <li className="footer__section-item-preview">
                                        <a
                                            aria-label="link app google"
                                            href={linksForApps(currentLocale).google}
                                            target="_blank"
                                            // rel="nofollow"
                                            className="footer__link-app footer__link-app--google-play"
                                        />
                                    </li>
                                </ul>
                            </div>
                            <div
                                className={ios || safari ? "footer__section-cell-wrapper footer__section-cell-wrapper--ios" : "footer__section-cell-wrapper"}
                            >
                                <p className="footer__section-title">{t('weInSocialNetworks')}</p>
                                <p className="footer__section-text">
                                    {t('prizesTipsLeisure')}
                                </p>
                                <ul className="footer__section-list-last">
                                    <li className="footer__section-item-preview">
                                        <a
                                            aria-label="link instagram"
                                            href="https://www.instagram.com/briz_official/"
                                            target="_blank"
                                            // rel="nofollow"
                                            className="footer__link-app footer__link-app--insta"
                                        />
                                    </li>
                                    <li className="footer__section-item-preview">
                                        <a
                                            aria-label="link facebook"
                                            href="https://www.facebook.com/brizodessa/"
                                            target="_blank"
                                            // rel="nofollow"
                                            className="footer__link-app footer__link-app--fb"
                                        />
                                    </li>
                                    <li className="footer__section-item-preview">
                                        <a
                                            aria-label="link invite viber"
                                            href="https://invite.viber.com/?g2=AQBP6T8C0GnSf0zICnMwJglZHVgGiKRa7zlioveG8yysTGbXk39nRbU14kQCy1Wm"
                                            target="_blank"
                                            // rel="nofollow"
                                            className="footer__link-app footer__link-app--viber"
                                        />
                                    </li>
                                    <li className="footer__section-item-preview">
                                        <a
                                            aria-label="link telegram brizinfo"
                                            href="https://t.me/brizinfo"
                                            target="_blank"
                                            // rel="nofollow"
                                            className="footer__link-app footer__link-app--telegram"
                                        />
                                    </li>
                                </ul>
                            </div>
                            <div className={
                                ios || safari ?
                                    "footer__section-cell-wrapper footer__section-cell-wrapper--speedtest footer__section-cell-wrapper--speedtest-ios"
                                    :
                                    "footer__section-cell-wrapper footer__section-cell-wrapper--speedtest"
                            }
                            >
                                <p className="footer__section-title">{t('speedCheck')}</p>

                                <a
                                    aria-label="link speedtest"
                                    href="https://ispbriz.speedtestcustom.com/"
                                    target="_blank"
                                    rel="nofollow"
                                    className="footer__speedtest-img"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <p className="footer__copyright">
                    Copyright © {new Date().getFullYear()}{" "}
                    <span className="footer__copyright-label">BRIZ</span>. All
                    rights reserved.
                </p>
            </div>
        </footer>
    )
};

export default Footer;
