import React from "react";
import PropTypes from "prop-types";

const FaqButton = ({ titleButton, handler }) => {
    return (
        <button
            type="button"
            className="faq__no-match-question-btn custom-button"
            onClick={() => handler()}
        >
            {titleButton}
        </button>
    );
};

FaqButton.propTypes = {
    titleButton: PropTypes.string,
    handler: PropTypes.func,
};

FaqButton.defaultProps = {
    titleButton: "",
    handler: () => {},
};

export default FaqButton;
