import React from "react";
import PropTypes from "prop-types";

const ShopProductButtonCard = ({ titleButton, handler, customClass, disabled }) => {
    return (
        <button
            type="button"
            className={`consoles__item-order-btn custom-button product-cart__order-btn ${customClass}`}
            onClick={(e) => handler(e)}
            disabled={disabled}
        >
            {titleButton}
        </button>
    );
};

ShopProductButtonCard.propTypes = {
    titleButton: PropTypes.string,
    handler: PropTypes.func,
    customClass: PropTypes.string,
    disabled: PropTypes.bool,
};

ShopProductButtonCard.defaultProps = {
    titleButton: "",
    handler: () => {},
    customClass: "",
    disabled:false
};

export default ShopProductButtonCard;
