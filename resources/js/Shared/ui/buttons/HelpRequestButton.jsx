import React from "react";
import PropTypes from "prop-types";

const HelpRequestButton = ({ titleButton, handler }) => {
    return (
        <button
            type="button"
            className="help-request__btn custom-button"
            onClick={(e) => handler(e)}
        >
            {titleButton}
        </button>
    );
};

HelpRequestButton.propTypes = {
    titleButton: PropTypes.string,
    handler: PropTypes.func,
};

HelpRequestButton.defaultProps = {
    titleButton: "",
    handler: () => {},
};

export default HelpRequestButton;
