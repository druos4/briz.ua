import React from "react";
import PropTypes from "prop-types";

const ButtonCloseModal = ({ content, handleCloseModal }) => {
    return (
        <button
            type="button"
            onClick={() => handleCloseModal(false, "")}
            className="custom-button modal-popup__button"
        >
            {content}
        </button>
    );
};
ButtonCloseModal.propTypes = {
    content: PropTypes.string,
    handleCloseModal: PropTypes.func,
};

ButtonCloseModal.defaultProps = {
    content: "",
    handleCloseModal: () => {},
};

export default ButtonCloseModal;
