import React from "react";
import PropTypes from "prop-types";

const ButtonLangSidebar = ({ titleButton, handler, active, customClass }) => {
    return (
        <button
            type="button"
            className={customClass}
            // className={
            //     active
            //         ? "mobile-sidebar__language-link mobile-sidebar__language-link--active"
            //         : "mobile-sidebar__language-link"
            // }
            onClick={handler}
        >
            {titleButton}
        </button>
    );
};

ButtonLangSidebar.propTypes = {
    titleButton: PropTypes.string,
    handler: PropTypes.func,
    active: PropTypes.bool,
};

ButtonLangSidebar.defaultProps = {
    titleButton: "",
    handler: () => {},
    active: false,
};

export default ButtonLangSidebar;
