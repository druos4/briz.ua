import React from "react";
import PropTypes from "prop-types";

const ButtonSubmitForm = ({
    titleButton,
    seoId,
    isLoading,
    type,
    submit,
    errors,
    className
}) => {
    const classFromType = (typeButton) => {
        switch (typeButton) {
            case "contacts":
                return "contacts__custom-button";
            case "callMeHeader":
                return "support__callme-btn custom-button";
            default:
                return "modal-popup__button modal-popup__button--tariff-submit";
        }
    };
    return (
        <button
            id={seoId}
            className={
                isLoading
                    ?  `custom-button custom-button--is-loading ${classFromType(type)} ${className}`
                    :  `custom-button ${classFromType(type)} ${className}`
            }
            type="submit"
            onClick={(e) => submit(e)}
            disabled={isLoading || errors}
        >
            {isLoading ? (
                <svg className="spinner" viewBox="0 0 50 50">
                    <circle
                        className="path"
                        cx="25"
                        cy="25"
                        r="20"
                        fill="none"
                        strokeWidth="5"
                    />
                </svg>
            ) : (
                titleButton
            )}
        </button>
    );
};
ButtonSubmitForm.propTypes = {
    titleButton: PropTypes.string,
    seoId: PropTypes.string,
    isLoading: PropTypes.bool,
    type: PropTypes.string,
    submit: PropTypes.func,
    errors: PropTypes.string,
    className: PropTypes.string
};
ButtonSubmitForm.defaultProps = {
    titleButton: "",
    seoId: "",
    isLoading: false,
    type: "",
    submit: () => {},
    errors: "",
    className:""
};

export default ButtonSubmitForm;
