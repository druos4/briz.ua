import React from "react";
import PropTypes from "prop-types";

const ShopProductButton = ({ titleButton, handler }) => {
    return (
        <button
            type="button"
            className="shop__item-order-btn"
            onClick={(e) => handler(e)}
        >
            {titleButton}
        </button>
    );
};

ShopProductButton.propTypes = {
    titleButton: PropTypes.string,
    handler: PropTypes.func,
};

ShopProductButton.defaultProps = {
    titleButton: "",
    handler: () => {},
};

export default ShopProductButton;
