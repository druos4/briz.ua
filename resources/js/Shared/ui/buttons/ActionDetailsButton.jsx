import React from "react";
import PropTypes from "prop-types";

const ActionDetailsButton = ({ titleButton, handler }) => {
    return (
        <button
            type="button"
            className="actions__item-order-btn custom-button"
            onClick={() => handler()}
        >
            {titleButton}
        </button>
    );
};

ActionDetailsButton.propTypes = {
    titleButton: PropTypes.string,
    handler: PropTypes.func,
};

ActionDetailsButton.defaultProps = {
    titleButton: "",
    handler: () => {},
};

export default ActionDetailsButton;
