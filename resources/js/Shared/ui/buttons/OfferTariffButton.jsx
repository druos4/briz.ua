import React from "react";
import PropTypes from "prop-types";

const OfferTariffButton = ({ titleButton, handler }) => {
    return (
        <button
            type="button"
            className="offer-tariffs__card-content-link"
            onClick={(e) => handler(e)}
        >
            {titleButton}
        </button>
    );
};

OfferTariffButton.propTypes = {
    titleButton: PropTypes.string,
    handler: PropTypes.func,
};

OfferTariffButton.defaultProps = {
    titleButton: "",
    handler: () => {},
};

export default OfferTariffButton;
