import React from "react";
import PropTypes from "prop-types";

const ActionsButton = ({ titleButton, handler }) => {
    return (
        <button
            type="button"
            className="consoles__item-order-btn custom-button"
            onClick={() => handler()}
        >
            {titleButton}
        </button>
    );
};

ActionsButton.propTypes = {
    titleButton: PropTypes.string,
    handler: PropTypes.func,
};

ActionsButton.defaultProps = {
    titleButton: "",
    handler: () => {},
};

export default ActionsButton;
