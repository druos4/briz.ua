/* eslint-disable react/forbid-prop-types */
/* eslint-disable no-shadow */
/* eslint-disable no-return-assign */
/* eslint-disable react/no-array-index-key */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-console */
import React, { useEffect, useState, useRef } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import L from "leaflet";
import { GestureHandling } from "leaflet-gesture-handling";
import { MapContainer, useMap, TileLayer, Marker } from "react-leaflet";
import MarkerClusterGroup from "react-leaflet-markercluster";
import { useTranslation } from "react-i18next";
import MapForm from "./MapForm";
import { setModalAccept } from "../../actions/actions";
import { usePosition } from "../GeoPosition/usePosition";
import "leaflet-gesture-handling/dist/leaflet-gesture-handling.css";
import { PRODUCT_PRODUCT_BY_ADDRESS } from "../../constants/apiParams";
import { useMediaQuery } from "../MediaQuery/useMediaQuery";
import { getLocaleHref } from "@/utils";

const CustomComponentMap = ({ lat, lon, zoom }) => {
    const map = useMap();

    map.setView([lat, lon], zoom);

    return null;
};

const createClusterCustom = (cluster) => {
    const count = cluster.getChildCount();

    let classNames = "";
    let value = 0;
    if (count >= 1000) {
        classNames = "large";
        value = 60;
    } else if (count < 1000 && count > 100) {
        classNames = "medium";
        value = 42;
    } else if (count < 100) {
        classNames = "small";
        value = 26;
    }

    return L.divIcon({
        html: `<span>${cluster.getChildCount()}</span>`,
        className: `marker-cluster-custom marker-cluster-custom--${classNames}`,
        iconSize: L.point(value, value, true),
    });
};

const CoverageMap = ({
    title,
    setModalAccept,
    watch,
    settings,
    currentPage,
    locale
}) => {
    const markerRef = useRef();
    const [markers, setMarkers] = useState([]);
    const [clickedMarker, setClickMarker] = useState({});
    const [valueStreetPopup, setValueStreetPopup] = useState(null);
    const [valueBuildingPopup, setValueBuildingPopup] = useState(null);
    const [markerRequested, setMarkerRequested] = useState(null);
    const [defaultValueCenterMarker, setDefaultValueCenterMarker] = useState({
        lat: 46.48282147637722,
        lon: 30.735668696567473,
    });
    const [defaultValueZoom, setDefaultValueZoom] = useState(11);
    const [isOpenPopupExistsAddress, setPopupExistsAddress] = useState(false);
    const { t } = useTranslation();
    const [isGeoPosition, setGeoPosition] = useState(null);
    const [isLoadingClickedMarker, setLoadingClickedMarker] = useState(false);
    const [isHiddenPartPopup, setHiddenPartPopup] = useState(false);
    const [isOpen, setOpenPopup] = useState(false);

    const isWidthDesktop = useMediaQuery("(min-width: 1280px)");

    const currentLocaleHref = getLocaleHref(locale)

    const { latitude, longitude } = usePosition(watch, settings);

    const getMarkers = async () => {
        try {
            await axios
                .get(`${currentLocaleHref}/map/markers`)
                .then((res) => {
                    setMarkers(res.data.markers);
                })
                .catch((e) => console.log(e.message));
        } catch (e) {
            console.log(e.message);
        }
    };

    useEffect(() => {
        if (Object.keys(clickedMarker).length) {
            setValueStreetPopup({ street: clickedMarker.street });
            setValueBuildingPopup({ dom: clickedMarker.dom });
            setLoadingClickedMarker(true);
        }

        setPopupExistsAddress(true);
    }, [clickedMarker]);

    const handleClickMarker = (marker) => {
        if (marker.lat !== clickedMarker.lat) {
            setClickMarker(marker);
            setOpenPopup(true);
            setTimeout(() => setLoadingClickedMarker(false), 1000);
        }
    };

    useEffect(() => {
        let cleanupFunction = false;

        if (!markers.length) {
            if (!cleanupFunction) {
                getMarkers();
            }
        }

        return () => (cleanupFunction = true);
    }, []);

    useEffect(() => {
        if (!!markerRequested && !!Object.keys(markerRequested).length) {
            setDefaultValueCenterMarker({
                lat: markerRequested.lat,
                lon: markerRequested.lon,
            });
            setDefaultValueZoom(18);
        }
    }, [markerRequested]);

    const handleAppareModal = (productTitle) => {
        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle,
            productId: productTitle,
            titleModal: t("connectionRequest"),
            titleButton: t("send"),
            apiUrl: PRODUCT_PRODUCT_BY_ADDRESS.url,
            typeProduct: PRODUCT_PRODUCT_BY_ADDRESS.type,
            titleProduct: "",
            body: {
                [PRODUCT_PRODUCT_BY_ADDRESS.type]: productTitle,
                ghost: "",
                exist: true,
            },
            keyFormRedux: "default",
        });
    };

    const renderModalPopup = (valueStr, valueDom) => {
        const address = `${valueStr}, ${valueDom}`;

        return (
            <div
                className={
                    isHiddenPartPopup
                        ? "modal-popup-coverage-map__content modal-popup-coverage-map__content--mobile-modif"
                        : "modal-popup-coverage-map__content"
                }
            >
                {isLoadingClickedMarker ? (
                    <div className="search-modal-map-loader">
                        <svg
                            version="1.1"
                            id="L4"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            x="0px"
                            y="0px"
                            viewBox="0 0 100 100"
                            enableBackground="new 0 0 0 0"
                            xmlSpace="preserve"
                        >
                            <circle
                                fill="#00A3A4"
                                stroke="none"
                                cx="6"
                                cy="50"
                                r="6"
                            >
                                <animate
                                    attributeName="opacity"
                                    dur="1s"
                                    values="0;1;0"
                                    repeatCount="indefinite"
                                    begin="0.1"
                                />
                            </circle>
                            <circle
                                fill="#00A3A4"
                                stroke="none"
                                cx="26"
                                cy="50"
                                r="6"
                            >
                                <animate
                                    attributeName="opacity"
                                    dur="1s"
                                    values="0;1;0"
                                    repeatCount="indefinite"
                                    begin="0.2"
                                />
                            </circle>
                            <circle
                                fill="#00A3A4"
                                stroke="none"
                                cx="46"
                                cy="50"
                                r="6"
                            >
                                <animate
                                    attributeName="opacity"
                                    dur="1s"
                                    values="0;1;0"
                                    repeatCount="indefinite"
                                    begin="0.3"
                                />
                            </circle>
                        </svg>
                    </div>
                ) : (
                    <>
                        <div className="modal-popup-coverage-map__address-client">
                            <span className="modal-popup-coverage-map__title">
                                {t("address")}
                            </span>
                            <p className="modal-popup-coverage-map__have">
                                {address}
                            </p>
                        </div>
                        <span
                            className="modal-popup-coverage-map__border"
                            style={
                                isHiddenPartPopup
                                    ? { display: "none" }
                                    : { display: "block" }
                            }
                        />
                        <div
                            className="modal-popup-coverage-map__inet"
                            style={
                                isHiddenPartPopup
                                    ? { display: "none" }
                                    : { display: "block" }
                            }
                        >
                            <span
                                className="modal-popup-coverage-map__title"
                                style={
                                    isHiddenPartPopup
                                        ? { display: "none" }
                                        : { display: "block" }
                                }
                            >
                                {t("internet")}
                            </span>
                            <p
                                className="modal-popup-coverage-map__have"
                                style={
                                    isHiddenPartPopup
                                        ? { display: "none" }
                                        : { display: "block" }
                                }
                            >
                                {t("thereIs")}
                            </p>
                        </div>
                        <span
                            className="modal-popup-coverage-map__border"
                            style={
                                isHiddenPartPopup
                                    ? { display: "none" }
                                    : { display: "block" }
                            }
                        />
                        <div
                            className="modal-popup-coverage-map__tv"
                            style={
                                isHiddenPartPopup
                                    ? { display: "none" }
                                    : { display: "block" }
                            }
                        >
                            <span
                                className="modal-popup-coverage-map__title"
                                style={
                                    isHiddenPartPopup
                                        ? { display: "none" }
                                        : { display: "block" }
                                }
                            >
                                {t("television")}
                            </span>
                            <p
                                className="modal-popup-coverage-map__have"
                                style={
                                    isHiddenPartPopup
                                        ? { display: "none" }
                                        : { display: "block" }
                                }
                            >
                                {t("thereIs")}
                            </p>
                        </div>
                        <button
                            type="button"
                            className="offer-tariffs__card-content-link modal-popup-coverage-map__btn"
                            onClick={() => handleAppareModal(address)}
                            style={
                                isHiddenPartPopup
                                    ? { display: "none" }
                                    : { display: "block" }
                            }
                        >
                            {t("sendRequest")}
                        </button>
                    </>
                )}
            </div>
        );
    };

    const haveNotNet = (valueStr, valueDom) => {
        const address = `${valueStr}, ${valueDom}`;

        return (
            <div
                className={
                    isHiddenPartPopup
                        ? "modal-popup-coverage-map__content modal-popup-coverage-map__content--mobile-modif"
                        : "modal-popup-coverage-map__content"
                }
            >
                <div className="modal-popup-coverage-map__address-client">
                    <span className="modal-popup-coverage-map__title">
                        {t("address")}
                    </span>
                    <p className="modal-popup-coverage-map__have">{address}</p>
                </div>
                <span
                    className="modal-popup-coverage-map__border"
                    style={
                        isHiddenPartPopup
                            ? { display: "none" }
                            : { display: "block" }
                    }
                />
                <div
                    className="modal-popup-coverage-map__inet"
                    style={
                        isHiddenPartPopup
                            ? { display: "none" }
                            : { display: "block" }
                    }
                >
                    <span
                        className="modal-popup-coverage-map__title"
                        style={
                            isHiddenPartPopup
                                ? { display: "none" }
                                : { display: "block" }
                        }
                    >
                        {t("internet")}
                    </span>
                    <p
                        className="modal-popup-coverage-map__have-not"
                        style={
                            isHiddenPartPopup
                                ? { display: "none" }
                                : { display: "block" }
                        }
                    >
                        {t("buildingNetwork")}
                    </p>
                </div>
                <span
                    className="modal-popup-coverage-map__border"
                    style={
                        isHiddenPartPopup
                            ? { display: "none" }
                            : { display: "block" }
                    }
                />
                <div
                    className="modal-popup-coverage-map__tv"
                    style={
                        isHiddenPartPopup
                            ? { display: "none" }
                            : { display: "block" }
                    }
                >
                    <span
                        className="modal-popup-coverage-map__title"
                        style={
                            isHiddenPartPopup
                                ? { display: "none" }
                                : { display: "block" }
                        }
                    >
                        {t("television")}
                    </span>
                    <p
                        className="modal-popup-coverage-map__have-not"
                        style={
                            isHiddenPartPopup
                                ? { display: "none" }
                                : { display: "block" }
                        }
                    >
                        {t("buildingNetwork")}
                    </p>
                </div>
                <button
                    type="button"
                    className="offer-tariffs__card-content-link modal-popup-coverage-map__btn"
                    onClick={() => handleAppareModal(address)}
                    style={
                        isHiddenPartPopup
                            ? { display: "none" }
                            : { display: "block" }
                    }
                >
                    {t("orderConnection")}
                </button>
            </div>
        );
    };

    const hidePartPopup = () => {
        setHiddenPartPopup(!isHiddenPartPopup);
    };

    const callbackFormMarkerRequested = (data) => {
        if (!data) {
            setMarkerRequested(data);
            setOpenPopup(true);
        }

        if (Object.keys(data).length) {
            setMarkerRequested(data);
            setOpenPopup(true);
        }
    };

    const callbackFormCenterMarker = (data) => {
        setDefaultValueCenterMarker(data);
    };

    const callbackFormValueZook = (zoom) => {
        setDefaultValueZoom(zoom);
    };

    const callbackFormClickMarker = (marker) => {
        setClickMarker(marker);
    };

    const callbackFormValueStreetPopup = (data) => {
        setValueStreetPopup(data);
    };

    const callbackFormValueBuildingPopup = (data) => {
        setValueBuildingPopup(data);
    };

    const callbackFormIsExistsAddress = (flag) => {
        setPopupExistsAddress(flag);
    };

    const callbackFormIsGeoPosition = (flag) => {
        setGeoPosition(flag);
    };

    const handleClosePopup = () => {
        setOpenPopup(false);
    };

    const scrollTextMobile =
        (document.querySelector("html").lang === "ru" &&
            "Чтобы изменить масштаб, прокручивайте карту двумя пальцами") ||
        (document.querySelector("html").lang === "ua" &&
            "Щоб змінити масштаб, прокручивайте карту двома пальцями") ||
        (document.querySelector("html").lang === "en" &&
            "Use two fingers to scroll the map to zoom");
    const scrollText =
        (document.querySelector("html").lang === "ru" &&
            "Чтобы изменить масштаб, прокручивайте карту, удерживая клавишу Ctrl") ||
        (document.querySelector("html").lang === "ua" &&
            "Щоб змінити масштаб, прокручивайте карту, утримуючи клавішу Ctrl") ||
        (document.querySelector("html").lang === "en" &&
            "To zoom in, scroll the map while holding down the Ctrl key");
    const valStreet =
        valueStreetPopup !== null && valueStreetPopup.hasOwnProperty("street")
            ? valueStreetPopup.street
            : "";
    const valBuilding =
        valueBuildingPopup !== null && valueBuildingPopup.hasOwnProperty("dom")
            ? valueBuildingPopup.dom
            : "";
    return (
        <>
            <div className="wrapper home__wrapper-fomr-map">
                {currentPage === "Map" ? (
                    <h1 className="page-subtitle home__page-subtitle">
                        {title}
                    </h1>
                ) : (
                    <div className="page-subtitle home__page-subtitle">
                        {title}
                    </div>
                )}

                <MapForm
                    callbackFormMarkerRequested={callbackFormMarkerRequested}
                    callbackFormCenterMarker={callbackFormCenterMarker}
                    callbackFormValueZook={callbackFormValueZook}
                    callbackFormClickMarker={callbackFormClickMarker}
                    callbackFormValueStreetPopup={callbackFormValueStreetPopup}
                    callbackFormValueBuildingPopup={
                        callbackFormValueBuildingPopup
                    }
                    callbackFormIsExistsAddress={callbackFormIsExistsAddress}
                    callbackFormIsGeoPosition={callbackFormIsGeoPosition}
                    valueStreetPopup={valueStreetPopup}
                    valueBuildingPopup={valueBuildingPopup}
                    currentLocaleHref={currentLocaleHref}
                />
            </div>
            <div className="margin-map">
                {(!!Object.keys(clickedMarker).length &&
                    isOpenPopupExistsAddress &&
                    isOpen) ||
                    (isOpen &&
                        markerRequested !== null &&
                        !!Object.keys(markerRequested).length &&
                        !!valueStreetPopup &&
                        valueStreetPopup.street.length) ? (
                    <div
                        className={
                            isHiddenPartPopup
                                ? "modal-popup-coverage-map modal-popup-coverage-map--mobile-modif"
                                : "modal-popup-coverage-map"
                        }
                    >
                        <span
                            className="modal-popup-coverage-map__close"
                            onClick={() => handleClosePopup()}
                        />
                        {renderModalPopup(valStreet, valBuilding)}
                        <span
                            className={
                                isHiddenPartPopup
                                    ? "modal-popup-coverage-map__mobile-bottom-pannel modal-popup-coverage-map__mobile-bottom-pannel--hide-icon"
                                    : "modal-popup-coverage-map__mobile-bottom-pannel modal-popup-coverage-map__mobile-bottom-pannel--show-icon"
                            }
                            onClick={() => hidePartPopup()}
                        />
                    </div>
                ) : null}
                {isOpen &&
                    markerRequested === false &&
                    !Object.keys(clickedMarker).length ? (
                    <div
                        className={
                            isHiddenPartPopup
                                ? "modal-popup-coverage-map modal-popup-coverage-map--mobile-modif"
                                : "modal-popup-coverage-map"
                        }
                    >
                        <span
                            className="modal-popup-coverage-map__close"
                            onClick={() => handleClosePopup()}
                        />
                        {haveNotNet(valStreet, valBuilding)}
                        <span
                            className={
                                isHiddenPartPopup
                                    ? "modal-popup-coverage-map__mobile-bottom-pannel modal-popup-coverage-map__mobile-bottom-pannel--hide-icon"
                                    : "modal-popup-coverage-map__mobile-bottom-pannel modal-popup-coverage-map__mobile-bottom-pannel--show-icon"
                            }
                            onClick={() => hidePartPopup()}
                        />
                    </div>
                ) : null}

                <MapContainer
                    className="coverage-map"
                    center={[
                        defaultValueCenterMarker.lat,
                        defaultValueCenterMarker.lon,
                    ]}
                    zoom={defaultValueZoom}
                    maxZoom={18}
                    scrollWheelZoom={false}
                    gestureHandling={isWidthDesktop ? true : false}
                    gestureHandlingOptions={{
                        duration: 5000,
                        text: {
                            touch: scrollTextMobile,
                            scroll:
                                window.screen.availWidth > 1025
                                    ? scrollText
                                    : scrollTextMobile,
                            scrollMac: scrollText,
                        },
                    }}
                >
                    {!!markerRequested &&
                        !!Object.keys(markerRequested).length &&
                        !Object.keys(clickedMarker).length && (
                            <CustomComponentMap
                                lat={markerRequested.lat}
                                lon={markerRequested.lon}
                                zoom={18}
                            />
                        )}
                    {isGeoPosition && !!latitude && !!longitude && (
                        <CustomComponentMap
                            lat={latitude}
                            lon={longitude}
                            zoom={18}
                        />
                    )}
                    <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

                    <MarkerClusterGroup
                        showCoverageOnHover={false}
                        iconCreateFunction={createClusterCustom}
                    >
                        {!!markers.length &&
                            markers.map((marker, index) => {
                                return (
                                    <Marker
                                        position={[marker.lat, marker.lon]}
                                        key={index}
                                        eventHandlers={{
                                            click: () =>
                                                handleClickMarker(marker),
                                        }}
                                        ref={markerRef}
                                    />
                                );
                            })}
                    </MarkerClusterGroup>
                </MapContainer>
            </div>
            <div className="wrapper">
                <section className="coverage-map__bottom-section">
                    <div className="coverage-map__bottom-item">
                        <span className="coverage-map__bottom-item-icon coverage-map__bottom-item-icon--green" />
                        <span className="coverage-map__bottom-item-text">
                            {t("internet")} {t("and")} {t("television")}
                        </span>
                    </div>
                    {/* <div className="coverage-map__bottom-item">
                        <span className="coverage-map__bottom-item-icon coverage-map__bottom-item-icon--percent" />
                        <span className="coverage-map__bottom-item-text">
                            {t("connectionDiscounts")}
                        </span>
                    </div> */}
                    <div className="coverage-map__bottom-item">
                        <span className="coverage-map__bottom-item-icon coverage-map__bottom-item-icon--figure" />
                        <span className="coverage-map__bottom-item-text">
                            {t("buildingNetworkBeHereSoon")}
                        </span>
                    </div>
                </section>
            </div>
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        currentPage: state.currentPage.currentPage,
    };
};

CoverageMap.propTypes = {
    title: PropTypes.string,
    setModalAccept: PropTypes.func,
    currentPage: PropTypes.string,
    watch: PropTypes.any,
    settings: PropTypes.any,
    locale: PropTypes.string
};
CoverageMap.defaultProps = {
    title: "",
    setModalAccept: () => { },
    currentPage: "",
    watch: undefined,
    settings: undefined,
    locale: "ua"
};

export default connect(mapStateToProps, { setModalAccept })(CoverageMap);
