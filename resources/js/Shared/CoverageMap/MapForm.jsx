/* eslint-disable no-empty */
/* eslint-disable no-return-assign */
/* eslint-disable no-console */
/* eslint-disable no-prototype-builtins */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import Autocomplete, {
    createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import { useTranslation } from "react-i18next";

const filter = createFilterOptions();

const CssTextField = withStyles({
    root: {
        "& input": {
            height:
                (window.screen.availWidth < 768 && 7) ||
                (window.screen.availWidth >= 768 && 15),
        },
        "& label.Mui-focused": {
            color: "#00a3a4",
            fontSize: "16px",
        },

        "& .MuiInputLabel-outlined": {
            transform:
                window.screen.availWidth < 768
                    ? "translate(14px, 14px) scale(1)"
                    : "translate(14px, 18px) scale(1)",
        },

        "& .MuiInputLabel-outlined.MuiInputLabel-shrink": {
            transform: "translate(14px, -7px) scale(0.75)",
        },

        "&:hover .MuiInputLabel-formControl": {
            color: "#424242",
            transition: ".2s ease-in",
        },

        "& .MuiOutlinedInput-root": {
            "& fieldset": {
                borderColor: "#B8B8B8",
                borderRadius: "10px",
            },
            "&:hover fieldset": {
                borderColor: "#424242",
                color: "#424242",
                transition: ".2s ease-in",
            },
            "&.Mui-focused fieldset": {
                borderColor: "#00a3a4",
                borderRadius: "10px",
            },
        },
    },
})(TextField);

const useStyles = makeStyles(() => ({
    root: {
        display: "flex",
        flexWrap: "wrap",
    },
    margin: {
        margin: 0,
    },
}));

const MapForm = ({
    callbackFormMarkerRequested,
    callbackFormCenterMarker,
    callbackFormValueZook,
    callbackFormClickMarker,
    callbackFormValueStreetPopup,
    callbackFormValueBuildingPopup,
    callbackFormIsExistsAddress,
    callbackFormIsGeoPosition,
    currentLocaleHref
}) => {
    const classes = useStyles();
    const streetField = useRef();
    const buildingField = useRef();

    const [optionsStreet, setOptionsStreet] = useState([]);
    const [valueStreet, setValueStreet] = useState(null);
    const [valueBuilding, setValueBuilding] = useState(null);
    const [buildings, setBuildings] = useState([]);

    const { t } = useTranslation();

    useEffect(() => {
        let cleanupFunction = false;

        (async () => {
            try {
                await axios
                    .get(`${currentLocaleHref}/map/streets`)
                    .then((res) => {
                        if (!cleanupFunction) {
                            setOptionsStreet(res.data.result);
                        }
                    });
            } catch (e) {
                console.log(e.message);
            }
        })();

        return () => (cleanupFunction = true);
    }, []);

    useEffect(() => {
        if (buildings.length) {
            const field =
                buildingField.current.getElementsByTagName("input")[0];

            field.focus();
        }
    }, [buildings]);

    const getBuildings = async () => {
        if (!!valueStreet && valueStreet.hasOwnProperty("id")) {
            await axios
                .get(`/map/houses?street=${valueStreet.street}&street_id=${valueStreet.id}`)
                .then((res) => setBuildings(res.data.result));
        }
    };

    useEffect(() => {
        if (
            !!valueStreet &&
            !!Object.keys(valueStreet).length &&
            valueStreet.hasOwnProperty("id")
        ) {
            getBuildings();
        }

        if (!valueStreet) {
            setBuildings([]);
            setValueBuilding(null);
            buildingField.current.value = "";
            callbackFormMarkerRequested({});
            callbackFormCenterMarker({
                lat: 46.48282147637722,
                lon: 30.735668696567473,
            });
            callbackFormValueZook(11);
        }
    }, [valueStreet]);

    const handleClickBtn = async (e) => {
        e.preventDefault();

        if (
            !!valueStreet &&
            !!Object.keys(valueStreet).length &&
            !!valueBuilding &&
            !!valueBuilding.dom.length &&
            valueBuilding.dom !== "0"
        ) {
            const valueStr =
                typeof valueStreet === "string"
                    ? valueStreet
                    : valueStreet.street;
            const valueStrId = valueStreet.hasOwnProperty("id")
                ? valueStreet.id
                : 0;
            const valueDom =
                typeof valueBuilding === "string"
                    ? valueBuilding
                    : valueBuilding.dom;
            const valueDomId = valueBuilding.hasOwnProperty("id")
                ? valueBuilding.id
                : 0;

            callbackFormMarkerRequested({});
            callbackFormClickMarker({});
            await axios
                .get(`/map/marker?street=${valueStr}&street_id=${valueStrId}&dom=${valueDom}&dom_id=${valueDomId}`)
                .then((res) => {
                    callbackFormMarkerRequested(res.data.result);
                    callbackFormValueStreetPopup({
                        street: valueStreet.street,
                    });
                    callbackFormValueBuildingPopup({ dom: valueBuilding.dom });
                });
        }
        // else {
        //     console.log('Попытка отправить пробелы')
        // }
    };

    const defineGeoPosition = () => {
        callbackFormIsGeoPosition(true);
    };

    const leaveDefineGeoPosition = () => {
        callbackFormIsGeoPosition(false);
    };

    const clearHandlerStreet = (e, value) => {
        if (!value.length) {
            callbackFormIsExistsAddress(false);
            callbackFormClickMarker({});
        }
    };

    const handleBlurStreet = async (e) => {
        if (
            !/^ *$/.test(e.target.value) &&
            e.target.value.length > 3 &&
            valueStreet === null
        ) {
            setValueStreet({ street: e.target.value });
            await axios
                .get("/map/houses", {
                    street_id: 0,
                    street: e.target.value,
                })
                .then((res) => setBuildings(res.data.result));
        }
    };

    const handleEnterSubmit = async (e) => {
        if (e.key === "Enter") {
            if (!/^ *$/.test(valueStreet) && valueStreet.length > 3) {
                setValueStreet({ street: valueStreet });
                await axios
                    .get("/map/houses", {
                        street_id: 0,
                        street: valueStreet,
                    })
                    .then((res) => setBuildings(res.data.result));
            }
        }
    };

    const clearHandlerBuilding = (e, value) => {
        if (!value.length) {
        }
    };

    // const handleChangeStreet = (e) => {
    //     setValueStreet({ street: e.target.value });
    // };

    const handleChangeBuilding = (e) => {
        if (!/^ *$/.test(e.target.value) && !buildings.length) {
            setValueBuilding({ dom: e.target.value });
        }
    };

    const handleBlurBuilding = (e) => {
        if (!/^ *$/.test(e.target.value)) {
            setValueBuilding({ dom: e.target.value });
        }
    };

    return (
        <form onSubmit={(e) => handleClickBtn(e)}>
            <div className="search-address">
                <div
                    className="search-address__centering"
                    onClick={(e) => defineGeoPosition(e)}
                    onMouseLeave={(e) => leaveDefineGeoPosition(e)}
                >
                    <span className="search-address__centering-icon" />
                </div>

                <div className="search-address__autocomplete">
                    <Autocomplete
                        id="street"
                        // freeSolo
                        onInputChange={(e, value, reason) =>
                            clearHandlerStreet(e, value, reason)
                        }
                        options={optionsStreet.map((option) => option)}
                        getOptionLabel={(option) => {
                            if (typeof option.street === "string") {
                                return option.street;
                            }

                            if (option.inputValue) {
                                return option.inputValue;
                            }

                            if (option.street) {
                                return option.street;
                            }

                            return "";
                        }}
                        className="search-address__autocomplete-element"
                        renderInput={(params) => (
                            <CssTextField
                                {...params}
                                type="text"
                                required
                                onBlur={(e) => handleBlurStreet(e)}
                                className={classes.margin}
                                label={t("streetPlaceholder")}
                                variant="outlined"
                                name="street"
                                ref={streetField}
                                inputProps={{
                                    ...params.inputProps,
                                    maxLength: 50,
                                }}
                                onKeyPress={(e) => handleEnterSubmit(e)}
                            />
                        )}
                        // renderOption={(option) => option.street}
                        value={valueStreet}
                        onChange={(event, newValue) => {
                            if (typeof newValue === "string") {
                                setValueStreet(newValue);
                            } else if (newValue && newValue.inputValue) {
                                setValueStreet({
                                    street: newValue.inputValue,
                                });
                            } else {
                                setValueStreet(newValue);
                            }
                        }}
                        filterOptions={(options, params) => {
                            return filter(options, params);
                        }}
                        selectOnFocus
                        handleHomeEndKeys
                        freeSolo
                    />
                </div>
                <div className="search-address__building">
                    <Autocomplete
                        id="building"
                        freeSolo
                        options={buildings.map((building) => building.dom)}
                        onInputChange={(e, value, reason) =>
                            clearHandlerBuilding(e, value, reason)
                        }
                        getOptionLabel={(option) => {
                            if (typeof option === "string") {
                                return option;
                            }

                            if (option.inputValue) {
                                return option.inputValue;
                            }

                            return option.dom;
                        }}
                        className="search-address__autocomplete-element-building"
                        renderInput={(params) => (
                            <CssTextField
                                required
                                {...params}
                                onChange={handleChangeBuilding}
                                onBlur={handleBlurBuilding}
                                className={classes.margin}
                                label={t("housePlaceholder")}
                                variant="outlined"
                                name="building"
                                ref={buildingField}
                                type="text"
                                inputProps={{
                                    ...params.inputProps,
                                    maxLength: 20,
                                }}
                            />
                        )}
                        // renderOption={(option) => option.dom}
                        value={valueBuilding}
                        onChange={(event, newValue) => {
                            if (typeof newValue === "string") {
                                setValueBuilding({
                                    dom: newValue,
                                });
                            } else if (newValue && newValue.inputValue) {
                                setValueBuilding({
                                    dom: newValue.inputValue,
                                });
                            } else {
                                setValueBuilding(newValue);
                            }
                        }}
                        filterOptions={(options, params) => {
                            return filter(options, params);
                        }}
                        selectOnFocus
                        clearOnBlur
                        handleHomeEndKeys
                    />
                </div>
                <button
                    type="submit"
                    className="custom-button search-address__btn-search"
                >
                    {t("verify")}
                </button>
            </div>
        </form>
    );
};

MapForm.propTypes = {
    callbackFormMarkerRequested: PropTypes.func,
    callbackFormCenterMarker: PropTypes.func,
    callbackFormValueZook: PropTypes.func,
    callbackFormClickMarker: PropTypes.func,
    callbackFormValueStreetPopup: PropTypes.func,
    callbackFormValueBuildingPopup: PropTypes.func,
    callbackFormIsExistsAddress: PropTypes.func,
    callbackFormIsGeoPosition: PropTypes.func,
    currentLocaleHref: PropTypes.string
};
MapForm.defaultProps = {
    callbackFormMarkerRequested: () => {},
    callbackFormCenterMarker: () => {},
    callbackFormValueZook: () => {},
    callbackFormClickMarker: () => {},
    callbackFormValueStreetPopup: () => {},
    callbackFormValueBuildingPopup: () => {},
    callbackFormIsExistsAddress: () => {},
    callbackFormIsGeoPosition: () => {},
    currentLocaleHref: "ua"
};

export default MapForm;
