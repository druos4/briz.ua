import React from 'react';
import CoverageMap from './CoverageMap';
import { useTranslation } from "react-i18next";
import { useIsSsr } from '../../hooks/useIsSsr';
import { isIos } from '../../utils';

const CoverageMapContainer = ({locale}) => {
    const isSsr = useIsSsr();
    const { t } = useTranslation();
    
    const ios = !isSsr && isIos();
    return (
        <section className={ios ? "coverage-map-page coverage-map-page--ios" : "coverage-map-page"}>
            <CoverageMap title={t('findOutIfThereIsBreeze')} locale={locale} />
        </section>
    )
};

export default CoverageMapContainer;