import React from 'react';
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { useIsSsr } from '../../hooks/useIsSsr';

const OfferBackCard = ({ onClick, currentTariff }) => {
    const isSsr = useIsSsr();
    const { t } = useTranslation();
    const lang = !isSsr && app.getLang();

    const handleClick = (e) => {
        onClick(e);
    }

    return (
        <div className="offer-tariffs__cards-item" onClick={handleClick}>
            <div className="offer-tariffs__card offer-tariffs__card--back">
                {/* <div className="offer-tariffs__card-back"> */}
                {
                    !!currentTariff.can_prepay_discount && !!Object.keys(currentTariff.discounts).length ?
                        <div className="offer-tariffs__back-block">
                            <p className="offer-tariffs__back-title">{t('prepaidDiscounts')}</p>
                            {
                                Object.keys(currentTariff.discounts).map((discountIndex) => {
                                    const discountItem = currentTariff.discounts[discountIndex];

                                    return (
                                        <div className="offer-tariffs__back-list-discounts" key={discountIndex}>
                                            <span className="offer-tariffs__back-discounts-month">
                                                {discountItem.month}{" "}
                                                {t('monthShort')}
                                            </span>
                                            <span className="offer-tariffs__back-discounts-percent">{discountItem.discount}</span>
                                            <span className="offer-tariffs__back-discounts-count">
                                                <strong>{discountItem.amount}</strong>{" "} 
                                                <span className={lang === 'en' ? "offer-tariffs__currency-back-card" : ""}>{t('UAHmonth')}</span>
                                            </span>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        :
                        null
                }
                <div className="offer-tariffs__back-block">
                    {
                        !!currentTariff.can_get_bonuses || !!currentTariff.can_seven_days ?
                            <p className="offer-tariffs__back-title">{t('afterConnecting')}</p>
                            :
                            null
                    }
                    
                    <div className="offer-tariffs__back-bonuses">
                        {
                            currentTariff.can_get_bonuses ?
                                <p className="offer-tariffs__back-bonuses-txt" onClick={(e) => e.stopPropagation()}>
                                    <span 
                                        className="offer-tariffs__back-bonuses-icon offer-tariffs__back-bonuses-icon--bonus"
                                    /> {t('accrual')} {currentTariff.bonuses} {t('accrual25Bonuses')}
                                </p>
                                :
                                null
                        }

                        {
                            currentTariff.can_seven_days ?
                                <p className="offer-tariffs__back-bonuses-txt" onClick={(e) => e.stopPropagation()}>
                                    <span 
                                        className="offer-tariffs__back-bonuses-icon offer-tariffs__back-bonuses-icon--7days"
                                    /> {t('service7daysActive')}
                                </p>
                                :
                                null
                        }
                    </div>
                </div>
                <div className="offer-tariffs__back-btn-wrapper">
                    <span className="offer-tariffs__back-btn">{t('backTo')}</span>
                </div>
                {/* </div> */}
            </div>
        </div>
    )
}

OfferBackCard.propTypes = {
    onClick: PropTypes.func,
    currentTariff: PropTypes.shape({
        can_prepay_discount: PropTypes.number,
        can_seven_days: PropTypes.number,
        bonuses: PropTypes.number,
        can_get_bonuses: PropTypes.number,
        discounts: PropTypes.arrayOf(PropTypes.object),
    }),
};
OfferBackCard.defaultProps = {
    onClick: () => {},
    currentTariff: {},
};

export default OfferBackCard;
