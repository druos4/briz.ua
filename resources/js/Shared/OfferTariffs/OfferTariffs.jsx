/* eslint-disable react/prop-types */
/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */
/* eslint-disable no-return-assign */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable array-callback-return */
/* eslint-disable react/no-danger */
import React, { useState, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { InertiaLink } from "@inertiajs/inertia-react";
import ReactCardFlip from "react-card-flip";
import { useTranslation } from "react-i18next";
import OfferBackCard from "./OfferBackCard";
import { setModalAccept } from "../../actions/actions";
import { PRODUCT_SERVICE } from "../../constants/apiParams";
import OfferTariffButton from "../ui/buttons/OfferTariffButton";
import { useIsSsr } from "../../hooks/useIsSsr";
import { isMozilla } from "../../utils";

const OfferTariffs = ({
    tariff,
    linkText,
    linkUrl,
    tariffKey,
    cardsListElement,
    setModalAccept,
}) => {
    const isSsr = useIsSsr();
    const { t } = useTranslation();
    const lang = !isSsr && app.getLang();
    const mozilla = !isSsr && isMozilla();
    // ссылка на элемент input range
    const inputRange = useRef();
    // дефолтный цвет фона для input range
    const inactive = "#D9D9D9";

    const [value, setValue] = useState(1);
    const [currentTariff, setCurrentTariff] = useState(tariff[0]);
    // Булевое значение на переворот карточек
    const [isFlipped, setIsFlipped] = useState(false);

    // минимальное и максимальное значения для range шкалы
    const minValue = 1;
    const maxValue = tariff.length;
    // Начальный рассчет range шкалы
    const progress = Math.floor(
        ((Number(value) - Number(minValue)) /
            (Number(maxValue) - Number(minValue))) *
            100
    );

    // Начальный фон для range шкалы
    const styleInputRange = {
        background: `linear-gradient(90deg, #63A7F7 ${
            progress <= 0 ? progress : 0
        }%, #46C7BE ${progress <= 21.35 ? progress : 21.35}%, #3CC763 ${
            progress <= 42.71 ? progress : 42.71
        }%, #FAC23D ${progress <= 61.98 ? progress : 61.98}%, #F9A33F ${
            progress <= 80.59 ? progress : 80.59
        }%, #F9A33F ${
            progress <= 100 ? progress : 100
        }%, ${inactive} ${progress}% 100%)`,
    };

    // const standalone = window.navigator.standalone;
    // const userAgent = window.navigator.userAgent.toLowerCase();
    // const safari = /safari/.test(userAgent);
    // const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    // const ios = /iphone|ipod|ipad/.test(userAgent);

    // Если в тарифах всего один итем, то через эффекты установится прогресс бар на 100%, применится градиент и установится input range в disabled, чтобы избежать ошибок
    useEffect(() => {
        tariff.map((item, index) => {
            if (item.first_to_show) {
                setCurrentTariff(item);
                setValue(index + 1);
            }
        });

        if (Number(minValue) === maxValue) {
            inputRange.current.min = 0;
            inputRange.current.disabled = true;
            inputRange.current.style.background = `linear-gradient(90deg, #00a3a4 13%, #FAC23D 50%, #FA483D  100%)`;
        }

        // Парсер для разметки описания тарифа
        // let a = currentTariff.description;
        // const parser = new DOMParser();
        // const document = parser.parseFromString(a, 'text/html');
        // const rows = document.querySelectorAll('p');
        // const employList = Array.from(rows).map(row => row.innerHTML);

        // console.log(employList, 'EMPLOY LIST')
    }, []);

    const handleRange = (min, max) => (e) => {
        const { value } = e.target;
        // Расчет шкалы при перетаскивании. Полученный результат служит для рассчета градиента range панели
        const progress = Math.floor(
            ((Number(value) - Number(min)) / (Number(max) - Number(min))) * 100
        );
        setValue(value);

        // Сохранение текущего тарифа в локальный стейт
        setCurrentTariff(tariff[Number(value) - 1]);
        // Установка фона для шкалы прогресса
        const backgroundRangeBar = `linear-gradient(90deg, #63A7F7 ${
            progress <= 0 ? progress : 0
        }%, #46C7BE ${progress <= 21.35 ? progress : 21.35}%, #3CC763 ${
            progress <= 42.71 ? progress : 42.71
        }%, #FAC23D ${progress <= 61.98 ? progress : 61.98}%, #F9A33F ${
            progress <= 80.59 ? progress : 80.59
        }%, #F9A33F ${
            progress <= 100 ? progress : 100
        }%, ${inactive} ${progress}% 100%)`;

        inputRange.current.style.background = backgroundRangeBar;
    };

    const handleClick = () => {
        // Переворот карточек при клике
        setIsFlipped(!isFlipped);
    };

    const handleAppareModal = (title, id, seoId) => {
        // Открытие модального окна
        setModalAccept({
            showModalAccept: true,
            typeModalAccept: "modalForm",
            productTitle: title,
            productId: id,
            titleModal: t("connectionRequest"),
            titleButton: t("send"),
            apiUrl: PRODUCT_SERVICE.url,
            typeProduct: PRODUCT_SERVICE.type,
            titleProduct: t("rate"),
            seoId,
            body: { [PRODUCT_SERVICE.type]: id, ghost: "" },
            keyFormRedux: "default",
        });
    };

    return (
        <div className="offer-tariffs__card-link-wrapper">
            <a
                href={linkUrl}
                className="offer-tariffs__cards-item-link"
            >
                {linkText}
            </a>
            <div className="scale offer-tariffs__scale">
                <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
                    <div
                        className="offer-tariffs__cards-item"
                        onClick={handleClick}
                    >
                        <div className="offer-tariffs__card offer-tariffs__card--default">
                            <div
                                className={`offer-tariffs__card-top-bg offer-tariffs__card-top-bg--${tariffKey}`}
                            >
                                <img
                                    src={currentTariff.picture}
                                    className="offer-tariffs__card-img-bg"
                                    alt={`Тариф ${linkText} ${currentTariff.title} - інтернет-провайдер Briz в Одесі`}
                                    title={`Тариф ${linkText} ${currentTariff.title}`}
                                />
                                {currentTariff.price_old ? (
                                    <span className="offer-tariffs__card-top-discount">
                                        {t("discount")}
                                    </span>
                                ) : null}
                            </div>
                            <div className="offer-tariffs__card-content">
                                <div className="offer-tariffs__card-content-item">
                                    <div
                                        className="offer-tariffs__card-content-item-margin"
                                        onClick={(e) => e.stopPropagation()}
                                    >
                                        <span
                                            className={
                                                // currentTariff.title.length > 19 ?
                                                // "offer-tariffs__card-content-speed offer-tariffs__card-content-speed--other-font-size"
                                                // :
                                                "offer-tariffs__card-content-speed"
                                            }
                                        >
                                            {currentTariff.title}
                                        </span>
                                        <span
                                            className={
                                                tariff[tariff.length - 1] ===
                                                currentTariff
                                                    ? "range-wrapper offer-tariffs__range-wrapper"
                                                    : "offer-tariffs__range-wrapper"
                                            }
                                            onTouchStart={() =>
                                                (cardsListElement.current.style.overflow =
                                                    "hidden")
                                            }
                                            onTouchEnd={() =>
                                                (cardsListElement.current.style.overflow =
                                                    "auto")
                                            }
                                        >
                                            <span className="input-range-chep" />
                                            <input
                                                type="range"
                                                onChange={handleRange(
                                                    minValue,
                                                    maxValue
                                                )}
                                                max={maxValue}
                                                min={minValue}
                                                value={value}
                                                // className="offer-tariffs__card-content-range input-range"
                                                ref={inputRange}
                                                style={styleInputRange}
                                                className="inputRange"
                                                onTouchStart={() =>
                                                    (cardsListElement.current.style.overflow =
                                                        "hidden")
                                                }
                                                onTouchEnd={() =>
                                                    (cardsListElement.current.style.overflow =
                                                        "auto")
                                                }
                                            />
                                        </span>
                                        <p
                                            className="offer-tariffs__card-content-description"
                                            dangerouslySetInnerHTML={{
                                                __html: currentTariff.description,
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="offer-tariffs__card-content-item offer-tariffs__card-content-item--price">
                                    {!!currentTariff.can_prepay_discount &&
                                    !!currentTariff.price_old ? (
                                        <span
                                            className="offer-tariffs__card-old-price"
                                            onClick={(e) => e.stopPropagation()}
                                        >
                                            {currentTariff.price_old}{" "}
                                            <span
                                                className={
                                                    lang === "en"
                                                        ? "offer-tariffs__currency-price-old"
                                                        : ""
                                                }
                                            >
                                                {t("UAHmonth")}
                                            </span>
                                        </span>
                                    ) : null}
                                    <span
                                        className="offer-tariffs__card-content-price"
                                        onClick={(e) => e.stopPropagation()}
                                    >
                                        {currentTariff.price}{" "}
                                        <span
                                            className={
                                                lang === "en"
                                                    ? "offer-tariffs__currency"
                                                    : ""
                                            }
                                        >
                                            {t("UAHmonth")}
                                        </span>
                                    </span>
                                    <span onClick={(e) => e.stopPropagation()}>
                                        <OfferTariffButton
                                            handler={() => {
                                                handleAppareModal(
                                                    currentTariff.title,
                                                    currentTariff.id,
                                                    currentTariff.seo_id
                                                );
                                            }}
                                            titleButton={t("toPlugBtn")}
                                        />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <OfferBackCard
                        tariff={tariff}
                        tariffKey={tariffKey}
                        onClick={handleClick}
                        currentTariff={currentTariff}
                    />
                </ReactCardFlip>
            </div>
        </div>
    );
};

export default connect(null, { setModalAccept })(OfferTariffs);
