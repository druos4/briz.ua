import React from "react";
import { useTranslation } from "react-i18next";

const AdvantagesProviderItemWithoutLink = ({ elem, index }) => {
    const { t } = useTranslation();

    return (
        <li className="strong-side__item" key={index}>
            <span className={`strong-side__miniature ${elem.imgSelector}`} />
            <span className="strong-side__text">
                <span
                // href="#"
                // className="strong-side__link main-link"
                >
                    {t(elem.description.paragraphTextFirst)}{" "}
                </span>
                {t(elem.description.paragraphTextSecond)}
            </span>
        </li>
    );
};

export default AdvantagesProviderItemWithoutLink;
