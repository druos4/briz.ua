import React from "react";
import { useTranslation } from "react-i18next";

const AdvantagesProviderItemInsideLink = ({ currentLocaleHref, elem, index }) => {
    const { t } = useTranslation();

    return (
        <li className="strong-side__item" key={index}>
            <span className={`strong-side__miniature ${elem.imgSelector}`} />
            <span className="strong-side__text">
                {t(elem.description.paragraphTextFirst)}{" "}
                <a
                    href={`${currentLocaleHref}${elem.linkPath}`}
                    className="strong-side__link main-link"
                >
                    {t(elem.linkText)}
                </a>
                {" "}{t(elem.description.paragraphTextSecond)}
            </span>
        </li>
    );
};

export default AdvantagesProviderItemInsideLink;
