import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { linksForApps } from "@/utils";
import AdvantagesProviderItemWithoutLink from "./AdvantagesProviderItemWithoutLink";
import AdvantagesProviderItemLinkEnd from "./AdvantagesProviderItemLinkEnd";
import AdvantagesProviderItemLinkStart from "./AdvantagesProviderItemLinkStart";
import AdvantagesProviderItemTwoLink from "./AdvantagesProviderItemTwoLink";
import AdvantagesProviderItemInsideLink from "@/Shared/AdvantagesProvider/AdvantagesProviderItemInsideLink";

const AdvantagesProvider = ({ advantagesData, currentLocaleHref, currentLocale }) => {
    const { t } = useTranslation();

    const [langAppLinks, setLangAppLinks] = useState({ apple: "", google: "" });

    useEffect(() => {
        setLangAppLinks(linksForApps(app.getLang()));
    }, []);

    const renderComponent = (type, index, elem) => {
        switch (type) {
            case "withoutLink":
                return (
                    <AdvantagesProviderItemWithoutLink
                        key={index}
                        elem={elem}
                    />
                );
            case "linkEnd":
                return (
                    <AdvantagesProviderItemLinkEnd
                        key={index}
                        elem={elem}
                        currentLocaleHref={currentLocaleHref}
                    />
                );
            case "linkStart":
                return (
                    <AdvantagesProviderItemLinkStart
                        key={index}
                        elem={elem}
                        currentLocale={currentLocale}
                        currentLocaleHref={currentLocaleHref}
                    />
                );
            case "twoLink":
                return (
                    <AdvantagesProviderItemTwoLink
                        key={index}
                        elem={elem}
                        langAppLinks={langAppLinks}
                    />
                );
            case "insideLink":
                return (
                    <AdvantagesProviderItemInsideLink
                        key={index}
                        elem={elem}
                        currentLocaleHref={currentLocaleHref}
                    />
                );
            default:
                console.log("error, the required component was not found");
        }
        return <h1>ERROR</h1>;
    };
    return (
        <>
            <section className="section home__section">
                <div className="page-subtitle home__page-subtitle home__page-subtitle--modif">
                    {t(advantagesData.title)}
                </div>
                <ul className="home__strong-sides-list strong-side">
                    {advantagesData.items.map((elem, index) => {
                        return renderComponent(elem.type, index, elem);
                    })}
                </ul>
            </section>
        </>
    );
};

export default AdvantagesProvider;
