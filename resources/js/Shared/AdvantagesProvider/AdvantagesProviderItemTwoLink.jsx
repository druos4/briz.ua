import React from "react";
import {useTranslation} from "react-i18next";

const AdvantagesProviderItemTwoLink = ({langAppLinks, elem, index}) => {
    const {t} = useTranslation();

    return (
        <li className="strong-side__item" key={index}>
            <span className={`strong-side__miniature ${elem.imgSelector}`}/>
            <span className="strong-side__text">
                {t(elem.description.paragraphTextFirst)}{" "}
                <a
                    href={langAppLinks.apple}
                    target="_blank"
                    className="strong-side__link main-link"
                >
                    {elem.linkText.linkTextFirst}
                </a>{" "}
                {t(elem.description.paragraphTextSecond)}{" "}
                <a
                    href={langAppLinks.google}
                    target="_blank"
                    className="strong-side__link main-link"
                >
                    {elem.linkText.linkTextSecond}
                </a>
                : {t(elem.description.paragraphTextThird)}
            </span>
        </li>
    );
};

export default AdvantagesProviderItemTwoLink;
