import React from "react";
import { useTranslation } from "react-i18next";
import { InertiaLink } from "@inertiajs/inertia-react";

const AdvantagesProviderItemLinkStart = ({ elem, index, currentLocale, currentLocaleHref }) => {
    const { t } = useTranslation();

    return (
        <li className="strong-side__item" key={index}>
            <span className={`strong-side__miniature ${elem.imgSelector}`} />
            <span className="strong-side__text">
                {elem.isExternal ? (
                    <a
                        href={`${elem.linkPath}${currentLocale}`}
                        target="_blank"
                        className="strong-side__link main-link"
                        rel={elem.linkPath.includes("stat") ? "nofollow" : ''}
                    >
                        {t(elem.linkText)}
                    </a>
                ) : (
                    <a
                        href={`${currentLocaleHref}${elem.linkPath}`}
                        className="strong-side__link main-link"
                    >
                        {t(elem.linkText)}
                    </a>
                )}
                {elem.isColon ? ":" : ""}
                {" "}{t(elem.description.paragraphTextFirst)}
            </span>
        </li>
    );
};

export default AdvantagesProviderItemLinkStart;
