export const advantagesProviderData = {
    brizProviderComfort: {
        title: "notOnlyFastInternet",
        items: [
            {
                type: "linkEnd",
                imgSelector: "strong-side__miniature--pig",
                description: {
                    paragraphTextFirst: "DoNotGoIntoTheNegative",
                },
                linkPath: "/actions/kosmiceskie-skidki-na-god-do-20",
                linkText: "byPrepayment",
            },
            {
                type: "linkEnd",
                imgSelector: "strong-side__miniature--usb",
                description: {
                    paragraphTextFirst: "connectionFor",
                },
                linkPath: "/news/texnologiya-gpon-vysokoskorostnoi-internet-ot-briz",
                linkText: "gponTechnology",
            },
            {
                type: "insideLink",
                imgSelector: "strong-side__miniature--support",
                description: {
                    paragraphTextFirst: "friendly",
                    paragraphTextSecond: "24/7"
                },
                linkText: "supportLower",
                linkPath: "/faq/texniceskie-voprosy/yak-zvernutisya-do-operatora-pidtrimki"
            },
            {
                type: "linkEnd",
                imgSelector: "strong-side__miniature--go-on",
                description: {
                    paragraphTextFirst: "gottaLeave"
                },
                linkText: "suspensionOfServices",
                linkPath: "/faq/texniceskie-voprosy/shho-take-prizupinennya-poslug"
            },
            {
                type: "linkStart",
                imgSelector: "strong-side__miniature--bitcoin",
                isColon: true,
                description: {
                    paragraphTextFirst: "pickUseBonuses",
                },
                linkPath: "/actions/programma-bonus-briz",
                linkText: "programBonusBriz",
            },
            {
                type: "twoLink",
                imgSelector: "strong-side__miniature--apps",
                description: {
                    paragraphTextFirst: "applicationsFor",
                    paragraphTextSecond: "and",
                    paragraphTextThird: "payForServicesFromGadget",
                },
                linkText: {
                    linkTextFirst: "iOS",
                    linkTextSecond: "Android",
                },
            },
            {
                type: "linkStart",
                imgSelector: "strong-side__miniature--message",
                description: {
                    paragraphTextFirst: "accountStatus",
                },
                linkPath: "/faq/informirovanie",
                linkText: "informing"
            },
            {
                type: "linkEnd",
                imgSelector: "strong-side__miniature--plus7",
                description: {
                    paragraphTextFirst: "didNotPayOnTime",
                },
                linkPath:
                    "/actions/knopka-7-dnei-sutki-interneta-na-skorosti-tarifa",
                linkText: "program7days",
            },
        ],
    },
    advantagesIptv: {
        title: "advantagesOfIPTV",
        items: [
            {
                type: "withoutLink",
                imgSelector: "strong-side__miniature--iptv-console",
                description: {
                    paragraphTextFirst: "IPTVtelevisionOverInternet",
                },
            },
            {
                type: "withoutLink",
                imgSelector: "strong-side__miniature--five-devices",
                description: {
                    paragraphTextFirst: "IPTVcanWatchedFiveDevices",
                },
            },
            {
                type: "withoutLink",
                imgSelector: "strong-side__miniature--catalog-channels",
                description: {
                    paragraphTextFirst: "convenientChannelCatalogOnline",
                },
            },
            {
                type: "withoutLink",
                imgSelector: "strong-side__miniature--more-than-ctv",
                description: {
                    paragraphTextFirst: "largeSelectionHDchannels",
                },
            },
        ],
    },
    reliableAndProfitablePartner: {
        title: "reliableProfitablePartner",
        items: [
            {
                type: "withoutLink",
                imgSelector: "strong-side__miniature--fop",
                description: {
                    paragraphTextFirst: "typeBusiness",
                    paragraphTextSecond: "workAllTypesLegal",
                },
            },
            {
                type: "withoutLink",
                imgSelector: "strong-side__miniature--speed-is-actual",
                description: {
                    paragraphTextFirst: "speedCorrespondsDeclared2",
                },
            },
            {
                type: "withoutLink",
                imgSelector: "strong-side__miniature--e-document",
                description: {
                    paragraphTextFirst: "useElectronicDocManagement",
                },
            },
            {
                type: "withoutLink",
                imgSelector: "strong-side__miniature--prepay-for-business",
                description: {
                    paragraphTextFirst: "youNotGoIntoNegative",
                },
            },
            {
                type: "withoutLink",
                imgSelector: "strong-side__miniature--manager-for-business",
                description: {
                    paragraphTextFirst: "quickSolutionAccounting",
                },
            },
            {
                type: "withoutLink",
                imgSelector: "strong-side__miniature--our-cable",
                description: {
                    paragraphTextFirst: "roomConnectedCable",
                },
            },
        ],
    },
};
