import { SET_CURRENT_PAGE } from "../actions/types";

const initialState = {
    currentPage: "",
};

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case SET_CURRENT_PAGE:
            return {
                ...state,
                currentPage: payload,
            };

        default:
            return state;
    }
};
