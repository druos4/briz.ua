import { LOADED_MENU } from "../actions/types";

const initialState = {
    loaded: false,
};

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case LOADED_MENU:
            return {
                ...state,
                loaded: payload,
            };
        default:
            return state;
    }
};
