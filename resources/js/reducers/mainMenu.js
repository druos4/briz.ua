import { GET_MENU } from "../actions/types";

const initialState = {
    menu: {},
    loading: true,
    error: {},
};

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case GET_MENU:
            return {
                ...state,
                menu: payload,
                loading: false,
            };

        default:
            return state;
    }
};
