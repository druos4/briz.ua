import { SET_SHOP_PRODUCT } from '../actions/types';
import Product from '../entities/Product';

const initialState = {
    product: {},
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_SHOP_PRODUCT:
            return {
                product: new Product(action.payload)
            }
        default:
            return state;
    }
};