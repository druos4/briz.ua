import { combineReducers } from "redux";
import mainMenu from "./mainMenu";
import candidacyBuyProduct from "./candidacyBuyProduct";
import preload from "./preload";
import stickyHeader from "./stickyHeader";
import currentPage from "./currentPage";
import partLinkFaq from "./partLinkFaq";
import shopCatalog from "./shopCatalog";
import modalAccept from "./modalAccept";
import shopProduct from "./shopProduct";
import blogStuff from "./blogStuff";

export default combineReducers({
    mainMenu,
    preload,
    candidacyBuyProduct,
    stickyHeader,
    currentPage,
    partLinkFaq,
    shopCatalog,
    modalAccept,
    shopProduct,
    blogStuff,
});
