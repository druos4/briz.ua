import { SET_MODAL_ACCEPT, CLEAR_MODAL_ACCEPT } from "../actions/types";

const initialState = {
    showModalAccept: false,
    typeModalAccept: "",
    productTitle: "",
    productId: null,
    titleModal: "",
    titleButton: "",
    typeProduct: "",
    apiUrl: "",
    titleProduct: "",
    seoId: "",
    body: {},
    currentImgLink: "",
    keyFormRedux: "",
    commentId: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_MODAL_ACCEPT:
            return {
                ...state,
                ...action.payload,
            };
        case CLEAR_MODAL_ACCEPT:
            return {
                ...initialState,
            };
        default:
            return state;
    }
};
