import { SET_PART_LINK_FAQ } from "../actions/types";

const initialState = {
    partLink: "",
};

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case SET_PART_LINK_FAQ:
            return {
                ...state,
                partLink: payload,
            };

        default:
            return state;
    }
};
