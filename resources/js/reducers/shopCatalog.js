import {
    SET_TAB_AND_FILTERS_SHOP,
    SET_TAB_SHOP,
    SET_TAB_SHOP_FILTER_URL,
    SET_TAB_AND_SORT_SHOP,
} from "../actions/types";

const initialState = {
    all: {},
    "iptv-pristavka": {},
    mikrotik: {},
    "besprovodnye-tocki-dostupa": {},
    drugoe: {},
    kommutatori: {},
    routery: {},
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_TAB_AND_FILTERS_SHOP:
            return {
                ...state,
                [`${action.payload.slug}`]: {
                    [`${action.payload.slug}`]: action.payload.body,
                },
            };
        // if (filterBodyRequest1.hasOwnProperty(slugCat)) {
        //     Reflect.deleteProperty(filterBodyRequest1, slugCat);
        //     body = filterBodyRequest1;
        // } else {
        //     body = {...filterBodyRequest1, [`${slugCat}`]: code};
        // }

        // if (state[`${action.payload.slugTab}`].hasOwnProperty(action.payload.slugCat)) {
        // Reflect.deleteProperty(state[`${action.payload.slugTab}`], action.payload.slugCat);

        // return state;

        // } else {
        // return {
        //     ...state,
        //     [`${action.payload.slugTab}`]: {
        //         ...state[`${action.payload.slugTab}`], [`${action.payload.slugCat}`]: action.payload.code
        //     }
        // };
        // }
        // RABOCHIY VARIANT
        // return {
        //     ...state,
        //         [`${action.payload.slugTab}`]: {
        //             ...state[`${action.payload.slugTab}`], [`${action.payload.slugCat}`] : action.payload.code
        //         }
        //     };

        case SET_TAB_AND_SORT_SHOP:
            return {
                ...state,
                [`${action.payload.slugTab}`]: {
                    ...state[`${action.payload.slugTab}`],
                    [`${action.payload.sort}`]: action.payload.code,
                },
            };

        case SET_TAB_SHOP:
            return {};

        case SET_TAB_SHOP_FILTER_URL:
            return {};

        default:
            return state;
    }
};
