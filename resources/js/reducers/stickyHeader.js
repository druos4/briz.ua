import { SET_STICKY_HEADER } from "../actions/types";

const initialState = {
    stickyHeader: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        // console.log(action, 'ACTION FROM REDUCER');
        case SET_STICKY_HEADER:
            return {
                stickyHeader: action.payload,
            };

        default:
            return state;
    }
};
