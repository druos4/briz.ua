import {
    PENDING_API_CANDIDACY_BUY_PRODUCT,
    SUCCESS_API_CANDIDACY_BUY_PRODUCT,
    FAILURE_API_CANDIDACY_BUY_PRODUCT,
    CLEAR_MODAL_CONTENT,
    CLEAR_API_CANDIDACY_BUY_PRODUCT,
} from "../actions/types";

const initialState = {
    contacts: {
        loading: false,
        error: false,
        errorPhone: "",
        isInited: false,
        errorMessage: "",
        errorSecondSend: false,
    },
    default: {
        loading: false,
        error: false,
        errorPhone: "",
        isInited: false,
        errorMessage: "",
        errorSecondSend: false,
    },
    callMe: {
        loading: false,
        error: false,
        errorPhone: "",
        isInited: false,
        errorMessage: "",
        errorSecondSend: false,
    },
    comment: {
        loading: false,
        error: false,
        errorPhone: "",
        isInited: false,
        errorMessage: "",
        errorSecondSend: {},
    },
};

export default (state = initialState, action) => {
    switch (action.type) {
        case PENDING_API_CANDIDACY_BUY_PRODUCT:
            return {
                ...state,
                ...action.payload,
            };
        case SUCCESS_API_CANDIDACY_BUY_PRODUCT:
            return {
                ...state,
                ...action.payload,
            };
        case FAILURE_API_CANDIDACY_BUY_PRODUCT:
            return {
                ...state,
                ...action.payload,
            };
        case CLEAR_API_CANDIDACY_BUY_PRODUCT:
            return {
                ...initialState,
            };

        case CLEAR_MODAL_CONTENT:
            return {
                ...state,
                errorMessage: action.payload,
            };
        default:
            return state;
    }
};
