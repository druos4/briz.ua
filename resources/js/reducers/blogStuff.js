import { SET_LIKE_BLOG_ARTICLE } from "../actions/types";

const initialState = {
    like: 0,
};

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case SET_LIKE_BLOG_ARTICLE:
            return {
                ...state,
                like: payload,
            };

        default:
            return state;
    }
};
