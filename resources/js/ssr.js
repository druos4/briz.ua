import React from 'react'
import express from 'express'
import ReactDOMServer from 'react-dom/server'
import { createInertiaApp } from '@inertiajs/inertia-react'
const result = require('dotenv').config();

const server = express()
server.use(express.json({limit: '10mb', extended: true}))
server.use(express.urlencoded({limit: '10mb', extended: true}))
server.post('/render', async (request, response, next) => {
  try {
    response.json(
      await createInertiaApp({
        page: request.body,
        render: ReactDOMServer.renderToString,
        resolve: name => require(`./Pages/${name}`),
        setup: ({ App, props }) => <App {...props} />,
      })
    )
  } catch (error) {
    next(error)
  }
})
server.listen(result.parsed.SSR_PORT, () => console.log(`Server started on port ${result.parsed.SSR_PORT}.`))

console.log(`Starting SSR server on port ${result.parsed.SSR_PORT}...`)