class Product {
    title;
    id;
    slug;
    price;
    price_old;
    can_buy;
    has_discount;
    promo;
    new;
    quantity;
    url;
    picture;
    anons;
    rating;
    section_description;

    constructor(params) {
        Object.keys(params).forEach(param => {
            if (this.hasOwnProperty(param)) {
                this[param] = params[param];
            }
        })
    }

    getTitle() {
        return this.title;
    }

    getId() {
        return this.id;
    }

    getSlug() {
        return this.slug;
    }

    getPrice() {
        return this.price;
    }

    getOldPrice() {
        return this.price_old;
    }

    isCanBuy() {
        return this.can_buy;
    }

    isHasDiscount() {
        return this.has_discount;
    }

    isPromo() {
        return this.promo;
    }

    isNew() {
        return this.new;
    }

    getQuantity() {
        return this.quantity;
    }

    getUrl() {
        return this.url;
    }

    getPicture() {
        return this.picture;
    }

    getAnons() {
        return this.anons;
    }

    getRating() {
        return this.rating;
    }

    getSectionDescription() {
        return this.section_description;
    }
}

export default Product;