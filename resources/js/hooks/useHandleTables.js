import { useState, useEffect } from "react";

export const useHandleTables = (wrapperElement) => {

    useEffect(() => {
        const table = wrapperElement.current.querySelectorAll("table");

        if (table.length) {
            table.forEach(function (item) {
                const tbody = item.querySelector('tbody');
                const firstTr = tbody.children[0];
                const dataArr = [];
                const tableWidth = item.getBoundingClientRect().width;
                
                for (let item of firstTr.children) {
                    dataArr.push(item.textContent);
                }

                for (let trItem of tbody.children) {
                    for (let i = 0; i < trItem.children.length; i++) {
                        trItem.children[i].setAttribute('data-label', dataArr[i]);
                    }
                }

                const div = document.createElement("div");
                div.classList.add("actions__wrapper-table");
                wrapperElement.current.insertBefore(div, item);
                div.insertBefore(item, null);

                const widthWrapperElement = div.getBoundingClientRect().width;
                
                if (window.screen.availWidth > 1024) {
                    if (tableWidth > widthWrapperElement) {
                        div.addEventListener('mousewheel', keepTrackWhellEvent, false);
                    }
                }

                if (tableWidth < widthWrapperElement) {
                    item.classList.add('table-less-then-content');
                }
                
            });
        }

    }, []);

    const keepTrackWhellEvent = function(e) {
        this.scrollLeft -= (e.wheelDelta);
        e.preventDefault();
    };
};