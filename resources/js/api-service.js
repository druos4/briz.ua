/* eslint-disable no-return-await */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */
import axios from "axios";
import {
    SUCCESS,
    LOCKED,
    UNPROCESSABLE_ENTITY,
} from "./constants/status-code-constants";

axios.defaults.validateStatus = (status) => {
    const isValid = status === SUCCESS;

    if (!isValid) {
        console.warn(`Could not fetch API response! Status code is ${status}`);
    }

    return isValid;
};

const fetchApi = (
    url,
    method = "get",
    constant,
    data,
    keyFormRedux,
    typeProduct
) => {
    return async (dispatch) => {
        dispatch({
            type: `PENDING_${constant}`,
            payload: {
                [keyFormRedux]: {
                    loading: true,
                    error: false,
                    isInited: false,
                },
            },
        });

        let sendData;
        if (data) {
            sendData = new FormData();

            for (const key in data) {
                sendData.append(key, data[key]);
            }
            console.warn("request with data");
        } else {
            sendData = { ...data };
            console.warn("request with empty data");
        }

        return await axios({
            method: method.toLowerCase(),
            url: `${url}`,
            data: sendData,
        })
            .then((response) => {
                // set cookie 300s check second send
                document.cookie = `${encodeURIComponent(
                    typeProduct
                )}=${encodeURIComponent("orderConnectSend")}; max-age=300`;
                dispatch({
                    type: `SUCCESS_${constant}`,
                    payload: {
                        [keyFormRedux]: {
                            phone: "",
                            username: "",
                            loading: false,
                            error: false,
                            isInited: true,
                            errorMessage: response,
                            errorSecondSend: response?.data?.errors,
                        },
                    },
                });
            })
            .catch((err) => {
                console.log(err, "ERR");
                if (err.response.status === LOCKED) {
                    dispatch({
                        type: `FAILURE_${constant}_LOCKED`,
                        payload: { [keyFormRedux]: err.response.data },
                    });
                } else if (err.response.status === UNPROCESSABLE_ENTITY) {
                    dispatch({
                        type: `FAILURE_${constant}_UNPROCESSABLE_ENTITY`,
                        payload: { [keyFormRedux]: err.response.data },
                    });
                } else {
                    dispatch({
                        type: `FAILURE_${constant}`,
                        payload: {
                            [keyFormRedux]: {
                                loading: false,
                                error: true,
                                token: "",
                                phone: "",
                                isInited: false,
                            },
                        },
                    });
                }

                console.warn(err, `IN ${constant}`);
            });
    };
};

export default fetchApi;
