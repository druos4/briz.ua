<?php

use Illuminate\Support\Facades\Route;

Route::post('/make-slug','Controller@makeSlug');
Route::post('/ckeditor/image_upload', 'UploaderController@upload')->name('upload');

Route::get('/reload-captcha', 'CaptchaServiceController@reloadCaptcha');
Route::post('captcha-validation', 'CaptchaServiceController@capthcaFormValidate');


Route::group(
    [
        'as' => 'client.',
        'namespace' => 'Client',
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeViewPath' ]
    ], function() {

    Route::get('/lang/{lang}','LangController@switchLang')->name('langs');
    Route::post('/get-header-menu','MainController@getHeaderMenu');
    Route::get('/get-header-menu','MainController@getHeaderMenu');

    Route::get('/faq/search','FaqController@faqSearch');
    Route::get('/actions/get-more-item','ActionsController@getMoreItems');
    Route::get('/check-for-remont','NewsController@checkForRemont');
    Route::get('/news/get-more-item','NewsController@getMoreItems');

    Route::group(['prefix' => 'map'], function () {
        Route::get('/markers', 'MapController@loadMarkers');
        Route::get('/streets', 'MapController@getStreet');
        Route::get('/houses', 'MapController@getHouses');
        Route::get('/address', 'MapController@getAddress');
        Route::get('/marker', 'MapController@getMarker');
        Route::get('/find-by-address', 'MapController@findByAddress');
    });


    Route::post('/feedback','FeedbackController@feedback')->middleware('throttle:6,10,site');
    Route::post('/call-me','FeedbackController@callMe')->middleware('throttle:6,10,site');
    Route::post('/order-service','FeedbackController@orderService')->middleware('throttle:6,10,site');
    Route::post('/order-product','FeedbackController@orderDevice')->middleware('throttle:6,10,site');
    Route::post('/map-connect','FeedbackController@mapConnect')->middleware('throttle:6,10,site');
    Route::post('/cable-rent','FeedbackController@cableRent')->middleware('throttle:6,10,site');
    Route::post('/connect-action','FeedbackController@connectAction')->middleware('throttle:6,10,site');

    Route::post('/get-header-menu','MainController@getHeaderMenu');
    Route::get('/get-header-menu','MainController@getHeaderMenu');
    Route::post('/get-shop-menu','MainController@getShopMenu');
    Route::post('/shop/product/comments/add','ProductCommentsController@createComment');
    Route::post('/shop/product/comments/get','ProductCommentsController@getComments');

    Route::post('/emails','EmailsController@addEmail')->middleware('throttle:1,1,emails');
    Route::post('/blog/like','BlogController@setLike');
    Route::post('/blog/comment','BlogController@storeComment')->middleware('throttle:1,1,blog');
});


Route::group(
    [
        'as' => 'client.',
        'namespace' => 'Client',
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeViewPath','localizationRedirect' ]
    ], function() {

    Route::get('/','MainController@index')->name('index');
    Route::get('/500','MainController@error500')->name('error500');
    Route::get('/ie-browser','MainController@ieBrowser')->name('ie');
    Route::get('/sitemap','MainController@sitemap')->name('sitemap');

    Route::get('/politika-konfidentsialnosti-briztv','MainController@politicaBriztv')->name('politicaBriztv');
    Route::get('/usloviya-i-pravila','MainController@usloviyaIPravila')->name('usloviya-i-pravila');

    Route::get('/contacts','MainController@contacts')->name('contacts');
    Route::get('/payment','MainController@payment')->name('payment');
    Route::get('/documents','MainController@documents')->name('documents');

    Route::get('/blog/comments','BlogController@getComments')->name('blog.comments');
    Route::get('/blog/article/{slug}','BlogController@article')->name('blog.article');
    Route::get('/blog/category/{slug}','BlogController@category')->name('blog.category');
    Route::get('/blog/category/{slug}/{params}','BlogController@category')->name('blog.category');
    Route::get('/blog/{params}','BlogController@index')->name('blog');
    Route::get('/blog','BlogController@index')->name('blog');

    Route::get('/ott','MainController@ott')->name('ott');

    Route::get('/faq','FaqController@index')->name('faq');
    Route::get('/faq/search','FaqController@faqSearchFront')->name('faq.search');
    Route::get('/faq/{slug}','FaqController@groups')->name('faq.groups');
    Route::get('/faq/{slug}/{itemslug}','FaqController@items')->name('faq.group.items');

    Route::get('/help','HelpController@index')->name('help');
    Route::get('/help/{directory}','HelpController@directory')->name('help.directory');

/*
    Route::get('/help/subdir','HelpController@subdir')->name('subdir');
    Route::get('/help/dirarticles/lines','HelpController@dirarticlesLines')->name('dirarticlesLines');
    Route::get('/help/dirarticles/cubes','HelpController@dirarticlesCubes')->name('dirarticlesCubes');
    Route::get('/help/article','HelpController@article')->name('article');
    Route::get('/help/{directory}/{article}','HelpController@articleDetail')->name('help.article');
*/





    Route::get('/channels/{id}','MainController@channels')->name('channels');

    Route::get('/internet-dlya-biznesa','TariffsController@inetBusiness')->name('internet-dlya-biznesa');
    Route::get('/televidenie','TariffsController@tv')->name('televidenie');
    Route::get('/domashniy-internet','TariffsController@inetHome')->name('domashniy-internet');
    Route::get('/televidenie-i-internet','TariffsController@tvInet')->name('televidenie-i-internet');

    Route::get('/actions','ActionsController@index')->name('actions.index');
    Route::get('/actions/{slug}','ActionsController@detail')->name('actions.detail');


    Route::get('/news','NewsController@index')->name('news.index');

    Route::get('/tech','NewsController@remont')->name('news.tech');
    Route::get('/news/remont','NewsController@remont')->name('news.remont');
    Route::get('/news/{slug}','NewsController@detail')->name('news.detail');

    Route::get('/map','MapController@index')->name('map');



    Route::get('/spisok-kanalov-paket-bazovyiy-iptv','ChannelsController@channel41')->name('spisok-kanalov-paket-bazovyiy-iptv');
    Route::get('/spisok-kanalov-paket-rasshirennyiy-iptv','ChannelsController@channel42')->name('spisok-kanalov-paket-rasshirennyiy-iptv');
    Route::get('/spisok-kanalov-paket-bazovyiy','ChannelsController@channel38')->name('spisok-kanalov-paket-bazovyiy');
    Route::get('/spisok-kanalov-paket-premium','ChannelsController@channel39')->name('spisok-kanalov-paket-premium');


    Route::get('/slider-test/{id}','MainController@sliderTest')->name('slider-test');

    Route::get('/equipment','ShopController@index')->name('equipment');
    Route::get('/equipment/top','ShopController@categoryTop')->name('equipment.category.top');
    Route::get('/equipment/{category_slug}','ShopController@category')->name('equipment.category');

    /*
    Route::get('/shop','ShopController@index')->name('shop.index');
    Route::get('/shop/{category_slug}','ShopController@category')->name('shop.category');
    */
    Route::get('/equipment/product/{product_slug}','ShopController@product')->name('equipment.product');



});

Auth::routes(['register' => false]);
Route::post('/map/get-own-markers','Controller@getOwnMarkers');
/*
Route::get('/map','Controller@map')->name('map');
Route::get('/map-google','Controller@mapGoogle')->name('map-google');
Route::get('/map-leaf','Controller@mapLeaf')->name('map-leaf');
Route::post('/map/get-markers','Controller@getMarkers');

Route::post('/map/get-street','Controller@getStreet');
*/
/*


Route::get('/export','Controller@export')->name('export');
Route::get('/map','Controller@map')->name('map');
Route::get('/map-google','Controller@mapGoogle')->name('map-google');
Route::get('/map-leaf','Controller@mapLeaf')->name('map-leaf');
Route::post('/map/get-markers','Controller@getMarkers');
Route::post('/map/get-own-markers','Controller@getOwnMarkers');
Route::post('/map/get-street','Controller@getStreet');
*/
/*
Route::group(
    [
        'as' => 'client.',
        'namespace' => 'Client',
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){


    Route::get('/','MainController@index')->name('index');


    Route::get('/news','NewsController@index')->name('news.index');
    Route::get('/news/{slug}','NewsController@detail')->name('news.detail');
    Route::get('/news/tag/{code}','NewsController@tags')->name('news.tags');

    Route::get('/shop','ShopController@index')->name('shop.index');
    Route::get('/shop/{category_slug}','ShopController@category')->name('shop.category');
    Route::get('/shop/{category_slug}/{product_slug}','ShopController@product')->name('shop.product');

    Route::post('/shop/filter','ShopController@filter');

    Route::get('/test','Controller@test');


    Route::group(['prefix' => 'cart', 'as' => 'cart.'], function () {

        Route::post('/get-small-cart','CartController@getSmallCart');
        Route::post('/getCount','CartController@getCartCount');
        Route::post('/add','CartController@add');
        Route::post('/update','CartController@update');
        Route::post('/delete','CartController@delete');
        Route::get('/','CartController@index')->name('index');

    });

    Route::get('/order','OrderController@make')->name('order.make');
    Route::post('/order','OrderController@createOrder');
    Route::get('/order/{id}','OrderController@done')->name('order.done');




    Route::group(['prefix' => 'cabinet', 'as' => 'cabinet.', 'middleware' => ['auth']], function () {
            Route::get('/','CabinetController@index')->name('index');
            Route::get('/profile','CabinetController@profile')->name('profile');
            Route::post('/profile','CabinetController@profileSave');
            Route::get('/orders','CabinetController@orders')->name('orders');
            Route::get('/orders/{id}','CabinetController@orderDetail')->name('orders.detail');
    });

});
*/
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth','can:admin_access']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/clear-cache', 'HomeController@clearCache')->name('resetcache');

    Route::get('/sitemap', 'HomeController@sitemap')->name('sitemap');

    Route::get('/deleted-urls', 'DeletedUrlsController@index')->name('deleted-urls');

    Route::get('/filemanager', 'HomeController@filemanager')->name('filemanager');
    Route::get('/imgsitemap', 'HomeController@imgSitemap')->name('imgsitemap');

    Route::get('/emails', 'EmailsController@index')->name('emails.index');

    Route::get('/genres','GenresController@index')->name('genres.index');
    Route::get('/genres/{id}/edit','GenresController@edit')->name('genres.edit');
    Route::put('/genres/{id}/update','GenresController@update')->name('genres.update');

    Route::get('/channels','ChannelsController@index')->name('channels.index');
    Route::post('/channels/get-genres','ChannelsController@getGenres');
    Route::post('/channels/update','ChannelsController@update');
    Route::get('/channels/sync','ChannelsController@sync')->name('channels.sync');
    Route::get('/channels/clear','ChannelsController@clear')->name('channels.clear');


    Route::get('tariffs/{id}/history','TariffsController@history')->name('tariffs.history');
    Route::get('/tariffs','TariffsController@index')->name('tariffs.index');
    Route::get('/tariffs/logs','TariffsController@logs')->name('tariffs.logs');
    Route::get('/tariffs-test','TariffsController@test')->name('tariffs.test');
    Route::get('/tariffs/sync','TariffsController@sync')->name('tariffs.sync');
    Route::get('/tariffs/{id}/edit','TariffsController@edit')->name('tariffs.edit');
    Route::put('/tariffs/{id}/update','TariffsController@update')->name('tariffs.update');
    Route::get('/tariffs/{id}/copy','TariffsController@copy')->name('tariffs.copy');
    Route::post('/tariffs/{id}/copy','TariffsController@copyTariff');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');
    Route::post('products-delete','ProductsController@delete');
    Route::post('product-more-categories-set','ProductsController@moreCategoriesSet');
    Route::post('product-picture-upload','ProductsController@pictureUpload');
    Route::put('product-gallery-upload/{id}','ProductsController@galleryUpload');
    Route::post('product-gallery/{id}/delete/{row}','ProductsController@galleryDelete');
    Route::post('product-img360/delete','ProductsController@img360Delete');

    Route::put('product-gallery-upload/{id}/360','ProductsController@galleryUpload360');

    Route::post('product-documents-upload/{id}','ProductsController@documentUpload');
    Route::post('product-documents-delete','ProductsController@documentDelete');

    Route::post('product-video/{id}/add','ProductsController@videoAdd');
    Route::post('product-video/{id}/delete','ProductsController@videoDelete');
    Route::get('products/sync','ProductsController@syncWithBilling')->name('products.sync');
    Route::resource('products', 'ProductsController');
    Route::post('products/getByCatId','ProductsController@getByCatId');
    Route::post('products/get-buy-with','ProductsController@getBuyWith');
    Route::post('products/add-to-buy-with','ProductsController@addToBuyWith');
    Route::post('products/del-from-buy-with','ProductsController@delFromBuyWith');

    Route::get('products-comments','ProductsCommentsController@index')->name('products.comments');
    Route::get('products-comments/{id}/edit','ProductsCommentsController@edit')->name('products.comments.edit');
    Route::post('products-comments/{id}/edit','ProductsCommentsController@save');
    Route::get('products-comments/{id}/delete','ProductsCommentsController@delete');

    Route::post('products-comments/emails/add','ProductsCommentsController@emailAdd');
    Route::get('products-comments/emails/{id}/delete','ProductsCommentsController@emailDelete')->name('products.comments.email-delete');


    Route::resource('services', 'ServicesController');
    Route::post('services/get-prods','ServicesController@getProds');
    Route::post('services/add-prod','ServicesController@addProd');
    Route::post('services/remove-prod','ServicesController@removeProd');
    Route::post('services/products-save-sort','ServicesController@productsSaveSort');
    Route::get('services-test/{code}','ServicesController@testService')->name('services.test');

    Route::get('news/{id}/history','NewsController@history')->name('news.history');
    Route::post('news/slug','NewsController@slug');
    Route::resource('news', 'NewsController');

    Route::get('actions/{id}/history','ActionsController@history')->name('actions.history');
    Route::post('actions/slug','ActionsController@slug');
    Route::resource('actions', 'ActionsController');

    Route::get('payments/{id}/history','PaymentsController@history')->name('payments.history');
    Route::resource('payments', 'PaymentsController');

    Route::get('tags/{id}/history','TagsController@history')->name('tags.history');
    Route::resource('tags', 'TagsController');

    Route::get('pages/{id}/history','PagesController@history')->name('pages.history');
    Route::get('pages/clear-deleted','PagesController@clearDeleted')->name('pages.clearDeleted');
    Route::resource('pages', 'PagesController');

    Route::get('sliders/{id}/history','SlidersController@history')->name('sliders.history');
    Route::resource('sliders', 'SlidersController');

    Route::get('maps','MapsController@index')->name('maps.index');
    Route::get('maps/sync','MapsController@sync')->name('maps.sync');
    Route::get('maps/add','MapsController@create')->name('maps.add');
    Route::post('maps/get-by-id','MapsController@getMarkerById');

    Route::get('menu','MenuController@index')->name('menu');
    Route::get('menu/create','MenuController@create')->name('create');
    Route::get('menu/{id}/edit','MenuController@edit')->name('menu-edit');
    Route::get('menu/show','MenuController@show')->name('menu-show');
    Route::get('menu/{id}/generate','MenuController@generate')->name('menu-generate');
    Route::post('menu/{id}/add-item','MenuController@addItem');
    Route::post('menu/{id}/items-save-sort','MenuController@itemsSaveSort');
    Route::post('menu/{id}/del-item','MenuController@delItem');
    Route::post('menu/{id}/get-item','MenuController@getItem');

    Route::post('property-value-save','PropertiesController@propertyValueSave');
    Route::post('properties-add-value','PropertiesController@propertyAddValue');
    Route::post('properties-delete','PropertiesController@delete');
    Route::post('properties-delete-value','PropertiesController@propertyDelValue');
    Route::resource('properties', 'PropertiesController');

    Route::post('categories-update-active','CategoryController@updateActive');
    Route::post('categories-save-sort','CategoryController@saveSort');
    Route::post('categories-delete','CategoryController@delete');
    Route::delete('categories/destroy', 'CategoryController@destroy')->name('categories.destroy');
    Route::resource('categories', 'CategoryController');
    Route::post('categories-add-prop','CategoryController@addProperty');
    Route::post('categories-del-prop','CategoryController@removeProperty');

    Route::post('payment-methods-delete','PaymentMethodsController@delete');
    Route::resource('payment-methods', 'PaymentMethodsController');

    Route::post('shipment-methods-delete','ShipmentMethodsController@delete');
    Route::resource('shipment-methods', 'ShipmentMethodsController');

    Route::get('orders','OrdersController@index')->name('orders');
    Route::get('orders/{id}','OrdersController@detail')->name('orders-detail');
    Route::get('orders/{id}/edit','OrdersController@edit')->name('orders-edit');
    Route::post('orders/{id}/save-status','OrdersController@statusSave');
    Route::post('orders/{id}/save','OrdersController@orderSave');
    Route::get('orders/{id}/print','OrdersController@printOrder')->name('orders-print');

    Route::get('order-status','OrdersController@statuses')->name('order-status');
    Route::post('order-status/save','OrdersController@statusesSave');
    Route::post('order-add-product','OrdersController@addProductToOrder');
    Route::post('order-delete-product','OrdersController@deleteProductInOrder');
    Route::post('order-update-product','OrdersController@updateProductInOrder');

    Route::get('faqs','FaqsController@index')->name('faqs.index');
    Route::get('faqs/create','FaqsController@groupCreate')->name('faqs.group.create');
    Route::post('faqs/store','FaqsController@groupStore');
    Route::get('faqs/{id}/edit','FaqsController@groupEdit')->name('faqs.group.edit');
    Route::put('faqs/{id}/update','FaqsController@groupUpdate');
    Route::delete('faqs/{id}','FaqsController@groupDelete');
    Route::get('faqs/{id}/elements','FaqsController@groupElements')->name('faqs.elements');
    Route::get('faqs/{id}/elements/create','FaqsController@elementCreate')->name('faqs.elements.create');
    Route::post('faqs/{id}/elements/store','FaqsController@elementStore');
    Route::get('faqs/{id}/elements/{element_id}/edit','FaqsController@elementEdit')->name('faqs.elements.edit');
    Route::put('faqs/{id}/elements/{element_id}/update','FaqsController@elementUpdate');
    Route::delete('faqs/{id}/elements/{element_id}','FaqsController@elementDelete');

    Route::get('faqs-test','FaqsController@test');

    Route::get('recommend-products','RecommendProductsController@index')->name('recommend-products');
    Route::post('recommend-products','RecommendProductsController@update');

    Route::resource('documents', 'DocumentsController');
    Route::resource('prices', 'PricesController');
    Route::resource('rules', 'RulesController');
    Route::resource('docactions', 'DocsActionsController');
    Route::resource('sertificats', 'SertificatsController');
    Route::resource('qualities', 'QualitiesController');

    Route::get('help','HelpController@index')->name('help.index');
    Route::get('help/{id}/children','HelpController@index')->name('help.children');
    Route::get('help/create','HelpController@directoryCreate')->name('help.directory.create');
    Route::post('help/store','HelpController@directoryStore')->name('help.directory.store');
    Route::get('help/{id}/edit','HelpController@directoryEdit')->name('help.directory.edit');
    Route::put('help/{id}/update','HelpController@directoryUpdate')->name('help.directory.update');
    Route::delete('help/{id}','HelpController@directoryDestroy')->name('help.directory.destroy');

    Route::get('blogs/comments','BlogCommentsController@index')->name('blog.comments.index');
    Route::get('blogs/comments/{id}','BlogCommentsController@edit')->name('blog.comments.edit');
    Route::post('blogs/comments/{id}','BlogCommentsController@update');
    Route::post('blogs/comments/{id}/moderate','BlogCommentsController@moderate');
    Route::delete('blogs/comments/{id}','BlogCommentsController@destroy')->name('blog.comments.destroy');

    Route::post('blogs/slug','BlogsController@slug');
    Route::resource('blogs', 'BlogsController');

    Route::post('blogcategories/slug','BlogCategoriesController@slug');
    Route::resource('blogcategories', 'BlogCategoriesController');

    /*
    Route::get('help/{id}/articles','HelpController@articles')->name('help.articles');
    Route::get('help/{id}/articles/create','HelpController@articleCreate')->name('help.articles.create');
    Route::post('help/{id}/articles/store','HelpController@articleStore')->name('help.articles.store');
    Route::get('help/{id}/articles/{article_id}/edit','HelpController@articleEdit')->name('help.articles.edit');
    Route::put('help/{id}/articles/{article_id}/update','HelpController@articleUpdate')->name('help.articles.update');
    Route::delete('help/{id}/article/{article_id}','HelpController@articleDestroy')->name('help.articles.destroy');
    */
});

