const mix = require("laravel-mix");
const path = require("path");

// THIS IS A TEMPORARY SOLUTION.
const { hmrOptions, devServer } = require("./webpack.fix");
require("laravel-mix-merge-manifest");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.extract();

mix.js(["resources/js/app.js"], "public/js")
    .react()
    .sass("resources/assets/sass/app.scss", "public/css")
    .options({
        hmrOptions: hmrOptions,
    })
    .webpackConfig({
        output: { chunkFilename: "js/[name].js?id=[chunkhash]" },
        module: {
            rules: [
                {
                    test: /\.m?js$/,
                    //exclude: /(node_modules|bower_components)/,
                    exclude: /node_modules\/(?!@react-leaflet)/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: [
                                [
                                    "@babel/preset-env",
                                    {
                                        targets: {
                                            chrome: "58",
                                            ie: "11",
                                        },
                                    },
                                ],
                                "@babel/preset-react",
                            ],
                            plugins: ["@babel/plugin-syntax-dynamic-import"],
                        },
                    },
                },
                {
                    test: /\.mp4$/,
                    use: "file-loader?name=videos/[name].[ext]",
                },
            ],
        },

        resolve: {
            alias: {
                "@": path.resolve("resources/js"),
            },
        },
        devServer: devServer,
    })
    .mergeManifest()
    .version();
// .sourceMaps();
mix.copyDirectory("resources/assets/fonts", "public/fonts");
mix.browserSync("http://localhost:8000/");
