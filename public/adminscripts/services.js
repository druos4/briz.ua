$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.btn-add-prod').click(function (e) {
    e.preventDefault();
    $('#product').html('');
    $('#category').prop('selectedIndex',0);
    $('#addProductModal').modal('show');
});
$('#category').change(function (e) {
    var id = $(this).val();
    $.ajax({
        type:'POST',
        url:'/admin/services/get-prods',
        data:{id:id},
        success:function(data) {
            $('#product').html('');
            if(data.options != ''){
                $('#product').html(data.options);
            }
        }
    });
});
$('.btn-add-selected-product').click(function (e) {
    e.preventDefault();
    var prodId = $('#product').val();
    var ids = $('#products_in').val();
    $.ajax({
        type:'POST',
        url:'/admin/services/add-prod',
        data:{prodId:prodId, ids:ids},
        success:function(data) {
            $('#products_in').val(data.ids);
            if(data.html != ''){
                $('#nestable .dd-list').append(data.html);

            }
            $('#addProductModal').modal('hide');
        }
    });
});

$('#nestable').on("click", '.btn-remove-product', function(e) {
    e.preventDefault();
    var prodId = $(this).data('id');
    var ids = $('#products_in').val();
    $.ajax({
        type:'POST',
        url:'/admin/services/remove-prod',
        data:{prodId:prodId, ids:ids},
        success:function(data) {
            $('#products_in').val(data.ids);
            $('#prod-' + data.id).remove();
        }
    });
});


jQuery(function($){
    var nestable,
        serialized,
        settings = {maxDepth:1},
        saveOrder = $('#saveOrder'),

        edit = $('.edit');

    nestable = $('.dd').nestable(settings);

    saveOrder.on('click', function(e) {
        e.preventDefault();
        serialized = nestable.nestable('serialize');

        $.ajax({
            method:'POST',
            url : "/admin/services/products-save-sort",
            data: { _token: "{!! csrf_token() !!}", serialized: serialized }

        }).done(function (data) {

            $('#products_in').val(data.ids);
            alert("Сохранено!");

        })
    })

    $('#nestable').on("mousedown", '.dd-handle a', function(e){
        e.stopPropagation();
    });

    $('.dd').on('change', function() {
        var serialized = nestable.nestable('serialize');
        $.ajax({
            method:'POST',
            url : "/admin/services/products-save-sort",
            data: { _token: "{!! csrf_token() !!}", serialized: serialized }

        }).done(function (data) {

            $('#products_in').val(data.ids);

        })
    });

    $('[data-rel="tooltip"]').tooltip();

});
