$(document).ready(function () {
    window._token = $('meta[name="csrf-token"]').attr('content')

    moment.updateLocale('en', {
        week: {dow: 1} // Monday is the first day of the week
    })

    $('.date').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'en'
    })

    $('.datetime').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        locale: 'en',
        sideBySide: true
    })

    $('.timepicker').datetimepicker({
        format: 'HH:mm:ss'
    })

    $('.select-all').click(function () {
        let $select2 = $(this).parent().siblings('.select2')
        $select2.find('option').prop('selected', 'selected')
        $select2.trigger('change')
    })
    $('.deselect-all').click(function () {
        let $select2 = $(this).parent().siblings('.select2')
        $select2.find('option').prop('selected', '')
        $select2.trigger('change')
    })

    $('.select2').select2()

    $('.treeview').each(function () {
        var shouldExpand = false
        $(this).find('li').each(function () {
            if ($(this).hasClass('active')) {
                shouldExpand = true
            }
        })
        if (shouldExpand) {
            $(this).addClass('active')
        }
    });



    $('.cb-value').click(function() {
        var mainParent = $(this).parent('.toggle-btn');
        if($(mainParent).find('input.cb-value').is(':checked')) {
            $(mainParent).addClass('active');
            $(mainParent).find('input.cb-value').attr('checked', true);
        } else {
            $(mainParent).removeClass('active');
            $(mainParent).find('input.cb-value').attr('checked', false);
        }

    });

    $('.btn-make-slug').click(function(e) {
        e.preventDefault();
        var title = $('.title_for_slug').val();
        $.ajax({
            type:'POST',
            url:'/make-slug',
            data:{title:title},
            success:function(data) {
                $('#slug').val(data.slug);
            }
        });
    });


    $('.btn-item-del').click(function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var title = $(this).data('title');
        var url = $(this).data('url');
        var headTitle = 'Удалить элемент';

        $('#del-item-id').val(id);
        $('#del-item-url').val(url);
        $('#itemDelModalTitle').html(headTitle);
        $('#del-item-title').html(title);
        $('#itemDelModal').modal('show');
    });

    $('.btn-item-delete').click(function(e) {
        e.preventDefault();
        var id = $('#del-prod-id').val();
        var url = $('#del-item-url').val();
        $.ajax({
            type:'DELETE',
            url:url,
            data:{},
            success:function(data) {
                if(data.id > 0){
                    $('#item-' + data.id).remove();
                    $('#del-item-id').val('');
                    $('#del-item-url').val('');
                    $('#itemDelModalTitle').html('');
                    $('#del-item-title').html('');
                    $('#itemDelModal').modal('hide');
                }
            }
        });
    });

    $('.btn-create-submit').click(function (e) {
        $(this).attr("disabled", true);
        $('#form-create').submit();
    });

});
