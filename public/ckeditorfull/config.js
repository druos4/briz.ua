/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

    config.format_tags = 'p;h2;h3;pre';

    config.extraPlugins = 'youtube';
    config.youtube_width = '320';
    config.youtube_height = '200';
    config.youtube_controls = true;
    config.allowedContent = true;
};
